<?php
/*
 * Author: PhilWeb Corporation
 * Date Created: September 30, 2011
 * File: TMS_LoadTest.php
 * Purpose: For Load Testing
 */
/* URL: 
 * http://192.168.20.8:8088/v2/TicketManagementCMsystem/controller/TMS_LoadTest.php?username=loadtester2&password=password&uname=loadtester7&ticketno=999000024001&virn=999327429475&cancelticketno=999000025001&cancelbookno=999000026000&remarks=for load testing&uniqueid=1
*/
/* ------------------------ Load All Class Modules ---------------------------*/
require_once ("../init.inc.php");
App::LoadModuleClass("TicketManagementCM", "TMAccessRights");
App::LoadModuleClass("TicketManagementCM", "TMAccountSession");
App::LoadModuleClass("TicketManagementCM", "TMAccounts");
App::LoadModuleClass("TicketManagementCM", "TMAccountTypes");
App::LoadModuleClass("TicketManagementCM", "TMAddGame");
App::LoadModuleClass("TicketManagementCM", "TMAuditFunctions");
App::LoadModuleClass("TicketManagementCM", "TMAuditLog");
App::LoadModuleClass("TicketManagementCM", "TMBooks");
App::LoadModuleClass("TicketManagementCM", "TMBoxes");
App::LoadModuleClass("TicketManagementCM", "TMDeckInfo");
App::LoadModuleClass("TicketManagementCM", "TMDecks");
App::LoadModuleClass("TicketManagementCM", "TMDistributors");
App::LoadModuleClass("TicketManagementCM", "TMGameImport");
App::LoadModuleClass("TicketManagementCM", "TMGameManagement");
App::LoadModuleClass("TicketManagementCM", "TMInventoryTemp");
App::LoadModuleClass("TicketManagementCM", "TMLogin");
App::LoadModuleClass("TicketManagementCM", "TMPasswordUpdateRequests");
App::LoadModuleClass("TicketManagementCM", "TMPrizes");
App::LoadModuleClass("TicketManagementCM", "TMPrizesTemp");
App::LoadModuleClass("TicketManagementCM", "TMProducts");
App::LoadModuleClass("TicketManagementCM", "TMProvider");
App::LoadModuleClass("TicketManagementCM", "TMTicketCancellation");
App::LoadModuleClass("TicketManagementCM", "TMTicketValidation");
App::LoadModuleClass("TicketManagementCM", "TMTickets");
App::LoadModuleClass("TicketManagementCM", "TMWinnings");

/* --------------------- Instantiate Class Variables -------------------------*/
$tmlogin = new TMLogin();
$tmdecks = new TMDecks();
$tmbooks = new TMBooks();
$tmboxes = new TMBoxes();
$tmprizes = new TMPrizes();
$tmtickets = new TMTickets();
$tmaddgame = new TMAddGame();
$tmaccounts = new TMAccounts();
$tmproducts = new TMProducts();
$tmprovider = new TMProvider();
$tmwinnings = new TMWinnings();
$tmdeckinfo = new TMDeckInfo();
$tmauditlog = new TMAuditLog();
$tmgames = new TMGameManagement();
$tmprizestemp = new TMPrizesTemp();
$tmgameimport = new TMGameImport();
$tmdistributors = new TMDistributors();
$tmaccessrights = new TMAccessRights();
$tmaccounttypes = new TMAccountTypes();
$tminventorytemp = new TMInventoryTemp();
$tmaccountsession = new TMAccountSession();
$tmauditfunctions = new TMAuditFunctions();
$tmticketvalidation = new TMTicketValidation();
$tmticketcancellation = new TMTicketCancellation();
$tmpasswordupdaterequests = new TMPasswordUpdateRequests();

/* ------------------------------ PARAMETERS ---------------------------------*/
if (isset($_GET['username']) && isset($_GET['password']) && isset($_GET['uname']) && isset($_GET['ticketno']) && isset($_GET['virn']) && isset($_GET['cancelticketno']) && isset($_GET['cancelbookno']) && isset($_GET['remarks']))
{
    $username = $_GET['username'];
    $password = $_GET['password'];
    $uname = $_GET['uname'];
    $uniqueid = $_GET['uniqueid'];
    $virn = $_GET['virn'];
    $remarks = $_GET['remarks'];
    $ticketnum = $_GET['ticketno'];
    $cancelticketno = $_GET['cancelticketno'];
    $cancelbookno = $_GET['cancelbookno'];
}

/* ------------------------- Variable Declarations ---------------------------*/
$deckids = null;
$decktoonstock = null;
$decktoonassigned = null;
$decktoonactive = null;
$booktransbookids = null;
$start = ((($uniqueid - 1) * 50) + 1);
$end = ($uniqueid * 50);
$bookstart = ((($uniqueid - 1) * 50));
$bookend = (($uniqueid * 50) - 1);

/* ----------------------------- Login Process -------------------------------*/
$arrlogin = $tmlogin->Login($username, MD5($password)); 
$logindtls = $arrlogin[0];
$acctid = $logindtls["AID"];
$accttype = $logindtls["AccountTypeID"];
session_regenerate_id();
$sid = session_id(); 

/* ------------------------ Insert Account Session ---------------------------*/
$tmacctsid["AID"] = $acctid;
$tmacctsid["SessionID"] = $sid;            
$tmacctsid["TransDate"] = 'now_usec()';     
$tmacctsid["DateStart"] = 'now_usec()'; 
$tmacctsid["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
$addsession = $tmaccountsession->Insert($tmacctsid);
if($tmaccountsession->HasError)
{
    App::Pr("Error in inserting account session occured: " . $tmaccountsession->getError());
}

/* ---------------------------- User Accounts --------------------------------*/
$arrviewacct = $tmaccounts->SelectAll();

/* -------------------------- Add User Accounts ------------------------------*/
//$insertacct["Username"] = addslashes($uname);
$insertacct["Username"] = $uname;
$insertacct["Password"] = MD5("password");
$insertacct["AccountTypeID"] = '1';
$insertacct["Status"] = '1';
$insertacct["DateCreated"] = 'now_usec()';
$insertacct["CreatedByAID"] = '1';
$adduser = $tmaccounts->Insert($insertacct);
$lastacctid = $tmaccounts->LastInsertID;
if($tmaccounts->HasError)
{
    App::Pr("Error in adding user account: " . $tmaccounts->getError());
}

/* ----------------------- Add Account Information ---------------------------*/
$tmaccounts->UpdateAccountDetails($lastacctid,"load testing name","load testing address","test".$uniqueid."@email.com","1234567890");

/* ---------------------- Accounts List Using Filter -------------------------*/
$acctfilterwhere = " WHERE ac. AccountTypeID IN(1,4,3) ORDER BY ad.Name ASC";
$tmaccounts->SelectAccountByWhere($acctfilterwhere);

/* ---------------------- Accounts List Using Search -------------------------*/
$acctsearchwhere = " WHERE (ac.UserName LIKE '%distribution%' OR ad.Name LIKE '%distribution%')  ORDER BY ad.Name ASC";
$tmaccounts->SelectAccountByWhere($acctsearchwhere);

/* ------------------------------- Add Game ----------------------------------*/
$game["ProviderID"] = '1';
$game["ProductID"] = '3';
$game["GameNumber"] = '11' . $uniqueid;
$game["DateCreated"] = "now_usec()";
$game["CreatedByAID"] = $acctid;
$game["Status"] = "1";
$addgame = $tmgames->Insert($game);
$lastgameid = $tmgames->LastInsertID;
if($tmgames->HasError)
{
    App::Pr("Error in adding game occured: " . $tmgames->getError());
}

/* ------------------------------ Update Game --------------------------------*/
$game["GameNumber"] = '21' . $uniqueid;
$game["GameID"] = $lastgameid;
$updategame = $tmgames->UpdateByArray($game);
if($tmgames->HasError)
{
    App::Pr("Error in updating game occured: " . $tmgames->getError());
}

/* ----------------------------- Add Product ---------------------------------*/
$tmprod["ProductName"] = 'Golden Genie';
$tmprod["ProductDescription"] = 'Golden Genie';
$tmprod["ProviderID"] = '1';
$tmproducts->Insert($tmprod);
$lastprodid = $tmproducts->LastInsertID;
if ($tmproducts->HasError)
{
    App::Pr("Error in adding product occured: " . $tmproducts->getError());
}

/* --------------------------- Update Product --------------------------------*/
$tmprod["ProductName"] = 'Golden Genie2';
$tmprod["ProductDescription"] = 'Golden Genie2';
$tmprod["ProductID"] = $lastprodid;
$tmprod["ProviderID"] = '1';
$tmproducts->UpdateByArray($tmprod);
if ($tmproducts->HasError)
{
    App::Pr("Error in updating product occured: " . $tmproducts->getError());
}

/* ----------------------------- Add Provider --------------------------------*/
$tmprov["Name"] = "LoadTestProvider" . $uniqueid;
$tmprov["Description"] = "LoadTestProvider" . $uniqueid;
$tmprov["Address"] = "Makati City";
$tmprov["ContactNumber"] = "555-55-55";
$addProvider = $tmprovider->Insert($tmprov);
$lastproviderid = $tmprovider->LastInsertID;
if($tmprovider->HasError)
{
    App::Pr("Error in adding provider occured: " . $tmprovider->getError());
}

/* --------------------------- Update Provider -------------------------------*/
$tmprov["Address"] = "Makati City Philippines";
$tmprov["ContactNumber"] = "555555";
$tmprov["ProviderID"] = $lastproviderid;
$updateProvider = $tmprovider->UpdateByArray($tmprov);
if($tmprovider->HasError)
{
    App::Pr("Error in updating provider occured: " . $tmprovider->getError());
}

/* ------------------- Warehouse Inventory Using Filter ----------------------*/
$winvfilterwhere = "A.Status = '4' AND A.AssignedToAID = '6' AND B.GameID = '1' ORDER BY A.BookNumber ASC";
$tmdecks->SelectDeckCount($winvfilterwhere);

/* ------------------- Warehouse Inventory Using Search ----------------------*/
$winvsearchwhere = "A.BookNumber LIKE '%000001%' AND A.Status IN (1,2,3,4) ORDER BY A.BookNumber ASC";
$tmdecks->SelectDeckCount($winvsearchwhere);

/* --------------------------- Item Information ------------------------------*/
for($i = $start,$a = 1 ; $i <= $end ; $i++,$a++)
{
    $deckids[] = $i;
    if($a < 16)
        $decktoonstock[] = $i;
    else if($a < 31)
        $decktoonassigned[] = $i;
    else
        $decktoonactive[] = $i;
}

/*--------------- Item Information from OnFreight to OnStock -----------------*/
$tmdecks->UpdateDeckStatusToOnStock('3',$deckids,$acctid);

/*--------------- Item Information from OnStock to Assigned ------------------*/
$tmdecks->UpdateStatusToAssigned('4','1',$deckids,$acctid);

/*--------------- Item Information from Assigned to Active -------------------*/
$tmdecks->UpdateStatusToActive('1',date('m-d-Y' , strtotime ('+1 day' , strtotime(date("Y-m-d")))),$decktoonactive,$acctid);

/*--------------- Item Information from Assigned to OnStock ------------------*/
$tmdecks->UpdateDeckStatusToOnStock('3',$decktoonstock,$acctid);

/*--------------- Item Information from Assigned to Assigned ------------------*/
$tmdecks->UpdateStatusToAssigned('4','2',$decktoonassigned,$acctid);

/* ------------------- Book Transfer List using filter -----------------------*/
$booktransfilterwhere = "A.GameID = '1' AND A.AssignedToAID = '5' AND A.Status = 1";
$tmdecks->SelectDeckCount($booktransfilterwhere);

/* ------------------- Book Transfer List using search -----------------------*/
$booktranssrchwhere = "A.BookNumber LIKE '%0000%' AND A.Status = 1";
$tmdecks->SelectDeckCount($booktransfilterwhere);

/* -------------------------- Book Transfer Update ---------------------------*/
$booktranswhere = "A.GameID = '1' AND A.AssignedToAID = '5' AND A.Status = 1 LIMIT 1000";
$allactivebooks = $tmdecks->SelectDeckCondition($booktranswhere);
for($i = $bookstart ; $i <= $bookend ; $i++)
{
    $booktransbookids[] = $allactivebooks[$i]["ID"];
}
$updatebooktrans = $tmdecks->UpdateDeckDtlsBookTrans($booktransbookids, '6', $acctid, $acctid);
if($tmdecks->HasError)
{
    App::Pr("Error in book transfer: " . $tmdecks->getError());
}
else
{
    //insert to booktransferhistory
    for($i = 0 ; $i < count($bookid) ; $i++)
    {
        $history["BookID"] = $bookid[$i];
        $history["FromAssignedAID"] = '5';
        $history["ToAssignedAID"] = '6';
        $history["DateCreated"] = 'now_usec()';
        $history["TransferredByAID"] = $acctid;
        $history["ApprovedByAID"] = '1';
        $booktranshist = $tmbooktranshist->Insert($history);
        if($tmbooktranshist->HasError)
        {
            $errortitle = "ERROR!";
            $errormsg = "Error in inserting book transfer history: " . $tmbooktranshist->getError();
        }
    }
}

/* -------------------------- Ticket Validation ------------------------------*/
$valno = $virn;
$fullvirn = $virn;
$checksum = substr($fullvirn,11,12);
$book = $ticketnum;      
$gameno = substr($valno, 0,3);
$valno = substr($valno, 0,11);
$valno2 = substr($valno, 3,11);
$ticketno = substr($book,9,3);
$book = substr($book, 3,6);
    
//check if game number is valid
$arrGame = $tmgames->SelectByGameNumber($gameno);
if (count($arrGame) == 1)
{
    $arrdtls = $arrGame[0];
    $gameid = $arrdtls["GameID"];
}
    
//check if valid checksum
$checkdigit = CheckDigit($valno);
if($checkdigit % 10 == $checksum)
{
    //check if valid VIRN
    $arrWinning = $tmticketvalidation->IsValidVIRN($valno2,$gameid);
    if (count($arrWinning) == 1)
    {
        //check if valid book number
        $arrBookDtls = $tmdecks->IsBookNoValid($book);
        if (count($arrBookDtls) == 1)
        {
            $arrdtlsbooks = $arrBookDtls[0];
            $statusbooks = $arrdtlsbooks["Status"];
            $bookid = $arrdtlsbooks["BookID"];

            if ($statusbooks == 1)
            { 
                //check if ticketno is valid
                $arrTicketDtls = $tmtickets->IsTicketNoValid($ticketno, $bookid);
                if (count($arrTicketDtls) == 1)
                {
                    $arrdtls2 = $arrTicketDtls[0];
                    $ticketstatus = $arrdtls2["Status"];
                    if ($ticketstatus == 1)
                    {
                        $arrdtls = $arrWinning[0];
                        $winningcardid = $arrdtls["WinningCardID"];
                        $cardno = $arrdtls["CardNumber"];
                        $prizetypeid = $arrdtls["PrizeTypeID"];
                        $prizeid = $arrdtls["PrizeID"];
                        $prizename = $arrdtls["PrizeName"];
                        $claimdate = $arrdtls["ClaimDate"];
                        $claimtime = $arrdtls["ClaimTime"];
                        $status = $arrdtls["Status"];
                    }                        
                }
            }
        }
    }
        
    //get payout level and check if user is allowed to payout the prize
    $arrprize = $tmprizes->SelectByPrizeStatus($prizename);                
    if (count($arrprize) == 1)
    {
        $arrdtls = $arrprize[0];
        $payoutlevel = $arrdtls["PayoutLevel"];
    }
        
    //start claiming
    //update book number        
    $tmparam["BookNumber"] = $book;
    $tmparam["Status"] = '2';
    $tmparam["WinningCardID"] = $winningcardid;
    $tmparam["DateClaimed"] = 'now_usec()';
    $tmparam["ClaimedByAID"] = $acctid;       
    $tmupdatewinningcard = $tmticketvalidation->UpdateByArray($tmparam);

    //update deckinfo
    $arrdeckinfo = $tmdeckinfo->GetDeckInfoByGameID($gameid);
    if (count($arrdeckinfo) == 1)
    {
        $arrdtls = $arrdeckinfo[0];
        $usedwinningcardcount = $arrdtls["UsedWinningCardCount"];
        $claimedwinningcardcount = $arrdtls["ClaimedWinningCardCount"];
        $deckid = $arrdtls["DeckID"];

        $usedwinningcardcount = $usedwinningcardcount + 1;
        $claimedwinningcardcount = $claimedwinningcardcount + 1;

        $tmdeckinfoparam["UsedWinningCardCount"] = $usedwinningcardcount; 
        $tmdeckinfoparam["ClaimedWinningCardCount"] = $claimedwinningcardcount; 
        $tmdeckinfoparam["DeckID"] = $deckid;

        $tmupdatedeckinfo = $tmdeckinfo->UpdateByArray($tmdeckinfoparam);
    }

    //insert in auditlog table                
    $tmaudit["SessionID"] = $sid;
    $tmaudit["AID"] = $acctid;
    $tmaudit["TransDetails"] = 'Validation No.: '.$valno;
    $tmaudit["TransDateTime"] = 'now_usec()';
    $tmaudit["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
    $tmaudit["AuditTrailFunctionID"] = '12';    
    $tmauditlog->Insert($tmaudit);           
    //end claiming
} 

/*------------------------------ Site Inventory ------------------------------*/
$arrDecks = $tmdecks->SelectDeck('0', '0', '2011-10-04', '2011-10-04');

/*------------------------------- Cancellation -------------------------------*/
//get book status 
$bookno = $cancelbookno; 
$bookno2 = $cancelticketno;
$gameno = substr($bookno, 0,3);
$ticketno = substr($bookno,9,3);
$bookno = substr($bookno, 3,6);
$bookno2 =  substr($bookno2, 3,6);
     
$arrGame = $tmgames->SelectByGameNumber($gameno);
if (count($arrGame) == 1)
{
    $arrdtls = $arrGame[0];
    $gameid = $arrdtls["GameID"];
}
        
$arrBookStatus = $tmdecks->GetBookStatus($bookno,$gameid);
if (count($arrBookStatus) == 1)
{
    $arrdtls = $arrBookStatus[0];
    $bookstatus = $arrdtls["Status"];
    $id = $arrdtls["ID"];
    $bookid = $arrdtls["BookID"];
    if ($bookstatus != '5')
    {
        if ($ticketno == '000')
        {
            //update book status        
            $tmbookparam["Status"] = '5';
            $tmbookparam["Remarks"] = $remarks;
            $tmbookparam["CancelledByAID"] = $acctid;
            $tmbookparam["DateCancelled"] = 'now_usec()';
            $tmbookparam["UpdatedByAID"] = $acctid;
            $tmbookparam["ID"] = $id;
            $tmupdatebook = $tmdecks->UpdateByArray($tmbookparam);

            //update tickets 
            $tmcanceltickets = $tmtickets->UpdateTicketsByBook($bookid);

            //insert in ticket cancellation table
            $tmbatchinsert = $tmticketcancellation->TicketBatchInsert($acctid, $remarks, $bookid);

            //insert into audittrail
            $tmaudit["SessionID"] = $sid;
            $tmaudit["AID"] = $acctid;
            $tmaudit["TransDetails"] = 'Book Number: '.$bookno;
            $tmaudit["TransDateTime"] = 'now_usec()';
            $tmaudit["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
            $tmaudit["AuditTrailFunctionID"] = '13';    
            $tmauditlog->Insert($tmaudit);                    
        }
        if ($ticketno2 != '000')
        {
            //check ticket status
            $arrTicketDtls = $tmtickets->IsTicketNoValid($ticketno, $bookid);
            if (count($arrTicketDtls) == 1)
            {
                $arrdtls2 = $arrTicketDtls[0];
                $ticketstatus = $arrdtls2["Status"];
                $ticketid = $arrdtls2["TicketID"];
                if ($ticketstatus == 1)
                {
                    //update tickets table
                    $tmticketsparam["Status"] = '5';
                    $tmticketsparam["TicketID"] = $ticketid;
                    $tmupdatebook = $tmtickets->UpdateByArray($tmticketsparam);                         

                    //insert in ticket cancellation table
                    $tmticket["TicketID"] = $ticketid;
                    $tmticket["DateCancelled"] = 'now_usec()';
                    $tmticket["CancelledByAID"] = $acctid;
                    $tmticket["Remarks"] =  $remarks;
                    $tmticket["CancelType"] =  1;
                    $tmticketcancellation->Insert($tmticket);

                    //insert into audittrail
                    $tmaudit["SessionID"] = $sid;
                    $tmaudit["AID"] = $acctid;
                    $tmaudit["TransDetails"] = 'Book Number: '.$bookno;
                    $tmaudit["TransDateTime"] = 'now_usec()';
                    $tmaudit["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
                    $tmaudit["AuditTrailFunctionID"] = '23';    
                    $tmauditlog->Insert($tmaudit);     
                }
                elseif ($ticketstatus == 5)
                {
                    $errormsg = 'Ticket number has already been cancelled.';
                    $errormsgtitle = 'SUCCESSFUL!';
                }
                else
                {
                    $errormsg = "Ticket number does not exist.";
                    $errormsgtitle = 'ERROR!';  
                }                        
            }
        }
    }
}  

/* ------------------------------ Game List ----------------------------------*/
$gamelstwhere = " WHERE gm.ProviderID = '1' ORDER BY gm.GameNumber ASC";
$tmgames->SelectGameByWhere($gamelstwhere);

/* ---------------------------- Product List ---------------------------------*/
$tmproducts->SelectAllProducts();

/* ---------------------------- Provider List --------------------------------*/
$tmprovider->SelectAllProvider();


/* ------------------------ FUNCTION CHECKDIGIT() ----------------------------*/
function CheckDigit($number) 
{ 
    // Strip any non-digits 
    $number=preg_replace('/\D/', '', $number);

    // Set the string length 
    $number_length=strlen($number);
    
    for ($i=1; $i<=$number_length; $i++) 
    {
            $digit = $number[$i-1];
            if($i % 2)
            {
                    //step 1
                    //odd numbers
                    $total_step1 += $digit;
            }
            else
            {
                    //step 3
                    //even numbers
                    $total_step3 += $digit;
            }
    }
    //step2
    $result = $total_step1 * 3;
    //step4
    $result = $result + $total_step3;
    //step5
    $result = 10 - $result % 10;

    return $result;      
}
?>
