<?php
/*
 * Added By : Arlene R. Salazar
 * Added On : Aug 24, 2011
 * Purpose : Process for account list 
 */

$pagesubmenuid = 1;
$stylesheets[] = "css/default.css";

App::LoadModuleClass("TicketManagementCM", "TMAccountType");
App::LoadModuleClass("TicketManagementCM", "TMAccounts");

App::LoadControl("TextBox");
App::LoadControl("ComboBox");
App::LoadControl("Button");
App::LoadControl("Hidden");
App::LoadControl("PagingControl2");

$frmacctlist = new FormsProcessor();
$accounttype = new TMAccountType();
$account = new TMAccounts();

/*PAGING*/
$itemsperpage = 15;
$pgcon = new PagingControl2($itemsperpage, 1);
$pgcon->URL = "javascript:ChangePage(%currentpage);";
$pgcon->ShowMoveToFirstPage = true;
$pgcon->ShowMoveToLastPage = true;
/*PAGING*/

/* Added By Noel Antonio - June 19, 2012 */
unset($_SESSION['srchaccttype']);
unset($_SESSION['srchstatus']);
unset($_SESSION['srchuname']);
/* Added By Noel Antonio - June 19, 2012 */

$txtsrchuname = new TextBox("txtsrchuname","txtsrchuname","");
$txtsrchuname->Length = 150;
$txtsrchuname->Args = "size='50'";
$txtsrchuname->ShowCaption = true;
if(isset($_SESSION['srchuname']))
    $txtsrchuname->Text = $_SESSION['srchuname'];

$hiddenaccountid = new Hidden("hiddenaccountid","hiddenaccountid","Hidden Account Id for Edit");

$hiddenctr = new Hidden("hiddenctr","hiddenctr","Hidden Counter");
$hiddenctr->Text = '0';

$accounttypes = $accounttype->SelectAll();
$accttype_list = new ArrayList();
$accttype_list->AddArray($accounttypes);

$ddlsrchaccttype = new ComboBox("ddlaccttype","ddlaccttype","Account Type: ");
$litem = null;
$litem[] = new ListItem("Please select", "", true);
$litem[] = new ListItem("All", "0");
$ddlsrchaccttype->Items = $litem;
$ddlsrchaccttype->ShowCaption = true;
$ddlsrchaccttype->DataSource = $accttype_list;
$ddlsrchaccttype->DataSourceText = "Name";
$ddlsrchaccttype->DataSourceValue = "AccountTypeID";
$ddlsrchaccttype->DataBind();
if(isset($_SESSION['srchaccttype']))
    $ddlsrchaccttype->SetSelectedValue($_SESSION['srchaccttype']);

$ddlStatus = new ComboBox("ddlStatus","ddlStatus","Status: ");
$ddlStatus->ShowCaption = true;
$options = null;
$options[] = new ListItem("Please select", "", true);
$options[] = new ListItem("All","0");
$options[] = new ListItem("Active","1");
$options[] = new ListItem("Locked","4");
$options[] = new ListItem("Locked - Invalid Attempts","3");
$ddlStatus->Items = $options;
if(isset($_SESSION['srchstatus']))
    $ddlStatus->SetSelectedValue($_SESSION['srchstatus']);

if((isset($_SESSION['srchuname'])) || (isset($_SESSION['srchaccttype'])) || (isset($_SESSION['srchstatus'])))
{
    if(isset($_SESSION['srchuname']))
    {
        $hiddenctr->Text = 0;
        $where = " WHERE (ac.UserName LIKE '%" . mysql_escape_string(trim($_SESSION['srchuname'])) . "%' OR ad.Name LIKE '%" . mysql_escape_string(trim($_SESSION['srchuname'])) . "%') ORDER BY ad.Name ASC";
    }
    if((isset($_SESSION['srchaccttype'])) && (isset($_SESSION['srchstatus'])))
    {
        $hiddenctr->Text = 0;
        if(($_SESSION['srchaccttype'] == 0) && ($_SESSION['srchstatus'] == 0))
            $where = " WHERE ac.Status IN (1,4,3) ORDER BY ad.Name ASC";
        else if(($_SESSION['srchaccttype'] > 0) && ($_SESSION['srchstatus'] == 0))
            $where = " WHERE ac. AccountTypeID = '" . trim($_SESSION['srchaccttype']) . "'  ORDER BY ad.Name ASC";
        else if(($_SESSION['srchaccttype'] == 0) && ($_SESSION['srchstatus'] > 0))
            $where = " WHERE ac.Status = '".trim($_SESSION['srchstatus'])."' ORDER BY ad.Name ASC";
        else
            $where = " WHERE ac. AccountTypeID = '" . trim($_SESSION['srchaccttype']) . "' AND ac.Status = '".trim($_SESSION['srchstatus'])."' ORDER BY ad.Name ASC";
    }

    $srchacctlist = $account->SelectAccountByWhere($where);
    $providercount = count($srchacctlist);
    $pgcon->Initialize($itemsperpage, $providercount);	
    $pgTransactionHistory = $pgcon->PreRender();
    $arrproviders = $account->SelectAccountByWhereWithLimit(($pgcon->SelectedItemFrom - 1), $itemsperpage,$where);
    $accounts_list = new ArrayList();
    $accounts_list->AddArray($arrproviders);
}


$btnAddAcct = new Button("btnAddAcct","btnAddAcct","Add Account");
$btnAddAcct->IsSubmit = true;

$btnSearch = new Button("btnSearch","btnSearch","Search");
$btnSearch->IsSubmit = true;
$btnSearch->Args = "onclick='javascript: return checkusersearch();'";

$btnSubmit = new Button("btnSubmit","btnSubmit","Submit");
$btnSubmit->IsSubmit = true;
$btnSubmit->Args = "onclick='javascript: return checkAcctType();'";

$frmacctlist->AddControl($txtsrchuname);
$frmacctlist->AddControl($ddlsrchaccttype);
$frmacctlist->AddControl($btnAddAcct);
$frmacctlist->AddControl($btnSearch);
$frmacctlist->AddControl($hiddenaccountid);
$frmacctlist->AddControl($hiddenctr);
$frmacctlist->AddControl($btnSubmit);
$frmacctlist->AddControl($ddlStatus);

$frmacctlist->ProcessForms();

if($frmacctlist->IsPostBack)
{
    if($btnAddAcct->SubmittedValue == "Add Account")
    {
        URL::Redirect('adduser.php');
    }
    if($hiddenctr->Text == 0)
    {
	$where = " WHERE ac.UserName LIKE '%" . mysql_escape_string(trim($txtsrchuname->SubmittedValue)) . "%' OR ad.Name LIKE '%" . mysql_escape_string(trim($txtsrchuname->SubmittedValue)) . "%'  ORDER BY ad.Name ASC";
    }
    if($hiddenctr->Text > 0)
    {
	if(($ddlsrchaccttype->SelectedValue == 0) && ($ddlStatus->SelectedValue == 0))
            $where = " WHERE ac.Status IN (1,4,3) ORDER BY ad.Name ASC";
        else if(($ddlsrchaccttype->SelectedValue > 0) && ($ddlStatus->SelectedValue == 0))
            $where = " WHERE ac. AccountTypeID = '" . $ddlsrchaccttype->SelectedValue . "'  ORDER BY ad.Name ASC";
        else if(($ddlsrchaccttype->SelectedValue == 0) && ($ddlStatus->SelectedValue > 0))
            $where = " WHERE ac.Status = '".$ddlStatus->SelectedValue."' ORDER BY ad.Name ASC";
        else
            $where = " WHERE ac. AccountTypeID = '" . $ddlsrchaccttype->SelectedValue . "' AND ac.Status = '".$ddlStatus->SelectedValue."' ORDER BY ad.Name ASC";
    }
    /*if($btnAddAcct->SubmittedValue == "Add Account")
    {
        URL::Redirect("views/adduser.php");
    }*/
    if($btnSearch->SubmittedValue == "Search")
    {
	unset($_SESSION['srchaccttype']);
	unset($_SESSION['srchstatus']);
    $_SESSION['srchuname'] = $txtsrchuname->SubmittedValue;
    $hiddenctr->Text = "0";
    $where = " WHERE (ac.UserName LIKE '%" . mysql_escape_string(trim($txtsrchuname->SubmittedValue)) . "%' OR ad.Name LIKE '%" . mysql_escape_string(trim($txtsrchuname->SubmittedValue)) . "%')  ORDER BY ad.Name ASC"; 
	$pgcon->SelectedPage = 1;
    }
    if($btnSubmit->SubmittedValue == "Submit")
    {
	unset($_SESSION['srchuname']);
        $_SESSION['srchaccttype'] = $ddlsrchaccttype->SelectedValue;
	$_SESSION['srchstatus'] = $ddlStatus->SelectedValue;
        $hiddenctr->Text = "1";
        if(($ddlsrchaccttype->SelectedValue == 0) && ($ddlStatus->SelectedValue == 0))
            $where = " WHERE ac.Status IN (1,4,3) ORDER BY ad.Name ASC";
        else if(($ddlsrchaccttype->SelectedValue > 0) && ($ddlStatus->SelectedValue == 0))
            $where = " WHERE ac. AccountTypeID = " . $ddlsrchaccttype->SelectedValue . "  ORDER BY ad.Name ASC";
        else if(($ddlsrchaccttype->SelectedValue == 0) && ($ddlStatus->SelectedValue > 0))
            $where = " WHERE ac.Status = ".$ddlStatus->SelectedValue." ORDER BY ad.Name ASC";
        else
            $where = " WHERE ac. AccountTypeID = " . $ddlsrchaccttype->SelectedValue . " AND ac.Status = ".$ddlStatus->SelectedValue." ORDER BY ad.Name ASC";
	$pgcon->SelectedPage = 1;
    }
    /*PAGING*/
    $srchacctlist = $account->SelectAccountByWhere($where);
    $providercount = count($srchacctlist);
    $pgcon->Initialize($itemsperpage, $providercount);
    $pgTransactionHistory = $pgcon->PreRender();
    $arrproviders = $account->SelectAccountByWhereWithLimit(($pgcon->SelectedItemFrom - 1), $itemsperpage,$where);
    $accounts_list = new ArrayList();
    $accounts_list->AddArray($arrproviders);
    /*PAGING*/
}
?>