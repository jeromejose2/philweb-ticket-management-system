<?php
/*
 * Created by Arlene R. Salazar on 09-15-2011
 * Purpose: controller for add game
 */
$pagesubmenuid = 18;
$stylesheets[] = "css/default.css";
$javascripts[] = "jscripts/validations.js";
$javascripts[] = "jscripts/jquery-1.5.2.min.js";

App::LoadModuleClass("TicketManagementCM", "TMGameManagement");
App::LoadModuleClass("TicketManagementCM", "TMProvider");
App::LoadModuleClass("TicketManagementCM", "TMProducts");
App::LoadModuleClass("TicketManagementCM", "TMAuditLog");

$tmgames = new TMGameManagement();
$tmproviders = new TMProvider();
$tmproducts = new TMProducts();
$tmauditlog = new TMAuditLog();

App::LoadControl("TextBox");
App::LoadControl("ComboBox");
App::LoadControl("Button");
App::LoadControl("Hidden");

$frmAddgame = new FormsProcessor();

$providers = $tmproviders->SelectAllProvider();
$provider_list = new ArrayList();
$provider_list->AddArray($providers);

$ddlProvider = new ComboBox("ddlProvider","ddlProvider","Provider: ");
$options = null;
$options[] = new ListItem("","",true);
$ddlProvider->Items = $options;
$ddlProvider->DataSource = $provider_list;
$ddlProvider->DataSourceText = "Name";
$ddlProvider->DataSourceValue = "ProviderID";
$ddlProvider->DataBind();
$ddlProvider->Style = "width: 130px";
$ddlProvider->Args = "onchange = 'javascript: return get_products(); ClearTextbox();'";

$products = $tmproducts->SelectAllProduct();

$ddlProduct = new ComboBox("ddlProduct","ddlProduct","Product: ");
$ddlProduct->Style = "width: 130px";
$ddlProduct->Args = "onchange='get_products_cardvalue(); ClearTextbox();'";

$gamenums = $tmgames->SelectAllGames();
$gamenum_list = new ArrayList();
$gamenum_list->AddArray($gamenums);

$txtGameNumber = new TextBox("txtGameNumber","txtGameNumber","Game Number: ");
$txtGameNumber->Length = 3;
$txtGameNumber->Args = "size='13' onkeypress='javascript: return isNumberKey(event);' autocomplete='off'";

$txtNewGameName = new TextBox("txtNewGameName","txtNewGameName","New Game Name");
$txtNewGameName->Length = 30;
$txtNewGameName->Args = "size='23'";
$txtNewGameName->Args = "onblur='get_products_cardvalue2();' onfocus='enabletxtcardval();' autocomplete='off'";

$txtCardValue = new TextBox("txtCardValue","txtCardValue","Card Value");
$txtCardValue->Length = 14;
$txtCardValue->Args = "size='13' onkeypress='javascript: return isNumberKey(event);' autocomplete='off'";

$txtBookSize = new TextBox("txtBookSize","txtBookSize","Book Size");
$txtBookSize->Length = 11;
$txtBookSize->Args = "size='13' onkeypress='javascript: return isNumberKey(event);' autocomplete='off'";

$btnSave = new Button("btnSave","btnSave","Save");
$btnSave->IsSubmit = true;
$btnSave->Args = "onclick='javascript: return checkAddGame();'";

$btnConfirm = new Button("btnConfirm","btnConfirm","Save");
$btnConfirm->IsSubmit = true;

$btnCancel = new Button("btnCancel","btnCancel","Cancel");
$btnCancel->Args = "onclick='javascript: return redirectToGameList();'";

$hiddenCardValue = new Hidden("hiddenCardValue","hiddenCardValue");
$hiddenBookSize = new Hidden("hiddenBookSize", "hiddenBookSize");
$hiddenGameNumber = new Hidden("hiddenGameNumber", "hiddenGameNumber");
$hidFlag = new Hidden("hidFlag", "hidFlag");

$frmAddgame->AddControl($ddlProvider);
$frmAddgame->AddControl($ddlProduct);
$frmAddgame->AddControl($txtGameNumber);
$frmAddgame->AddControl($txtNewGameName);
$frmAddgame->AddControl($txtCardValue);
$frmAddgame->AddControl($txtBookSize);
$frmAddgame->AddControl($btnSave);
$frmAddgame->AddControl($btnCancel);
$frmAddgame->AddControl($btnConfirm);
$frmAddgame->AddControl($hiddenCardValue);
$frmAddgame->AddControl($hiddenBookSize);
$frmAddgame->AddControl($hiddenGameNumber);
$frmAddgame->AddControl($hidFlag);

$frmAddgame->ProcessForms();

if($frmAddgame->IsPostBack)
{
    // Populate Game Name drop down list
    $options = null;
    $options[] = new ListItem("- - - - - - - - - - - - - -","",true);
    $ddlProduct->Items = $options;
    $products = $tmproducts->GetAllProductsPerProvider($ddlProvider->SubmittedValue);
    $product_list = new ArrayList();
    $product_list->AddArray($products);
    $ddlProduct->DataSource = $product_list;
    $ddlProduct->DataSourceText = "ProductName";
    $ddlProduct->DataSourceValue = "ProductID";
    $ddlProduct->DataBind();
    $ddlProduct->SetSelectedValue($ddlProduct->SubmittedValue);
    
    if ($btnSave->SubmittedValue == "Save")
    {
        // Flag 1 means new game. Flow of data starts from textfield to hidden.
        if ($hidFlag->SubmittedValue == 1)
        {
            $hiddenCardValue->Text = $txtCardValue->Text;
            $hiddenBookSize->Text = $txtBookSize->Text;
        }
        
        // Pass the values of hidden to textfields again
        $txtCardValue->Text = $hiddenCardValue->Text;
        $txtBookSize->Text = $hiddenBookSize->Text;
        $txtGameNumber->Text = $hiddenGameNumber->Text;
        
        // Check game number already exist
        $isGameNumExists = $tmgames->CheckIfGameNumberExists($txtGameNumber->Text, $ddlProvider->SubmittedValue);
        if(count($isGameNumExists) > 0)
        {
                $message = "Game Number already exists. Please enter a unique Game Number.";
                $title = "ERROR!";
        }
        else if ($ddlProduct->SubmittedValue == "")
        {
            $bool_product_exist = false;
            
            // Check product name already exist
            $isExist = $tmproducts->DoesProductExists(mysql_escape_string(trim($txtNewGameName->SubmittedValue)),$ddlProvider->SubmittedValue);
            if(count($isExist) > 0)
            {
                $message = "Game name is already used. Please enter a different game name.";
                $title = "ERROR!";
                $bool_product_exist = true;
            }
            else
                $product_success = "ok";
        }
        else
            $product_success = "ok";
        
        
        // Enable-Disable of fields
        if ($ddlProduct->SubmittedValue != "")
        {
            $txtNewGameName->Enabled = false;
            $txtBookSize->Enabled = false;
            $txtCardValue->Enabled = false;
        }
        else
        {
            $txtNewGameName->Enabled = true;
            $txtBookSize->Enabled = true;
            $txtCardValue->Enabled = true;
        }
        
        if ($txtNewGameName->SubmittedValue != "" && !$bool_product_exist)
        {
            $txtCardValue->Enabled = true;
            $txtBookSize->Enabled = true;
        }
        else
        {
            $txtCardValue->Enabled = false;
            $txtBookSize->Enabled = false;
        }
     
    }

    if ($btnConfirm->SubmittedValue == "Save")
    {
        $txtCardValue->Text = $hiddenCardValue->Text;
        $txtBookSize->Text = $hiddenBookSize->Text;
        $txtGameNumber->Text = $hiddenGameNumber->Text;
        
        $tmpname = $txtNewGameName->SubmittedValue;
        if(strlen($tmpname) > 0)
        {
            // Insert product or game name
            $product["ProductName"] = trim($txtNewGameName->SubmittedValue);
            $product["ProductDescription"] = trim($txtNewGameName->SubmittedValue);
            $product["ProviderID"] = $ddlProvider->SelectedValue;
            $tmproducts->Insert($product);
            if($tmproducts->HasError)
            {
                $message = "Error occured: " . $tmproducts->getError();
                $title = "ERROR!";
                $product_success = "error";
            }
            else
            {
                $productid = $tmproducts->LastInsertID;
                $product_success = "ok";
            }
        }
        else
        {
            $productid = $ddlProduct->SelectedValue;
        }
        
        // Insert game
        $game["ProviderID"] = $ddlProvider->SelectedValue;
        $game["ProductID"] = $productid;
        $game["GameNumber"] = trim($txtGameNumber->Text);    
        $game["CardPrice"] = $txtCardValue->Text;
        $game["CurrentDollarRate"] = "1";
        $game["CurrencyID"] = "1";
        $game["DateCreated"] = "now_usec()";
        $game["CreatedByAID"] = $_SESSION['acctid'];
        $game["BookTicketCount"] = $txtBookSize->Text; 
        $game["Status"] = "1";
        $tmgames->Insert($game);
        if($tmgames->HasError)
        {
            $message = "Error occured: " . $tmgames->getError();
            $title = "ERROR!";
        }
        else
        {
	    // Insert in auditlog table
            $tmaudit["SessionID"] = $_SESSION['sid'];
            $tmaudit["AID"] = $_SESSION['acctid'];
            $tmaudit["TransDetails"] = 'Game ID: '.$tmgames->LastInsertID;
            $tmaudit["TransDateTime"] = 'now_usec()';
            $tmaudit["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
            $tmaudit["AuditTrailFunctionID"] = '24';    
            $tmauditlog->Insert($tmaudit);
            if ($tmauditlog->HasError)
            {
                $message = "Error occured: " . $tmproducts->getError();
                $title = "ERROR!";
            }
            else
            {
                $successmsg = "A new game has been successfully created.";
                $successtitle = "SUCCESS";
            } 
        }
    }
}
?>