<?php
/*
 * Created by Arlene R. Salazar on 09-15-2011
 * Purpose: controller for add game
 */
$pagesubmenuid = 18;
$stylesheets[] = "css/default.css";
$javascripts[] = "jscripts/validations.js";
$javascripts[] = "jscripts/jquery-1.5.2.min.js";

App::LoadModuleClass("TicketManagementCM", "TMGameManagement");
App::LoadModuleClass("TicketManagementCM", "TMProvider");
App::LoadModuleClass("TicketManagementCM", "TMProducts");
App::LoadModuleClass("TicketManagementCM", "TMAuditLog");

$tmgames = new TMGameManagement();
$tmproviders = new TMProvider();
$tmproducts = new TMProducts();
$tmauditlog = new TMAuditLog();

App::LoadControl("TextBox");
App::LoadControl("ComboBox");
App::LoadControl("Button");

$frmAddgame = new FormsProcessor();

$providers = $tmproviders->SelectAllProvider();
$provider_list = new ArrayList();
$provider_list->AddArray($providers);

$ddlProvider = new ComboBox("ddlProvider","ddlProvider","Provider: ");
$options = null;
$options[] = new ListItem("","",true);
$ddlProvider->Items = $options;
$ddlProvider->DataSource = $provider_list;
$ddlProvider->DataSourceText = "Name";
$ddlProvider->DataSourceValue = "ProviderID";
$ddlProvider->DataBind();
//$ddlProvider->SetSelectedValue(1);
$ddlProvider->Style = "width: 130px";
$ddlProvider->Args = "onchange = 'javascript: return get_products(); ClearTextbox();'";

$products = $tmproducts->SelectAllProduct();

$ddlProduct = new ComboBox("ddlProduct","ddlProduct","Product: ");
//$options = null;
//$options[] = new ListItem("- - - - - - - - - - - - - -","",true);
/*$ddlProduct->DataSource = $product_list;
$ddlProduct->DataSourceText = "ProductName";
$ddlProduct->DataSourceValue = "ProductID";
$ddlProduct->DataBind();*/
$ddlProduct->Style = "width: 130px";
$ddlProduct->Args = "onchange='get_products_cardvalue(); ClearTextbox();'";

$gamenums = $tmgames->SelectAllGames();
$gamenum_list = new ArrayList();
$gamenum_list->AddArray($gamenums);

/*$ddlGameNum = new ComboBox("ddlGameNum","ddlGameNum","Game Number: ");
$options = null;
$options[] = new ListItem("","",true);
$ddlGameNum->Items = $options;
$ddlGameNum->DataSource = $gamenum_list;
$ddlGameNum->DataSourceText = "GameNumber";
$ddlGameNum->DataSourceValue = "GameID";
$ddlGameNum->DataBind();
$ddlGameNum->Style = "text-align: center;width: 130px;";*/

$txtGameNumber = new TextBox("txtGameNumber","txtGameNumber","Game Number: ");
$txtGameNumber->Length = 3;
//$txtGameNumber->Args = "size='13' onkeypress='javascript: return isNumberKey(event);'"; 
//$txtGameNumber->Args = "size='13' onkeypress='javascript: return isAlphaNumericKey(event);' onkeyup='javascript: return isAlphaNumericKey2(event);'";
$txtGameNumber->Args = "disabled = 'true' size='13' onkeypress='javascript: return isNumberKey(event);'";

$txtNewGameName = new TextBox("txtNewGameName","txtNewGameName","New Game Name");
$txtNewGameName->Length = 30;
//$txtNewGameName->Args = "size='23' onkeypress='javascript: return isAlphaKey(event);'";
$txtNewGameName->Args = "size='23'";
$txtNewGameName->Args = "disabled = 'true' onblur='get_products_cardvalue2();' onfocus='enabletxtcardval();'";

$txtCardValue = new TextBox("txtCardValue","txtCardValue","Card Value");
$txtCardValue->Length = 14;
$txtCardValue->Args = "disabled = 'true' size='13' onkeypress='javascript: return verifycardvalue(event);'";

$btnSave = new Button("btnSave","btnSave","Save");
$btnSave->IsSubmit = true;
$btnSave->Args = "onclick='javascript: return checkAddGame();'";

$btnConfirm = new Button("btnConfirm","btnConfirm","Save");
$btnConfirm->IsSubmit = true;


$btnCancel = new Button("btnCancel","btnCancel","Cancel");
$btnCancel->Args = "onclick='javascript: return redirectToGameList();'";

$frmAddgame->AddControl($ddlProvider);
$frmAddgame->AddControl($ddlProduct);
$frmAddgame->AddControl($txtGameNumber);
$frmAddgame->AddControl($txtNewGameName);
$frmAddgame->AddControl($txtCardValue);
$frmAddgame->AddControl($btnSave);
$frmAddgame->AddControl($btnCancel);
$frmAddgame->AddControl($btnConfirm);

$frmAddgame->ProcessForms();

if($frmAddgame->IsPostBack)
{
if($ddlProduct->SubmittedValue != ""){ 
	$products = $tmproducts->GetAllProductsPerProvider($ddlProvider->SubmittedValue);
	$product_list = new ArrayList();
	$product_list->AddArray($products);
	$ddlProduct->DataSource = $product_list;
	$ddlProduct->DataSourceText = "ProductName";
	$ddlProduct->DataSourceValue = "ProductID";
	$ddlProduct->DataBind();
	$ddlProduct->SetSelectedValue($ddlProduct->SubmittedValue);
}        
    if($btnSave->SubmittedValue == "Save")
    {
        $txtGameNumber->Args = "enabled = 'true'";
        $txtNewGameName->Args = "enabled = 'true'";
        $txtCardValue->Args = "enabled = 'true'";        
        
        $isGameNumExists = $tmgames->CheckIfGameNumberExists($txtGameNumber->SubmittedValue,$ddlProvider->SubmittedValue);
        $isExist = $tmproducts->DoesProductExists(mysql_escape_string(trim($txtNewGameName->SubmittedValue)),$ddlProvider->SubmittedValue);//echo print_r($isExist);exit();
	if($ddlProduct->SubmittedValue != ""){
	$txtNewGameName->Enabled = false;
        }
        if(count($isGameNumExists) > 0)
        {
                $message = "Game Number already exists. Please enter a unique Game Number.";
                $title = "ERROR!";
                
        }
        else if($ddlProduct->SubmittedValue == "")
        {
            if(count($isExist) > 0)
            {
                $message = "Game name is already used. Please enter a different game name.";
                $title = "ERROR!";
            }
            else
            {
                $product_success = "ok";
            }
        }
        else
        {
            $product_success = "ok";
        }
     
    }

    if($btnConfirm->SubmittedValue == "Save")
    {   
        $tmpcardprice = $tmgames->GetCardValueByName($ddlProduct->SubmittedValue);
        
        //if($ddlProduct->SelectedValue == "")
        $tmpname = $txtNewGameName->SubmittedValue;
        if(strlen($tmpname)> 0)   
        {
//        if (get_magic_quotes_gpc()) {
//        $newgame = stripslashes($txtNewGameName->SubmittedValue);
//        }
            //insert product / game name
            $product["ProductName"] = trim($txtNewGameName->SubmittedValue);
            $product["ProductDescription"] = trim($txtNewGameName->SubmittedValue);
            $product["ProviderID"] = $ddlProvider->SelectedValue;
            $insertproduct = $tmproducts->Insert($product);
            if($tmproducts->HasError)
            {
                $message = "Error occured: " . $tmproducts->getError();
                $title = "ERROR!";
                $product_success = "error";
            }
            else
            {
                $productid = $tmproducts->LastInsertID;
                $product_success = "ok";
            }
        }
        else
        {
            $productid = $ddlProduct->SelectedValue;
        }
        //insert game
        $game["ProviderID"] = $ddlProvider->SelectedValue;
        $game["ProductID"] = $productid;
        $game["GameNumber"] = trim($txtGameNumber->SubmittedValue);
        if($ddlProduct->SubmittedValue != ""){      
        $game["CardPrice"] = $tmpcardprice[0][CardPrice];
        }else{
        $game["CardPrice"] = $txtCardValue->SubmittedValue; 
        }
        $game["CurrentDollarRate"] = "1";
        $game["CurrencyID"] = "1";
        $game["DateCreated"] = "now_usec()";
        $game["CreatedByAID"] = $_SESSION['acctid'];
        $game["Status"] = "1";
        $insertgame = $tmgames->Insert($game);
        if($tmgames->HasError)
        {
            $message = "Error occured: " . $tmgames->getError();
            $title = "ERROR!";
        }
        else
        {
	    //insert in auditlog table
            $tmaudit["SessionID"] = $_SESSION['sid'];
            $tmaudit["AID"] = $_SESSION['acctid'];
            $tmaudit["TransDetails"] = 'Game ID: '.$tmgames->LastInsertID;
            $tmaudit["TransDateTime"] = 'now_usec()';
            $tmaudit["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
            $tmaudit["AuditTrailFunctionID"] = '24';    
            $tmauditlog->Insert($tmaudit);
            if ($tmauditlog->HasError)
            {
                $message = "Error occured: " . $tmproducts->getError();
                $title = "ERROR!";
            }
            else
            {
                $successmsg = "A new game has been successfully created.";
                $successtitle = "SUCCESS";
            } 
        }
    }
}
?>