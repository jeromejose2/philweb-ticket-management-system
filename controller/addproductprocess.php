<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
$pagesubmenuid = 23;
$javascripts[] = "../jscripts/validations.js";
$stylesheets[] = "../css/default.css";
$pagetitle = "";

App::LoadModuleClass("TicketManagementCM", "TMProducts");
App::LoadModuleClass("TicketManagementCM", "TMAuditLog");
App::LoadModuleClass("TicketManagementCM", "TMProvider");

App::LoadControl("TextBox");
App::LoadControl("ComboBox");
App::LoadControl("Button");

$fproc = new FormsProcessor();
$tmproductdtls = new TMProducts();
$tmauditlog = new TMAuditLog();
$tmprovider = new TMProvider();

$txtCode = new TextBox("txtCode", "txtCode", "Code: ");
$txtCode->ShowCaption = true;
$txtCode->Length = 20;

$txtName = new TextBox("txtName", "txtName", "Product: ");
$txtName->ShowCaption = true;
$txtName->Length = 30;

$txtDescription = new TextBox("txtDescription", "txtDescription", "Description: ");
$txtDescription->ShowCaption = true;
$txtDescription->Length = 50;

$arrproviders = $tmprovider->SelectAll();
$providerlist = new ArrayList();
$providerlist->AddArray($arrproviders);

$ddlProviders = new ComboBox("ddlProviders","ddlProviders","Provider: ");
$ddlProviders->ShowCaption = true;
$options = null;
$options[] = new ListItem("Select Provider","");
$ddlProviders->Items = $options;
$ddlProviders->DataSource = $providerlist;
$ddlProviders->DataSourceText = "Name";
$ddlProviders->DataSourceValue = "ProviderID";
$ddlProviders->DataBind();

$btnSave = new Button("btnSave", "btnSave", "Save");
$btnSave->Args="onclick='javascript: return checkaddproduct();'";
$btnSave->IsSubmit = true;

$btnCancel = new Button("btnCancel", "btnCancel", "Cancel");
$btnCancel->Args="onclick='javascript: return redirectToProductList();'";
//$btnCancel->IsSubmit = true;


$fproc->AddControl($txtCode);
$fproc->AddControl($txtName);
$fproc->AddControl($txtDescription);
$fproc->AddControl($btnSave);
$fproc->AddControl($btnCancel); 
$fproc->AddControl($ddlProviders);

$fproc->ProcessForms();

if ($fproc->IsPostBack)
{
    if ($btnSave->SubmittedValue == "Save")
    {
        //$tmproduct["ProductCode"] = $txtCode->Text;
        $tmproduct["ProductName"] = trim($txtName->Text);
        $tmproduct["ProductDescription"] = trim($txtName->Text);
        $tmproduct["ProviderID"] = $ddlProviders->SelectedValue;
        $arrProduct = $tmproductdtls->DoesProductExists($txtName->Text);
        if (count($arrProduct) > 0)
        {
            $errormsg = "Product name already exists.";
        }
        else
        {
            //add product
            $tmproductdtls->Insert($tmproduct);
        
            if ($tmproductdtls->HasError)
            {
                 $errormsg = $tmproductdtls->getError();
            }
            else
            {
                //insert in auditlog table
                $tmaudit["SessionID"] = $_SESSION['sid'];
                $tmaudit["AID"] = $_SESSION['acctid'];
                $tmaudit["TransDetails"] = 'Account ID: '.$_SESSION['acctid'];
                $tmaudit["TransDateTime"] = 'now_usec()';
                $tmaudit["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
                $tmaudit["AuditTrailFunctionID"] = '19';    
                $tmauditlog->Insert($tmaudit);
                if ($tmauditlog->HasError)
                {
                    $errormsg = $tmauditlog->getError();
                }
                else
                {
                    $successmsg = "New product successfully created.";
                } 
            }
        }        
    }    
}
?>