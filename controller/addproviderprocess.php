<?php
$pagesubmenuid = 22;
$javascripts[] = "jscripts/validations.js";
$stylesheets[] = "css/default.css";
$javascripts[] = "jscripts/jquery-1.5.2.min.js";
$pagetitle = "";

App::LoadModuleClass("TicketManagementCM", "TMProvider");
App::LoadModuleClass("TicketManagementCM", "TMAuditLog");

App::LoadControl("TextBox");
App::LoadControl("Button");

$fproc = new FormsProcessor();

$tmprovider = new TMProvider();   

$txtName = new TextBox("txtName", "txtName", "Name: ");
//$txtName->ShowCaption = true;
$txtName->Length = 30;
$txtName->Args = "onkeypress = 'javascript: return DisableSpecialCharacters(event);'";

$txtContactNumber = new TextBox("txtContactNumber", "txtContactNumber", "Contact Number: ");
//$txtContactNumber->ShowCaption = true;
$txtContactNumber->Length = 20;
$txtContactNumber->Args = "onkeypress='javascript: return isNumberKey(event)'";

$txtAddress = new TextBox("txtAddress", "txtAddress", "Head Office Address: ");
//$txtAddress->ShowCaption = true;
$txtAddress->Multiline = true;
$txtAddress->Style = "width: 300px;height: 50px;resize: none; vertical-align:top;";
$txtAddress->Args = "maxlength='100'";

$txtDescription = new TextBox("txtDescription", "txtDescription", "Description: ");
$txtDescription->ShowCaption = true;
$txtDescription->Length = 50;

$btnSave = new Button("btnSave", "btnSave", "Save");
$btnSave->Args="onclick='javascript: return checkaddprovider();'";
$btnSave->IsSubmit = true;

$btnCancel = new Button("btnCancel", "btnCancel", "Cancel");
$btnCancel->Args="onclick='javascript: return redirectToProviderList();'";
//$btnCancel->IsSubmit = true;

$btnConfirm = new Button("btnConfirm","btnConfirm","Okay");
$btnConfirm->IsSubmit = true;

$fproc->AddControl($txtName);
$fproc->AddControl($txtAddress);
$fproc->AddControl($txtContactNumber);
$fproc->AddControl($txtDescription);
$fproc->AddControl($btnSave);
$fproc->AddControl($btnCancel);
$fproc->AddControl($btnConfirm);

$fproc->ProcessForms();

if ($fproc->IsPostBack)
{
    if ($btnSave->SubmittedValue == "Save")
    {
        //check if provider already exists
        $providername = trim($txtName->Text);
        $providerdescription = trim($txtDescription->Text);
		$provideraddress = trim($txtAddress->SubmittedValue);
        $providercontactnum = trim($txtContactNumber->SubmittedValue);
        $arrProvider = $tmprovider->DoesProviderExists(mysql_escape_string(trim($providername)));//print_r($arrProvider);exit();
        if (count($arrProvider) > 0)
        {
            $errormsg = "Provider name already exists. Please enter a unique Provider Name.";
        }
        else
        {
			$confirm = "ok";
        }

    }   

	if($btnConfirm->SubmittedValue == "Okay")
    {
        $providername = trim($txtName->SubmittedValue);
        $providerdescription = trim($txtDescription->SubmittedValue);
        $provideraddress = trim($txtAddress->SubmittedValue);
        $providercontactnum = trim($txtContactNumber->SubmittedValue);
        //add provider info
        $tmprov["Name"] = $providername;//$txtName->Text;
        $tmprov["Description"] = $providername;
        $tmprov["Address"] = $provideraddress;
        $tmprov["ContactNumber"] = $providercontactnum;
        //$tmprov["Description"] = $providerdescription;//$txtDescription->Text;
        $tmprovider->Insert($tmprov);

        if ($tmprovider->HasError)
        {
            $errormsg =  $tmprovider->getError();
        }
        else
        {
            //insert in auditlog table
            $tmauditlog = new TMAuditLog();

            $tmaudit["SessionID"] = $_SESSION['sid'];
            $tmaudit["AID"] = $_SESSION['acctid'];
            $tmaudit["TransDetails"] = 'Account ID: '.$_SESSION['acctid'];
            $tmaudit["TransDateTime"] = 'now_usec()';
            $tmaudit["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
            $tmaudit["AuditTrailFunctionID"] = '8';
            $tmauditlog->Insert($tmaudit);
            if ($tmauditlog->HasError)
            {
                $errormsg =$tmauditlog->getError();
            }
            else
            {
                $successmsg ="A new provider has been successfully added.";
            }
        }
    } 
}
?>