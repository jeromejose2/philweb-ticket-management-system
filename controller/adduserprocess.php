<?php
/* 
 * Added By : Arlene R. Salazar
 * Added On : Aug 18, 2011
 * Purpose : Process for adding operator
 */
$pagesubmenuid = 1;
$stylesheets[] = "css/default.css";
$javascripts[] = "jscripts/jquery-1.5.2.min.js";

App::LoadModuleClass("TicketManagementCM", "TMAccountType");
App::LoadModuleClass("TicketManagementCM", "TMAccounts");
//App::LoadModuleClass("TicketManagementCM", "TMProjects");
App::LoadModuleClass("TicketManagementCM", "TMAuditLog");
App::LoadCore("PHPMailer.class.php");

App::LoadControl("TextBox");
App::LoadControl("ComboBox");
App::LoadControl("Button");

/*CLASS DECLARATIONS*/
$adduserform = new FormsProcessor();
$tmaccount = new TMAccounts();
$accounttype = new TMAccountType();
//$project = new TMProjects();
$auditlog = new TMAuditLog();
/*CLASS DECLARATIONS*/

$txtusername = new TextBox("txtusername","txtusername","*Username: ");
$txtusername->ShowCaption = true;
$txtusername->Length = 20; 
$txtusername->Args = "size='34'";
//$txtusername->Args = "onkeypress='javascript:return DisableSpecialCharacters(event)'";

$txtpassword = new TextBox("txtpassword","txtpassword","*Password: ");
$txtpassword->ShowCaption = true;
$txtpassword->Password = true;
$txtpassword->Length = 20;
$txtpassword->Args = "size='34'";
$txtpassword->Args = "onkeypress='javascript: return disableSpace(event);'";

$txtcpassword = new TextBox("txtcpassword","txtcpassword","*Confirm Password: ");
$txtcpassword->ShowCaption = true;
$txtcpassword->Password = true;
$txtcpassword->Length = 20;
$txtcpassword->Args = "size='28'";
$txtcpassword->Args = "onkeypress='javascript: return disableSpace(event);'";

$txtfname = new TextBox("txtfname","txtfname","*Full Name: ");
$txtfname->ShowCaption = true;
$txtfname->Length = 150;
$txtfname->Args = "onkeypress='javascript:return isAlphaKey(event)' size='89'";
//$txtfname->Args = "size='89'";

$txtmname = new TextBox("txtmname","txtmname","Middle Initial:");
$txtmname->ShowCaption = true;
$txtmname->Length = 2;
$txtmname->Args = "onkeypress='javascript:return isAlphaKey(event);' size='2'";

$txtlname = new TextBox("txtlname","txtlname","Last Name:");
$txtlname->ShowCaption = true;
$txtlname->Args = "onkeypress='javascript:return isAlphaKey(event)'";

$txtaddress = new TextBox("txtaddress","txtaddress","*Address: ");
$txtaddress->ShowCaption = true;
$txtaddress->Length = 150;
$txtaddress->Args = "size='90'";

$txtemail = new TextBox("txtemail","txtemail","Email Address: ");
$txtemail->ShowCaption = true;
$txtemail->Length = 100;
//$txtemail->Args = "size='40' onkeypress='javascript: return disableSpace(event);'";
$txtemail->Args = "size='40' onkeypress='javascript: return verifyEmail(event);'";

$txtcontactnum = new TextBox("txtcontactnum","txtcontactnum","*Contact Number: ");
$txtcontactnum->ShowCaption = true;
$txtcontactnum->Args = "onkeypress='javascript:return isNumberKey(event)' size='29'";
$txtcontactnum->Length = 20;

/*if($_SESSION['accid'] == 11)
{
    $where = " WHERE ID = 12";
    $accounttypes = $accounttype->SelectByWhere($where);
}
else
{
    $accounttypes = $accounttype->SelectAll();
}*/
$accounttypes = $accounttype->SelectAll();
$accttype_list = new ArrayList();
$accttype_list->AddArray($accounttypes);

$ddlaccttype = new ComboBox("ddlaccttype","ddlaccttype","Account Type: ");
$ddlaccttype->ShowCaption = true;
$ddlaccttype->DataSource = $accttype_list;
$ddlaccttype->DataSourceText = "Name";
$ddlaccttype->DataSourceValue = "AccountTypeID";
$ddlaccttype->DataBind();

/*$projects = $project->SelectAll();
$project_list = new ArrayList();
$project_list->AddArray($projects);
$ddlprjtid = new ComboBox("ddlprjtid","ddlprjtid","Project:");
$ddlprjtid->ShowCaption = true;
$ddlprjtid->DataSource = $project_list;
$ddlprjtid->DataSourceValue = "ID";
$ddlprjtid->DataSourceText = "ProjectName";
$ddlprjtid->DataBind();*/

$ddlstatus = new ComboBox("ddlstatus","ddlstatus","Status");
$ddlstatus->ShowCaption = true;
$litem = null;
$litem[] = new ListItem("Active", 1, true);
$litem[] = new ListItem("Inactive", 2);
$ddlstatus->Items = $litem;

$btnSubmit = new Button("btnSubmit","btnSubmit","Save");
$btnSubmit->IsSubmit = true;
$btnSubmit->Args = "onclick='javascript: return checkuserdata();'";

$btnCancel = new Button("btnCancel","btnCancel","Okay");
$btnCancel->Args = "onclick='javascript: return redirectToAcctList();'";

$btnCancelForm = new Button("btnCancel","btnCancel","Cancel");
$btnCancelForm->Args = "onclick='javascript: return redirectToAcctList();'";

$btnConfirm = new Button("btnConfirm","btnConfirm","Okay");
$btnConfirm->IsSubmit = true;

$btnResetPword = new Button("btnResetPword","btnResetPword","RESET PASSWORD");
$btnResetPword->IsSubmit = true;
$btnResetPword->IsSubmit = true;

$btnChangeStat = new Button("btnChangeStat","btnChangeStat","CHANGE STATUS");
$btnChangeStat->IsSubmit = true;
$btnResetPword->IsSubmit = true;

$adduserform->AddControl($txtusername);
$adduserform->AddControl($txtpassword);
$adduserform->AddControl($txtcpassword);
$adduserform->AddControl($txtfname);
$adduserform->AddControl($txtmname);
$adduserform->AddControl($txtlname);
$adduserform->AddControl($txtemail);
$adduserform->AddControl($ddlaccttype);
//$adduserform->AddControl($ddlprjtid);
$adduserform->AddControl($btnSubmit);
$adduserform->AddControl($txtcontactnum);
$adduserform->AddControl($btnConfirm);
$adduserform->AddControl($ddlstatus);
$adduserform->AddControl($btnChangeStat);
$adduserform->AddControl($btnResetPword);
$adduserform->AddControl($txtaddress);
$adduserform->AddControl($btnCancelForm);

$adduserform->ProcessForms();

if($adduserform->IsPostBack)
{
    if ($btnSubmit->SubmittedValue == "Save")
    {
		$emailcount = 0;
        if(trim($txtemail->SubmittedValue) == "")
        {
            $emailcount = 0;
        }
        else
        {
            $emailchecking = $tmaccount->CheckEmail(mysql_escape_string(trim($txtemail->SubmittedValue)));
            $emailcount = $emailchecking[0][0];
        }
        if($emailcount > 0)
        {
            $adduser_title = "ERROR!";
            $adduser_msg = "Email address already taken.";
        }
        $unamechecking = $tmaccount->CheckUsername(mysql_escape_string(trim($txtusername->SubmittedValue)));
        if($unamechecking[0][0] > 0)
        {
            $adduser_title = "ERROR!";
            $adduser_msg = "Username is already used, please enter different username.";
        }
        if(($emailcount == 0) && ($unamechecking[0][0] == 0))
        {
            $okadduser_msg = "ok";
        }
    }

    if($btnConfirm->SubmittedValue == "Okay")
    {
	$insertacct["Username"] = trim($txtusername->SubmittedValue); 
	$insertacct["Password"] = MD5(trim($txtpassword->SubmittedValue));
	$insertacct["AccountTypeID"] = $ddlaccttype->SelectedValue;
	$insertacct["Status"] = 1;
	$insertacct["DateCreated"] = "now_usec()";
    $insertacct["CreatedByAID"] = $_SESSION['acctid'];
	$adduser = $tmaccount->Insert($insertacct); 
        
//        if (get_magic_quotes_gpc()) {
//            $address = stripslashes($txtaddress->SubmittedValue);
//        }
	$adduserdtls = $tmaccount->UpdateAccountDetails($tmaccount->LastInsertID,mysql_escape_string(trim($txtfname->SubmittedValue)),mysql_escape_string(trim($txtaddress->SubmittedValue)),mysql_escape_string(trim($txtemail->SubmittedValue)),mysql_escape_string($txtcontactnum->SubmittedValue));
        if($tmaccount->HasError)
        {
            $confadduser_title = "ERROR!";
            $confadduser_msg = "Error has occured:" . $tmaccount->getError();
        }
        else
        {
            $auditdtls["SessionID"] = $_SESSION['sid'];
            $auditdtls["AID"] = $_SESSION['acctid'];
            $auditdtls["TransDetails"] = "Account ID: " . $tmaccount->LastInsertID;
            $auditdtls["TransDateTime"] = "now_usec()";
            $auditdtls["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
            $auditdtls["AuditTrailFunctionID"] = 14;
            $insertauditlog = $auditlog->Insert($auditdtls);
            if($auditlog->HasError)
            {
                $confadduser_title = "ERROR!";
                $confadduser_msg = "Error has occured:" . $tmaccount->getError();
            }
            $confadduser_title = "SUCCESSFUL!";
            $confadduser_msg = "The account has been successfully created. Thank you.";
            
            // add emailing
            $pm = new PHPMailer();
            $pm->AddAddress($txtemail->SubmittedValue, $txtfname->SubmittedValue);
            $pageURL = 'http';
            if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
            $pageURL .= "://";
            $folder = $_SERVER["REQUEST_URI"];
            $folder = substr($folder,0,strrpos($folder,'/') + 1);
            if ($_SERVER["SERVER_PORT"] != "80") 
            {
              $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$folder;
            } 
            else 
            {
              $pageURL .= $_SERVER["SERVER_NAME"].$folder;
            }
            
            $regards = App::getParam('country');

            $pm->Body = "Dear $txtfname->SubmittedValue, \n\nThis is to inform you that your account has been successfully created on this date ".date("m/d/Y")." and time ".date("H:i:s").". \n\nHere are your new Account Details:\n\n\t\t\t\t\tUsername: ".trim($txtusername->SubmittedValue).
            "\n\t\t\t\t\tPassword: ".trim($txtpassword->SubmittedValue)."\n\n".
            "For security purposes, please change this temporary password provided upon login at ".$pageURL.". \n\nIf you have problems logging in the username and password provided above, please report this to our toll free Customer Service hotline at +63 02 338 5599 or email us at customersupport@philweb.com.ph.
            \n\nRegards,\n\nCustomer Support\n$regards Ticket Management System";

            $pm->From = "no-reply@ticketmanagementsystem.com";
            $pm->FromName = "Ticket Management System";
            $pm->Host = "localhost";
            $pm->Subject = "Ticket Management System Account Created";
            $email_sent = $pm->Send();
            if($email_sent)
            {
                $errormsg = "Your password has been sent to your e-mail account. For security purpose, please change this system-generated password upon log in.";
                $errormsgtitle = "SUCCESSFUL!";
            }
            else
            {
                $errormsgfp = "An error occurred while sending the email to your email address";
                $errormsgtitle = "ERROR!";
            }
        }
    }
    if($btnResetPword->SubmittedValue == "RESET PASSWORD")
    {
        URL::Redirect("views/resetpassword.php");
    }
    if($btnChangeStat->SubmittedValue == "CHANGE STATUS")
    {
        URL::Redirect("views/changestatus.php");
    }
}
?>