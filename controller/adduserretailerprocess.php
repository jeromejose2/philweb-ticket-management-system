<?php
/* 
 * Added By : Arlene R. Salazar
 * Added On : Sept 05, 2011
 * Purpose : Process for adding user of product manager
 */
$pagesubmenuid = 28;
$stylesheets[] = "css/default.css";
$javascripts[] = "jscripts/jquery-1.5.2.min.js";

App::LoadModuleClass("TicketManagementCM", "TMAccountType");
App::LoadModuleClass("TicketManagementCM", "TMAccounts");
App::LoadModuleClass("TicketManagementCM", "TMAuditLog");

App::LoadControl("TextBox");
App::LoadControl("ComboBox");
App::LoadControl("Button");

/*CLASS DECLARATIONS*/
$adduserform = new FormsProcessor();
$tmaccount = new TMAccounts();
$accounttype = new TMAccountType();
//$project = new TMProjects();
$auditlog = new TMAuditLog();
/*CLASS DECLARATIONS*/

$txtusername = new TextBox("txtusername","txtusername","Username: ");
$txtusername->ShowCaption = true;
$txtusername->Length = 20; 
//$txtusername->Args = "onkeypress='javascript:return isAlphaKey(event)'";
$txtusername->Args = "size='34'";
//$txtusername->Args = "onkeypress='javascript:return DisableSpecialCharacters(event)'";

$txtpassword = new TextBox("txtpassword","txtpassword","Password: ");
$txtpassword->ShowCaption = true;
$txtpassword->Password = true;
$txtpassword->Length = 20;
$txtpassword->Args = "size='34'";
$txtpassword->Args = "onkeypress='javascript: return disableSpace(event);'";

$txtcpassword = new TextBox("txtcpassword","txtcpassword","Confirm Password: ");
$txtcpassword->ShowCaption = true;
$txtcpassword->Password = true;
$txtcpassword->Length = 20;
$txtcpassword->Args = "size='28'";
$txtcpassword->Args = "onkeypress='javascript: return disableSpace(event);'";

$txtfname = new TextBox("txtfname","txtfname","Full Name: ");
$txtfname->ShowCaption = true;
$txtfname->Length = 150;
//$txtfname->Args = "onkeypress='javascript:return isAlphaKey(event)' size='90'";
$txtfname->Args = "size='90'";

$txtmname = new TextBox("txtmname","txtmname","Middle Initial: ");
$txtmname->ShowCaption = true;
$txtmname->Length = 2;
$txtmname->Args = "onkeypress='javascript:return isAlphaKey(event);' size='2'";

$txtlname = new TextBox("txtlname","txtlname","Last Name: ");
$txtlname->ShowCaption = true;
$txtlname->Args = "onkeypress='javascript:return isAlphaKey(event)'";

$txtaddress = new TextBox("txtaddress","txtaddress","Address: ");
$txtaddress->ShowCaption = true;
$txtaddress->Length = 150;
$txtaddress->Args = "size='91'";

$txtemail = new TextBox("txtemail","txtemail","Email Address: ");
$txtemail->ShowCaption = true;
$txtemail->Length = 100;
$txtemail->Args = "size='40' onkeypress='javascript: return disableSpace(event);'";

$txtcontactnum = new TextBox("txtcontactnum","txtcontactnum","Contact Number: ");
$txtcontactnum->ShowCaption = true;
$txtcontactnum->Args = "onkeypress='javascript:return isNumberKey(event)' size='29'";
$txtcontactnum->Length = 20;

$where = " WHERE AccountTypeID IN (5,6)";
$accounttypes = $accounttype->SelectByWhere($where);
$accttype_list = new ArrayList();
$accttype_list->AddArray($accounttypes);

$ddlaccttype = new ComboBox("ddlaccttype","ddlaccttype","Account Type: ");
$ddlaccttype->ShowCaption = true;
$ddlaccttype->DataSource = $accttype_list;
$ddlaccttype->DataSourceText = "Name";
$ddlaccttype->DataSourceValue = "AccountTypeID";
$ddlaccttype->DataBind();

/*$projects = $project->SelectAll();
$project_list = new ArrayList();
$project_list->AddArray($projects);
$ddlprjtid = new ComboBox("ddlprjtid","ddlprjtid","Project:");
$ddlprjtid->ShowCaption = true;
$ddlprjtid->DataSource = $project_list;
$ddlprjtid->DataSourceValue = "ID";
$ddlprjtid->DataSourceText = "ProjectName";
$ddlprjtid->DataBind();*/

$ddlstatus = new ComboBox("ddlstatus","ddlstatus","Status");
$ddlstatus->ShowCaption = true;
$litem = null;
$litem[] = new ListItem("Active", 1, true);
$litem[] = new ListItem("Inactive", 2);
$ddlstatus->Items = $litem;

$btnSubmit = new Button("btnSubmit","btnSubmit","Save");
$btnSubmit->IsSubmit = true;
$btnSubmit->Args = "onclick='javascript: return checkuserdata();'";

$btnCancel = new Button("btnCancel","btnCancel","Cancel");
$btnCancel->Args = "onclick='javascript: return redirectToAcctListRetailer();'";

$btnCancel = new Button("btnCancel","btnCancel","Cancel");
$btnCancel->Args = "onclick='javascript: return redirectToAcctListRetailer();'";

$btnConfirm = new Button("btnConfirm","btnConfirm","Okay");
$btnConfirm->IsSubmit = true;

$btnResetPword = new Button("btnResetPword","btnResetPword","RESET PASSWORD");
$btnResetPword->IsSubmit = true;

$btnChangeStat = new Button("btnChangeStat","btnChangeStat","CHANGE STATUS");
$btnChangeStat->IsSubmit = true;

$adduserform->AddControl($txtusername);
$adduserform->AddControl($txtpassword);
$adduserform->AddControl($txtcpassword);
$adduserform->AddControl($txtfname);
$adduserform->AddControl($txtmname);
$adduserform->AddControl($txtlname);
$adduserform->AddControl($txtemail);
$adduserform->AddControl($ddlaccttype);
//$adduserform->AddControl($ddlprjtid);
$adduserform->AddControl($btnSubmit);
$adduserform->AddControl($txtcontactnum);
$adduserform->AddControl($btnConfirm);
$adduserform->AddControl($ddlstatus);
$adduserform->AddControl($btnChangeStat);
$adduserform->AddControl($btnResetPword);
$adduserform->AddControl($txtaddress);

$adduserform->ProcessForms();

if($adduserform->IsPostBack)
{
    if ($btnSubmit->SubmittedValue == "Save")
    {
		$emailcount = 0;
        if(trim($txtemail->SubmittedValue) == "")
        {
            $emailcount = 0;
        }
        else
        {
            $emailchecking = $tmaccount->CheckEmail(mysql_escape_string(trim($txtemail->SubmittedValue)));
            $emailcount = $emailchecking[0][0];
        }
        if($emailcount > 0)
        {
            $adduser_title = "ERROR!";
            $adduser_msg = "Email address already taken.";
        }
        $unamechecking = $tmaccount->CheckUsername(mysql_escape_string(trim($txtusername->SubmittedValue)));
        if($unamechecking[0][0] > 0)
        {
            $adduser_title = "ERROR!";
            $adduser_msg = "Username is already used, please enter different username.";
        }
        if(($emailcount == 0) && ($unamechecking[0][0] == 0))
        {
            $okadduser_msg = "ok";
        }
    }

    if($btnConfirm->SubmittedValue == "Okay")
    {
		$insertacct["Username"] = trim($txtusername->SubmittedValue);
		$insertacct["Password"] = MD5(trim($txtpassword->SubmittedValue));
		$insertacct["AccountTypeID"] = $ddlaccttype->SelectedValue;
		$insertacct["Status"] = 1;
		$insertacct["DateCreated"] = "now_usec()";
		$insertacct["CreatedByAID"] = $_SESSION['acctid'];
		$adduser = $tmaccount->Insert($insertacct);
		//$adduser = $tmaccount->InsertAccount($txtusername->SubmittedValue,MD5($txtpassword->SubmittedValue),$ddlaccttype->SelectedValue,1);
		$adduserdtls = $tmaccount->UpdateAccountDetails($tmaccount->LastInsertID,mysql_escape_string(trim($txtfname->SubmittedValue)),mysql_escape_string(trim($txtaddress->SubmittedValue)),mysql_escape_string(trim($txtemail->SubmittedValue)),mysql_escape_string(trim($txtcontactnum->SubmittedValue)));
        if($tmaccount->HasError)
        {
            $confadduser_title = "ERROR!";
            $confadduser_msg = "Error has occured:" . $tmaccount->getError();
        }
        else
        {
            $auditdtls["SessionID"] = $_SESSION['sid'];
            $auditdtls["AID"] = $_SESSION['acctid'];
            $auditdtls["TransDetails"] = "Account ID: " . $tmaccount->LastInsertID;
            $auditdtls["TransDateTime"] = "now_usec()";
            $auditdtls["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
            $auditdtls["AuditTrailFunctionID"] = 14;
            $insertauditlog = $auditlog->Insert($auditdtls);
            if($auditlog->HasError)
            {
                $confadduser_title = "ERROR!";
                $confadduser_msg = "Error has occured:" . $tmaccount->getError();
            }
            $confadduser_title = "SUCCESSFULL!";
            $confadduser_msg = "The account has been successfully created. Thank you.";
        }
    }
    if($btnResetPword->SubmittedValue == "RESET PASSWORD")
    {
        URL::Redirect("views/resetpassword.php");
    }
    if($btnChangeStat->SubmittedValue == "CHANGE STATUS")
    {
        URL::Redirect("views/changestatus.php");
    }
}
?>