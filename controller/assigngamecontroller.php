<?php
/*
 * Author: Noel Antonio
 * Date Created: 2011-08-26
 * Company: Philweb Corporation
 */
$pagesubmenuid = 18;
require_once ("../init.inc.php");
App::LoadModuleClass("TicketManagementCM", "TMGameManagement");
App::LoadModuleClass("TicketManagementCM", "TMProducts");
App::LoadModuleClass("TicketManagementCM", "TMProvider");
App::LoadModuleClass("TicketManagementCM", "TMAuditLog");
App::LoadControl("TextBox");
App::LoadControl("Button");
App::LoadControl("ComboBox");
$fproc = new FormsProcessor();

$tmassigngame = new TMGameManagement();
$tmproducts = new TMProducts();
$tmprovider = new TMProvider();
$tmauditlog = new TMAuditLog();

$cboProvider = new ComboBox("cboProvider", "cboProvider", "");
$cboProvider->Args = "onchange='javascript: SelectProduct();'";
$cboProvider->ShowCaption = true;
$options = null;
$options[] = new ListItem("Please select a provider", "0");
$cboProvider->Items = $options;
$arrprov = $tmprovider->SelectOrderBy("Name", "ASC");
$provlist = new ArrayList();
$provlist->AddArray($arrprov);
$cboProvider->DataSource = $provlist;
$cboProvider->DataSourceText = "Name";
$cboProvider->DataSourceValue = "ProviderID";
$cboProvider->DataBind();

$cboProduct = new ComboBox("cboProduct", "cboProduct", "");
$cboProduct->ShowCaption = true;
$options = null;
$options[] = new ListItem("", "0");
$cboProduct->Items = $options;

$txtGameID = new TextBox("txtGameID", "txtGameID", "");
$txtGameID->Length = 3;
$txtGameID->ShowCaption = true;
$txtGameID->Args = "onkeypress=\"javascript: return isNumber(event);\"";

$btnConfirm = new Button("btnConfirm", "btnConfirm", "OKAY");
$btnConfirm->IsSubmit = true;

$btnSave = new Button("btnSave", "btnSave", "Save");
$btnSave->IsSubmit = true;
$btnSave->Args = "onclick='javascript: return chkInputs()';";
$btnOkay = new Button("btnOkay", "btnOkay", "OKAY");
$btnOkay->Args = "onclick='javascript: window.location = \"gamemgmt.php\"';";
$btnCancel = new Button("btnCancel", "btnCancel", "Cancel");
$btnCancel->IsSubmit = true;

$fproc->AddControl($cboProduct);
$fproc->AddControl($cboProvider);
$fproc->AddControl($txtGameID);
$fproc->AddControl($btnSave);
$fproc->AddControl($btnCancel);
$fproc->AddControl($btnOkay);
$fproc->AddControl($btnConfirm);
$fproc->ShowCaptions = true;
$fproc->ProcessForms();

if ($fproc->IsPostBack)
{
    if ($btnConfirm->SubmittedValue == "OKAY")
    {
        //insert into games table
        $tmgame["GameNumber"] = $txtGameID->Text;
        $tmgame["ProductID"] = $cboProduct->SubmittedValue;
        $tmgame["ProviderID"] = $cboProvider->SubmittedValue;
        $tmgame["CreatedByAID"] = $_SESSION['acctid'];
        $tmgame["DateCreated"] = 'now_usec()';
        $tmgame["Status"] = 1;
        $tmassigngame->Insert($tmgame); 
        
        //insert in auditlog table
        $tmaudit["SessionID"] = $_SESSION['sid'];
        $tmaudit["AID"] = $_SESSION['acctid'];
        $tmaudit["TransDetails"] = 'Game ID: '.$txtGameID->Text;
        $tmaudit["TransDateTime"] = 'now_usec()';
        $tmaudit["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
        $tmaudit["AuditTrailFunctionID"] = '11'; 
        $tmauditlog->Insert($tmaudit);
        
        if ($tmauditlog->HasError){
            $errormsg = $tmauditlog->getError();
            $errormsgtitle = "ERROR!";
        }
        
        if ($tmassigngame->HasError){ 
            App::Pr($tmassigngame->getError());
        }
        else { $added = "RECORD ADDED";}
    }
    if ($btnSave->SubmittedValue == "Save")
    {
        if ($txtGameID->Text != ""){ $confirm = "CONFIRMATION";}
    }
    
    if ($cboProvider->SubmittedValue != '')
    {
        $cboProduct->ClearItems();
        $arrproducts = $tmproducts->SelectProduct($cboProvider->SubmittedValue);
        $productlist = new ArrayList();
        $productlist->AddArray($arrproducts);
        $cboProduct->DataSource = $productlist;
        $cboProduct->DataSourceText = "ProductName";
        $cboProduct->DataSourceValue = "ProductID";
        $cboProduct->DataBind();
    }
    
    if ($btnCancel->SubmittedValue == "Cancel"){ URL::Redirect("gamemgmt.php"); }
}
?>
