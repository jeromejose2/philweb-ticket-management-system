<?php
/*
 * @author
 * Purpose   : controller for audittrail
 */
$pagesubmenuid = 17;
//$javascripts[] = "../jscripts/validations.js";
$stylesheets[] = "css/default.css";
$pagetitle = "Audit Trail History";

App::LoadModuleClass("TicketManagementCM", "TMAuditLog");
App::LoadModuleClass("TicketManagementCM", "TMAuditFunctions");
//App::LoadModuleClass("TicketManagementCM", "Pdf");
//App::LoadLibrary("fpdf/fpdf.php");
// Load Controls
App::LoadControl("DataTable");
App::LoadControl("Button");
App::LoadControl("TextBox");
App::LoadControl("ComboBox");
App::LoadControl("Hidden");
App::LoadControl("PagingControl2");
//App::LoadControl("Pdf");
$fproc = new FormsProcessor();

$tmal = new TMAuditLog();
$tmalfunctions = new TMAuditFunctions();

$ddlFunction = new ComboBox("ddlFunction","ddlFunction","Audit Trail Function: ");
$ddlFunction->ShowCaption = true;

$txtFromDate = new TextBox("txtFromDate", "txtFromDate", "Date ");
$txtFromDate->Text = date('Y-m-d');
$txtFromDate->ShowCaption = true;
$txtFromDate->ReadOnly = true;
$txtFromDate->Length = 10;
$txtFromDate->Attributes = "width='10'";
$txtFromDate->Style = "text-align: center;";

$btnSubmit = new Button("btnSubmit", "btnSubmit", "Submit");
$btnSubmit->Args="onclick='javascript: return checkaudittrail();'";
$btnSubmit->IsSubmit = true;

$arralfunctions = $tmalfunctions->SelectAllWithOrder();
$alftnlist = new ArrayList();
$alftnlist->AddArray($arralfunctions);
$ddlFunction->ClearItems();
$litem = null;
$litem[] = new ListItem("ALL", "0", true);
$ddlFunction->Items = $litem;
$ddlFunction->DataSource = $alftnlist;
$ddlFunction->DataSourceText = "AuditFunctionName";
$ddlFunction->DataSourceValue = "AuditTrailFunctionID";
$ddlFunction->DataBind();

$itemsperpage = 50;
$pgcon = new PagingControl2($itemsperpage, 1);
$pgcon->URL = "javascript:ChangePage(%currentpage);";
$pgcon->ShowMoveToFirstPage = true;
$pgcon->ShowMoveToLastPage = true;

$btnExport = new Button("btnExport","btnExport","Export to PDF");
$btnExport->IsSubmit = true;

$btnExportCSV = new Button("btnExportCSV","btnExportCSV","Export to CSV");
$btnExportCSV->IsSubmit = true;

$fproc->AddControl($ddlFunction);
$fproc->AddControl($txtFromDate);
$fproc->AddControl($btnSubmit);
$fproc->AddControl($btnExport);
$fproc->AddControl($btnExportCSV);
$fproc->ProcessForms();

if ($fproc->IsPostBack)
{   
    if ($btnSubmit->SubmittedValue == "Submit") 
    {
        $whatdate = $txtFromDate->Text;
        $functionid = $ddlFunction->SubmittedValue;
        $_SESSION['whatdate'] = $whatdate;
        $_SESSION['functionid'] = $functionid;

        $transactioncount = $tmal->SelectCount($whatdate,$functionid);
        $transactioncount = $transactioncount[0][0];

        //start pagination//      
        if ($btnSubmit->SubmittedValue == "Submit") { $pgcon->SelectedPage = 1; }

        $pgcon->Initialize($itemsperpage, $transactioncount);

        $pgcon->PageGroup = 5;
        $pgTransactionHistory = $pgcon->PreRender();
        $pagefrom = $pgcon->SelectedItemFrom - 1;
        $dateTime = new DateTime($whatdate);
        $whatdate = $dateTime->format('Y-m-d');

        $arrtmal = $tmal->SelectWithLimit($whatdate, $pagefrom, $itemsperpage, $functionid);
        $tbldata_list = new ArrayList();
        $tbldata_list->AddArray($arrtmal);
    }
    /*
    * Added by: Arlene R. Salazar
    * Date: Feb 29, 2012
    */
    if($btnExportCSV->SubmittedValue == "Export to CSV")
    {	
        $data = "";
        $transactioncount = $tmal->SelectTrails($_SESSION['whatdate'],$_SESSION['functionid']);
        $tbldata_list = new ArrayList();
        $tbldata_list->AddArray($transactioncount);

        $fp = fopen("../csv/Audit_Trail_Logs.csv","w");
        if($fp)
        {
            $header = "Date,Username,IP Address,Function\r\n";
            fwrite($fp,$header);
            if(count($tbldata_list) > 0)
            {
                for($i = 0 ; $i < count($tbldata_list) ; $i++)
                {
                    $dateTime = new DateTime($tbldata_list[$i]['TransDateTime']);
                    $transdate = $dateTime->format('Y-m-d h:i:s A');
                        $data .= $transdate . "," . $tbldata_list[$i]['UserName'] . "," . $tbldata_list[$i]['RemoteIP'] . "," . $tbldata_list[$i]['AuditFunctionName'] . "\r\n";
                }
            }
            else
            {	
                $data .= "No Records Found;\r\n";
            }
            fwrite($fp,$data);
            header('Content-type: text/csv');
            header("Content-Disposition: attachment; filename=".'Audit_Trail_Logs.csv');
            header('Pragma: public');
            readfile('../csv/Audit_Trail_Logs.csv');
            exit;
        }
        else
        {
            echo "<script>alert('Cannot create file.')</script>";
        }
    }
    /*Added by: Arlene R. Salazar*/
        
    /*
     * Added by: Sheryl S. Basbas
     * Date: Feb 6, 2012
     */
    if($btnExport->SubmittedValue == "Export to PDF")
    {
        $transactioncount = $tmal->SelectTrails($whatdate,$functionid);
        $tbldata_list = new ArrayList();
        $tbldata_list->AddArray($transactioncount);
        
        $pdf = new PDF(P,mm,Legal);
        $pdf->AliasNbPages();        
        $title = 'Audit Trail Logs';
        $pdf->SetTitle($title);
        $lengths = array(50, 50, 50, 50);
        // Column headings
        $header = array('Date', 'Username', 'IP Address', 'Function');
        $cols = array('TransDateTime', 'UserName', 'RemoteIP', 'AuditFunctionName');
             
        // Data loading
        $data = $pdf->LoadData($tbldata_list);      
        $pdf->SetFont('Arial','',9);   
        $pdf->AddPage();
        $pdf->FancyTable($header,$data,$lengths,$cols);
        $pdf->Output('Audit Trail Logs','D');
        
    }
    /*Added by: Sheryl S. Basbas */
}
?>