<?php
$pagesubmenuid = 17;
//$javascripts[] = "../jscripts/validations.js";
$stylesheets[] = "../css/default.css";
$pagetitle = "Audit Trail History";

App::LoadModuleClass("TicketManagementCM", "TMAuditLog");
App::LoadModuleClass("TicketManagementCM", "TMAuditFunctions");

// Load Controls
App::LoadControl("DataTable");
App::LoadControl("Button");
App::LoadControl("TextBox");
App::LoadControl("ComboBox");
App::LoadControl("Hidden");
App::LoadControl("PagingControl2");
$fproc = new FormsProcessor();

$tmal = new TMAuditLog();
$tmalfunctions = new TMAuditFunctions();

$ddlFunction = new ComboBox("ddlFunction","ddlFunction","Audit Trail Function: ");
$ddlFunction->ShowCaption = true;

$txtFromDate = new TextBox("txtFromDate", "txtFromDate", "Date ");
$txtFromDate->Text = date('m/d/Y');
$txtFromDate->ShowCaption = true;
$txtFromDate->ReadOnly = true;
$txtFromDate->Length = 10;
$txtFromDate->Attributes = "width='10'";
$txtFromDate->Style = "text-align: center;";

$btnSubmit = new Button("btnSubmit", "btnSubmit", "Submit");
$btnSubmit->Args="onclick='javascript: return checkaudittrail();'";
$btnSubmit->IsSubmit = true;

$arralfunctions = $tmalfunctions->SelectAllWithOrder();
$alftnlist = new ArrayList();
$alftnlist->AddArray($arralfunctions);
$ddlFunction->ClearItems();
$litem = null;
$litem[] = new ListItem("ALL", "0", true);
$ddlFunction->Items = $litem;
$ddlFunction->DataSource = $alftnlist;
$ddlFunction->DataSourceText = "AuditFunctionName";
$ddlFunction->DataSourceValue = "AuditTrailFunctionID";
$ddlFunction->DataBind();

$itemsperpage = 50;
$pgcon = new PagingControl2($itemsperpage, 1);
$pgcon->URL = "javascript:ChangePage(%currentpage);";
$pgcon->ShowMoveToFirstPage = true;
$pgcon->ShowMoveToLastPage = true;

$fproc->AddControl($ddlFunction);
$fproc->AddControl($txtFromDate);
$fproc->AddControl($btnSubmit);

$fproc->ProcessForms();

if ($fproc->IsPostBack)
{    
    $whatdate = $txtFromDate->Text;
    $functionid = $ddlFunction->SubmittedValue;
      
    $transactioncount = $tmal->SelectCount($whatdate,$functionid);
    $transactioncount = $transactioncount[0][0];
    
    //start pagination//      
    if ($btnSubmit->SubmittedValue == "Submit")
    {
        $pgcon->SelectedPage = 1;
    }
    
    $pgcon->Initialize($itemsperpage, $transactioncount);
    
    $pgcon->PageGroup = 5;
    $pgTransactionHistory = $pgcon->PreRender();
    $pagefrom = $pgcon->SelectedItemFrom - 1;
    $arrtmal = $tmal->SelectWithLimit($whatdate, $pagefrom, $itemsperpage, $functionid);
    $tbldata_list = new ArrayList();
    $tbldata_list->AddArray($arrtmal);
}
?>
