<?php
/*
 * Created By: Noel Antonio
 * Date: March 8, 2012
 * Purpose: Record history of batch file uploads.
 */


$stylesheets[] = "css/default.css";
$stylesheets[] = "css/datepicker.css";
$javascripts[] = "jscripts/validations.js";
$javascripts[] = "jscripts/datetimepicker.js";
$javascripts[] = "jscripts/jquery-1.5.2.min.js";
$pagesubmenuid = 46;

App::LoadCore("CSV.class.php");
App::LoadCore("File.class.php");
App::LoadModuleClass("TicketManagementCM", "TMBatchFileValidationSession");
App::LoadModuleClass("TicketManagementCM", "TMBatchFileValidationTickets");
App::LoadModuleClass("TicketManagementCM", "TMBatchValidationSession");
App::LoadModuleClass("TicketManagementCM", "TMBatchValidationTickets");
App::LoadModuleClass("TicketManagementCM", "TMTicketValidation");
App::LoadModuleClass("TicketManagementCM", "TMBatchTransactionLog");
App::LoadModuleClass("TicketManagementCM", "TMDecks");
App::LoadModuleClass("TicketManagementCM", "TMTickets");
App::LoadModuleClass("TicketManagementCM", "TMAccounts");
App::LoadModuleClass("TicketManagementCM", "TMProducts");
App::LoadModuleClass("TicketManagementCM", "TMGameManagement");
App::LoadModuleClass("TicketManagementCM", "TMAuditLog");

App::LoadControl("Button");
App::LoadControl("Hidden");
App::LoadControl("ComboBox");
App::LoadControl("DataTable");
App::LoadControl("TextBox");
App::LoadControl("PagingControl2");

$tmbatchvalidationsession = new TMBatchValidationSession();
$tmbatchvalidationtickets = new TMBatchValidationTickets();
$tmbatchfilevalidationsession = new TMBatchFileValidationSession();
$tmbatchfilevalidationtickets = new TMBatchFileValidationTickets();
$tmticketvalidation = new TMTicketValidation();
$tmbatchtranslog = new TMBatchTransactionLog();
$tmdecks = new TMDecks();
$tmtickets = new TMTickets();
$tmaccount = new TMAccounts();
$tmproducts = new TMProducts();
$tmgamemgt = new TMGameManagement();
$tmauditlog = new TMAuditLog();

$frmXmlUpload = new FormsProcessor();

$aid = $_SESSION['acctid'];
$arrSessionData = '';

$gamenames = $tmproducts->SelectAllGameName();
$gamename_list = new ArrayList();
$gamename_list->AddArray($gamenames);
$ddlgamename = new ComboBox("ddlgamename","ddlgamename","Game Name: ");
$ddlgamename->Args = "onchange = 'javascript: get_gamenumber();'";
$ddlgamename->ShowCaption = true;
$gamenameopt = null;
$gamenameopt[] = new ListItem("ALL", "0", true);
$ddlgamename->Items = $gamenameopt;
$ddlgamename->DataSource = $gamename_list;
$ddlgamename->DataSourceText = "ProductName";
$ddlgamename->DataSourceValue = "ProductID";
$ddlgamename->DataBind();

$games = $tmgamemgt->SelectByGameType();
$games_list = new ArrayList();
$games_list->AddArray($games);
$ddlgametype = new ComboBox("ddlgametype", "ddlgametype", "Game Number: ");
$ddlgametype->Args = "onchange='javascript: get_batchID(); get_gamename();'";
$litemgmetype = null;
$litemgmetype[] = new ListItem("ALL", "0", true);
$ddlgametype->Items = $litemgmetype;
$ddlgametype->ShowCaption = true;
$ddlgametype->DataSource = $games_list;
$ddlgametype->DataSourceText = "GameNumber";
$ddlgametype->DataSourceValue = "GameID";
$ddlgametype->DataBind();

$ddlgamebatch = new ComboBox("ddlgamebatch","ddlgamebatch","Game Batch:");
$ddlgamebatch->ShowCaption = true;
$gamebatch = null;
$gamebatch[] = new ListItem("ALL","0", true);
$ddlgamebatch->Items  = $gamebatch;

$flag = new Hidden("flag","flag","flag");
$xmltype = new Hidden("xmltype","xmltype","xmltype");

$btnExport = new Button("export","export","Export to PDF");
$btnExport->IsSubmit = true;

$btnExportCSV = new Button("btnExportCSV","btnExportCSV","Export to CSV");
$btnExportCSV->IsSubmit = true;

$ddlUsername = new ComboBox("ddlUsername", "ddlUsername", "Distributor: ");
$ddlUsername->ShowCaption =TRUE;
$ddlUsername->TabIndex = 4;
$arraccounts = $tmaccount->SelectForValidationModule($aid);
$accountlist = new ArrayList();
$accountlist->AddArray($arraccounts);
$ddlUsername->ClearItems();
$litem = null;
$litem[] = new ListItem("ALL", "0", true);
$ddlUsername->Items = $litem;
$ddlUsername->DataSource = $accountlist;
$ddlUsername->DataSourceText = "UserName";
$ddlUsername->DataSourceValue = "AID";
$ddlUsername->DataBind();

$txtDateFr = new TextBox("txtDateFr", "txtDateFr", "From : ");
$txtDateFr->ReadOnly = true;
$txtDateFr->Style = "text-align: center";
$txtDateFr->Args = "size='9'";
$txtDateFr->Text = date('Y-m-d');

$txtDateTo = new TextBox("txtDateTo", "txtDateTo", "To : ");
$txtDateTo->ReadOnly = true;
$txtDateTo->Style = "text-align: center";
$txtDateTo->Args = "size='9'";
$txtDateTo->Text = date('Y-m-d');

$btnSubmit = new Button("btnSubmit","btnSubmit","Submit");
$btnSubmit->IsSubmit = true;
$btnSubmit->Args = "onclick='javascript: return validate();'";

$itemsperpage = 50;
$pgcon = new PagingControl2($itemsperpage, 1);
$pgcon->URL = "javascript:ChangePage(%currentpage);";
$pgcon->ShowMoveToFirstPage = true;
$pgcon->ShowMoveToLastPage = true;
$pgcon->PageGroup = 5;

$hiddenflag = new Hidden("hiddenflag","hiddenflag","Hidden Flag");
$hidaid = new Hidden("hidaid", "hidaid", "Hidden AID");

$frmXmlUpload->AddControl($ddlUsername);
$frmXmlUpload->AddControl($ddlgamename);
$frmXmlUpload->AddControl($ddlgametype);
$frmXmlUpload->AddControl($txtDateFr);
$frmXmlUpload->AddControl($txtDateTo);
$frmXmlUpload->AddControl($btnSubmit);
$frmXmlUpload->AddControl($btnExportCSV);
$frmXmlUpload->AddControl($hiddenflag);
$frmXmlUpload->AddControl($xmltype);
$frmXmlUpload->AddControl($flag);
$frmXmlUpload->AddControl($hidaid);

$frmXmlUpload->ProcessForms();

if ($frmXmlUpload->IsPostBack)
{   
    if ($flag->SubmittedValue == 1)
    {
        $ddlgametype->ClearItems();
        $prodid1 = $ddlgamename->SubmittedValue;
        $gamenumbers = $tmproducts->SelectGameNumPerGameName($prodid1);
        $gamenumbers_list1 = new ArrayList();
        $gamenumbers_list1->AddArray($gamenumbers);       
        $litemgmetype1 = null;
        $litemgmetype1[] = new ListItem("ALL", "0", false);
        $ddlgametype->Items = $litemgmetype1;
        $ddlgametype->DataSource = $gamenumbers_list1;
        $ddlgametype->DataSourceText = "GameNumber";
        $ddlgametype->DataSourceValue = "GameID";
        $ddlgametype->DataBind();
        $ddlgametype->SetSelectedValue($ddlgametype->SubmittedValue);
    }
    if ($flag->SubmittedValue == 2)
    {
        $ddlgamename->Args = "onchange='javascript:'";          
        $ddlgamename->ClearItems();
        $gamenum1 = $ddlgametype->SubmittedValue;
        $gamenames = $tmproducts->SelectGameNamePerGameNum($gamenum1);
        $gamename_list1 = new ArrayList();
        $gamename_list1->AddArray($gamenames);       
        $litemgmetype1 = null;
        $litemgmetype1[] = new ListItem("ALL", "0", false);
        $ddlgamename->Items = $litemgmetype1;
        $ddlgamename->DataSource = $gamename_list1;
        $ddlgamename->DataSourceText = "ProductName";
        $ddlgamename->DataSourceValue = "ProductID";
        $ddlgamename->DataBind();
        $ddlgamename->SetSelectedValue($ddlgametype->SubmittedValue);
    }
 
    $ddlgametype->SetSelectedValue($ddlgametype->SubmittedValue);
    $ddlgamename->SetSelectedValue($ddlgamename->SubmittedValue);
    
    if ($btnSubmit->SubmittedValue == "Submit" || $frmXmlUpload->GetPostVar("pgSelectedPage") != '')
    {
        $user = $ddlUsername->SubmittedValue;
        $gameid = $ddlgametype->SubmittedValue;
        $datefr = $txtDateFr->SubmittedValue;
        $dateto = $txtDateTo->SubmittedValue;
        
        $array = $tmbatchfilevalidationtickets->getBatchFileUploadHistory($ddlgamename->SubmittedValue,$user, $gameid, $datefr, $dateto);
        $reccount = count($array);
        if ($btnSubmit->SubmittedValue == "Submit")
        {
            $pgcon->SelectedPage = 1;
        }
        $pgcon->Initialize($itemsperpage, $reccount);
        $pgTransactionHistory = $pgcon->PreRender();
        
        $array2 = $tmbatchfilevalidationtickets->getBatchFileUploadHistoryWithLimit($ddlgamename->SubmittedValue,$user, $gameid, $datefr, $dateto, $pgcon->SelectedItemFrom-1, $itemsperpage);
        $batchhistory = new ArrayList();
        $batchhistory->AddArray($array2);
    }
    
    if ($btnExportCSV->SubmittedValue == "Export to CSV")
    {
        $user = $ddlUsername->SubmittedValue;
        $gameid = $ddlgametype->SubmittedValue;
        $datefr = $txtDateFr->SubmittedValue;
        $dateto = $txtDateTo->SubmittedValue;
        $array = $tmbatchfilevalidationtickets->getBatchFileUploadHistory($ddlgamename->SubmittedValue,$user, $gameid, $datefr, $dateto);
        
        $fp = fopen("../csv/Batch_File_Validation_History.csv","w");
        if ($fp)
        {
            $header = "Date,Game Number,Game Name,Retailer, File Name, Session ID"."\r\n";
            fwrite($fp, $header);
            for ($i=0;$i<count($array);$i++)
            {
                $dateTime = new DateTime($array[$i]["TransactionDate"]);
                $data .= $dateTime->format('Y-m-d h:i:s A').",".$array[$i]["GameNumber"].",".$array[$i]["ProductName"].",".$array[$i]["UserName"].",".$array[$i]["FileName"].",".$array[$i]["BatchFileValidationSessionID"]."\r\n";
            }
            fwrite($fp,$data);
            fclose($fp);
            header('Content-type: text/csv');
            header("Content-Disposition: attachment; filename=".'Batch_File_Validation_History.csv');
            header('Pragma: public');
            readfile('../csv/Batch_File_Validation_History.csv');
            exit;
        } else {
            echo "<script>alert('ok');</script>";
        }
    }
}
?>