<?php
ignore_user_abort(true);                                                        //Ignore user aborts and force the script running until the last part
set_time_limit(0);                                                              //Limits the maximum execution time , if set to 0 no maximum time is imposed. Safe mode should be turned off.

$stylesheets[] = "css/default.css";
$javascripts[] = "jscripts/validations.js";
$pagesubmenuid = 44;

App::LoadCore("CSV.class.php");
App::LoadCore("File.class.php");
App::LoadModuleClass("TicketManagementCM", "TMBatchFileValidationSession");
App::LoadModuleClass("TicketManagementCM", "TMBatchFileValidationTickets");
App::LoadModuleClass("TicketManagementCM", "TMBatchValidationSession");
App::LoadModuleClass("TicketManagementCM", "TMBatchValidationTickets");
App::LoadModuleClass("TicketManagementCM", "TMTicketValidation");
App::LoadModuleClass("TicketManagementCM", "TMBatchTransactionLog");
App::LoadModuleClass("TicketManagementCM", "TMDecks");
App::LoadModuleClass("TicketManagementCM", "TMTickets");
App::LoadModuleClass("TicketManagementCM", "TMAccounts");
App::LoadModuleClass("TicketManagementCM", "TMProducts");
App::LoadModuleClass("TicketManagementCM", "TMGameManagement");
App::LoadModuleClass("TicketManagementCM", "TMAuditLog");
App::LoadModuleClass("TicketManagementCM", "TMCurrencies");
App::LoadModuleClass("TicketManagementCM", "TMHotTickets");
App::LoadModuleClass("TicketManagementCM", "TMTicketCancellation");
App::LoadModuleClass("TicketManagementCM", "TMDeckStatusHistory");

App::LoadControl("Button");
App::LoadControl("Hidden");
App::LoadControl("ComboBox");
App::LoadControl("DataTable");
App::LoadControl("TextBox");

$tmbatchvalidationsession = new TMBatchValidationSession();
$tmbatchvalidationtickets = new TMBatchValidationTickets();
$tmbatchfilevalidationsession = new TMBatchFileValidationSession();
$tmbatchfilevalidationtickets = new TMBatchFileValidationTickets();
$tmticketvalidation = new TMTicketValidation();
$tmbatchtranslog = new TMBatchTransactionLog();
$tmdecks = new TMDecks();
$tmtickets = new TMTickets();
$tmaccount = new TMAccounts();
$tmproducts = new TMProducts();
$tmgamemgt = new TMGameManagement();
$tmauditlog = new TMAuditLog();
$tmcurrencies = new TMCurrencies();
$tmdeckstatus = new TMDeckStatusHistory();

$frmXmlUpload = new FormsProcessor();

$csvnaming = App::getParam("csvnaming");
$currency = App::getParam('currency');
$arrSessionData = '';
$total_reimbursed = 0;
$total_claimed = 0;
$total_cancelled = 0;
$total_inactive = 0;
$total_prize_redeem = 0;
$total_stolen_pay = 0;
$total_stolen_nonpay = 0;
$total_expired_pay = 0;
$total_expired_nonpay = 0;
$ctr = 0;
$countInvalid = 0;
$countValid = 0;
$winstatus = '';
$prizename = '';

$ddlUsername = new ComboBox("ddlUsername", "ddlUsername", "Distributor: ");
$ddlUsername->ShowCaption =TRUE;
$ddlUsername->TabIndex = 4;
$ddlUsername->Args = "onchange='javascript: get_gamename();'";
$arraccounts = $tmaccount->SelectForValidationModule($_SESSION['acctid']);
$accountlist = new ArrayList();
$accountlist->AddArray($arraccounts);
$ddlUsername->ClearItems();
$litem = null;
$litem[] = new ListItem("Select Distributor", "0", true);
$ddlUsername->Items = $litem;
$ddlUsername->DataSource = $accountlist;
$ddlUsername->DataSourceText = "UserName";
$ddlUsername->DataSourceValue = "AID";
$ddlUsername->DataBind();

$ddlcardvalue = new ComboBox('ddlcardvalue','ddlcardvalue', 'Card Value: ');
$ddlcardvalue->ShowCaption = true;
$ddlcardvalue->Args = "onchange='javascript: get_dist();'";
$cardprize = $tmgamemgt->SelectByCardValue();
$cardprize_list = new ArrayList();
$cardprize_list->AddArray($cardprize);
$cardlist = null;
$cardlist[] = new ListItem("Please select","0",false);
$ddlcardvalue->Items = $cardlist;
$ddlcardvalue->DataSource = $cardprize_list;
$ddlcardvalue->DataSourceText = "CardPrice";
$ddlcardvalue->DataSourceValue = "CardPrice";
$ddlcardvalue->DataBind();

$gamenames = $tmproducts->SelectAllGameName();
$gamename_list = new ArrayList();
$gamename_list->AddArray($gamenames);
$ddlgamename = new ComboBox("ddlgamename","ddlgamename","Game Name: ");
$ddlgamename->Args = "onchange = 'javascript: get_gamenumber();'";
$gamenameopt = null;
$gamenameopt[] = new ListItem("Please select", "0", true);
$ddlgamename->Items = $gamenameopt;
$ddlgamename->DataSource = $gamename_list;
$ddlgamename->DataSourceText = "ProductName";
$ddlgamename->DataSourceValue = "ProductID";
$ddlgamename->DataBind();
$ddlgamename->ShowCaption = true;

$ddlgametype = new ComboBox("ddlgametype", "ddlgametype", "Game Number: ");
$ddlgametype->ShowCaption = true;
$ddlgametype->Args = "onchange='javascript: '";
$gamenumbers = $tmgamemgt->SelectByGameType();
$gamename_list = new ArrayList();
$gamename_list->AddArray($gamenumbers);
$litem = null;
$litem[] = new ListItem("Please Select", "0", true);
$ddlgametype->Items = $litem;
$ddlgametype->DataSource = $gamename_list;
$ddlgametype->DataSourceText = "GameNumber";
$ddlgametype->DataSourceValue = "GameID";
$ddlgametype->DataBind();

$txtSearchByID = new TextBox("txtSearchByID", "txtSearchByID", "Session ID: ");
$txtSearchByID->ShowCaption = true;
$txtSearchByID->Args="onkeypress='javascript: return isNumberKey(event);'";

$btnSearchByID = new Button("btnSearchByID", "btnSearchByID", "Search By ID");
$btnSearchByID->Args = "onclick='javascript: return SearchValidation();'";
$btnSearchByID->IsSubmit = true;

$btnUpload = new Button("btnUpload","btnUpload","Upload and Validate");
$btnUpload->IsSubmit = true;
$btnUpload->Args = "onclick='javascript: return validation(); return showloadingpage3(); '";

$btnClaim = new Button("btnClaim", "btnClaim", "Claim and Reimburse");
$btnClaim->Args = "onclick='javascript: confirm();'";

$btnOkay = new Button("btnOkay", "btnOkay", "Okay");
$btnOkay->IsSubmit = true;

$btnExportCSV = new Button("btnExportCSV", "btnExportCSV", "Export to CSV");
$btnExportCSV->IsSubmit = true;

$hiddenfilename = new Hidden("hiddenfilename","hiddenfilename","Hidden file name");
$hiddenId = new Hidden("hiddenId", "hiddenId", "");
$hiddenflag = new Hidden("hiddenflag","hiddenflag","Hidden Flag");
$hidaid = new Hidden("hidaid", "hidaid", "Hidden AID");
$xmltype = new Hidden("xmltype","xmltype","xmltype");

$frmXmlUpload->AddControl($ddlUsername);
$frmXmlUpload->AddControl($ddlcardvalue);
$frmXmlUpload->AddControl($ddlgamename);
$frmXmlUpload->AddControl($ddlgametype);
$frmXmlUpload->AddControl($txtSearchByID);
$frmXmlUpload->AddControl($btnUpload);
$frmXmlUpload->AddControl($btnSearchByID);
$frmXmlUpload->AddControl($hiddenfilename);
$frmXmlUpload->AddControl($btnClaim);
$frmXmlUpload->AddControl($hiddenId);
$frmXmlUpload->AddControl($hiddenflag);
$frmXmlUpload->AddControl($xmltype);
$frmXmlUpload->AddControl($hidaid);
$frmXmlUpload->AddControl($btnOkay);
$frmXmlUpload->AddControl($btnExportCSV);

$frmXmlUpload->ProcessForms();

$bfvid = '';

$arrcurr = $tmcurrencies->SelectByCurrencySymbol("KHR");
$fixedrate = $arrcurr[0]["FixedRate"];

if ($frmXmlUpload->IsPostBack)
{
    $cardprice = $ddlcardvalue->SubmittedValue;
    $distributorID = $ddlUsername->SubmittedValue;
    $gamename = $ddlgamename->SubmittedValue;
    $gameid = $ddlgametype->SubmittedValue;
    
    if ($hiddenflag == 1)
    {
        $ddlgametype->ClearItems();
        $gamenumbers = $tmproducts->SelectGameNumPerGameName($gamename);
        $gamenumbers_list1 = new ArrayList();
        $gamenumbers_list1->AddArray($gamenumbers);       
        $litemgmetype1 = null;
        $litemgmetype1[] = new ListItem("Please Select", "0", false);
        $ddlgametype->Items = $litemgmetype1;
        $ddlgametype->DataSource = $gamenumbers_list1;
        $ddlgametype->DataSourceText = "GameNumber";
        $ddlgametype->DataSourceValue = "GameID";
        $ddlgametype->DataBind();
        $ddlgametype->SetSelectedValue($gameid);
        
        $ddlgamename->ClearItems();
        $gamenames = $tmproducts->SelectGameNamePerCardPrice($cardprice);
        $gamename_list1 = new ArrayList();
        $gamename_list1->AddArray($gamenames);       
        $litemgmetype1 = null;
        $litemgmetype1[] = new ListItem("Please Select", "0", false);
        $ddlgamename->Items = $litemgmetype1;
        $ddlgamename->DataSource = $gamename_list1;
        $ddlgamename->DataSourceText = "ProductName";
        $ddlgamename->DataSourceValue = "ProductID";
        $ddlgamename->DataBind();
        $ddlgamename->SetSelectedValue($gamename);
    }
    
    if ($btnUpload->SubmittedValue == "Upload and Validate")
    {
        if($_FILES['file']['type'] == "")
        {
            $errormsg = "Please specify file.";
            $errormsgtitle = "ERROR!";
        }
        else if (($_FILES['file']['type'] == "application/octet-stream") || ($_FILES['file']['type'] == "text/csv") || ($_FILES['file']['type'] == "application/vnd.ms-excel") || ($_FILES['file']['type'] == "text/x-comma-separated-values"))
        {
            if ($_FILES["file"]["error"] > 0)
            {
                $errormsg = "Return Code: " . $_FILES["file"]["error"] . "<br />";
                $errormsgtitle = "ERROR!";
            }
            else
            {
                $nameconv = explode("_", $_FILES["file"]["name"]);
                if ($nameconv[0] == $csvnaming)
                {
                    if (strlen($nameconv[1]) >= 2 && strlen($nameconv[1]) < 6)
                    {
                        if (strlen($nameconv[2]) == 10)
                        {
                            $nameconv2 = explode(".csv", $nameconv[3]);
                            if (strlen($nameconv2[0]) == 3)
                            {
                                $date_time = date("Y-m-d H:m:i");
                                $filename = explode(".csv", $_FILES["file"]["name"]);
                                $newfilename = $filename[0]."_".$date_time.".csv";
                                $fullpath =  "../xml/csv/" . $newfilename;

                                $retval = move_uploaded_file($_FILES["file"]["tmp_name"], $fullpath);
                                if (!$retval){
                                    $errormsg = "Error uploading the file.";
                                    $errormsgtitle = "ERROR!";
                                } 
                                else 
                                {
                                    $csvStr = file_get_contents($fullpath);
                                    $csvdata = explode("\n", $csvStr);
                                    /*if (count($csvdata) > 1)
                                    {
                                        $countcsv = count($csvdata) - 1;
                                    }
                                    else
                                    {*/
                                        $countcsv = count($csvdata);
                                    //}

                                    $arrEntries = array();
                                    $arrEntries1 = array();
                                    $has_alpha = false;
                                    $boolean = false;
                                    $boolean_game = true;
                                    
                                    for($i = 0; $i < $countcsv; $i++)
                                    {
                                        $Entry1 = "";
                                        $csvrow = $csvdata[$i];
                                        $array = explode(",", $csvrow);
                                        
                                        if ($array[0] != "" || $array[1] != "")
                                        {
                                            if (!is_numeric(trim($array[0])) || !is_numeric(trim($array[1])))
                                            {
                                                $has_alpha = true;                                            
                                            }
                                        }
                                        $arrvirn[] = trim($array[1]);
                                        $arrtn[] = trim($array[0]);
                                        $Entry1["ValidationNum"] = trim($array[1]);                            
                                        $Entry1["TicketNum"] = trim($array[0]);                                        
                                        $arrEntries1[] = $Entry1;
                                    }
                                    
                                    if (hasDuplicate($arrvirn) || hasDuplicate($arrtn))
                                    {
                                        $errormsg = "Invalid file content. Please upload a valid csv files.";
                                        $errormsgtitle = "ERROR!";
                                        unset($arrvirn);
                                        unset($arrtn);
                                    }
                                    else
                                    {
                                        if($has_alpha == false)
                                        {    
                                            for ($i = 0; $i < count($arrEntries1); $i++)
                                            {
                                                $Entry = "";
                                                $csvrow = $arrEntries1[$i];
                                                $ValidationNum = trim(str_replace(array('\r', '\n', '\r\n'), "", $csvrow['ValidationNum']));
                                                $TicketNum = trim(str_replace(array('\r', '\n', '\r\n'), "", $csvrow['TicketNum']));

                                                if ($ValidationNum != "" && $TicketNum != "")
                                                {
                                                    $virn_gameno = substr($ValidationNum, 0, 3);
                                                    $ticket_gameno = substr($TicketNum, 0, 3);
                                                    $array_virn_game = $tmgamemgt->SelectByGameNumber($virn_gameno);
                                                    $array_ticket_game = $tmgamemgt->SelectByGameNumber($ticket_gameno);
                                                    $tmpgameid = $array_virn_game[0]["GameID"];
                                                    $tmpgameid2 = $array_ticket_game[0]["GameID"];

                                                    if (($tmpgameid != $gameid) || ($tmpgameid2 != $gameid))
                                                    {
                                                        $boolean_game = false;
                                                    }
                                                    else if (strlen($ValidationNum) < 12 || strlen($TicketNum) < 12)
                                                    {
                                                        $boolean = true;
                                                    }
                                                    else 
                                                    {   
                                                        $tmpvalno = substr($ValidationNum, 3,8);
                                                        $virn_nochksum = substr($ValidationNum, 0,strlen($ValidationNum) - 1);
                                                        $checksum = substr($ValidationNum, strlen($ValidationNum) - 1, strlen($ValidationNum));

                                                        $tmpbookno = substr($TicketNum,3,6);
                                                        $tmpticket = substr($TicketNum, 9,3);

                                                        // Check if Book Status
                                                        $arrBookDtls = $tmdecks->IsBookNoValid($tmpbookno,$tmpgameid);
                                                        $bookstatus = $arrBookDtls[0]["Status"];
                                                        $bookid = $arrBookDtls[0]["BookID"];
                                                        if ($bookstatus == 1 || $bookstatus == 5 || $bookstatus == 7 || $bookstatus == 8 || $bookstatus == 9 || $bookstatus == 10) // Active
                                                        {
                                                            $where = "WHERE BookID='$bookid' ORDER BY DateChanged DESC LIMIT 1";
                                                            $ds = $tmdeckstatus->SelectByWhere($where);
                                                            $oldstatus = $ds[0]["OldStatus"];
                                                            if (($oldstatus == 2 || $oldstatus == 3 || $oldstatus == 4) && ($bookstatus != 1)){
                                                                $Entry["Status"] = 2;
                                                            }
                                                            else
                                                            {
                                                                // Check Ticket Status
                                                                $arrTicketDtls = $tmtickets->IsTicketNoValid($tmpticket, $bookid);
                                                                $ticketid = $arrTicketDtls[0]["TicketID"];
                                                                $ticketstatus = $arrTicketDtls[0]["Status"];
                                                                $arrWinning = $tmticketvalidation->IsValidVIRN($tmpvalno,$tmpgameid);
                                                                if (count($arrWinning) > 0)
                                                                {
                                                                    $winningcardid = $arrWinning[0]["WinningCardID"];
                                                                    $prizename = $arrWinning[0]["PrizeName"];
                                                                    $winstatus = $arrWinning[0]["Status"];
                                                                }

                                                                if ($ticketstatus == 1 || $ticketstatus == 6) // Active or Claimed
                                                                {
                                                                    // START CHECKING CHECK DIGIT
                                                                    $checkdigit = CheckDigit($virn_nochksum);
                                                                    if($checkdigit % 10 != $checksum)
                                                                    {
                                                                        $Entry["Status"] = 0;                                      // Temporart Status: Invalid Checksum
                                                                        $winstatus = 6;                
                                                                        $prizename = 0;
                                                                    } else {
                                                                        // Check if Valid VIRN
                                                                        $arrWinning = $tmticketvalidation->IsValidVIRN($tmpvalno,$tmpgameid);
                                                                        if (count($arrWinning) <= 0)
                                                                        {
                                                                            $winningcardid = NULL;
                                                                            $Entry["Status"] = 2;                                   // Invalid
                                                                            $winstatus = 6;                                         // Temporary status for Non-existing VIRN
                                                                            $prizename = 0;
                                                                        } else {
                                                                            $winningcardid = $arrWinning[0]["WinningCardID"];
                                                                            $prizename = $arrWinning[0]["PrizeName"];
                                                                            $winstatus = $arrWinning[0]["Status"];
                                                                            switch ($winstatus)
                                                                            {
                                                                                case 0: $Entry["Status"] = 1; break;                // Pending - Winning
                                                                                case 1: $Entry["Status"] = 1; break;                // Verified - Winning
                                                                                case 2: $Entry["Status"] = 3; break;                // Claimed - Claimed
                                                                                case 3: $Entry["Status"] = 5; break;                // Cancelled - Cancelled
                                                                                case 4: $Entry["Status"] = 2; break;                // Expired - Nonwinning
                                                                                case 5: $Entry["Status"] = 4; break;                // Reimbursed - Reimbursed             
                                                                            }
                                                                        }
                                                                    }
                                                                } 
                                                                else if ($ticketstatus == 5)                                        // Cancelled Ticket
                                                                {
                                                                    $Entry["Status"] = 5;
                                                                    saveToHotTickets($ticketid, $_SESSION['acctid'], $ValidationNum, $TicketNum);
                                                                }
                                                                else if ($ticketstatus == 7) { $Entry["Status"] = 7;}               // Stolen (Payable)
                                                                else if ($ticketstatus == 8)                                        // Stolen (Non-Payable)
                                                                {
                                                                    $Entry["Status"] = 8; 
                                                                    saveToHotTickets($ticketid, $_SESSION['acctid'], $ValidationNum, $TicketNum);
                                                                }
                                                                else if ($ticketstatus == 9) { $Entry["Status"] = 9;}               // Expired (Payable)
                                                                else if ($ticketstatus == 10)                                       // Expired (Non-Payable)
                                                                {
                                                                    $Entry["Status"] = 10;
                                                                    saveToHotTickets($ticketid, $_SESSION['acctid'], $ValidationNum, $TicketNum);
                                                                }
                                                                else { $Entry["Status"] = 2; }                                      // Invalid Ticket (2-On Freight, 3-On Stock, 4-Assigned)
                                                            }
                                                        }
                                                        else { $Entry["Status"] = 6; }                                          // Invalid Book (2-On Freight, 3-On Stock, 4-Assigned) 
                                                    }

                                                    $Entry["VIRN"] = $tmpvalno;
                                                    $Entry["ValidationNo"] = $ValidationNum;
                                                    $Entry["GameNumber"] = substr($ValidationNum, 0, 3);
                                                    $Entry["TicketNo"] =  $TicketNum;
                                                    $Entry["GameID"] = $gameid;
                                                    $Entry["AssignedToAID"] = $distributorID;
                                                    $Entry["WinStatus"] = $winstatus;
                                                    $Entry["PrizeName"] = $prizename;
                                                    $arrEntries[$i] = $Entry;
                                                }
                                            }

                                            if ($boolean_game == false)
                                            {
                                                $errormsg = "Game Number selected does not match with the CSV content. Kindly check your CSV file.";
                                                $errormsgtitle = "Error!";
                                            }
                                            else if ($boolean == true)
                                            {
                                                $errormsg = "Ticket Number and Validation Number digits are not complete. Kindly check your CSV file.";
                                                $errormsgtitle = "Error!";
                                            } 
                                            else 
                                            {
                                                // Create Batch File Validation Session
                                                $bfvs["DateCreated"] = "now_usec()";
                                                $bfvs["CreatedByAID"] = $_SESSION['acctid'];
                                                $bfvs["Status"] = 1;
                                                $tmbatchfilevalidationsession->Insert($bfvs);            
                                                if ($tmbatchfilevalidationsession->HasError)
                                                {
                                                    $errormsgtitle = "ERROR!";
                                                }
                                                $bfvid = $tmbatchfilevalidationsession->LastInsertID;

                                                for ($i = 0; $i < count($arrEntries); $i++)
                                                {
                                                    $Entry["Status"] = $arrEntries[$i]["Status"];
                                                    $Entry["VIRN"] = $arrEntries[$i]["VIRN"];
                                                    $Entry["ValidationNo"] = $arrEntries[$i]["ValidationNo"];
                                                    $Entry["GameNumber"] = $arrEntries[$i]["GameNumber"];
                                                    $Entry["TicketNo"] =  $arrEntries[$i]["TicketNo"];
                                                    $Entry["GameID"] = $arrEntries[$i]["GameID"];
                                                    $Entry["AssignedToAID"] = $arrEntries[$i]["AssignedToAID"];
                                                    $Entry["WinStatus"] = $arrEntries[$i]["WinStatus"];
                                                    $Entry["PrizeName"] = $arrEntries[$i]["PrizeName"];
                                                    $Entry["BatchFileValidationSessionID"] = $bfvid;
                                                    $arrEntries[$i] = $Entry;
                                                }

                                                $filename1 = explode("_", $newfilename);
                                                $newfilename1 = $filename1[0]."_".$filename1[1]."_".$filename1[2]."_".$filename1[3]."_".$bfvid.".csv";
                                                $fullpath1 =  "../xml/csv/" . $newfilename1;
                                                rename($fullpath, $fullpath1);                                      

                                                $tmbatchfilevalidationtickets->InsertMultiple($arrEntries);
                                                if ($tmbatchfilevalidationtickets->HasError)
                                                {
                                                    $errormsg = "An error occured while uploading. Please try again.1";
                                                    $errortitle = "Error!";
                                                }
                                                else
                                                {
                                                    $tmbatchfilevalidationsession->EndValidation($bfvid); 
                                                    if ($tmbatchfilevalidationsession->HasError)
                                                    {
                                                        $errormsg = "An error occured while uploading. Please try again.2";
                                                        $errortitle = "Error!";
                                                    }
                                                    else
                                                    {
                                                        $uploadmsg = "Batch upload validation is successful. <br />Your batch session ID is <br /><b>$bfvid</b>.<br /><br />Please take note of this value as reference to this session.";
                                                        $uploadmsgtitle = "Successful!";

                                                        // Update Audit Trail Log
                                                        $tmaudit["SessionID"] = $_SESSION['sid'];
                                                        $tmaudit["AID"] = $_SESSION['acctid'];
                                                        $tmaudit["TransDetails"] = 'Batch File Upload Session ID:'. $bfvid;
                                                        $tmaudit["TransDateTime"] = 'now_usec()';
                                                        $tmaudit["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
                                                        $tmaudit["AuditTrailFunctionID"] = '35';  
                                                        $tmauditlog->Insert($tmaudit);
                                                        if ($tmauditlog->HasError)
                                                        {
                                                            $errormsg = $tmauditlog->getError();
                                                            $errormsgtitle = "ERROR!";
                                                        }
                                                        else
                                                        {
                                                            /* insert to batch transaction log*/
                                                            $batchtrans["BatchID"] = $bfvid;
                                                            $batchtrans["GameID"] = $gameid;
                                                            $batchtrans["FileName"] = $newfilename1;
                                                            $batchtrans["TransactionDate"] = "now_usec()";
                                                            $batchtrans["TransactionType"] = 4;
                                                            $batchtrans["ProcessedByAID"] = $_SESSION["acctid"];
                                                            $batchtrans["Status"] = 1;
                                                            $tmbatchtranslog->Insert($batchtrans);
                                                            if ($tmbatchtranslog->HasError) 
                                                            {
                                                                $errormsgtitle = "ERROR!";
                                                                $errormsg = "Error has occured:" . $tmbatchtranslog->getError();                                           
                                                            }
                                                        }
                                                    }
                                                }
                                            } // End of Boolean
                                    } else { $errormsg = "Invalid file content. Please upload a valid csv file."; $errormsgtitle = "ERROR!";}
                                } // End of checking for content duplication
                              } // End of Return Value - Uploading CSV
                            } else { $errormsg = "Incorrect file format. Please upload a valid file format."; $errormsgtitle = "ERROR 1!";}
                        } else { $errormsg = "Incorrect file format. Please upload a valid file format."; $errormsgtitle = "ERROR 2!";}
                    } else { $errormsg = "Incorrect file format. Please upload a valid file format."; $errormsgtitle = "ERROR 3!";}
                } else { $errormsg = "Incorrect file format. Please upload a valid file format."; $errormsgtitle = "ERROR 4!";}
            }
            
            // UPDATE IN BATCH VALIDATION SESSION & TICKETS TABLE                
            $validtickets = $tmbatchfilevalidationtickets->SelectValidTicketsForClaiming($bfvid);
            if(count($validtickets) != '')
            {
                 $arr["DateCreated"] = "now_usec()";
                 $arr["CreatedByAID"] = $_SESSION['acctid'];
                 $arr["Status"] = 1;
                 $tmbatchvalidationsession->Insert($arr);

                if ($tmbatchvalidationsession->HasError)
                {
                    $errormsgtitle = "ERROR!";
                }
                else
                {
                    $bvid1 = $tmbatchvalidationsession->LastInsertID;

                    for($i=0; $i < count($validtickets); $i++)
                    {
                        $arrWinningCardID = $tmbatchfilevalidationtickets->getWinningCardID($validtickets[$i]['VIRN'], $validtickets[$i]['GameID']);
                        $where = " WHERE WinningCardID = " . $arrWinningCardID[0]["WinningCardID"];
                        $arr_chk = $tmbatchvalidationtickets->SelectByWhere($where);
                        if (count($arr_chk) == 0)
                        {
                            $arr1["GameID"] = $validtickets[$i]["GameID"];
                            $arr1["TicketNumber"] = $validtickets[$i]["TicketNo"];
                            $arr1["ValidationNumber"] = $validtickets[$i]["ValidationNo"];
                            $arr1["WinningCardID"] = $arrWinningCardID[0]["WinningCardID"];
                            $arr1["BatchValidationSessionID"] = $bvid1;
                            $arr1["DateCreated"] = "now_usec()";
                            $arr1["AssignedtoAID"] = $distributorID;
                            $arr1["Status"] = 1;
                            $arrInsert[] = $arr1;
                        }
                        else
                        {
                            // Overwrite the existing ticket with the current ticket.
                            $newTicket = trim(str_replace(array('\r', '\n', '\r\n'), "", $validtickets[$i]["TicketNo"]));
                            //$existingTicket = $arr_chk[0]["TicketNumber"];
                            //if ($existingTicket == $newTicket){
                                $tmbatchvalidationtickets->UpdateExistingTicket($newTicket, $arrWinningCardID[0]["WinningCardID"],$bvid1, $distributorID);
                            //}
                        }
                    }
                    
                    if (isset($arrInsert) || count($arrInsert) >= 1)
                    {
                        $tmbatchvalidationtickets->InsertMultiple($arrInsert);
                        if($tmbatchvalidationtickets->HasError)
                        {
                            $errormsgtitle = "ERROR!";
                            $errormsg = $tmbatchvalidationtickets->getError();
                        }
                    }
               }

            }
            $hiddenId->Text = $bfvid;
            $validate = 1;
        }
        else
        {
            $errormsg = "Incorrect file format. Please upload a valid file format.";
            $errormsgtitle = "ERROR 5!";
        }
    } // END OF UPLOAD AND VALIDATE BUTTON
        
    if ($btnSearchByID->SubmittedValue == "Search By ID" && $txtSearchByID->Text != '')
    {
        $bfvid = $txtSearchByID->SubmittedValue;
        $hiddenId->Text = $bfvid;
    }

    if ($bfvid != '')
    {
        $arrSessionData = $tmbatchfilevalidationtickets->SelectByValidationSessionID($bfvid);
        if (is_array($arrSessionData))
        {
            for ($i = 0; $i < count($arrSessionData); $i++)
            {
                $sessiondata = $arrSessionData[$i];
                $sessiondata["CardPrice"] = $sessiondata["CardPrice"];
                $sessiondata["GameNumber"] = $sessiondata["GameNumber"];
                $sessiondata["GameName"] = $sessiondata["ProductName"];
                $sessiondata["Distributor"] = $sessiondata["UserName"];
                
                if ($sessiondata["TicketStatus"] == 1)
                {
                    $sessiondata["StatusString"] = "Winning";
                    $countValid += 1;
                }
                if ($sessiondata["TicketStatus"] == 2 || $sessiondata["TicketStatus"] == 0)
                {
                    $sessiondata["StatusString"] = "Non-Winning";
                    $countInvalid += 1;
                }
                if ($sessiondata["TicketStatus"] == 3)
                {
                    $sessiondata["StatusString"] = "Claimed";
                    $total_claimed += 1;
                }
                if ($sessiondata["TicketStatus"] == 4)
                {
                    $sessiondata["StatusString"] = "Reimbursed";
                    $total_reimbursed += 1;
                }
                if ($sessiondata["TicketStatus"] == 5)
                {
                    $sessiondata["StatusString"] = "Void";
                    $total_cancelled += 1;
                }
                if ($sessiondata["TicketStatus"] == 6)
                {
                    $sessiondata["StatusString"] = "Inactive";
                    $total_inactive += 1;
                }
                if ($sessiondata["TicketStatus"] == 7)
                {
                    $sessiondata["StatusString"] = "Stolen (Payable)";
                    $total_stolen_pay += 1;
                }
                if ($sessiondata["TicketStatus"] == 8)
                {
                    $sessiondata["StatusString"] = "Stolen (Non-Payable)";
                    $total_stolen_nonpay += 1;
                }
                if ($sessiondata["TicketStatus"] == 9)
                {
                    $sessiondata["StatusString"] = "Expired (Payable)";
                    $total_expired_pay += 1;
                }
                if ($sessiondata["TicketStatus"] == 10)
                {
                    $sessiondata["StatusString"] = "Expired (Non-Payable)";
                    $total_expired_nonpay += 1;
                }
                
                // Sum the Total Prize to be redeemed
                if ($sessiondata["TicketStatus"] == 1 || $sessiondata["TicketStatus"] == 7 || $sessiondata["TicketStatus"] == 9)
                {
                    if (strpos($sessiondata["PrizeName"], 'KHR') !== false)
                    {
                        $str_prize = preg_replace('/[\KHR,]/', '', $sessiondata["PrizeName"]);
                        $sessiondata["PrizeName"] = '$' . $str_prize / $fixedrate;
                        $prize = $str_prize / $fixedrate;
                    }
                    else if (strpos($sessiondata["PrizeName"], $currency) !== false)
                    {
                        $prize = preg_replace('/[\"'.$currency.'",]/', '', $sessiondata["PrizeName"]);
                    }
                    $total_prize_redeem += $prize; 
                }
                $arrSessionData[$i] = $sessiondata;
            }
        }
        $tmpCount = 0;
        $tmpCount = $countValid + $total_expired_pay + $total_stolen_pay + $total_claimed;
        ($tmpCount) > 0 ? $validate = 1 : $validate = 0;

        /* Added By Noel Antonio - June 15, 2012 */
        $fp = fopen("../csv/Batch_File_Upload.csv","w");
        if($fp)
        {
            $wvalid = "No. of Valid Tickets,". number_format($countValid) ."\r\n";
            fwrite($fp,$wvalid);
            $winvalid = "No. of Invalid Tickets,". number_format($countInvalid) ."\r\n";
            fwrite($fp,$winvalid);
            $wclaim = "No. of Claimed Tickets,". number_format($total_claimed) ."\r\n";
            fwrite($fp,$wclaim);
            $wcancel = "No. of Void Tickets,". number_format($total_cancelled) ."\r\n";
            fwrite($fp,$wcancel);
            $wreim = "No. of Reimbursed Tickets,". number_format($total_reimbursed) ."\r\n";
            fwrite($fp,$wreim);
            $winc = "No. of Inactive Tickets,". number_format($total_inactive) ."\r\n";
            fwrite($fp,$winc);
            $wsp = "No. of Stolen Tickets (Payable),". number_format($total_stolen_pay) ."\r\n";
            fwrite($fp,$wsp);
            $wsnp = "No. of Stolen Tickets (Non-Payable),". number_format($total_stolen_nonpay) ."\r\n";
            fwrite($fp,$wsnp);
            $wep = "No. of Expired Tickets (Payable),". number_format($total_expired_pay) ."\r\n";
            fwrite($fp,$wep);
            $wenp = "No. of Expired Tickets (Non-Payable),". number_format($total_expired_nonpay) ."\r\n";
            fwrite($fp,$wenp);
            $wprize = "Total Redeemable Prize,". $currency . number_format($total_prize_redeem) ."\r\n\r\n";
            fwrite($fp,$wprize);
            $data = '';
            $header = "Card Value,Game Number,Game Name,Retailer Username,Book Ticket Number,Validation Number,Status,Prize Amount\r\n";
            fwrite($fp,$header);
            if(count($arrSessionData) > 0)
            {
                for($i = 0 ; $i < count($arrSessionData) ; $i++)
                {
                    if (strpos($arrSessionData[$i]['PrizeName'], 'KHR') !== false)
                    {
                        $str_prize = preg_replace('/[\KHR,]/', '', $arrSessionData[$i]['PrizeName']);
                        $arrSessionData[$i]['PrizeName'] = '$' . $str_prize / $fixedrate;
                    }
                    
                    $data .= $arrSessionData[$i]['CardPrice'] . "," .$arrSessionData[$i]['GameNumber'] . "," . $arrSessionData[$i]['GameName'] . "," . $arrSessionData[$i]['Distributor'] . "," . trim(str_replace(array('\r', '\n', '\r\n'), "", $arrSessionData[$i]['TicketNo'])) . "," . trim(str_replace(array('\r', '\n', '\r\n'), "", $arrSessionData[$i]['ValidationNo'])) . "," . $arrSessionData[$i]['StatusString'] . "," . str_replace(',','',$arrSessionData[$i]['PrizeName']) . "\r\n";
                }
            }
            else
            {	
                $data .= "No Records Found;\r\n";
            }
            fwrite($fp,$data);
        }
        else
        {
            echo "<script>alert('Cannot create file.')</script>";
        }
        /* Added By Noel Antonio - June 15, 2012 */
    }
    
    /* Added By Noel Antonio - June 15, 2012 */
    if ($btnExportCSV->SubmittedValue == "Export to CSV")
    {
        header('Content-type: text/csv');
        header("Content-Disposition: attachment; filename=".'Batch_File_Upload.csv');
        header('Pragma: public');
        readfile('../csv/Batch_File_Upload.csv');
        exit;
    }
    /* Added By Noel Antonio - June 15, 2012 */
    
    if($btnOkay->SubmittedValue == "Okay")
    {
        $countValid = 0;
        $countInvalid = 0;
        $total_prize_redeem = 0;
        
        $validtickets1 = $tmbatchfilevalidationtickets->SelectValidTicketsForClaiming($hiddenId->Text);
        for($i=0; $i < count($validtickets1); $i++)
        {
            $bookid = $validtickets1[$i]['BookID'];
            $ticketno = $validtickets1[$i]['Ticket'];
            
            // Update WINNINGCARDS
            $arrWinningCardID = $tmbatchfilevalidationtickets->getWinningCardID($validtickets1[$i]['VIRN'], $validtickets1[$i]['GameID']);
            $book = substr($validtickets1[$i]['TicketNo'],3,6);
            $remarks = "Claimed by ".$validtickets1[$i]['AssignedToAID'];
            $tmticketvalidation->UpdateWinningCards($arrWinningCardID[0]['WinningCardID'], $book, 5, 'now_usec()', $validtickets1[$i]['AssignedToAID'], $remarks);
            if ($tmticketvalidation->HasError)
            {
                $errormsg = $tmticketvalidation->getError();
                $errormsgtitle = "ERROR!";
            }
            
            
            // Update Batch File Validation Tickets and VIRN which were already REIMBURSED
            $tmbatchfilevalidationtickets->UpdateBatchFileTickets($validtickets1[$i]['VIRN'], $validtickets1[$i]['AssignedToAID'], $validtickets1[$i]['GameID']);
            if ($tmbatchfilevalidationtickets->HasError)
            {
                $errormsg = $tmbatchfilevalidationtickets->getError();
                $errormsgtitle = 'ERROR!';
            }
            
            // Update Batch Validation Tickets to PROCESSED
            $tmbatchvalidationtickets->UpdateBatchTickets($arrWinningCardID[0]['WinningCardID'], $validtickets1[$i]['AssignedToAID']);
            if ($tmbatchvalidationtickets->HasError)
            {
                $errormsg = $tmbatchvalidationtickets->getError();
                $errormsgtitle = 'ERROR!';
            }
            
            // Update TICKETS to CLAIMED
            $tmtickets->UpdateTicketStatus($bookid, $ticketno);
            if ($tmtickets->HasError)
            {
                $errormsg = $tmticketvalidation->getError();
                $errormsgtitle = 'ERROR!';
            }
            
            if (strpos($validtickets1[$i]["PrizeName"], 'KHR') !== false)
            {
                $str_prize = preg_replace('/[\KHR,]/', '', $validtickets1[$i]["PrizeName"]);
                $prize = $str_prize / $fixedrate;
            }
            else if (strpos($validtickets1[$i]["PrizeName"], $currency) !== false)
            {
                $prize = preg_replace('/[\"'.$currency.'",]/', '', $validtickets1[$i]["PrizeName"]);
            }
                    
            $total_prize_redeem += $prize; 
            $countValid += 1;
        }

        $arrSessionData = $tmbatchfilevalidationtickets->SelectByValidationSessionID($hiddenId->SubmittedValue);
        $count = count($arrSessionData);
        $countValid = count($validtickets1);
        $countInvalid = $count - $countValid;
        $errormsg = '<table class=\"table-list\" style=\"width:90%; margin-left: 5%;\"><tr class = \"evenrow\"><th style=\"width:315px;\">No. of Valid Tickets</th><td align=\"center\">' . $countValid .'</td></tr><tr class = \"oddrow\"><th style=\"width:310px;\">No. of Invalid Tickets</th><td align=\"center\">' . $countInvalid . '</td></tr><tr class = \"evenrow\"><th style=\"width:310px;\">Total Prize Redeemed</th><td align=\"center\">'. $currency . number_format($total_prize_redeem) . '</td></tr></table>';
        $errormsgtitle = 'VALIDATION SUMMARY';                        
    } // btnClaim      
}

function CheckDigit($number) 
{ 
        $total_step1 = "";
        $total_step3 = "";
	// Strip any non-digits 
	$number=preg_replace('/\D/', '', $number);
	
	// Set the string length 
	$number_length=strlen($number);
	

	for ($i=1; $i<=$number_length; $i++) 
	{
		$digit = $number[$i-1];
		if($i % 2)
		{
                    //step 1
                    //odd numbers
                    $total_step1 += $digit;
		}
		else
		{
                    //step 3
                    //even numbers
                    $total_step3 += $digit;
		}
	}
	//step2
	$result = $total_step1 * 3;
	//step4
	$result = $result + $total_step3;
	//step5
	$result = 10 - $result % 10;
	
	return $result;      
}

/**
 * This function will check if array has duplicate values.
 * @author Noel Antonio 04-03-2013
 * @param array
 * @return boolean
 */
function hasDuplicate($array){
      return (count($array)==count(array_unique($array))) ? false:true;  
}

/**
 * @author Noel Antonio 04-08-2013
 * This function will insert record to hot tickets table once the user validate a ticket
 * which is either stolen or expired and not payable.
 * @param $ticketid the ticket id to be validated
 * @param $acctid the logged-in user
 * @param $virn the validation number
 * @param $ticketno the ticket number
 * @return boolean
 */
function saveToHotTickets($ticketid, $acctid, $virn, $ticketno)
{
    $tmhottickets = new TMHotTickets();
    $tmticketcancellation = new TMTicketCancellation();
    
    $arrtc = $tmticketcancellation->SelectCancelTypeByTicketID($ticketid);
    if (count($arrtc) == 1)
    {
        $canceltype = $arrtc[0]["CancelType"];

        $insertht["AID"] = $acctid;
        $insertht["VIRN"] = $virn;
        $insertht["TicketNumber"] = $ticketno;
        $insertht["DateCreated"] = "now_usec()";
        $insertht["CreatedByAID"] = $acctid;
        $insertht["CancellationMode"] = $canceltype;
        $tmhottickets->Insert($insertht);
        if ($tmhottickets->HasError){
            return false;
        } else {
            return true;
        }
    }
}
?>