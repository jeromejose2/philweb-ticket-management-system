<?php 
                                                       //Limits the maximum execution time , if set to 0 no maximum time is imposed. Safe mode should be turned off.
$stylesheets[] = "css/default.css";
$javascripts[] = "jscripts/validations.js";
$pagesubmenuid = 44;

App::LoadCore("CSV.class.php");
App::LoadCore("File.class.php");
App::LoadModuleClass("TicketManagementCM", "TMBatchFileValidationSession");
App::LoadModuleClass("TicketManagementCM", "TMBatchFileValidationTickets");

App::LoadControl("Button");
App::LoadControl("Hidden");
App::LoadControl("DataTable");
App::LoadControl("TextBox");

$batchfilevalidation = new TMBatchFileValidationSession();
$arrSessionData = '';

$frmXmlUpload = new FormsProcessor();

$btnUpload = new Button("btnUpload","btnUpload","Upload and Validate");
$btnUpload->IsSubmit = true;
$btnUpload->Args = "onclick='javascript: return showloadingpage3();'";
$hiddenfilename = new Hidden("hiddenfilename","hiddenfilename","Hidden file name");

$txtSearchByID = new TextBox("txtSearchByID", "txtSearchByID", "Session ID: ");
$txtSearchByID->ShowCaption = true;
$frmXmlUpload->AddControl($txtSearchByID);

$btnSearchByID = new Button("btnSearchByID", "btnSearchByID", "Search By ID");
$btnSearchByID->IsSubmit = true;
$frmXmlUpload->AddControl($btnSearchByID);

$frmXmlUpload->AddControl($btnUpload);
$frmXmlUpload->AddControl($hiddenfilename);

$frmXmlUpload->ProcessForms();

$bfvid = '';
if ($frmXmlUpload->IsPostBack)
{
    $batchfilevalidationtickets = new TMBatchFileValidationTickets();

    if ($btnUpload->SubmittedValue == "Upload and Validate")
    {
            $bfvs["DateCreated"] = "now()";
            $bfvs["CreatedByAID"] = $_SESSION['acctid'];
            $batchfilevalidation->Insert($bfvs);
            $bfvid = $batchfilevalidation->LastInsertID;

		if($_FILES['file']['type'] == "")
		{
		    $errormsg = "Please specify file.";
		    $errormsgtitle = "ERROR!";
		}
		else if (($_FILES['file']['type'] == "application/octet-stream") || ($_FILES["file"]["type"] == "text/csv") || ($_FILES["file"]["type"] == "application/vnd.ms-excel"))
		{
                    if ($_FILES["file"]["error"] > 0)
		    {
		        $errormsg = "Return Code: " . $_FILES["file"]["error"] . "<br />";
		        $errormsgtitle = "ERROR!";
		    }
                    else
                    {
                        $filename = $_FILES["file"]["name"];
                        $fullpath = "../xml/csv/" . $filename;
                        move_uploaded_file($_FILES["file"]["tmp_name"], $fullpath);
                        
                        $csvStr = file_get_contents($fullpath);
                        
                        $fp = new File($filename);
                        $strfile = $fp->ReadToEnd();
                        $strfile = trim($strfile);
                        
                        $strfile = str_replace("\r", "", $csvStr);
                        $csvdata = explode("\n", $csvStr);
                        
                                               
                        //$csv = new CSV();
                        //$csvdata = $csv->ParseCSV($filename);                        
                       
                        $arrEntries = "";
                        for ($i = 0; $i < count($csvdata); $i++)
                        {
                            $Entry = "";
                            $csvrow = $csvdata[$i];
                            $Entry["VIRN"] = substr($csvrow, 3, 8);
                            $Entry["GameNumber"] = substr($csvrow, 0, 3);
                            $Entry["BatchFileValidationSessionID"] = $bfvid;
                            $arrEntries[] = $Entry;
                        }

                        $batchfilevalidationtickets = new TMBatchFileValidationTickets();                        
                        $batchfilevalidationtickets->InsertMultiple($arrEntries);
                        //$batchfilevalidationtickets->TempForceUpdateGameNumber();
                        if ($batchfilevalidationtickets->HasError)
                        {
                            $errormsg = "An error occured while uploading. Please try again.";
                            $errortitle = "Error!";
                        }
                        else
                        {
                            $batchfilevalidationtickets->UpdateTicketsGameID();
                            if ($batchfilevalidationtickets->HasError)
                            {
                                $errormsg = "An error occured while uploading. Please try again.";
                                $errortitle = "Error!";
                            }
                            else
                            {
                                $batchfilevalidationtickets->ValidateTickets();
                                if ($batchfilevalidationtickets->HasError)
                                {
                                    $errormsg = "An error occured while uploading. Please try again.";
                                    $errortitle = "Error!";
                                }
                                else
                                {
                                    $batchfilevalidation->EndValidation($bfvid);
                                    if ($batchfilevalidation->HasError)
                                    {
                                        $errormsg = "An error occured while uploading. Please try again.";
                                        $errortitle = "Error!";
                                    }
                                    else
                                    {
                                        $uploadmsg = "Batch upload validation is successful. <br />Your batch session ID is <br /><b>$bfvid</b>.<br /><br />Please take note of this value as reference to this session.";
                                        $uploadmsgtitle = "Successful!";
                                    }
                                }
                            }
                        }
                        
                        
                        
                    }
		}
		else
		{
                    $errormsg = "Incorrect file format. Please upload a valid file format.";
		    $errormsgtitle = "ERROR!";
		}
	}

    if ($btnSearchByID->SubmittedValue == "Search By ID" && $txtSearchByID->Text != '')
    {
        $bfvid = $txtSearchByID->Text;
    }

    if ($bfvid != '')
    {
        $arrSessionData = $batchfilevalidationtickets->SelectByValidationSessionID($bfvid);
        if (is_array($arrSessionData))
        {
            for ($i = 0; $i < count($arrSessionData); $i++)
            {
                $sessiondata = $arrSessionData[$i];
                if ($sessiondata["Status"] == 0)
                {
                    $sessiondata["StatusString"] = "VIRN does not exist";
                }

                if ($sessiondata["Status"] == 1)
                {
                    $sessiondata["StatusString"] = "Valid";
                }
                if ($sessiondata["Status"] == 2)
                {
                    $sessiondata["StatusString"] = "Invalid";
                }
                $arrSessionData[$i] = $sessiondata;
            }
            $chs = null;
            $dth = null;
            $dic = null;
            $dtf = null;
            $chs[] = new ColumnHeader('Game Number', 1, 'GameNumber');
            $chs[] = new ColumnHeader('VIRN', 1, 'VIRN');
            $chs[] = new ColumnHeader('StatusString', 1, 'StatusString');
            $dth[] = new DataTableHeader($chs, 'bg_tableheader2');
            $dic[] = new DataItemColumn('', '', "GameNumber", '', '', '', false, false);
            $dic[] = new DataItemColumn('', '', "VIRN", '', '', '', false, false);
            $dic[] = new DataItemColumn('', '', "StatusString", '', '', '', false, false);

            $dtf[] = new DataTableFooter($dic, '', 'font-weight:bold;');

            $dtable = new DataTable();
            $dtable->DataTableHeaders = $dth;
            $dtable->DataItemColumns = $dic;
            $dtable->AlternatingClass = "bg_alternate";
            $dtable->Class = "reporttable twothirdpadded";
            $dtable->DataItems = $arrSessionData;
            $dtable->SelectIdentity = "SubMenuID";


            $subjtable = $dtable->Render();
        }
    }
}
?> 
