<?php
/*
 * Created by Arlene R. Salazar on 01-24-2012
 * Purpose: controller for batch redemption
 */
$pagesubmenuid = 41;
$stylesheets[] = "css/default.css";
$javascripts[] = "jscripts/validations.js";
$javascripts[] = "jscripts/jquery-1.5.2.min.js";

App::LoadModuleClass("TicketManagementCM", "TMAccounts");
App::LoadModuleClass("TicketManagementCM", "TMProducts");
App::LoadModuleClass("TicketManagementCM", "TMWinnings");
App::LoadModuleClass("TicketManagementCM", "TMGameManagement");
App::LoadModuleClass("TicketManagementCM", "TMBatchValidationTickets");
App::LoadModuleClass("TicketManagementCM", "TMBatchValidationSession");
App::LoadModuleClass("TicketManagementCM", "TMGameBatches");
App::LoadModuleClass("TicketManagementCM", "TMBatchTransactionLog");
App::LoadModuleClass("TicketManagementCM", "TMDecks");
App::LoadModuleClass("TicketManagementCM", "TMTickets");

App::LoadModuleClass("TicketManagementCM","Pdf");
//App::LoadCore("Pdf.class.php");
App::LoadLibrary("fpdf/fpdf.php");

App::LoadControl("ComboBox");
App::LoadControl("Button");
App::LoadControl("Hidden");
App::LoadControl("PagingControl2");

$batchredemptionform = new FormsProcessor();
$tmaccounts = new TMAccounts();
$tmproducts = new TMProducts();
$tmwinnings = new TMWinnings();
$tmgamemngt = new TMGameManagement();
$tmbatchvalidationtickets = new TMBatchValidationTickets();
$tmbatchvalidationsession = new TMBatchValidationSession();
$tmgamebatches = new TMGameBatches();
$tmbatchtransactionlog = new TMBatchTransactionLog();
$tmdecks = new TMDecks();
$tmtickets = new TMTickets();

$incentive = 2;

$ddlUsername = new ComboBox("ddlUsername","ddlUsername","Username");
$options = null;
$options[] = new ListItem("Please Select", "0");
$ddlUsername->Items = $options;
$arrRetailers = $tmaccounts->SelectAllActiveRetailer();
$retailer_list = new ArrayList();
$retailer_list->AddArray($arrRetailers);
$ddlUsername->DataSource = $retailer_list;
$ddlUsername->DataSourceText = "UserName";
$ddlUsername->DataSourceValue = "AID";
$ddlUsername->DataBind();

$ddlGameName = new ComboBox("ddlGameName","ddlGameName","Game Name");
$ddlGameName->Args = "onchange = 'javascript: get_gamenumber();'";
$gamenameopt = null;
$gamenameopt[] = new ListItem("Please select", "0", true);
$ddlGameName->Items = $gamenameopt;
$arrGameNames = $tmproducts->SelectAllGameName();
$gamename_list = new ArrayList();
$gamename_list->AddArray($arrGameNames);
$ddlGameName->DataSource = $gamename_list;
$ddlGameName->DataSourceText = "ProductName";
$ddlGameName->DataSourceValue = "ProductID";
$ddlGameName->DataBind();

$ddlGameNum = new ComboBox("ddlGameNum","ddlGameNum","Game Number");
$gamenumopt = null;
$gamenumopt[] = new ListItem("Please select", "0", true);
$ddlGameNum->Items = $gamenameopt;
$ddlGameNum->Args = "onchange='javascript:  get_gamebatches();get_gamename();'";
$allgames = $tmgamemngt->SelectAllActive();
$gameslist = new ArrayList();
$gameslist->AddArray($allgames);
$ddlGameNum->DataSource = $gameslist;
$ddlGameNum->DataSourceText = "GameNumber";
$ddlGameNum->DataSourceValue = "GameID";
$ddlGameNum->DataBind();

$btnSubmit = new Button("btnSubmit","btnSubmit","Submit");
$btnSubmit->IsSubmit = true;
$btnSubmit->Args = "onclick = 'return checkCommissionSummary();'";

$btnClaim =  new Button("btnClaim","btnClaim","Claim");
$btnClaim->IsSubmit = true;

$hiddengameid = new Hidden("hiddengameid","hiddengameid","Hidden Game ID");
$hiddenprodid = new Hidden("hiddenprodid","hiddenprodid","Hidden Product ID");
$hiddenuserid = new Hidden("hiddenuserid","hiddenuserid","Hidden User ID");
$hiddenbatchvalidationid = new Hidden("hiddenbatchvalidationid","hiddenbatchvalidationid","Hidden Batch Validation ID");
$hiddenbatchid = new Hidden("hiddenbatchid","hiddenbatchid","Hidden Batch ID");
$hiddenprocessedtag = new Hidden("hiddenprocessedtag","hiddenprocessedtag","Hidden processed tag");
$hiddenprocessedtag->Text = 2;
$hiddenflag = new Hidden("hiddenflag","hiddenflag","Hidden Flag");

$ddlBatchID = new ComboBox("ddlBatchID","ddlBatchID","Game Batch");
$gamebatchopt = null;
$gamebatchopt[] = new ListItem("Please select", "", true);
$ddlBatchID->Items = $gamebatchopt;

$btnConfirmClaimTickets = new Button("btnConfirmClaimTickets","btnConfirmClaimTickets","Claim");
$btnConfirmClaimTickets->IsSubmit = true;

$itemsperpage = 50;
$pgcon = new PagingControl2($itemsperpage, 1);
$pgcon->URL = "javascript:ChangePage(%currentpage);";
$pgcon->ShowMoveToFirstPage = true;
$pgcon->ShowMoveToLastPage = true;
$pgcon->PageGroup = 5;

$btnExport = new Button("btnExport","btnExport","Export to PDF");
$btnExport->IsSubmit = true;

if(isset($_SESSION['prodid']))
{
    $ddlUsername->SetSelectedValue($_SESSION['acctid']);
    $ddlGameName->SetSelectedValue($_SESSION['prodid']);

    $hiddengameid->Text = $_SESSION['gameid'];
    $hiddenprodid->Text = $_SESSION['prodid'];
    $hiddenuserid->Text = $_SESSION['acctid'];
    $hiddenbatchid->Text = $_SESSION['batchid'];

    $gamenum_perproduct = $tmproducts->SelectGameNumPerGameName($_SESSION['prodid']);
    $gamenum_perproduct_list = new ArrayList();
    $gamenum_perproduct_list->AddArray($gamenum_perproduct);
    $ddlGameNum->DataSource = $gamenum_perproduct_list;
    $ddlGameNum->DataSourceText = "GameNumber";
    $ddlGameNum->DataSourceValue = "GameID";
    $ddlGameNum->DataBind();
    $ddlGameNum->SetSelectedValue($_SESSION['gameid']);

    $ddlBatchID->ClearItems();
    $gamebatchopt = null;
    $gamebatchopt[] = new ListItem("ALL", "0", true);
    $ddlBatchID->Items = $gamebatchopt;
    $gamebatches = $tmproducts->SelectBatchIDPerGameNum($_SESSION['gameid'],2);
    $gamebatches_list = new ArrayList();
    $gamebatches_list->AddArray($gamebatches);
    $ddlBatchID->DataSource = $gamebatches_list;
    $ddlBatchID->DataSourceText = "BatchID";
    $ddlBatchID->DataSourceValue = "GameBatchID";
    $ddlBatchID->DataBind();
    $ddlBatchID->SetSelectedValue($_SESSION['batchid']);

    $batchredemption = $tmwinnings->GetBatchRedemptionPerBatchWithLimit($_SESSION['acctid'], $_SESSION['gameid'], $_SESSION['prodid'], ($pgcon->SelectedItemFrom - 1), $itemsperpage , 0, 0 ,1 , $_SESSION['batchid']);
    $batchredemptionlist = new ArrayList();
    $batchredemptionlist->AddArray($batchredemption);
    //$totalprizesredeemed = $tmwinnings->GetTotalRedemptionCommision($acctid, $gameid, $prodid , 0, 0);
    $recordcount_array = $tmwinnings->GetBatchRedemptionPerBatch($_SESSION['acctid'], $_SESSION['gameid'], $_SESSION['prodid'] , 0, 0 ,1 , $_SESSION['batchid']);

    $recordcount = count($recordcount_array);
    $pgcon->Initialize($itemsperpage, $recordcount);
    $pgTransactionHistory = $pgcon->PreRender();
}

$batchredemptionform->AddControl($ddlUsername);
$batchredemptionform->AddControl($ddlGameName);
$batchredemptionform->AddControl($ddlGameNum);
$batchredemptionform->AddControl($btnSubmit);
$batchredemptionform->AddControl($hiddengameid);
$batchredemptionform->AddControl($hiddenprodid);
$batchredemptionform->AddControl($hiddenuserid);
$batchredemptionform->AddControl($hiddenbatchvalidationid);
$batchredemptionform->AddControl($btnClaim);
$batchredemptionform->AddControl($ddlBatchID);
$batchredemptionform->AddControl($btnConfirmClaimTickets);
$batchredemptionform->AddControl($hiddenprocessedtag);
$batchredemptionform->AddControl($hiddenbatchid);
$batchredemptionform->AddControl($hiddenflag);
$batchredemptionform->AddControl($btnExport);

$batchredemptionform->ProcessForms();

unset($_SESSION['gameid']);
unset($_SESSION['prodid']);
unset($_SESSION['acctid']);
unset($_SESSION['batchid']);

if($batchredemptionform->IsPostBack)
{
    if($btnSubmit->SubmittedValue == "Submit")
    {
        $pgcon->SelectedPage = 1;
    }

    
    $acctid = $ddlUsername->SubmittedValue;
    $prodid = $ddlGameName->SubmittedValue;
    $gameid = $ddlGameNum->SubmittedValue;
    $batchid = $ddlBatchID->SubmittedValue;
    $hiddengameid->Text = $gameid;
    $hiddenprodid->Text = $prodid;
    $hiddenuserid->Text = $acctid;
	$hiddenbatchid->Text = $batchid;

	if($hiddenflag->SubmittedValue == 1)
    {
        $ddlGameNum->Args = "onchange='javascript: get_gamebatches(); '";
        $ddlGameNum->ClearItems();

        $gamenumbers = $tmproducts->SelectGameNumPerGameName($prodid);

        $gamenumbers_list1 = new ArrayList();
        $gamenumbers_list1->AddArray($gamenumbers);

        $litemgmetype1 = null;
        $litemgmetype1[] = new ListItem("Please select", "0", false);
        $ddlgametype->Items = $litemgmetype1;

        $ddlGameNum->ClearItems();
        $gamenameopt = null;
        $gamenameopt[] = new ListItem("Please select", "0", true);
        $ddlGameNum->Items = $gamenameopt;
        $gamenum_perproduct = $tmproducts->SelectGameNumPerGameName($prodid);
        $gamenum_perproduct_list = new ArrayList();
        $gamenum_perproduct_list->AddArray($gamenum_perproduct);
        $ddlGameNum->DataSource = $gamenum_perproduct_list;
        $ddlGameNum->DataSourceText = "GameNumber";
        $ddlGameNum->DataSourceValue = "GameID";
        $ddlGameNum->DataBind();
        $ddlGameNum->SetSelectedValue($gameid);
    }

    if($hiddenflag->SubmittedValue == 2)
    {
        $ddlGameName->Args = "";
        $ddlGameName->ClearItems();

        $gamenames = $tmproducts->SelectGameNamePerGameNum($gameid);

        $gamename_list1 = new ArrayList();
        $gamename_list1->AddArray($gamenames);
        $litemgmetype1 = null;
        $litemgmetype1[] = new ListItem("Please select", "0", false);
        $ddlGameName->Items = $litemgmetype1;

        $ddlGameName->DataSource = $gamename_list1;
        $ddlGameName->DataSourceText = "ProductName";
        $ddlGameName->DataSourceValue = "ProductID";
        $ddlGameName->DataBind();

        $ddlGameName->SetSelectedValue($prodid);
    }

    $ddlBatchID->ClearItems();
    $gamebatchopt = null;
    $gamebatchopt[] = new ListItem("ALL", "0", true);
    $ddlBatchID->Items = $gamebatchopt;
    $gamebatches = $tmproducts->SelectBatchIDPerGameNum($gameid,2);
    $gamebatches_list = new ArrayList();
    $gamebatches_list->AddArray($gamebatches);
    $ddlBatchID->DataSource = $gamebatches_list;
    $ddlBatchID->DataSourceText = "BatchID";
    $ddlBatchID->DataSourceValue = "GameBatchID";
    $ddlBatchID->DataBind();
    $ddlBatchID->SetSelectedValue($batchid);

    $batchredemption = $tmwinnings->GetBatchRedemptionPerBatchWithLimit($acctid, $gameid, $prodid, ($pgcon->SelectedItemFrom - 1), $itemsperpage , 0, 0 ,1 , $batchid);
    $batchredemptionlist = new ArrayList();
    $batchredemptionlist->AddArray($batchredemption);
    //$totalprizesredeemed = $tmwinnings->GetTotalRedemptionCommision($acctid, $gameid, $prodid , 0, 0);
    $recordcount_array = $tmwinnings->GetBatchRedemptionPerBatch($acctid, $gameid, $prodid , 0, 0 ,1 , $batchid);

    $recordcount = count($recordcount_array);
    $pgcon->Initialize($itemsperpage, $recordcount);
    $pgTransactionHistory = $pgcon->PreRender();

    if($batchredemptionform->GetPostVar("btnClaimTickets"))
    {
        $claimtickets = "ok";
    }

    if($btnConfirmClaimTickets->SubmittedValue == "Claim")
    {
        $batchvalidationid = $hiddenbatchvalidationid->Text;
        $gameid = $hiddengameid->Text;
        $prodid = $hiddenprodid->Text;
        $userid = $hiddenuserid->Text;
        $status = 1;
        $userdtls = $tmaccounts->SelectByID($userid);
        $username = $userdtls[0]["UserName"];
        $gamedtls = $tmproducts->SelectGameNameByGameID($gameid);
        $gamename = $gamedtls[0]["ProductName"];
        $gamenum = $gamedtls[0]["GameNumber"];
        
        //total prizes redeemed & commission details
        $prizesredeemed = $tmwinnings->GetTotalPrizesRedeemedPerBatchID($gameid,$prodid,$userid,$batchvalidationid, 0, 0);
        $total_prizes_redeemed = $prizesredeemed[0]["Total"];
        $total_redemption_comm = ($total_prizes_redeemed * ($incentive / 100));

        $winningcards = $tmbatchvalidationtickets->SelectWinningCardDtlsPerSession($batchvalidationid);
        if(count($winningcards) > 0)
        {
            //update batchvalidationticket status
            $tmbatchvalidationtickets->UpdateTicketValidationStatus($batchvalidationid,2);
            if($tmbatchvalidationtickets->HasError)
            {
                $errortitle = "ERROR";
                $errormsg = "Error has occured: " . $tmbatchvalidationtickets->getErrors();
                $status = 2;
            }

            //update winningcard status
            for($i = 0 ; $i < count($winningcards) ; $i++)
            {
                $winningcardid_array[] = $winningcards[$i]['WinningCardID'];
            }
            $winningcardid = "'" . implode("','",$winningcardid_array) . "'";

            $tmwinnings->UpdateWinningCardStatus($winningcardid,2,$userid);
            if($tmwinnings->HasError)
            {
                $errortitle = "ERROR";
                $errormsg = "Error has occured: " . $tmwinnings->getErrors();
                $status = 2;
            }

            //update batchvalidationsession status
            $sessionvalidation["Status"] = 3;
            $sessionvalidation["BatchValidationSessionID"] = $batchvalidationid;
            $tmbatchvalidationsession->UpdateByArray($sessionvalidation);
            if($tmbatchvalidationsession->HasError)
            {
                $errortitle = "ERROR";
                $errormsg = "Error has occured: " . $tmbatchvalidationsession->getErrors();
                $status = 2;
            }
            //update deck info

            //insert batch validation logs
            $gamebatchnum = $tmgamebatches->SelectByID($batchid);
            $batchtranslog["GameID"] = $gameid;
            $batchtranslog["BatchID"] = $gamebatchnum[0]['BatchID'];
            $batchtranslog["TransactionDate"] = 'now_usec()';
            $batchtranslog["TransactionType"] = 4;
            $batchtranslog["ProcessedByAID"] = $_SESSION['acctid'];
            $batchtranslog["Status"] = $status;
            $tmbatchtransactionlog->Insert($batchtranslog);
            if($tmbatchtransactionlog->HasError)
            {
                $errortitle = "ERROR!";
                $errormsg = "Error has occurred : " . $tmbatchtransactionlog->getError();
            }

            //insert audit logs
            $gamebatchnum = $tmgamebatches->SelectByID($batchid);
            $batchtranslog["GameID"] = $gameid;
            $batchtranslog["BatchID"] = $gamebatchnum[0]['BatchID'];
            $batchtranslog["TransactionDate"] = 'now_usec()';
            $batchtranslog["TransactionType"] = 4;
            $batchtranslog["ProcessedByAID"] = $_SESSION['acctid'];
            $batchtranslog["Status"] = $status;
            $tmbatchtransactionlog->Insert($batchtranslog);
            if($tmbatchtransactionlog->HasError)
            {
                $errortitle = "ERROR!";
                $errormsg = "Error has occurred : " . $tmbatchtransactionlog->getError();
            }

            //update ticket status
            if(count($winningcards) > 0)
            {
                for($i = 0 ; $i < count($winningcards) ; $i++)
                {
                    $book = trim($winningcards[$i]["TicketNumber"]);
                    $ticketno = substr($book,strlen($book) - 3,3);
                    $book = substr($book, strlen($book) - 9 ,6);

                    $arrBookDtls = $tmdecks->IsBookNoValid($book,$gameid);
                    if(count($arrBookDtls) > 0)
                    {
                        $bookid = $arrBookDtls[0]["BookID"];
                        $updTicket = $tmtickets->UpdateTicketStatus($bookid, $ticketno);
                        if($tmtickets->HasError)
                        {
                            $errortitle = "ERROR!";
                            $errormsg = "Error has occurred : " . $tmtickets->getError();
                        }
                    }
                }
            }

            //success message
            if(!isset($errormsg))
            {
                $successmsg = "Retailer Username : " . $username . "<br/>".
                    "Date and Time : ".date("m/d/Y h:i a")."<br/>".
                    "<b>Claimed Tickets</b><br/>".
                    "Game Number : " . $gamenum . "<br/>".
                    "Game Name : " . $gamename . "<br/>".
                    "Total Prizes Redeemed : $".number_format($total_prizes_redeemed , 2 , "." , ",")."<br/>".
                    "Total Redemption Commission : $".number_format($total_redemption_comm , 2 , "." , ",")."<br/>";
                $successtitle = "SUCCESS";
            }
        }
    }

	if($btnExport->SubmittedValue == "Export to PDF")
    {   
        $pdf = new PDF('L','mm','Legal');
        $pdf->AddPage();
        $pdf->AliasNbPages();

        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(0,5,"Batch Redemption",0,0,'C');
        $pdf->Ln(8);

        if(count($recordcount_array) > 0)
        {
            for($i = 0 ; $i < count($recordcount_array) ; $i++)
            {
                $pdf->SetFont('Arial','B',9);
                $pdf->SetX(10);
                $pdf->Cell(20,5,'Game Batch',1,0,'C');
                $pdf->SetX(30);
                $pdf->Cell(25,5,'Username',1,0,'C');
                $pdf->SetX(55);
                $pdf->Cell(35,5,'Date & Time Validated',1,0,'C');
                $pdf->SetX(90);
                $pdf->Cell(50,5,'Total Redemption Commission',1,0,'C');
                $pdf->SetX(140);
                $pdf->Cell(40,5,'Date & Time Claimed',1,0,'C');
                $pdf->SetX(180);
                $pdf->Cell(30,5,'Game Number',1,0,'C');
                $pdf->SetX(210);
                $pdf->Cell(30,5,'Game Name',1,0,'C');
                $pdf->SetX(240);
                $pdf->Cell(40,5,'Book Ticket Number',1,0,'C');
                $pdf->SetX(280);
                $pdf->Cell(40,5,'Validation Number',1,0,'C');
                $pdf->SetX(320);
                $pdf->Cell(30,5,'Prize Amount',1,1,'C');

                $pdf->SetFont('Arial','',9);
                $pdf->SetX(10);
                $pdf->Cell(20,5,$recordcount_array[$i]["BatchID"],1,0,'C');
                $pdf->Cell(25,5,$recordcount_array[$i]["Username"],1,0,'C');
                $pdf->Cell(35,5,$recordcount_array[$i]["DateValidated"],1,0,'C');
                $pdf->Cell(50,5,"$" . number_format(($recordcount_array[$i]["Total"] * ($incentive / 100)) , 2 , "." , ","),1,0,'C');


                $prizesredeemed = $tmwinnings->GetRedemptionSummaryPerBatch2($recordcount_array[$i]["GameID"],$prodid,$acctid,$recordcount_array[$i]["BatchValidationSessionID"], 0, 0);
                $totalprizesredeemed = $tmwinnings->GetTotalPrizesRedeemedPerBatchID2($recordcount_array[$i]["GameID"],$prodid,$acctid,$recordcount_array[$i]["BatchValidationSessionID"], 0, 0);
                $total_prizes_redeemed = $totalprizesredeemed[0]["Total"];
                $total_redemption_comm = ($total_prizes_redeemed * ($incentive / 100));
                if(count($prizesredeemed) > 0)
                {

                    for($a = 0 ; $a < count($prizesredeemed) ; $a++)
                    {
                        $pdf->SetX(140);
                        $pdf->Cell(40,5,$prizesredeemed[$a]["DateClaimed"],1,0,'C');
                        $pdf->Cell(30,5,$prizesredeemed[$a]["GameNumber"],1,0,'C');
                        $pdf->Cell(30,5,$prizesredeemed[$a]["ProductName"],1,0,'C');
                        $pdf->Cell(40,5,$prizesredeemed[$a]["TicketNumber"],1,0,'C');
                        $pdf->Cell(40,5,$prizesredeemed[$a]["ValidationNumber"],1,0,'C');
                        $pdf->Cell(30,5,$prizesredeemed[$a]["PrizeName"],1,1,'R');

                    }

                    $pdf->SetX(140);
                    $pdf->SetFont('Arial','B',9);
                    $pdf->Cell(180,5,'Total Prizes Redeemed:',1,0,'C');
                    $pdf->Cell(30,5,"$" . number_format($total_prizes_redeemed , 2 , '.' , ','),1,1,'R');
                    $pdf->SetX(140);
                    $pdf->Cell(180,5,'Total Redemption Commission:',1,0,'C');
                    $pdf->Cell(30,5,"$" . number_format($total_redemption_comm , 2 , '.' , ','),1,1,'R');
                }
                else
                {
                    $pdf->SetX(140);
                    $pdf->Cell(200,5,'No records found',1,0,'C');
                }

                $pdf->Ln(8);
            }
        }
        else
        {
            $pdf->SetFont('Arial','',9);
            $pdf->SetX(10);
            $pdf->Cell(330,5,'No records found',0,1,'C');
        }

        $pdf->Output('Batch Redemption','D');
        Header('Content-Type: application/pdf');
    }
}
?>