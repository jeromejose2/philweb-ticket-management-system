<?php
/*
 * Created By: Noel Antonio 01/09/2012
 * Purpose: For Retailer Validation of Cards per Batch
 */

$pagesubmenuid = 38;
$stylesheets[] = "css/default.css";
$pagetitle = "Retailer Ticket Validation";

App::LoadModuleClass("TicketManagementCM", "TMTicketValidation");
App::LoadModuleClass("TicketManagementCM", "TMDecks");
App::LoadModuleClass("TicketManagementCM", "TMDeckInfo");
App::LoadModuleClass("TicketManagementCM", "TMAccounts");
App::LoadModuleClass("TicketManagementCM", "TMAuditLog");
App::LoadModuleClass("TicketManagementCM", "TMPrizes");
App::LoadModuleClass("TicketManagementCM", "TMProducts");
App::LoadModuleClass("TicketManagementCM", "TMGameManagement");
App::LoadModuleClass("TicketManagementCM", "TMTickets");
App::LoadModuleClass("TicketManagementCM", "TMBatchValidationSession");
App::LoadModuleClass("TicketManagementCM", "TMBatchValidationTickets");
App::LoadModuleClass("TicketManagementCM", "TMBatchFileValidationTickets");
App::LoadModuleClass("TicketManagementCM", "TMBatchTransactionLog");
App::LoadModuleClass("TicketManagementCM", "TMCurrencies");
App::LoadModuleClass("TicketManagementCM", "TMTicketCancellation");
App::LoadModuleClass("TicketManagementCM", "TMHotTickets");
App::LoadModuleClass("TicketManagementCM", "TMDeckStatusHistory");

App::LoadControl("TextBox");
App::LoadControl("Label");
App::LoadControl("ComboBox");
App::LoadControl("Button");
App::LoadControl("Hidden");

$currency = App::getParam('currency');

$fproc = new FormsProcessor();
$confirmation = 'false';
$prizetypeid = '0';
$accttypeid = $_SESSION['accttype'];

$tmbatchfilevalidationtickets = new TMBatchFileValidationTickets();
$tmbatchvalidationsession = new TMBatchValidationSession();
$tmbatchvalidationtickets = new TMBatchValidationTickets();
$tmbatchtransactionlog = new TMBatchTransactionLog();
$tmticketvalidation = new TMTicketValidation();
$tmdecks = new TMDecks();
$tmdeckinfo = new TMDeckInfo();
$tmaccount = new TMAccounts();
$tmauditlog = new TMAuditLog();
$tmproducts = new TMProducts();
$tmgamemgt = new TMGameManagement();
$tmtickets = new TMTickets();
$tmcurrencies = new TMCurrencies();
$tmdeckstatus = new TMDeckStatusHistory();

$ddlUsername = new ComboBox("ddlUsername", "ddlUsername", "Account Name: ");
$ddlUsername->ShowCaption = true;
$arraccounts = $tmaccount->SelectForValidationModule($_SESSION['acctid']);
$accountlist = new ArrayList();
$accountlist->AddArray($arraccounts);
$litem = null;
$litem[] = new ListItem("Please Select", "0", true);
$ddlUsername->Items = $litem;
$ddlUsername->DataSource = $accountlist;
$ddlUsername->DataSourceText = "UserName";
$ddlUsername->DataSourceValue = "AID";
$ddlUsername->DataBind();

$ddlgamename = new ComboBox("ddlgamename", "ddlgamename", "Game Name: ");
$ddlgamename->ShowCaption = true;
$ddlgamename->Args = "onchange = 'javascript: get_gamenumber();'";
$gamenames = $tmproducts->SelectAllGameName();
$gamename_list = new ArrayList();
$gamename_list->AddArray($gamenames);
$litem = null;
$litem[] = new ListItem("Please Select", "0", true);
$ddlgamename->Items = $litem;
$ddlgamename->DataSource = $gamename_list;
$ddlgamename->DataSourceText = "ProductName";
$ddlgamename->DataSourceValue = "ProductID";
$ddlgamename->DataBind();

$ddlgamenum = new ComboBox("ddlgamenum", "ddlgamenum", "Game Number: ");
$ddlgamenum->ShowCaption = true;
$ddlgamenum->Args = "onchange = 'javascript: get_gamename();'";
$gamenumbers = $tmgamemgt->SelectByGameType();
$gamename_list = new ArrayList();
$gamename_list->AddArray($gamenumbers);
$litem = null;
$litem[] = new ListItem("Please Select", "0", true);
$ddlgamenum->Items = $litem;
$ddlgamenum->DataSource = $gamename_list;
$ddlgamenum->DataSourceText = "GameNumber";
$ddlgamenum->DataSourceValue = "GameID";
$ddlgamenum->DataBind();

$ddlcardvalue = new ComboBox('ddlcardvalue','ddlcardvalue', 'Card Value: ');
$ddlcardvalue->ShowCaption = true;
$ddlcardvalue->Args = "onchange='javascript: get_gamename();'";
$cardprize = $tmgamemgt->SelectByCardValue();
$cardprize_list = new ArrayList();
$cardprize_list->AddArray($cardprize);
$cardlist = null;
$cardlist[] = new ListItem("Please select","0",false);
$ddlcardvalue->Items = $cardlist;
$ddlcardvalue->DataSource = $cardprize_list;
$ddlcardvalue->DataSourceText = "CardPrice";
$ddlcardvalue->DataSourceValue = "CardPrice";
$ddlcardvalue->DataBind();

$txtValidationNumber = new TextBox("txtValidationNumber", "txtValidationNumber", "Validation Number: ");
$txtValidationNumber->Args="onkeypress='javascript: return isNumberKey(event);' autocomplete='off'";
$txtValidationNumber->Length = 12;
$txtValidationNumber->ReadOnly = true;

$txtBookNumber = new TextBox("txtBookNumber", "txtBookNumber", "Book Number: ");
$txtBookNumber->Args="onkeypress='javascript: return isNumberKey(event);' autocomplete='off'";
$txtBookNumber->Length = 12;
$txtBookNumber->ReadOnly = true;

$btnCreateSession = new Button("btnCreateSession", "btnCreateSession", "Create Session");
$btnCreateSession->IsSubmit = true;
$btnCreateSession->Args="onclick='javascript: return checkcreatesession();'";

$btnCancelSession = new Button("btnCancelSession", "btnCancelSession", "Cancel");
$btnCancelSession->IsSubmit = true;

$btnAddToBatch = new Button("btnAddToBatch", "btnAddToBatch", "Add");
$btnAddToBatch->IsSubmit = true;
$btnAddToBatch->Args="onclick='javascript: return checkvalidation();'";
$btnAddToBatch->Enabled = false;

$btnValidate = new Button("btnValidate", "btnValidate", "YES");
$btnValidate->IsSubmit = true;

$btnFinalize = new Button("btnFinalize", "btnFinalize", "Claim and Reimburse");
$btnFinalize->IsSubmit = true;

$lbluserid = new Label("lbluserid", "lbluserid", "User");
$lbluserid->Style = "font-weight: bold; font-size: 12pt;";

$lblprodid = new Label("lblprodid", "lblprodid", "Product");
$lblprodid->Style = "font-weight: bold; font-size: 12pt;";

$lblgameid = new Label("lblgameid", "lblgameid", "Game Number");
$lblgameid->Style = "font-weight: bold; font-size: 12pt;";

$lblcardvalue = new Label("lblcardvalue", "lblcardvalue", "Card Value");
$lblcardvalue->Style = "font-weight: bold; font-size: 12pt;";

$flag = new Hidden("flag", "flag", "flag");

$bool_disable = false;

$fproc->AddControl($ddlUsername);
$fproc->AddControl($ddlgamename);
$fproc->AddControl($ddlgamenum);
$fproc->AddControl($ddlcardvalue);
$fproc->AddControl($txtValidationNumber);
$fproc->AddControl($txtBookNumber);
$fproc->AddControl($btnValidate);
$fproc->AddControl($btnAddToBatch);
$fproc->AddControl($btnCreateSession);
$fproc->AddControl($btnCancelSession);
$fproc->AddControl($btnFinalize);
$fproc->AddControl($lbluserid);
$fproc->AddControl($lblprodid);
$fproc->AddControl($lblgameid);
$fproc->AddControl($lblcardvalue);
$fproc->AddControl($flag);
$fproc->ProcessForms();

if ($accttypeid == 5 || $accttypeid == 6) {
    $ddlUsername->Enabled = false;
}

$arrcurr = $tmcurrencies->SelectByCurrencySymbol("KHR");
$fixedrate = $arrcurr[0]["FixedRate"];

$valTickets_list = new ArrayList();

/* IF SESSION & TICKETS ALREADY EXISTS **************************************************************************/
$aidhaspending = $tmbatchvalidationsession->HasPending($_SESSION['acctid']);
if (count($aidhaspending) >= 1)
{
    $_SESSION['batchvalidationsessionid'] = $aidhaspending[0]["BatchValidationSessionID"];
    $chkSession = $tmbatchvalidationtickets->checkCurrentSession($_SESSION['acctid']);
    if (count($chkSession) > 0)
    {
        $ddlcardvalue->SetSelectedValue($chkSession[0]["CardPrice"]);
        $ddlgamename->SetSelectedValue($chkSession[0]["ProductID"]);
        $ddlgamenum->SetSelectedValue($chkSession[0]["GameID"]);
        $ddlUsername->SetSelectedValue($chkSession[0]["AssignedToAID"]);
        
        $lblcardvalue->Text = $ddlcardvalue->SelectedText;
        $lblgameid->Text = $ddlgamenum->SelectedText;
        $lblprodid->Text = $ddlgamename->SelectedText;
        $lbluserid->Text = $ddlUsername->SelectedText;

        $arrValidTickets = $tmbatchvalidationtickets->loadCurrentSession($_SESSION['batchvalidationsessionid']);           
        $valTickets_list = new ArrayList();
        $valTickets_list->AddArray($arrValidTickets); 
        
        $bool_disable = true;
    }
}
else { $btnCancelSession->Enabled = false; }
/* IF SESSION & TICKETS ALREADY EXISTS **************************************************************************/

if ($fproc->IsPostBack)
{   
    $lblcardvalue->Text = $ddlcardvalue->SelectedText;
    $lblgameid->Text = $ddlgamenum->SelectedText;
    $lblprodid->Text = $ddlgamename->SelectedText;
    $lbluserid->Text = $ddlUsername->SelectedText;
    
    
/* CREATE SESSION ***********************************************************************************************/
   if ($btnCreateSession->SubmittedValue == "Create Session")
    {    
        // check and cancel existing session with no tickets added.
        $aidhaspending = $tmbatchvalidationsession->HasPending($_SESSION['acctid']);
        $chkSession = $tmbatchvalidationtickets->checkCurrentSession($_SESSION['acctid']);
        if ((count($aidhaspending) > 0) && (count($chkSession) <= 0))
        {
            $errormsg = "Your old session will be used.";
            $errormsgtitle = "SESSION";  
            $_SESSION['batchvalidationsessionid'] = $aidhaspending[0]["BatchValidationSessionID"];
        }
        else
        {
            $newsess["Status"] = 0;
            $newsess["DateCreated"] = 'now_usec()';
            $newsess["CreatedByAID"] = $_SESSION['acctid'];  
            $tmbatchvalidationsession->Insert($newsess);
            if ($tmbatchvalidationsession->HasError)
            {
                $errormsg = $tmauditlog->getError();
                $errormsgtitle = "ERROR!";
            }        
            $errormsg = "New Session was created.";
            $errormsgtitle = "SESSION";  
            $_SESSION['batchvalidationsessionid'] = $tmbatchvalidationsession->LastInsertID;
        }
        $bool_disable = true;
    }
/* CREATE SESSION ***********************************************************************************************/
   
    
    
/* CANCEL SESSION ***********************************************************************************************/    
    if ($btnCancelSession->SubmittedValue == "Cancel")
    {
        $result = $result = cancelsession($_SESSION['batchvalidationsessionid']);        
        $errormsg = "Current Session successfully cancelled";
        $errormsgtitle = "SESSION";        
        $bool_disable = false;
        unset($_SESSION['batchvalidationsessionid']);
        unset($arrValidTickets);
    }
/* CANCEL SESSION ***********************************************************************************************/
    
    
    
/* ADD TO BATCH *************************************************************************************************/    
    if ($btnAddToBatch->SubmittedValue == "Add")
    {
        $batchsessionid = $_SESSION['batchvalidationsessionid'];
        if (isset($_SESSION["tmpcardprice"]))
        {
            $tmpcardprice = $_SESSION["tmpcardprice"];
            $tmpgame = $_SESSION["tmpgame"];
            $tmpproduct = $_SESSION["tmpproduct"];
            $tmpuser = $_SESSION["tmpuser"];
        }
        else
        {
            $tmpcardprice = $ddlcardvalue->SubmittedValue;
            $tmpgame = $ddlgamenum->SubmittedValue;
            $tmpproduct = $ddlgamename->SubmittedValue;
            $tmpuser = $ddlUsername->SubmittedValue;
        }
        
        $fullvirn = trim(str_replace(array('\r', '\n', '\r\n'), "", $txtValidationNumber->SubmittedValue));
        $checksum = substr($fullvirn, strlen($fullvirn) - 1, strlen($fullvirn));
        $fullbook = trim(str_replace(array('\r', '\n', '\r\n'), "", $txtBookNumber->SubmittedValue));
        $gameno = substr($fullvirn,0,strlen($fullvirn) - 9);
        $valno = substr($fullvirn, 0,strlen($fullvirn) - 1);
        $valno2 = substr($valno, strlen($valno) - 8, strlen($valno));
        $ticketno = substr($fullbook,strlen($fullbook) - 3,3);
        $book = substr($fullbook, strlen($fullbook) - 9 ,6);
        $gameno1 = substr($fullbook,0,strlen($fullbook) - 9);
        $subvirn = $gameno;
        $subticket = $gameno1;
        

        if ($lblgameid->Text != $subticket)
        {
            $errormsg = "Game Number selected does not match with the ticket.";
            $errormsgtitle = 'ERROR!';
        } 
        else if ($lblgameid->Text != $subvirn)
        {
            $errormsg = "Game Number selected does not match with the VIRN number.";
            $errormsgtitle = 'ERROR!';
        }
        else 
        {
                $arrGame = $tmgamemgt->SelectByGameNumber($gameno);
                if (count($arrGame) == 1)
                {
                    $gameid = $arrGame[0]["GameID"];  
                    $bookticketcount = $arrGame[0]["BookTicketCount"];
                    // START CHECKING CHECK DIGIT
                    $checkdigit = CheckDigit($valno);
                    if($checkdigit % 10 != $checksum)
                    {
                        $errormsg = 'Please enter a valid VIRN.';
                        $errormsgtitle = 'ERROR!';
                    } 
                    else
                    {
                        // CHECK IF VALID VIRN
                        $arrWinning = $tmticketvalidation->IsValidVIRN($valno2,$gameid);
                        if (count($arrWinning) == 1)
                        {
                            $arrdtls = $arrWinning[0];
                            $winningcardid = $arrdtls["WinningCardID"];
                            $claimdate = $arrdtls["ClaimDate"];
                            $claimtime = $arrdtls["ClaimTime"];
                            $status = $arrdtls["Status"];
                            $prizetypeid = $arrdtls["PrizeTypeID"];

                            if ( $status == 5)  
                            {
                                $errormsgtitle = "";
                                $errormsg = "Scratch card prize has already been claimed last ".$claimdate." at ".$claimtime.".";
                            }
                            else
                            {
                                // CHECK IF CARD HAS MAJOR OR MINOR PRIZE
                                if ($prizetypeid == 1)
                                {
                                    $errormsgtitle = "";
                                    $errormsg = "Sorry, we cannot process the card. The scratch card is a major prize.";
                                }
                                else
                                {
                                  
                                    // START CHECKING IF EXISTING IN BATCH VALIDATION TABLE
                                $ticketExist =  $tmbatchvalidationtickets->IsExist($tmpgame, $tmpuser, trim(str_replace(array('\r', '\n', '\r\n'), "", $txtBookNumber->SubmittedValue)), trim(str_replace(array('\r', '\n', '\r\n'), "", $txtValidationNumber->SubmittedValue)), $_SESSION['acctid']); 
                                if ($ticketExist[0][0] >= 1)
                                {
                                    $errormsg = 'Ticket and VIRN already added in the batch.';
                                    $errormsgtitle = 'ERROR!';
                                }
                                else
                                { 
                                        // CHECK IF VALID BOOK NUMBER
                                        $arrBookDtls = $tmdecks->IsBookNoValid($book,$gameid);
                                        if (count($arrBookDtls) == 1)
                                        {
                                            $arrdtlsbooks = $arrBookDtls[0];
                                            $statusbooks = $arrdtlsbooks["Status"];
                                            $bookid = $arrdtlsbooks["BookID"];
                                            if ($statusbooks == 1 || $statusbooks == 5 || $statusbooks == 7 || $statusbooks == 8 || $statusbooks == 9 || $statusbooks == 10)
                                            {
                                                $where = "WHERE BookID='$bookid' ORDER BY DateChanged DESC LIMIT 1";
                                                $ds = $tmdeckstatus->SelectByWhere($where);
                                                $oldstatus = $ds[0]["OldStatus"];
                                                if (($oldstatus == 2 || $oldstatus == 3 || $oldstatus == 4) && ($statusbooks != 1)){
                                                    $errormsg = "Inactive tickets prior to cancellation are not allowed for validation.";
                                                    $errormsgtitle = 'ERROR!';
                                                }
                                                else
                                                {
                                                    // CHECK IF VALID TICKET NUMBER
                                                    $arrTicketDtls = $tmtickets->IsTicketNoValid($ticketno, $bookid);
                                                    if (count($arrTicketDtls) == 1)
                                                    {
                                                        $arrdtls2 = $arrTicketDtls[0];
                                                        $ticketid = $arrdtls2["TicketID"];
                                                        $ticketstatus = $arrdtls2["Status"];
                                                        if ($ticketstatus == 1 || $ticketstatus == 7 || $ticketstatus == 9 || $ticketstatus == 6)
                                                        {
                                                            $where = " WHERE WinningCardID = " . $winningcardid;
                                                            $arr_chk = $tmbatchvalidationtickets->SelectByWhere($where);
                                                            if (count($arr_chk) == 0)
                                                            {
                                                                    //insert to batchvalidationtickets table
                                                                    $insertvaltickets["GameID"] = $tmpgame;
                                                                    $insertvaltickets["TicketNumber"] = trim(str_replace(array('\r', '\n', '\r\n'), "", $txtBookNumber->SubmittedValue));
                                                                    $insertvaltickets["ValidationNumber"] = trim(str_replace(array('\r', '\n', '\r\n'), "", $txtValidationNumber->SubmittedValue));
                                                                    $insertvaltickets["WinningCardID"] = $winningcardid;
                                                                    $insertvaltickets["BatchValidationSessionID"] = $batchsessionid;
                                                                    $insertvaltickets["DateCreated"] = 'now_usec()';
                                                                    $insertvaltickets["AssignedToAID"] = $tmpuser;
                                                                    $insertvaltickets["Status"] = 0;
                                                                    $tmbatchvalidationtickets->Insert($insertvaltickets);
                                                            }
                                                            else
                                                            {
                                                                    // Overwrite the existing ticket with the current ticket.
                                                                    $newTicket = trim(str_replace(array('\r', '\n', '\r\n'), "", $txtBookNumber->SubmittedValue));
                                                                    $tmbatchvalidationtickets->UpdateExistingTicket($newTicket, $winningcardid, $batchsessionid, $tmpuser);
                                                            }

                                                            if ($tmbatchvalidationtickets->HasError){
                                                                $errormsgtitle = "ERROR!";
                                                                $errormsg = $tmbatchvalidationtickets->getErrors();
                                                            }                                                            

                                                        }
                                                        elseif ($ticketstatus == 5)
                                                        {
                                                            saveToHotTickets($ticketid, $_SESSION['acctid'], $fullvirn, $fullbook);
                                                            $errormsg = "Cannot process. This card/book has been declared void.";
                                                            $errormsgtitle = 'ERROR!';    
                                                        }
                                                        elseif ($ticketstatus == 6)
                                                        {
                                                            $errormsg = "Ticket was already claimed. Performing the ticket validation will not be allowed.";
                                                            $errormsgtitle = 'ERROR!';    
                                                        }
                                                        /*elseif ($ticketstatus == 7)
                                                        {
                                                            $confirmation = 'true';
                                                            $okay = 'false';
                                                            $errormsg = "This card has been reported stolen.Proceed to Claim?";
                                                            $errormsgtitle = 'ERROR!';    
                                                        }*/
                                                        elseif ($ticketstatus == 8)
                                                        {
                                                            saveToHotTickets($ticketid, $_SESSION['acctid'], $fullvirn, $fullbook);
                                                            $errormsg = "This card has been reported stolen and declared invalid for prize claiming.";
                                                            $errormsgtitle = 'ERROR!';    
                                                        }
                                                        /*elseif ($ticketstatus == 9)
                                                        {
                                                            $confirmation = 'true';
                                                            $okay = 'false';
                                                            $errormsg = "This card has expired. Proceed to Claim?";
                                                            $errormsgtitle = 'ERROR!';    
                                                        }*/
                                                        elseif ($ticketstatus == 10)
                                                        {
                                                            saveToHotTickets($ticketid, $_SESSION['acctid'], $fullvirn, $fullbook);
                                                            $errormsg = "This card has expired and declared invalid for prize claiming";
                                                            $errormsgtitle = 'ERROR!';    
                                                        }
                                                    } else {
                                                            // check for book ticket size
                                                            if ($ticketno > $bookticketcount){
                                                                $errormsg = "Ticket exceeds the maximum book ticket size.";
                                                                $errormsgtitle = 'ERROR!';
                                                            } else {
                                                                $errormsg = "Scratch card does not exist";
                                                                $errormsgtitle = 'ERROR!';    
                                                            }  
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                if ($statusbooks == 4)
                                                {
                                                    $errormsg = "Please enter a book ticket number that has already been activated."; //"Book number is assigned.";                    
                                                }
                                                elseif ($statusbooks == 2)
                                                {
                                                    $errormsg = "Please enter a book ticket number that has already been activated."; //"Book number is still on freight.";                    
                                                }
                                                elseif ($statusbooks == 3)
                                                {
                                                    $errormsg = "Please enter a book ticket number that has already been activated."; //"Book number is on stock.";                    
                                                }
                                                
                                                $errormsgtitle = 'ERROR!';  
                                            } // END OF CHECKING VALID TICKET NUMBER
                                        }
                                        else
                                        {            
                                            $errormsg = "Book number does not exist.";
                                            $errormsgtitle = 'ERROR!';                
                                        } // END OF CHECKING VALID BOOK NUMBER
                                    }// END OF CHECKING EXISTING BATCH TICKETS
                                } // END OF CHECKING IF MINOR OR MAJOR PRIZE    
                            } // END OF CHECKING IF CLAIMED OR UNCLAIMED
                        } 
                        else
                        {
                            $errormsg = "Sorry, your ticket is not a winning card.";
                            $errormsgtitle = 'ERROR!';
                        }  // END CHECKING IF VALID VIRN
                    } // END CHECKING CHECK DIGIT
                }
                else
                {
                    $errormsg = 'Game number does not exist.';
                    $errormsgtitle = 'ERROR!';
                } // END CHECKING OF GAME NUMBER 
        }  
        
        $arrValidTickets = $tmbatchvalidationtickets->loadCurrentSession($_SESSION['batchvalidationsessionid']);
        $valTickets_list = new ArrayList();
        $txtValidationNumber->Text = "";
        $txtBookNumber->Text = "";
        $bool_disable = true;	
    }
/* ADD TO BATCH *************************************************************************************************/    
    
    
    
/* CLAIM AND REIMBURSE ******************************************************************************************/    
    if ($btnFinalize->SubmittedValue == "Claim and Reimburse")
    {
        $batchsessionid = $_SESSION['batchvalidationsessionid'];        
        $confirmation = 'true';
        $okay = 'false';
        $errormsg = "Would you like to process this batch?";
        $errormsgtitle = "CONFIRMATION";        
        $arrValidTickets = $tmbatchvalidationtickets->loadCurrentSession($_SESSION['batchvalidationsessionid']);
        $valTickets_list = new ArrayList();
        $bool_disable = true;
    }
/* CLAIM AND REIMBURSE ******************************************************************************************/    
    

    
/* YES BUTTON - FINALIZATION ************************************************************************************/        
    if ($btnValidate->SubmittedValue == "YES")
    {
        $arrBatchTickets = $tmbatchvalidationtickets->SelectAllNonValidatedTickets($_SESSION['acctid'], $_SESSION['batchvalidationsessionid']);
        $arrBatch_List = new ArrayList();
        $arrBatch_List->AddArray($arrBatchTickets);
     
        for ($ctr=0;$ctr < count($arrBatch_List);$ctr++)
        {
                $batchlistaccname = $arrBatch_List[$ctr]["AssignedToAID"];
                $batchlistbooknum = $arrBatch_List[$ctr]["TicketNumber"];
                $batchlistvalnum = $arrBatch_List[$ctr]["ValidationNumber"];
                $batchwinningcardid = $arrBatch_List[$ctr]['WinningCardID'];
                $gameid = $arrBatch_List[$ctr]['GameID'];
                $batchgameid = $arrBatch_List[$ctr]['GameBatchID'];
                $batchvalidationticketID = $arrBatch_List[$ctr]['BatchValidationTicketID'];
                $batchvalidationsessionID = $arrBatch_List[$ctr]['BatchValidationSessionID'];
                $batchlistprizename = $arrBatch_List[$ctr]['PrizeName'];
                
                if (strpos($batchlistprizename, 'KHR') !== false)
                {
                    $str_prize = preg_replace('/[\KHR,]/', '', $batchlistprizename);
                    $batchlistprizename = $str_prize / $fixedrate;
                }
                else if (strpos($batchlistprizename, $currency) !== false)
                {
                    $batchlistprizename = preg_replace('/[\"'.$currency.'",]/', '', $batchlistprizename);
                }

                $unameid = $batchlistaccname;
                $valno = trim(str_replace(array('\r', '\n', '\r\n'), "", $batchlistvalnum));
                $book = trim(str_replace(array('\r', '\n', '\r\n'), "", $batchlistbooknum));
                $winningcardid = $batchwinningcardid;

                $gameno = substr($valno,0,strlen($valno) - 9);
                $valno = substr($valno, 0,strlen($valno) - 1);
                $valno2 = substr($valno, 4, 8);
                $ticketno = substr($book,strlen($book) - 3,3);
                $book = substr($book, strlen($book) - 9 ,6);

                $arrGame = $tmgamemgt->SelectByGameNumber($gameno);
                $gameid = $arrGame[0]["GameID"];
                $arrBookDtls = $tmdecks->IsBookNoValid($book,$gameid);
                $bookid = $arrBookDtls[0]["BookID"];

                
                // UPDATE WINNING CARDS
                if ($unameid > 0)
                {
                    $tmparam["ClaimedByAID"] = $unameid; 
                    $claimby = $unameid; 
                    $remarks = "Claiming of AcctID ".$_SESSION['acctid']." overridden by AcctID ".$unameid;
                }
                else
                {
                    $tmparam["ClaimedByAID"] = $_SESSION['acctid'];
                    $claimby = $_SESSION['acctid'];
                    $remarks = "";
                }       
                
                $tmticketvalidation->UpdateWinningCards($winningcardid, $book, 5, 'now_usec()', $claimby, $remarks);
                if ($tmticketvalidation->HasError)
                {
                    $errormsg = $tmticketvalidation->getError();
                    $errormsgtitle = "ERROR!";
                }

                // Update Batch FILE Tickets to (4) - REIMBURSED
                $tmbatchfilevalidationtickets->UpdateTicketsToReimbursed($valno2, $tmparam["ClaimedByAID"]);

                // UPDATE DECK INFO
                $arrdeckinfo = $tmdeckinfo->GetDeckInfoByGameBatchID($batchgameid);
                if (count($arrdeckinfo) == 1)
                {
                    $arrdtls = $arrdeckinfo[0];
                    $usedwinningcardcount = $arrdtls["UsedWinningCardCount"];
                    $claimedwinningcardcount = $arrdtls["ClaimedWinningCardCount"];
                    $deckid = $arrdtls["DeckID"];
                    $usedwinningcardcount += 1;
                    $claimedwinningcardcount += 1;
                    $tmdeckinfoparam["UsedWinningCardCount"] = $usedwinningcardcount; 
                    $tmdeckinfoparam["ClaimedWinningCardCount"] = $claimedwinningcardcount; 
                    $tmdeckinfoparam["DeckID"] = $deckid;
                    $tmdeckinfo->UpdateByArray($tmdeckinfoparam);
                    if ($tmdeckinfo->HasError)
                    {
                        $errormsg = $tmdeckinfo->getError();
                        $errormsgtitle = "ERROR!";
                    }
                }

                // UPDATE BATCH VALIDATION TICKETS
                $updBatchTicket["BatchValidationTicketID"] = $batchvalidationticketID;
                $updBatchTicket["Status"] = 2; // Processed
                $tmbatchvalidationtickets->UpdateByArray($updBatchTicket);
                if ($tmbatchvalidationtickets->HasError)
                {
                    $errormsg = $tmbatchvalidationtickets->getError();
                    $errormsgtitle = "ERROR!";
                }

                // Update Batch Tickets to (2) - PROCESSED
                $tmbatchvalidationtickets->UpdateProcessedTickets($batchlistbooknum, $batchlistvalnum);

                // Update Tickets to (6) - CLAIMED
                $tmtickets->UpdateTicketStatus($bookid, $ticketno);

                // INSERT TO AUDIT TRAIL LOGS              
                $tmaudit["SessionID"] = $_SESSION['sid'];
                $tmaudit["AID"] = $_SESSION['acctid'];
                $tmaudit["TransDetails"] = 'Batch Session ID: '.$_SESSION['batchvalidationsessionid'];
                $tmaudit["TransDateTime"] = 'now_usec()';
                $tmaudit["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
                $tmaudit["AuditTrailFunctionID"] = '37';    
                $tmauditlog->Insert($tmaudit);
                if ($tmauditlog->HasError)
                {
                    $errormsg = $tmauditlog->getError();
                    $errormsgtitle = "ERROR!";
                }

                //$_SESSION['batchvalidationsessionid'] = $batchvalidationsessionID;
                $_SESSION['gameid'] = $gameid;  
                $totalprizeredeemed = $totalprizeredeemed + $batchlistprizename;
                $commission = $totalprizeredeemed * 0.02;
                $_SESSION['totalprizeredeemed'] = $totalprizeredeemed;
                $_SESSION['commission'] = $commission;
        } // END OF BATCH CLAIMING (LOOP)
        
        
        // Update Batch Validation Session to Closed/Finalized
        $updBatchSession["BatchValidationSessionID"] = $_SESSION['batchvalidationsessionid'];
        $updBatchSession["Status"] = 3;
        $tmbatchvalidationsession->UpdateByArray($updBatchSession);
        if ($tmbatchvalidationsession->HasError)
        {
            $errormsg = $tmbatchvalidationsession->getError();
            $errormsgtitle = "ERROR!";
        }

        // Insert to Batch Validation Logs
        $insBatchLogs["GameID"] = $_SESSION['gameid'];
        $insBatchLogs["BatchID"] = $_SESSION['batchvalidationsessionid'];;
        $insBatchLogs["TransactionDate"] = 'now_usec()';
        $insBatchLogs["TransactionType"] = 2;
        $insBatchLogs["ProcessedByAID"] = $_SESSION['acctid'];
        $insBatchLogs["Status"] = 1;
        $tmbatchtransactionlog->Insert($insBatchLogs);
        if ($tmbatchtransactionlog->HasError)
        {
            $errormsg = $tmbatchtransactionlog->getError();
            $errormsgtitle = "ERROR!";
        }

        $okay = 'true';
        $errormsg = 'Retailer Username : '.$ddlUsername->SelectedText.'<br/>'.
                    'Date and Time : '.date("m/d/Y h:i a").'<br/>'.
                    '<b>Claimed Tickets</b><br/>'.
                    'Game Number : '.$ddlgamenum->SelectedText.'<br/>'.
                    'Game Name : '.$ddlgamename->SelectedText.'<br/>'.
                    'Total Prizes Redeemed : ' . $currency . " " . number_format($_SESSION['totalprizeredeemed'] , 2 , "." , ",").'<br/>'.
                    'Total Redemption Commission : ' . $currency . " " . number_format($_SESSION['commission'] , 2 , "." , ",").'<br/>';
        $errormsgtitle = 'SUMMARY';
        
        $arrValidTickets = $tmbatchvalidationtickets->loadCurrentSession($_SESSION['batchvalidationsessionid']);
        $valTickets_list = new ArrayList();
        $bool_disable = false;
        unset($_SESSION['totalprizeredeemed']);
        unset($_SESSION['commission']);
        unset($_SESSION['batchvalidationsessionid']);
        unset($_SESSION['tmpgame']);
        unset($_SESSION['tmpproduct']);
        unset($_SESSION['tmpuser']);
        unset($_SESSION['tmpcardprice']);
        unset($_SESSION['gameid']);
        unset($_SESSION['isSessionCreated']);
    }
/* YES BUTTON - FINALIZATION ************************************************************************************/    
    
    $valTickets_list->AddArray($arrValidTickets);
}

if (isset($valTickets_list))
{
    for ($i = 0; $i < count($valTickets_list); $i++)
    {
        if (strpos($valTickets_list[$i]["PrizeName"], 'KHR') !== false)
        {
            $str_prize = preg_replace('/[\KHR,]/', '', $valTickets_list[$i]["PrizeName"]);
            $usd_prize = $str_prize / $fixedrate;
            $valTickets_list[$i]["PrizeName"] = "$" . $usd_prize;
        }
    }
}

if ($bool_disable)
{    
    $txtValidationNumber->ReadOnly = false;
    $txtBookNumber->ReadOnly = false;
    $btnAddToBatch->Enabled = true;
    $btnCreateSession->Enabled = false;
    $btnCancelSession->Enabled = true;
    $ddlcardvalue->Visible = false;
    $ddlUsername->Visible = false;
    $ddlgamename->Visible = false;
    $ddlgamenum->Visible = false;
} else {
    $btnCreateSession->Enabled = true;
    $btnCancelSession->Enabled = false;
    $txtValidationNumber->ReadOnly = true;
    $txtBookNumber->ReadOnly = true;
    $btnAddToBatch->Enabled = false;
    $lblcardvalue->Text = "";
    $lbluserid->Text = "";
    $lblgameid->Text = "";
    $lblprodid->Text = "";
    $ddlcardvalue->Visible = true;
    $ddlUsername->Visible = true;
    $ddlgamename->Visible = true;
    $ddlgamenum->Visible = true;
}

function cancelsession($sessionid)
{
    $tmbatchvalidationsession = new TMBatchValidationSession();
    $tmbatchvalidationtickets = new TMBatchValidationTickets();    
    $cancelsess["BatchValidationSessionID"] = $sessionid;
    $cancelsess["Status"] = 5;
    $tmbatchvalidationsession->UpdateByArray($cancelsess);
    $tmbatchvalidationtickets->UpdateTicketValidationStatus($sessionid, 5);
    return true;
}

function CheckDigit($number) 
{ 
    $number=preg_replace('/\D/', '', $number);
    $number_length=strlen($number);
    for ($i=1; $i<=$number_length; $i++) 
    {
        $digit = $number[$i-1];
        if($i % 2)
        {
            $total_step1 += $digit;
        }
        else
        {
            $total_step3 += $digit;
        }
    }
    $result = $total_step1 * 3;
    $result = $result + $total_step3;
    $result = 10 - $result % 10;

    return $result;      
}


/**
 * @author Noel Antonio 04-04-2013
 * This function will insert record to hot tickets table once the user validate a ticket
 * which is either stolen or expired and not payable.
 * @param $ticketid the ticket id to be validated
 * @param $acctid the logged-in user
 * @param $virn the validation number
 * @param $ticketno the ticket number
 * @return boolean
 */
function saveToHotTickets($ticketid, $acctid, $virn, $ticketno)
{
    $tmhottickets = new TMHotTickets();
    $tmticketcancellation = new TMTicketCancellation();
    
    $arrtc = $tmticketcancellation->SelectCancelTypeByTicketID($ticketid);
    if (count($arrtc) == 1)
    {
        $canceltype = $arrtc[0]["CancelType"];

        $insertht["AID"] = $acctid;
        $insertht["VIRN"] = $virn;
        $insertht["TicketNumber"] = $ticketno;
        $insertht["DateCreated"] = "now_usec()";
        $insertht["CreatedByAID"] = $acctid;
        $insertht["CancellationMode"] = $canceltype;
        $tmhottickets->Insert($insertht);
        if ($tmhottickets->HasError){
            return false;
        } else {
            return true;
        }
    }
}
?>
