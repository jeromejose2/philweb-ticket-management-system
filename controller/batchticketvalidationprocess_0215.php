<?php
/*
 * Created By: Noel Antonio 01/09/2012
 */

$pagesubmenuid = 38;
$stylesheets[] = "css/default.css";
$pagetitle = "Retailer Ticket Validation";

App::LoadModuleClass("TicketManagementCM", "TMTicketValidation");
App::LoadModuleClass("TicketManagementCM", "TMDecks");
App::LoadModuleClass("TicketManagementCM", "TMDeckInfo");
App::LoadModuleClass("TicketManagementCM", "TMAccounts");
App::LoadModuleClass("TicketManagementCM", "TMAuditLog");
App::LoadModuleClass("TicketManagementCM", "TMPrizes");
App::LoadModuleClass("TicketManagementCM", "TMProducts");
App::LoadModuleClass("TicketManagementCM", "TMGameManagement");
App::LoadModuleClass("TicketManagementCM", "TMTickets");
App::LoadModuleClass("TicketManagementCM", "TMBatchValidationSession");
App::LoadModuleClass("TicketManagementCM", "TMBatchValidationTickets");
App::LoadModuleClass("TicketManagementCM", "TMBatchTransactionLog");

App::LoadControl("TextBox");
App::LoadControl("Hidden");
App::LoadControl("ComboBox");
App::LoadControl("Button");

$confirmation = 'false';
$prizetypeid = '0';
$accttypeid = $_SESSION['accttype'];
$aid = $_SESSION['acctid'];

$pos = 0;

$fproc = new FormsProcessor();

$tmbatchvalidationsession = new TMBatchValidationSession();
$tmbatchvalidationtickets = new TMBatchValidationTickets();
$tmbatchtransactionlog = new TMBatchTransactionLog();
$tmticketvalidation = new TMTicketValidation();
$tmdecks = new TMDecks();
$tmdeckinfo = new TMDeckInfo();
$tmaccount = new TMAccounts();
$tmauditlog = new TMAuditLog();
$tmprizes = new TMPrizes();
$tmproducts = new TMProducts();
$tmgamemgt = new TMGameManagement();
$tmtickets = new TMTickets();

$ddlUsername = new ComboBox("ddlUsername", "ddlUsername", "Account Name: ");
$ddlUsername->ShowCaption = true;
$arraccounts = $tmaccount->SelectForValidationModule($aid);
$accountlist = new ArrayList();
$accountlist->AddArray($arraccounts);
$litem = null;
$litem[] = new ListItem("Please Select", "0", true);
$ddlUsername->Items = $litem;
$ddlUsername->DataSource = $accountlist;
$ddlUsername->DataSourceText = "UserName";
$ddlUsername->DataSourceValue = "AID";
$ddlUsername->DataBind();

$ddlgamename = new ComboBox("ddlgamename", "ddlgamename", "Game Name: ");
$ddlgamename->ShowCaption = true;
$ddlgamename->Args = "onchange = 'javascript: get_gamenumber();'";
$gamenames = $tmproducts->SelectAllGameName();
$gamename_list = new ArrayList();
$gamename_list->AddArray($gamenames);
$litem = null;
$litem[] = new ListItem("Please Select", "0", true);
$ddlgamename->Items = $litem;
$ddlgamename->DataSource = $gamename_list;
$ddlgamename->DataSourceText = "ProductName";
$ddlgamename->DataSourceValue = "ProductID";
$ddlgamename->DataBind();

$ddlgamenum = new ComboBox("ddlgamenum", "ddlgamenum", "Game Number: ");
$ddlgamenum->ShowCaption = true;
$ddlgamenum->Args = "onchange = 'javascript: get_gamename();'";
$gamenumbers = $tmgamemgt->SelectByGameType();
$gamename_list = new ArrayList();
$gamename_list->AddArray($gamenumbers);
$litem = null;
$litem[] = new ListItem("Please Select", "0", true);
$ddlgamenum->Items = $litem;
$ddlgamenum->DataSource = $gamename_list;
$ddlgamenum->DataSourceText = "GameNumber";
$ddlgamenum->DataSourceValue = "GameID";
$ddlgamenum->DataBind();

$txtValidationNumber = new TextBox("txtValidationNumber", "txtValidationNumber", "Validation Number: ");
$txtValidationNumber->Args="onkeypress='javascript: return isAlphaNumericKey(event);'";
$txtValidationNumber->Length = 12;
$txtValidationNumber->ReadOnly = true;

$txtBookNumber = new TextBox("txtBookNumber", "txtBookNumber", "Book Number: ");
$txtBookNumber->Args="onkeypress='javascript: return isAlphaNumericKey(event);'";
$txtBookNumber->Length = 12;
$txtBookNumber->ReadOnly = true;

$btnCreateSession = new Button("btnCreateSession", "btnCreateSession", "Create Session");
$btnCreateSession->IsSubmit = true;
$btnCreateSession->Args="onclick='javascript: return checkcreatesession();'";

$btnContinueSession = new Button("btnContinueSession", "btnContinueSession", "Continue Session");
$btnContinueSession->IsSubmit = true;

$btnAddToBatch = new Button("btnAddToBatch", "btnAddToBatch", "Add");
$btnAddToBatch->IsSubmit = true;
$btnAddToBatch->Args="onclick='javascript: return checkvalidation();'";
$btnAddToBatch->Enabled = false;

$btnValidate = new Button("btnValidate", "btnValidate", "YES");
$btnValidate->IsSubmit = true;

$btnFinalize = new Button("btnFinalize", "btnFinalize", "Finalize");
$btnFinalize->IsSubmit = true;

$fproc->AddControl($ddlUsername);
$fproc->AddControl($ddlgamename);
$fproc->AddControl($ddlgamenum);
$fproc->AddControl($txtValidationNumber);
$fproc->AddControl($txtBookNumber);
$fproc->AddControl($btnValidate);
$fproc->AddControl($btnAddToBatch);
$fproc->AddControl($btnCreateSession);
$fproc->AddControl($btnContinueSession);
$fproc->AddControl($btnFinalize);

$fproc->ProcessForms();

if ($accttypeid == 5 || $accttypeid == 6)
{
    $ddlUsername->Enabled = false;
}

$aidhaspending = $tmbatchvalidationsession->HasPending($aid);
if (count($aidhaspending) >= 1)
{
    $btnCreateSession->Enabled = false;
}
else
{
    $btnContinueSession->Enabled = false;
}

if ($fproc->IsPostBack)
{  
    $gamenumbers = $tmproducts->SelectGameNumPerGameName($ddlgamename->SubmittedValue);
    $gamename_list = new ArrayList();
    $gamename_list->AddArray($gamenumbers);
    $ddlgamenum->DataSource = $gamename_list;
    $ddlgamenum->DataSourceText = "GameNumber";
    $ddlgamenum->DataSourceValue = "GameID";
    $ddlgamenum->DataBind();
    $ddlgamenum->SetSelectedValue($ddlgamenum->SubmittedValue);
    
    if ($btnCreateSession->SubmittedValue == "Create Session")
    {           
        $aid = $_SESSION['acctid'];
        
        $newsess["Status"] = 0;
        $newsess["DateCreated"] = 'now_usec()';
        $newsess["CreatedByAID"] = $aid;  
        $tmbatchvalidationsession->Insert($newsess);
        if ($tmbatchvalidationsession->HasError)
        {
            $errormsg = $tmauditlog->getError();
            $errormsgtitle = "ERROR!";
        }
        
        $getsessionid = $tmbatchvalidationsession->HasPending($aid);
        $_SESSION['batchvalidationsessionid'] = $getsessionid[0]["BatchValidationSessionID"];
        $batchsessionid = $_SESSION['batchvalidationsessionid'];
        $errormsg = "New Session was created";
        $errormsgtitle = "SESSION";
        $txtValidationNumber->ReadOnly = false;
        $txtBookNumber->ReadOnly = false;
        $btnAddToBatch->Enabled = true;
        $btnCreateSession->Enabled = false;
        $btnContinueSession->Enabled = false;
    }
    
    if ($btnFinalize->SubmittedValue == "Finalize")
    {
        $batchsessionid = $_SESSION['batchvalidationsessionid'];
        
        $confirmation = 'true';
        $okay = 'false';
        $errormsg = "Would you like to process this batch?";
        $errormsgtitle = "CONFIRMATION";
        // LOAD CURRENT SESSION
        $arrValidTickets = $tmbatchvalidationtickets->loadCurrentSession($ddlgamenum->SubmittedValue, $ddlUsername->SubmittedValue, $aid);
        $txtValidationNumber->ReadOnly = false;
        $txtBookNumber->ReadOnly = false;
        $btnAddToBatch->Enabled = true;
        $btnCreateSession->Enabled = false;
        $btnContinueSession->Enabled = false;
    }
    
    if ($btnContinueSession->SubmittedValue == "Continue Session")
    {
        $aid = $_SESSION['acctid'];
        
        $getsessionid = $tmbatchvalidationsession->HasPending($aid);
        $_SESSION['batchvalidationsessionid'] = $getsessionid[0]["BatchValidationSessionID"];
        $batchsessionid = $_SESSION['batchvalidationsessionid'];
        
        // If there is existing session and tickets..
        $chkSession = $tmbatchvalidationtickets->checkCurrentSession($aid);
        if (count($chkSession)>0)
        {
            $gamenumbers = $tmproducts->SelectGameNumPerGameName($chkSession[0]["ProductID"]);
            $gamename_list = new ArrayList();
            $gamename_list->AddArray($gamenumbers);
            $ddlgamenum->DataSource = $gamename_list;
            $ddlgamenum->DataSourceText = "GameNumber";
            $ddlgamenum->DataSourceValue = "GameID";
            $ddlgamenum->DataBind();

            $ddlgamename->SetSelectedValue($chkSession[0]["ProductID"]);
            $ddlgamenum->SetSelectedValue($chkSession[0]["GameID"]);
            $ddlUsername->SetSelectedValue($chkSession[0]["AssignedToAID"]);
            
            // LOAD CURRENT SESSION 
            $arrValidTickets = $tmbatchvalidationtickets->loadCurrentSession($chkSession[0]["GameID"], $chkSession[0]["AssignedToAID"], $aid);           
            $txtValidationNumber->ReadOnly = false;
            $txtBookNumber->ReadOnly = false;
            $btnAddToBatch->Enabled = true;
            $btnCreateSession->Enabled = false;
            $btnContinueSession->Enabled = false;
        }
        else
        {
            if ($ddlUsername->SubmittedValue==0 || $ddlgamename->SubmittedValue==0 || $ddlgamenum->SubmittedValue==0)
            {
                $errormsg = "No tickets were added in your current session. Please add new tickets to validate.";
                $errormsgtitle = "ERROR";   
            }
            else
            {
                $txtValidationNumber->ReadOnly = false;
                $txtBookNumber->ReadOnly = false;
                $btnAddToBatch->Enabled = true;
                $btnCreateSession->Enabled = false;
                $btnContinueSession->Enabled = false;
            }
        }
    }
    
    if ($btnAddToBatch->SubmittedValue == "Add")
    {  
        $aid = $_SESSION['acctid'];
        $batchsessionid = $_SESSION['batchvalidationsessionid'];
        
        $valno = trim($txtValidationNumber->SubmittedValue);
        $fullvirn = trim($txtValidationNumber->SubmittedValue);
        $checksum = substr($fullvirn, strlen($fullvirn) - 1, strlen($fullvirn));
        $book = trim($txtBookNumber->SubmittedValue);
        $gameno = substr($valno,0,strlen($valno) - 9);
        $valno = substr($valno, 0,strlen($valno) - 1);
        $valno2 = substr($valno, strlen($valno) - 8, strlen($valno));
        $ticketno = substr($book,strlen($book) - 3,3);
        $book = substr($book, strlen($book) - 9 ,6);
        
        // START CHECKING OF GAME NUMBER
        $arrGame = $tmgamemgt->SelectByGameNumber($gameno);
        if (count($arrGame) == 1)
        {
            $gameid = $arrGame[0]["GameID"];
                
            // START CHECKING CHECK DIGIT
            $checkdigit = CheckDigit($valno);
            if($checkdigit % 10 != $checksum)
            {
                $errormsg = 'Please enter a valid VIRN.';
                $errormsgtitle = 'ERROR!';
            } 
            else
            {
                    // CHECK IF VALID VIRN
                    $arrWinning = $tmticketvalidation->IsValidVIRN($valno2,$gameid);
                    if (count($arrWinning) == 1)
                    {
                        $arrdtls = $arrWinning[0];
                        $winningcardid = $arrdtls["WinningCardID"];
                        $claimdate = $arrdtls["ClaimDate"];
                        $claimtime = $arrdtls["ClaimTime"];
                        $status = $arrdtls["Status"];
                        $prizetypeid = $arrdtls["PrizeTypeID"];
                        
                        if ($status == 2)
                        {
                            $errormsgtitle = "";
                            $errormsg = "Scratch card prize has already been claimed last ".$claimdate." at ".$claimtime.".";
                        }
                        else
                        {
                            // CHECK IF CARD HAS MAJOR OR MINOR PRIZE
                            if ($prizetypeid == 1)
                            {
                                $errormsgtitle = "";
                                $errormsg = "Sorry, we cannot process the card. The scratch card is a major prize.";
                            }
                            else
                            {
                                // START CHECKING IF EXISTING IN BATCH VALIDATION TABLE
                                $ticketExist =  $tmbatchvalidationtickets->IsExist($ddlgamenum->SubmittedValue, $ddlUsername->SubmittedValue, $txtBookNumber->SubmittedValue, $txtValidationNumber->SubmittedValue, $aid); 
                                if (count($ticketExist) == 1)
                                {
                                    $errormsg = 'Ticket and VIRN already added in the batch.';
                                    $errormsgtitle = 'ERROR!';
                                }
                                else
                                {
                                    // CHECK IF VALID BOOK NUMBER
                                    $arrBookDtls = $tmdecks->IsBookNoValid($book,$gameid);
                                    if (count($arrBookDtls) == 1)
                                    {
                                        $arrdtlsbooks = $arrBookDtls[0];
                                        $statusbooks = $arrdtlsbooks["Status"];
                                        $bookid = $arrdtlsbooks["BookID"];
                                        if ($statusbooks == 1)
                                        {     
                                            // CHECK IF VALID TICKET NUMBER
                                            $arrTicketDtls = $tmtickets->IsTicketNoValid($ticketno, $bookid);
                                            if (count($arrTicketDtls) == 1)
                                            {
                                                $arrdtls2 = $arrTicketDtls[0];
                                                $ticketstatus = $arrdtls2["Status"];
                                                if ($ticketstatus == 1)
                                                {
                                                    //insert to batchvalidationtickets table
                                                    $insertvaltickets["GameID"] = $ddlgamenum->SubmittedValue;
                                                    $insertvaltickets["TicketNumber"] = $txtBookNumber->SubmittedValue;
                                                    $insertvaltickets["ValidationNumber"] = $txtValidationNumber->SubmittedValue;
                                                    $insertvaltickets["WinningCardID"] = $winningcardid;
                                                    $insertvaltickets["BatchValidationSessionID"] = $batchsessionid;
                                                    $insertvaltickets["DateCreated"] = 'now_usec()';
                                                    $insertvaltickets["AssignedToAID"] = $ddlUsername->SubmittedValue;
                                                    $insertvaltickets["Status"] = 0;
                                                    $tmbatchvalidationtickets->Insert($insertvaltickets);

                                                    // LOAD CURRENT SESSION
                                                    $arrValidTickets = $tmbatchvalidationtickets->loadCurrentSession($ddlgamenum->SubmittedValue, $ddlUsername->SubmittedValue, $aid);
                                                    $txtValidationNumber->ReadOnly = false;
                                                    $txtBookNumber->ReadOnly = false;
                                                    $btnAddToBatch->Enabled = true;
                                                    $btnCreateSession->Enabled = false;
                                                    $btnContinueSession->Enabled = false;
                                                }
                                                elseif ($ticketstatus == 5)
                                                {
                                                    $errormsg = "Scratch card has been deemed cancelled. Performing the ticket validation will not be allowed.";
                                                    $errormsgtitle = 'ERROR!';    
                                                }
                                                elseif ($ticketstatus == 6)
                                                {
                                                    $errormsg = "Ticket was already claimed. Performing the ticket validation will not be allowed.";
                                                    $errormsgtitle = 'ERROR!';    
                                                }
                                            }
                                            else
                                            {
                                                $errormsg = "Scratch card does not exist";//"Invalid book number.";
                                                $errormsgtitle = 'ERROR!';    
                                            }

                                        }
                                        else
                                        {
                                            if ($statusbooks == 4)
                                            {
                                                $errormsg = "Please enter a book ticket number that has already been activated."; //"Book number is assigned.";                    
                                            }
                                            elseif ($statusbooks == 2)
                                            {
                                                $errormsg = "Please enter a book ticket number that has already been activated."; //"Book number is still on freight.";                    
                                            }
                                            elseif ($statusbooks == 3)
                                            {
                                                $errormsg = "Please enter a book ticket number that has already been activated."; //"Book number is on stock.";                    
                                            }
                                            elseif ($statusbooks == 5)
                                            {
                                                $errormsg = "Scratch card has been deemed cancelled. Performing the ticket validation will not be allowed.";                    
                                            }
                                            $errormsgtitle = 'ERROR!';  
                                        } // END OF CHECKING VALID TICKET NUMBER
                                    }
                                    else
                                    {            
                                        $errormsg = "Book number does not exist.";
                                        $errormsgtitle = 'ERROR!';                
                                    } // END OF CHECKING VALID BOOK NUMBER
                                } // END OF CHECKING EXISTING BATCH TICKETS
                            } // END OF CHECKING IF MINOR OR MAJOR PRIZE    
                        } // END OF CHECKING IF CLAIMED OR UNCLAIMED
                    } 
                    else
                    {
                        $errormsg = "Sorry, your ticket is not a winning card.";
                        $errormsgtitle = 'ERROR!';
                    }  // END CHECKING IF VALID VIRN
            } // END CHECKING CHECK DIGIT
        }
        else
        {
            $errormsg = 'Game number does not exist.';
            $errormsgtitle = 'ERROR!';
        } // END CHECKING OF GAME NUMBER 
        
        // LOAD CURRENT SESSION
        $arrValidTickets = $tmbatchvalidationtickets->loadCurrentSession($ddlgamenum->SubmittedValue, $ddlUsername->SubmittedValue, $aid);
//        $txtValidationNumber->Text = "";
//        $txtBookNumber->Text = "";
        $txtValidationNumber->ReadOnly = false;
        $txtBookNumber->ReadOnly = false;
        $btnAddToBatch->Enabled = true;
        $btnCreateSession->Enabled = false;
        $btnContinueSession->Enabled = false;

    } // END OF ADD TO BATCH BUTTON
    
    // START OF BUTTON VALIDATE
    if ($btnValidate->SubmittedValue == "YES")
    {   
        $aid = $_SESSION['acctid'];
        
        $arrBatchTickets = $tmbatchvalidationtickets->SelectAllNonValidatedTickets($aid, $_SESSION['batchvalidationsessionid']);
        $arrBatch_List = new ArrayList();
        $arrBatch_List->AddArray($arrBatchTickets);
        
        for ($ctr=0;$ctr < count($arrBatch_List);$ctr++)
        {
            $batchlistaccname = $arrBatch_List[$ctr]["AssignedToAID"];
            $batchlistbooknum = $arrBatch_List[$ctr]["TicketNumber"];
            $batchlistvalnum = $arrBatch_List[$ctr]["ValidationNumber"];
            $batchwinningcardid = $arrBatch_List[$ctr]['WinningCardID'];
            $gameid = $arrBatch_List[$ctr]['GameID'];
            $batchgameid = $arrBatch_List[$ctr]['GameBatchID'];
            $batchvalidationticketID = $arrBatch_List[$ctr]['BatchValidationTicketID'];
            $batchvalidationsessionID = $arrBatch_List[$ctr]['BatchValidationSessionID'];
            $batchlistprizename = $arrBatch_List[$ctr]['PrizeName'];
            $batchlistprizename = preg_replace('/[\$,]/', '', $batchlistprizename);
            
            $unameid = $batchlistaccname;
            $valno = trim($batchlistvalnum);
            $book = trim($batchlistbooknum);
            $winningcardid = $batchwinningcardid;
            
            $gameno = substr($valno,0,strlen($valno) - 9);
            $valno = substr($valno, 0,strlen($valno) - 1);
            $ticketno = substr($book,strlen($book) - 3,3);
            $book = substr($book, strlen($book) - 9 ,6);
            
            $arrGame = $tmgamemgt->SelectByGameNumber($gameno);
            $gameid = $arrGame[0]["GameID"];
            $arrBookDtls = $tmdecks->IsBookNoValid($book,$gameid);
            $bookid = $arrBookDtls[0]["BookID"];
                
            // UPDATE WINNING CARD STATUS TO VERIFIED 
            if ($status != '2')
            {
                $tmparam["Status"] = '1';
            }
            $tmparam["WinningCardID"] = $winningcardid;
            $tmupdatewinningcard = $tmticketvalidation->UpdateByArray($tmparam);
            if ($tmticketvalidation->HasError)
            {
                $errormsg = $tmticketvalidation->getError();
                $errormsgtitle = 'ERROR!';
            }

            // UPDATE BOOK NUMBER        
            $tmparam["BookNumber"] = $book;
            $tmparam["Status"] = '2';
            $tmparam["WinningCardID"] = $winningcardid;
            $tmparam["DateClaimed"] = 'now_usec()';
            if ($unameid > 0)
            {                    
                $tmparam["ClaimedByAID"] = $unameid; 
                $tmparam["Remarks"] = "Claiming of AcctID ".$_SESSION['acctid']." overridden by AcctID ".$unameid;
            }
            else
            {
                $tmparam["ClaimedByAID"] = $aid;
            }        
            $tmupdatewinningcard = $tmticketvalidation->UpdateByArray($tmparam);
            if ($tmupdatewinningcard->HasError)
            {
                $errormsg = $tmupdatewinningcard->getError();
                $errormsgtitle = 'ERROR!';
            }

            // UPDATE DECK INFO
            $arrdeckinfo = $tmdeckinfo->GetDeckInfoByGameBatchID($batchgameid);
            if (count($arrdeckinfo) == 1)
            {
                $arrdtls = $arrdeckinfo[0];
                $usedwinningcardcount = $arrdtls["UsedWinningCardCount"];
                $claimedwinningcardcount = $arrdtls["ClaimedWinningCardCount"];
                $deckid = $arrdtls["DeckID"];
                $usedwinningcardcount = $usedwinningcardcount + 1;
                $claimedwinningcardcount = $claimedwinningcardcount + 1;
                $tmdeckinfoparam["UsedWinningCardCount"] = $usedwinningcardcount; 
                $tmdeckinfoparam["ClaimedWinningCardCount"] = $claimedwinningcardcount; 
                $tmdeckinfoparam["DeckID"] = $deckid;
                $tmupdatedeckinfo = $tmdeckinfo->UpdateByArray($tmdeckinfoparam);
                if ($tmdeckinfo->HasError)
                {
                    $errormsg = $tmdeckinfo->getError();
                    $errormsgtitle = "ERROR!";
                }
            }

            // UPDATE BATCH VALIDATION TICKETS
            $updBatchTicket["BatchValidationTicketID"] = $batchvalidationticketID;
            $updBatchTicket["Status"] = 1;
            $tmbatchvalidationtickets->UpdateByArray($updBatchTicket);
            if ($tmbatchvalidationtickets->HasError)
            {
                $errormsg = $tmbatchvalidationtickets->getError();
                $errormsgtitle = "ERROR!";
            }
            
            // UPDATE PROCESSED TICKETS
            $updProcessed = $tmbatchvalidationtickets->UpdateProcessedTickets($batchlistbooknum, $batchlistvalnum);
            
            // UPDATE TICKET STATUS
            $updTicket = $tmtickets->UpdateTicketStatus($bookid, $ticketno);

            // INSERT TO AUDIT TRAIL LOGS              
            $tmaudit["SessionID"] = $_SESSION['sid'];
            $tmaudit["AID"] = $_SESSION['acctid'];
            $tmaudit["TransDetails"] = 'Validation No.: '.$valno;
            $tmaudit["TransDateTime"] = 'now_usec()';
            $tmaudit["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
            $tmaudit["AuditTrailFunctionID"] = '12';    
            $tmauditlog->Insert($tmaudit);
            if ($tmauditlog->HasError)
            {
                $errormsg = $tmauditlog->getError();
                $errormsgtitle = "ERROR!";
            }

            $_SESSION['batchvalidationsessionid'] = $batchvalidationsessionID;
            $_SESSION['gameid'] = $gameid;  
            $totalprizeredeemed = $totalprizeredeemed + $batchlistprizename;
            $commission = $totalprizeredeemed * 0.02;
            $_SESSION['totalprizeredeemed'] = $totalprizeredeemed;
            $_SESSION['commission'] = $commission;
        } // END OF BATCH CLAIMING (LOOP)
        
        // Update Batch Validation Session to Closed/Finalized
        $updBatchSession["BatchValidationSessionID"] = $_SESSION['batchvalidationsessionid'];
        $updBatchSession["Status"] = 3;
        $tmbatchvalidationsession->UpdateByArray($updBatchSession);
        if ($tmbatchvalidationsession->HasError)
        {
            $errormsg = $tmbatchvalidationsession->getError();
            $errormsgtitle = "ERROR!";
        }

        // Insert to Batch Validation Logs
        $insBatchLogs["GameID"] = $_SESSION['gameid'];
        $insBatchLogs["BatchID"] = $_SESSION['batchvalidationsessionid'];;
        $insBatchLogs["TransactionDate"] = 'now_usec()';
        $insBatchLogs["TransactionType"] = 2;
        $insBatchLogs["ProcessedByAID"] = $aid;
        $insBatchLogs["Status"] = 1;
        $tmbatchtransactionlog->Insert($insBatchLogs);
        if ($tmbatchtransactionlog->HasError)
        {
            $errormsg = $tmbatchtransactionlog->getError();
            $errormsgtitle = "ERROR!";
        }

        $okay = 'true';
        $errormsg = 'Retailer Username : '.$ddlUsername->SelectedText.'<br/>'.
                    'Date and Time : '.date("m/d/Y h:i a").'<br/>'.
                    '<b>Claimed Tickets</b><br/>'.
                    'Game Number : '.$ddlgamenum->SelectedText.'<br/>'.
                    'Game Name : '.$ddlgamename->SelectedText.'<br/>'.
                    'Total Prizes Redeemed : $'.number_format($_SESSION['totalprizeredeemed'] , 2 , "." , ",").'<br/>'.
                    'Total Redemption Commission : $'.number_format($_SESSION['commission'] , 2 , "." , ",").'<br/>';
        $errormsgtitle = 'SUMMARY';
        
        // LOAD CURRENT SESSION
        $arrValidTickets = $tmbatchvalidationtickets->loadCurrentSession($ddlgamenum->SubmittedValue, $ddlUsername->SubmittedValue, $aid);
        $txtValidationNumber->Text = "";
        $txtBookNumber->Text = "";
        $txtValidationNumber->ReadOnly = true;
        $txtBookNumber->ReadOnly = true;
        $btnAddToBatch->Enabled = false;
        $btnCreateSession->Enabled = true;
        $btnContinueSession->Enabled = false;
        unset($_SESSION['totalprizeredeemed']);
        unset($_SESSION['commission']);
        unset($_SESSION['batchvalidationsessionid']);
        unset($_SESSION['gameid']);
    } // END OF VALIDATE BUTTON
    
    $valTickets_list = new ArrayList();
    $valTickets_list->AddArray($arrValidTickets); 
}

function CheckDigit($number) 
{ 
	// Strip any non-digits 
	$number=preg_replace('/\D/', '', $number);
	
	// Set the string length 
	$number_length=strlen($number);
	

	for ($i=1; $i<=$number_length; $i++) 
	{
		$digit = $number[$i-1];
		if($i % 2)
		{
			//step 1
			//odd numbers
			$total_step1 += $digit;
		}
		else
		{
			//step 3
			//even numbers
			$total_step3 += $digit;
		}
	}
	//step2
	$result = $total_step1 * 3;
	//step4
	$result = $result + $total_step3;
	//step5
	$result = 10 - $result % 10;
	
	return $result;      
}
?>
