<?php
/*
 * Batch Usage Report
 * 
 * Controller of the report module. Process and outputs the scratch card usage.
 * 
 * @author: Theresa Calderon
 * 
 * lastupdatedby: Noel Antonio
 * dateupdated: feb. 6, 2013
 * purpose: fix prize counter outputs. it must be the same with the prize history.
 * change the xmltype pointing to winners.
 * 
 */
$pagesubmenuid = 42;
$stylesheets[] = "css/default.css";
$javascripts[] = "jscripts/validations.js";
$javascripts[] = "jscripts/jquery-1.5.2.min.js";

App::LoadModuleClass("TicketManagementCM", "TMGameManagement");
App::LoadModuleClass("TicketManagementCM", "TMProducts");
App::LoadModuleClass("TicketManagementCM", "TMDecks");
App::LoadModuleClass("TicketManagementCM", "TMWinnings");
App::LoadModuleClass("TicketManagementCM", "TMTickets");
App::LoadModuleClass("TicketManagementCM", "TMGameImport");
App::LoadModuleClass("TicketManagementCM", "TMCurrencies");
App::LoadControl("TextBox");
App::LoadControl("Button");
App::LoadControl("ComboBox");
App::LoadControl("DataTable");
App::LoadControl("Hidden");

/*Added by Arlene R. Salazar 01-26-2012*/
//App::LoadLibrary("fpdf/fpdf.php");

$fproc = new FormsProcessor();

$currency = App::getParam("currency");
$TMCurrencies = new TMCurrencies();
$TMDecks = new TMDecks();
$TMGames = new TMGameManagement();
$TMProducts = new TMProducts();
$TMWinnings = new TMWinnings();
$TMTickets = new TMTickets();
$TMGameImport = new TMGameImport();

$gamenames = $TMProducts->SelectAllGameName();
$gamename_list = new ArrayList();
$gamename_list->AddArray($gamenames);
$ddlgamename = new ComboBox("ddlgamename","ddlgamename","Game Name: ");
$ddlgamename->Args = "onchange = 'javascript: get_gamenumber();'";
$ddlgamename->ShowCaption = true;
$gamenameopt = null;
$gamenameopt[] = new ListItem("ALL", "0", true);
$ddlgamename->Items = $gamenameopt;
$ddlgamename->DataSource = $gamename_list;
$ddlgamename->DataSourceText = "ProductName";
$ddlgamename->DataSourceValue = "ProductID";
$ddlgamename->DataBind();

$games = $TMGames->SelectByGameType();
$games_list = new ArrayList();
$games_list->AddArray($games);
$ddlgametype = new ComboBox("ddlgametype", "ddlgametype", "Game Number: ");
$ddlgametype->Args = "onchange='javascript: get_batchID(); get_gamename();'";
$litemgmetype = null;
$litemgmetype[] = new ListItem("ALL", "0", true);
$ddlgametype->Items = $litemgmetype;
$ddlgametype->ShowCaption = true;
$ddlgametype->DataSource = $games_list;
$ddlgametype->DataSourceText = "GameNumber";
$ddlgametype->DataSourceValue = "GameID";
$ddlgametype->DataBind();

$ddlgamebatch = new ComboBox("ddlgamebatch","ddlgamebatch","Game Batch:");
$ddlgamebatch->ShowCaption = true;
$gamebatch = null;
$gamebatch[] = new ListItem("ALL","0", true);
$ddlgamebatch->Items  = $gamebatch;

$flag = new Hidden("flag","flag","flag");
$xmltype = new Hidden("xmltype","xmltype","xmltype");

$btnExport = new Button("export","export","Export to PDF");
$btnExport->IsSubmit = true;

$btnExportCSV = new Button("btnExportCSV","btnExportCSV","Export to CSV");
$btnExportCSV->IsSubmit = true;

$btnSubmit = new Button("btnSubmit", "btnSubmit", "Submit");
$btnSubmit->IsSubmit = true;
$btnSubmit->Args = "onclick='javascript: return batchusagevalidation();'";

$hiddenflag = new Hidden("hiddenflag","hiddenflag","Hidden Flag");

$fproc->AddControl($ddlgametype);
$fproc->AddControl($btnSubmit);
$fproc->AddControl($ddlgamename);
$fproc->AddControl($ddlgamebatch);
$fproc->AddControl($xmltype);
$fproc->AddControl($flag);
$fproc->AddControl($btnExport);
$fproc->AddControl($btnExportCSV);
$fproc->AddControl($hiddenflag);
$fproc->ProcessForms();

if ($fproc->IsPostBack)
{
    if ($flag->SubmittedValue == 1)
    {
        $ddlgametype->Args = "onchange='javascript: get_batchID(); '";
        $ddlgametype->ClearItems();
        $prodid1 = $ddlgamename->SubmittedValue;
        $gamenumbers = $TMProducts->SelectGameNumPerGameName($prodid1);
        $gamenumbers_list1 = new ArrayList();
        $gamenumbers_list1->AddArray($gamenumbers);       
        $litemgmetype1 = null;
        $litemgmetype1[] = new ListItem("ALL", "0", false);
        $ddlgametype->Items = $litemgmetype1;
        $ddlgametype->DataSource = $gamenumbers_list1;
        $ddlgametype->DataSourceText = "GameNumber";
        $ddlgametype->DataSourceValue = "GameID";
        $ddlgametype->DataBind();
        $ddlgametype->SetSelectedValue($ddlgametype->SubmittedValue);
    }
    if ($flag->SubmittedValue == 2)
    {
        $ddlgamename->Args = "onchange='javascript:'";          
        $ddlgamename->ClearItems();
        $gamenum1 = $ddlgametype->SubmittedValue;
        $gamenames = $TMProducts->SelectGameNamePerGameNum($gamenum1);
        $gamename_list1 = new ArrayList();
        $gamename_list1->AddArray($gamenames);       
        $litemgmetype1 = null;
        $litemgmetype1[] = new ListItem("ALL", "0", false);
        $ddlgamename->Items = $litemgmetype1;
        $ddlgamename->DataSource = $gamename_list1;
        $ddlgamename->DataSourceText = "ProductName";
        $ddlgamename->DataSourceValue = "ProductID";
        $ddlgamename->DataBind();
        $ddlgamename->SetSelectedValue($ddlgametype->SubmittedValue);
    }
 
    $ddlgametype->SetSelectedValue($ddlgametype->SubmittedValue);
    $ddlgamename->SetSelectedValue($ddlgamename->SubmittedValue);
   
    $gamebatchID = $TMProducts->SelectBatchIDPerGameNum($ddlgametype->SelectedValue,$xmltype->SubmittedValue);
    $batchlist = new ArrayList();
    $batchlist->AddArray($gamebatchID);
    $gamebatch = null;
    $gamebatch[] = new ListItem("ALL","0", true);
    $ddlgamebatch->Items  = $gamebatch;
    $ddlgamebatch->DataSource = $batchlist;
    $ddlgamebatch->DataSourceText = "BatchID";
    $ddlgamebatch->DataSourceValue = "GameBatchID";
    $ddlgamebatch->DataBind();
    $ddlgamebatch->SetSelectedValue($ddlgamebatch->SubmittedValue);

    $moneycurrency = 'KHR';
    $getconvertmoney = $TMCurrencies->SelectByCurrencySymbol($moneycurrency);
    $fixedrate = $getconvertmoney[0]['FixedRate'];
    
    if ($btnSubmit->SubmittedValue == "Submit")
    {
        // Query for Date Imported
        $di = $TMGameImport->GetDateImported($ddlgametype->SubmittedValue, $ddlgamebatch->SelectedText);
        
        // Query  for Batch Consumption
        $batchsize = $TMDecks->GetBatchSize($ddlgamename->SubmittedValue,$ddlgametype->SubmittedValue, $ddlgamebatch->SelectedText);
        $twe = $TMWinnings->GetTotalWinningEntries($ddlgamename->SubmittedValue,$ddlgametype->SubmittedValue, $ddlgamebatch->SelectedText);
        $teu = $TMDecks->GetTotalEntriesUsed($ddlgamename->SubmittedValue,$ddlgametype->SubmittedValue, $ddlgamebatch->SelectedText);
        $tce = $TMTickets->GetCancelledEntries($ddlgamename->SubmittedValue,$ddlgametype->SubmittedValue, $ddlgamebatch->SelectedText);
        
        // Query for Prize Amount Consumption
        $tp = $TMWinnings->GetTotalPrizeAmount($ddlgamename->SubmittedValue,$ddlgametype->SubmittedValue, $ddlgamebatch->SelectedText);
        $tp[0]["TotalPrize"] = $tp[0]["TotalPrize"] + ($tp[0]["TotalPrizeinKHR"]/$fixedrate);
        $pu = $TMWinnings->GetTotalPrizeUsed($ddlgamename->SubmittedValue,$ddlgametype->SubmittedValue, $ddlgamebatch->SelectedText);
        $pu[0]["TotalPrizeUsed"] = $pu[0]["TotalPrizeUsed"] + ($pu[0]["TotalPrizeUsedinKHR"]/$fixedrate);
        
        // Query for Prize Counter
        $getprize = $TMWinnings->GetPrizeCounter($ddlgamename->SubmittedValue,$ddlgametype->SubmittedValue, $ddlgamebatch->SubmittedValue);
        
        // Start conversion from KHR to $        
        for($i = 0 ; $i < count($getprize) ; $i++)
        {
                $tmpvariable = $getprize[$i]["PrizeName"];

                if (strpos($tmpvariable,'KHR') !== false)
                {
                        $tmpvariable = trim($tmpvariable, KHR);
                        $tmpvariable = (int)str_replace(array(' ', ','), '', $tmpvariable);

                        if($tmpvariable > $fixedrate)
                        {    
                                $tmpvariable = $tmpvariable/$fixedrate;            
                                $getprize[$i]["PrizeName"] = "$".number_format($tmpvariable);
                                $getprize[$i]["0"] = $tmpvariable;
                        } 
                        else
                        {
                                $tmpvariable = $tmpvariable/$fixedrate;            
                                $getprize[$i]["PrizeName"] = "$".$tmpvariable;
                                $getprize[$i]["0"] = $tmpvariable;
                        }
                }
        }
        // End conversion from KHR to $    

        $getprize_list = new ArrayList();
        $getprize_list->AddArray($getprize);
        $getprizeclaimed = $TMWinnings->GetPrizeCounterClaimed($ddlgamename->SubmittedValue,$ddlgametype->SubmittedValue, $ddlgamebatch->SubmittedValue);
        $getprizeclaimed_list = new ArrayList();
        $getprizeclaimed_list->AddArray($getprizeclaimed);
    }

    /* 
     * PDF Download
     * 
    if($btnExport->SubmittedValue == "Export to PDF") //Added by ARS 01-26-2012
    {
        //get game details
        $gamename = $ddlgamename->SelectedText;
        $gameno = $ddlgametype->SelectedText;
        $gamebatch = $ddlgamebatch->SelectedText;
            
        // Query for Date Imported
        $di = $TMGameImport->GetDateImported($ddlgametype->SubmittedValue, $ddlgamebatch->SelectedText);
        // Query  for Batch Consumption
        $batchsize = $TMDecks->GetBatchSize($ddlgametype->SubmittedValue, $ddlgamebatch->SelectedText);
        $twe = $TMWinnings->GetTotalWinningEntries($ddlgametype->SubmittedValue, $ddlgamebatch->SelectedText);
        $teu = $TMDecks->GetTotalEntriesUsed($ddlgametype->SubmittedValue, $ddlgamebatch->SelectedText);
        $tce = $TMTickets->GetCancelledEntries($ddlgametype->SubmittedValue, $ddlgamebatch->SelectedText);
        
        // Query for Prize Amount Consumption
        $tp = $TMWinnings->GetTotalPrizeAmount($ddlgametype->SubmittedValue, $ddlgamebatch->SelectedText); 
        $pu = $TMWinnings->GetTotalPrizeUsed($ddlgametype->SubmittedValue, $ddlgamebatch->SelectedText);
        
        // Query for Prize Counter
        $getprize = $TMWinnings->GetPrizeCounter($ddlgametype->SubmittedValue, $ddlgamebatch->SubmittedValue);
        $getprize_list = new ArrayList();
        $getprize_list->AddArray($getprize);
        $getprizeclaimed = $TMWinnings->GetPrizeCounterClaimed($ddlgametype->SubmittedValue, $ddlgamebatch->SubmittedValue);
        $getprizeclaimed_list = new ArrayList();
        $getprizeclaimed_list->AddArray($getprizeclaimed);

        $pdf = new FPDF('L','mm','Legal');
        $pdf->AddPage();

        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(0,5,"Scratch Card Batch Usage",0,0,'C');

        $pdf->Ln(8);
        
        $pdf->SetFont('Arial','B',9);
        $pdf->SetX(40);
        $pdf->Cell(40,5,'Game Name:',1,0,'C');
        $pdf->SetFont('Arial','',9);
        $pdf->SetX(80);
        $pdf->Cell(40,5,$gamename,1,0,'C');
        $pdf->SetFont('Arial','B',9);
        $pdf->SetX(120);
        $pdf->Cell(40,5,'Game Number:',1,0,'C');
        $pdf->SetFont('Arial','',9);
        $pdf->SetX(160);
        $pdf->Cell(40,5,$gameno,1,0,'C');
        $pdf->SetFont('Arial','B',9);
        $pdf->SetX(200);
        $pdf->Cell(40,5,'Game Batch:',1,0,'C');
        $pdf->SetFont('Arial','',9);
        $pdf->SetX(240);
        $pdf->Cell(40,5,$gamebatch,1,0,'C');
        
        $pdf->Ln(8);
        $pdf->Ln(8);

        $pdf->SetFont('Arial','B',9);
        $pdf->SetX(40);
        $pdf->Cell(40,5,'Date Imported :',1,0,'C');
        $pdf->SetFont('Arial','',9);
        $pdf->SetX(80);
        $pdf->Cell(40,5,$di[0]["DateImported"],1,1,'C');

        $pdf->Ln(8);

        $pdf->SetFont('Arial','B',9);
        $pdf->SetX(40);
        $pdf->Cell(270,5,'BATCH CONSUMPTION',1,1,'C');
        $pdf->SetX(40);
        $pdf->Cell(40,5,'Batch Size',1,0,'C');
        $pdf->SetX(80);
        $pdf->Cell(40,5,'Total Winning Entries',1,0,'C');
        $pdf->SetX(120);
        $pdf->Cell(40,5,'Total Entries Used',1,0,'C');
        $pdf->SetX(160);
        $pdf->Cell(40,5,'Total Canceled Entries',1,0,'C');
        $pdf->SetX(200);
        $pdf->Cell(40,5,'Balance Entries',1,0,'C');
        $pdf->SetX(240);
        $pdf->Cell(70,5,'Batch Consumption Percentage',1,1,'C');

        $pdf->SetFont('Arial','',9);
        if(count($batchsize) > 0)
        {
            for($i = 0 ; $i < count($batchsize) ; $i++)
            {
                    $be = (($batchsize[$i]['DeckSize'] - $teu[$i]['EntriesUsed']) - $tce[$i]['CancelledTicketCount']);
                    $bcp = round((($teu[$i]['EntriesUsed'] / $batchsize[$i]['DeckSize']) * 100), 4);

                    $pdf->SetX(40);
                    $pdf->Cell(40,5,number_format($batchsize[$i]['DeckSize'] , 0 , '' , ','),1,0,'C');
                    $pdf->Cell(40,5,number_format($twe[$i]['WinningsCount'] , 0 , '' , ','),1,0,'C');
                    $pdf->Cell(40,5,number_format($teu[$i]['EntriesUsed'] , 0 , '' , ','),1,0,'C');
                    $pdf->Cell(40,5,number_format($tce[$i]['CancelledTicketCount'] , 0 , '' , ','),1,0,'C');
                    $pdf->Cell(40,5,number_format($be , 0 , '' , ','),1,0,'C');
                    $pdf->Cell(70,5,$bcp . "%",1,1,'C');
            }
        }
        else
        {
            $pdf->SetX(40);
            $pdf->Cell(270,5,'No records found',1,1,'C');
        }

        $pdf->Ln(8);

        $pdf->SetFont('Arial','B',9);
        $pdf->SetX(40);
        $pdf->Cell(190,5,'PRIZE AMOUNT CONSUMPTION',1,1,'C');
        $pdf->SetX(40);
        $pdf->Cell(40,5,'Total Prize',1,0,'C');
        $pdf->SetX(80);
        $pdf->Cell(40,5,'Prize Used',1,0,'C');
        $pdf->SetX(120);
        $pdf->Cell(40,5,'Prize Left',1,0,'C');
        $pdf->SetX(160);
        $pdf->Cell(70,5,'Prize Consumption Percentage',1,1,'C');	
		
        $pdf->SetFont('Arial','',9);
        if(count($tp) > 0)
        {
            for($i = 0 ; $i < count($tp) ; $i++)
            {
				$pl = ($tp[$i]['TotalPrize'] - $pu[$i]['TotalPrizeUsed']);
				$pcp = round((($pu[$i]['TotalPrizeUsed'] / $tp[$i]['TotalPrize']) * 100), 4);

				$pdf->SetX(40);
				$pdf->Cell(40,5,$currency.number_format($tp[$i]['TotalPrize'] , 0 , '' , ','),1,0,'C');
				$pdf->Cell(40,5,$currency.number_format($pu[$i]['TotalPrizeUsed'] , 0 , '' , ','),1,0,'C');
				$pdf->Cell(40,5,$currency.number_format($pl , 0 , '' , ','),1,0,'C');
				$pdf->Cell(70,5,$pcp . "%",1,1,'C');	
            }
        }
        else
        {
            $pdf->SetX(40);
            $pdf->Cell(190,5,'No records found',1,1,'C');
        }

        $pdf->Ln(8);

        $pdf->SetFont('Arial','B',9);
        $pdf->SetX(40);
        $pdf->Cell(210,5,'PRIZE COUNTER',1,1,'C');
        $pdf->SetX(40);
        $pdf->Cell(40,5,'Prize Type',1,0,'C');
        $pdf->SetX(80);
        $pdf->Cell(40,5,'Total Quantity of Prize',1,0,'C');
        $pdf->SetX(120);
        $pdf->Cell(40,5,'Total Quantity Used',1,0,'C');
        $pdf->SetX(160);
        $pdf->Cell(40,5,'Prize Balance Entries',1,0,'C');
        $pdf->SetX(200);
        $pdf->Cell(50,5,'Consumption Percentage',1,1,'C');

        $pdf->SetFont('Arial','',9);
        if(count($getprize_list) > 0)
        {
            for($i = 0 ; $i < count($getprize_list) ; $i++)
            {
                if ($getprize_list[$i]['TQP'] == "")
                { 
                        $getprize_list[$i]['TQP'] = 0;
                }
                if ($getprizeclaimed_list[$i]['TQU'] == "")
                { 
                        $getprizeclaimed_list[$i]['TQU'] = 0;
                }
                $pbe = $getprize_list[$i]['TQP'] - $getprizeclaimed_list[$i]['TQU'];
                $cp = round((($getprizeclaimed_list[$i]['TQU'] / $getprize_list[$i]['TQP']) * 100), 4);

                $pdf->SetX(40);
                $pdf->Cell(40,5,$getprize_list[$i]['PrizeName'],1,0,'C');
                $pdf->Cell(40,5,number_format($getprize_list[$i]['TQP'] , 0 , '' , ','),1,0,'C');
                $pdf->Cell(40,5,number_format($getprizeclaimed_list[$i]['TQU'] , 0 , '' , ','),1,0,'C');
                $pdf->Cell(40,5,number_format($pbe , 0 , '' , ','),1,0,'C');
                $pdf->Cell(50,5,$cp . "%",1,1,'C');
            }
        }
        else
        {
                $pdf->SetX(40);
                $pdf->Cell(210,5,'No records found',1,1,'C');
        }

        $pdf->Output('Scratch Card Batch Usage','D');
        Header('Content-Type: application/pdf');
    }
    */

    //Added by ARS 03-01-2012
    if($btnExportCSV->SubmittedValue == "Export to CSV")
    {       
	// Query for Date Imported
        $di = $TMGameImport->GetDateImported($ddlgametype->SubmittedValue, $ddlgamebatch->SelectedText);
        
        // Query  for Batch Consumption
        $batchsize = $TMDecks->GetBatchSize($ddlgamename->SubmittedValue,$ddlgametype->SubmittedValue, $ddlgamebatch->SelectedText);
        $twe = $TMWinnings->GetTotalWinningEntries($ddlgamename->SubmittedValue,$ddlgametype->SubmittedValue, $ddlgamebatch->SelectedText);
        $teu = $TMDecks->GetTotalEntriesUsed($ddlgamename->SubmittedValue,$ddlgametype->SubmittedValue, $ddlgamebatch->SelectedText);
        $tce = $TMTickets->GetCancelledEntries($ddlgamename->SubmittedValue,$ddlgametype->SubmittedValue, $ddlgamebatch->SelectedText);
        
        // Query for Prize Amount Consumption
        $tp = $TMWinnings->GetTotalPrizeAmount($ddlgamename->SubmittedValue,$ddlgametype->SubmittedValue, $ddlgamebatch->SelectedText);
        $tp[0]["TotalPrize"] = $tp[0]["TotalPrize"] + ($tp[0]["TotalPrizeinKHR"]/$fixedrate);
        $pu = $TMWinnings->GetTotalPrizeUsed($ddlgamename->SubmittedValue,$ddlgametype->SubmittedValue, $ddlgamebatch->SelectedText);        
        $pu[0]["TotalPrizeUsed"] = $pu[0]["TotalPrizeUsed"] + ($pu[0]["TotalPrizeUsedinKHR"]/$fixedrate);
        
        // Query for Prize Counter
        $getprize = $TMWinnings->GetPrizeCounter($ddlgamename->SubmittedValue,$ddlgametype->SubmittedValue, $ddlgamebatch->SubmittedValue);
        
        // Start conversion from KHR to $        
        for($i = 0 ; $i < count($getprize) ; $i++)
        {
                $tmpvariable = $getprize[$i]["PrizeName"];

                if (strpos($tmpvariable,'KHR') !== false)
                {
                        $tmpvariable = trim($tmpvariable, KHR);
                        $tmpvariable = (int)str_replace(array(' ', ','), '', $tmpvariable);

                        if($tmpvariable>$fixedrate)
                        {    
                                $tmpvariable = $tmpvariable/$fixedrate;            
                                $getprize[$i]["PrizeName"] = "$".number_format($tmpvariable);
                                $getprize[$i]["0"] = $tmpvariable;
                        }
                        else
                        {
                                $tmpvariable = $tmpvariable/$fixedrate;            
                                $getprize[$i]["PrizeName"] = "$".$tmpvariable;
                                $getprize[$i]["0"] = $tmpvariable;
                        }
                }
        }
        //end conversion from KHR to $        
        $getprize_list = new ArrayList();
        $getprize_list->AddArray($getprize);
        $getprizeclaimed = $TMWinnings->GetPrizeCounterClaimed($ddlgamename->SubmittedValue,$ddlgametype->SubmittedValue, $ddlgamebatch->SubmittedValue);
        $getprizeclaimed_list = new ArrayList();
        $getprizeclaimed_list->AddArray($getprizeclaimed);

        $data1 = "";
        $data2 = "";
        $data3 = "";
        $data4 = "";
        $fp = fopen("../csv/Scratch_Card_Batch_Usage.csv","w");
        if($fp)
        {
                $data1 .= "Date Imported :," . $di[0]["DateImported"] . "\r\n\n";
                fwrite($fp,$data1);

                $header1 = "BATCH CONSUMPTION\r\nBatch Size,Total Winning Entries,Total Entries Used,Total Cancelled Entries,Balance Entries,Batch Consumption Percentage\r\n";
                fwrite($fp,$header1);

                if(count($batchsize) > 0)
                {
                        for($i = 0 ; $i < count($batchsize) ; $i++)
                        {
                                $be = (($batchsize[$i]['DeckSize'] - $teu[$i]['EntriesUsed']) - $tce[$i]['CancelledTicketCount']);
                                $bcp = round((($teu[$i]['EntriesUsed'] / $batchsize[$i]['DeckSize']) * 100), 4);
                                $data2 .= $batchsize[$i]['DeckSize'] . "," . $twe[$i]['WinningsCount'] . "," . $teu[$i]['EntriesUsed'] . "," . $tce[$i]['CancelledTicketCount'] . "," . $be . "," . $bcp . "%" . "\r\n\n";
                        }
                }
                else
                {
                        $data2 .= "No Records Found\r\n\n";
                }
                fwrite($fp,$data2);
	
                $header2 = "PRIZE AMOUNT CONSUMPTION\r\nTotal Prize,Prize Used,Prize Left,Prize Consumption Percentage\r\n";
                fwrite($fp,$header2);
                if(count($tp) > 0)
                {
                        for($i = 0 ; $i < count($tp) ; $i++)
		        {
                                $pl = ($tp[$i]['TotalPrize'] - $pu[$i]['TotalPrizeUsed']);
                                $pcp = round((($pu[$i]['TotalPrizeUsed'] / $tp[$i]['TotalPrize']) * 100), 4);
                                $data3 .= $currency . $tp[$i]['TotalPrize'] . "," . $currency . $pu[$i]['TotalPrizeUsed'] . "," . $currency . $pl . "," . $pcp . "%" . "\r\n\n";
		        }
                }
                else
                {
                        $data3 .= "No Records Found.\r\n\n";
                }
                fwrite($fp,$data3);

                $header3 = "PRIZE COUNTER\r\nPrize Type,Total Quantity of Prize,Total Quantity Used,Prize Balance Entries,Consumption Percentage\r\n";
                fwrite($fp,$header3);
                if(count($getprize_list) > 0)
                {
                        // Added By Noel Antonio 02-06-2013
                        $array = array();
                        for($ctr=0; $ctr < count($getprize_list); $ctr++)
                        {
                            for ($j = 0; $j < count($getprizeclaimed_list); $j++)
                            {
                                if (!isset($array[$getprizeclaimed_list[$j]["PrizeName"]]))
                                {
                                    $array[$getprizeclaimed_list[$j]["PrizeName"]] = $getprizeclaimed_list[$j]['TQU'];
                                }
                            }
                            $array[$getprize_list[$ctr]["PrizeName"]] += 0;
                        }
                        
                        for($i = 0 ; $i < count($getprize_list) ; $i++)
		        {
                                /*if ($getprize_list[$i]['TQP'] == "")
                                { 
                                        $getprize_list[$i]['TQP'] = 0;
                                }
                                if ($getprizeclaimed_list[$i]['TQU'] == "")
                                { 
                                        $getprizeclaimed_list[$i]['TQU'] = 0;
                                }*/
                            
                                $pbe = $getprize_list[$i]['TQP'] - $array[$getprize_list[$i]['PrizeName']];
                                $cp = round((($array[$getprize_list[$i]['PrizeName']] / $getprize_list[$i]['TQP']) * 100), 4);
                                $data4 .= str_replace(",", "", $getprize_list[$i]['PrizeName']) . "," .$getprize_list[$i]['TQP'] . "," . $array[$getprize_list[$i]['PrizeName']] . "," . $pbe . "," . $cp . "%" . "\r\n";
		        }
                }
                else
                {
                        $data4 .= "No Records Found\r\n";
                }
                fwrite($fp,$data4);
                header('Content-type: text/csv');
	        header("Content-Disposition: attachment; filename=".'Scratch_Card_Batch_Usage.csv');
                header('Pragma: public');
	        readfile('../csv/Scratch_Card_Batch_Usage.csv');
	        exit;
        }
        else
        {
                echo "<script>alert('Cannot create file.')</script>";
        }
    }
}
?>