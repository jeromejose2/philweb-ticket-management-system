<?php
/*
 * Created By : MTCC
 * Modified By : NDA
 * Added On : January 17, 2012
 * Purpose : Process for scratch card batch usage 
 */
$pagesubmenuid = 42;
$stylesheets[] = "css/default.css";
$javascripts[] = "jscripts/validations.js";
$javascripts[] = "jscripts/jquery-1.5.2.min.js";

App::LoadModuleClass("TicketManagementCM", "TMGameManagement");
App::LoadModuleClass("TicketManagementCM", "TMProducts");
App::LoadModuleClass("TicketManagementCM", "TMDecks");
App::LoadModuleClass("TicketManagementCM", "TMWinnings");
App::LoadModuleClass("TicketManagementCM", "TMTickets");
App::LoadModuleClass("TicketManagementCM", "TMGameImport");
App::LoadControl("TextBox");
App::LoadControl("Button");
App::LoadControl("ComboBox");
App::LoadControl("DataTable");
App::LoadControl("Hidden");

/*Added by Arlene R. Salazar 01-26-2012*/
App::LoadLibrary("fpdf/fpdf.php");

$fproc = new FormsProcessor();

$TMDecks = new TMDecks();
$TMGames = new TMGameManagement();
$TMProducts = new TMProducts();
$TMWinnings = new TMWinnings();
$TMTickets = new TMTickets();
$TMGameImport = new TMGameImport();

//get list of game names
$gamenames = $TMProducts->SelectAllGameName();
$gamename_list = new ArrayList();
$gamename_list->AddArray($gamenames);
$ddlgamename = new ComboBox("ddlgamename","ddlgamename","Game Name: ");
$ddlgamename->Args = "onchange = 'javascript: get_gamenumber();'";
$gamenameopt = null;
$gamenameopt[] = new ListItem("Please select", "", true);
$ddlgamename->Items = $gamenameopt;
$ddlgamename->DataSource = $gamename_list;
$ddlgamename->DataSourceText = "ProductName";
$ddlgamename->DataSourceValue = "ProductID";
$ddlgamename->DataBind();
$ddlgamename->ShowCaption = true;

$cboGameID = new ComboBox("cboGameID", "cboGameID", "Game Number : ");
$cboGameID->Args = "onchange='get_batchID();'";
$cboGameID->ShowCaption = true;
$options = null;
$options[] = new ListItem("Please select", "", true);
$cboGameID->Items = $options;

$ddlgamebatch = new ComboBox("ddlgamebatch", "ddlgamebatch", "Game Batch : ");
$ddlgamebatch->ShowCaption = true;
$options = null;
$options[] = new ListItem("Please Select", "", true);
$ddlgamebatch->Items = $options;

$xmltype = new Hidden("xmltype","xmltype","xmltype");

$btnSubmit = new Button("btnSubmit", "btnSubmit", "Submit");
$btnSubmit->IsSubmit = true;
$btnSubmit->Args = "onclick='javascript: return batchusagevalidation();'";

$btnExport = new Button("btnExport","btnExport","Export to PDF");
$btnExport->IsSubmit = true;

$fproc->AddControl($cboGameID);
$fproc->AddControl($btnSubmit);
$fproc->AddControl($ddlgamename);
$fproc->AddControl($ddlgamebatch);
$fproc->AddControl($xmltype);
$fproc->AddControl($btnExport);
$fproc->ProcessForms();

if ($fproc->IsPostBack)
{
    $cboGameID->ClearItems();
    $options = null;
//    $options[] = new ListItem("ALL", "0");
    $options[] = new ListItem("Please Select", "");
    $cboGameID->Items = $options;
    $gamenumbers = $TMProducts->SelectGameNumPerGameName($ddlgamename->SubmittedValue);
    $gamename_list = new ArrayList();
    $gamename_list->AddArray($gamenumbers);
    $cboGameID->DataSource = $gamename_list;
    $cboGameID->DataSourceText = "GameNumber";
    $cboGameID->DataSourceValue = "GameID";
    $cboGameID->DataBind();
    $cboGameID->SetSelectedValue($cboGameID->SubmittedValue);
    
    $ddlgamebatch->ClearItems();
    $options = null;
    $options[] = new ListItem("ALL", "0");
//    $options[] = new ListItem("Please Select", "");
    $ddlgamebatch->Items = $options;
    $gamebatches = $TMProducts->SelectBatchIDPerGameNum($cboGameID->SubmittedValue,$xmltype->SubmittedValue);
    $gamebatch_list = new ArrayList();
    $gamebatch_list->AddArray($gamebatches);
    $ddlgamebatch->DataSource = $gamebatch_list;
    $ddlgamebatch->DataSourceText = "BatchID";
    $ddlgamebatch->DataSourceValue = "GameBatchID";
    $ddlgamebatch->DataBind();
    $ddlgamebatch->SetSelectedValue($ddlgamebatch->SubmittedValue);
    
    if ($btnSubmit->SubmittedValue == "Submit")
    {      
        // Query for Date Imported
        $di = $TMGameImport->GetDateImported($cboGameID->SubmittedValue, $ddlgamebatch->SelectedText);
        // Query  for Batch Consumption
        $batchsize = $TMDecks->GetBatchSize($cboGameID->SubmittedValue, $ddlgamebatch->SelectedText);
        $twe = $TMWinnings->GetTotalWinningEntries($cboGameID->SubmittedValue, $ddlgamebatch->SelectedText);
        $teu = $TMDecks->GetTotalEntriesUsed($cboGameID->SubmittedValue, $ddlgamebatch->SelectedText);
        $tce = $TMTickets->GetCancelledEntries($cboGameID->SubmittedValue, $ddlgamebatch->SelectedText);
        
        // Query for Prize Amount Consumption
        $tp = $TMWinnings->GetTotalPrizeAmount($cboGameID->SubmittedValue, $ddlgamebatch->SelectedText); 
        $pu = $TMWinnings->GetTotalPrizeUsed($cboGameID->SubmittedValue, $ddlgamebatch->SelectedText);
        
        // Query for Prize Counter
        $getprize = $TMWinnings->GetPrizeCounter($cboGameID->SubmittedValue, $ddlgamebatch->SubmittedValue);
        $getprize_list = new ArrayList();
        $getprize_list->AddArray($getprize);
        $getprizeclaimed = $TMWinnings->GetPrizeCounterClaimed($cboGameID->SubmittedValue, $ddlgamebatch->SubmittedValue);
        $getprizeclaimed_list = new ArrayList();
        $getprizeclaimed_list->AddArray($getprizeclaimed);
    }

	//Added by ARS 01-26-2012
	if($btnExport->SubmittedValue == "Export to PDF")
	{
        //get game details
        $gamename = $ddlgamename->SelectedText;
        $gameno = $cboGameID->SelectedText;
        $gamebatch = $ddlgamebatch->SelectedText;
            
	// Query for Date Imported
        $di = $TMGameImport->GetDateImported($cboGameID->SubmittedValue, $ddlgamebatch->SelectedText);
        // Query  for Batch Consumption
        $batchsize = $TMDecks->GetBatchSize($cboGameID->SubmittedValue, $ddlgamebatch->SelectedText);
        $twe = $TMWinnings->GetTotalWinningEntries($cboGameID->SubmittedValue, $ddlgamebatch->SelectedText);
        $teu = $TMDecks->GetTotalEntriesUsed($cboGameID->SubmittedValue, $ddlgamebatch->SelectedText);
        $tce = $TMTickets->GetCancelledEntries($cboGameID->SubmittedValue, $ddlgamebatch->SelectedText);
        
        // Query for Prize Amount Consumption
        $tp = $TMWinnings->GetTotalPrizeAmount($cboGameID->SubmittedValue, $ddlgamebatch->SelectedText); 
        $pu = $TMWinnings->GetTotalPrizeUsed($cboGameID->SubmittedValue, $ddlgamebatch->SelectedText);
        
        // Query for Prize Counter
        $getprize = $TMWinnings->GetPrizeCounter($cboGameID->SubmittedValue, $ddlgamebatch->SubmittedValue);
        $getprize_list = new ArrayList();
        $getprize_list->AddArray($getprize);
        $getprizeclaimed = $TMWinnings->GetPrizeCounterClaimed($cboGameID->SubmittedValue, $ddlgamebatch->SubmittedValue);
        $getprizeclaimed_list = new ArrayList();
        $getprizeclaimed_list->AddArray($getprizeclaimed);

        $pdf = new FPDF('L','mm','Legal');
        $pdf->AddPage();

        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(0,5,"Scratch Card Batch Usage",0,0,'C');

        $pdf->Ln(8);
        
        $pdf->SetFont('Arial','B',9);
        $pdf->SetX(40);
        $pdf->Cell(40,5,'Game Name:',1,0,'C');
        $pdf->SetFont('Arial','',9);
        $pdf->SetX(80);
        $pdf->Cell(40,5,$gamename,1,0,'C');
        $pdf->SetFont('Arial','B',9);
        $pdf->SetX(120);
        $pdf->Cell(40,5,'Game Number:',1,0,'C');
        $pdf->SetFont('Arial','',9);
        $pdf->SetX(160);
        $pdf->Cell(40,5,$gameno,1,0,'C');
        $pdf->SetFont('Arial','B',9);
        $pdf->SetX(200);
        $pdf->Cell(40,5,'Game Batch:',1,0,'C');
        $pdf->SetFont('Arial','',9);
        $pdf->SetX(240);
        $pdf->Cell(40,5,$gamebatch,1,0,'C');
        
        $pdf->Ln(8);
        $pdf->Ln(8);

        $pdf->SetFont('Arial','B',9);
        $pdf->SetX(40);
        $pdf->Cell(40,5,'Date Imported :',1,0,'C');
        $pdf->SetFont('Arial','',9);
        $pdf->SetX(80);
        $pdf->Cell(40,5,$di[0]["DateImported"],1,1,'C');

        $pdf->Ln(8);

        $pdf->SetFont('Arial','B',9);
        $pdf->SetX(40);
        $pdf->Cell(270,5,'BATCH CONSUMPTION',1,1,'C');
        $pdf->SetX(40);
        $pdf->Cell(40,5,'Batch Size',1,0,'C');
        $pdf->SetX(80);
        $pdf->Cell(40,5,'Total Winning Entries',1,0,'C');
        $pdf->SetX(120);
        $pdf->Cell(40,5,'Total Entries Used',1,0,'C');
        $pdf->SetX(160);
        $pdf->Cell(40,5,'Total Canceled Entries',1,0,'C');
        $pdf->SetX(200);
        $pdf->Cell(40,5,'Balance Entries',1,0,'C');
        $pdf->SetX(240);
        $pdf->Cell(70,5,'Batch Consumption Percentage',1,1,'C');

        $pdf->SetFont('Arial','',9);
        if(count($batchsize) > 0)
        {
            for($i = 0 ; $i < count($batchsize) ; $i++)
            {
                    $be = (($batchsize[$i]['DeckSize'] - $teu[$i]['EntriesUsed']) - $tce[$i]['CancelledTicketCount']);
                    $bcp = round((($teu[$i]['EntriesUsed'] / $batchsize[$i]['DeckSize']) * 100), 4);

                    $pdf->SetX(40);
                    $pdf->Cell(40,5,number_format($batchsize[$i]['DeckSize'] , 0 , '' , ','),1,0,'C');
                    $pdf->Cell(40,5,number_format($twe[$i]['WinningsCount'] , 0 , '' , ','),1,0,'C');
                    $pdf->Cell(40,5,number_format($teu[$i]['EntriesUsed'] , 0 , '' , ','),1,0,'C');
                    $pdf->Cell(40,5,number_format($tce[$i]['CancelledTicketCount'] , 0 , '' , ','),1,0,'C');
                    $pdf->Cell(40,5,number_format($be , 0 , '' , ','),1,0,'C');
                    $pdf->Cell(70,5,$bcp . "%",1,1,'C');
            }
        }
        else
        {
            $pdf->SetX(40);
            $pdf->Cell(270,5,'No records found',1,1,'C');
        }

        $pdf->Ln(8);

        $pdf->SetFont('Arial','B',9);
        $pdf->SetX(40);
        $pdf->Cell(190,5,'PRIZE AMOUNT CONSUMPTION',1,1,'C');
        $pdf->SetX(40);
        $pdf->Cell(40,5,'Total Prize',1,0,'C');
        $pdf->SetX(80);
        $pdf->Cell(40,5,'Prize Used',1,0,'C');
        $pdf->SetX(120);
        $pdf->Cell(40,5,'Prize Left',1,0,'C');
        $pdf->SetX(160);
        $pdf->Cell(70,5,'Prize Consumption Percentage',1,1,'C');	
		
        $pdf->SetFont('Arial','',9);
        if(count($tp) > 0)
        {
            for($i = 0 ; $i < count($tp) ; $i++)
            {
                    $pl = ($tp[$i]['TotalPrize'] - $pu[$i]['TotalPrizeUsed']);
            $pcp = round((($pu[$i]['TotalPrizeUsed'] / $tp[$i]['TotalPrize']) * 100), 4);

                    $pdf->SetX(40);
                    $pdf->Cell(40,5,'$'.number_format($tp[$i]['TotalPrize'] , 0 , '' , ','),1,0,'C');
                    $pdf->Cell(40,5,'$'.number_format($pu[$i]['TotalPrizeUsed'] , 0 , '' , ','),1,0,'C');
                    $pdf->Cell(40,5,'$'.number_format($pl , 0 , '' , ','),1,0,'C');
                    $pdf->Cell(70,5,$pcp . "%",1,1,'C');	
            }
        }
        else
        {
            $pdf->SetX(40);
            $pdf->Cell(190,5,'No records found',1,1,'C');
        }

        $pdf->Ln(8);

        $pdf->SetFont('Arial','B',9);
        $pdf->SetX(40);
        $pdf->Cell(210,5,'PRIZE COUNTER',1,1,'C');
        $pdf->SetX(40);
        $pdf->Cell(40,5,'Prize Type',1,0,'C');
        $pdf->SetX(80);
        $pdf->Cell(40,5,'Total Quantity of Prize',1,0,'C');
        $pdf->SetX(120);
        $pdf->Cell(40,5,'Total Quantity Used',1,0,'C');
        $pdf->SetX(160);
        $pdf->Cell(40,5,'Prize Balance Entries',1,0,'C');
        $pdf->SetX(200);
        $pdf->Cell(50,5,'Consumption Percentage',1,1,'C');

        $pdf->SetFont('Arial','',9);
        if(count($getprize_list) > 0)
        {
            for($i = 0 ; $i < count($getprize_list) ; $i++)
            {
                if ($getprize_list[$i]['TQP'] == "")
                { 
                        $getprize_list[$i]['TQP'] = 0;
                }
                if ($getprizeclaimed_list[$i]['TQU'] == "")
                { 
                        $getprizeclaimed_list[$i]['TQU'] = 0;
                }
                $pbe = $getprize_list[$i]['TQP'] - $getprizeclaimed_list[$i]['TQU'];
                $cp = round((($getprizeclaimed_list[$i]['TQU'] / $getprize_list[$i]['TQP']) * 100), 4);

                $pdf->SetX(40);
                $pdf->Cell(40,5,$getprize_list[$i]['PrizeName'],1,0,'C');
                $pdf->Cell(40,5,number_format($getprize_list[$i]['TQP'] , 0 , '' , ','),1,0,'C');
                $pdf->Cell(40,5,number_format($getprizeclaimed_list[$i]['TQU'] , 0 , '' , ','),1,0,'C');
                $pdf->Cell(40,5,number_format($pbe , 0 , '' , ','),1,0,'C');
                $pdf->Cell(50,5,$cp . "%",1,1,'C');
            }
        }
        else
        {
                $pdf->SetX(40);
                $pdf->Cell(210,5,'No records found',1,1,'C');
        }

        $pdf->Output('Scratch Card Batch Usage','D');
        Header('Content-Type: application/pdf');
    }
}
?>
