<?php
/*
 * Added by: Arlene R. Salazar on 10-06-2011
 * Purpose: Controller for book transfer update
 */
$pagesubmenuid = 35;
$stylesheets[] = "css/default.css";
$javascripts[] = "jscripts/validations.js";
$javascripts[] = "jscripts/jquery-1.5.2.min.js";

App::LoadModuleClass("TicketManagementCM", "TMDecks");
App::LoadModuleClass("TicketManagementCM", "TMAccounts");
App::LoadModuleClass("TicketManagementCM", "TMAuditLog"); 
App::LoadModuleClass("TicketManagementCM", "TMBookTransferHistory");

App::LoadControl("TextBox");
App::LoadControl("ComboBox");
App::LoadControl("Button");
App::LoadControl("Hidden");

$tmdecks = new TMDecks();
$tmaccounts = new TMAccounts();
$tmauditlog = new TMAuditLog();
$tmbooktranshist = new TMBookTransferHistory();

$frmbooktransupdate = new FormsProcessor();

$ddlassingedto = new ComboBox("ddlassingedto","ddlassingedto","Assigned To:");
$litem = null;
$litem[] = new ListItem("-----","");
$ddlassingedto->Items = $litem;


$btnProcess = new Button("btnProcess","btnProcess","Process");
$btnProcess->IsSubmit = true;
$btnProcess->Args = "onclick = 'javascript: return checkBookTransUpdate();'";

$btnOkay = new Button("btnOkay","btnOkay","OKAY");
$btnOkay->Args = "onclick = 'javascript: return redirectToBookTransfer();'";

$btnConfirm = new Button("btnConfirm","btnConfirm","Confirm");
$btnConfirm->IsSubmit = true;
$btnConfirm->Args = "onclick = 'javascript: return ValidateAdminConfirmation();'";

$txtusername = new TextBox("txtusername","txtusername","Username");
$txtusername->Length = 20;
//$txtusername->Args="onkeypress='javascript: return alphanumeric(event);' autocomplete='off'";

$txtpassword = new TextBox("txtpassword","txtpassword","Password");
$txtpassword->Length = 20;
$txtpassword->Password = true;
$txtpassword->Args="onkeypress='javascript: return disableSpace(event);' autocomplete='off'";

$btnError = new Button("btnError","btnError","Okay");
$btnError->Args = "onclick = 'javascript: return ShowAdminConfirmation();'";

$hiddenassignedto = new Hidden("hiddenassignedto","hiddenassignedto","Hidden assigned to");

$hiddenassignedtouname = new Hidden("hiddenassignedtouname","hiddenassignedtouname","Hidden assigned to username");

$hiddenbookids = new Hidden("hiddenbookids","hiddenbookids","Hidden book ids");

$frmbooktransupdate->AddControl($ddlassingedto);
$frmbooktransupdate->AddControl($btnProcess);
$frmbooktransupdate->AddControl($btnOkay);
$frmbooktransupdate->AddControl($btnConfirm);
$frmbooktransupdate->AddControl($txtusername);
$frmbooktransupdate->AddControl($txtpassword);
$frmbooktransupdate->AddControl($hiddenassignedto);
$frmbooktransupdate->AddControl($hiddenassignedtouname);
$frmbooktransupdate->AddControl($hiddenbookids);

$frmbooktransupdate->ProcessForms();
if($frmbooktransupdate->IsPostBack)
{
    if($frmbooktransupdate->GetPostVar("chkbooktrans"))
    {
        //get deckid to update
        $bookids = $frmbooktransupdate->GetPostVar("chkbooktrans");
        $deckstatus = $tmdecks->SelectStatus($bookids);
        $hiddenassignedto->Text = $deckstatus[0]["AssignedToAID"];
        $hiddenassignedtouname->Text = $deckstatus[0]["Username"];
        $assignedtouname = $deckstatus[0]["Username"];
        $_SESSION['bookids'] = $bookids;
    }

    $deckstatus = $tmdecks->SelectStatus($_SESSION['bookids']);
    $accounts = $tmaccounts->SelectAllExceptCurrent($deckstatus[0]["AssignedToAID"]);
    $accounts_list = new ArrayList();
    $accounts_list->AddArray($accounts);
    $ddlassingedto->DataSource = $accounts_list;
    $ddlassingedto->DataSourceText = "Name";
    $ddlassingedto->DataSourceValue = "AID";
    $ddlassingedto->DataBind();

    if($btnProcess->SubmittedValue == "Process")
    {
        $assignedtouname = $hiddenassignedtouname->Text;
        $ddlassingedto->SetSelectedValue($ddlassingedto->SubmittedValue);
        $accttype = $_SESSION['accttype'];
        if ($accttype != 1)
        { 
            $confirm = "ok";         
        } else {
            $result = transferbooks($_SESSION['bookids'], $ddlassingedto->SubmittedValue, $_SESSION['acctid'], $hiddenassignedto->SubmittedValue);
            $successtitle = $result["title"];
            $successmsg = $result["msg"];
        }
    }
    
    if($btnConfirm->SubmittedValue == "Confirm")
    {
        $assignedtouname = $hiddenassignedtouname->Text;
        $ddlassingedto->SetSelectedValue($ddlassingedto->SubmittedValue);
        $username = mysql_escape_string(trim($txtusername->Text));
        $password = mysql_escape_string(trim($txtpassword->Text));
        $isAdmin = $tmaccounts->CheckIfAdminAccount($username,$password);
        if(count($isAdmin) > 0)
        {
            $result = transferbooks($_SESSION['bookids'], $ddlassingedto->SubmittedValue, $_SESSION['acctid'], $hiddenassignedto->SubmittedValue);
            $successtitle = $result["title"];
            $successmsg = $result["msg"];
        }
        else
        {
            $txtusername->Text = "";
            $txtpassword->Text = "";
            $logintitle = "ERROR!";
            $loginmsg = "Please enter a valid administrator account.";
        }
    }
    $tbldata = $tmdecks->SelectDeckByID($_SESSION['bookids']);
    $bookslist = new ArrayList();
    $bookslist->AddArray($tbldata);

}
else
{
    URL::Redirect('booktransfer.php');
}

function transferbooks($bookid, $assignedto, $aid, $hidden)
{
    App::LoadModuleClass("TicketManagementCM", "TMDecks");
    App::LoadModuleClass("TicketManagementCM", "TMAccounts");
    App::LoadModuleClass("TicketManagementCM", "TMAuditLog"); 
    App::LoadModuleClass("TicketManagementCM", "TMBookTransferHistory");

    $tmdecks = new TMDecks();
    $tmaccounts = new TMAccounts();
    $tmauditlog = new TMAuditLog();
    $tmbooktranshist = new TMBookTransferHistory();
    
    $updatebooktrans = $tmdecks->UpdateDeckDtlsBookTrans($bookid, $assignedto, $aid);
    if($tmdecks->HasError)
    {
        $errortitle = "ERROR!";
        $errormsg = "Error in transferring book: " . $tmdecks->getError();
    }
    else
    {
        $reassigneddtls = $tmaccounts->SelectByAccountID($assignedto);
        $fromreassigne =  $tmaccounts->SelectByAccountID($hidden);
        $btnCancel->Text = "Okay";
        $errortitle = "SUCCESSFUL TRANSFER!";
        $errormsg = "Book/s item has been successfully transferred from " . $fromreassigne[0]["UserName"] . " account to ". $reassigneddtls[0]["UserName"] ." account.";

        //insert to booktransferhistory
        $bookid = $_SESSION['bookids'];
        for($i = 0 ; $i < count($bookid) ; $i++)
        {
            $history["BookID"] = $bookid[$i];
            $history["FromAssignedAID"] = $hidden;
            $history["ToAssignedAID"] = $reassigneddtls[0]["AID"];
            $history["DateCreated"] = 'now_usec()';
            $history["TransferredByAID"] = $_SESSION['acctid'];
            $history["ApprovedByAID"] = $isAdmin[0]["AID"];
            $booktranshist = $tmbooktranshist->Insert($history);
            if($tmbooktranshist->HasError)
            {
                $errortitle = "ERROR!";
                $errormsg = "Error in inserting book transfer history: " . $tmbooktranshist->getError();
            }
        }
        
        //insert to audittrail
        $auditdtls["AuditTrailFunctionID"] = '33';
        $auditdtls["SessionID"] = $_SESSION['sid'];
        $auditdtls["AID"] = $_SESSION['acctid'];
        $auditdtls["TransDetails"] = "Transferred from " . $hidden . " to " . $assignedto . " Book ID/S : " . join(",",$_SESSION['bookids']);
        $auditdtls["TransDateTime"] = 'now_usec()';
        $auditdtls["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
        $auditlog = $tmauditlog->Insert($auditdtls);
        if($tmauditlog->HasError)
        {
            $errortitle = "ERROR!";
            $errormsg = "Error in inserting auditlog: " . $tmauditlog->getError();
        }
    }
    
    $array = array('title'=>$errortitle, 'msg'=>$errormsg);
    return $array;
}
?>