<?php
/*
 * Added by: Arlene R. Salazar on 10-07-2011
 * Purpose: Controller for book transfer history
 */
$pagesubmenuid = 36;
$stylesheets[] = "css/default.css";
$stylesheets[] = "css/datepicker.css";
$javascripts[] = "jscripts/datetimepicker.js";
$javascripts[] = "jscripts/validations.js";
$javascripts[] = "jscripts/jquery-1.5.2.min.js";

App::LoadModuleClass("TicketManagementCM", "TMGameManagement");
App::LoadModuleClass("TicketManagementCM", "TMBookTransferHistory");
App::LoadModuleClass("TicketManagementCM", "TMProducts");
//App::LoadModuleClass("TicketManagementCM", "Pdf");
App::LoadControl("PagingControl2");

App::LoadControl("TextBox");
App::LoadControl("ComboBox");
App::LoadControl("Button");
App::LoadControl("Hidden");
//App::LoadControl("Pdf");

$tmgames = new TMGameManagement();
$tmbooktranshist = new TMBookTransferHistory();
$tmproducts = new TMProducts();

$frmBookTransHist = new FormsProcessor();

$gamenames = $tmproducts->SelectAllGameName();
$gamename_list = new ArrayList();
$gamename_list->AddArray($gamenames);
$ddlgamename = new ComboBox("ddlgamename","ddlgamename","Game Name: ");
$ddlgamename->Args = "onchange = 'javascript: get_gamenumber();'";
$gamenameopt = null;
$gamenameopt[] = new ListItem("ALL", "0", true);
$ddlgamename->Items = $gamenameopt;
$ddlgamename->DataSource = $gamename_list;
$ddlgamename->DataSourceText = "ProductName";
$ddlgamename->DataSourceValue = "ProductID";
$ddlgamename->DataBind();

$games = $tmgames->SelectByGameType();
$games_list = new ArrayList();
$games_list->AddArray($games);
$ddlgametype = new ComboBox("ddlgametype", "ddlgametype", "Game Number: ");
$ddlgametype->Args = "onchange='javascript: get_batchID(); get_gamename();'";
$litemgmetype = null;
$litemgmetype[] = new ListItem("ALL", "0", true);
$ddlgametype->Items = $litemgmetype;
$ddlgametype->DataSource = $games_list;
$ddlgametype->DataSourceText = "GameNumber";
$ddlgametype->DataSourceValue = "GameID";
$ddlgametype->DataBind();

$ddlgamebatch = new ComboBox("ddlgamebatch","ddlgamebatch","Game Batch:");
$gamebatch = null;
$gamebatch[] = new ListItem("ALL","0", true);
$ddlgamebatch->Items  = $gamebatch;

$flag = new Hidden("flag","flag","flag");
$xmltype = new Hidden("xmltype","xmltype","xmltype");

$btnExport = new Button("export","export","Export to PDF");
$btnExport->IsSubmit = true;

$btnExportCSV = new Button("btnExportCSV","btnExportCSV","Export to CSV");
$btnExportCSV->IsSubmit = true;

$txtDateFr = new TextBox("txtDateFr","txtDateFr","Date From");
$txtDateFr->ReadOnly = true;
$txtDateFr->Style = "text-align: center";
$txtDateFr->Text = date("Y-m-d");

$txtDateTo = new TextBox("txtDateTo","txtDateTo","Date To");
$txtDateTo->ReadOnly = true;
$txtDateTo->Style = "text-align: center";
$txtDateTo->Text = date("Y-m-d");

$txtprodtype = new TextBox("txtprodtype","txtprodtype","Game Name");
$txtprodtype->ReadOnly = true;

$btnSubmit = new Button("btnSubmit","btnSubmit","Submit");
$btnSubmit->IsSubmit = true;
$btnSubmit->Enabled = true;
$btnSubmit->Args = "onclick = 'javascript: return booktranshistvalidation();'";

$itemsperpage = 50;
$pgcon = new PagingControl2($itemsperpage, 1);
$pgcon->URL = "javascript:ChangePage(%currentpage);";
$pgcon->ShowMoveToFirstPage = true;
$pgcon->ShowMoveToLastPage = true;
$pgcon->PageGroup = 5;

$hiddenflag = new Hidden("hiddenflag","hiddenflag","Hidden Flag");

$frmBookTransHist->AddControl($txtDateFr);
$frmBookTransHist->AddControl($txtDateTo);
$frmBookTransHist->AddControl($ddlgametype);
$frmBookTransHist->AddControl($txtprodtype);
$frmBookTransHist->AddControl($btnSubmit);
$frmBookTransHist->AddControl($ddlgamename);
$frmBookTransHist->AddControl($hiddenflag);
$frmBookTransHist->AddControl($ddlgamebatch);
$frmBookTransHist->AddControl($btnExport);
$frmBookTransHist->AddControl($btnExportCSV);
$frmBookTransHist->AddControl($xmltype);
$frmBookTransHist->AddControl($flag);
$frmBookTransHist->ProcessForms();
if($frmBookTransHist->IsPostBack)
{
    if ($flag->SubmittedValue == 1)
    {
        $ddlgametype->Args = "onchange='javascript: get_batchID(); '";
        $ddlgametype->ClearItems();
        $prodid1 = $ddlgamename->SubmittedValue;
        $gamenumbers = $tmproducts->SelectGameNumPerGameName($prodid1);
        $gamenumbers_list1 = new ArrayList();
        $gamenumbers_list1->AddArray($gamenumbers);       
        $litemgmetype1 = null;
        $litemgmetype1[] = new ListItem("ALL", "0", false);
        $ddlgametype->Items = $litemgmetype1;
        $ddlgametype->DataSource = $gamenumbers_list1;
        $ddlgametype->DataSourceText = "GameNumber";
        $ddlgametype->DataSourceValue = "GameID";
        $ddlgametype->DataBind();
        $ddlgametype->SetSelectedValue($ddlgametype->SubmittedValue);
    }
    if ($flag->SubmittedValue == 2)
    {
        $ddlgamename->Args = "onchange='javascript:'";          
        $ddlgamename->ClearItems();
        $gamenum1 = $ddlgametype->SubmittedValue;
        $gamenames = $tmproducts->SelectGameNamePerGameNum($gamenum1);
        $gamename_list1 = new ArrayList();
        $gamename_list1->AddArray($gamenames);       
        $litemgmetype1 = null;
        $litemgmetype1[] = new ListItem("ALL", "0", false);
        $ddlgamename->Items = $litemgmetype1;
        $ddlgamename->DataSource = $gamename_list1;
        $ddlgamename->DataSourceText = "ProductName";
        $ddlgamename->DataSourceValue = "ProductID";
        $ddlgamename->DataBind();
        $ddlgamename->SetSelectedValue($ddlgametype->SubmittedValue);
    }
 
    $ddlgametype->SetSelectedValue($ddlgametype->SubmittedValue);
    $ddlgamename->SetSelectedValue($ddlgamename->SubmittedValue);
   
    $gamebatchID = $tmproducts->SelectBatchIDPerGameNum($ddlgametype->SelectedValue,$xmltype->SubmittedValue);
    $batchlist = new ArrayList();
    $batchlist->AddArray($gamebatchID);
    $gamebatch = null;
    $gamebatch[] = new ListItem("ALL","0", true);
    $ddlgamebatch->Items  = $gamebatch;
    $ddlgamebatch->DataSource = $batchlist;
    $ddlgamebatch->DataSourceText = "BatchID";
    $ddlgamebatch->DataSourceValue = "GameBatchID";
    $ddlgamebatch->DataBind();
    $ddlgamebatch->SetSelectedValue($ddlgamebatch->SubmittedValue);

    $and = "";
    if ($ddlgamename->SubmittedValue != 0) { $and.=" AND D.ProductID = '" . $ddlgamename->SubmittedValue . "'"; }
    if ($ddlgametype->SubmittedValue != 0) { $and.=" AND F.GameID = '" . $ddlgametype->SubmittedValue . "'"; }
    if ($ddlgamebatch->SubmittedValue != 0) { $and.=" AND J.GameBatchID = '".$ddlgamebatch->SubmittedValue."'"; }
    
    $where = " WHERE A.DateCreated >= '$txtDateFr->SubmittedValue"." 00:00:00"."'   AND A.DateCreated <= '$txtDateTo->SubmittedValue"." 23:59:59' "." $and";
    if($btnSubmit->SubmittedValue == "Submit")
    {
        $pgcon->SelectedPage = 1;
    }
    $histlist = $tmbooktranshist->SelectBookTransHist($where);
    $pgcon->Initialize($itemsperpage, count($histlist));
    $pgBookTransHist = $pgcon->PreRender();
    $wherelimit = $where . " ORDER BY A.DateCreated DESC LIMIT " . ($pgcon->SelectedItemFrom-1) . "," . $itemsperpage;
    $arrtmd = $tmbooktranshist->SelectBookTransHistWithLimit($wherelimit);
    $booktranshistlist = new ArrayList();
    $booktranshistlist->AddArray($arrtmd);
    
	/*
	* Added by: Sheryl S. Basbas
	* Date: Feb 6, 2012
	*/
     if($btnExport->SubmittedValue == "Export to PDF")
    {
       
        $record= $tmbooktranshist->SelectBookTransHistWithLimit($where);
        $cancel_list = new ArrayList();
        $cancel_list->AddArray($record);
        
        $pdf = new PDF(L,mm,Legal);
        $pdf->AliasNbPages();        
        $title = 'Book Reassignment History';
        $pdf->SetTitle($title);
        $lengths = array(20, 25, 40, 25, 45, 45, 45, 45,45);
        // Column headings
        $header = array('Game Name', 'Game Number', 'Game Batch',  'Book Number','Originally Assigned', 'Transferred To', 'Transferred By', 'Approved By','Transferred Date and Time');
        $cols = array('ProductName', 'GameNumber', 'BatchID', 'BookNumber','AssignedFrom', 'AssignedTo', 'TransferredBy', 'ApprovedBy', 'DateReassigned');
             	
        // Data loading
        $data = $pdf->LoadData($cancel_list);      
        $pdf->SetFont('Arial','',9);   
        $pdf->AddPage();
        $pdf->FancyTable($header,$data,$lengths,$cols);
        $pdf->Output('Book Reassignment History','D');
    }
    /*Added by: Sheryl S. Basbas */  

	/*
	* Added by: Arlene R. Salazar
	* Date: March 1, 2012
        * Modified by: Mark Kenneth Esguerra 
        * Date: April 25, 2013
	*/
	if($btnExportCSV->SubmittedValue == "Export to CSV")
    {
		$data = "";
		$record= $tmbooktranshist->SelectBookTransHistWithLimit($where);
        $cancel_list = new ArrayList();
        $cancel_list->AddArray($record);

		$fp = fopen("../csv/Book_Reassignment_History.csv","w");
		if($fp)
		{
			$header = "Game Name,Book Size,Game Number,Game Batch,Book Number,Originally Assigned,Old Invoice Number,Transferred To,New Invoice Number,Transferred By,Approved By,Transferred Date and Time\r\n";
			fwrite($fp,$header);
			if(count($cancel_list) > 0)
			{
				for($i = 0 ; $i < count($cancel_list) ; $i++)
				{
                                    $dateTime = new DateTime($cancel_list[$i]['DateReassigned']);
					$data .= $cancel_list[$i]['ProductName'] . ",".$cancel_list[$i]['BookTicketCount'] ."," . $cancel_list[$i]['GameNumber'] . "," . $cancel_list[$i]['BatchID'] . "," . $cancel_list[$i]['BookNumber'] . "," . $cancel_list[$i]['AssignedFrom']. "," . $cancel_list[$i]['OldInvoiceNo'] . "," . $cancel_list[$i]['AssignedTo'] . "," . $cancel_list[$i]['CurrentInvoiceNo'] . "," . $cancel_list[$i]['TransferredBy'] . "," . $cancel_list[$i]['ApprovedBy'] . "," . $dateTime->format('Y-m-d h:i:s A') . "\r\n";
				}
			}
			else
			{
				$data .= "No Records Found";
			}
			fwrite($fp,$data);
			header('Content-type: text/csv');
	        header("Content-Disposition: attachment; filename=".'Book_Reassignment_History.csv');
			header('Pragma: public');
	        readfile('../csv/Book_Reassignment_History.csv');
	        exit;
		}
		else
		{
			echo "<script>alert('Cannot create file.')</script>";
		}
	}
    /*Added by: Arlene R. Salazar */
}

            
        
    
?>
