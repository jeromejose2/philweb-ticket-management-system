<?php
/*
 * Added by: Arlene R. Salazar on 10-07-2011
 * Purpose: Controller for book transfer history
 */
$pagesubmenuid = 36;
$stylesheets[] = "css/default.css";
$stylesheets[] = "css/datepicker.css";
$javascripts[] = "jscripts/datetimepicker.js";
$javascripts[] = "jscripts/validations.js";
$javascripts[] = "jscripts/jquery-1.5.2.min.js";

App::LoadModuleClass("TicketManagementCM", "TMGameManagement");
App::LoadModuleClass("TicketManagementCM", "TMBookTransferHistory");
App::LoadModuleClass("TicketManagementCM", "TMProducts");
App::LoadControl("PagingControl2");

App::LoadControl("TextBox");
App::LoadControl("ComboBox");
App::LoadControl("Button");
App::LoadControl("Hidden");

$tmgames = new TMGameManagement();
$tmbooktranshist = new TMBookTransferHistory();
$tmproducts = new TMProducts();

$frmBookTransHist = new FormsProcessor();

$txtDateFr = new TextBox("txtDateFr","txtDateFr","Date From");
$txtDateFr->ReadOnly = true;
$txtDateFr->Style = "text-align: center";
$txtDateFr->Text = date("m/d/Y");

$txtDateTo = new TextBox("txtDateTo","txtDateTo","Date To");
$txtDateTo->ReadOnly = true;
$txtDateTo->Style = "text-align: center";
$txtDateTo->Text = date("m/d/Y");

$allgames = $tmgames->SelectAllActive();
$gameslist = new ArrayList();
$gameslist->AddArray($allgames);
$ddlgametype = new ComboBox("ddlgametype","ddlgametype","Game Number: ");
$ddlgametype->Args = "onchange='javascript: get_batchid(); get_gamename();'";
$options = null;
$options[] = new ListItem("Please Select","0");
$ddlgametype->Items = $options;
$ddlgametype->DataSource = $gameslist;
$ddlgametype->DataSourceText = "GameNumber";
$ddlgametype->DataSourceValue = "GameID";
$ddlgametype->DataBind();
//$ddlgametype->Args = "onchange='javascript: return get_product()'";

$txtprodtype = new TextBox("txtprodtype","txtprodtype","Game Name");
$txtprodtype->ReadOnly = true;

$btnSubmit = new Button("btnSubmit","btnSubmit","Submit");
$btnSubmit->IsSubmit = true;
$btnSubmit->Enabled = true;
$btnSubmit->Args = "onclick = 'javascript: return booktranshistvalidation();'";

//get list of game names
$gamenames = $tmproducts->SelectAllGameName();
$gamename_list = new ArrayList();
$gamename_list->AddArray($gamenames);

$ddlgamename = new ComboBox("ddlgamename","ddlgamename","Game Name: ");
$ddlgamename->Args = "onchange = 'javascript: get_gamenumber();'";
$gamenameopt = null;
$gamenameopt[] = new ListItem("Please select", "0", true);
$ddlgamename->Items = $gamenameopt;
$ddlgamename->DataSource = $gamename_list;
$ddlgamename->DataSourceText = "ProductName";
$ddlgamename->DataSourceValue = "ProductID";
$ddlgamename->DataBind();

$itemsperpage = 50;
$pgcon = new PagingControl2($itemsperpage, 1);
$pgcon->URL = "javascript:ChangePage(%currentpage);";
$pgcon->ShowMoveToFirstPage = true;
$pgcon->ShowMoveToLastPage = true;
$pgcon->PageGroup = 5;

$hiddenflag = new Hidden("hiddenflag","hiddenflag","Hidden Flag");

$ddlBatchID = new ComboBox("ddlBatchID","ddlBatchID","Game Batch");
$gamebatchopt = null;
$gamebatchopt[] = new ListItem("Please select", "0", true);
$ddlBatchID->Items = $gamebatchopt;

$frmBookTransHist->AddControl($txtDateFr);
$frmBookTransHist->AddControl($txtDateTo);
$frmBookTransHist->AddControl($ddlgametype);
$frmBookTransHist->AddControl($txtprodtype);
$frmBookTransHist->AddControl($btnSubmit);
$frmBookTransHist->AddControl($ddlgamename);
$frmBookTransHist->AddControl($hiddenflag);
$frmBookTransHist->AddControl($ddlBatchID);

$frmBookTransHist->ProcessForms();
if($frmBookTransHist->IsPostBack)
{
	if($hiddenflag->SubmittedValue == 1)
    {
        $ddlgametype->Args = "onchange='javascript: get_batchID(); '";
   
        $ddlgametype->ClearItems();
        $gamenameopt = null;
        $gamenameopt[] = new ListItem("Please select", "0", true);
        $ddlgametype->Items = $gamenameopt;
        $gamenum_perproduct = $tmproducts->SelectGameNumPerGameName($ddlgamename->SubmittedValue);
        $gamenum_perproduct_list = new ArrayList();
        $gamenum_perproduct_list->AddArray($gamenum_perproduct);
        $ddlgametype->DataSource = $gamenum_perproduct_list;
        $ddlgametype->DataSourceText = "GameNumber";
        $ddlgametype->DataSourceValue = "GameID";
        $ddlgametype->DataBind();
        $ddlgametype->SetSelectedValue($ddlgametype->SubmittedValue);
    }

    if($hiddenflag->SubmittedValue == 2)
    {
        $ddlgamename->Args = "onchange='javascript:'";
        $ddlgamename->ClearItems();

        $gamenames = $tmproducts->SelectGameNamePerGameNum($ddlgametype->SubmittedValue);

        $gamename_list1 = new ArrayList();
        $gamename_list1->AddArray($gamenames);
        $litemgmetype1 = null;
        $litemgmetype1[] = new ListItem("Please select", "0", false);
        $ddlGameName->Items = $litemgmetype1;

        $ddlgamename->DataSource = $gamename_list1;
        $ddlgamename->DataSourceText = "ProductName";
        $ddlgamename->DataSourceValue = "ProductID";
        $ddlgamename->DataBind();

        $ddlgamename->SetSelectedValue($ddlgamename->SubmittedValue);
    }

	$gamebatches = $tmproducts->SelectBatchIDPerGameNum($ddlgametype->SubmittedValue,1);
    $gamebatches_list = new ArrayList();
    $gamebatches_list->AddArray($gamebatches);
    $ddlBatchID->DataSource = $gamebatches_list;
    $ddlBatchID->DataSourceText = "BatchID";
    $ddlBatchID->DataSourceValue = "GameBatchID";
    $ddlBatchID->DataBind();
    $ddlBatchID->SetSelectedValue($ddlBatchID->SubmittedValue);

    //$where = " WHERE DATE_FORMAT(A.DateCreated,'%m-%d-%Y') BETWEEN '" . $txtDateFr->SubmittedValue . "' AND '" . $txtDateTo->SubmittedValue . "' AND F.GameID = '" . $ddlgametype->SubmittedValue . "'";
    $where = " WHERE DATE_FORMAT(A.DateCreated,'%Y-%m-%d') BETWEEN STR_TO_DATE('" . $txtDateFr->SubmittedValue . "','%m/%d/%Y') AND STR_TO_DATE('" . $txtDateTo->SubmittedValue . "','%m/%d/%Y') AND F.GameID = '" . $ddlgametype->SubmittedValue . "' AND J.GameBatchID = '".$ddlBatchID->SubmittedValue."'";
    if($btnSubmit->SubmittedValue == "Submit")
    {
        $pgcon->SelectedPage = 1;
    }
    $histlist = $tmbooktranshist->SelectBookTransHist($where);
    $pgcon->Initialize($itemsperpage, count($histlist));
    $pgBookTransHist = $pgcon->PreRender();
    $wherelimit = $where . " ORDER BY A.DateCreated DESC LIMIT " . ($pgcon->SelectedItemFrom-1) . "," . $itemsperpage;
    $arrtmd = $tmbooktranshist->SelectBookTransHistWithLimit($wherelimit);
    $booktranshistlist = new ArrayList();
    $booktranshistlist->AddArray($arrtmd);
}
?>
