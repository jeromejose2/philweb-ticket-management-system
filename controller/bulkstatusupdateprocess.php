<?php
/*
 * Created by Arlene R. Salazar on 01-16-2011
 * Purpose: controller for scratch card bulk update
 */
$pagesubmenuid = 43;
$stylesheets[] = "css/default.css";
$stylesheets[] = "css/datepicker.css";
$javascripts[] = "jscripts/validations.js";
$javascripts[] = "jscripts/datetimepicker.js";

App::LoadModuleClass("TicketManagementCM", "TMAccounts");
App::LoadModuleClass("TicketManagementCM", "TMDecks");
App::LoadModuleClass("TicketManagementCM", "TMGameManagement");
App::LoadModuleClass("TicketManagementCM", "TMProducts");
App::LoadModuleClass("TicketManagementCM", "TMBatchTransactionLog");
App::LoadModuleClass("TicketManagementCM", "TMGameBatches");

App::LoadControl("ComboBox");
App::LoadControl("Button");
App::LoadControl("PagingControl2");
App::LoadControl("Hidden");
App::LoadControl("TextBox");

$bulkstatusupdateform = new FormsProcessor();
$tmaccounts = new TMAccounts();
$tmproducts = new TMProducts();
$tmdecks = new TMDecks();
$tmgamemngt = new TMGameManagement();
$tmbatchtransactionlog = new TMBatchTransactionLog();
$tmgamebatches = new TMGameBatches();

$ddlgamename = new ComboBox("ddlgamename","ddlgamename","Game Name");
$ddlgamename->Args = "onchange = 'javascript: get_gamenumber();'";
$gamenameopt = null;
$gamenameopt[] = new ListItem("Please select", "0", true);
$ddlgamename->Items = $gamenameopt;
$arrGameNames = $tmproducts->SelectAllGameName();
$gamename_list = new ArrayList();
$gamename_list->AddArray($arrGameNames);
$ddlgamename->DataSource = $gamename_list;
$ddlgamename->DataSourceText = "ProductName";
$ddlgamename->DataSourceValue = "ProductID";
$ddlgamename->DataBind();

$ddlgametype = new ComboBox("ddlgametype","ddlgametype","Game Number");
$gamenumopt = null;
$gamenumopt[] = new ListItem("Please select", "0", true);
$ddlgametype->Items = $gamenameopt;
$ddlgametype->Args = "onchange='javascript: get_batchid();'";
$allgames = $tmgamemngt->SelectAllActive();
$gameslist = new ArrayList();
$gameslist->AddArray($allgames);
$ddlgametype->DataSource = $gameslist;
$ddlgametype->DataSourceText = "GameNumber";
$ddlgametype->DataSourceValue = "GameID";
$ddlgametype->DataBind();

$ddlgamebatch = new ComboBox("ddlgamebatch","ddlgamebatch","Batch ID");
$batchidopt = null;
$batchidopt[] = new ListItem("Please select", "0", true);
$ddlgamebatch->Items = $batchidopt;
$ddlgamebatch->Args = "onchange = 'javascript: get_booknumbers();'";

//get list of game card value
$cardprize = $tmgamemngt->SelectByCardValue();
$cardprize_list = new ArrayList();
$cardprize_list->AddArray($cardprize);


$ddlcardvalue = new ComboBox('ddlcardvalue','ddlcardvalue','');
$ddlcardvalue->Args = "onchange='javascript: get_status();'";
$cardlist = null;
$cardlist[] = new ListItem("Please select"," ",false);
//$cardlist[] = new ListItem("ALL","0",false);
$ddlcardvalue->Items = $cardlist;
$ddlcardvalue->DataSource = $cardprize_list;
$ddlcardvalue->DataSourceText = "CardPrice";
$ddlcardvalue->DataSourceValue = "CardPrice";
$ddlcardvalue->DataBind();

$btnSubmit = new Button("btnSubmit","btnSubmit","Submit");
$btnSubmit->Args = "onclick = 'return checkBulkStatusUpdate();'";
$btnSubmit->IsSubmit = true;

$ddlStartBookNum = new ComboBox("ddlStartBookNum","ddlStartBookNum","Start Book Number");
$startbooknum = null;
$startbooknum[] = new ListItem("Please select", "", true);
$ddlStartBookNum->Items = $startbooknum;

$ddlEndBookNum = new ComboBox("ddlEndBookNum","ddlEndBookNum","End Book Number");
$endbooknum = null;
$endbooknum[] = new ListItem("Please select", "", true);
$ddlEndBookNum->Items = $endbooknum;
$ddlEndBookNum->Args = "onchange='javascript: get_ddlassign();'";

$ddlStatus = new ComboBox("ddlStatus","ddlStatus","Status");
$litem = null;
$litem[] = new ListItem("Please select", "0");
$litem[] = new ListItem("On Stock", "3");
$litem[] = new ListItem("Assigned", "4");
$ddlStatus->Items = $litem;
$ddlStatus->Args = "onchange='get_gamename();'";//check_active();

$ddlAssignedTo = new ComboBox("ddlAssignedTo","ddlAssignedTo","AssignedTo");
$accounts = $tmaccounts->SelectByAccountType();
$accounts_list = new ArrayList();
$accounts_list->AddArray($accounts);
$assignedto = null;
$assignedto[] = new ListItem("Please select", "0", true);
$ddlAssignedTo->Items = $assignedto;
$ddlAssignedTo->DataSource = $accounts_list;
$ddlAssignedTo->DataSourceText = "Name";
$ddlAssignedTo->DataSourceValue = "AID";
$ddlAssignedTo->DataBind();
//$ddlAssignedTo->Enabled = false;

$ddlNewStatus = new ComboBox("ddlNewStatus","ddlNewStatus","New Status");
$ddlNewStatus->Args = "onchange='javascript: check_new_status();'";
$newStatus = null;
$newStatus[] = new ListItem("Please select", "0", true);
$ddlNewStatus->Items = $newStatus;

$ddlNewAssignedTo = new ComboBox("ddlNewAssignedTo","ddlNewAssignedTo","New Assigned To");
$ddlNewAssignedTo->Args = "onchange='javascript: get_address();'";
$litemassto = null;
$litemassto[] = new ListItem("Please select", "0", true);
$ddlNewAssignedTo->Items = $litemassto;
$ddlNewAssignedTo->DataSource = $accounts_list;
$ddlNewAssignedTo->DataSourceText = "Name";
$ddlNewAssignedTo->DataSourceValue = "AID";
$ddlNewAssignedTo->DataBind();
$ddlNewAssignedTo->Enabled = false;

$txtRetailerAddress = new TextBox("txtRetailerAddress","txtRetailerAddress","Address");
$txtRetailerAddress->ReadOnly = true;

$txtReleaseDate = new TextBox("txtReleaseDate","txtReleaseDate","Release Date");
$txtReleaseDate->ReadOnly = true;

$btnProcess = new Button("btnProcess","btnProcess","Process");
$btnProcess->IsSubmit = true;
$btnProcess->Args = "onclick='javascript: return checkNewStatus()'";

$btnConfirmProcess = new Button("btnConfirmProcess","btnConfirmProcess","Okay");
$btnConfirmProcess->IsSubmit = true;

$btnClose = new Button("btnClose","btnClose","Cancel");
$btnClose->Args = "onclick='javascript: return CloseLightBox2();'";

$btnOkay = new Button("btnOkay","btnOkay","Okay");
$btnOkay->Args = "onclick='javascript: return redirectToBulkUpdate();'";

$btnExportCSV = new Button("btnExportCSV", "btnExportCSV", "Export to CSV");
$btnExportCSV->IsSubmit = true;

$hiddendeckids = new Hidden("hiddendeckids","hiddendeckids","Hidden Deck Ids");
$hidden_date = new Hidden("hidden_date","hidden_date","Hidden Date");
$hidden_date->Text = date("m-d-Y");
$hiddenflag = new Hidden("hiddenflag","hiddenflag","Hidden Flag");

$txtInvoiceNo = new TextBox("txtInvoiceNo", "txtInvoiceNo", "txtInvoiceNo");
$txtInvoiceNo->Length = 5;
$txtInvoiceNo->Enabled = false;
$txtInvoiceNo->Style = "width:70px;";
$txtInvoiceNo->Args = "autocomplete='off'; onkeypress = 'javascript: return OnlyNumbers(event);'";

$bulkstatusupdateform->AddControl($ddlgamename);
$bulkstatusupdateform->AddControl($ddlgametype);
$bulkstatusupdateform->AddControl($ddlgamebatch);
$bulkstatusupdateform->AddControl($btnSubmit);
$bulkstatusupdateform->AddControl($ddlStartBookNum);
$bulkstatusupdateform->AddControl($ddlEndBookNum);
$bulkstatusupdateform->AddControl($ddlStatus);
$bulkstatusupdateform->AddControl($ddlAssignedTo);
$bulkstatusupdateform->AddControl($ddlNewStatus);
$bulkstatusupdateform->AddControl($ddlNewAssignedTo);
$bulkstatusupdateform->AddControl($txtRetailerAddress);
$bulkstatusupdateform->AddControl($txtReleaseDate);
$bulkstatusupdateform->AddControl($btnProcess);
$bulkstatusupdateform->AddControl($hiddendeckids);
$bulkstatusupdateform->AddControl($btnConfirmProcess);
$bulkstatusupdateform->AddControl($hidden_date);
$bulkstatusupdateform->AddControl($btnOkay);
$bulkstatusupdateform->AddControl($hiddenflag);
$bulkstatusupdateform->AddControl($btnExportCSV);
$bulkstatusupdateform->AddControl($ddlcardvalue);
$bulkstatusupdateform->AddControl($txtInvoiceNo);

$bulkstatusupdateform->ProcessForms();

if($bulkstatusupdateform->IsPostBack)
{
    //if($btnSubmit->SubmittedValue == "Submit")
    //{
		$prodid = $ddlgamename->SubmittedValue;
		$gameid = $ddlgametype->SubmittedValue;
		$batchid = $ddlgamebatch->SubmittedValue;
		$frombooknum = $ddlStartBookNum->SubmittedValue;
		$tobooknum = $ddlEndBookNum->SubmittedValue;
		$status = $ddlStatus->SubmittedValue;
		$assignedto = $ddlAssignedTo->SubmittedValue;

        /*$gamenumbers = $tmproducts->SelectGameNumPerGameName($prodid);
        $gamenum_list = new ArrayList();
        $gamenum_list->AddArray($gamenumbers);
        $ddlgametype->DataSource = $gamenum_list;
        $ddlgametype->DataSourceText = "GameNumber";
        $ddlgametype->DataSourceValue = "GameID";
        $ddlgametype->DataBind();
        $ddlgametype->SetSelectedValue($gameid);*/

if($hiddenflag->Text == 1 || $hiddenflag->Text == 4)
        {
        //game name
        $ddlgamename->Args = "onchange='javascript: get_gamenumber();'";          
        $ddlgamename->ClearItems();
        
        $gamenum1 = $ddlgametype->SubmittedValue;
        $gamenames = $tmproducts->SelectGameNamePerGameNum($gamenum1);
        $gamename_list1 = new ArrayList();
        $gamename_list1->AddArray($gamenames);       
        $litemgmetype1 = null;
        $litemgmetype1[] = new ListItem("Please select", "0", false);
        $ddlgamename->Items = $litemgmetype1;
       
        $ddlgamename->DataSource = $gamename_list1;
        $ddlgamename->DataSourceText = "ProductName";
        $ddlgamename->DataSourceValue = "ProductID";
        $ddlgamename->DataBind();
        
        $ddlgamename->SetSelectedValue($ddlgametype->SubmittedValue);
        //game num
        $ddlgametype->Args = "onchange='javascript: get_batchid(); '";
        $ddlgametype->ClearItems();
       
        $prodid1 = $ddlgamename->SubmittedValue;
        $gamenumbers = $tmproducts->SelectGameNumPerGameName($prodid1);
   
        $gamenumbers_list1 = new ArrayList();
        $gamenumbers_list1->AddArray($gamenumbers);       
        $litemgmetype12 = null;
        $litemgmetype12[] = new ListItem("Please select", "0", false);
        $ddlgametype->Items = $litemgmetype12;
        
        $ddlgametype->DataSource = $gamenumbers_list1;
        $ddlgametype->DataSourceText = "GameNumber";
        $ddlgametype->DataSourceValue = "GameID";
        $ddlgametype->DataBind();
        $ddlgametype->SetSelectedValue($ddlgametype->SubmittedValue);
	}
		
		if($hiddenflag->SubmittedValue == 2)
		{
		    $ddlgamename->Args = "onchange='javascript:'";
		    $ddlgamename->ClearItems();
		    $gamenames = $tmproducts->SelectGameNamePerGameNum($gameid);
		    $gamename_list1 = new ArrayList();
		    $gamename_list1->AddArray($gamenames);
		    $litemgmetype1 = null;
		    $litemgmetype1[] = new ListItem("Please select", "0", false);
		    $ddlgamename->Items = $litemgmetype1;
		    $ddlgamename->DataSource = $gamename_list1;
		    $ddlgamename->DataSourceText = "ProductName";
		    $ddlgamename->DataSourceValue = "ProductID";
		    $ddlgamename->DataBind();
		    $ddlgamename->SetSelectedValue($prodid);
		}
        $ddlgamename->SetSelectedValue($ddlgamename->SubmittedValue);
	$gamebatches = $tmproducts->SelectBatchIDPerGameNum($gameid,1);
        $gamebatch_list = new ArrayList();
        $gamebatch_list->AddArray($gamebatches);
        $ddlgamebatch->DataSource = $gamebatch_list;
        $ddlgamebatch->DataSourceText = "BatchID";
        $ddlgamebatch->DataSourceValue = "GameBatchID";
        $ddlgamebatch->DataBind();
        $ddlgamebatch->SetSelectedValue($batchid);

        $booknumbers = $tmdecks->SelectBookNumbersPerBatchID($batchid,$status);
        if(count($booknumbers) > 0)
        {
            for($i = 0 ; $i < count($booknumbers) ; $i++)
            {
                $arrbooknum[] = array( "IncrementalID" => $i ,
                                       "BookNumber" => $booknumbers[$i]["BookNumber"] ,
                                       "ID" => $booknumbers[$i]["ID"] ,
                                       "GameBatchID" => $booknumbers[$i]["GameBatchID"]
                                     );
            }
        }
        $frombooknum_list = new ArrayList();
        $frombooknum_list->AddArray($arrbooknum);
        $ddlStartBookNum->DataSource = $frombooknum_list;
        $ddlStartBookNum->DataSourceText = "BookNumber";
        $ddlStartBookNum->DataSourceValue = "IncrementalID";
        $ddlStartBookNum->DataBind();
        $ddlStartBookNum->SetSelectedValue($frombooknum);

        $tobooknum_list = new ArrayList();
        $tobooknum_list->AddArray($arrbooknum);
        $ddlEndBookNum->DataSource = $tobooknum_list;
        $ddlEndBookNum->DataSourceText = "BookNumber";
        $ddlEndBookNum->DataSourceValue = "IncrementalID";
        $ddlEndBookNum->DataBind();
        $ddlEndBookNum->SetSelectedValue($tobooknum);

        $selectedbooknums = array_slice($arrbooknum , $frombooknum , (($tobooknum - $frombooknum) + 1));
        $selecteddeckid = array();
        for($i = 0 ; $i < count($selectedbooknums) ; $i++)
        {
            array_push($selecteddeckid,$selectedbooknums[$i]["ID"]);
        }
        $selecteddeckids = implode("','" , $selecteddeckid);
        $hiddendeckids->Text = $selecteddeckids;
        //echo $selecteddeckids;
        if($status == 3)
        {
            $ddlAssignedTo->Enabled = false;
            if($ddlcardvalue->SubmittedValue == 0)
                {
                       $where = " A.ID IN ('$selecteddeckids') AND A.Status = '$status' ORDER BY A.BookNumber";
        
                }else
            $where = " A.ID IN ('$selecteddeckids') AND B.CardPrice = ".$ddlcardvalue->SelectedText." AND A.Status = '$status' ORDER BY A.BookNumber";
        }
        else
        {
            $ddlAssignedTo->Enabled = true;
            if($ddlcardvalue->SubmittedValue == 0)
                {
                         $where = " A.ID IN ('$selecteddeckids') AND  A.Status = '$status' AND A.AssignedToAID = '$assignedto' ORDER BY A.BookNumber";
      
                }else
            $where = " A.ID IN ('$selecteddeckids') AND B.CardPrice = ".$ddlcardvalue->SelectedText." AND A.Status = '$status' AND A.AssignedToAID = '$assignedto' ORDER BY A.BookNumber";
        }
        $selecteddeckdtls = $tmdecks->SelectDeckDtlsPerRange($where);
        $selecteddeckdtls_list = new ArrayList();
        $selecteddeckdtls_list->AddArray($selecteddeckdtls);

        if($status == 3)
        {
            $newStatus[] = new ListItem("Assigned","4");
        }
        else if($status == 4)
        {
            $newStatus[] = new ListItem("Active","1");
            $address = $tmaccounts->SelectAddress($assignedto);
            $txtRetailerAddress->Text = $address[0]["Address"];
        }
        else
        {

        }
        
        $ddlNewStatus->Items = $newStatus;
        $ddlNewAssignedTo->SetSelectedValue($assignedto);
    //}
        
    if ($btnExportCSV->SubmittedValue == "Export to CSV")
    {
        if($status == 3)
        {
            if($ddlcardvalue->SubmittedValue == 0)
                {
                         $where = " A.ID IN ('$selecteddeckids') AND  A.Status = '$status' ORDER BY A.BookNumber";
       
                }else
            $where = " A.ID IN ('$selecteddeckids') AND B.CardPrice = ".$ddlcardvalue->SelectedText." AND A.Status = '$status' ORDER BY A.BookNumber";
        }
        else
        {
            if($ddlcardvalue->SubmittedValue == 0)
                {
                         $where = " A.ID IN ('$selecteddeckids') AND  A.Status = '$status' AND A.AssignedToAID = '$assignedto' ORDER BY A.BookNumber";
       
                }else
            $where = " A.ID IN ('$selecteddeckids') AND B.CardPrice = ".$ddlcardvalue->SelectedText." AND A.Status = '$status' AND A.AssignedToAID = '$assignedto' ORDER BY A.BookNumber";
        }
        $selecteddeckdtls = $tmdecks->SelectDeckDtlsPerRange($where);
        $selecteddeckdtls_list = new ArrayList();
        $selecteddeckdtls_list->AddArray($selecteddeckdtls);
        
        for ($ctr = 0; $ctr < count($selecteddeckdtls_list); $ctr++)
        {            
            switch($selecteddeckdtls_list[$ctr]["Status"])
            {
                case 1: $status1 = "Active";
                case 3: $status1 = "On Stock"; break;
                case 4: $status1 = "Assigned"; break;
                default : $status1 = "Cancelled"; break;
            }
            $csvData[] = $selecteddeckdtls_list[$ctr]["CardPrice"].",".
                         $selecteddeckdtls_list[$ctr]["ProductName"].",".
                         $selecteddeckdtls_list[$ctr]["BookTicketCount"].",".
                         $selecteddeckdtls_list[$ctr]["GameNumber"].",".                         
                         $selecteddeckdtls_list[$ctr]["BatchID"].",".
                         $selecteddeckdtls_list[$ctr]["BoxNumber"].",".
                         $selecteddeckdtls_list[$ctr]["BookNumber"].",".
                         $status1.",".
                         $selecteddeckdtls_list[$ctr]["Username"]."\r\n";
        }
        
        $csvData[] = 'Total Books,'.count($selecteddeckdtls_list);
        $fp = fopen("../csv/Scratch_Card_Bulk_Update.csv","w");
        if ($fp)
        {
            $header = "Card Value,Game Name,Book Size,Game Number,Game Batch,Carton Number,Book Number,Status,Assigned To" . "\r\n";
            fwrite($fp,$header);
            if ($csvData){
                foreach($csvData as $rc)
                {
                    if(count($rc)>0) { fwrite($fp,$rc); }
                }
            }
            else
            {
                $rc = "\nNo Records Found;\n";
                fwrite($fp,$rc);
            }
            fclose($fp);
            header('Content-type: text/csv');
            header("Content-Disposition: attachment; filename=".'Scratch_Card_Bulk_Update.csv');
            header('Pragma: public');
            readfile('../csv/Scratch_Card_Bulk_Update.csv');
            exit;
        } else {
            echo "<script>alert('ok');</script>";
        }
    }

    if($btnProcess->SubmittedValue == "Process")
    {
        $newstatus = $ddlNewStatus->SubmittedValue;
        $ddlNewStatus->SetSelectedValue($newstatus);
        $ddlNewAssignedTo->SetSelectedValue($ddlNewAssignedTo->Value);
//                        $ddlNewAssignedTo
                    switch($newstatus)
                    {
                        case 1:
                            if($txtInvoiceNo->SubmittedValue == "" || $txtInvoiceNo->SubmittedValue == NULL || strlen($txtInvoiceNo->SubmittedValue) == 0)
                            {
                                $errortitle = "ERROR!";
                                $errormsg = "Please input an Invoice number.";
                                $txtInvoiceNo->Enabled = true;
                            }else if (strlen($txtInvoiceNo->SubmittedValue) < 5)
                            {
                                $errortitle = "ERROR!";
                                $errormsg = "Invoice number must not be less than 5 digits.";
                                $txtInvoiceNo->Enabled = true;
                            }else{
//                                    $deckinforesult = $tmdecks->getdeckinfo($txtInvoiceNo->SubmittedValue);
//                                    if($deckinforesult[0]["AssignedToAID"] == $ddlNewAssignedTo->Value || count($deckinforesult[0]["AssignedToAID"]) == 0)
//                                    {                            

                                                $confirm_msg = "Items that are tagged as 'Active' will no longer allowed to be modified.Proceed?";
                                                $btnConfirmProcess->Text = "Yes";
                                                $btnClose->Text = "No";
                                                $txtReleaseDate->ReadOnly = true;
                                                $txtInvoiceNo->Enabled = true;
                                                break;
//                                    }else{
//                                        $errortitle = "ERROR!";
//                                        $errormsg = "Invoice number is already used by another account.";
//                                        $txtInvoiceNo->Enabled = true;
//
//                                    }
                            }
                        case 4:
                            if(strlen($errortitle)==0)
                            {
                            $confirm_msg = "Save changes?";
                            $btnConfirmProcess->Text = "Okay";
                            $btnClose->Text = "Cancel";
                            $ddlNewAssignedTo->Enabled = true;
                            $txtReleaseDate->ReadOnly = true;
                            break;
                            }
                    }
                    if(strlen($errortitle)==0)
                    {
                    $confirm_title = "CONFIRMATION";
                    }

        
    }

    if(($btnConfirmProcess->SubmittedValue == "Okay") || ($btnConfirmProcess->SubmittedValue == "Yes"))
    {
        $newstatus = $ddlNewStatus->SubmittedValue;
        $releasedate = $txtReleaseDate->SubmittedValue;
        $deckids = $hiddendeckids->Text;
        $assignedto = $ddlNewAssignedTo->SubmittedValue;
        switch($newstatus)
        {
            case 1: 
                $tmdecks->UpdateDeckIdsToActive($newstatus,$releasedate,$deckids,$_SESSION['acctid'],$txtInvoiceNo->SubmittedValue);
                break;
            case 4:
                $tmdecks->UpdateDeckIdsToAssigned($newstatus,$assignedto,$deckids,$_SESSION['acctid']);
                break;
        }
        if($tmdecks->HasError)
        {
            $msgtitle = "ERROR!";
            $msg = "Error has occurred : " . $tmdecks->getError();
			$status = 2;
        }
        else
        {
            $msgtitle = "SUCCESSFUL!";
            $msg = "The item information has been successfully modified.";
			$status = 1;
        }

		//insert into batch transaction logs
        $gamebatchnum = $tmgamebatches->SelectByID($batchid);
        $batchtranslog["GameID"] = $gameid;
        $batchtranslog["BatchID"] = $gamebatchnum[0]['BatchID'];
        $batchtranslog["TransactionDate"] = 'now_usec()';
        $batchtranslog["TransactionType"] = 3;
        $batchtranslog["ProcessedByAID"] = $_SESSION['acctid'];
        $batchtranslog["Status"] = $status;
        $tmbatchtransactionlog->Insert($batchtranslog);
        if($tmbatchtransactionlog->HasError)
        {
            $msgtitle = "ERROR!";
            $msg = "Error has occurred : " . $tmbatchtransactionlog->getError();
        }

    }
}
?>