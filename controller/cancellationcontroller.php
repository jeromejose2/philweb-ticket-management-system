<?php
/*
 * Author: Noel Antonio
 * Date Created: 2011-08-31
 * Company: Philweb Corporation
 * Modified by: Frances Ralph DL. Sison
 * Modified on: 2013-04-05
 */
$pagesubmenuid = 10;
$stylesheets[] = "css/default.css";
$javascripts[] = "jscripts/jquery-1.5.2.min.js";

App::LoadModuleClass("TicketManagementCM", "TMDecks");
App::LoadModuleClass("TicketManagementCM", "TMProducts");
App::LoadModuleClass("TicketManagementCM", "TMGameManagement");
App::LoadModuleClass("TicketManagementCM", "TMTicketCancellation");
//App::LoadModuleClass("TicketManagementCM", "Pdf");
//App::LoadLibrary("fpdf/fpdf.php");
App::LoadControl("TextBox");
App::LoadControl("Button");
App::LoadControl("ComboBox");
App::LoadControl("PagingControl2");
App::LoadControl("Hidden");

$fproc = new FormsProcessor();
$TMDecks = new TMDecks();
$ticketcancellation = new TMTicketCancellation();
$TMProducts = new TMProducts();
$tmgames = new TMGameManagement();

$gamenames = $TMProducts->SelectAllGameName();
$gamename_list = new ArrayList();
$gamename_list->AddArray($gamenames);
$ddlgamename = new ComboBox("ddlgamename","ddlgamename","Game Name: ");
$ddlgamename->Args = "onchange = 'javascript: get_gamenumber();'";
$gamenameopt = null;
$gamenameopt[] = new ListItem("ALL", "0", true);
$ddlgamename->Items = $gamenameopt;
$ddlgamename->DataSource = $gamename_list;
$ddlgamename->DataSourceText = "ProductName";
$ddlgamename->DataSourceValue = "ProductID";
$ddlgamename->DataBind();

$games = $tmgames->SelectByGameType();
$games_list = new ArrayList();
$games_list->AddArray($games);
$ddlgametype = new ComboBox("ddlgametype", "ddlgametype", "Game Type: ");
$ddlgametype->Args = "onchange='javascript: get_batchID(); get_gamename();'";
$litemgmetype = null;
$litemgmetype[] = new ListItem("ALL", "0", true);
$ddlgametype->Items = $litemgmetype;
$ddlgametype->ShowCaption = false;
$ddlgametype->DataSource = $games_list;
$ddlgametype->DataSourceText = "GameNumber";
$ddlgametype->DataSourceValue = "GameID";
$ddlgametype->DataBind();

$ddlgamebatch = new ComboBox("ddlgamebatch","ddlgamebatch","Game Batch:");
$gamebatch = null;
$gamebatch[] = new ListItem("ALL","0", true);
$ddlgamebatch->Items  = $gamebatch;

$flag = new Hidden("flag","flag","flag");
$xmltype = new Hidden("xmltype","xmltype","xmltype");

$btnExport = new Button("export","export","Export to PDF");
$btnExport->IsSubmit = true;

$btnExportCSV = new Button("btnExportCSV","btnExportCSV","Export to CSV");
$btnExportCSV->IsSubmit = true;

$txtDateFr = new TextBox("txtDateFr", "txtDateFr", "");
$txtDateFr->ReadOnly = true;
$txtDateFr->Style = "text-align: center;width: 100px;";
$txtDateFr->Text = date("Y-m-d");

$txtDateTo = new TextBox("txtDateTo", "txtDateTo", "");
$txtDateTo->ReadOnly = true;
$txtDateTo->Style = "text-align: center;width: 100px;";
$txtDateTo->Text = date("Y-m-d");

$btnSubmit = new Button("btnSubmit", "btnSubmit", "Submit");
$btnSubmit->IsSubmit = true;
$btnSubmit->Args = "onclick='javascript: return validate();'";

$txtprodtype = new TextBox("txtprodtype", "txtprodtype", "");
$txtprodtype->ReadOnly = true;

$ddlcanceltype = new ComboBox("ddlcanceltype","ddlcanceltype","View By: ");
$options = null;
$options[] = new ListItem("ALL","0", true);
$options[] = new ListItem("Void","5");
$options[] = new ListItem("Stolen (Payable)","7");
$options[] = new ListItem("Stolen (Non-Payable)","8");
$options[] = new ListItem("Expired (Payable)","9" );
$options[] = new ListItem("Expired (Non-Payable)","10" );
$ddlcanceltype->Items = $options;

$itemsperpage = 50;
$pgcon = new PagingControl2($itemsperpage, 1);
$pgcon->URL = "javascript:ChangePage(%currentpage);";
$pgcon->ShowMoveToFirstPage = true;
$pgcon->ShowMoveToLastPage = true;
$pgcon->PageGroup = 5;

$fproc->AddControl($txtDateFr);
$fproc->AddControl($txtDateTo);
$fproc->AddControl($btnSubmit);
$fproc->AddControl($ddlgametype);
$fproc->AddControl($txtprodtype);
$fproc->AddControl($ddlcanceltype);
$fproc->AddControl($ddlgamename);
$fproc->AddControl($ddlgamebatch);
$fproc->AddControl($flag);
$fproc->AddControl($xmltype);
$fproc->AddControl($btnExport);
$fproc->AddControl($btnExportCSV);
$fproc->ProcessForms();

if ($fproc->IsPostBack)
{
    
    /* Added by: Sheryl S. Basbas Date: Feb 1, 2012 */
    if ($flag->SubmittedValue == 1)
    {
        $ddlgametype->Args = "onchange='javascript: get_batchID(); '";
        $ddlgametype->ClearItems();
        $prodid1 = $ddlgamename->SubmittedValue;
        $gamenumbers = $TMProducts->SelectGameNumPerGameName($prodid1);
        $gamenumbers_list1 = new ArrayList();
        $gamenumbers_list1->AddArray($gamenumbers);       
        $litemgmetype1 = null;
        $litemgmetype1[] = new ListItem("ALL", "0", false);
        $ddlgametype->Items = $litemgmetype1;
        $ddlgametype->DataSource = $gamenumbers_list1;
        $ddlgametype->DataSourceText = "GameNumber";
        $ddlgametype->DataSourceValue = "GameID";
        $ddlgametype->DataBind();
        $ddlgametype->SetSelectedValue($ddlgametype->SubmittedValue);
    }
    if ($flag->SubmittedValue == 2)
    {
        $ddlgamename->Args = "onchange='javascript:'";          
        $ddlgamename->ClearItems();
        $gamenum1 = $ddlgametype->SubmittedValue;
        $gamenames = $TMProducts->SelectGameNamePerGameNum($gamenum1);
        $gamename_list1 = new ArrayList();
        $gamename_list1->AddArray($gamenames);       
        $litemgmetype1 = null;
        $litemgmetype1[] = new ListItem("ALL", "0", false);
        $ddlgamename->Items = $litemgmetype1;
        $ddlgamename->DataSource = $gamename_list1;
        $ddlgamename->DataSourceText = "ProductName";
        $ddlgamename->DataSourceValue = "ProductID";
        $ddlgamename->DataBind();
        $ddlgamename->SetSelectedValue($ddlgametype->SubmittedValue);
    }
 
    $ddlgametype->SetSelectedValue($ddlgametype->SubmittedValue);
    $ddlgamename->SetSelectedValue($ddlgamename->SubmittedValue);
    $gamebatchID = $TMProducts->SelectBatchIDPerGameNum($ddlgametype->SelectedValue,$xmltype->SubmittedValue);
    $batchlist = new ArrayList();
    $batchlist->AddArray($gamebatchID);
    $gamebatch = null;
    $gamebatch[] = new ListItem("ALL","0", true);
    $ddlgamebatch->Items  = $gamebatch;
    $ddlgamebatch->DataSource = $batchlist;
    $ddlgamebatch->DataSourceText = "BatchID";
    $ddlgamebatch->DataSourceValue = "GameBatchID";
    $ddlgamebatch->DataBind();
    $ddlgamebatch->SetSelectedValue($ddlgamebatch->SubmittedValue);
    /*Added by: Sheryl S. Basbas */    
    
    if ($btnSubmit->SubmittedValue == "Submit" || $fproc->GetPostVar("pgSelectedPage") != '')
    {
        $arrDecks = $ticketcancellation->SelectCancel2($ddlgametype->SubmittedValue, $txtDateFr->Text, $txtDateTo->Text, $ddlcanceltype->SubmittedValue,$ddlgamebatch->SubmittedValue, $ddlgamename->SubmittedValue,$ddlcanceltype->SubmittedValue);
        $reccount = count($arrDecks);
        if ($btnSubmit->SubmittedValue == "Submit"){$pgcon->SelectedPage = 1;}
        $pgcon->Initialize($itemsperpage, $reccount);
        $pgTransactionHistory = $pgcon->PreRender();
        $arrtmd = $ticketcancellation->SelectCancelLimit2($ddlgametype->SubmittedValue, $txtDateFr->Text, $txtDateTo->Text, $ddlcanceltype->SubmittedValue, $pgcon->SelectedItemFrom-1, $itemsperpage, $ddlgamebatch->SubmittedValue, $ddlgamename->SubmittedValue, $ddlcanceltype->SubmittedValue);
        $cancel_list = new ArrayList();
        $cancel_list->AddArray($arrtmd);
    }
    
    /* Added by: Sheryl S. Basbas Date: Feb 3, 2012 */
    if($btnExport->SubmittedValue == "Export to PDF")
    {
        $arrDecks = $ticketcancellation->SelectCancel2($ddlgametype->SubmittedValue, $txtDateFr->Text, $txtDateTo->Text, $ddlcanceltype->SubmittedValue,$ddlgamebatch->SubmittedValue, $ddlgamename->SubmittedValue,$ddlcanceltype->SubmittedValue);
        $cancel_list = new ArrayList();
        $cancel_list->AddArray($arrDecks);
        $pdf = new PDF(L,mm,Legal);
        $pdf->AliasNbPages();        
        $title = 'Cancellation History';
        $pdf->SetTitle($title);
        $lengths = array(50, 40, 50, 30, 40, 30, 50, 50);
        $header = array('Date', 'Account', 'Mode of Cancellation', 'Game Number','Game Name', 'Game Batch', 'Tracking Number', 'Remarks');
        $cols = array('DateCancelled', 'Username', 'CancelMode', 'GameNumber','ProductName', 'BatchID', 'TicketTrackingNumber', 'Remarks');
        $data = $pdf->LoadData($cancel_list);      
        $pdf->SetFont('Arial','',9);   
        $pdf->AddPage();
        $pdf->FancyTable($header,$data,$lengths,$cols,'');
        $pdf->Output('Cancellation History','D');
    }
    /*Added by: Sheryl S. Basbas */  
    /*Modified by: Mark Kenneth Esguerra | 04-25-13*/
    if ($btnExportCSV->SubmittedValue == "Export to CSV")
    {
        $arrDecks = $ticketcancellation->SelectCancel2($ddlgametype->SubmittedValue, $txtDateFr->Text, $txtDateTo->Text, $ddlcanceltype->SubmittedValue,$ddlgamebatch->SubmittedValue, $ddlgamename->SubmittedValue,$ddlcanceltype->SubmittedValue);
        $cancel_list = new ArrayList();
        $cancel_list->AddArray($arrDecks);
        $csvData = array();
        for ($i = 0; $i < count($cancel_list); $i++)
        {
            $dateTime = new DateTime($cancel_list[$i]["DateCancelled"]);
            $date = $dateTime->format('Y-m-d h:i:s A');
            $acct = $cancel_list[$i]["Username"];
            $mode = $cancel_list[$i]["CancelMode"];
            $gameno = $cancel_list[$i]["GameNumber"];
            $gamename = $cancel_list[$i]["ProductName"];
            $batchid = $cancel_list[$i]["BatchID"];
            $trackno = $cancel_list[$i]["TicketTrackingNumber"];
            $booksize = $cancel_list[$i]["BookTicketCount"];
            $remarks = $cancel_list[$i]["Remarks"];
            $csvData[] = $date.",".$acct.",".$mode.",".$gamename.",".$gameno.",".$batchid.",".$trackno.",".$booksize.",".$remarks."\r\n";
        }
        $fp = fopen("../csv/Cancellation_History.csv","w");
        if ($fp)
        {
            $header = "Date,Account,Mode of Cancellation,Game Name,Game Number,Game Batch,Tracking Number,Book Size, Remarks" . "\r\n";
            fwrite($fp,$header);
            if ($csvData){
                foreach($csvData as $rc)
            {
                if(count($rc)>0)
                {
                    fwrite($fp,$rc);
                }
		}
		}
                else
                {
                    $rc = "\nNo Records Found;\n";
                    fwrite($fp,$rc);
                }
            
            
            fclose($fp);
            header('Content-type: text/csv');
            header("Content-Disposition: attachment; filename=".'Cancellation_History.csv');
            header('Pragma: public');
            readfile('../csv/Cancellation_History.csv');
            exit;
        } else {
            echo "<script>alert('ok');</script>";
        }
    }
}
?>
