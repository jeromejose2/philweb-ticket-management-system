<?php
/*
 * Author: Noel Antonio
 * Date Created: 2011-08-31
 * Company: Philweb Corporation
 */
$pagesubmenuid = 10;
$stylesheets[] = "css/default.css";
$javascripts[] = "jscripts/jquery-1.5.2.min.js";

App::LoadModuleClass("TicketManagementCM", "TMDecks");
App::LoadModuleClass("TicketManagementCM", "TMProducts");
App::LoadModuleClass("TicketManagementCM", "TMGameManagement");
App::LoadControl("TextBox");
App::LoadControl("Button");
App::LoadControl("ComboBox");
App::LoadControl("PagingControl2");
App::LoadControl("Hidden");

$fproc = new FormsProcessor();
$TMDecks = new TMDecks();
$TMProducts = new TMProducts();
$tmgames = new TMGameManagement();

$cboProduct = new ComboBox("cboProduct", "cboProduct", "");
$cboProduct->Args = "onchange='javascript: SelectGameType();'";
$options = null;
$options[] = new ListItem("ALL", "0");
$cboProduct->Items = $options;
$arrProd = $TMProducts->SelectOrderBy("ProductName", "ASC");
$prodList = new ArrayList();
$prodList->AddArray($arrProd);
$cboProduct->DataSource = $prodList;
$cboProduct->DataSourceText = "ProductName";
$cboProduct->DataSourceValue = "ProductID";
$cboProduct->DataBind();

$txtDateFr = new TextBox("txtDateFr", "txtDateFr", "");
$txtDateFr->ReadOnly = true;
$txtDateFr->Style = "text-align: center;width: 100px;";
$txtDateFr->Text = date("m/d/Y");

$txtDateTo = new TextBox("txtDateTo", "txtDateTo", "");
$txtDateTo->ReadOnly = true;
$txtDateTo->Style = "text-align: center;width: 100px;";
$txtDateTo->Text = date("m/d/Y");

$btnSubmit = new Button("btnSubmit", "btnSubmit", "Submit");
$btnSubmit->IsSubmit = true;
$btnSubmit->Args = "onclick='javascript: return validate();'";

//get list of all game types
$games = $tmgames->SelectByGameType();
$games_list = new ArrayList();
$games_list->AddArray($games);

$ddlgametype = new ComboBox("ddlgametype", "ddlgametype", "Game Type: ");
//$ddlgametype->Args = "onchange='javascript: get_product();'";
$ddlgametype->Args = "onchange='javascript: get_batchID(); get_gamename();'";
$litemgmetype = null;
$litemgmetype[] = new ListItem("Please select", "0", true);
$ddlgametype->Items = $litemgmetype;
$ddlgametype->ShowCaption = false;
$ddlgametype->DataSource = $games_list;
$ddlgametype->DataSourceText = "GameNumber";
$ddlgametype->DataSourceValue = "GameID";
$ddlgametype->DataBind();

$txtprodtype = new TextBox("txtprodtype", "txtprodtype", "");
$txtprodtype->ReadOnly = true;

$ddlcanceltype = new ComboBox("ddlcanceltype","ddlcanceltype","View By: ");
$options = null;
$options[] = new ListItem("Please Select","",true);
$options[] = new ListItem("All","0");
$options[] = new ListItem("Book","1");
$options[] = new ListItem("Ticket","2");
$ddlcanceltype->Items = $options;

$itemsperpage = 50;
$pgcon = new PagingControl2($itemsperpage, 1);
$pgcon->URL = "javascript:ChangePage(%currentpage);";
$pgcon->ShowMoveToFirstPage = true;
$pgcon->ShowMoveToLastPage = true;
$pgcon->PageGroup = 5;

//get list of game names
$gamenames = $TMProducts->SelectAllGameName();
$gamename_list = new ArrayList();
$gamename_list->AddArray($gamenames);

$ddlgamename = new ComboBox("ddlgamename","ddlgamename","Game Name: ");
$ddlgamename->Args = "onchange = 'javascript: get_gamenumber();'";
$gamenameopt = null;
$gamenameopt[] = new ListItem("Please select", "0", true);
$ddlgamename->Items = $gamenameopt;
$ddlgamename->DataSource = $gamename_list;
$ddlgamename->DataSourceText = "ProductName";
$ddlgamename->DataSourceValue = "ProductID";
$ddlgamename->DataBind();

/* Added by: Sheryl S. Basbas
 * Date: February 1, 2012
 */
$ddlgamebatch = new ComboBox("ddlgamebatch","ddlgamebatch","Game Barch:");
$gamebatch = null;
$gamebatch[] = new ListItem("Please select","0", true);
$ddlgamebatch->Items  = $gamebatch;

$flag = new Hidden("flag","flag","flag");
$xmltype = new Hidden("xmltype","xmltype","xmltype");
/*
 * Added by: Sheryl S. Basbas
 */

$fproc->AddControl($txtDateFr);
$fproc->AddControl($txtDateTo);
$fproc->AddControl($cboProduct);
$fproc->AddControl($btnSubmit);
$fproc->AddControl($ddlgametype);
$fproc->AddControl($txtprodtype);
$fproc->AddControl($ddlcanceltype);
$fproc->AddControl($ddlgamename);
$fproc->AddControl($ddlgamebatch);
$fproc->AddControl($flag);
$fproc->AddControl($xmltype);
//$fproc->ShowCaptions = true;
$fproc->ProcessForms();

if ($fproc->IsPostBack)
{
    /*
     * Added by: Sheryl S. Basbas
     * Date: Feb 1, 2012
     */
    if ($flag->SubmittedValue == 1)
    {
        $ddlgametype->Args = "onchange='javascript: get_batchID(); '";
        $ddlgametype->ClearItems();
       
        $prodid1 = $ddlgamename->SubmittedValue;
        $gamenumbers = $TMProducts->SelectGameNumPerGameName($prodid1);
    
        $gamenumbers_list1 = new ArrayList();
        $gamenumbers_list1->AddArray($gamenumbers);       
       
        $litemgmetype1 = null;
        $litemgmetype1[] = new ListItem("Please select", "0", false);
        $ddlgametype->Items = $litemgmetype1;
        
        $ddlgametype->DataSource = $gamenumbers_list1;
        $ddlgametype->DataSourceText = "GameNumber";
        $ddlgametype->DataSourceValue = "GameID";
        $ddlgametype->DataBind();
        
        $ddlgametype->SetSelectedValue($ddlgametype->SubmittedValue);
       
       
        
    }
     if ($flag->SubmittedValue == 2)
    {
        $ddlgamename->Args = "onchange='javascript:'";          
        $ddlgamename->ClearItems();
       
        $gamenum1 = $ddlgametype->SubmittedValue;
        $gamenames = $TMProducts->SelectGameNamePerGameNum($gamenum1);
    
        $gamename_list1 = new ArrayList();
        $gamename_list1->AddArray($gamenames);       
        $litemgmetype1 = null;
        $litemgmetype1[] = new ListItem("Please select", "0", false);
        $ddlgamename->Items = $litemgmetype1;
       
        $ddlgamename->DataSource = $gamename_list1;
        $ddlgamename->DataSourceText = "ProductName";
        $ddlgamename->DataSourceValue = "ProductID";
        $ddlgamename->DataBind();
        
        $ddlgamename->SetSelectedValue($ddlgametype->SubmittedValue);
       
        
        
    }
 
    $ddlgametype->SetSelectedValue($ddlgametype->SubmittedValue);
    $ddlgamename->SetSelectedValue($ddlgamename->SubmittedValue);
    
   
    $gamebatchID = $TMProducts->SelectBatchIDPerGameNum($ddlgametype->SelectedValue,$xmltype->SubmittedValue);
    $batchlist = new ArrayList();
    $batchlist->AddArray($gamebatchID);
    $gamebatch = null;
    $gamebatch[] = new ListItem("Please Select","0", true);
    $ddlgamebatch->Items  = $gamebatch;
    $ddlgamebatch->DataSource = $batchlist;
    $ddlgamebatch->DataSourceText = "BatchID";
    $ddlgamebatch->DataSourceValue = "GameBatchID";
    $ddlgamebatch->DataBind();
    $ddlgamebatch->SetSelectedValue($ddlgamebatch->SubmittedValue);
    /*
     * Added by: Sheryl S. Basbas
     */    
    
//    $gamenumbers = $TMProducts->SelectGameNumPerGameName($ddlgamename->SubmittedValue);
//    $gamename_list = new ArrayList();
//    $gamename_list->AddArray($gamenumbers);
//    $ddlgametype->DataSource = $gamename_list;
//    $ddlgametype->DataSourceText = "GameNumber";
//    $ddlgametype->DataSourceValue = "GameID";
//    $ddlgametype->DataBind();
//    $ddlgametype->SetSelectedValue($ddlgametype->SubmittedValue);
    if ($btnSubmit->SubmittedValue == "Submit" || $fproc->GetPostVar("pgSelectedPage") != '')
    {
        $arrDecks = $TMDecks->SelectCancel($ddlgametype->SubmittedValue, $txtDateFr->Text, $txtDateTo->Text, $ddlcanceltype->SubmittedValue,$ddlgamebatch->SubmittedValue);
        $reccount = count($arrDecks);
        if ($btnSubmit->SubmittedValue == "Submit"){$pgcon->SelectedPage = 1;}
        $pgcon->Initialize($itemsperpage, $reccount);
        $pgTransactionHistory = $pgcon->PreRender();
		//App::Pr(print_r($reccount));
        $arrtmd = $TMDecks->SelectCancelLimit($ddlgametype->SubmittedValue, $txtDateFr->Text, $txtDateTo->Text, $ddlcanceltype->SubmittedValue, $pgcon->SelectedItemFrom-1, $itemsperpage, $ddlgamebatch->SubmittedValue);
        $cancel_list = new ArrayList();
        $cancel_list->AddArray($arrtmd);
        //URL::Redirect($url);
    }
}
?>
