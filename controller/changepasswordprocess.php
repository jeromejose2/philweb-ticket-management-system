<?php
/*
 * Created By: Arlene R. Salazar on 08/26/2011
 * Purpose: For change password controller
 */
$rcode = $_GET["rcode"];
App::LoadModuleClass("TicketManagementCM", "TMAccounts");
App::LoadModuleClass("TicketManagementCM", "TMAuditLog");
App::LoadModuleClass("TicketManagementCM", "TMPasswordUpdateRequests");

App::LoadControl("TextBox");
App::LoadControl("Button");

$frmchgestat = new FormsProcessor();
$account = new TMAccounts();
$passwordupdaterequest = new TMPasswordUpdateRequests();

$where = " WHERE pr.RequestCode = '" . $rcode . "' AND pr.Status = 0";
$acctdtls = $passwordupdaterequest->SelectByHash($where);
if(count($acctdtls) > 0)
{
    $current_password = $acctdtls[0]["Password"];
    $username = $acctdtls[0]["UserName"];
    $id = $acctdtls[0]["AID"];
    $requestid = $acctdtls[0]["PasswordUpdateRequestID"];
}

$txtoldpword = new TextBox("txtoldpword","txtoldpword","Old Password: ");
$txtoldpword->Password = true;
$txtoldpword->ShowCaption = true;

$txtnewpword = new TextBox("txtnewpword","txtnewpword","New Password: ");
$txtnewpword->Password = true;
$txtnewpword->ShowCaption = true;
$txtnewpword->Args = "size='31'";
$txtnewpword->Length = 20;
$txtnewpword->Args = "onkeypress='javascript: return disableSpace(event);'";

$txtcfrmpword = new TextBox("txtcfrmpword","txtcfrmpword","Confirm Password: ");
$txtcfrmpword->Password = true;
$txtcfrmpword->ShowCaption = true;
$txtcfrmpword->Args = "size='29'";
$txtcfrmpword->Length = 20;
$txtcfrmpword->Args = "onkeypress='javascript: return disableSpace(event);'";

$btnSubmit = new Button("btnSubmit","btnSubmit","Save");
$btnSubmit->IsSubmit = true;
$btnSubmit->Args = "onclick='javascript: return checkChangePassword();'";

$btnConfirmChangePword = new Button("btnConfirmChangePword","btnConfirmChangePword","Okay");
$btnConfirmChangePword->IsSubmit = true;

$frmchgestat->AddControl($txtoldpword);
$frmchgestat->AddControl($txtnewpword);
$frmchgestat->AddControl($txtcfrmpword);
$frmchgestat->AddControl($btnSubmit);
$frmchgestat->AddControl($btnConfirmChangePword);

$frmchgestat->ProcessForms();

if($frmchgestat->IsPostBack)
{
    if($btnSubmit->SubmittedValue == "Save")
    {
        /*if(MD5($txtoldpword->SubmittedValue) != $current_password)
        {
            $chgepwordmsg = "Your current password and old password provided didn\'t match.";
            $chgepwordtitle = "ERROR!";
        }
        if(MD5($txtoldpword->SubmittedValue) == $current_password)*/
        $okchgepwordmsg = "ok";
    }
    if($btnConfirmChangePword->SubmittedValue == "Okay")
    {
        /*$updateacctdtls['ID'] = $id;
        $updateacctdtls['Password'] = MD5($txtnewpword->SubmittedValue);
        //$changepword["ForChangePassword"] = 1;
        $updatedpword = $account->UpdateByArray($updateacctdtls);*/
		$updatedpword = $account->UpdateAccountPassword($id,MD5(trim($txtnewpword->SubmittedValue)));
        if($account->HasError)
        {
            $confchangepword_title = "ERROR!";
            $confchangepword_msg = "Error has ocurred: " . $account->getError();
        }
        else
        {
            $auditlog = new TMAuditLog();
            $auditdtls["SessionID"] = "Request Code: " . $rcode; 
            $auditdtls["AID"] = $id; 
            $auditdtls["TransDetails"] = "Account ID: " . $id;
            $auditdtls["TransDateTime"] = "now_usec()";
            $auditdtls["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
            $auditdtls["AuditTrailFunctionID"] = 3;
            $insertauditlog = $auditlog->Insert($auditdtls);
            if($auditlog->HasError)
            {
                $confchangepword_title = "ERROR!";
                $confchangepword_msg = "Error has occured: "  . $auditlog->getError();
            }
            else
            {
                $confchangepword_title = "SUCCESSFUL!";
                $confchangepword_msg = "Password was successfully changed.";
            }
        }
		$updatechangepword = $account->UpdateAccountChangePasswordStatus($id,0);
		if($account->HasError)
        {
	    $confchangepword_title = "ERROR!";
            $confchangepword_msg = "Error has occurred: " . $account->getError();
        }

		$updaterequeststatus = $passwordupdaterequest->UpdateStatus($requestid,1);
		if($passwordupdaterequest->HasError)
        {
	    $confchangepword_title = "ERROR!";
            $confchangepword_msg = "Error has occurred: " . $passwordupdaterequest->getError();
        }
		//header("Location: ../views/login.php");
    }
}

?>