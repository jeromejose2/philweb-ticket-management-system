<?php
/*
 * Added By : Arlene R. Salazar
 * Added On : January 11, 2011
 * Purpose : Process for commision summary
 */
$pagesubmenuid = 41;
$stylesheets[] = "css/default.css";
$javascripts[] = "jscripts/validations.js";
$javascripts[] = "jscripts/jquery-1.5.2.min.js";

App::LoadModuleClass("TicketManagementCM", "TMAccounts");
App::LoadModuleClass("TicketManagementCM", "TMProducts");
App::LoadModuleClass("TicketManagementCM", "TMWinnings");
App::LoadModuleClass("TicketManagementCM", "TMGameManagement");
App::LoadModuleClass("TicketManagementCM", "TMCurrencies");

//App::LoadModuleClass("TicketManagementCM","Pdf");

App::LoadControl("ComboBox");
App::LoadControl("Button");
App::LoadControl("Hidden");
App::LoadControl("PagingControl2");

//App::LoadLibrary("fpdf/fpdf.php");

$commissionsummform = new FormsProcessor();

$currency = App::getParam("currency");
$tmaccounts = new TMAccounts();
$tmproducts = new TMProducts();
$tmwinnings = new TMWinnings();
$tmgamemngt = new TMGameManagement();
$tmcurrencies = new TMCurrencies();

$ddlUsername = new ComboBox("ddlUsername","ddlUsername","Username");
$options = null;
$options[] = new ListItem("ALL", "0");
$ddlUsername->Items = $options;
$arrRetailers = $tmaccounts->SelectAllActiveRetailer();
$retailer_list = new ArrayList();
$retailer_list->AddArray($arrRetailers);
$ddlUsername->DataSource = $retailer_list;
$ddlUsername->DataSourceText = "UserName";
$ddlUsername->DataSourceValue = "AID";
$ddlUsername->DataBind();

/*$ddlcardvalue = new ComboBox('ddlcardvalue','ddlcardvalue', 'Card Value: ');
$ddlcardvalue->Args = "onchange='javascript: get_gamename();'";
$cardprize = $tmgamemngt->SelectByCardValue();
$cardprize_list = new ArrayList();
$cardprize_list->AddArray($cardprize);
$cardlist = null;
$cardlist[] = new ListItem("ALL","0",false);
$ddlcardvalue->Items = $cardlist;
$ddlcardvalue->DataSource = $cardprize_list;
$ddlcardvalue->DataSourceText = "CardPrice";
$ddlcardvalue->DataSourceValue = "CardPrice";
$ddlcardvalue->DataBind();*/

$gamenames = $tmproducts->SelectAllGameName();
$gamename_list = new ArrayList();
$gamename_list->AddArray($gamenames);
$ddlgamename = new ComboBox("ddlgamename","ddlgamename","Game Name: ");
$ddlgamename->Args = "onchange = 'javascript: get_gamenumber();'";
$gamenameopt = null;
$gamenameopt[] = new ListItem("ALL", "0", true);
$ddlgamename->Items = $gamenameopt;
$ddlgamename->DataSource = $gamename_list;
$ddlgamename->DataSourceText = "ProductName";
$ddlgamename->DataSourceValue = "ProductID";
$ddlgamename->DataBind();

$games = $tmgamemngt->SelectByGameType();
$games_list = new ArrayList();
$games_list->AddArray($games);
$ddlgametype = new ComboBox("ddlgametype", "ddlgametype", "Game Number: ");
$ddlgametype->Args = "onchange='javascript: get_batchID();'";
$litemgmetype = null;
$litemgmetype[] = new ListItem("ALL", "0", true);
$ddlgametype->Items = $litemgmetype;
$ddlgametype->DataSource = $games_list;
$ddlgametype->DataSourceText = "GameNumber";
$ddlgametype->DataSourceValue = "GameID";
$ddlgametype->DataBind();

$ddlgamebatch = new ComboBox("ddlgamebatch","ddlgamebatch","Game Batch:");
$gamebatch = null;
$gamebatch[] = new ListItem("ALL","0", true);
$ddlgamebatch->Items  = $gamebatch;

$flag = new Hidden("flag","flag","flag");
$xmltype = new Hidden("xmltype","xmltype","xmltype");

$btnExport = new Button("export","export","Export to PDF");
$btnExport->IsSubmit = true;

$btnExportCSV = new Button("btnExportCSV","btnExportCSV","Export to CSV");
$btnExportCSV->IsSubmit = true;

$btnSubmit = new Button("btnSubmit","btnSubmit","Submit");
$btnSubmit->IsSubmit = true;
$btnSubmit->Args = "onclick = 'return checkCommissionSummary();'";

$hiddencardvalue = new Hidden("hiddencardvalue","hiddencardvalue","Hidden Card Value");
$hiddengameid = new Hidden("hiddengameid","hiddengameid","Hidden Game ID");
$hiddenselectedgameid = new Hidden("hiddenselectedgameid","hiddenselectedgameid","Hidden Selected Game ID");
$hiddenprodid = new Hidden("hiddenprodid","hiddenprodid","Hidden Product ID");
$hiddenuserid = new Hidden("hiddenuserid","hiddenuserid","Hidden User ID");
$hiddenbatchvalidationid = new Hidden("hiddenbatchvalidationid","hiddenbatchvalidationid","Hidden Batch Validation ID");
$hiddenbatchid = new Hidden("hiddenbatchid","hiddenbatchid","Hidden Batch ID");
$hiddenprocessedtag = new Hidden("hiddenprocessedtag","hiddenprocessedtag","Hidden processed tag");
$hiddenprocessedtag->Text = 1;
$hiddenflag = new Hidden("hiddenflag","hiddenflag","Hidden Flag");

$incentive = 2;

$arrcurr = $tmcurrencies->SelectByCurrencySymbol("KHR");
$fixedrate = $arrcurr[0]["FixedRate"];

$itemsperpage = 50;
$pgcon = new PagingControl2($itemsperpage, 1);
$pgcon->URL = "javascript:ChangePage(%currentpage);";
$pgcon->ShowMoveToFirstPage = true;
$pgcon->ShowMoveToLastPage = true;
$pgcon->PageGroup = 5;

if(isset($_SESSION['prodid']))
{
    $ddlUsername->SetSelectedValue($_SESSION['acct_id']);
//    $ddlcardvalue->SetSelectedValue($_SESSION['cardvalue']);
    $ddlgamename->SetSelectedValue($_SESSION['prodid']);

    $hiddengameid->Text = $_SESSION['gameid'];
    $hiddenprodid->Text = $_SESSION['prodid'];
    $hiddenuserid->Text = $_SESSION['acct_id'];
    $hiddenbatchid->Text = $_SESSION['batchid'];

    $gamenum_perproduct = $tmproducts->SelectGameNumPerGameName($_SESSION['prodid']);
    $gamenum_perproduct_list = new ArrayList();
    $gamenum_perproduct_list->AddArray($gamenum_perproduct);
    $ddlgametype->DataSource = $gamenum_perproduct_list;
    $ddlgametype->DataSourceText = "GameNumber";
    $ddlgametype->DataSourceValue = "GameID";
    $ddlgametype->DataBind();
    $ddlgametype->SetSelectedValue($_SESSION['gameid']);

    $ddlgamebatch->ClearItems();
    $gamebatchopt = null;
    $gamebatchopt[] = new ListItem("ALL", "0", true);
    $ddlgamebatch->Items = $gamebatchopt;
    $gamebatches = $tmproducts->SelectBatchIDPerGameNum($_SESSION['gameid'],2);
    $gamebatches_list = new ArrayList();
    $gamebatches_list->AddArray($gamebatches);
    $ddlgamebatch->DataSource = $gamebatches_list;
    $ddlgamebatch->DataSourceText = "BatchID";
    $ddlgamebatch->DataSourceValue = "GameBatchID";
    $ddlgamebatch->DataBind();
    $ddlgamebatch->SetSelectedValue($_SESSION['batchid']);

    $commissionsumm = $tmwinnings->GetCommissionSummaryPerBatchWithLimit($_SESSION['acct_id'], $_SESSION['gameid'], $_SESSION['prodid'], ($pgcon->SelectedItemFrom - 1), $itemsperpage , "2,5", "1,2", 1, $_SESSION['batchid']);
    $commissionsummlist = new ArrayList();
    $commissionsummlist->AddArray($commissionsumm);
    
    for ($i = 0; $i < count($commissionsummlist); $i++)
    {
        if (strpos($commissionsummlist[$i]["Total"], 'KHR') !== false)
        {
            $str_prize = preg_replace('/[\KHR,]/', '', $commissionsummlist[$i]["Total"]);
            $usd_prize = $str_prize / $fixedrate;
            $commissionsummlist[$i]["Total"] = number_format($usd_prize);
        }
        else if (strpos($commissionsummlist[$i]["Total"], $currency) !== false)
        {
            $commissionsummlist[$i]["Total"] = preg_replace('/[\"'.$currency.'",]/', '', $commissionsummlist[$i]["Total"]);
        }
    }
    
    $recordcount_array = $tmwinnings->GetCommissionSummaryPerBatch($_SESSION['acct_id'], $_SESSION['gameid'], $_SESSION['prodid'] , "2,5", "1,2", 1, $_SESSION['batchid']);

    $recordcount = count($recordcount_array);
    $pgcon->Initialize($itemsperpage, $recordcount);
    $pgTransactionHistory = $pgcon->PreRender();
}

$commissionsummform->AddControl($ddlUsername);
//$commissionsummform->AddControl($ddlcardvalue);
$commissionsummform->AddControl($ddlgamename);
$commissionsummform->AddControl($ddlgametype);
$commissionsummform->AddControl($ddlgamebatch);
$commissionsummform->AddControl($btnSubmit);
//$commissionsummform->AddControl($hiddencardvalue);
$commissionsummform->AddControl($hiddengameid);
$commissionsummform->AddControl($hiddenprodid);
$commissionsummform->AddControl($hiddenuserid);
$commissionsummform->AddControl($hiddenbatchvalidationid);
$commissionsummform->AddControl($btnExport);
$commissionsummform->AddControl($btnExportCSV);
$commissionsummform->AddControl($hiddenprocessedtag);
$commissionsummform->AddControl($hiddenbatchid);
$commissionsummform->AddControl($hiddenflag);
$commissionsummform->AddControl($hiddenselectedgameid);
$commissionsummform->AddControl($flag);
$commissionsummform->AddControl($xmltype);

$commissionsummform->ProcessForms();

//unset($_SESSION['cardvalue']);
unset($_SESSION['gameid']);
unset($_SESSION['prodid']);
unset($_SESSION['acct_id']);
unset($_SESSION['batchid']);

if($commissionsummform->IsPostBack)
{
    if($btnSubmit->SubmittedValue == "Submit")
    {
        $pgcon->SelectedPage = 1;
    }
    
//    $cardvalue = $ddlcardvalue->SubmittedValue;
    $acctid = $ddlUsername->SubmittedValue;
    $prodid = $ddlgamename->SubmittedValue;
    $gameid = $ddlgametype->SubmittedValue;
    $batchid = $ddlgamebatch->SubmittedValue;
    
//    $hiddencardvalue->Text = $cardvalue;
    $hiddengameid->Text = $gameid;
    $hiddenprodid->Text = $prodid;
    $hiddenuserid->Text = $acctid;
    $hiddenbatchid->Text = $batchid;

    if ($flag->SubmittedValue == 1)
    {
        $ddlgametype->Args = "onchange='javascript: get_batchID(); '";
        $ddlgametype->ClearItems();
        $prodid1 = $ddlgamename->SubmittedValue;
        $gamenumbers = $tmproducts->SelectGameNumPerGameName($prodid1);
        $gamenumbers_list1 = new ArrayList();
        $gamenumbers_list1->AddArray($gamenumbers);       
        $litemgmetype1 = null;
        $litemgmetype1[] = new ListItem("ALL", "0", false);
        $ddlgametype->Items = $litemgmetype1;
        $ddlgametype->DataSource = $gamenumbers_list1;
        $ddlgametype->DataSourceText = "GameNumber";
        $ddlgametype->DataSourceValue = "GameID";
        $ddlgametype->DataBind();
        $ddlgametype->SetSelectedValue($ddlgametype->SubmittedValue);
    }
    if ($flag->SubmittedValue == 2)
    {
        $ddlgamename->Args = "onchange='javascript:'";          
        $ddlgamename->ClearItems();
        $gamenum1 = $ddlgametype->SubmittedValue;
        $gamenames = $tmproducts->SelectGameNamePerGameNum($gamenum1);
        $gamename_list1 = new ArrayList();
        $gamename_list1->AddArray($gamenames);       
        $litemgmetype1 = null;
        $litemgmetype1[] = new ListItem("ALL", "0", false);
        $ddlgamename->Items = $litemgmetype1;
        $ddlgamename->DataSource = $gamename_list1;
        $ddlgamename->DataSourceText = "ProductName";
        $ddlgamename->DataSourceValue = "ProductID";
        $ddlgamename->DataBind();
        $ddlgamename->SetSelectedValue($ddlgametype->SubmittedValue);
    }
    
    /*if ($flag->SubmittedValue == 1)
    {
        $ddlgamename->ClearItems();
        $gamenames = $tmproducts->SelectGameNamePerCardPrice($ddlcardvalue->SubmittedValue);
        $gamename_list1 = new ArrayList();
        $gamename_list1->AddArray($gamenames);       
        $litemgmetype1 = null;
        $litemgmetype1[] = new ListItem("ALL", "0", false);
        $ddlgamename->Items = $litemgmetype1;
        $ddlgamename->DataSource = $gamename_list1;
        $ddlgamename->DataSourceText = "ProductName";
        $ddlgamename->DataSourceValue = "ProductID";
        $ddlgamename->DataBind();
        
        $ddlgametype->Args = "onchange='javascript: get_batchID(); '";
        $ddlgametype->ClearItems();
        $prodid1 = $ddlgamename->SubmittedValue;
        $gamenumbers = $tmproducts->SelectGameNumPerGameName($prodid1);
        $gamenumbers_list1 = new ArrayList();
        $gamenumbers_list1->AddArray($gamenumbers);       
        $litemgmetype1 = null;
        $litemgmetype1[] = new ListItem("ALL", "0", false);
        $ddlgametype->Items = $litemgmetype1;
        $ddlgametype->DataSource = $gamenumbers_list1;
        $ddlgametype->DataSourceText = "GameNumber";
        $ddlgametype->DataSourceValue = "GameID";
        $ddlgametype->DataBind();
        $ddlgametype->SetSelectedValue($ddlgametype->SubmittedValue);
    }*/
 
    $ddlgametype->SetSelectedValue($ddlgametype->SubmittedValue);
    $ddlgamename->SetSelectedValue($ddlgamename->SubmittedValue);
   
    $gamebatchID = $tmproducts->SelectBatchIDPerGameNum($ddlgametype->SelectedValue,$xmltype->SubmittedValue);
    $batchlist = new ArrayList();
    $batchlist->AddArray($gamebatchID);
    $gamebatch = null;
    $gamebatch[] = new ListItem("ALL","0", true);
    $ddlgamebatch->Items  = $gamebatch;
    $ddlgamebatch->DataSource = $batchlist;
    $ddlgamebatch->DataSourceText = "BatchID";
    $ddlgamebatch->DataSourceValue = "GameBatchID";
    $ddlgamebatch->DataBind();
    $ddlgamebatch->SetSelectedValue($ddlgamebatch->SubmittedValue);

    $commissionsumm = $tmwinnings->GetCommissionSummaryPerBatchWithLimit($acctid, $gameid, $prodid, ($pgcon->SelectedItemFrom - 1), $itemsperpage , "2,5", "1,2", 1, $batchid);
    $commissionsummlist = new ArrayList();
    $commissionsummlist->AddArray($commissionsumm);

    for ($i = 0; $i < count($commissionsummlist); $i++)
    {
        if (strpos($commissionsummlist[$i]["Total"], 'KHR') !== false)
        {
            $str_prize = preg_replace('/[\KHR,]/', '', $commissionsummlist[$i]["Total"]);
            $usd_prize = $str_prize / $fixedrate;
            $commissionsummlist[$i]["Total"] = $usd_prize;
        }
        else if (strpos($commissionsummlist[$i]["Total"], $currency) !== false)
        {
            $commissionsummlist[$i]["Total"] = preg_replace('/[\"'.$currency.'",]/', '', $commissionsummlist[$i]["Total"]);
        }
    }
    
    $recordcount_array = $tmwinnings->GetCommissionSummaryPerBatch($acctid, $gameid, $prodid , "2,5", "1,2", 1, $batchid);
    $recordcount = count($recordcount_array);
    $pgcon->Initialize($itemsperpage, $recordcount);
    $pgTransactionHistory = $pgcon->PreRender();
    
    if ($btnExportCSV->SubmittedValue == "Export to CSV")
    {
        $csvData = array();
        if(count($recordcount_array) > 0)
        {
            for($i = 0 ; $i < count($recordcount_array) ; $i++)
            {
                $prizesredeemed = $tmwinnings->GetRedemptionSummaryPerBatch($commissionsummlist[$i]["GameID"],$prodid,$acctid,$commissionsummlist[$i]["BatchValidationSessionID"], "2,5", "1,2");
                for ($i2=0; $i2 < count($prizesredeemed); $i2++)
                {
                    $batchid = $prizesredeemed[$i2]["BatchID"];
                    $username = $prizesredeemed[$i2]["Username"];
                    $datevalidated = date("d-M-Y", strtotime($commissionsummlist[$i]["DateValidated"]));
                    $prizeamt = $prizesredeemed[$i2]["PrizeName"];
                    
                    if (strpos($prizeamt, 'KHR') !== false)
                    {
                        $str_prize = preg_replace('/[\KHR,]/', '', $prizeamt);
                        $value = $str_prize / $fixedrate;
                    }
                    else if (strpos($prizeamt, $currency) !== false)
                    {
                        $value = preg_replace('/[\"'.$currency.'",]/', '', $prizeamt);
                    }
                    
                    $totalredemption = $currency . number_format(($value * ($incentive / 100)) , 2 , "." , ",");
                    $dateclaimed = date("d-M-Y", strtotime($prizesredeemed[$i2]["DateClaimed"]));
                    $gameno = $prizesredeemed[$i2]["GameNumber"];
                    $gamename = $prizesredeemed[$i2]["ProductName"];
                    $bookno = $prizesredeemed[$i2]["TicketNumber"];
                    $valno = $prizesredeemed[$i2]["ValidationNumber"];
                    $csvData[] = $batchid.",".$username.",".$datevalidated.",".$totalredemption.",".$dateclaimed.",".$gameno.",".$gamename.",".$bookno.",".$valno.",".str_replace(",", "", $value).",\r\n";
                }
            }
        }
        
        $fp = fopen("../csv/Redemption_Commission.csv","w");
        if ($fp)
        {
            $header = "Game Batch,Username,Date and Time Validated,Total Redemption Commission,Date and Time Claimed,Game Number,Game Name,Book Ticket Number,Validation Number,Prize Amount" ."\r\n";
            fwrite($fp,$header);
            foreach($csvData as $rc)
            {
                if(count($rc)>0)
                {
                    fwrite($fp,$rc);
                }
                else
                {
                    $rc = "No Records Found;\r\n";
                    fwrite($fp,$rc);
                }
            }
            
            fclose($fp);
            header('Content-type: text/csv');
            header("Content-Disposition: attachment; filename=".'Redemption_Commission.csv');
            header('Pragma: public');
            readfile('../csv/Redemption_Commission.csv');
            exit;
        } else {
            echo "<script>alert('ok');</script>";
        }
    }
}
?>