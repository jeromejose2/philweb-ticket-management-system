<?php
/*
 * Added By : Arlene R. Salazar
 * Added On : January 11, 2011
 * Purpose : Process for commision summary
 */
$pagesubmenuid = 41;
$stylesheets[] = "css/default.css";
$javascripts[] = "jscripts/validations.js";
$javascripts[] = "jscripts/jquery-1.5.2.min.js";

App::LoadModuleClass("TicketManagementCM", "TMAccounts");
App::LoadModuleClass("TicketManagementCM", "TMProducts");
App::LoadModuleClass("TicketManagementCM", "TMWinnings");
App::LoadModuleClass("TicketManagementCM", "TMGameManagement");

App::LoadControl("ComboBox");
App::LoadControl("Button");
App::LoadControl("Hidden");
App::LoadControl("PagingControl2");

App::LoadLibrary("fpdf/fpdf.php");

$commissionsummform = new FormsProcessor();
$tmaccounts = new TMAccounts();
$tmproducts = new TMProducts();
$tmwinnings = new TMWinnings();
$tmgamemngt = new TMGameManagement();

$ddlUsername = new ComboBox("ddlUsername","ddlUsername","Username");
$options = null;
$options[] = new ListItem("Please Select", "0");
$ddlUsername->Items = $options;
$arrRetailers = $tmaccounts->SelectAllActiveRetailer();
$retailer_list = new ArrayList();
$retailer_list->AddArray($arrRetailers);
$ddlUsername->DataSource = $retailer_list;
$ddlUsername->DataSourceText = "UserName";
$ddlUsername->DataSourceValue = "AID";
$ddlUsername->DataBind();

$ddlGameName = new ComboBox("ddlGameName","ddlGameName","Game Name");
$ddlGameName->Args = "onchange = 'javascript: get_gamenumber();'";
$gamenameopt = null;
$gamenameopt[] = new ListItem("Please select", "0", true);
$ddlGameName->Items = $gamenameopt;
$arrGameNames = $tmproducts->SelectAllGameName();
$gamename_list = new ArrayList();
$gamename_list->AddArray($arrGameNames);
$ddlGameName->DataSource = $gamename_list;
$ddlGameName->DataSourceText = "ProductName";
$ddlGameName->DataSourceValue = "ProductID";
$ddlGameName->DataBind();

$ddlGameNum = new ComboBox("ddlGameNum","ddlGameNum","Game Number");
$gamenumopt = null;
$gamenumopt[] = new ListItem("Please select", "0", true);
$ddlGameNum->Items = $gamenameopt;
$ddlGameNum->Args = "onchange = 'javascript: get_gamebatches();'";

$btnSubmit = new Button("btnSubmit","btnSubmit","Submit");
$btnSubmit->IsSubmit = true;
$btnSubmit->Args = "onclick = 'return checkCommissionSummary();'";

$hiddengameid = new Hidden("hiddengameid","hiddengameid","Hidden Game ID");
$hiddenprodid = new Hidden("hiddenprodid","hiddenprodid","Hidden Product ID");
$hiddenuserid = new Hidden("hiddenuserid","hiddenuserid","Hidden User ID");
$hiddenbatchvalidationid = new Hidden("hiddenbatchvalidationid","hiddenbatchvalidationid","Hidden Batch Validation ID");
$hiddenbatchid = new Hidden("hiddenbatchid","hiddenbatchid","Hidden Batch ID");
$hiddenprocessedtag = new Hidden("hiddenprocessedtag","hiddenprocessedtag","Hidden processed tag");
$hiddenprocessedtag->Text = 1;

$ddlBatchID = new ComboBox("ddlBatchID","ddlBatchID","Game Batch");
$gamebatchopt = null;
$gamebatchopt[] = new ListItem("Please select", "", true);
$ddlBatchID->Items = $gamebatchopt;

$btnExport = new Button("btnExport","btnExport","Export to PDF");
$btnExport->IsSubmit = true;

$incentive = 2;

$itemsperpage = 50;
$pgcon = new PagingControl2($itemsperpage, 1);
$pgcon->URL = "javascript:ChangePage(%currentpage);";
$pgcon->ShowMoveToFirstPage = true;
$pgcon->ShowMoveToLastPage = true;
$pgcon->PageGroup = 5;

if(isset($_SESSION['prodid']))
{
    $ddlUsername->SetSelectedValue($_SESSION['acctid']);
    $ddlGameName->SetSelectedValue($_SESSION['prodid']);

    $hiddengameid->Text = $_SESSION['gameid'];
    $hiddenprodid->Text = $_SESSION['prodid'];
    $hiddenuserid->Text = $_SESSION['acctid'];
    $hiddenbatchid->Text = $_SESSION['batchid'];

    $gamenum_perproduct = $tmproducts->SelectGameNumPerGameName($_SESSION['prodid']);
    $gamenum_perproduct_list = new ArrayList();
    $gamenum_perproduct_list->AddArray($gamenum_perproduct);
    $ddlGameNum->DataSource = $gamenum_perproduct_list;
    $ddlGameNum->DataSourceText = "GameNumber";
    $ddlGameNum->DataSourceValue = "GameID";
    $ddlGameNum->DataBind();
    $ddlGameNum->SetSelectedValue($_SESSION['gameid']);

    $ddlBatchID->ClearItems();
    $gamebatchopt = null;
    $gamebatchopt[] = new ListItem("ALL", "0", true);
    $ddlBatchID->Items = $gamebatchopt;
    $gamebatches = $tmproducts->SelectBatchIDPerGameNum($_SESSION['gameid'],2);
    $gamebatches_list = new ArrayList();
    $gamebatches_list->AddArray($gamebatches);
    $ddlBatchID->DataSource = $gamebatches_list;
    $ddlBatchID->DataSourceText = "BatchID";
    $ddlBatchID->DataSourceValue = "GameBatchID";
    $ddlBatchID->DataBind();
    $ddlBatchID->SetSelectedValue($_SESSION['batchid']);

    $commissionsumm = $tmwinnings->GetCommissionSummaryPerBatchWithLimit($_SESSION['acctid'], $_SESSION['gameid'], $_SESSION['prodid'], ($pgcon->SelectedItemFrom - 1), $itemsperpage , 2, 1, 1, $_SESSION['batchid']);
    $commissionsummlist = new ArrayList();
    $commissionsummlist->AddArray($commissionsumm);
    //$totalprizesredeemed = $tmwinnings->GetTotalRedemptionCommision($_SESSION['acctid'], $_SESSION['gameid'], $_SESSION['prodid'] , 2, 1, $_SESSION['batchid']);
    $recordcount_array = $tmwinnings->GetCommissionSummaryPerBatch($_SESSION['acctid'], $_SESSION['gameid'], $_SESSION['prodid'] , 2, 1, 1, $_SESSION['batchid']);

    $recordcount = count($recordcount_array);
    $pgcon->Initialize($itemsperpage, $recordcount);
    $pgTransactionHistory = $pgcon->PreRender();
}

$commissionsummform->AddControl($ddlUsername);
$commissionsummform->AddControl($ddlGameName);
$commissionsummform->AddControl($ddlGameNum);
$commissionsummform->AddControl($btnSubmit);
$commissionsummform->AddControl($hiddengameid);
$commissionsummform->AddControl($hiddenprodid);
$commissionsummform->AddControl($hiddenuserid);
$commissionsummform->AddControl($hiddenbatchvalidationid);
$commissionsummform->AddControl($ddlBatchID);
$commissionsummform->AddControl($btnExport);
$commissionsummform->AddControl($hiddenprocessedtag);
$commissionsummform->AddControl($hiddenbatchid);

$commissionsummform->ProcessForms();

unset($_SESSION['gameid']);
unset($_SESSION['prodid']);
unset($_SESSION['acctid']);
unset($_SESSION['batchid']);

if($commissionsummform->IsPostBack)
{
    if($btnSubmit->SubmittedValue == "Submit")
    {
        $pgcon->SelectedPage = 1;
    }

    $acctid = $ddlUsername->SubmittedValue;
    $prodid = $ddlGameName->SubmittedValue;
    $gameid = $ddlGameNum->SubmittedValue;
	$batchid = $ddlBatchID->SubmittedValue;
    $hiddengameid->Text = $gameid;
    $hiddenprodid->Text = $prodid;
    $hiddenuserid->Text = $acctid;
	$hiddenbatchid->Text = $batchid;

    $ddlGameNum->ClearItems();
    $gamenameopt = null;
    //$gamenameopt[] = new ListItem("ALL", "0", true);
	$gamenameopt[] = new ListItem("Please select", "0", true);
    $ddlGameNum->Items = $gamenameopt;
    $gamenum_perproduct = $tmproducts->SelectGameNumPerGameName($prodid);
    $gamenum_perproduct_list = new ArrayList();
    $gamenum_perproduct_list->AddArray($gamenum_perproduct);
    $ddlGameNum->DataSource = $gamenum_perproduct_list;
    $ddlGameNum->DataSourceText = "GameNumber";
    $ddlGameNum->DataSourceValue = "GameID";
    $ddlGameNum->DataBind();
    $ddlGameNum->SetSelectedValue($gameid);

	$ddlBatchID->ClearItems();
    $gamebatchopt = null;
    $gamebatchopt[] = new ListItem("ALL", "0", true);
    $ddlBatchID->Items = $gamebatchopt;
    $gamebatches = $tmproducts->SelectBatchIDPerGameNum($gameid,2);
    $gamebatches_list = new ArrayList();
    $gamebatches_list->AddArray($gamebatches);
    $ddlBatchID->DataSource = $gamebatches_list;
    $ddlBatchID->DataSourceText = "BatchID";
    $ddlBatchID->DataSourceValue = "GameBatchID";
    $ddlBatchID->DataBind();
    $ddlBatchID->SetSelectedValue($batchid);

    $commissionsumm = $tmwinnings->GetCommissionSummaryPerBatchWithLimit($acctid, $gameid, $prodid, ($pgcon->SelectedItemFrom - 1), $itemsperpage , 2, 1, 1, $batchid);
    $commissionsummlist = new ArrayList();
    $commissionsummlist->AddArray($commissionsumm);
    $totalprizesredeemed = $tmwinnings->GetTotalRedemptionCommision($acctid, $gameid, $prodid , 2, 1, $batchid);
    $recordcount_array = $tmwinnings->GetCommissionSummaryPerBatch($acctid, $gameid, $prodid , 2, 1, 1, $batchid);
    
    $recordcount = count($recordcount_array);
    $pgcon->Initialize($itemsperpage, $recordcount);
    $pgTransactionHistory = $pgcon->PreRender();

	if($btnExport->SubmittedValue == "Export to PDF")
    {
        $pdf = new FPDF('L','mm','Legal');
        $pdf->AddPage();

        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(0,5,"Redemption Commission",0,0,'C');
        $pdf->Ln(4);

        $pdf->Ln(8);

        if(count($recordcount_array) > 0)
        {
            for($i = 0 ; $i < count($recordcount_array) ; $i++)
            {
                $pdf->SetFont('Arial','B',9);
                $pdf->SetX(10);
                $pdf->Cell(20,5,'Game Batch',1,0,'C');
                $pdf->SetX(30);
                $pdf->Cell(25,5,'Username',1,0,'C');
                $pdf->SetX(55);
                $pdf->Cell(35,5,'Date & Time Validated',1,0,'C');
                $pdf->SetX(90);
                $pdf->Cell(50,5,'Total Redemption Commission',1,0,'C');
                $pdf->SetX(140);
                $pdf->Cell(40,5,'Date & Time Claimed',1,0,'C');
                $pdf->SetX(180);
                $pdf->Cell(30,5,'Game Number',1,0,'C');
                $pdf->SetX(210);
                $pdf->Cell(30,5,'Game Name',1,0,'C');
                $pdf->SetX(240);
                $pdf->Cell(40,5,'Book Ticket Number',1,0,'C');
                $pdf->SetX(280);
                $pdf->Cell(40,5,'Validation Number',1,0,'C');
                $pdf->SetX(320);
                $pdf->Cell(30,5,'Prize Amount',1,1,'C');

                $pdf->SetFont('Arial','',9);
                $pdf->SetX(10);
				$pdf->Cell(20,5,$commissionsummlist[$i]["BatchID"],1,0,'C');
                $pdf->Cell(25,5,$commissionsummlist[$i]["Username"],1,0,'C');
                $pdf->Cell(35,5,$commissionsummlist[$i]["DateValidated"],1,0,'C');
                $pdf->Cell(50,5,"$" . number_format(($commissionsummlist[$i]["Total"] * ($incentive / 100)) , 2 , "." , ","),1,0,'C');

                
                $prizesredeemed = $tmwinnings->GetRedemptionSummaryPerBatch($commissionsummlist[$i]["GameID"],$prodid,$acctid,$commissionsummlist[$i]["BatchValidationSessionID"], 2, 1);
                $totalprizesredeemed = $tmwinnings->GetTotalPrizesRedeemedPerBatchID($commissionsummlist[$i]["GameID"],$prodid,$acctid,$commissionsummlist[$i]["BatchValidationSessionID"], 2, 1);
                $total_prizes_redeemed = $totalprizesredeemed[0]["Total"];
                $total_redemption_comm = ($total_prizes_redeemed * ($incentive / 100));
                if(count($prizesredeemed) > 0)
                {
                    
                    for($a = 0 ; $a < count($prizesredeemed) ; $a++)
                    {
                        $pdf->SetX(140);
                        $pdf->Cell(40,5,$prizesredeemed[$a]["DateClaimed"],1,0,'C');
                        $pdf->Cell(30,5,$prizesredeemed[$a]["GameNumber"],1,0,'C');
                        $pdf->Cell(30,5,$prizesredeemed[$a]["ProductName"],1,0,'C');
                        $pdf->Cell(40,5,$prizesredeemed[$a]["TicketNumber"],1,0,'C');
                        $pdf->Cell(40,5,$prizesredeemed[$a]["ValidationNumber"],1,0,'C');
                        $pdf->Cell(30,5,$prizesredeemed[$a]["PrizeName"],1,1,'R');
                        
                    }
                    
                    $pdf->SetX(140);
                    $pdf->SetFont('Arial','B',9);
                    $pdf->Cell(180,5,'Total Prizes Redeemed:',1,0,'C');
                    $pdf->Cell(30,5,"$" . number_format($total_prizes_redeemed , 2 , '.' , ','),1,1,'R');
                    $pdf->SetX(140);
                    $pdf->Cell(180,5,'Total Redemption Commission:',1,0,'C');
                    $pdf->Cell(30,5,"$" . number_format($total_redemption_comm , 2 , '.' , ','),1,1,'R');
                }
                else
                {
                    $pdf->SetX(140);
                    $pdf->Cell(200,5,'No records found',1,0,'C');
                }

                $pdf->Ln(8);
            }
        }
        else
        {
            $pdf->SetFont('Arial','',9);
            $pdf->SetX(10);
            $pdf->Cell(330,5,'No records found',0,1,'C');
        }

        $pdf->Output('Redemption Commission','D');
        Header('Content-Type: application/pdf');
    }
}
?>
