<?php
/*
 * Added by: Arlene R. Salazar on 10-06-2011
 * Modified By: Noel Antonio on 01-14-2013
 * Purpose: Controller for book transfer update
 */
$pagesubmenuid = 48;
$stylesheets[] = "css/default.css";
$javascripts[] = "jscripts/validations.js";
$javascripts[] = "jscripts/jquery-1.5.2.min.js";

App::LoadModuleClass("TicketManagementCM", "TMDecks");
App::LoadModuleClass("TicketManagementCM", "TMAccounts");

App::LoadControl("TextBox");
App::LoadControl("ComboBox");
App::LoadControl("Button");
App::LoadControl("Hidden");

$tmdecks = new TMDecks();
$tmaccounts = new TMAccounts();

$frmbooktransupdate = new FormsProcessor();

$ddlassingedto = new ComboBox("ddlassingedto","ddlassingedto","Assigned To:");
$litem = null;
$litem[] = new ListItem("-----","");
$ddlassingedto->Items = $litem;

$txtInvoiceNo = new TextBox("txtInvoiceNo", "txtInvoiceNo", "txtInvoiceNo");
$txtInvoiceNo->Length = 5;
$txtInvoiceNo->Style = "width:70px;";
$txtInvoiceNo->Args = "onkeypress = 'javascript: return OnlyNumbers(event);' autocomplete='off'";

$btnProcess = new Button("btnProcess","btnProcess","Process");
$btnProcess->IsSubmit = true;
$btnProcess->Args = "onclick = 'javascript: return checkDistibutorUpdate();'";

$btnOkay = new Button("btnOkay","btnOkay","OKAY");
$btnOkay->Args = "onclick = 'javascript: return redirectToDistributorBookTransfer();'";

$txtusername = new TextBox("txtusername","txtusername","Username");
$txtusername->Length = 20;

$txtpassword = new TextBox("txtpassword","txtpassword","Password");
$txtpassword->Length = 20;
$txtpassword->Password = true;
$txtpassword->Args="onkeypress='javascript: return disableSpace(event);' autocomplete='off'";

$btnError = new Button("btnError","btnError","Okay");
$btnError->Args = "onclick = 'javascript: return ShowAdminConfirmation();'";

$hiddenassignedto = new Hidden("hiddenassignedto","hiddenassignedto","Hidden assigned to");

$hiddenassignedtouname = new Hidden("hiddenassignedtouname","hiddenassignedtouname","Hidden assigned to username");

$hiddenbookids = new Hidden("hiddenbookids","hiddenbookids","Hidden book ids");

$hiddeninvoiceno = new Hidden("hiddeninvoiceno","hiddeninvoiceno","hiddeninvoiceno");

$btnyes = new Button('btnyes','btnyes','YES');
$btnyes->IsSubmit = true;

$frmbooktransupdate->AddControl($txtInvoiceNo);
$frmbooktransupdate->AddControl($ddlassingedto);
$frmbooktransupdate->AddControl($btnProcess);
$frmbooktransupdate->AddControl($btnOkay);
$frmbooktransupdate->AddControl($txtusername);
$frmbooktransupdate->AddControl($txtpassword);
$frmbooktransupdate->AddControl($hiddenassignedto);
$frmbooktransupdate->AddControl($hiddenassignedtouname);
$frmbooktransupdate->AddControl($hiddenbookids);
$frmbooktransupdate->AddControl($hiddeninvoiceno);
$frmbooktransupdate->AddControl($btnyes);

$frmbooktransupdate->ProcessForms();

if($frmbooktransupdate->IsPostBack)
{    
    if($frmbooktransupdate->GetPostVar("chkbooktrans"))
    {
        //get deckid to update
        $bookids = $frmbooktransupdate->GetPostVar("chkbooktrans");
        $field = $frmbooktransupdate->GetPostVar("hiddenfield");
        $deckstatus = $tmdecks->SelectStatus($bookids, $field);
        $hiddenassignedto->Text = $deckstatus[0][$field];
        $hiddenassignedtouname->Text = $deckstatus[0]["Username"];
        $assignedtouname = $deckstatus[0]["Username"];
        $product = $frmbooktransupdate->GetPostVar("ddlgamename");
        $game = $frmbooktransupdate->GetPostVar("ddlgametype");
        $_SESSION['bookids'] = $bookids;
        $_SESSION['product'] = $product;
        $_SESSION['game'] = $game;
        $_SESSION['field'] = $field;
    }

    $deckstatus = $tmdecks->SelectStatus($_SESSION['bookids'], $_SESSION['field']);
    $accounts = $tmaccounts->SelectAllDistributorsExceptCurrent($deckstatus[0][$_SESSION['field']]);
    $accounts_list = new ArrayList();
    $accounts_list->AddArray($accounts);
    $ddlassingedto->DataSource = $accounts_list;
    $ddlassingedto->DataSourceText = "Name";
    $ddlassingedto->DataSourceValue = "AID";
    $ddlassingedto->DataBind();

    if($btnProcess->SubmittedValue == "Process")
    {   
            $assignedtouname = $hiddenassignedtouname->Text;
            $ddlassingedto->SetSelectedValue($ddlassingedto->SubmittedValue);
            $confirm = "ok";          
    }
    
    if($btnyes->SubmittedValue == "YES")
    {       
            $newinvoice = $txtInvoiceNo->SubmittedValue;
            
            $result = transferbooks($_SESSION['bookids'], $ddlassingedto->SubmittedValue, $_SESSION['acctid'], $hiddenassignedto->SubmittedValue, $_SESSION['product'], $_SESSION['game'], $newinvoice);
            $successtitle = $result["title"];
            $successmsg = $result["msg"];
            
            unset($_SESSION['bookids']);
            unset($_SESSION['product']);
            unset($_SESSION['game']);
    }
    
    $tbldata = $tmdecks->SelectDeckByID($_SESSION['bookids'], $_SESSION['field']);
    $bookslist = new ArrayList();
    $bookslist->AddArray($tbldata);
}
else
{
    URL::Redirect('distributorbookreassignment.php');
}

function transferbooks($bookid, $assignedto, $aid, $hidden, $prod, $game, $newinvoice)
{
    App::LoadModuleClass("TicketManagementCM", "TMAccounts");
    App::LoadModuleClass("TicketManagementCM", "TMAuditLog"); 
    App::LoadModuleClass("TicketManagementCM", "TMBookTransferHistory");
    App::LoadModuleClass("TicketManagementCM", "TMTicketTransferHistory");

    $tmaccounts = new TMAccounts();
    $tmauditlog = new TMAuditLog();
    $tmbooktranshist = new TMBookTransferHistory();
    $tickettranshist = new TMTicketTransferHistory();
    $tmdecks = new TMDecks();
    
    $deckshaserror = false;
    $booktranshaserror = false;
    $tickettranshaserror = false;
    
    $arrBookDtls = $tmdecks->selectBooksWithBookSize($bookid);
    for ($i = 0; $i < count($arrBookDtls); $i++)
    {
        $row = $arrBookDtls[$i];
        $id = $row["ID"];
        $originaldistributor = $row["AssignedToAID"];
        $datereleased = $row["DateReleased"];
        $oldinvoice = $row["InvoiceNumber"];
        $booksize = $row["BookTicketCount"];
        if (isset($originaldistributor))
            $tmdecks->UpdateDeckDtlsBookTrans2($id, $originaldistributor, $assignedto, $aid ,$newinvoice);
        else
            $tmdecks->UpdateDeckDtlsBookTrans2($id, $assignedto, $assignedto, $aid, $newinvoice); // additional checking just in case, then it's the same distributor in which the book will be assigned.
        
        // insert to booktransferhistory
        $history["BookID"] = $id;
        $history["FromAssignedAID"] = $hidden;
        $history["OldInvoiceNo"] = $oldinvoice;
        $history["CurrentInvoiceNo"] = $newinvoice;
        $history["ToAssignedAID"] = $assignedto;
        $history["DateCreated"] = 'now_usec()';
        $history["TransferredByAID"] = $aid;
        $tmbooktranshist->Insert($history);
        
        // insert to tickettransferhistory
        $arrticketranshist['ProductID'] = $prod;
        $arrticketranshist['GameID'] = $game;
        $arrticketranshist['BookID'] = $id;
        $arrticketranshist['TicketCount'] = $booksize;
        $arrticketranshist['FromAID'] = $hidden;
        $arrticketranshist['ToAID'] = $assignedto;
        $arrticketranshist['DateCreated'] = 'now_usec()';
        $arrticketranshist['TicketsRecalled'] = $booksize;
        $arrticketranshist['DateReleased'] = $datereleased;
        $tickettranshist->Insert($arrticketranshist);
        
        if ($tmdecks->HasError)
            $deckshaserror = true;
        
        if ($tmbooktranshist->HasError)
            $booktranshaserror = true;
        
        if ($tickettranshist->HasError)
            $tickettranshaserror = true;
    }
    
    if ($deckshaserror){
        $errortitle = "ERROR!";
        $errormsg = "Error in transferring book: " . $tmdecks->getError();
    }
    else if ($booktranshaserror){
        $errortitle = "ERROR!";
        $errormsg = "Error in inserting book transfer history: " . $tmbooktranshist->getError();
    }
    else if ($tickettranshaserror){
        $errortitle = "ERROR!";
        $errormsg = "Error in inserting  TicketTranshist: " . $tickettranshist->getError();
    }
    else
    {
        $reassigneddtls = $tmaccounts->SelectByAccountID($assignedto);
        $origdistributordtls = $tmaccounts->SelectByAccountID($hidden); 
        $originaldistributor = $origdistributordtls[0]["UserName"];
        $newdistributor = $reassigneddtls[0]["UserName"];
        
        //insert to audittrail
        $auditdtls["AuditTrailFunctionID"] = '38';
        $auditdtls["SessionID"] = $_SESSION['sid'];
        $auditdtls["AID"] = $aid;
        $auditdtls["TransDetails"] = "Transferred from " . $originaldistributor . " to " . $newdistributor . " Book ID/S : " . join(",", $bookid);
        $auditdtls["TransDateTime"] = 'now_usec()';
        $auditdtls["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
        $tmauditlog->Insert($auditdtls);
        if($tmauditlog->HasError) {
            $errortitle = "ERROR!";
            $errormsg = "Error in inserting auditlog: " . $tmauditlog->getError();
        }
        else {
            $btnCancel->Text = "Okay";
            $errortitle = "Successful Transfer";
            $errormsg = "Book/s item has been successfully transferred from " . $originaldistributor . " account to ". $newdistributor ." account under Invoice Number ".$newinvoice.".";
        }
    }
    
    $array = array('title'=>$errortitle, 'msg'=>$errormsg);
    return $array;
}
?>