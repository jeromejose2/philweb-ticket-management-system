<?php
/* 
 * Added by: Arlene R. Salazar on 10-06-2011
 * Modified By: Noel Antonio on 01-14-2013
 * Purpose: Controller for book transfer
 * Modified By: AGC 01-29-2013
 * Purpose: Disregard ddl when searching Game Batch
 */

$pagesubmenuid = 48;
$stylesheets[] = "css/default.css";
$javascripts[] = "jscripts/validations.js";
$javascripts[] = "jscripts/jquery-1.5.2.min.js";

App::LoadModuleClass("TicketManagementCM", "TMDecks");
App::LoadModuleClass("TicketManagementCM", "TMGameManagement");
App::LoadModuleClass("TicketManagementCM", "TMAccounts");
App::LoadModuleClass("TicketManagementCM", "TMProducts");

$tmdecks = new TMDecks();
$tmgames = new TMGameManagement();
$tmaccounts = new TMAccounts();
$tmproducts = new TMProducts();

App::LoadControl("TextBox");
App::LoadControl("ComboBox");
App::LoadControl("Button");
App::LoadControl("Hidden");
App::LoadControl("PagingControl2");

$frmbooktransfer = new FormsProcessor();

$txtsearch = new TextBox("txtsearch","txtsearch","Search: ");
$txtsearch->Length = 6;
$txtsearch->Args = "onkeypress = 'javascript: return isNumberKey(event);'";

$allgames = $tmgames->SelectAllActive();
$gameslist = new ArrayList();
$gameslist->AddArray($allgames);
$ddlgametype = new ComboBox("ddlgametype","ddlgametype","Game Number: ");
$options = null;
$options[] = new ListItem("Please select","0");
$ddlgametype->Items = $options;
$ddlgametype->Args = "onchange='javascript: get_batchID();'";
$ddlgametype->DataSource = $gameslist;
$ddlgametype->DataSourceText = "GameNumber";
$ddlgametype->DataSourceValue = "GameID";
$ddlgametype->DataBind();
/*$ddlgametype->Args = "onchange='javascript: return get_product()'";*/

$txtproductname = new TextBox("txtproductname","txtproductname","Product Name");
//$txtproductname->Enabled = false;
$txtproductname->ReadOnly = true;

$accounts = $tmaccounts->SelectAllDistributorUser();
$accounts_list = new ArrayList();
$accounts_list->AddArray($accounts);
$ddlassingedto = new ComboBox("ddlassingedto","ddlassingedto","Assigned To:");
$litem = null;
$litem[] = new ListItem("Please select","-");
$ddlassingedto->Items = $litem;
$ddlassingedto->DataSource = $accounts_list;
$ddlassingedto->DataSourceText = "Name";
$ddlassingedto->DataSourceValue = "AID";
$ddlassingedto->DataBind();

$btnsearch = new Button("btnsearch","btnsearch","Search");
$btnsearch->IsSubmit = true;
$btnsearch->Args = "onclick = 'javascript: return booktranssearch();'";

$btnsubmit = new Button("btnsubmit","btnsubmit","Submit");
$btnsubmit->IsSubmit = true;
$btnsubmit->Args = "onclick = 'javascript: return distributorbooktransvalidation();'";

$btntransfer = new Button("btntransfer","btntransfer","Transfer");
$btntransfer->IsSubmit = true;
$btntransfer->Visible = false;
$btntransfer->Args = "onclick = 'javascript: return formUpdateBookTransSubmit();'";

//get list of game names
$gamenames = $tmproducts->SelectAllGameName();
$gamename_list = new ArrayList();
$gamename_list->AddArray($gamenames);

$ddlgamename = new ComboBox("ddlgamename","ddlgamename","Game Name: ");
$ddlgamename->Args = "onchange = 'javascript: get_gamenumber();'";
$gamenameopt = null;
$gamenameopt[] = new ListItem("Please select", "0", true);
$ddlgamename->Items = $gamenameopt;
$ddlgamename->DataSource = $gamename_list;
$ddlgamename->DataSourceText = "ProductName";
$ddlgamename->DataSourceValue = "ProductID";
$ddlgamename->DataBind();

$ddlgamebatch = new Combobox("ddlgamebatch","ddlgamebatch","Game Batch:");
$gamebatchopt = null;
$gamebatchopt[] = new ListItem("Please select","0", true);
$ddlgamebatch->Items  = $gamebatchopt;
$ddlgamebatch->Args = "onchange='javascript: get_assignedto();'";

//get list of game card value
$cardprize = $tmgames->SelectByCardValue();
$cardprize_list = new ArrayList();
$cardprize_list->AddArray($cardprize);

$ddlcardvalue = new ComboBox('ddlcardvalue','ddlcardvalue','');
$ddlcardvalue->Args = "onchange='javascript: get_gamename();'";
$cardlist = null;
$cardlist[] = new ListItem("Please select"," ",false);
//$cardlist[] = new ListItem("ALL","0",false);
$ddlcardvalue->Items = $cardlist;
$ddlcardvalue->DataSource = $cardprize_list;
$ddlcardvalue->DataSourceText = "CardPrice";
$ddlcardvalue->DataSourceValue = "CardPrice";
$ddlcardvalue->DataBind();

$hiddencttrid = new Hidden("hiddencttrid","hiddencttrid","Hidden Ctr ID");
$hiddenflag = new Hidden("hiddenflag","hiddenflag","Hidden Flag");
$hiddenfield = new Hidden("hiddenfield", "hiddenfield", "Hidden Distributor Field");

$btnExportCSV = new Button("btnExportCSV", "btnExportCSV", "Export to CSV");
$btnExportCSV->IsSubmit = true;

//pagination
$itemsperpage = 50;
$pgcon = new PagingControl2($itemsperpage, 1);
$pgcon->URL = "javascript:ChangePage(%currentpage);";
$pgcon->ShowMoveToFirstPage = true;
$pgcon->ShowMoveToLastPage = true;
$pgcon->PageGroup = 5;

$frmbooktransfer->AddControl($txtsearch);
$frmbooktransfer->AddControl($ddlgametype);
$frmbooktransfer->AddControl($txtproductname);
$frmbooktransfer->AddControl($ddlassingedto);
$frmbooktransfer->AddControl($btnsearch);
$frmbooktransfer->AddControl($btnsubmit);
$frmbooktransfer->AddControl($hiddencttrid);
$frmbooktransfer->AddControl($hiddenfield);
$frmbooktransfer->AddControl($btntransfer);
$frmbooktransfer->AddControl($ddlgamename);
$frmbooktransfer->AddControl($ddlgamebatch);
$frmbooktransfer->AddControl($hiddenflag);
$frmbooktransfer->AddControl($btnExportCSV);
$frmbooktransfer->AddControl($ddlcardvalue);

$frmbooktransfer->ProcessForms();

if($frmbooktransfer->IsPostBack)
{
    $cardvalue = $ddlcardvalue->SubmittedValue;
    $prodid = $ddlgamename->SubmittedValue;
    $gameid = $ddlgametype->SubmittedValue;
    $batchid = $ddlgamebatch->SubmittedValue;
    $assignedto = $ddlassingedto->SubmittedValue;

    if($hiddenflag->Text == 1 || $hiddenflag->Text == 4)
    {
        //game name
        $ddlgamename->Args = "onchange='javascript: get_gamenumber();'";          
        $ddlgamename->ClearItems();
        $gamenames = $tmproducts->SelectGameNamePerCardPrice($cardvalue);
        $gamename_list1 = new ArrayList();
        $gamename_list1->AddArray($gamenames);       
        $litemgmetype1 = null;
        $litemgmetype1[] = new ListItem("Please select", "0", false);
        $ddlgamename->Items = $litemgmetype1;
        $ddlgamename->DataSource = $gamename_list1;
        $ddlgamename->DataSourceText = "ProductName";
        $ddlgamename->DataSourceValue = "ProductID";
        $ddlgamename->DataBind();
        $ddlgamename->SetSelectedValue($prodid);
        
        //game num
        $ddlgametype->Args = "onchange='javascript: get_batchID(); '";
        $ddlgametype->ClearItems();
        
        $gamenumbers = $tmproducts->SelectGameNumPerGameName($prodid);
        $gamenumbers_list1 = new ArrayList();
        $gamenumbers_list1->AddArray($gamenumbers);       
        $litemgmetype12 = null;
        $litemgmetype12[] = new ListItem("Please select", "0", false);
        $ddlgametype->Items = $litemgmetype12;
        
        $ddlgametype->DataSource = $gamenumbers_list1;
        $ddlgametype->DataSourceText = "GameNumber";
        $ddlgametype->DataSourceValue = "GameID";
        $ddlgametype->DataBind();
        $ddlgametype->SetSelectedValue($gameid);
    }

    $gamebatches = $tmproducts->SelectBatchIDPerGameNum($gameid,1);
    $gamebatches_list = new ArrayList();
    $gamebatches_list->AddArray($gamebatches);
    $ddlgamebatch->DataSource = $gamebatches_list;
    $ddlgamebatch->DataSourceText = "BatchID";
    $ddlgamebatch->DataSourceValue = "GameBatchID";
    $ddlgamebatch->DataBind();
    $ddlgamebatch->SetSelectedValue($batchid);

    $ddlgametype->SetSelectedValue($gameid);
    $txtproductname->Text = $txtproductname->SubmittedValue;
    
    $chkdistributor = $tmdecks->CurrentDistributorAssigned($cardvalue, $prodid, $gameid, $batchid, $assignedto);
    // $field = $chkdistributor[0]["Field"];
    $field = "TransferredToAID";
    $hiddenfield->Text = $field;
    
    // Search button was clicked
    if($hiddencttrid->SubmittedValue == 0)
    {
        if($cardvalue == 0)
            $where = " A.BookNumber LIKE '%". $txtsearch->SubmittedValue ."%' AND A.Status = 1";
        else
            $where = " B.CardPrice = ".$cardvalue." AND A.BookNumber LIKE '%". $txtsearch->SubmittedValue ."%' AND A.Status = 1";
    }
    
    // Submit button was clicked
    if($hiddencttrid->SubmittedValue == 1)
    {
        if($cardvalue == 0)
            $where = " A.GameID = '". $gameid ."' AND A.$field = '". $assignedto ."' AND A.Status = 1 AND A.GameBatchID = '" . $batchid . "'";
        else
            $where = " B.CardPrice = ".$cardvalue." AND A.GameID = '". $gameid ."' AND A.$field = '". $assignedto ."' AND A.Status = 1 AND A.GameBatchID = '" . $batchid . "'";
    }
    
    if($btnsubmit->SubmittedValue == "Submit")
    {   
        $_SESSION["Search"] = False;
        $hiddencttrid->Text = 1;
        $pgcon->SelectedPage = 1;
        
        if($cardvalue == 0)
            $where = " A.GameID = '". $gameid ."' AND A.$field = '". $assignedto ."' AND A.Status = 1 AND A.GameBatchID = '" . $batchid . "'";
        else
            $where = " B.CardPrice = ".$cardvalue." AND A.GameID = '". $gameid ."' AND A.$field = '". $assignedto ."' AND A.Status = 1 AND A.GameBatchID = '" . $batchid . "'";
    }
    
    if ($btnExportCSV->SubmittedValue == "Export to CSV")
    {
        $csvData = array();
        if($hiddencttrid->SubmittedValue == 0)
        {
            if($cardvalue == 0)
                $where = " A.BookNumber LIKE '%". $txtsearch->SubmittedValue ."%' AND A.Status = 1";
            else
                $where = " B.CardPrice = ".$cardvalue." AND A.BookNumber LIKE '%". $txtsearch->SubmittedValue ."%' AND A.Status = 1";
        }
        
        if($hiddencttrid->SubmittedValue == 1)
        {
            if($cardvalue == 0)
                $where = " A.GameID = '". $gameid ."' AND A.$field = '". $assignedto ."' AND A.Status = 1 AND A.GameBatchID = '" . $batchid . "'";
            else
                $where = " B.CardPrice = ".$cardvalue." AND A.GameID = '". $gameid ."' AND A.$field = '". $assignedto ."' AND A.Status = 1 AND A.GameBatchID = '" . $batchid . "'";
        }
        
        if($_SESSION["Search"] == True){
            $where = $_SESSION["tmpwhere"];
        }
        
        $tbldata = $tmdecks->SelectDeckCondition($where);
        $booktranslist = new ArrayList();
        $booktranslist->AddArray($tbldata);
        
        for ($ctr = 0; $ctr < count($booktranslist); $ctr++)
        {            
            $csvData[] = $booktranslist[$ctr]["CardPrice"].",".
                         $booktranslist[$ctr]["ProductName"].",".
                         $booktranslist[$ctr]["BookTicketCount"].",".
                         $booktranslist[$ctr]["GameNumber"].",".
                         $booktranslist[$ctr]["BatchID"].",".
                         $booktranslist[$ctr]["BookNumber"].",".
                         $booktranslist[$ctr]["Status"].",".
                         $booktranslist[$ctr]["Username"].",".
                         $booktranslist[$ctr]["InvoiceNumber"]."\r\n";
        }
        
        $fp = fopen("../csv/Distributor_Book_Reassignment.csv","w");
        if ($fp)
        {
            $header = "Card Value,Game Name,Book Size,Game Number,Game Batch,Book Number,Status,Assigned To,Invoice Number" . "\r\n";
            fwrite($fp,$header);
            if ($csvData)
            {
                foreach($csvData as $rc)
                {
                    if(count($rc)>0) { fwrite($fp,$rc); }
                }
            }
            else
            {
                $rc = "\nNo Records Found;\n";
                fwrite($fp,$rc);
            }
            fclose($fp);
            header('Content-type: text/csv');
            header("Content-Disposition: attachment; filename=".'Distributor_Book_Reassignment.csv');
            header('Pragma: public');
            readfile('../csv/Distributor_Book_Reassignment.csv');
            exit;
        } 
        else 
        {
            echo "<script>alert('ok');</script>";
        }
    }
    
    if($btnsearch->SubmittedValue == "Search")
    {
        $hiddencttrid->Text = 0;
           $where = " A.BookNumber LIKE '%". $txtsearch->SubmittedValue ."%' AND A.Status = 1";
        $pgcon->SelectedPage = 1;
        $_SESSION["tmpwhere"] = $where;
        $_SESSION["Search"] = True;
    }
    
    if($_SESSION["Search"] == True){
        $wherelimit = $_SESSION["tmpwhere"] . " LIMIT " . ($pgcon->SelectedItemFrom - 1) . " , " . $itemsperpage;
    }else{
        $wherelimit = $where . " LIMIT " . ($pgcon->SelectedItemFrom - 1) . " , " . $itemsperpage;
    }
    
    $count = $tmdecks->SelectDeckCount($where, $field);
    $rowcount = $count[0]['Count'];
    $pgcon->Initialize($itemsperpage, $rowcount);
    $pgbooktransfer = $pgcon->PreRender();
    $tbldata = $tmdecks->SelectDeckCondition($wherelimit, $field);
    $booktranslist = new ArrayList();
    $booktranslist->AddArray($tbldata);
}
?>