<?php
/*
 * Added By : Arlene R. Salazar
 * Added On : Aug 24, 2011
 * Purpose : Process for account list 
 */
$pagesubmenuid = 18;
$stylesheets[] = "css/default.css";
$javascripts[] = "jscripts/validations.js";

App::LoadModuleClass("TicketManagementCM", "TMProducts");
App::LoadModuleClass("TicketManagementCM", "TMProvider");
App::LoadModuleClass("TicketManagementCM", "TMGameManagement");

App::LoadControl("TextBox");
App::LoadControl("ComboBox");
App::LoadControl("Button");
App::LoadControl("Hidden");
App::LoadControl("PagingControl2");

$frmgamelist = new FormsProcessor();
$tmproduct = new TMProducts();
$tmprovider = new TMProvider();
$tmgames = new TMGameManagement();

/*PAGING*/
$itemsperpage = 15;
$pgcon = new PagingControl2($itemsperpage, 1);
$pgcon->URL = "javascript:ChangePage(%currentpage);";
$pgcon->ShowMoveToFirstPage = true;
$pgcon->ShowMoveToLastPage = true;
/*PAGING*/

$games = $tmgames->SelectAllGames();
$gamecount = count($games);
$pgcon->Initialize($itemsperpage, $gamecount);
$pgTransactionHistory = $pgcon->PreRender();
$arrproviders = $tmgames->SelectAllGamesWithLimit(($pgcon->SelectedItemFrom - 1), $itemsperpage);
$gameslist = new ArrayList();
$gameslist->AddArray($arrproviders);

$where = " ORDER BY Name ASC";
$providers = $tmprovider->SelectAllProvider();
$providers_list = new ArrayList();
$providers_list->AddArray($providers);

$ddlProviders = new ComboBox("ddlProviders","ddlProviders","Provider: ");
$ddlProviders->ShowCaption = true;
$options = null;
$options[] = new ListItem("Please Select","",true);
$options[] = new ListItem("All","0");
$ddlProviders->Items = $options;
$ddlProviders->DataSource = $providers_list;
$ddlProviders->DataSourceText = "Name";
$ddlProviders->DataSourceValue = "ProviderID";
$ddlProviders->DataBind();
if(isset($_SESSION['prodaccttype']))
    $ddlProviders->SetSelectedValue ($_SESSION['prodaccttype']);

$hiddengameid  = new Hidden("hiddengameid","hiddengameid","Hidden game id");

$hiddenctr = new Hidden("hiddenctr","hiddenctr","Hidden Button Ctr");

$btnSubmit = new Button("btnSubmit","btnSubmit","Submit");
$btnSubmit->IsSubmit = true;
$btnSubmit->Args = "onclick='javascript: return checkProviderSubmit()'";

$btnAddGame = new Button("btnAddGame","btnAddGame","Add New Game");
$btnAddGame->IsSubmit = true;
 
$frmgamelist->AddControl($hiddengameid);
$frmgamelist->AddControl($ddlProviders);
$frmgamelist->AddControl($btnSubmit);
$frmgamelist->AddControl($btnAddGame);
$frmgamelist->AddControl($hiddenctr);


$frmgamelist->ProcessForms();

if($frmgamelist->IsPostBack)
{
    if($hiddenctr->Text == 0)
    {
        $where = " ORDER BY gm.GameNumber ASC";
    }
    if($hiddenctr->Text == 1)
    {
		if($ddlProviders->SelectedValue == "0")
            $where = " ORDER BY gm.GameNumber ASC";
        else
        	$where = " WHERE gm.ProviderID = " . $ddlProviders->SelectedValue . " ORDER BY gm.GameNumber ASC";
    }
    if($btnAddGame->SubmittedValue == "Add New Game")
    {
        URL::Redirect('addgame.php');
    }
    if($btnSubmit->SubmittedValue == "Submit")
    {
        $hiddenctr->Text = 1;
		if($ddlProviders->SelectedValue == "0")
            $where = " ORDER BY gm.GameNumber ASC";
        else
        	$where = " WHERE gm.ProviderID = " . $ddlProviders->SelectedValue . " ORDER BY gm.GameNumber ASC";
        $pgcon->SelectedPage = 1;
    }
    /*PAGING*/
    $srchgamelist = $tmgames->SelectGameByWhere($where);
    $providercount = count($srchgamelist);
    $pgcon->Initialize($itemsperpage, $providercount);
    $arrproviders = $tmgames->SelectGameByWhereWithLimit($where,($pgcon->SelectedItemFrom - 1), $itemsperpage);
    //$providercount = count($arrproviders);
    $pgTransactionHistory = $pgcon->PreRender();
    //$arrproviders = $tmgames->SelectAccountByWhereWithLimit($where,($pgcon->SelectedItemFrom - 1), $itemsperpage);
    $gameslist = new ArrayList();
    $gameslist->AddArray($arrproviders);

    /*PAGING*/
}
?>