<?php
/* * *****************
 * Author: J.O. Pormento
 * Date Created: 2011-09-08
 * Description: Get Account address
 * ***************** */

require_once '../views/init.inc.php';

$aid = $_GET['aid'];

App::LoadModuleClass("TicketManagementCM", "TMAccounts");

$account = new TMAccounts();

$address = $account->SelectAddress($aid);

$addr = $address[0]['Address'];

echo $addr;

?>