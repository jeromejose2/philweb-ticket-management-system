<?php
/* 
 * Created by: NDA 01-06-2012
 */
$gameid = $_POST["gameid"];

include("../views/init.inc.php");
App::LoadModuleClass("TicketManagementCM", "TMGameBatches");

$tmgamebatches = new TMGameBatches();

$arrbatchid = $tmgamebatches->SelectDistinctBatchID($gameid);

$options = "<option value = ''>Please Select</option>";
if(count($arrbatchid) > 0)
{
    $options = "<option value = '0'>ALL</option>";
    for($i = 0 ; $i < count($arrbatchid) ; $i++)
    {
        $options .= "<option value='".$arrbatchid[$i]["GameBatchID"]."'>".$arrbatchid[$i]["BatchID"]."</option>";
    }  
}

echo $options;
?>