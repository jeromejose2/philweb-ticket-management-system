<?php
/* 
 * Created by: Noel Antonio 03-13-2012
 */

$gamenum = $_POST["gamenum"];
$xmltype = $_POST["xmltype"];
$prodid = $_POST["prodid"];

include("../views/init.inc.php");
App::LoadModuleClass("TicketManagementCM", "TMProducts");

$tmproducts = new TMProducts();

$gamenumbers = $tmproducts->SelectBatchIDPerGameNum($gamenum,$xmltype);

$options = "<option value = '0'>ALL</option>";
if(count($gamenumbers) > 0)
{
    $options = "<option value = '0'>ALL</option>";
    for($i = 0 ; $i < count($gamenumbers) ; $i++)
    {
        $options .= "<option value='".$gamenumbers[$i]["GameBatchID"]."'>".$gamenumbers[$i]["BatchID"]."</option>";
    }
}
echo $options;
?>