<?php
/*
 * Created by: ARS 01-16-2012
 */
$batchid = $_POST["batchid"];
$status = $_POST["status"];

include("../views/init.inc.php");
App::LoadModuleClass("TicketManagementCM", "TMDecks");

$tmdecks = new TMDecks();

$booknumbers = $tmdecks->SelectBookNumbersPerBatchID($batchid,$status);

$options = "<option value = ''>Please select</option>";
if(count($booknumbers) > 0)
{
    for($i = 0 ; $i < count($booknumbers) ; $i++)
    {
        $options .= "<option value='".$i."'>".$booknumbers[$i]["BookNumber"]."</option>";
    }
}

echo $options;
?>