<?php
/*
 * Created by: ARS 01-16-2012
 */
$gamename = $_POST["gamename"];
$gamenumber = $_POST["gamenumber"];
$status = $_POST["status"];

include("../views/init.inc.php");
App::LoadModuleClass("TicketManagementCM", "TMDecks");

$tmdecks = new TMDecks();

$booknumbers = $tmdecks->SelectBookNumbersPerProductIDandGameID($gamename,$gamenumber,$status);

$options = "<option value = '0'>Please select</option>";
if(count($booknumbers) > 0)
{
    for($i = 0 ; $i < count($booknumbers) ; $i++)
    {
//        $options .= "<option value='".$i."'>".$booknumbers[$i]["BookNumber"]."</option>";
         $options .= "<option value='".$booknumbers[$i]["BookID"]."'>".$booknumbers[$i]["BookNumber"]."</option>";
    }
}

echo $options;
?>