<?php

/*
 * Created By : Jerome F. Jose 
 * Date : December 17 , 2012
 */

$gameid = $_POST["gamename"];
$cardvalue = $_POST["cardvalue"];
$flag = $_POST["flag"];

include("../views/init.inc.php");
App::LoadModuleClass("TicketManagementCM", "TMGameManagement");

$game = new TMGameManagement();


if ($flag == 1)
{
    $gamenumbers = $game->SelectByCardValue();
}
else
{
    $gamenumbers = $game->SelectCardbyCardPrice($gameid);
}

if($flag == 0 && $gameid == 0 && $cardvalue == 0) //reset all values
{
    $gamenumbers = $game->SelectByCardValue();
}


$options = "<option value = '0'>Please select</option>";
if(count($gamenumbers) > 0)
{
    for($i = 0 ; $i < count($gamenumbers) ; $i++)
    {
        if ($cardvalue == $gamenumbers[$i]["CardPrice"])
            $options .= "<option selected value='".$gamenumbers[$i]["CardPrice"]."'>".$gamenumbers[$i]["CardPrice"]."</option>";
        else
            $options .= "<option value='".$gamenumbers[$i]["CardPrice"]."'>".$gamenumbers[$i]["CardPrice"]."</option>";
    }
}

echo $options;
?>
