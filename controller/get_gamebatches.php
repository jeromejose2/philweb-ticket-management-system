<?php
/* 
 * Created by: ARS 12-01-2011
 */
$gamenum = $_POST["gamenum"];
$xmltype = $_POST["xmltype"];
$prodid = $_POST["prodid"];

include("../views/init.inc.php");
App::LoadModuleClass("TicketManagementCM", "TMProducts");

$tmproducts = new TMProducts();

$gamenumbers = $tmproducts->SelectBatchIDPerGameNum($gamenum,$xmltype);

$options = "<option value = '0'>Please select</option>";
if(count($gamenumbers) > 0)
{
    for($i = 0 ; $i < count($gamenumbers) ; $i++)
    {
        $options .= "<option value='".$gamenumbers[$i]["GameBatchID"]."'>".$gamenumbers[$i]["BatchID"]."</option>";
    }
}
echo $options;
?>