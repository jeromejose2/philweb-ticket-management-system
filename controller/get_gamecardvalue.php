<?php
/* Modified By Noel Antonio 01-24-2013 */

$prodId = $_POST['gamename'];
$gameno = $_POST['gameno'];
$flag = $_POST['flag'];
$selectno = $_POST['selectno'];

include("../views/init.inc.php");

App::LoadModuleClass("TicketManagementCM", "TMGameManagement");

$tmgames = new TMGameManagement();

if ($flag == 'Sales')
{
    if ($selectno == 1) // meaning the first entry of selection is the Product Drop Down list
    {
        if ($prodId == 0)
            $product = $tmgames->SelectByCardValue();
        else
            $product = $tmgames->GetCardValue($prodId);
    }
    else if ($selectno == 2) // else the Game Drop Down list
    {
        if ($gameno == 0)
            $product = $tmgames->SelectByCardValue();
        else
            $product = $tmgames->SelectCardValueByGameNumber($gameno);
    }
        
    $options = "";
    $options = "<option value = '0'>ALL</option>";
    if(count($product) > 0)
    {
        $options = "<option value = '0'>ALL</option>";
        for($i = 0 ; $i < count($product) ; $i++)
        {
            $options .= "<option value='".$product[$i]["CardPrice"]."'>".$product[$i]["CardPrice"]."</option>";
        }
    }
    
    echo $options;
}
else if ($flag == 'addgame')
{
    $product = $tmgames->GetCardValue($prodId);
    if(count($product) > 0)
    {
        $array = array('CardPrice'=>$product[0]["CardPrice"], 'BookTicketCount'=>$product[0]["BookTicketCount"]);
    }
    else
    {
        // then it should be blank
        $array = array('CardPrice'=>'', 'BookTicketCount'=>'');
    }
    echo json_encode($array);
}
else
{
    $product = $tmgames->GetCardValue($prodId);
    if(count($product) > 0)
    {
        echo $product[0]["CardPrice"];
    }
}

?>