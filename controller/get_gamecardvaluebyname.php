<?php
$flag = $_POST['flag'];
$gamename = addslashes($_POST['gamename']);
$provid = $_POST['provid'];
$provname = $_POST['providername'];
$cardval = $_POST['cardval'];
$bookcount = $_POST['bookcount'];

include("../views/init.inc.php");

App::LoadModuleClass("TicketManagementCM", "TMGameManagement");
App::LoadModuleClass("TicketManagementCM", "TMProducts");
App::LoadModuleClass("TicketManagementCM", "TMProvider");

$tmgames = new TMGameManagement();
$tmproducts = new TMProducts();
$tmproviders = new TMProvider();

if(isset($provname))
{
    $tmpid = $tmproviders->GetProviderID($provname);
    $provid = $tmpid[0][ProviderID];
}

$id = $tmproducts->GetProdIDByName($gamename, $provid);
$product = $tmgames->GetCardValue($id[0][0]);
if ($flag == 'addgame')
{
    if(count($product) > 0)
    {
        $array = array('CardPrice'=>$product[0]["CardPrice"], 'BookTicketCount'=>$product[0]["BookTicketCount"]);
    }
    else
    {
        $array = array('CardPrice'=>'', 'BookTicketCount'=>'');
    }
    
    echo json_encode($array);
}
else
{
    if(count($product) > 0)
    {   
        $Price = $product[0]["CardPrice"];
        echo $Price;
    }
    else
    {
        if ($cardval != NULL)
        {
            $Price = $cardval."x";
            echo $Price;
        }
    }
}
?>
