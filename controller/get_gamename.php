<?php
/* 
 * Created by: ARS 12-01-2011
 */
$cardvalue = $_POST["cardvalue"];
$gamename = $_POST["gamename"];
$gamenum = $_POST["gamenum"];
$module = $_POST["module"];

include("../views/init.inc.php");
App::LoadModuleClass("TicketManagementCM", "TMProducts");

$tmproducts = new TMProducts();

if ($module == 'TicketReassignment')
{
    if ($gamenum != 0)
        $gamenames = $tmproducts->SelectGameNamePerGameNum($gamenum);
    else if ($gamenum == 0)
        $gamenames = $tmproducts->SelectAllGameName();
}
else
{
    if ($cardvalue != 0)
        $gamenames = $tmproducts->SelectGameNamePerCardPrice($cardvalue);
}

$options = "<option value = '0'>Please select</option>";
if(count($gamenames) > 0)
{
    for($i = 0 ; $i < count($gamenames) ; $i++)
    {
        if ($gamename == $gamenames[$i]["ProductID"])
            $options .= "<option selected value='".$gamenames[$i]["ProductID"]."'>".$gamenames[$i]["ProductName"]."</option>";
        else
            $options .= "<option value='".$gamenames[$i]["ProductID"]."'>".$gamenames[$i]["ProductName"]."</option>";
    }    
}

echo $options;
?>