<?php
/* 
 * Created by: ARS 12-01-2011
 */
$gamenum = $_POST["gamenum"];

include("../views/init.inc.php");
App::LoadModuleClass("TicketManagementCM", "TMProducts");

$tmproducts = new TMProducts();

if ($gamenum == 0)
{
    $gamenames = $tmproducts->SelectAllGameName();   
    $options = "<option value = '0'>Please select</option>";
    if(count($gamenames) > 0)
    {
        for($i = 0 ; $i < count($gamenames) ; $i++)
        {   
            $options .= "<option value='".$gamenames[$i]["ProductID"]."'>".$gamenames[$i]["ProductName"]."</option>";
        }    
    }
}
else
{
    $gamenames = $tmproducts->SelectGameNamePerGameNum($gamenum);
    $options = "<option value = '0'>Please select</option>";
    if(count($gamenames) > 0)
    {
        for($i = 0 ; $i < count($gamenames) ; $i++)
        {
            if($i == 0)
            {
                $selected = "selected";
            }
            
            else
            {
                
                $selected = null;
            }
            $options .= "<option value='".$gamenames[$i]["ProductID"]."'$selected>".$gamenames[$i]["ProductName"]."</option>";
        }    
    }
}

echo $options;
?>