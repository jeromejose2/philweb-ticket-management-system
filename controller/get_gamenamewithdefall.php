<?php
/* 
 * Created by: Noel Antonio 03-13-2012
 */
$gamenum = $_POST["gamenum"];

include("../views/init.inc.php");
App::LoadModuleClass("TicketManagementCM", "TMProducts");

$tmproducts = new TMProducts();

if ($gamenum == 0)
{
    $gamenames = $tmproducts->SelectAllGameName();   
}
else
{
    $gamenames = $tmproducts->SelectGameNamePerGameNum($gamenum);
}

$options = "<option value = '0'>ALL</option>";
    if(count($gamenames) > 0)
    {
        $options = "<option value = '0'>ALL</option>";
        for($i = 0 ; $i < count($gamenames) ; $i++)
        {
            $options .= "<option value='".$gamenames[$i]["ProductID"]."'>".$gamenames[$i]["ProductName"]."</option>";
        }    
    }
echo $options;
?>