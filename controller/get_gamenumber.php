<?php
/* 
 * Created by: ARS 12-01-2011
 */
$prodid = $_POST["prodid"];

include("../views/init.inc.php");
App::LoadModuleClass("TicketManagementCM", "TMProducts");

$tmproducts = new TMProducts();

$gamenumbers = $tmproducts->SelectGameNumPerGameName($prodid);

$options = "<option value = '0'>Please select</option>";
if(count($gamenumbers) > 0)
{
    for($i = 0 ; $i < count($gamenumbers) ; $i++)
    {
        $options .= "<option value='".$gamenumbers[$i]["GameID"]."'>".$gamenumbers[$i]["GameNumber"]."</option>";
    }
    
}

echo $options;
?>