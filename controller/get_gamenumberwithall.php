<?php
/*
 * Created by: ARS 01-16-2012
 */
$prodid = $_POST["prodid"];

include("../views/init.inc.php");
App::LoadModuleClass("TicketManagementCM", "TMProducts");

$tmproducts = new TMProducts();

$gamenumbers = $tmproducts->SelectGameNumPerGameName($prodid);

$options = "<option value = ''>Please Select</option>";
if(count($gamenumbers) > 0)
{
    $options = "<option value = '0'>ALL</option>";
    for($i = 0 ; $i < count($gamenumbers) ; $i++)
    {
        $options .= "<option value='".$gamenumbers[$i]["GameID"]."'>".$gamenumbers[$i]["GameNumber"]."</option>";
    }
}

echo $options;
?>