<?php
/* 
 * Created by: Noel Antonio 03-13-2012
 */
$prodid = $_POST["prodid"];

include("../views/init.inc.php");
App::LoadModuleClass("TicketManagementCM", "TMProducts");

$tmproducts = new TMProducts();

if ($prodid == 0)
{
    $gamenumbers = $tmproducts->SelectAllGameNum();
}
else
{
    $gamenumbers = $tmproducts->SelectCardPricePerGameName($prodid);
}

$options = "<option value = '0'>ALL</option>";
if(count($gamenumbers) > 0)
{
    $options = "<option value = '0'>ALL</option>";
    for($i = 0 ; $i < count($gamenumbers) ; $i++)
    {
        $options .= "<option value='".$gamenumbers[$i]["CardPrice"]."'>".$gamenumbers[$i]["CardPrice"]."</option>";
    }
}
echo $options;
?>