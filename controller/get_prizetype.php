<?php
/* 
 * Created By: ARS 12-02-2011
 */
$gameid = $_POST["gameid"];

include("../views/init.inc.php");

App::LoadModuleClass("TicketManagementCM", "TMWinnings");
App::LoadModuleClass("TicketManagementCM", "TMCurrencies");
$TMWinnings = new TMWinnings();

$TMcurrencies = new TMCurrencies();
$arrOperators = $TMWinnings->SelectPrizeType($gameid);


$moneycurrency = 'KHR';
$getconvertmoney = $TMcurrencies->SelectByCurrencySymbol($moneycurrency);
$fixedrate = $getconvertmoney[0]['FixedRate'];

$options = "<option value = '0'>ALL</option>";
if(count($arrOperators) > 0)
{   
   
    $options = "<option value = '0'>ALL</option>";
    for($i = 0 ; $i < count($arrOperators) ; $i++)
    {

        if (strpos($arrOperators[$i]["Description"],'KHR') !== false) {
            $arrOperators[$i]["Description"] = trim($arrOperators[$i]["Description"], KHR);
            $arrOperators[$i]["Description"] = (int)str_replace(array(' ', ','), '', $arrOperators[$i]["Description"]);
            $arrOperators[$i]["Description"] = $arrOperators[$i]["Description"]/$fixedrate;
            $options .= "<option value = '". $arrOperators[$i]["PrizeID"] ."'>". '$' .number_format($arrOperators[$i]["Description"]) ."</option>";
        }else{       
        $options .= "<option value = '". $arrOperators[$i]["PrizeID"] ."'>". $arrOperators[$i]["Description"] ."</option>";
        }
        $myarray[PrizeID] = $arrOperators[$i]["PrizeID"];
        $myarray[Description] = $arrOperators[$i]["Description"];
        
    }
}
else
{
    $options = "<option value = '0'>ALL</option>";
}
echo $options;
$_SESSION[option] = $myarray;
?>