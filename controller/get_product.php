<?php
/* * *****************
 * Author: J.O. Pormento
 * Date Created: 2011-09-12
 * Description: Get Product Name by Game ID
 * ***************** */

$pid = $_GET['pid'];
$provid = $_GET['provid'];

include("../views/init.inc.php");

App::LoadModuleClass("TicketManagementCM", "TMGameManagement");
App::LoadModuleClass("TicketManagementCM", "TMProducts");

$tmgames = new TMGameManagement();
$tmproducts = new TMProducts();

if (isset($pid))
{
    $product = $tmgames->SelectProductNameByGameID($pid);
    $options = $product[0]['ProductName'];
}
else if (isset($provid))
{
    $product = $tmproducts->GetAllProductsPerProvider($provid);
    $options = "<option value = ''>- - - - - - - - - - - - - -</option>";
    if(count($product) > 0)
    {
        for($i = 0 ; $i < count($product) ; $i++)
        {
            $options .= "<option value = '" . $product[$i]["ProductID"] . "'>" . $product[$i]["ProductName"] . "</option>";
        }
    }
}

echo $options;
?>