<?php
/* Created By: Arlene R. Salazar
* Created On: 10/14/1990
* Purpose: For Adding Game
*/

$provid = $_GET['provid'];

include("../views/init.inc.php");

App::LoadModuleClass("TicketManagementCM", "TMProducts");

$tmproducts = new TMProducts();

$product = $tmproducts->GetAllProductsPerProvider($provid);

$options = "<option value = ''>- - - - - - - - - - - - - -</option>";

if(count($product) > 0)
{
    for($i = 0 ; $i < count($product) ; $i++)
    {
        $options .= "<option value = '" . $product[$i]["ProductID"] . "'>" . $product[$i]["ProductName"] . "</option>";
    }
}

echo $options;
?>