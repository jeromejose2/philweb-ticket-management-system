<?php
//error_reporting(E_ALL);
ignore_user_abort(true);                                                        //Ignore user aborts and force the script running until the last part
set_time_limit(0);                                                              //Limits the maximum execution time , if set to 0 no maximum time is imposed. Safe mode should be turned off.
/* 
 * Created By: Arlene R. Salazar 03/21/2012
 * Purpose: Controller for multiple inventory xml upload
 */

$pagesubmenuid = 31;
$stylesheets[] = "css/default.css";
$javascripts[] = "jscripts/validations.js";

App::LoadModuleClass("TicketManagementCM", "TMGameImport");
App::LoadModuleClass("TicketManagementCM", "TMProducts");
App::LoadModuleClass("TicketManagementCM", "TMProvider");
App::LoadModuleClass("TicketManagementCM", "TMGameManagement");
App::LoadModuleClass("TicketManagementCM", "TMAuditLog");
App::LoadModuleClass("TicketManagementCM", "TMDecks");
App::LoadModuleClass("TicketManagementCM", "TMDeckInfo");
App::LoadModuleClass("TicketManagementCM", "TMBoxes");
App::LoadModuleClass("TicketManagementCM", "TMBooks");
App::LoadModuleClass("TicketManagementCM", "TMTickets");
App::LoadModuleClass("TicketManagementCM", "TMInventoryTemp");
App::LoadModuleClass("TicketManagementCM", "TMGameBatches");
App::LoadModuleClass("TicketManagementCM", "TMBatchTransactionLog");

$tmgameimport = new TMGameImport();
$tmproducts = new TMProducts();
$tmproviders = new TMProvider();
$tmgames = new TMGameManagement();
$auditlog = new TMAuditLog();
$tmdecks2 = new TMDecks();
$tmdeckinfo = new TMDeckInfo();
$tmboxes = new TMBoxes();
$tmbooks = new TMBooks();
$tmtickets = new TMTickets();
$tminventorytemp = new TMInventoryTemp();
$tmgamebatches = new TMGameBatches();
$tmbatchtranslog = new TMBatchTransactionLog();

App::LoadControl("ComboBox");
App::LoadControl("Button");
App::LoadControl("Hidden");

$frmMultipleXMLUpload = new FormsProcessor();

$xmlnaming = App::getParam("xmlnaming");

$hiddenid = new Hidden("hiddenid","hiddenid","Hidden ID");
$hiddenid->Text = "1";
$hiddenUploadedFiles = new Hidden("hiddenUploadedFiles","hiddenUploadedFiles","Hidden Uploaded Files");
$hiddenTag = new Hidden("hiddenTag","hiddenTag","Hidden Tag");
$hiddenSuccessfulUpload = new Hidden("hiddenSuccessfulUpload","hiddenSuccessfulUpload","Hidden Successful Upload");
$hiddenSuccessfulUpload->Text = 0;
$hiddenBookCount = new Hidden("hiddenBookCount","hiddenBookCount","Hidden Book Count");
$hiddenBookCount->Text = "0";
$hiddenBoxCount = new Hidden("hiddenBoxCount","hiddenBoxCount","Hidden Box Count");
$hiddenBoxCount->Text = "0";
$hiddenProviderName = new Hidden("hiddenProviderName","hiddenProviderName","Hidden Provider Name");
$hiddenGameName = new Hidden("hiddenGameName","hiddenGameName","Hidden Game Name");
$hiddenGameNum = new Hidden("hiddenGameNum","hiddenGameNum","Hidden Game Name");


$hiddenfilename = new Hidden("hiddenfilename","hiddenfilename","Hidden file name");
$hiddengameid = new Hidden("hiddengameid","hiddengameid","Hidden gameid");
$hiddenproductid = new Hidden("hiddenproductid","hiddenproductid","Hidden productid");
$hiddenproviderid = new Hidden("hiddenproviderid","hiddenproviderid","Hidden providerid");
//$hiddenprovidername = new Hidden("hiddenprovidername","hiddenprovidername","Hidden provider name");
$hiddenprodname = new Hidden("hiddenprodname","hiddenprodname","Hidden prod name");
//$hiddengamenum = new Hidden("hiddengamenum","hiddengamenum","Hidden game num");
$hiddenbatchid = new Hidden("hiddenbatchid","hiddenbatchid","Hidden batch id");

$btnDone = new Button("btnDone","btnDone","Upload");
$btnDone->IsSubmit = true;

$btnOkay = new Button("btnOkay","btnOkay","Okay");
$btnOkay->Args = "onclick = 'javascript: return redirecttoMultipleUpload()'";

$providers = $tmproviders->SelectAllProvider();
$ddlProviders = new ComboBox("ddlProviders","ddlProviders","Provider : ");
$ddlProviders->ShowCaption = true;

if(count($providers) > 0)
{
    $providers_list = new ArrayList();
    $providers_list->AddArray($providers);
    $ddlProviders->DataSource = $providers_list;
    $ddlProviders->DataSourceText = "Name";
    $ddlProviders->DataSourceValue = "ProviderID";
    $ddlProviders->DataBind();
    $ddlProviders->SetSelectedValue(1);
}
else
{
    $options = null;
    $options[] = new ListItem("- - - - - - - - - - - - - -","",true);
    $ddlProviders->Items = $options;
}

$frmMultipleXMLUpload->AddControl($hiddenid);
$frmMultipleXMLUpload->AddControl($hiddenUploadedFiles);
$frmMultipleXMLUpload->AddControl($hiddenTag);
$frmMultipleXMLUpload->AddControl($hiddenSuccessfulUpload);
$frmMultipleXMLUpload->AddControl($hiddenBookCount);
$frmMultipleXMLUpload->AddControl($hiddenBoxCount);
$frmMultipleXMLUpload->AddControl($hiddenProviderName);
$frmMultipleXMLUpload->AddControl($hiddenGameName);
$frmMultipleXMLUpload->AddControl($hiddenGameNum);
$frmMultipleXMLUpload->AddControl($btnDone);
$frmMultipleXMLUpload->AddControl($ddlProviders);

$frmMultipleXMLUpload->AddControl($hiddenfilename);
$frmMultipleXMLUpload->AddControl($hiddengameid);
$frmMultipleXMLUpload->AddControl($hiddenproductid);
$frmMultipleXMLUpload->AddControl($hiddenproviderid);
//$frmMultipleXMLUpload->AddControl($hiddenprovidername);
$frmMultipleXMLUpload->AddControl($hiddenprodname);
//$frmMultipleXMLUpload->AddControl($hiddengamenum);
$frmMultipleXMLUpload->AddControl($hiddenbatchid);

$frmMultipleXMLUpload->ProcessForms();
$loading = "Uploading Inventory XML file.";
if($frmMultipleXMLUpload->IsPostBack)
{   
    $filetoupload = $hiddenid->Text;
    $successful_upload = $hiddenSuccessfulUpload->Text;
    
    if(isset($_FILES['file' . $filetoupload]['name']) || isset($_FILES))
    {
        if (!isset($_FILES['file' . $filetoupload]))
        {
            $filetoupload = 1;
        }
        
        if($_FILES['file' . $filetoupload]['type'] == "")
        {
            $errormsg = "File does not exist. Please enter the directory where the file is located.";
            $errormsgtitle = "ERROR!";
        }
        else if($_FILES['file' . $filetoupload]['type'] == "text/xml")
        {
            $filename = $_FILES["file" . $filetoupload]["name"];
            $fullpath = "../xml/inventory/" . $_FILES["file" . $filetoupload]["name"];

            // checking if the corect file is being uploaded
            $filechecking = explode("_", $filename);

            // checking if file format is correct
            $length = 0;
            if ($xmlnaming == "FT"){ $length = 20;} 
            else { $length = 23; }
            $fileformatchecking = explode("_", $filename);
            if((count($fileformatchecking) > 3) || (count($fileformatchecking) < 3))
            {
                $errormsg = "Invalid file name format. Please upload a valid file name format.";
                $errormsgtitle = "ERROR!";
            }
            else
            {
                if((strlen($fileformatchecking[0]) > $length) || (strlen($fileformatchecking[0]) < $length))
                {
                    $errormsg = "Invalid file name format. Please upload a valid file name format.";
                    $errormsgtitle = "ERROR!";
                }
                else if((strlen($fileformatchecking[1]) > 1) || (strlen($fileformatchecking[1]) < 1))
                {
                    $errormsg = "Invalid file name format. Please upload a valid file name format.";
                    $errormsgtitle = "ERROR!";
                }
      		else if(strlen($fileformatchecking[2]) > 5) 
                {
                    $errormsg = "Invalid file name format. Please upload a valid file name format.";
                    $errormsgtitle = "ERROR!";
                }                            
                else
                {
                    if($filechecking[0] != "WLIST")
                    {
                        //read xml file
                        move_uploaded_file($_FILES["file" . $filetoupload]["tmp_name"], $fullpath);
                        $xml = simplexml_load_file($fullpath);
                        libxml_use_internal_errors(true);

                        //checking if xml structure is correct
                        if(!$xml)
                        {
                            $errors = libxml_get_errors();

                            foreach ($errors as $error)
                            {
                                $errormsgtitle = "ERROR!";
                                $errormsg = display_xml_error($error);
                            }

                            libxml_clear_errors();
                        }
                        else
                        {
                            $xmlUrl = $fullpath;
                            $xmlStr = file_get_contents($xmlUrl);
                            $xmlObj = simplexml_load_string($xmlStr);
                            $arrXml = objectsIntoArray($xmlObj);
                            $xmlname = $xml->getName();
                            $keys_array = array_keys_multi($arrXml);

                            //checking if node carton_NNNN exists
                            $carton_node = preg_grep('/^carton_.*/', $keys_array);

                            //checking if node pack_number exists
                            $packnum_node = preg_grep('/^pack_number.*/', $keys_array);

                            //checking if there are empty nodes
                            $emptynodes = in_array_r(Array(), $arrXml);
                            if(!preg_match("/$xmlnaming/", $xmlname))
                            {
                                $errormsgtitle = "ERROR!";
                                $errormsg = "Fatal Error: Missing game number node. <br/> File: $fullpath";
                            }
                            else if(count($emptynodes) > 0)
                            {
                                $errormsgtitle = "ERROR!";
                                $errormsg = "Fatal Error: Empty node/s in " . implode(',',$emptynodes) . "<br/> File: $fullpath";
                            }
                            else if(count($carton_node) == 0)
                            {
                                $errormsgtitle = "ERROR!";
                                $errormsg = "Fatal Error: Missing carton node. <br/> File: $fullpath";
                            }
                            else if(count($packnum_node) == 0)
                            {
                                $errormsgtitle = "ERROR!";
                                $errormsg = "Fatal Error: Missing pack_number node. <br/> File: $fullpath";
                            }
                            else if(count($carton_node) != count($packnum_node))
                            {
                                $errormsgtitle = "ERROR!";
                                $errormsg = "Fatal Error: Carton count and pack_number do not match. <br/> File: $fullpath";
                            }
                            else
                            {
                                //checking if gamenumber exists
                                $explode_uscore = explode("_" , $filename);
                                $explode_space = explode(" " , $explode_uscore[0]);
                                $gamecode = $filechecking[0];
                                $providerid = $ddlProviders->SelectedValue;
                                $gameinfo = $tmgames->SelectGameDtlsByWhere(substr($xmlname, 2, strlen($xmlname)), $providerid);

                                if(count($gameinfo) > 1)
                                {
                                    $errormsgtitle = "ERROR!";
                                    $errormsg = "Fatal Error: Game number has multiple instances. <br/> File: $fullpath";
                                }
                                else if(count($gameinfo) == 1)
                                {
                                    $gameid = $gameinfo[0]["GameID"];
                                    $bookticketcount = $gameinfo[0]["BookTicketCount"];
                                    $isUploaded = $tmgameimport->CheckIfGameIdIsUploaded($gameid, "inventory");
                                    $productgameinfo = $tmproducts->GetProductNameGameNum($gameid);
                                    $providername = $tmproviders->SelectByID($providerid);
                                    $productid = $productgameinfo[0]["ProductID"];

                                    if($productid != "")
                                    {
                                        if ($productgameinfo[0]["ProviderID"] != $providerid)
                                        {
                                            $errormsgtitle = "ERROR!";
                                            $errormsg = "Provider assigned to game number do not match chosen provider.";
                                        }
                                        else
                                        {
                                            //generate batch id
                                            $batch = $tmgamebatches->SelectBatchID($gameid,0);
                                            if(count($batch) > 0)
                                            {
                                                $gamebatchnum = 1;
                                            }
                                            else
                                            {
                                                $batches = $tmgamebatches->SelectMaxBatchID($gameid,1);
                                                if ($batches[0]["maxbatchid"] == 0 || $batches[0]["maxbatchid"] == NULL)
                                                    $gamebatchnum = 0;
                                                else
                                                    $gamebatchnum = ($batches[0]["maxbatchid"] + 1);
                                            }
                                            
                                            list($box,$book,$msgcode,$msg,$msgtitle) = UploadInventoryXmlFile($filename,$gameid,$productid,$providerid,$arrXml,$xmlname,$providername[0]["Name"],$productgameinfo[0]["ProductName"],$productgameinfo[0]["GameNumber"],$gamebatchnum,$bookticketcount);
                                            if ($msgcode == 0)
                                            {
                                                $boxcount = ($hiddenBoxCount->Text + $box);
                                                $bookcount = ($hiddenBookCount->Text + $book);
                                                $hiddenBoxCount->Text = $boxcount;
                                                $hiddenBookCount->Text = $bookcount;

                                                $hiddenGameNum->Text = $productgameinfo[0]["GameNumber"];
                                                $hiddenProviderName->Text = $providername[0]["Name"];
                                                $hiddenGameName->Text = $productgameinfo[0]["ProductName"];
                                                $hiddenTag->Text = 1;
                                                $files_uploaded = $hiddenUploadedFiles->Text;
                                                $files_uploaded = $files_uploaded . $_FILES['file' . $filetoupload]['name'] . "<br/>";
                                                $hiddenUploadedFiles->Text = $files_uploaded;

                                                //-xmlupload
    //                                                    $hiddenfilename->Text = $filename;
    //                                                    $hiddengameid->Text = $gameid;
    //                                                    $hiddenproductid->Text = $productid;
    //                                                    $hiddenproviderid->Text = $providerid;
    //                                                    $hiddenprodname->Text = $productgameinfo[0]["ProductName"];
    //                                                    $hiddenprovidername->Text = $providername[0]["Name"];
    //                                                    $hiddengamenum->Text = $productgameinfo[0]["GameNumber"];
    //                                                    $hiddenbatchid->Text = $gamebatchnum;
                                                     //-xmlupload

                                                $successful_upload++;
                                                $hiddenSuccessfulUpload->Text = $successful_upload;  
                                            } else {
                                                $errormsgtitle = $msgtitle;
                                                $errormsg = $msg;
                                            }
//                                                if (count($isUploaded) > 0)
//                                                {
//                                                    $confirmuploadmsg = "Are you sure you want to upload another file for this game number?";
//                                                    $confirmuploadtitle = "CONFIRMATION";
//                                                }
//                                                else
//                                                {
//                                                    list($msgtitle,$msg) = UploadInventoryXmlFile($filename,$gameid,$productid,$providerid,$arrXml,$xmlname,$providername[0]["Name"],$productgameinfo[0]["ProductName"],$productgameinfo[0]["GameNumber"],$gamebatchnum);
//                                                    $countmsgtitle = $msgtitle;
//                                                    $countmsg = $msg;
//                                                }  
                                        }
                                    }
                                    else
                                    {
                                        $errormsgtitle = "ERROR!";
                                        $errormsg = "Product id do not exists.";
                                    }
                                }
                                else
                                {
                                    $errormsgtitle = "ERROR!";
                                    $errormsg = "Game number do not exists. Please ensure that the filename is correct.";
                                }
                            }
                        }
                    }
                }
            }
        }
        else
        {
            $errormsg = "Incorrect file format. Please upload a valid file format.";
            $errormsgtitle = "ERROR!";
        }
    }
//    else{
//        if($_FILES['file' . $filetoupload]['type'] == "")
//        {
//            $errormsg = "File does not exist. Please enter the directory where the file is located.";
//            $errormsgtitle = "ERROR!";
//        }else {
//        $counttitle = "Inventory File Summary";
//        $countmsg = "Provider Name: " . $hiddenProviderName->Text . "<br/>" .
//                    "Game Name: " . $hiddenGameName->Text . "<br/>" .
//                    "Game Number: " . $hiddenGameNum->Text . "<br/>" .
//                    "Total No. of Cartons: " . number_format((int)$hiddenBoxCount->Text) . "<br/>" .
//                    "Total No. of Books: " . number_format((int)$hiddenBookCount->Text);
//    }
//        
        
//        $counttitle = "Inventory File Summary";
//        $countmsg = "Provider Name: " . $hiddenProviderName->Text . "<br/>" .
//                    "Game Name: " . $hiddenGameName->Text . "<br/>" .
//                    "Game Number: " . $hiddenGameNum->Text . "<br/>" .
//                    "Total No. of Cartons: " . number_format((int)$hiddenBoxCount->Text) . "<br/>" .
//                    "Total No. of Books: " . number_format((int)$hiddenBookCount->Text);

    if($btnDone->SubmittedValue == "Upload")
    { 
        if ($hiddenBookCount->SubmittedValue > 0)
        {
            $counttitle = "Inventory File Summary";
            $countmsg = "Provider : " . $hiddenProviderName->SubmittedValue . "<br/>" .
                    "Game Name : " . $hiddenGameName->SubmittedValue . "<br/>" .
                    "Game Number : " . $hiddenGameNum->SubmittedValue . "<br/>" .
                    "Total No. of Cartons : " . number_format((int)$hiddenBoxCount->SubmittedValue) . "<br/>" .
                    "Total No. of Books : " . number_format((int)$hiddenBookCount->SubmittedValue);
        }
    }
}
//function for checking xml structure
function display_xml_error($error)
{
    $return = "";

    switch ($error->level) {
        case LIBXML_ERR_WARNING:
            $return .= "Warning $error->code: ";
            break;
         case LIBXML_ERR_ERROR:
            $return .= "Error $error->code: ";
            break;
        case LIBXML_ERR_FATAL:
            $return .= "Fatal Error $error->code: ";
            break;
    }

    $return .= trim($error->message) .
               "<br>  Line: $error->line" .
               "<br/>  Column: $error->column";

    if ($error->file) {
        $return .= "<br/>  File: " . $error->file . "";
    }

    return addslashes("$return");
}

//function for checking if xml array have empty nodes
function in_array_r($needle, $haystack) {
    $emptynodeskey = array();
    foreach ($haystack as $key => $item) {
        if ($item === $needle || (is_array($item) && in_array_r($needle, $item))) {
            //return true;
            $emptynodeskey[] = $key;

        }
    }
    //return false;
    return $emptynodeskey;
}

//function for converting array keys to 1 dimensional array
function array_keys_multi(array $array)
{
    $keys = array();

    foreach ($array as $key => $value) {
        $keys[] = $key;

        if (is_array($array[$key])) {
            $keys = array_merge($keys, array_keys_multi($array[$key]));
        }
    }

    return $keys;
}

//function in converting xml to array
function objectsIntoArray($arrObjData, $arrSkipIndices = array())
{
    $arrData = array();

    // if input is object, convert into array
    if (is_object($arrObjData)) {
        $arrObjData = get_object_vars($arrObjData);
    }

    if (is_array($arrObjData)) {
        foreach ($arrObjData as $index => $value) {
            if (is_object($value) || is_array($value)) {
                $value = objectsIntoArray($value, $arrSkipIndices); // recursive call
            }
            if (in_array($index, $arrSkipIndices)) {
                continue;
            }
            $arrData[$index] = $value;
        }
    }
    return $arrData;
}

//function in parsing xml data
function UploadInventoryXmlFile($filename,$gameid,$productid,$providerid,$arrXml,$xmlname,$providername,$productname,$gamenum,$gamebatchnum,$bookticketcount)
{
    $tmgameimport = new TMGameImport();
    $tmproducts = new TMProducts();
    $tmproviders = new TMProvider();
    $tmgames = new TMGameManagement();
    $auditlog = new TMAuditLog();
    $tmdecks2 = new TMDecks();
    $tmdeckinfo = new TMDeckInfo();
    $tmboxes = new TMBoxes();
    $tmbooks = new TMBooks();
    $tmtickets = new TMTickets();
    $tminventorytemp = new TMInventoryTemp();
    $tmgamebatches = new TMGameBatches();
    $tmbatchtranslog = new TMBatchTransactionLog();

    $totalboxcount = 0;
    $totalbookcount = 0;
    $totaldeckcount = 0;
    $totalticketcount = 0;
    $ticketcount = $bookticketcount;
    $deletedecksaffectedrow = 0;
    $deletebooksaffectedrow = 0;
    $deleteboxesaffectedrow = 0;
    $deleteticketaffectedrow = 0;
    $boxesaffectedrows = 0;
    $booksaffectedrows = 0;
    $decksaffectedrows = 0;
    $msgcode = 0;
    
    /* Step 1:
     * generate batch id
     */
    if($gamebatchnum == 1)
    {
        //update XMLType
        $batchid = $tmgamebatches->SelectBatchID($gameid,0);
        $rowid = $batchid[0]["GameBatchID"];
        $gamebatch["XMLType"] = 1;
        $gamebatch["GameBatchID"] = $rowid;
        $tmgamebatches->UpdateByArray($gamebatch);
        $gamebatchid = $rowid;
    }
    else
    {
        if ($gamebatchnum == 0)
            $gamebatchnum = 1;
        
        //generate new game batch number
        $gamebatch["GameID"] = $gameid;
        $gamebatch["BatchID"] = $gamebatchnum;
        $gamebatch["XMLType"] = 1;
        $tmgamebatches->Insert($gamebatch);
        $gamebatchid = $tmgamebatches->LastInsertID;
    }

    /*Added by Arlene R. Salazar 03/08/2012*/
    $fullpath = "../xml/inventory/" . $filename;
    $filename_parts = explode(".",$filename);
    $filename_1 = $filename_parts[0] . "_" . $gamebatchid;
    $filename_2 = $filename_parts[1];
    $new_filename = $filename_1 . "." . $filename_2;
    $new_fullpath = "../xml/inventory/" . $new_filename;
    rename($fullpath,$new_fullpath);


    /* Step 2:
     * insert uploaded xml in the table
     */
    $tmimport["ProviderID"] = $providerid;
    $tmimport["FileName"] = $new_filename;
    $tmimport["FilePath"] = $new_fullpath;
    $tmimport["DateUploaded"] = 'now_usec()';
    $tmimport["ProductID"] = $productid;
    $tmimport["Status"] = '0';
    $tmimport["GameBatchID"] = $gamebatchid;
    $tmimport["GameID"] = $gameid;
    $tmgameimport->Insert($tmimport);
    $gameimportid = $tmgameimport->LastInsertID;
    if ($tmgameimport->HasError)
    {
        $errormsg = $tmgameimport->getError();
        $errormsgtitle = "ERROR!";
        $msgcode = 1;
    }
    /* Step 3:
     * audit trail log
     */
    $auditdtls["SessionID"] = $_SESSION['sid'];
    $auditdtls["AID"] = $_SESSION['acctid'];
    $auditdtls["TransDetails"] = "GameImport ID: " . $tmgameimport->LastInsertID;
    $auditdtls["TransDateTime"] = "now_usec()";
    $auditdtls["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
    $auditdtls["AuditTrailFunctionID"] = 17;
    $insertauditlog = $auditlog->Insert($auditdtls);
    if ($auditlog->HasError) {
        $errormsgtitle = "ERROR!";
        $errormsg = "Error has occured:" . $tmaccount->getError();
        $msgcode = 1;
    }

    $errormsg = $filename . " has been successfully uploaded";
    $errormsgtitle = "UPLOAD SUCCESSFUL";

    $gameno = substr($xmlname, 2, strlen($xmlname));
    $gamecode = "$xmlnaming" . $gameno;
    $id = $gameimportid;

    /* Step 4:
     * Insert to deck info
     */
    $deckinfo_success = 0;
    $deckinfo["ProductID"] = $productid;
    $deckinfo["GameID"] = $gameid;
    $deckinfo["ProviderID"] = $providerid;
    $deckinfo["CreatedByAID"] = $_SESSION["acctid"];
    $insertdeck = $tmdeckinfo->Insert($deckinfo);

    if ($tmdeckinfo->HasError)
    {
        $msgcode = 1;
    }
    else
    {
        $deckinfo_success++;
        $lastinsertdeckid = $tmdeckinfo->LastInsertID;
    }

    if ($deckinfo_success == 1)
    {
          $booksaffectedrows = $totalbooks;
        $cartoncount = 0;
        $bookcount = 0;
        $imax1 = 0;
        $inventory_array = null;
        $status = 0;
        $csvcontent ='';

        foreach($arrXml as $key => $value)
        {
            if($status != 2)
            {
                $inventoryinfo = null;
                $imax1 = 0;
                $carton = $key;
                $inventory = $value;

                do{
                    $tempinventory_array = null;
                    for($i = 0 ; $i < count($inventory["pack_number"]) ; $i++)
                    {
                        $inventoryinfo["GameID"] = $gameid;
                        $inventoryinfo["ProviderID"] = $providerid;
                        $inventoryinfo["ProductID"] = $productid;
                        $inventoryinfo["DeckID"] = $lastinsertdeckid;
                        $inventoryinfo["PackNumber"] = trim($inventory["pack_number"][$i]);
                        $inventoryinfo["Carton"] = trim($carton);
                        $inventoryinfo["AccountID"] = $_SESSION["acctid"];
                        $inventoryinfo["GameImportID"] = $gameimportid;
                        $inventoryinfo["GameBatchID"] = $gamebatchid;

                        $csvcontent .= implode(";", $inventoryinfo) . "<br>";
                        $tempinventory_array++;
                        $inventory_array++;
                    }
                    $imax1++;

                    if ($imax1 > 2)
                    {
                        $status = 2;
                        break;
                    }
                    else
                    {
                        $status = 1;
                    }
                }while($tempinventory_array != count($inventory["pack_number"]));
            }
            else
            {
                break;
            }
        }

        $imax = 0;
        $tmax = 0;

        $inventory_array_count = $inventory_array;
        if($status != 2)
        {
             do{
                /* Step 5:
                 * delete from inventorytemp where AccountAID == $_SESSION["acctid"]
                 * Insert into temp table inventorytemp
                 */
                //$tminventorytemp->DeleteRecords($_SESSION['acctid']);
                $tminventorytemp->TruncateInventoryTemp();
                //insert into CSV file
                /*$array_1dimension = array_map("implode_inner_array", $inventory_array);
                $csv_content = implode('<br>', $array_1dimension);
                $inventory_array = null;*/
                $fp = fopen('../csv/inventory.csv', 'w');
                fwrite($fp, $csvcontent);
                fclose($fp);
                $csvcontent = null;
                //insert to inventorytemp using csv file
                $tminventorytemp->InsertInventoryUsingCSVFile('../csv/inventory.csv', ';', '<br>');
                //$tminventorytemp->InsertMultiple($inventory_array);
                $decks = $tmdecks2->SelectAll();
                $tempinv = $tminventorytemp->SelectAll();

                $isExist = $tminventorytemp->CheckGameandBookExistInventory($gameid);
                if ($isExist[0]["count"] > 0)
                {
                    $msgcode = 2;
                } else {
                    /* Step 6:
                     * Insert boxes
                     */
                    $tmboxes->InsertBoxesFromInventoryTemp($gameid,$providerid,$productid,$_SESSION['acctid'],$lastinsertdeckid,$gameimportid,$gamebatchid);
                    $boxesaffectedrows = $tmboxes->AffectedRows;

                    /* Step 7:
                     * Insert books
                     */
                    $tmbooks->InsertBooksFromInventoryTemp($gameid,$providerid,$productid,$_SESSION['acctid'],$lastinsertdeckid,$gameimportid,$gamebatchid);
                    $booksaffectedrows = $tmbooks->AffectedRows;
                  
                    
                    /* Step 8:
                     * Insert to decks
                     */
                    $tmdecks2->InsertDecksFromInventoryTemp($gameid, $providerid, $productid, $lastinsertdeckid, $_SESSION['acctid'], $gameimportid,$gamebatchid);
                    $decksaffectedrows = $tmdecks2->AffectedRows;

                    /* Step 9:
                     * If affected rows do not match the total no of rows that must be inserted, delete all inserted records
                     */
                    if($decksaffectedrows != $inventory_array_count)
                    {
                        $lastdeckid = $tmdecks2->GetLastDeckID();
                        $lastboxid = $tmboxes->GetLastBoxID();
                        $lastbookid = $tmbooks->GetLastBookID();

                        //delete all inserted decks
                        if($lastdeckid[0]["LastID"] != NULL)
                        {
                            $start = ($lastdeckid[0]["LastID"] - $decksaffectedrows);
                            $deletealltransactions = $tmdecks2->DeleteAllTransactions($start,$lastdeckid[0]["LastID"]);
                            $deletedecksaffectedrow = ($deletedecksaffectedrow + $tmdecks2->AffectedRows);
                        }

                        if($lastboxid[0]["LastID"] != NULL)
                        {
                            $start = ($lastboxid[0]["LastID"] - $boxesaffectedrows);
                            $deletealltransactions = $tmboxes->DeleteAllTransactions($start,$lastboxid[0]["LastID"]);
                            $deleteboxesaffectedrow = ($deleteboxesaffectedrow + $tmboxes->AffectedRows);
                        }

                        if($lastbookid[0]["LastID"] != NULL)
                        {
                            $start = ($lastbookid[0]["LastID"] - $booksaffectedrows);
                            $deletealltransactions = $tmbooks->DeleteAllTransactions($start,$lastbookid[0]["LastID"]);
                            $deletebooksaffectedrow = ($deletebooksaffectedrow + $tmbooks->AffectedRows);
                        }

                        //delete or update batch id
                        if($gamebatchnum == 1)
                        {
                            $tmgamebatches->UpdateStatus($gamebatchid, 0);
                            $gamebatchid = 0;
                        }
                        else
                        {
                            $tmgamebatches->DeleteByGameBatchesID($gamebatchid);
                            $gamebatchid = 0;
                        }
                        $decksaffectedrows = 0;
                        $booksaffectedrows = 0;
                        $boxesaffectedrows = 0;
                    }

                    $imax++;

                    if ($imax > 2)
                    {
                        $status = 2;
                        break;
                    }
                    else
                    {
                        $status = 1;
                    }
            }            
            }while($decksaffectedrows != $inventory_array_count);
        }
        else
        {
            //delete or update batch id
            if($gamebatchnum == 1)
            {
                $tmgamebatches->UpdateStatus($gamebatchid, 0);
                $gamebatchid = 0;
            }
            else
            {
                $tmgamebatches->DeleteByGameBatchesID($gamebatchid);
                $gamebatchid = 0;
            }
        }

        if($status != 2)
        {
            /* Step 10:
             * Insert tickets per book
             */
            $bookid = $tmbooks->SelectAllInsertedBookID($gameid,$providerid,$productid,$_SESSION['acctid'],$lastinsertdeckid,$gameimportid,$gamebatchid);

            if(count($bookid) > 0)
            {
                do
                {
                    for($b = 0 ; $b < count($bookid) ; $b++)
                    {
                        for($a = 1 ; $a < ($ticketcount + 1) ; $a++)
                        {
                            $ticket["BookID"] = $bookid[$b]["BookID"];
                            $ticket["TicketNumber"] = str_pad($a,3,0,STR_PAD_LEFT);
                            $csvcontent .= implode(";", $ticket) . "<br>";
                            $arrTickets++;
                        }
                    }
                    $tmax++;
                    if ($tmax > 2)
                    {
                        $arrTickets = null;
                        $status = 2;
                        break;
                    }
                    else
                    {
                        $status = 1;
                    }
                }while($arrTickets != (count($bookid) * $ticketcount));

                $arrTickets_count = $arrTickets;
                if($arrTickets_count > 0)
                {
                    //insert into CSV file
                    $fp = fopen('../csv/tickets.csv', 'w');
                    fwrite($fp, $csvcontent);
                    fclose($fp);
                    $csvcontent = null;
                    //insert to inventorytemp using csv file
                    $tmtickets->InsertTicketsUsingCSVFile('../csv/tickets.csv', ';', '<br>');
                    $insertticketaffectedrows = $tmtickets->AffectedRows;
                    $maxid = $tmtickets->GetLastTicketID();
                    if($insertticketaffectedrows != (count($bookid) * $ticketcount))
                    {
                        if($maxid[0]["lastid"] != NULL)
                        {
                            $start = ($maxid[0]["lastid"] - $insertticketaffectedrows);
                            $deletealltransactions = $tmtickets->DeleteAllTransactions($start,$maxid[0]["lastid"]);
                            $deleteticketaffectedrow = ($deleteticketaffectedrow + $tmtickets->AffectedRows);
                        }
                    }
                }
            }
        }

        /* Step 11:
         * Delete all records to inventorytemp
         */
        $tminventorytemp->TruncateInventoryTemp();

        if ($msgcode != 2)
        {
            /* Step 12:
             * Update xml status
             */
            $xmlstatus["GameID"] = $gameid;
            $xmlstatus["Status"] = $status;
            $xmlstatus["GameImportID"] = $gameimportid;
            $xmlstatus["DateImported"] = "now_usec()";
            $xmlstatus["GameBatchID"] = $gamebatchid;
            $updatestatus = $tmgameimport->UpdateByArray($xmlstatus);
            if ($tmgameimport->HasError)
            {
                $errormsgtitle = "ERROR!";
                $errormsg = "Error:" . $tmgameimport->getError();
                $msgcode = 1;
            }

            /* Step 13:
             * Update deck info
             */
            if($status != 2)
            {
                $deckinfoupdate["BoxCount"] = $boxesaffectedrows;
                $deckinfoupdate["BookCount"] = $booksaffectedrows;
                $deckinfoupdate["DeckID"] = $lastinsertdeckid;
                $deckinfoupdate["GameBatchID"] = $gamebatchid;
                $updatedeckinfo = $tmdeckinfo->UpdateByArray($deckinfoupdate);
                if ($tmdeckinfo->HasError)
                {
                    $errormsgtitle = "ERROR!";
                    $errormsg = "Error:" . $tmdeckinfo->getError();
                    $msgcode = 1;
                }
            }
            else
            {
                $tmdeckinfo->DeleteByDeckInfoID($lastinsertdeckid);
            }

            /* Step 14:
             * insert to audit trail
             */
            $auditdtls["SessionID"] = session_id();
            $auditdtls["AID"] = $_SESSION["acctid"];
            $auditdtls["TransDetails"] = "GameImport ID: " . $id;
            $auditdtls["TransDateTime"] = "now_usec()";
            $auditdtls["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
            $auditdtls["AuditTrailFunctionID"] = 22;
            $insertauditlog = $auditlog->Insert($auditdtls);
            if ($auditlog->HasError)
            {
                $errormsgtitle = "ERROR!";
                $errormsg = "Error has occured:" . $tmaccount->getError();
                $msgcode = 1;
            }

            /* Step 15:
             * insert to batch transaction log
             */
            $batchtrans["GameID"] = $gameid;
            $batchtrans["BatchID"] = $gamebatchnum;
            $batchtrans["FileName"] = $filename;
            $batchtrans["TransactionDate"] = "now_usec()";
            $batchtrans["TransactionType"] = 1;
            $batchtrans["ProcessedByAID"] = $_SESSION["acctid"];
            $batchtrans["Status"] = $status;
            $tmbatchtranslog->Insert($batchtrans);
            if ($tmbatchtranslog->HasError) {
                $errormsgtitle = "ERROR!";
                $errormsg = "Error has occured:" . $tmbatchtranslog->getError();
                $msgcode = 1;
            }
        }
        
        if ($status != 2 && $msgcode != 2) {
            $counttitle = "Inventory File Summary";
            $countmsg = "Provider : " . $providername . "<br/>" .
                    "Game Name : " . $productname . "<br/>" .
                    "Game Number : " . $gamenum . "<br/>" .
                    "Total No. of Cartons : " . number_format($boxesaffectedrows) . "<br/>" .
                    "Total No. of Books : " . number_format($totalbooks);
        } else {
            if ($msgcode == 2)
            {
                $errormsgtitle = "ERROR!";
                $errormsg = "Game and Book number already exist in the deck.";
            } else {
                $errormsgtitle = "ERROR!";
                $errormsg = "Uploading was unsuccessful . Please try again.";
            }
        }
    }
    
    if($msgcode == 0)
    {
        $msgtitle = $counttitle;
        $msg = $countmsg;
    }
    else
    {
        $msgtitle = $errormsgtitle;
        $msg = $errormsg;
    }

    return array($boxesaffectedrows, $booksaffectedrows, $msgcode, $msg, $msgtitle);
}

//function to convert inner array into string
function implode_inner_array($n)
{
    return implode(";", $n);
}
?>