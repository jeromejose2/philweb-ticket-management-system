<?php
ignore_user_abort(true);                                                        //Ignore user aborts and force the script running until the last part
set_time_limit(0);                                                              //Limits the maximum execution time , if set to 0 no maximum time is imposed. Safe mode should be turned off.
$stylesheets[] = "css/default.css";
$javascripts[] = "jscripts/validations.js";
$pagesubmenuid = 31;

App::LoadModuleClass("TicketManagementCM", "TMGameImport");
App::LoadModuleClass("TicketManagementCM", "TMProducts");
App::LoadModuleClass("TicketManagementCM", "TMProvider");
App::LoadModuleClass("TicketManagementCM", "TMGameManagement");
App::LoadModuleClass("TicketManagementCM", "TMAuditLog");
App::LoadModuleClass("TicketManagementCM", "TMDecks");
//App::LoadModuleClass("TicketManagementCM", "TMDecks");
App::LoadModuleClass("TicketManagementCM", "TMDeckInfo");
App::LoadModuleClass("TicketManagementCM", "TMBoxes");
App::LoadModuleClass("TicketManagementCM", "TMBooks");
App::LoadModuleClass("TicketManagementCM", "TMTickets");
App::LoadModuleClass("TicketManagementCM", "TMInventoryTemp");

App::LoadControl("ComboBox");
App::LoadControl("Button");

$tmgameimport = new TMGameImport();
$tmproducts = new TMProducts();
$tmproviders = new TMProvider();
$tmgames = new TMGameManagement();
$auditlog = new TMAuditLog();
$tmdecks2 = new TMDecks();
$tmdeckinfo = new TMDeckInfo();
$tmboxes = new TMBoxes();
$tmbooks = new TMBooks();
$tmtickets = new TMTickets();
$tminventorytemp = new TMInventoryTemp();


$frmInventoryXmlUpload = new FormsProcessor();

/*$where = " ORDER BY ProductName";
$products = $tmproducts->SelectByWhere($where);
$products_list = new ArrayList();
$products_list->AddArray($products);

$ddlProducts = new ComboBox("ddlProducts","ddlProducts","Products: ");
$ddlProducts->ShowCaption = true;
$ddlProducts->DataSource = $products_list;
$ddlProducts->DataSourceText = "ProductName";
$ddlProducts->DataSourceValue = "ProductID";
$ddlProducts->DataBind();*/

$where = " ORDER BY Name";
$providers = $tmproviders->SelectAllProvider();


$ddlProviders = new ComboBox("ddlProviders","ddlProviders","Provider: ");
$ddlProviders->ShowCaption = true;

if(count($providers) > 0)
{
    $providers_list = new ArrayList();
    $providers_list->AddArray($providers);
    $ddlProviders->DataSource = $providers_list;
    $ddlProviders->DataSourceText = "Name";
    $ddlProviders->DataSourceValue = "ProviderID";
    $ddlProviders->DataBind();
}
else
{   
    $options = null;
    $options[] = new ListItem("- - - - - - - - - - - - - -","",true);
    $ddlProviders->Items = $options;
}

$btnUpload = new Button("btnUpload","btnUpload","Upload");
$btnUpload->IsSubmit = true;
$btnUpload->Args = "onclick='javascript: return showloadingpage();'";

//$frmInventoryXmlUpload->AddControl($ddlProducts);
$frmInventoryXmlUpload->AddControl($ddlProviders);
$frmInventoryXmlUpload->AddControl($btnUpload);

$frmInventoryXmlUpload->ProcessForms();

if($frmInventoryXmlUpload->IsPostBack)
{
	if($_FILES['file']['type'] == "")
    {
        $errormsg = "Please specify file.";
        $errormsgtitle = "ERROR!";
    }
    else if (($_FILES['file']['type'] == "text/xml"))
    {
		if ($_FILES["file"]["error"] > 0)
        {
            $errormsg = "Return Code: " . $_FILES["file"]["error"] . "<br />";
            $errormsgtitle = "ERROR!";
        }
		else
		{
			$filename = $_FILES["file"]["name"];
            $fullpath = "../xml/inventory/" . $_FILES["file"]["name"];

            //reading of country code
            $myFile = "../countrycode.txt";
            $fileopen = fopen($myFile,'r');
            if(filesize($myFile) > 0)
            {
                $countrycode = trim(fread($fileopen,filesize($myFile)));
            }
            else
            {
                $countrycode = "";
            }
            fclose($fileopen);

			//reading proper filename format
            $filenamechecking = "../inventoryfile.txt";
            $filenameopen = fopen($filenamechecking,'r');
            if(filesize($filenamechecking) > 0)
            {
                $formatfromtxt = trim(fread($filenameopen,filesize($filenamechecking)));
                if(strlen($formatfromtxt) > 0)
                {
                    $filenameformat = explode(" ", $formatfromtxt);
                    $fntotal = count($filenameformat);
                    $fn1 = strlen($filenameformat[0]);
                    $fn2 = strlen($filenameformat[1]);
                    $fn3 = strlen($filenameformat[2]);
                    $fn4 = strlen($filenameformat[3]);
					$fnstring1 = $filenameformat[0]; //Philweb 
					$fnstring2 = $filenameformat[2]; //country name
                }
                else
                {
                    $fntotal = 0;
                    $fn1 = 0;
                    $fn2 = 0;
                    $fn3 = 0;
                    $fn4 = 0;
					$fnstring1 = "";
					$fnstring2 = "";
                }
            }
            else
            {
                $fntotal = 0;
                $fn1 = 0;
                $fn2 = 0;
                $fn3 = 0;
                $fn4 = 0;
				$fnstring1 = "";
				$fnstring2 = "";
            }
            fclose($filenameopen);

			if(strlen($countrycode) > 0)
            {
				// check if file exists
                $fileExixts = $tmgameimport->CheckFile($fullpath);

                //if (file_exists($fullpath))
                if (count($fileExixts) > 0) {
                    $errormsg = "File was already imported. Please enter a new batch of inventory file.";
                    //$errormsg = $filename . " already exists. ";
                    $errormsgtitle = "ERROR!";
                } else {
                    // checking if the corect file is being uploaded
                    $filechecking = explode("_", $filename);

                    // checking if file format is correct
                    $fileformatchecking = explode(" ", $filename);
                    if ((count($fileformatchecking) > $fntotal) || (count($fileformatchecking) < $fntotal)) {
                        $errormsg = "Invalid file name format. Please upload a valid file name format1.";
                        $errormsgtitle = "ERROR!";
                    } else {
                        if ((strlen($fileformatchecking[0]) > $fn1) || (strlen($fileformatchecking[0]) < $fn1) || ($fileformatchecking[0] != $fnstring1)) {
                            $errormsg = "Invalid file name format. Please upload a valid file name format2.$fn1";
                            $errormsgtitle = "ERROR!";
                        } else if ((strlen($fileformatchecking[1]) > $fn2) || (strlen($fileformatchecking[1]) < $fn2)) {
                            $errormsg = "Invalid file name format. Please upload a valid file name format3.";
                            $errormsgtitle = "ERROR!";
                        } else if ((strlen($fileformatchecking[2]) > $fn3) || (strlen($fileformatchecking[2]) < $fn3) || ($fileformatchecking[2] != $fnstring2)) {
                            $errormsg = "Invalid file name format. Please upload a valid file name format4.";
                            $errormsgtitle = "ERROR!";
                        } else if ((strlen($fileformatchecking[3]) > $fn4) || (strlen($fileformatchecking[3]) < $fn4)) {
                            $errormsg = "Invalid file name format. Please upload a valid file name format5.";
                            $errormsgtitle = "ERROR!";
                        } else {
                            //====================
                            if ($filechecking[1] != "WLIST") {
                                //read xml file
                                libxml_use_internal_errors(true);
                                move_uploaded_file($_FILES["file"]["tmp_name"], $fullpath);
                                $xml = simplexml_load_file($fullpath);

                                //checking if xml structure is correct
                                if (!$xml) {
                                    $errors = libxml_get_errors();

                                    foreach ($errors as $error) {
                                        $errormsgtitle = "ERROR!";
                                        $errormsg = display_xml_error($error);
                                    }

                                    libxml_clear_errors();
                                } else {
                                    $xmlUrl = $fullpath;
                                    $xmlStr = file_get_contents($xmlUrl);
                                    $xmlObj = simplexml_load_string($xmlStr);
                                    $arrXml = objectsIntoArray($xmlObj);
                                    $xmlname = $xml->getName();
                                    $keys_array = array_keys_multi($arrXml);

                                    //checking if node carton_NNNN exists
                                    $carton_node = preg_grep('/^carton_.*/', $keys_array);

                                    //checking if node pack_number exists
                                    $packnum_node = preg_grep('/^pack_number.*/', $keys_array);

                                    //checking if there are empty nodes
                                    $emptynodes = in_array_r(Array(), $arrXml);
                                    if (!preg_match("/" . $countrycode . "/", $xmlname)) {
                                    //if (!preg_match("/FT/", $xmlname)) {                                          /*---> for timor*/
                                    //if (!preg_match("/FC/", $xmlname)) {                                            /*---> for cambodia*/
                                        $errormsgtitle = "ERROR!";
                                        $errormsg = "Fatal Error: Missing game number node. <br/> File: $fullpath";
                                    } else if (count($emptynodes) > 0) {
                                        $errormsgtitle = "ERROR!";
                                        $errormsg = "Fatal Error: Empty node/s in " . implode(',', $emptynodes) . "<br/> File: $fullpath";
                                    } else if (count($carton_node) == 0) {
                                        $errormsgtitle = "ERROR!";
                                        $errormsg = "Fatal Error: Missing carton node. <br/> File: $fullpath";
                                    } else if (count($packnum_node) == 0) {
                                        $errormsgtitle = "ERROR!";
                                        $errormsg = "Fatal Error: Missing pack_number node. <br/> File: $fullpath";
                                    } else if (count($carton_node) != count($packnum_node)) {
                                        $errormsgtitle = "ERROR!";
                                        $errormsg = "Fatal Error: Carton count and pack_number do not match. <br/> File: $fullpath";
                                    } else {
                                        //checking if gamenumber exists
                                        $explode_uscore = explode("_", $filename);
                                        $explode_space = explode(" ", $explode_uscore[0]);

                                        /* $where = " WHERE GameNumber = " . substr($explode_space[3] , 2 , 3);
                                          $gameid = $tmgames->SelectByWhere($where);
                                          if(count($gameid) > 0)
                                          { */
                                        $totalboxcount = 0;
                                        $totalbookcount = 0;
                                        $totaldeckcount = 0;
                                        $totalticketcount = 0;
                                        $ticketcount = 200;
                                        $deletedecksaffectedrow = 0;
                                        $deletebooksaffectedrow = 0;
                                        $deleteboxesaffectedrow = 0;
                                        $deleteticketaffectedrow = 0;
                                        $boxesaffectedrows = 0;
                                        $booksaffectedrows = 0;
                                        $decksaffectedrows = 0;

                                        //insert uploaded xml in the table
                                        //$gameno = explode(" " , substr($filename, 0,strpos($filename,'_')));
                                        //$gameid = $tmgames->SelectGameDtlsByWhere(substr($gameno[3] , 2 , 3));

                                        /* Parse Data */
                                        $gamecode = $filechecking[0];
                                        $providerid = $ddlProviders->SelectedValue;
                                        //$productid = $ddlProducts->SelectedValue;
                                        $gameinfo = $tmgames->SelectGameDtlsByWhere(substr($xmlname, 2, 3),$providerid);
                                        if(count($gameinfo) > 1)
                                        {
                                            $errormsgtitle = "ERROR!";
                                            $errormsg = "Fatal Error: Game number has multiple instances. <br/> File: $fullpath";
                                        }
                                        else if (count($gameinfo) == 1)
                                        {
                                            //check if game id is already uploaded
											$gameid = $gameinfo[0]["GameID"];
                                            $isUploaded = $tmgameimport->CheckIfGameIdIsUploaded($gameid, "inventory");
                                            $productgameinfo = $tmproducts->GetProductNameGameNum($gameid);
                                            $providername = $tmproviders->SelectByID($providerid);
                                            $productid = $productgameinfo[0]["ProductID"];
                                            if (count($isUploaded) == 0) {
                                            //==================
                                            if ($productid != "") {
                                                //checking if the provider assigned to game number is the same as he chosen provider
                                                if ($productgameinfo[0]["ProviderID"] != $providerid) {
                                                    $errormsgtitle = "ERROR!";
                                                    $errormsg = "Provider assigned to game number do not match chosen provider.";
                                                } else {
                                                    /* Step 1:
                                                     * insert uploaded xml in the table
                                                     */
                                                    //$tmimport["GameID"] = $gameid[0]["GameID"];
                                                    $tmimport["ProviderID"] = $ddlProviders->SelectedValue;
                                                    $tmimport["FileName"] = $filename;
                                                    $tmimport["FilePath"] = $fullpath;
                                                    $tmimport["DateUploaded"] = 'now_usec()';
                                                    $tmimport["ProductID"] = $productid;
                                                    $tmimport["Status"] = '0';
                                                    $tmgameimport->Insert($tmimport);
                                                    $gameimportid = $tmgameimport->LastInsertID;
                                                    if ($tmgameimport->HasError) {
                                                        $errormsg = $tmgameimport->getError();
                                                        $errormsgtitle = "ERROR!";
                                                    }
                                                    /* Step 2:
                                                     * audit trail log
                                                     */
                                                    $auditdtls["SessionID"] = $_SESSION['sid'];
                                                    $auditdtls["AID"] = $_SESSION['acctid'];
                                                    $auditdtls["TransDetails"] = "GameImport ID: " . $tmgameimport->LastInsertID;
                                                    $auditdtls["TransDateTime"] = "now_usec()";
                                                    $auditdtls["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
                                                    $auditdtls["AuditTrailFunctionID"] = 17;
                                                    $insertauditlog = $auditlog->Insert($auditdtls);
                                                    if ($auditlog->HasError) {
                                                        $confadduser_title = "ERROR!";
                                                        $confadduser_msg = "Error has occured:" . $tmaccount->getError();
                                                    }

                                                    $errormsg = $filename . " has been successfully uploaded";
                                                    $errormsgtitle = "UPLOAD SUCCESSFUL";


                                                        $gameno = substr($xmlname, 2, 3);
                                                        $gamecode = $countrycode . $gameno;
                                                        //$gamecode = "FT" . $gameno;                                         /*---> for timor*/
                                                        //$gamecode = "FC" . $gameno;                                       /*---> for cambodia*/
                                                        $id = $gameimportid;

                                                        /* Step 3:
                                                         * Insert to deck info
                                                         */
                                                        $deckinfo_success = 0;
                                                        //do {
                                                            $deckinfo["ProductID"] = $productid;
                                                            $deckinfo["GameID"] = $gameid;
                                                            $deckinfo["ProviderID"] = $providerid;
                                                            $deckinfo["CreatedByAID"] = $_SESSION["acctid"];
                                                            $insertdeck = $tmdeckinfo->Insert($deckinfo);

                                                            if ($tmdeckinfo->HasError) {

                                                            } else {
                                                                $deckinfo_success++;
                                                                $lastinsertdeckid = $tmdeckinfo->LastInsertID;
                                                            }
                                                        //} while ($deckinfo_success != 1);

                                                        if ($deckinfo_success == 1) {
                                                            $cartoncount = 0;
                                                            $bookcount = 0;
                                                            $imax1 = 0;
                                                            $inventory_array = null;
                                                            //do {
                                                            foreach($arrXml as $key => $value)
                                                            {
                                                                        $imax1 = 0;
                                                                $carton = $key;
                                                                $inventory = $value;
                                                                do{
                                                                    $tempinventory_array = null;
                                                                    for($i = 0 ; $i < count($inventory["pack_number"]) ; $i++)
                                                                    {
                                                                        $inventoryinfo["GameID"] = $gameid;
                                                                        $inventoryinfo["ProviderID"] = $providerid;
                                                                        $inventoryinfo["ProductID"] = $productid;
                                                                        $inventoryinfo["DeckID"] = $lastinsertdeckid;
                                                                        $inventoryinfo["PackNumber"] = trim($inventory["pack_number"][$i]);
                                                                        $inventoryinfo["Carton"] = trim($carton);
                                                                        $inventoryinfo["AccountID"] = $_SESSION["acctid"];
                                                                        $inventoryinfo["GameImportID"] = $gameimportid;
                                                                        $tempinventory_array[] = $inventoryinfo;
                                                                    }
                                                                                $imax1++;

                                                                                if ($imax1 > 2)
                                                                                {
                                                                                        $status = 2;
                                                                                        break;
                                                                                }
                                                                                else
                                                                                {
                                                                                        $status = 1;
                                                                                }
                                                                }while(count($tempinventory_array) != count($inventory["pack_number"]));
                                                                foreach($tempinventory_array as $value)
                                                                {
                                                                    $inventory_array[] = $value;
                                                                }

                                                            }
                                                                /*$imax1++;

                                                                if ($imax1 > 2) {
                                                                    $status = 2;
                                                                    break;
                                                                } else {
                                                                    $status = 1;
                                                                }*/
                                                            //} while (count($inventory_array) != count($inventory["pack_number"]));

                                                            $imax = 0;
                                                            $tmax = 0;

                                                            if ($status != 2) {
                                                                do {
                                                                    /* Step 4:
                                                                     * delete from inventorytemp where AccountAID == $_SESSION["acctid"]
                                                                     * Insert into temp table inventorytemp
                                                                     */
                                                                    $tminventorytemp->DeleteRecords($_SESSION['acctid']);
                                                                    $tminventorytemp->InsertMultiple($inventory_array);

                                                                    /* Step 5:
                                                                     * Insert boxes
                                                                     */
                                                                    $tmboxes->InsertBoxesFromInventoryTemp($gameid, $providerid, $productid, $_SESSION['acctid'], $lastinsertdeckid, $gameimportid);
                                                                    $boxesaffectedrows = $tmboxes->AffectedRows;
                                                                    /* Step 6:
                                                                     * Insert books
                                                                     */
                                                                    $tmbooks->InsertBooksFromInventoryTemp($gameid, $providerid, $productid, $_SESSION['acctid'], $lastinsertdeckid, $gameimportid);
                                                                    $booksaffectedrows = $tmbooks->AffectedRows;

                                                                    /* Step 7:
                                                                     * Insert to decks
                                                                     */
                                                                    $tmdecks2->InsertDecksFromInventoryTemp($gameid, $providerid, $productid, $lastinsertdeckid, $_SESSION['acctid'], $gameimportid);
                                                                    $decksaffectedrows = $tmdecks2->AffectedRows;

                                                                    /* Step 8:
                                                                     * If affected rows do not match the total no of rows that must be inserted, delete all inserted records
                                                                     */
                                                                    if ($decksaffectedrows != count($inventory_array)) {
                                                                        $lastdeckid = $tmdecks2->GetLastDeckID();
                                                                        $lastboxid = $tmboxes->GetLastBoxID();
                                                                        $lastbookid = $tmbooks->GetLastBookID();

                                                                        //delete all inserted decks
                                                                        if ($lastdeckid[0]["LastID"] != NULL) {
                                                                            //do {
                                                                                $start = ($lastdeckid[0]["Lastid"] - $decksaffectedrows);
                                                                                $deletealltransactions = $tmdecks2->DeleteAllTransactions($start, $lastdeckid[0]["LastID"]);
                                                                                //$deletedecksaffectedrow = ($deletedecksaffectedrow + $tmwinnings2->AffectedRows);
                                                                                $deletedecksaffectedrow = ($deletedecksaffectedrow + $tmdecks2->AffectedRows);
                                                                            //} while ($deletedecksaffectedrow != $decksaffectedrows);
                                                                        }

																		//delete all inserted boxes
                                                                        if ($lastboxid[0]["LastID"] != NULL) {
                                                                            //do {
                                                                                $start = ($lastboxid[0]["LastID"] - $boxesaffectedrows);
                                                                                $deletealltransactions = $tmboxes->DeleteAllTransactions($start, $lastboxid[0]["LastID"]);
                                                                                $deleteboxesaffectedrow = ($deleteboxesaffectedrow + $tmboxes->AffectedRows);
                                                                            //} while ($deleteboxesaffectedrow != $boxesaffectedrows);
                                                                        }

                                                                        //delete all inserted books
                                                                        if ($lastbookid[0]["LastID"] != NULL) {
                                                                            //do {
                                                                                $start = ($lastbookid[0]["LastID"] - $booksaffectedrows);
                                                                                $deletealltransactions = $tmbooks->DeleteAllTransactions($start, $lastbookid[0]["LastID"]);
                                                                                $deletebooksaffectedrow = ($deletebooksaffectedrow + $tmbooks->AffectedRows);
                                                                            //} while ($deletebooksaffectedrow != $booksaffectedrows);
                                                                        }

                                                                        

                                                                        $tmdeckinfo->DeleteByDeckInfoID($lastinsertdeckid);

                                                                        $tmgameimport->DeleteByGameImportID($gameimportid);

                                                                        $decksaffectedrows = 0;
                                                                        $booksaffectedrows = 0;
                                                                        $boxesaffectedrows = 0;
                                                                    }

                                                                    $imax++;

                                                                    if ($imax > 2) {
                                                                        $status = 2;
                                                                        exit;
                                                                    } else {
                                                                        $status = 1;
                                                                    }
                                                                } while ($decksaffectedrows != count($inventory_array));
                                                            }

                                                            if ($status != 2) {
                                                                /* Step 9:
                                                                 * Insert tickets per book
                                                                 */
                                                                $bookid = $tmbooks->SelectAllInsertedBookID($gameid, $providerid, $productid, $_SESSION['acctid'], $lastinsertdeckid, $gameimportid);
                                                                if (count($bookid) > 0) {
                                                                    do {
                                                                        for ($b = 0; $b < count($bookid); $b++) {

                                                                            for ($a = 1; $a < ($ticketcount + 1); $a++) {
                                                                                $ticket["BookID"] = $bookid[$b]["BookID"];
                                                                                $ticket["TicketNumber"] = str_pad($a, 3, 0, STR_PAD_LEFT);
                                                                                $arrTickets[] = $ticket;
                                                                            }
                                                                        }
                                                                        $tmax++;
                                                                        if ($tmax > 2) {
                                                                            $arrTickets = null;
                                                                            $status = 2;
                                                                            exit;
                                                                        } else {
                                                                            $status = 1;
                                                                        }
                                                                    } while (count($arrTickets) != (count($bookid) * $ticketcount));

                                                                    if (count($arrTickets) > 0) {
                                                                        $insertticket = $tmtickets->InsertMultiple($arrTickets);
                                                                        $insertticketaffectedrows = $tmtickets->AffectedRows;
                                                                        $maxid = $tmtickets->GetLastTicketID();
                                                                        if ($insertticketaffectedrows != (count($bookid) * $ticketcount)) {
                                                                            if ($maxid[0]["lastid"] != NULL) {
                                                                                //do {
                                                                                    $start = ($maxid[0]["lastid"] - $insertticketaffectedrows);
                                                                                    $deletealltransactions = $tmtickets->DeleteAllTransactions($start, $maxid[0]["lastid"]);
                                                                                    $deleteticketaffectedrow = ($deleteticketaffectedrow + $tmtickets->AffectedRows);
                                                                                //} while ($deleteticketaffectedrow != $insertticketaffectedrows);
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }

                                                            /* Step 10:
                                                             * Delete all records to inventorytemp
                                                             */
                                                            $tminventorytemp->DeleteRecords($_SESSION['acctid']);

                                                            /* Step 11:
                                                             * Update xml status
                                                             */
                                                            $xmlstatus["GameID"] = $gameid;
                                                            $xmlstatus["Status"] = $status;
                                                            $xmlstatus["GameImportID"] = $gameimportid;
                                                            $xmlstatus["DateImported"] = "now_usec()";
                                                            $updatestatus = $tmgameimport->UpdateByArray($xmlstatus);
                                                            if ($tmgameimport->HasError) {
                                                                $errormsgtitle = "ERROR!";
                                                                $errormsg = "Error:" . $tmgameimport->getError();
                                                            }

                                                            /* Step 12:
                                                             * Update deck info
                                                             */
                                                            $deckinfoupdate["BoxCount"] = $boxesaffectedrows;
                                                            $deckinfoupdate["BookCount"] = $booksaffectedrows;
                                                            $deckinfoupdate["DeckID"] = $lastinsertdeckid;
                                                            $updatedeckinfo = $tmdeckinfo->UpdateByArray($deckinfoupdate);
                                                            if ($tmdeckinfo->HasError) {
                                                                $errormsgtitle = "ERROR!";
                                                                $errormsg = "Error:" . $tmdeckinfo->getError();
                                                            }

                                                            /* Step 13:
                                                             * insert to audit trail
                                                             */
                                                            $auditdtls["SessionID"] = session_id();
                                                            $auditdtls["AID"] = $_SESSION["acctid"];
                                                            $auditdtls["TransDetails"] = "GameImport ID: " . $id;
                                                            $auditdtls["TransDateTime"] = "now_usec()";
                                                            $auditdtls["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
                                                            $auditdtls["AuditTrailFunctionID"] = 22;
                                                            $insertauditlog = $auditlog->Insert($auditdtls);
                                                            if ($auditlog->HasError) {
                                                                $errormsgtitle = "ERROR!";
                                                                $errormsg = "Error has occured:" . $tmaccount->getError();
                                                            }

                                                            if ($status != 2) {
                                                                $countmsgtitle = "Inventory File Summary";
                                                                $countmsg = "Provider Name: " . $providername[0]["Name"] . "<br/>" .
                                                                        "Game Name: " . $productgameinfo[0]["ProductName"] . "<br/>" .
                                                                        "Game Number: " . $productgameinfo[0]["GameNumber"] . "<br/>" .
                                                                        "Total No. of Cartons: " . number_format($boxesaffectedrows) . "<br/>" .
                                                                        "Total No. of Books: " . number_format($booksaffectedrows);
                                                            } else {
                                                                $countmsgtitle = "ERROR!";
                                                                $countmsg = "Uploading was unsuccessful . Please try again.";
                                                            }
                                                        }

                                                    //here
                                                    }
                                                } else {
                                                    $errormsgtitle = "ERROR!";
                                                    $errormsg = "Product id do not exists.";
                                                }
                                                //===================
                                            } else {
                                                $errormsgtitle = "ERROR!";
                                                $errormsg = "You cannot proceed because books were already uploaded for this game number.";
                                            }
                                        } else {
                                            $errormsgtitle = "ERROR!";
                                            $errormsg = "Game number in the file do not exists.";
                                        }



                                        /* }
                                          else
                                          {
                                          $errormsg = "Game number do not exists. Please ensure that the filename is correct.";
                                          $errormsgtitle = "ERROR!";
                                          } */
                                        //file structure
                                    }
                                }
                            } else {
                                $errormsg = "Incorrect file name format. Please upload a valid file name format.";
                                //$errormsg = "I suppose you are uploading a wrong file.";
                                $errormsgtitle = "ERROR!";
                            }
                            //===========================
                        }
                    }
                }
			}
			else
			{
				$errormsg = "You cannot proceed because country code text file is empty.";
                $errormsgtitle = "ERROR!";
			}
		}
	}
    else
    {
		$errormsg = "Incorrect file format. Please upload a valid file format.";
        //$errormsg = "Invalid file";
        $errormsgtitle = "ERROR!";
    }
}

//function for checking if xml array have empty nodes
function in_array_r($needle, $haystack) {
    $emptynodeskey = array();
    foreach ($haystack as $key => $item) {
        if ($item === $needle || (is_array($item) && in_array_r($needle, $item))) {
            //return true;
            $emptynodeskey[] = $key;

        }
    }
    //return false;
    return $emptynodeskey;
}

//function for converting array keys to 1 dimensional array
function array_keys_multi(array $array)
{
    $keys = array();

    foreach ($array as $key => $value) {
        $keys[] = $key;

        if (is_array($array[$key])) {
            $keys = array_merge($keys, array_keys_multi($array[$key]));
        }
    }

    return $keys;
}

//function for checking xml structure
function display_xml_error($error)
{
    $return = "";
    //$return .= $error->column . "\n";

    switch ($error->level) {
        case LIBXML_ERR_WARNING:
            $return .= "Warning $error->code: ";
            break;
         case LIBXML_ERR_ERROR:
            $return .= "Error $error->code: ";
            break;
        case LIBXML_ERR_FATAL:
            $return .= "Fatal Error $error->code: ";
            break;
    }

    $return .= trim($error->message) .
               "<br>  Line: $error->line" .
               "<br/>  Column: $error->column";

    if ($error->file) {
        $return .= "<br/>  File: " . $error->file . "";
    }

    return addslashes("$return");
}

//function in converting xml to array
function objectsIntoArray($arrObjData, $arrSkipIndices = array())
{
    $arrData = array();

    // if input is object, convert into array
    if (is_object($arrObjData)) {
        $arrObjData = get_object_vars($arrObjData);
    }

    if (is_array($arrObjData)) {
        foreach ($arrObjData as $index => $value) {
            if (is_object($value) || is_array($value)) {
                $value = objectsIntoArray($value, $arrSkipIndices); // recursive call
            }
            if (in_array($index, $arrSkipIndices)) {
                continue;
            }
            $arrData[$index] = $value;
        }
    }
    return $arrData;
}
?>