<?php
/*
 * @author
 * Purpose   : controller for inventoryxmlupload_proposal
 */
ignore_user_abort(true);                                                        //Ignore user aborts and force the script running until the last part
set_time_limit(0);                                                              //Limits the maximum execution time , if set to 0 no maximum time is imposed. Safe mode should be turned off.
$stylesheets[] = "css/default.css";
$javascripts[] = "jscripts/validations.js";
$pagesubmenuid = 31;

App::LoadModuleClass("TicketManagementCM", "TMGameImport");
App::LoadModuleClass("TicketManagementCM", "TMProducts");
App::LoadModuleClass("TicketManagementCM", "TMProvider");
App::LoadModuleClass("TicketManagementCM", "TMGameManagement");
App::LoadModuleClass("TicketManagementCM", "TMAuditLog");
App::LoadModuleClass("TicketManagementCM", "TMDecks");
App::LoadModuleClass("TicketManagementCM", "TMDeckInfo");
App::LoadModuleClass("TicketManagementCM", "TMBoxes");
App::LoadModuleClass("TicketManagementCM", "TMBooks");
App::LoadModuleClass("TicketManagementCM", "TMTickets");
App::LoadModuleClass("TicketManagementCM", "TMInventoryTemp");
App::LoadModuleClass("TicketManagementCM", "TMGameBatches");
App::LoadModuleClass("TicketManagementCM", "TMBatchTransactionLog");

App::LoadControl("ComboBox");
App::LoadControl("Button");
App::LoadControl("Hidden");
App::LoadControl("TextBox");

list($tmgameimport,$tmproducts,$tmproviders,$tmgames,$auditlog,$tmdecks2,$tmdeckinfo,$tmboxes,$tmbooks,$tmtickets,$tminventorytemp,$tmgamebatches,$tmbatchtranslog) = DeclareVariables();

$frmInventoryXmlUpload = new FormsProcessor();

$where = " ORDER BY Name";
$providers = $tmproviders->SelectAllProvider();


$ddlProviders = new ComboBox("ddlProviders","ddlProviders","Provider: ");
$ddlProviders->ShowCaption = true;

if(count($providers) > 0)
{
    $providers_list = new ArrayList();
    $providers_list->AddArray($providers);
    $ddlProviders->DataSource = $providers_list;
    $ddlProviders->DataSourceText = "Name";
    $ddlProviders->DataSourceValue = "ProviderID";
    $ddlProviders->DataBind();
	$ddlProviders->SetSelectedValue(1);
}
else
{   
    $options = null;
    $options[] = new ListItem("- - - - - - - - - - - - - -","",true);
    $ddlProviders->Items = $options;
}

$btnUpload = new Button("btnUpload","btnUpload","Upload");
$btnUpload->IsSubmit = true;
$btnUpload->Args = "onclick='javascript: return showloadingpage();'";

$btnConfirmUpload = new Button("btnConfirmUpload","btnConfirmUpload","Yes");
$btnConfirmUpload->IsSubmit = true;
$btnConfirmUpload->Args = "onclick='javascript: return showloadingpage2();'";

$txtPONumber = new TextBox("txtPONumber","txtPONumber","PO Number: ");
$txtPONumber->ShowCaption = true;
$txtPONumber->Length = 30;
$txtPONumber->Args = "size='30'";

$txtDeliveryNumber = new TextBox("txtDeliveryNumber","txtDeliveryNumber","Delivery Number: ");
$txtDeliveryNumber->ShowCaption = true;
$txtDeliveryNumber->Length = 30;
$txtDeliveryNumber->Args = "size='30'";

$btnOkay = new Button("btnOkay","btnOkay","Okay");
$btnOkay->Args = "onclick = 'javascript: return redirectToInventoryXMLUpload()'";

$hiddenfilename = new Hidden("hiddenfilename","hiddenfilename","Hidden file name");
$hiddengameid = new Hidden("hiddengameid","hiddengameid","Hidden gameid");
$hiddenproductid = new Hidden("hiddenproductid","hiddenproductid","Hidden productid");
$hiddenproviderid = new Hidden("hiddenproviderid","hiddenproviderid","Hidden providerid");
$hiddenprovidername = new Hidden("hiddenprovidername","hiddenprovidername","Hidden provider name");
$hiddenprodname = new Hidden("hiddenprodname","hiddenprodname","Hidden prod name");
$hiddengamenum = new Hidden("hiddengamenum","hiddengamenum","Hidden game num");
$hiddenbatchid = new Hidden("hiddenbatchid","hiddenbatchid","Hidden batch id");
$hiddenPOnum = new Hidden("hiddenPOnum","hiddenPOnum","Hidden PO number");
$hiddenDelnum = new Hidden("hiddenDelnum","hiddenDelnum","Hidden Delivery number");

$frmInventoryXmlUpload->AddControl($ddlProviders);
$frmInventoryXmlUpload->AddControl($btnUpload);
$frmInventoryXmlUpload->AddControl($btnConfirmUpload);
$frmInventoryXmlUpload->AddControl($hiddenfilename);
$frmInventoryXmlUpload->AddControl($hiddengameid);
$frmInventoryXmlUpload->AddControl($hiddenproductid);
$frmInventoryXmlUpload->AddControl($hiddenproviderid);
$frmInventoryXmlUpload->AddControl($hiddenprovidername);
$frmInventoryXmlUpload->AddControl($hiddenprodname);
$frmInventoryXmlUpload->AddControl($hiddengamenum);
$frmInventoryXmlUpload->AddControl($hiddenbatchid);
$frmInventoryXmlUpload->AddControl($txtPONumber);
$frmInventoryXmlUpload->AddControl($txtDeliveryNumber);
$frmInventoryXmlUpload->AddControl($hiddenPOnum);
$frmInventoryXmlUpload->AddControl($hiddenDelnum);
$frmInventoryXmlUpload->AddControl($btnOkay);

$frmInventoryXmlUpload->ProcessForms();

if($frmInventoryXmlUpload->IsPostBack)
{
    if($btnUpload->SubmittedValue == "Upload")
    {
        if($_FILES['file']['type'] == "")
        {
            $errormsg = "Please specify file.";
            $errormsgtitle = "ERROR!";
        }
        else if (($_FILES['file']['type'] == "text/xml"))
        {
            if ($_FILES["file"]["error"] > 0)
            {
                $errormsg = "Return Code: " . $_FILES["file"]["error"] . "<br />";
                $errormsgtitle = "ERROR!";
            }
            else
            {
                $filename = $_FILES["file"]["name"];
                //$filename = "Philweb - Timor 0999_1.xml";
                $fullpath = "../xml/inventory/" . $_FILES["file"]["name"];
                //$fullpath = "../xml/inventory/Philweb - Timor 0999_1.xml";

                // check if file exists
                $fileExixts = $tmgameimport->CheckFile($fullpath);
                
                //if (file_exists($fullpath))
                /* Removed by ARS 12-29-2011
                 * Purpose: To allow the uploading of xml files with the same game number
                 */
                /*if(count($fileExixts) > 0)
                {
                    $errormsg = "File was already imported. Please enter a new batch of inventory file.";
                    $errormsgtitle = "ERROR!";
                }
                else
                {*/
                // checking if the corect file is being uploaded
                $filechecking = explode("_", $filename);

                // checking if file format is correct
                $fileformatchecking = explode(" ", $filename);
                if((count($fileformatchecking) > 4) || (count($fileformatchecking) < 4))
                {
                    $errormsg = "Invalid file name format. Please upload a valid file name format.";
                    $errormsgtitle = "ERROR!";
                }
                else
                {
                    if((strlen($fileformatchecking[0]) > 7) || (strlen($fileformatchecking[0]) < 7))
                    {
                        $errormsg = "Invalid file name format. Please upload a valid file name format.";
                        $errormsgtitle = "ERROR!";
                    }
                    else if((strlen($fileformatchecking[1]) > 1) || (strlen($fileformatchecking[1]) < 1))
                    {
                        $errormsg = "Invalid file name format. Please upload a valid file name format.";
                        $errormsgtitle = "ERROR!";
                    }
                    else if((strlen($fileformatchecking[2]) > 5) || (strlen($fileformatchecking[2]) < 5))
                    {
                        $errormsg = "Invalid file name format. Please upload a valid file name format.";
                        $errormsgtitle = "ERROR!";
                    }
                    /* Removed by ARS 12-28-2011 Purpose: To allow the uploading of xml files with more than 3 game number in the filename
                    else if((strlen($fileformatchecking[3]) > 10) || (strlen($fileformatchecking[3]) < 10))
                    {
                        $errormsg = "Invalid file name format. Please upload a valid file name format.";
                        $errormsgtitle = "ERROR!";
                    }*/
                    else
                    {
                        if($filechecking[1] != "WLIST")
                        {
                            //read xml file
                            libxml_use_internal_errors(true);
                            move_uploaded_file($_FILES["file"]["tmp_name"], $fullpath);
                            $xml = simplexml_load_file($fullpath);

                            //checking if xml structure is correct
                            if (!$xml)
                            {
                                $errors = libxml_get_errors();

                                foreach ($errors as $error) {
                                    $errormsgtitle = "ERROR!";
                                    $errormsg = display_xml_error($error);
                                }

                                libxml_clear_errors();
                            }
                            else
                            {
                                $xmlUrl = $fullpath;
                                $xmlStr = file_get_contents($xmlUrl);
                                $xmlObj = simplexml_load_string($xmlStr);
                                $arrXml = objectsIntoArray($xmlObj);
                                $xmlname = $xml->getName();
                                $keys_array = array_keys_multi($arrXml);

                                //checking if node carton_NNNN exists
                                $carton_node = preg_grep('/^carton_.*/', $keys_array);

                                //checking if node pack_number exists
                                $packnum_node = preg_grep('/^pack_number.*/', $keys_array);

                                //checking if there are empty nodes
                                $emptynodes = in_array_r(Array(), $arrXml);
                                if(!preg_match("/FT/", $xmlname))
                                {
                                    $errormsgtitle = "ERROR!";
                                    $errormsg = "Fatal Error: Missing game number node. <br/> File: $fullpath";
                                }
                                else if(count($emptynodes) > 0)
                                {
                                    $errormsgtitle = "ERROR!";
                                    $errormsg = "Fatal Error: Empty node/s in " . implode(',',$emptynodes) . "<br/> File: $fullpath";
                                }
                                else if(count($carton_node) == 0)
                                {
                                    $errormsgtitle = "ERROR!";
                                    $errormsg = "Fatal Error: Missing carton node. <br/> File: $fullpath";
                                }
                                else if(count($packnum_node) == 0)
                                {
                                    $errormsgtitle = "ERROR!";
                                    $errormsg = "Fatal Error: Missing pack_number node. <br/> File: $fullpath";
                                }
                                else if(count($carton_node) != count($packnum_node))
                                {
                                    $errormsgtitle = "ERROR!";
                                    $errormsg = "Fatal Error: Carton count and pack_number do not match. <br/> File: $fullpath";
                                }
                                else
                                {
                                    //checking if gamenumber exists
                                    $explode_uscore = explode("_" , $filename);
                                    $explode_space = explode(" " , $explode_uscore[0]);

                                    /*$where = " WHERE GameNumber = " . substr($explode_space[3] , 2 , 3);
                                    $gameid = $tmgames->SelectByWhere($where);
                                    if(count($gameid) > 0)
                                    {*/

                                    /*Parse Data*/
                                    $gamecode = $filechecking[0];
                                    $providerid = $ddlProviders->SelectedValue;
                                    //$productid = $ddlProducts->SelectedValue;
                                    //$gameinfo = $tmgames->SelectGameDtlsByWhere(substr($xmlname , 2 , 3),$providerid);
                                    $gameinfo = $tmgames->SelectGameDtlsByWhere(substr($xmlname, 2, strlen($xmlname)), $providerid);
                                    
                                    if(count($gameinfo) > 1)
                                    {
                                        $errormsgtitle = "ERROR!";
                                        $errormsg = "Fatal Error: Game number has multiple instances. <br/> File: $fullpath";
                                    }
                                    else if (count($gameinfo) == 1)
                                    {
                                        //check if game id is already uploaded
                                        $gameid = $gameinfo[0]["GameID"];
                                        $isUploaded = $tmgameimport->CheckIfGameIdIsUploaded($gameid, "inventory");
                                        $productgameinfo = $tmproducts->GetProductNameGameNum($gameid);
                                        $providername = $tmproviders->SelectByID($providerid);
                                        $productid = $productgameinfo[0]["ProductID"];
                                        $POnum = $txtPONumber->SubmittedValue;
                                        $delnum = $txtDeliveryNumber->SubmittedValue;
                                        /* Removed by ARS 12-29-2011
                                         * Purpose: To allow the uploading of xml files with the same game number
                                         */
                                        /*if (count($isUploaded) == 0) {*/

                                        if ($productid != "")
                                        {
                                            //checking if the provider assigned to game number is the same as he chosen provider
                                            if ($productgameinfo[0]["ProviderID"] != $providerid)
                                            {
                                                $errormsgtitle = "ERROR!";
                                                $errormsg = "Provider assigned to game number do not match chosen provider.";
                                            }
                                            else
                                            {
                                                //generate batch id
                                                $batch = $tmgamebatches->SelectBatchID($gameid,0);
                                                if(count($batch) > 0)
                                                {
                                                    $gamebatchnum = 1;
                                                }
                                                else
                                                {
                                                    $batches = $tmgamebatches->SelectMaxBatchID($gameid,1);
                                                    $gamebatchnum = ($batches[0]["maxbatchid"] + 1);
                                                }
                                                $hiddenfilename->Text = $filename;
                                                $hiddengameid->Text = $gameid;
                                                $hiddenproductid->Text = $productid;
                                                $hiddenproviderid->Text = $providerid;
                                                $hiddenprodname->Text = $productgameinfo[0]["ProductName"];
                                                $hiddenprovidername->Text = $providername[0]["Name"];
                                                $hiddengamenum->Text = $productgameinfo[0]["GameNumber"];
                                                $hiddenbatchid->Text = $gamebatchnum;
                                                $hiddenPOnum->Text = $POnum;
                                                $hiddenDelnum->Text = $delnum;

                                                if (count($isUploaded) > 0)
                                                {
                                                    $confirmuploadmsg = "Are you sure you want to upload another file for this game number?";
                                                    $confirmuploadtitle = "CONFIRMATION";
                                                }
                                                else
                                                {
                                                    list($msgtitle,$msg) = UploadInventoryXmlFile($filename,$gameid,$productid,$providerid,$arrXml,$xmlname,$providername[0]["Name"],$productgameinfo[0]["ProductName"],$productgameinfo[0]["GameNumber"],$gamebatchnum,$POnum,$delnum);
                                                    $countmsgtitle = $msgtitle;
                                                    $countmsg = $msg;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            $errormsgtitle = "ERROR!";
                                            $errormsg = "Product id do not exists.";
                                        }
                                        /*} else {
                                            $errormsgtitle = "ERROR!";
                                            $errormsg = "You cannot proceed because books were already uploaded for this game number.";
                                        }*/
                                    }
                                    else
                                    {
                                        $errormsgtitle = "ERROR!";
                                        $errormsg = "Game number do not exists. Please ensure that the filename is correct.";
                                    }
                                    /*}
                                    else
                                    {
                                        $errormsg = "Game number do not exists. Please ensure that the filename is correct.";
                                        $errormsgtitle = "ERROR!";
                                    }*/
                                    //file structure
                                 }
                            }
                        }
                        else
                        {
                            $errormsg = "Incorrect file name format. Please upload a valid file name format.";
                            $errormsgtitle = "ERROR!";
                        }
                    }
                }
                //}
            }
        }
        else
        {
                $errormsg = "Incorrect file format. Please upload a valid file format.";
            $errormsgtitle = "ERROR!";
        }
    }

    if($btnConfirmUpload->SubmittedValue == "Yes")
    {
        $fullpath = "../xml/inventory/" . $hiddenfilename->SubmittedValue;
        $xml = simplexml_load_file($fullpath);
        $xmlUrl = $fullpath;
        $xmlStr = file_get_contents($xmlUrl);
        $xmlObj = simplexml_load_string($xmlStr);
        $arrXml = objectsIntoArray($xmlObj);
        $xmlname = $xml->getName();
        $keys_array = array_keys_multi($arrXml);
        
        list($msgtitle,$msg) = UploadInventoryXmlFile($hiddenfilename->SubmittedValue,$hiddengameid->SubmittedValue,$hiddenproductid->SubmittedValue,$hiddenproviderid->SubmittedValue,$arrXml,$xmlname,$hiddenprovidername->SubmittedValue,$hiddenprodname->SubmittedValue,$hiddengamenum->SubmittedValue,$hiddenbatchid->SubmittedValue,$hiddenPOnum->SubmittedValue,$hiddenDelnum->SubmittedValue);
        $countmsgtitle = $msgtitle;
        $countmsg = $msg;
    }
}

function DeclareVariables()
{
    $tmgameimport = new TMGameImport();
    $tmproducts = new TMProducts();
    $tmproviders = new TMProvider();
    $tmgames = new TMGameManagement();
    $auditlog = new TMAuditLog();
    $tmdecks2 = new TMDecks();
    $tmdeckinfo = new TMDeckInfo();
    $tmboxes = new TMBoxes();
    $tmbooks = new TMBooks();
    $tmtickets = new TMTickets();
    $tminventorytemp = new TMInventoryTemp();
    $tmgamebatches = new TMGameBatches();
    $tmbatchtranslog = new TMBatchTransactionLog();

    return array($tmgameimport,$tmproducts,$tmproviders,$tmgames,$auditlog,$tmdecks2,$tmdeckinfo,$tmboxes,$tmbooks,$tmtickets,$tminventorytemp,$tmgamebatches,$tmbatchtranslog);
}
//function for checking if xml array have empty nodes
function in_array_r($needle, $haystack) {
    $emptynodeskey = array();
    foreach ($haystack as $key => $item) {
        if ($item === $needle || (is_array($item) && in_array_r($needle, $item))) {
            //return true;
            $emptynodeskey[] = $key;

        }
    }
    //return false;
    return $emptynodeskey;
}

//function for converting array keys to 1 dimensional array
function array_keys_multi(array $array)
{
    $keys = array();

    foreach ($array as $key => $value) {
        $keys[] = $key;

        if (is_array($array[$key])) {
            $keys = array_merge($keys, array_keys_multi($array[$key]));
        }
    }

    return $keys;
}

//function for checking xml structure
function display_xml_error($error)
{
    $return = "";
    //$return .= $error->column . "\n";

    switch ($error->level) {
        case LIBXML_ERR_WARNING:
            $return .= "Warning $error->code: ";
            break;
         case LIBXML_ERR_ERROR:
            $return .= "Error $error->code: ";
            break;
        case LIBXML_ERR_FATAL:
            $return .= "Fatal Error $error->code: ";
            break;
    }

    $return .= trim($error->message) .
               "<br>  Line: $error->line" .
               "<br/>  Column: $error->column";

    if ($error->file) {
        $return .= "<br/>  File: " . $error->file . "";
    }

    return addslashes("$return");
}

//function in converting xml to array
function objectsIntoArray($arrObjData, $arrSkipIndices = array())
{
    $arrData = array();

    // if input is object, convert into array
    if (is_object($arrObjData)) {
        $arrObjData = get_object_vars($arrObjData);
    }

    if (is_array($arrObjData)) {
        foreach ($arrObjData as $index => $value) {
            if (is_object($value) || is_array($value)) {
                $value = objectsIntoArray($value, $arrSkipIndices); // recursive call
            }
            if (in_array($index, $arrSkipIndices)) {
                continue;
            }
            $arrData[$index] = $value;
        }
    }
    return $arrData;
}

//function in parsing xml data
function UploadInventoryXmlFile($filename,$gameid,$productid,$providerid,$arrXml,$xmlname,$providername,$productname,$gamenum,$gamebatchnum,$POnum,$delnum)
{
    $boxesaffectedrows = 0;
    $booksaffectedrows = 0;
    $decksaffectedrows = 0;
    $msgcode = 0;
    
    list($tmgameimport,$tmproducts,$tmproviders,$tmgames,$auditlog,$tmdecks2,$tmdeckinfo,$tmboxes,$tmbooks,$tmtickets,$tminventorytemp,$tmgamebatches,$tmbatchtranslog) = DeclareVariables();
    $fullpath = "../xml/inventory/" . $filename;

    /* Step 1:
     * generate batch id
     */
    if($gamebatchnum == 1)
    {
        $gamebatchid = UpdateGameBatchID($gameid,$rowid);
    }
    else
    {
        $gamebatchid = CreateNewGameBatchID($gameid,$gamebatchnum);
    }
    
    /* Step 2:
     * insert uploaded xml in the table
     */
    list($gameimportid,$stat,$msg) = CreateGameImport($providerid,$filename,$fullpath,$productid,$gamebatchid,$POnum,$delnum);
    if ($stat == 1)
    {
        $errormsg = $msg;
        $errormsgtitle = "ERROR!";
        $msgcode = 1;
    }
    /* Step 3:
     * audit trail log
     */
    list($stat,$msg) = CreateAuditTrailLog($gameimportid,17);
    if($stat == 1) {
        $errormsgtitle = "ERROR!";
        $errormsg = $msg;
        $msgcode = 1;
    }

    //$gameno = substr($xmlname, 2, 3);
    $gameno = substr($xmlname, 2, strlen($xmlname));
    $gamecode = "FT" . $gameno;
    $id = $gameimportid;

    /* Step 4:
     * Insert to deck info
     */
    $deckinfo_success = 0;
    list($lastinsertdeckid,$stat,$msg) = CreateDeckInfo($productid,$gameid,$providerid);
    if($stat == 1)
    {
        $errormsgtitle = "ERROR!";
        $errormsg = $msg;
        $msgcode = 1;
    } else {
        $deckinfo_success++;
    }

    if ($deckinfo_success == 1) {
        $cartoncount = 0;
        $bookcount = 0;
        $imax1 = 0;
        
        list($status,$csv_content,$inventory_array) = CreateCSVContent($arrXml,$gameid,$providerid,$productid,$lastinsertdeckid,$gameimportid,$gamebatchid);

        $imax = 0;
                    
        $inventory_array_count = $inventory_array;
        if($status != 2)
        {
            do{
                /* Step 5:
                 * truncate inventorytemp
                 * Insert into temp table inventorytemp
                 */
                $tminventorytemp->TruncateInventoryTemp();
                /*insert into CSV file*/
                $fp = fopen('../csv/inventory.csv', 'w');
                fwrite($fp, $csv_content);
                fclose($fp);
                $csv_content = "";
                /*/*insert to inventorytemp using csv file*/
                $tminventorytemp->InsertInventoryUsingCSVFile('../csv/inventory.csv', ';', '<br>');

                /* Step 6:
                 * Insert boxes
                 */
                $tmboxes->InsertBoxesFromInventoryTemp($gameid,$providerid,$productid,$_SESSION['acctid'],$lastinsertdeckid,$gameimportid,$gamebatchid);
                $boxesaffectedrows = $tmboxes->AffectedRows;
                /* Step 7:
                 * Insert books
                 */
                $tmbooks->InsertBooksFromInventoryTemp($gameid,$providerid,$productid,$_SESSION['acctid'],$lastinsertdeckid,$gameimportid,$gamebatchid);
                $booksaffectedrows = $tmbooks->AffectedRows;

                /* Step 8:
                 * Insert to decks
                 */
                $tmdecks2->InsertDecksFromInventoryTemp($gameid, $providerid, $productid, $lastinsertdeckid, $_SESSION['acctid'], $gameimportid,$gamebatchid);
                $decksaffectedrows = $tmdecks2->AffectedRows;

                /* Step 9:
                 * If affected rows do not match the total no of rows that must be inserted, delete all inserted records
                 */
                if($decksaffectedrows != $inventory_array_count)
                {
                    list($gamebatchid,$decksaffectedrows,$booksaffectedrows,$boxesaffectedrows) = DeleteAllAffectedRows($decksaffectedrows,$boxesaffectedrows,$booksaffectedrows,$gamebatchnum,$gamebatchid);
                }

                $imax++;

                if ($imax > 2)
                {
                    $status = 2;
                    break;
                }
                else
                {
                    $status = 1;
                }
            }while($decksaffectedrows != $inventory_array_count);
        }
        else
        {
            $gamebatchid = DeleteUpdateGameBatchID($gamebatchnum,$gamebatchid);
        }

        if($status != 2)
        {
            /* Step 10:
             * Insert tickets per book
             */
            //InsertTickets($gameid,$providerid,$productid,$lastinsertdeckid,$gameimportid,$gamebatchid);
        }

        /* Step 11:
         * truncate inventorytemp
         */
        $tminventorytemp->TruncateInventoryTemp();

        /* Step 12:
         * Update xml status
         */
        list($stat,$msg) = UpdateXMLStatus($gameid,$status,$gameimportid,$gamebatchid);
        if($stat == 1)
        {
            $errormsgtitle = "ERROR!";
            $errormsg = $msg;
            $msgcode = 1;
        }

        /* Step 13:
         * Update deck info
         */
        if($status != 2)
        {
            list($stat,$msg) = UpdateDeckInfo($boxesaffectedrows,$booksaffectedrows,$lastinsertdeckid,$gamebatchid);
            if($stat == 1)
            {
                $errormsgtitle = "ERROR!";
                $errormsg = $msg;
                $msgcode = 1;
            }
        }
        else
        {
            $tmdeckinfo->DeleteByDeckInfoID($lastinsertdeckid);
        }

        /* Step 14:
         * insert to audit trail
         */
        list($stat,$msg) = CreateAuditTrailLog($id,22);
        if($stat == 1)
        {
            $errormsgtitle = "ERROR!";
            $errormsg = $msg;
            $msgcode = 1;
        }

        /* Step 15:
         * insert to batch transaction log
         */
        list($stat,$msg) = CreateBatchTransactionLog($gameid,$gamebatchnum,$filename,$status);
        if ($stat == 1) {
            $errormsgtitle = "ERROR!";
            $errormsg = $msg;
            $msgcode = 1;
        }
        
        if ($status != 2) {
            $countmsgtitle = "Inventory File Summary";
            $countmsg = "Provider Name: " . $providername . "<br/>" .
                    "Game Name: " . $productname . "<br/>" .
                    "Game Number: " . $gamenum . "<br/>" .
                    "Total No. of Cartons: " . number_format($boxesaffectedrows) . "<br/>" .
                    "Total No. of Books: " . number_format($booksaffectedrows);
        } else {
            $countmsgtitle = "ERROR!";
            $countmsg = "Uploading was unsuccessful . Please try again.";
        }
    }
    if($msgcode == 0)
    {
        $msgtitle = $countmsgtitle;
        $msg = $countmsg;
    }
    else
    {
        $msgtitle = $errormsgtitle;
        $msg = $errormsg;
    }
    $msgtitle = $countmsgtitle;
    $msg = $countmsg;
    return array($msgtitle,$msg);
}

function UpdateGameBatchID($gameid,$rowid)
{
    list($tmgameimport,$tmproducts,$tmproviders,$tmgames,$auditlog,$tmdecks2,$tmdeckinfo,$tmboxes,$tmbooks,$tmtickets,$tminventorytemp,$tmgamebatches,$tmbatchtranslog) = DeclareVariables();
    //update XMLType
    $batchid = $tmgamebatches->SelectBatchID($gameid,0);
    $rowid = $batchid[0]["GameBatchID"];
    $gamebatch["XMLType"] = 1;
    $gamebatch["GameBatchID"] = $rowid;
    $tmgamebatches->UpdateByArray($gamebatch);
    $gamebatchid = $rowid;

    return $rowid;
}

function CreateNewGameBatchID($gameid,$gamebatchnum)
{
    list($tmgameimport,$tmproducts,$tmproviders,$tmgames,$auditlog,$tmdecks2,$tmdeckinfo,$tmboxes,$tmbooks,$tmtickets,$tminventorytemp,$tmgamebatches,$tmbatchtranslog) = DeclareVariables();
    //generate new game batch number
    $gamebatch["GameID"] = $gameid;
    $gamebatch["BatchID"] = $gamebatchnum;
    $gamebatch["XMLType"] = 1;
    $tmgamebatches->Insert($gamebatch);
    $gamebatchid = $tmgamebatches->LastInsertID;

    return $tmgamebatches->LastInsertID;
}

function CreateGameImport($providerid,$filename,$fullpath,$productid,$gamebatchid,$POnum,$delnum)
{
    list($tmgameimport,$tmproducts,$tmproviders,$tmgames,$auditlog,$tmdecks2,$tmdeckinfo,$tmboxes,$tmbooks,$tmtickets,$tminventorytemp,$tmgamebatches,$tmbatchtranslog) = DeclareVariables();
    $tmimport["ProviderID"] = $providerid;
    $tmimport["FileName"] = $filename;
    $tmimport["FilePath"] = $fullpath;
    $tmimport["DateUploaded"] = 'now_usec()';
    $tmimport["ProductID"] = $productid;
    $tmimport["Status"] = '0';
    $tmimport["PONumber"] = $POnum;
    $tmimport["DeliveryNumber"] = $delnum;
    $tmgameimport->Insert($tmimport);
    $gameimportid = $tmgameimport->LastInsertID;
    if ($tmgameimport->HasError) {
        $msg = $tmgameimport->getError();
        $stat = 1;
    }
    else
    {
        $msg = "";
        $stat = 0;
    }

    return array($tmgameimport->LastInsertID,$stat,$msg);
}

function CreateAuditTrailLog($gameimportid,$audittrailfunctionid)
{
    list($tmgameimport,$tmproducts,$tmproviders,$tmgames,$auditlog,$tmdecks2,$tmdeckinfo,$tmboxes,$tmbooks,$tmtickets,$tminventorytemp,$tmgamebatches,$tmbatchtranslog) = DeclareVariables();
    $auditdtls["SessionID"] = $_SESSION['sid'];
    $auditdtls["AID"] = $_SESSION['acctid'];
    $auditdtls["TransDetails"] = "GameImport ID: " . $gameimportid;
    $auditdtls["TransDateTime"] = "now_usec()";
    $auditdtls["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
    $auditdtls["AuditTrailFunctionID"] = $audittrailfunctionid;
    $insertauditlog = $auditlog->Insert($auditdtls);
    if ($auditlog->HasError)
    {
        $msg = "Error has occured:" . $tmaccount->getError();
        $stat = 1;
    }
    else
    {
        $msg = "";
        $stat = 0;
    }

    return array($stat,$msg);
}

function CreateDeckInfo($productid,$gameid,$providerid)
{
    list($tmgameimport,$tmproducts,$tmproviders,$tmgames,$auditlog,$tmdecks2,$tmdeckinfo,$tmboxes,$tmbooks,$tmtickets,$tminventorytemp,$tmgamebatches,$tmbatchtranslog) = DeclareVariables();
    $deckinfo["ProductID"] = $productid;
    $deckinfo["GameID"] = $gameid;
    $deckinfo["ProviderID"] = $providerid;
    $deckinfo["CreatedByAID"] = $_SESSION["acctid"];
    $insertdeck = $tmdeckinfo->Insert($deckinfo);
    if ($tmdeckinfo->HasError)
    {
        $msg = "Error has occured:" . $tmdeckinfo->getError();
        $stat = 1;
    }
    else
    {
        $msg = "";
        $stat = 0;
    }

    return array($tmdeckinfo->LastInsertID,$stat,$msg);
}

function CreateCSVContent($arrXml,$gameid,$providerid,$productid,$lastinsertdeckid,$gameimportid,$gamebatchid)
{
    $status = 0;
    $csv_content = "";
    $inventory_array = null;
    
    list($tmgameimport,$tmproducts,$tmproviders,$tmgames,$auditlog,$tmdecks2,$tmdeckinfo,$tmboxes,$tmbooks,$tmtickets,$tminventorytemp,$tmgamebatches,$tmbatchtranslog) = DeclareVariables();
    foreach($arrXml as $key => $value)
    {
        if($status != 2)
        {
            $inventoryinfo = null;
            $imax1 = 0;
            $carton = $key;
            $inventory = $value;
            do{
                $tempinventory_array = null;
                for($i = 0 ; $i < count($inventory["pack_number"]) ; $i++)
                {
                    $inventoryinfo["GameID"] = $gameid;
                    $inventoryinfo["ProviderID"] = $providerid;
                    $inventoryinfo["ProductID"] = $productid;
                    $inventoryinfo["DeckID"] = $lastinsertdeckid;
                    $inventoryinfo["PackNumber"] = trim($inventory["pack_number"][$i]);
                    $inventoryinfo["Carton"] = trim($carton);
                    $inventoryinfo["AccountID"] = $_SESSION["acctid"];
                    $inventoryinfo["GameImportID"] = $gameimportid;
                    $inventoryinfo["GameBatchID"] = $gamebatchid;
                    $csv_content .= implode(";", $inventoryinfo) . "<br>";
                    $tempinventory_array++;
                    $inventory_array++;
                }
                $imax1++;

                if ($imax1 > 2)
                {
                        $status = 2;
                        break;
                }
                else
                {
                        $status = 1;
                }
            }while($tempinventory_array != count($inventory["pack_number"]));

        }
        else
        {
            break;
        }
    }

    return array($status,$csv_content,$inventory_array);
}

function DeleteAllAffectedRows($decksaffectedrows,$boxesaffectedrows,$booksaffectedrows)
{
    list($tmgameimport,$tmproducts,$tmproviders,$tmgames,$auditlog,$tmdecks2,$tmdeckinfo,$tmboxes,$tmbooks,$tmtickets,$tminventorytemp,$tmgamebatches,$tmbatchtranslog) = DeclareVariables();
    $lastdeckid = $tmdecks2->GetLastDeckID();
    $lastboxid = $tmboxes->GetLastBoxID();
    $lastbookid = $tmbooks->GetLastBookID();

    //delete all inserted decks
    if($lastdeckid[0]["LastID"] != NULL)
    {
        $start = ($lastdeckid[0]["LastID"] - $decksaffectedrows);
        $deletealltransactions = $tmdecks2->DeleteAllTransactions($start,$lastdeckid[0]["LastID"]);
    }

    //delete all inserted boxes
    if($lastboxid[0]["LastID"] != NULL)
    {
        $start = ($lastboxid[0]["LastID"] - $boxesaffectedrows);
        $deletealltransactions = $tmboxes->DeleteAllTransactions($start,$lastboxid[0]["LastID"]);
    }

    //delete all inserted books
    if($lastbookid[0]["LastID"] != NULL)
    {
        $start = ($lastbookid[0]["LastID"] - $booksaffectedrows);
        $deletealltransactions = $tmbooks->DeleteAllTransactions($start,$lastbookid[0]["LastID"]);
    }

    $gamebatchid = DeleteUpdateGameBatchID($gamebatchnum,$gamebatchid);

    $decksaffectedrows = 0;
    $booksaffectedrows = 0;
    $boxesaffectedrows = 0;

    return array($gamebatchid,$decksaffectedrows,$booksaffectedrows,$boxesaffectedrows);
}

function DeleteUpdateGameBatchID($gamebatchnum,$gamebatchid)
{
    list($tmgameimport,$tmproducts,$tmproviders,$tmgames,$auditlog,$tmdecks2,$tmdeckinfo,$tmboxes,$tmbooks,$tmtickets,$tminventorytemp,$tmgamebatches,$tmbatchtranslog) = DeclareVariables();
    //delete or update batch id
    if($gamebatchnum == 1)
    {
        $tmgamebatches->UpdateStatus($gamebatchid, 0);
    }
    else
    {
        $tmgamebatches->DeleteByGameBatchesID($gamebatchid);
    }

    $gamebatchid = 0;

    return $gamebatchid;
}

function InsertTickets($gameid,$providerid,$productid,$lastinsertdeckid,$gameimportid,$gamebatchid)
{
    $ticketcount = 200;
    $tmax = 0;
    $csv_content = "";
    $arrTickets = 0;
    list($tmgameimport,$tmproducts,$tmproviders,$tmgames,$auditlog,$tmdecks2,$tmdeckinfo,$tmboxes,$tmbooks,$tmtickets,$tminventorytemp,$tmgamebatches,$tmbatchtranslog) = DeclareVariables();
    $bookid = $tmbooks->SelectAllInsertedBookID($gameid,$providerid,$productid,$_SESSION['acctid'],$lastinsertdeckid,$gameimportid,$gamebatchid);
    if(count($bookid) > 0)
    {
        do{
            for($b = 0 ; $b < count($bookid) ; $b++)
            {
                for($a = 1 ; $a < ($ticketcount + 1) ; $a++)
                {
                    $ticket["BookID"] = $bookid[$b]["BookID"];
                    $ticket["TicketNumber"] = str_pad($a,3,0,STR_PAD_LEFT);
                    $csv_content .= implode(";", $ticket) . "<br>";
                    $arrTickets++;
                }
            }
            $tmax++;
            if ($tmax > 2)
            {
                $arrTickets = null;
                $status = 2;
                break;
            }
            else
            {
                $status = 1;
            }
        }while($arrTickets != (count($bookid) * $ticketcount));

        $arrTickets_count = $arrTickets;
        if($arrTickets_count > 0)
        {
            /*insert into CSV file*/
            $fp = fopen('../csv/tickets.csv', 'w');
            fwrite($fp, $csv_content);
            fclose($fp);
            $csv_content = "";
            /*insert to inventorytemp using csv file*/
            $tmtickets->InsertTicketsUsingCSVFile('../csv/tickets.csv', ';', '<br>');
            $insertticketaffectedrows = $tmtickets->AffectedRows;
            $maxid = $tmtickets->GetLastTicketID();
            if($insertticketaffectedrows != (count($bookid) * $ticketcount))
            {
                if($maxid[0]["lastid"] != NULL)
                {
                    $start = ($maxid[0]["lastid"] - $insertticketaffectedrows);
                    $deletealltransactions = $tmtickets->DeleteAllTransactions($start,$maxid[0]["lastid"]);
                }
            }
        }
    }
}

function UpdateXMLStatus($gameid,$status,$gameimportid,$gamebatchid)
{
    list($tmgameimport,$tmproducts,$tmproviders,$tmgames,$auditlog,$tmdecks2,$tmdeckinfo,$tmboxes,$tmbooks,$tmtickets,$tminventorytemp,$tmgamebatches,$tmbatchtranslog) = DeclareVariables();
    $xmlstatus["GameID"] = $gameid;
    $xmlstatus["Status"] = $status;
    $xmlstatus["GameImportID"] = $gameimportid;
    $xmlstatus["DateImported"] = "now_usec()";
    $xmlstatus["GameBatchID"] = $gamebatchid;
    $updatestatus = $tmgameimport->UpdateByArray($xmlstatus);
    if ($tmgameimport->HasError)
    {
        $msg = "Error:" . $tmgameimport->getError();
        $stat = 1;
    }
    else
    {
        $msg = "";
        $stat = 0;
    }
    return array($stat,$msg);
}

function UpdateDeckInfo($boxesaffectedrows,$booksaffectedrows,$lastinsertdeckid,$gamebatchid)
{
    list($tmgameimport,$tmproducts,$tmproviders,$tmgames,$auditlog,$tmdecks2,$tmdeckinfo,$tmboxes,$tmbooks,$tmtickets,$tminventorytemp,$tmgamebatches,$tmbatchtranslog) = DeclareVariables();
    $deckinfoupdate["BoxCount"] = $boxesaffectedrows;
    $deckinfoupdate["BookCount"] = $booksaffectedrows;
    $deckinfoupdate["DeckID"] = $lastinsertdeckid;
    $deckinfoupdate["GameBatchID"] = $gamebatchid;
    $updatedeckinfo = $tmdeckinfo->UpdateByArray($deckinfoupdate);
    if ($tmdeckinfo->HasError)
    {
        $msg = "Error:" . $tmgameimport->getError();
        $stat = 1;
    }
    else
    {
        $msg = "";
        $stat = 0;
    }
    return array($stat,$msg);
}

function CreateBatchTransactionLog($gameid,$gamebatchnum,$filename,$status)
{
    list($tmgameimport,$tmproducts,$tmproviders,$tmgames,$auditlog,$tmdecks2,$tmdeckinfo,$tmboxes,$tmbooks,$tmtickets,$tminventorytemp,$tmgamebatches,$tmbatchtranslog) = DeclareVariables();
    $batchtrans["GameID"] = $gameid;
    $batchtrans["BatchID"] = $gamebatchnum;
    $batchtrans["FileName"] = $filename;
    $batchtrans["TransactionDate"] = "now_usec()";
    $batchtrans["TransactionType"] = 1;
    $batchtrans["ProcessedByAID"] = $_SESSION["acctid"];
    $batchtrans["Status"] = $status;
    $tmbatchtranslog->Insert($batchtrans);
    if ($tmbatchtranslog->HasError)
    {
        $msg = "Error has occured:" . $tmbatchtranslog->getError();
        $stat = 1;
    }
    else
    {
        $msg = "";
        $stat = 0;
    }
    return array($stat,$msg);
}
?>