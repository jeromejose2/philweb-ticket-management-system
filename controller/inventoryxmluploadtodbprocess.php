<?php
/*
 * Created by Arlene R. Salazar on Sept. 12 , 2011
 * Purpose: For Inventory XML parsing
 */
$stylesheets[] = "../css/default.css";
$javascripts[] = "";
$pagesubmenuid = 34;

App::LoadModuleClass("TicketManagementCM", "TMGameImport");
App::LoadModuleClass("TicketManagementCM", "TMDecks2");
App::LoadModuleClass("TicketManagementCM", "TMDeckInfo");
App::LoadModuleClass("TicketManagementCM", "TMBoxes");
App::LoadModuleClass("TicketManagementCM", "TMBooks");
App::LoadModuleClass("TicketManagementCM", "TMAuditLog");
App::LoadModuleClass("TicketManagementCM", "TMTickets");
App::LoadModuleClass("TicketManagementCM", "TMGameManagement");

$tmgameimport = new TMGameImport();
$tmdecks2 = new TMDecks2();
$tmdeckinfo = new TMDeckInfo();
$tmboxes = new TMBoxes();
$tmbooks = new TMBooks();
$auditlog = new TMAuditLog();
$tmtickets = new TMTickets();
$tmgames = new TMGameManagement();

$where = " WHERE Status = 0";
$xmlfiles = $tmgameimport->SelectByWhere($where);

$ticketcount = 200;
//$ticketcount = 10;

if(count($xmlfiles) > 0)
{
    $totalboxcount = 0;
    $totalbookcount = 0;
    $totaldeckcount = 0;
    $totalticketcount = 0;
    for($ctr = 0 ; $ctr < count($xmlfiles) ; $ctr++)
    {
        $filename = explode("_", $xmlfiles[$ctr]["FileName"]);
        if($filename[1] != "WLIST")
        {
            $providerid = $xmlfiles[$ctr]["ProviderID"];$providerid = "1";
            $productid = $xmlfiles[$ctr]["ProductID"];
            $gameid = $xmlfiles[$ctr]["GameID"];
            $id = $xmlfiles[$ctr]["GameImportID"];

	    $gamecode = $filename[0];
            $xml = simplexml_load_file($xmlfiles[$ctr]["FilePath"]);
            $xmlUrl = $xmlfiles[$ctr]["FilePath"];
            $xmlStr = file_get_contents($xmlUrl);
            $xmlObj = simplexml_load_string($xmlStr);
            $arrXml = objectsIntoArray($xmlObj);

	    $xmlname = $xml->getName();

	    //if($gamecode == $xmlname)
            //{
		//insert to deckinfo
		$deckinfo_success = 0;
		do{
		    $deckinfo["ProductID"] = $productid;
		    $deckinfo["GameID"] = $gameid;
		    $deckinfo["ProviderID"] = $providerid;
		    $deckinfo["CreatedByAID"] = 1; //since this will be executed by a cron job, there will be no $_SESSION['acctid'] created
		    $insertdeck = $tmdeckinfo->Insert($deckinfo);

		    if($tmdeckinfo->HasError)
		    {

		    }
		    else
		    {
		        $deckinfo_success++;
		        $lastinsertdeckid = $tmdeckinfo->LastInsertID;
		    }
		}while($deckinfo_success == 2);

                //read xml file
                if($deckinfo_success == 1)
                {
                    

                    //App::Pr(count($arrXml));
                    //for($ctr = 0 ; $ctr < count($arrXml) ; $ctr++)
                    $cartoncount = 0;
                    $bookcount = 0;
                    foreach($arrXml as $key => $value)
                    {
			$success_ctr = 0;
			$carton = $key;
			$inventory = $value;
			$cartoncount++;

                        do{
			    $box["BoxNumber"] = $carton;
			    $insertbox = $tmboxes->Insert($box);
			    if($tmboxes->HasError)
			    {
				$message = $tmboxes->getError();
			    }
			    else
			    {
				$totalboxcount++;
				$lastinsertboxid = $tmboxes->LastInsertID;
			    }
                        }while($tmboxes->LastInsertID == 0);
                        //App::Pr($carton);
                        //for($i = 0 ; $i < count($inventory["BOOK_NUMBER"]) ; $i++)
                        do{
                            $arrDecks = null;
                            //$arrTickets = null;
                            for($i = 0 ; $i < count($inventory["PACK_NUMBER"]) ; $i++)
                            {
                                //insert to books
                                do{
                                    $book["BookNumber"] = $inventory["PACK_NUMBER"][$i];
                                    $insertbox = $tmbooks->Insert($book);
                                    if($tmbooks->HasError)
                                    {
                                        $message = $tmbooks->getError();
                                    }
                                    else
                                    {
				        $totalbookcount++;
                                        $lastinsertbookid = $tmbooks->LastInsertID;
                                        $arrBookID[] = $lastinsertbookid;
                                    }
                                }while($tmbooks->LastInsertID == 0);

				//insert to decks
				$deck["DeckID"] = $lastinsertdeckid;
				$deck["GameID"] = $gameid;
				$deck["ProductID"] = $productid;
				$deck["BoxID"] = $lastinsertboxid;
				$deck["BoxNumber"] = $carton;
				$deck["BookNumber"] = $inventory["PACK_NUMBER"][$i];
				$deck["BookID"] = $lastinsertbookid;
				$arrDecks[] = $deck;
				/*$decks = $tmdecks2->Insert($deck);
				if($tmdecks2->HasError)
				{

				}
				else
				{
				$success_ctr++;
				}*/
				//App::Pr($carton . "==>" . $inventory["BOOK_NUMBER"][$i] . "<br/>");

				//}while($success_ctr != count($inventory["BOOK_NUMBER"]));
                            }
                        
                        $insertdecks = $tmdecks2->InsertMultiple($arrDecks);
                        $insertdecksaffectedrow = $tmdecks2->AffectedRows;
                        if($insertdecksaffectedrow != count($inventory["PACK_NUMBER"]))
                        {
                            do{
                                $maxid = $tmdecks2->GetLastTicketID();
                                $start = ($maxid[0]["lastid"] - $insertdecksaffectedrow);
                                //$start = ($tmdecks2->LastInsertID - $insertdecksaffectedrow);
                                $deletealltransactions = $tmdecks2->DeleteAllTransactions($start,$maxid[0]["lastid"]);
                                $deletesaffectedrow = ($insertdecksaffectedrow - $tmdecks2->AffectedRows);
                            //}while($tmwinnings2->AffectedRows != $success_ctr);
                            //}while(($tmdecks2->AffectedRows - $insertdecksaffectedrow) != 0);
                            }while($deletesaffectedrow != 0);
                        }
                        else
                        {
                            $bookcount = ($bookcount + count($inventory["PACK_NUMBER"]));
			    $totaldeckcount = ($totaldeckcount + count($inventory["PACK_NUMBER"]));
                        }
                        }while($insertdecksaffectedrow != count($inventory["PACK_NUMBER"]));
                    }

                    // insert tickets(200)
                    if(count($arrBookID) > 0)
                    {
			for($b = 0 ; $b < count($arrBookID) ; $b++)
			{
                            $arrTickets = null;
                            do{
				$success_ticket_ctr = 0;
				for($a = 1 ; $a < ($ticketcount + 1) ; $a++)
                                {
                                    //$gamenum = $tmgames->SelectByID($gameid);
                                    //$ticket["BookID"] = $lastinsertbookid;
                                    $ticket["BookID"] = $arrBookID[$b];
                                    //$ticket["TicketNumber"] = $gamenum[0]["GameNumber"] . $inventory["BOOK_NUMBER"][$i] . str_pad($a,3,0,STR_PAD_LEFT);
                                    $ticket["TicketNumber"] = str_pad($a,3,0,STR_PAD_LEFT);
                                    //$insertticket = $tmtickets->Insert($ticket);
                                    $arrTickets[] = $ticket;
				/*if($tmtickets->HasError)
				{
				    
				}
				else
				{
				    $success_ticket_ctr++;
				}*/
                                }
				$insertticket = $tmtickets->InsertMultiple($arrTickets);
				$insertticketaffectedrows = $tmtickets->AffectedRows;
				//$maxid = $tmtickets->GetLastTicketID();
				//App::Pr($maxid[0]["lastid"]);
				//App::Pr(print_r($maxid));
                                      
				if($insertticketaffectedrows != $ticketcount)
				{
                                    do{
					$maxid = $tmtickets->GetLastTicketID();
					$start = ($maxid[0]["lastid"] - $insertticketaffectedrows);
					$deletealltransactions = $tmtickets->DeleteAllTransactions($start,$maxid[0]["lastid"]);
					$deleteticketaffectedrow = ($insertticketaffectedrows - $tmtickets->AffectedRows);
                                    }while($deleteticketaffectedrow != 0);
                                }
                                else
                                {
                                    $totalticketcount = ($totalticketcount + $insertticketaffectedrows);
                                }
                            }while($insertticketaffectedrows != $ticketcount);
                        }
                    }
                    //update gameimport status
		    $status["Status"] = "1";
		    $status["GameImportID"] = $id;$status["DateImported"] = "now_usec()";
		    $updatestatus = $tmgameimport->UpdateByArray($status);
		    if($tmgameimport->HasError)
		    {
		        $message = "Error:" . $tmgameimport->getError();
		    }

                    //update deckinfo
                    $deckinfoupdate["BoxCount"] = $cartoncount;
                    $deckinfoupdate["BookCount"] = $bookcount;
                    $deckinfoupdate["DeckID"] = $lastinsertdeckid;
                    $updatedeckinfo = $tmdeckinfo->UpdateByArray($deckinfoupdate);
                    if($tmdeckinfo->HasError)
                    {
                        $message = "Error:" . $tmdeckinfo->getError();
                    }
                }

		//audit trail log
		$auditdtls["SessionID"] = session_id();
		$auditdtls["AID"] = 1; //since this will be executed by a cron job, there will be no $_SESSION['acctid'] created
		$auditdtls["TransDetails"] = "GameImport ID: " . $id;
		$auditdtls["TransDateTime"] = "now_usec()";
		$auditdtls["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
		$auditdtls["AuditTrailFunctionID"] = 22;
		$insertauditlog = $auditlog->Insert($auditdtls);
		if($auditlog->HasError)
		{
		    //$confadduser_title = "ERROR!";
		    $message = "Error has occured:" . $tmaccount->getError();
		}
	    /*}
            else
            {
                App::Pr($xmlfiles[$ctr]["FileName"] . " could not be processed.");
            }*/
        }
    }
    $countmessage = "Total Carton/Boxes: $totalboxcount \n Total Books: $totalbookcount \n Total Decks: $totaldeckcount \n Total Tickets: $totalticketcount \n was successfully loaded.";
}


function objectsIntoArray($arrObjData, $arrSkipIndices = array())
{
    $arrData = array();

    // if input is object, convert into array
    if (is_object($arrObjData)) {
        $arrObjData = get_object_vars($arrObjData);
    }

    if (is_array($arrObjData)) {
        foreach ($arrObjData as $index => $value) {
            if (is_object($value) || is_array($value)) {
                $value = objectsIntoArray($value, $arrSkipIndices); // recursive call
            }
            if (in_array($index, $arrSkipIndices)) {
                continue;
            }
            $arrData[$index] = $value;
        }
    }
    return $arrData;
}
?>