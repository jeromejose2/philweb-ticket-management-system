<?php
/*
 * Created By: mtcc
 * Created On: 08/24/2011
 */

App::LoadModuleClass("TicketManagementCM", "TMAccounts");
App::LoadModuleClass("TicketManagementCM", "TMAccountSession");
App::LoadModuleClass("TicketManagementCM", "TMAuditLog");
App::LoadModuleClass("TicketManagementCM", "TMAccessRights");
App::LoadModuleClass("TicketManagementCM", "TMPasswordUpdateRequests");

App::LoadControl("TextBox");
App::LoadControl("Button");

App::LoadCore("PHPMailer.class.php");

$errormsgtitle = "";
$regards = "";
$project_name = "Ticket Management System";
$splashpage = "index.php";
$regards = App::getParam('country');

$fproc = new FormsProcessor();
$tmlogin = new TMAccounts();
$tmauditlog = new TMAuditLog();
$passwordupdaterequest = new TMPasswordUpdateRequests();

$txtUsername = new TextBox("txtUsername", "txtUsername", "Username: ");
$txtUsername->Length = 20;
$txtUsername->Args="autocomplete='off'";

$txtPassword = new TextBox("txtPassword", "txtPassword", "Password: ");
$txtPassword->Password = true;
$txtPassword->Length = 20;
$txtPassword->Args="onkeypress='javascript: return disableSpace(event);' autocomplete='off'";

$txtEmail = new TextBox("txtEmail", "txtEmail", "Email");
$txtEmail->Length = 100;
$txtEmail->Style = "width:300px;";

$btnSubmit = new Button("btnSubmit", "btnSubmit", "Login");
$btnSubmit->Args="onclick='javascript: return checklogin();'";
$btnSubmit->IsSubmit = true;

$btnReset = new Button("btnReset", "btnReset", "RESET");
$btnReset->Args="onclick='javascript: return closelightbox();'";
$btnReset->IsSubmit = true;

$fproc->AddControl($txtUsername);
$fproc->AddControl($txtPassword);
$fproc->AddControl($txtEmail);
$fproc->AddControl($btnSubmit);
$fproc->AddControl($btnReset);

$fproc->ProcessForms();

if ($fproc->IsPostBack)
{    
    if ($btnSubmit->SubmittedValue == "Login")
    {

        $username = mysql_escape_string(trim($txtUsername->Text));
        $password = mysql_escape_string(trim($txtPassword->Text));
        
        // check if username exist
        $arrChkUser = $tmlogin->SelectLoginAttempts(MD5($username));
        if (count($arrChkUser) == 1){
            $dbPass = $arrChkUser[0]["Password"];
            $ctrAttempt = $arrChkUser[0]["LoginAttempts"];
            $dbAID = $arrChkUser[0]["AID"];
            $dbStat = $arrChkUser[0]["Status"];
            $tmpforChangePass = $arrChkUser[0]["ForChangePassword"];
           
            if ($dbStat == 3 || $dbStat == 4){
                // Locked Account
                $errormsg = "Account is locked. Please call customer service hotline to activate your account. Thank you.";
                $errormsgtitle = "ACCOUNT LOCKED!";
            } else if ($dbStat == 1){
                
            //check if resetting forgot password exist?
            if ($tmpforChangePass == 1){
                $errormsg = "You cannot login at the moment. Please check your email for the link to change your password.";
                $errormsgtitle = "ERROR!";
            } else { 
            //check if password is correct
            if (MD5($password) == $dbPass)
            {
                // Login
                $arrlogin = $tmlogin->Login(MD5($username), MD5($password));  
                $logindtls = $arrlogin[0];
                $_SESSION['uname'] =$username;
                $_SESSION['acctid'] = $logindtls["AID"];
                $_SESSION['accttype'] = $logindtls["AccountTypeID"];
                session_regenerate_id();
                $forchangepass = $logindtls["ForChangePassword"];
                $_SESSION['sid'] = session_id(); 
                $accounttypeid = $logindtls["AccountTypeID"];
                $tmaccessrights = new TMAccessRights();
                $dmenus = $tmaccessrights->GetMenusByAccountTypeID($accounttypeid);
                $_SESSION["usermenus"] = $dmenus;
                $landingpage = $tmaccessrights->GetDefaultPage($accounttypeid);
                $splashpage = $landingpage[0]["Link"];
                
                // Unset login attempts
                $tmupdateacct = $tmlogin->UnsetLoginAttempts(MD5($username));

                if ($forchangepass == 0)
                {
                    $tmacctsessionid = new TMAccountSession();   
                    //check if account has session
                    $arrSession = $tmacctsessionid->DoesAccountHasSession($_SESSION['acctid']);
                    if (count($arrSession) == 1)
                    {   
                        //update session id     
                        $sessiondtls = $arrSession[0];
                        $aid = $sessiondtls["AID"];

                        $tmsidparam["SessionID"] = $_SESSION['sid']; 
                        $tmsidparam["TransDate"] = 'now_usec()'; 
                        $tmsidparam["DateEnd"] = ''; 
                        $tmsidparam["AID"] = $aid;

                        $tmupdatesid = $tmacctsessionid->UpdateByArray($tmsidparam);
                        if ($tmacctsessionid->HasError)
                        {
                            $errormsg = $tmacctsessionid->getError();
                            $errormsgtitle = "ERROR!";
                            //App::Pr($tmacctsessionid->getError());
                        }
                        else
                        {                
                            //insert in auditlog table      
                            $tmaudit["SessionID"] = $_SESSION['sid'];
                            $tmaudit["AID"] = $_SESSION['acctid'];
                            $tmaudit["TransDetails"] = 'Account ID: '.$_SESSION['acctid'];
                            $tmaudit["TransDateTime"] = 'now_usec()';
                            $tmaudit["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
                            $tmaudit["AuditTrailFunctionID"] = '1';    
                            $tmauditlog->Insert($tmaudit);
                            if ($tmauditlog->HasError)
                            {
                                $errormsg = $tmauditlog->getError();
                                $errormsgtitle = "ERROR!";
                                //$_SESSION['errormsg'] = $tmauditlog->getError();
                            }
                            else
                            {
                                URL::Redirect($splashpage);
                            }                
                        }   
                    }
                    else
                    {
                        //insert into account sessions table
                        $tmacctsid["AID"] = $_SESSION['acctid'];
                        $tmacctsid["SessionID"] = $_SESSION['sid'];            
                        $tmacctsid["TransDate"] = 'now_usec()';     
                        $tmacctsid["DateStart"] = 'now_usec()'; 
                        $tmacctsid["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
                        $tmacctsessionid->Insert($tmacctsid);
                        if ($tmacctsessionid->HasError)
                        {
                            $errormsg = $tmacctsessionid->getError();
                            $errormsgtitle = "ERROR!";
                        }
                        else
                        {
                            //insert in auditlog table      
                            $tmaudit["SessionID"] = $_SESSION['sid'];
                            $tmaudit["AID"] = $_SESSION['acctid'];
                            $tmaudit["TransDetails"] = 'Account ID: '.$_SESSION['acctid'];
                            $tmaudit["TransDateTime"] = 'now_usec()';
                            $tmaudit["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
                            $tmaudit["AuditTrailFunctionID"] = '1';    
                            $tmauditlog->Insert($tmaudit);
                            if ($tmauditlog->HasError)
                            {
                                $errormsg = $tmauditlog->getError();
                                $errormsgtitle = "ERROR!";
                                //$_SESSION['errormsg'] = $tmauditlog->getError();
                            }
                            else
                            {
                                URL::Redirect($splashpage);
                            }     
                        } 
                    }
                }
                else
                {
                     $errormsg = "You cannot login at the moment. Please check your email for the link to change your password.";
                     $errormsgtitle = "ERROR!";
                }
            }
            else{
                // login attempts counter
                if ($ctrAttempt > 4){
                    // Lock the Account
                    $tmupdateacct = $tmlogin->UpdateStatusToLock(mysql_escape_string(trim($txtUsername->Text)));
                    $errormsg = "Account is locked. Please call customer service hotline to activate your account. Thank you.";
                    $errormsgtitle = "ACCOUNT LOCKED!";
                } else {
                    $ctrAttempt = $ctrAttempt + 1;
                    //update login attempt
                    $tmacctparam["LoginAttempts"] = $ctrAttempt; 
                    $tmupdateacct = $tmlogin->UpdateLoginAttempt(mysql_escape_string(trim($txtUsername->Text)), $ctrAttempt);

                    //insert in auditlog table 
                    $tmaudit["SessionID"] = session_id();
                    $tmaudit["AID"] = $dbAID;
                    $tmaudit["TransDetails"] = 'Username: '.$username;
                    $tmaudit["TransDateTime"] = 'now_usec()';
                    $tmaudit["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
                    $tmaudit["AuditTrailFunctionID"] = '4';  
                    $tmauditlog->Insert($tmaudit);
                    if ($tmauditlog->HasError)
                    {
                        $errormsg = $tmauditlog->getError();
                        $errormsgtitle = "ERROR!";
                    }
                    else{
                        $errormsg = "You have entered an invalid information.";
                        $errormsgtitle = "ERROR!";
                    }
                }
            }
        }
        }
        }
        else{
            // if username does not exist
            $errormsg = "You have entered an invalid information.";
            $errormsgtitle = "Invalid Log In";
            //App::Pr('successful log to audit here***');
        }
    }
//-----------------------------------------------------------------------
    
    if ($btnReset->SubmittedValue == "RESET")
    {        
        $email = $txtEmail->Text;
        $arrEmail = $tmlogin->CheckEmail(mysql_escape_string(trim($email)));
        $newpass = substr(session_id(),0,8); 
        if ($arrEmail[0]["cnt"] == 1)
        {
            //get acct details and insert in passwordupdaterequests
            $arracctdtls = $tmlogin->SelectAIDByEmail(mysql_escape_string(trim($email)));
            if  (count($arracctdtls) == 1)
            {
                $acctdtls = $arracctdtls[0];
                $aid = $acctdtls["AID"];
                $name = $acctdtls["Name"];
                $uname = $acctdtls["UserName"];
                
                $requestcode = MD5($email.$aid.date("Y-m-d H:i:s"));
                $passwordrequest["AID"] = $aid;
                $passwordrequest["RequestCode"] = $requestcode;
                $passwordrequest["DateRequested"] = date("Y-m-d H:i:s"); 
                $passwordrequest["Status"] = 0;
                $changepwordrequest = $passwordupdaterequest->Insert($passwordrequest);
                if($passwordupdaterequest->HasError)
                {
                    $errormsgfp = "Error has occurred: " . $passwordupdaterequest->getError();
                    $errormsgtitle = "ERROR!";
                }
                
                //update account info
                $tmacctparam["Password"] = MD5($newpass); 
                $tmacctparam["AID"] = $aid;
                
                //$tmupdateacct = $tmlogin->UpdateByArray($tmacctparam);
                $tmupdateacct = $tmlogin->UpdateAccountPassword($aid, MD5($newpass));
                if ($tmlogin->HasError)
                {
                    $errormsg = $tmlogin->getError();
                    $errormsgtitle = "ERROR!";
                }
                
                //log to audit trail
                //insert in auditlog table 
                $tmaudit["SessionID"] = session_id();
                $tmaudit["AID"] = $aid;
                $tmaudit["TransDetails"] = 'Username: '.$uname;
                $tmaudit["TransDateTime"] = 'now_usec()';
                $tmaudit["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
                $tmaudit["AuditTrailFunctionID"] = '3';  
                $tmauditlog->Insert($tmaudit);
                if ($tmauditlog->HasError)
                {
                    $errormsg = $tmauditlog->getError();
                    $errormsgtitle = "ERROR!";
                }
                else{
                    $errormsg = "You have entered an invalid information.";
                    $errormsgtitle = "ERROR!";
                }
                
                $pm = new PHPMailer();
                $pm->AddAddress($email, $name);
                $pageURL = 'http';
                if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
                $pageURL .= "://";
                $folder = $_SERVER["REQUEST_URI"];
                $folder = substr($folder,0,strrpos($folder,'/') + 1);
                if ($_SERVER["SERVER_PORT"] != "80") 
                {
                  $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$folder;
                } 
                else 
                {
                  $pageURL .= $_SERVER["SERVER_NAME"].$folder;
                }
                
                $pm->Body = "Dear $name, \n\nThis is to inform you that your password has been reset on this date ".date("m/d/Y")." and time ".date("H:i:s").". \nHere are your new Account Details:\n\n\t\t\t\t\tUsername: ".$uname.
                "\n\t\t\t\t\tPassword: ".$newpass."\n\n".
                "For security purposes, please change this system-generated password upon log in at   
                \n\t\t\t " . $pageURL . "resetforgotpassword.php?rcode=".$requestcode."\n\n".
                "If you didn't perform this procedure, please report this to our toll free Customer Service hotline at +63 02 338 5599 or email us at customersupport@philweb.com.ph.
                \n\nRegards,\n\nCustomer Support\n$regards Ticket Management System";
                
                $pm->From = "no-reply@ticketmanagementsystem.com";
                $pm->FromName = "Ticket Management System";
                $pm->Host = "localhost";
                $pm->Subject = "Ticket Management System Reset Password";
                $email_sent = $pm->Send();
                if($email_sent)
                {
                    $errormsg = "Your password has been sent to your e-mail account. For security purpose, please change this system-generated password upon log in.";
                    $errormsgtitle = "Reset Password";
                }
                else
                {
                    $errormsgfp = "An error occurred while sending the email to your email address";
                    $errormsgtitle = "ERROR!";
                }
            }
            else
            {
                $errormsg = "You have entered an invalid e-mail address. Please try again.";
                $errormsgtitle = "Reset Password";
            }
            
        }
        else
        {
            
            //if (($btnReset->SubmittedValue == "Reset") && isset($txtEmail))
            if ($btnReset->SubmittedValue == "RESET")
            {
                $errormsgfp = "You have entered an invalid e-mail address. Please try again.";
            }
            $errormsgtitle = "Reset Password";
        }
    }
}
?>