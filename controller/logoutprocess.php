<?php
App::LoadModuleClass("TicketManagementCM", "TMAccountSession");
App::LoadModuleClass("TicketManagementCM", "TMAuditLog");

$tmsid = $_SESSION['sid'];
$tmsession = new TMAccountSession();   
$tmauditlog = new TMAuditLog();

//check if session exists
$arrSession = $tmsession->DoesSessionExists($tmsid);
$id = $arrSession[0]["AID"];
if (count($arrSession) > 0)
{ 
    $tmsession->UpdateDateEnd($id);
    if ($tmsession->HasError)
    {
        $errormsg = $tmsession->getError();
        $errormsgtitle = "ERROR!";
    }
    
    //insert in auditlog table      
    $tmaudit["SessionID"] = $_SESSION['sid'];
    $tmaudit["AID"] = $id;
    $tmaudit["TransDetails"] = 'Account ID: '.$id;
    $tmaudit["TransDateTime"] = 'now_usec()';
    $tmaudit["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
    $tmaudit["AuditTrailFunctionID"] = '2';  
    $tmauditlog->Insert($tmaudit);
    if ($tmauditlog->HasError)
    {
        $errormsg = $tmauditlog->getError();
        $errormsgtitle = "ERROR!";
    }
    
    session_destroy();
    URL::Redirect("login.php");
}
else
{
     $errormsg = "Session does not exist.";  
     $errormsgtitle = "ERROR!";
}
App::Pr("<script> window.location = 'login.php'; </script>");
?>
