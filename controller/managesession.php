<?php
/*
 * @author
 * Purpose   : controller for managesession
 */
App::LoadModuleClass("TicketManagementCM", "TMAccountSession");
App::LoadModuleClass("TicketManagementCM", "TMAuditLog");
App::LoadModuleClass("TicketManagementCM", "TMAccessRights");


if (isset($_SESSION['sid']) && $_SESSION['sid'] != '')
{
    $tmsid = $_SESSION['sid'];
    $accttypeid = $_SESSION['accttype'];
    $tmsession = new TMAccountSession();
    $tmauditlog = new TMAuditLog();
    $tmaccessrights = new TMAccessRights();
    //check if has page access..
    $arrPageAccess = $tmaccessrights->PageAccessRights($accttypeid, $pagesubmenuid);

    if ($arrPageAccess == 0)
    {
        $chkPage = 0;
    }
    else
    {
        $_SESSION['currPage'] = URL::CurrentPage();
        $chkPage = 1;
        //check if session exists
        $arrSession = $tmsession->GetSessionDtls($tmsid);
        if (count($arrSession) == 1)
        {
            $sessiondtls = $arrSession[0];
            $sessionid = $sessiondtls["SessionID"];
            $currtime = $sessiondtls["CurrTime"];
            $timestart = $sessiondtls["TransDate"];
            $id = $sessiondtls["ID"];
            $aid = $sessiondtls["AID"];
            $_SESSION['acctid'] = $aid;
            
            //check if expired
            $timediff = (strtotime($currtime) - strtotime($timestart)) / 60;
            if($timediff >= 20) // SESSION TIMEOUT
            {
                $updSession["DateEnd"] = 'now_usec()';
                $tmsession->UpdateDateEnd($aid);
                if ($tmsession->HasError)
                {
                    $errormsgacc = $tmauditlog->getError();
                    $errormsgacctitleacc = "ERROR!";
                }

                //insert in auditlog table      
                $tmaudit["SessionID"] = $_SESSION['sid'];
                $tmaudit["AID"] = $_SESSION['acctid'];
                $tmaudit["TransDetails"] = 'Account ID: '.$_SESSION['acctid'];
                $tmaudit["TransDateTime"] = 'now_usec()';
                $tmaudit["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
                $tmaudit["AuditTrailFunctionID"] = '2';  
                $tmauditlog->Insert($tmaudit);
                if ($tmauditlog->HasError)
                {
                    $errormsgacc = $tmauditlog->getError();
                    $errormsgacctitleacc = "ERROR!";
                }
                session_destroy();
                URL::Redirect('login.php');
            }
            else
            {               
                //update session id  
                $tmupdatesid = $tmsession->UpdateTransDate($id);
                if ($tmsession->HasError)
                {
                    $errormsgacc = $tmsession->getError();
                    $errormsgacctitleacc = "ERROR!";
                }
            }
        }
        else
        {
            URL::Redirect('login.php');
        }

    }
}
else
{
    // If SessionID Does not Exist ..
    URL::Redirect('login.php');
}
?>
