<?php

/*
 * Author: Noel Antonio
 * Date Created: 2011-09-05
 * Company: Philweb Corporation
 */
$pagesubmenuid = 12;

App::LoadModuleClass("TicketManagementCM", "TMGameManagement");
App::LoadModuleClass("TicketManagementCM", "TMWinnings");
App::LoadModuleClass("TicketManagementCM", "TMProducts");
App::LoadModuleClass("TicketManagementCM", "TMTickets");
//App::LoadModuleClass("TicketManagementCM", "Pdf");
App::LoadModuleClass("TicketManagementCM", "TMCurrencies");

App::LoadControl("TextBox");
App::LoadControl("Button");
App::LoadControl("ComboBox");
App::LoadControl("PagingControl2");
App::LoadControl("Hidden");

$TMcurrencies = new TMCurrencies();
$fproc = new FormsProcessor();
$TMWinnings = new TMWinnings();
$TMGames = new TMGameManagement();
$TMProducts = new TMProducts();
$TMTickets = new TMTickets();

//get list of game card value
$ddlcardvalue = new ComboBox('ddlcardvalue','ddlcardvalue', 'Card Value: ');
$ddlcardvalue->Args = "onchange='javascript: get_gamename();'";
$cardprize = $TMGames->SelectByCardValue();
$cardprize_list = new ArrayList();
$cardprize_list->AddArray($cardprize);
$cardlist = null;
$cardlist[] = new ListItem("ALL","0",false);
$ddlcardvalue->Items = $cardlist;
$ddlcardvalue->DataSource = $cardprize_list;
$ddlcardvalue->DataSourceText = "CardPrice";
$ddlcardvalue->DataSourceValue = "CardPrice";
$ddlcardvalue->DataBind();


//$gamenames = $TMProducts->SelectAllGameName();
//$gamename_list = new ArrayList();
//$gamename_list->AddArray($gamenames);
$ddlgamename = new ComboBox("ddlgamename","ddlgamename","Game Name: ");
$ddlgamename->Args = "onchange = 'javascript: get_gamenumber();'";
$ddlgamename->ShowCaption = true;
$gamenameopt = null;
$gamenameopt[] = new ListItem("ALL", "0", true);
$ddlgamename->Items = $gamenameopt;
//$ddlgamename->DataSource = $gamename_list;
//$ddlgamename->DataSourceText = "ProductName";
//$ddlgamename->DataSourceValue = "ProductID";
//$ddlgamename->DataBind();


//$games = $TMGames->SelectByGameType();
//$games_list = new ArrayList();
//$games_list->AddArray($games);
$ddlgametype = new ComboBox("ddlgametype", "ddlgametype", "Game Number: ");
$ddlgametype->Args = "onchange='javascript: get_prizetype();'";
$litemgmetype = null;
$litemgmetype[] = new ListItem("ALL", "0", true);
$ddlgametype->Items = $litemgmetype;
$ddlgametype->ShowCaption = true;
//$ddlgametype->DataSource = $games_list;
//$ddlgametype->DataSourceText = "GameNumber";
//$ddlgametype->DataSourceValue = "GameID";
//$ddlgametype->DataBind();


$flag = new Hidden("flag","flag","flag");
$xmltype = new Hidden("xmltype","xmltype","xmltype");

$btnExport = new Button("export","export","Export to PDF");
$btnExport->IsSubmit = true;

$btnExportCSV = new Button("btnExportCSV","btnExportCSV","Export to CSV");
$btnExportCSV->IsSubmit = true;

$cboPrizeType = new ComboBox("cboPrizeType", "cboPrizeType", "Prize Type : ");
$cboPrizeType->ShowCaption = true;
$options = null;
$options[] = new ListItem("ALL","0",true);
$cboPrizeType->Items = $options;

$hidPrizeTypeID = new Hidden("hidPrizeTypeID", "hidPrizeTypeID","");
$hidGameTypeID = new Hidden("hidGameTypeID", "hidGameTypeID", "");

$btnSubmit = new Button("btnSubmit", "btnSubmit", "Submit");
$btnSubmit->IsSubmit = true;
$btnSubmit->Args = "onclick='javascript: return validate();'";

$itemsperpage = 10;
$pgcon = new PagingControl2($itemsperpage, 1);
$pgcon->URL = "javascript:ChangePage(%currentpage);";
$pgcon->ShowMoveToFirstPage = true;
$pgcon->ShowMoveToLastPage = true;
$pgcon->PageGroup = 5;

$fproc->AddControl($ddlcardvalue);
$fproc->AddControl($cboPrizeType);
$fproc->AddControl($ddlgametype);
$fproc->AddControl($hidGameTypeID);
$fproc->AddControl($hidPrizeTypeID);
$fproc->AddControl($btnSubmit);
$fproc->AddControl($ddlgamename);
$fproc->AddControl($btnExport);
$fproc->AddControl($btnExportCSV);
$fproc->AddControl($xmltype);
$fproc->AddControl($flag);
$fproc->ProcessForms();

if ($fproc->IsPostBack)
{
    if ($flag->SubmittedValue == 1 && $ddlcardvalue->SubmittedValue != 0)
    {
        $cardval = $ddlcardvalue->Value;
        $gamenames = $TMProducts->SelectGameNamePerCardPrice($cardval);
        $gamenumbers_list1 = new ArrayList();
        $gamenumbers_list1->AddArray($gamenames);       
        $litemgmetype1 = null;
        $litemgmetype1[] = new ListItem("ALL", "0", false);
        $ddlgamename->Items = $litemgmetype1;
        $ddlgamename->DataSource = $gamenumbers_list1;
        $ddlgamename->DataSourceText = "ProductName";
        $ddlgamename->DataSourceValue = "ProductID";
        $ddlgamename->DataBind();
        $ddlgamename->SetSelectedValue($ddlgamename->SubmittedValue);    

        $ddlgametype->ClearItems();
        $prodid1 = $ddlgamename->SubmittedValue;
        $gamenumbers = $TMProducts->SelectGameNumPerGameName($prodid1);
        $gamenumbers_list1 = new ArrayList();
        $gamenumbers_list1->AddArray($gamenumbers);       
        $litemgmetype1 = null;
        $litemgmetype1[] = new ListItem("ALL", "0", false);
        $ddlgametype->Items = $litemgmetype1;
        $ddlgametype->DataSource = $gamenumbers_list1;
        $ddlgametype->DataSourceText = "GameNumber";
        $ddlgametype->DataSourceValue = "GameID";
        $ddlgametype->DataBind();
        $ddlgametype->SetSelectedValue($ddlgametype->SubmittedValue);
    }
    
    $ddlgametype->SetSelectedValue($ddlgametype->SubmittedValue);
    $ddlgamename->SetSelectedValue($ddlgamename->SubmittedValue);

    $arrOperators = $TMWinnings->SelectPrizeType($ddlgametype->SubmittedValue);
    $prizetype_list = new ArrayList();
    $prizetype_list->AddArray($arrOperators);
    $options = null;
    $options[] = new ListItem("ALL","0", true);
    $cboPrizeType->Items = $options;
    
    $moneycurrency = 'KHR';
    $getconvertmoney = $TMcurrencies->SelectByCurrencySymbol($moneycurrency);
    $fixedrate = $getconvertmoney[0]['FixedRate'];
    for($i = 0 ; $i < count($prizetype_list) ; $i++)
    {
        if (strpos($prizetype_list[$i]["Description"],'KHR') !== false) {
            $prizetype_list[$i]["Description"] = trim($prizetype_list[$i]["Description"], KHR);
            $prizetype_list[$i]["Description"] = (int)str_replace(array(' ', ','), '', $prizetype_list[$i]["Description"]);
            $prizetype_list[$i]["Description"] = $prizetype_list[$i]["Description"]/$fixedrate;
            $prizetype_list[$i]["Description"] = "$".number_format($prizetype_list[$i]["Description"]);
        }   
            
    }
    
    $cboPrizeType->DataSource = $prizetype_list;
    $cboPrizeType->DataSourceText = "Description";
    $cboPrizeType->DataSourceValue = "PrizeID";
    $cboPrizeType->DataBind();
    $cboPrizeType->SetSelectedValue($cboPrizeType->SubmittedValue);
    
    if ($btnSubmit->SubmittedValue == "Submit" || $fproc->GetPostVar("pgSelectedPage") != ''||$btnExportCSV->SubmittedValue == "Export to CSV")
    {
        $total_tickets = $TMTickets->GetTotalTickets($ddlgametype->SubmittedValue,$ddlcardvalue->SubmittedValue);
        $total_winnings = $TMWinnings->GetTotalWinningCardsv2($ddlcardvalue->SubmittedValue,$ddlgamename->SubmittedValue,$ddlgametype->SubmittedValue);        
        $totalwinnings = $total_winnings[0]["Total_Winning"];

        $total_prizes = $TMWinnings->GetTotalPrize2($ddlcardvalue->SubmittedValue,$ddlgamename->SubmittedValue,$ddlgametype->SubmittedValue);        
        
        //convert KHR in total prizes  
        $totaldollar = $total_prizes[0]['Prize_TotalRedeem'];
        $totalKHR = $total_prizes[0]['Prize_TotalKHR'];
        $nocomma =  str_replace( ',', '',$totalKHR);
        $getcurrency =  str_replace( ' ', '',$nocomma);
        
        //convert to KHR to dollar
        $khrcurrency = convertCurrtodollar($getcurrency);
        $total_prizes = number_format($khrcurrency , 0 , "" , ",");
        $total_prizes += $totaldollar;
        $totalprizes = $total_prizes;
        
        //convert $total_prize_redeem start
        $total_prize_redeem = $TMWinnings->GetPrizeRedeemed2($ddlcardvalue->SubmittedValue,$ddlgamename->SubmittedValue,$ddlgametype->SubmittedValue);
        $totaldollar1 = $total_prize_redeem[0]['Prize_RedeemDollar'];
        $nocomma1 =  str_replace( ',', '',$total_prize_redeem[0]['Prize_RedeemKHR']);
        $getcurrency1 =  str_replace( ' ', '',$nocomma1);
        $khrcurrency1 = convertCurrtodollar($getcurrency1);
        $tmptotal = number_format($khrcurrency1 , 0 , "" , ",");
        $tmptotal += $totaldollar1;
        $total_prize_redeem_converted = $tmptotal; 
        //convert $total_prize_redeem end
        
        //convert $total_prize_left start
        $total_prize_left = $TMWinnings->GetPrizeLeft2($ddlcardvalue->SubmittedValue,$ddlgamename->SubmittedValue,$ddlgametype->SubmittedValue);
        $totaldollar2 = $total_prize_left[0]['Prize_LeftDollar'];
        $nocomma2 =  str_replace( ',', '',$total_prize_left[0]['Prize_leftKHR']);
        $getcurrency2 =  str_replace( ' ', '',$nocomma2);
        $khrcurrency2 = convertCurrtodollar($getcurrency2);
        $tmptotal1 = number_format($khrcurrency2 , 0 , "" , ",");
        $tmptotal1 += $totaldollar2;
        $total_prize_left_converted = $tmptotal1; 
        
        //convert $total_prize_left end  
        $percentage = round((($total_prize_redeem_converted / $totalprizes) * 100) , 4);
        $arrWinnings = $TMWinnings->SelectAllPrizes($ddlcardvalue->SubmittedValue,$ddlgamename->SubmittedValue, $ddlgametype->SubmittedValue, $cboPrizeType->SubmittedValue);
        $reccount = count($arrWinnings);

        if ($btnSubmit->SubmittedValue == "Submit"){$pgcon->SelectedPage = 1;}
        $pgcon->Initialize($itemsperpage, $reccount);
        $pgTransactionHistory = $pgcon->PreRender();
        $arrWinnings = $TMWinnings->SelectAllPrizesWithLimit($ddlcardvalue->SubmittedValue,$ddlgamename->SubmittedValue, $ddlgametype->SubmittedValue, $cboPrizeType->SubmittedValue, $pgcon->SelectedItemFrom-1, $itemsperpage);
        $allprize_list = new ArrayList();
        $allprize_list->AddArray($arrWinnings);
        $arrWinnings2 = $TMWinnings->SelectAllClaimed($ddlcardvalue->SubmittedValue,$ddlgamename->SubmittedValue,$ddlgametype->SubmittedValue, $cboPrizeType->SubmittedValue);
        $allprize_list2 = new ArrayList();
        $allprize_list2->AddArray($arrWinnings2);
          
        for ($i = 0; $i < count($arrWinnings); $i++)
        {
                if($arrWinnings[$i]['PrizeName'] == 'BIKE')
                {
                        $allprize_list[$i]['PrizeName'] = $arrWinnings[$i]['BIKE'];
                }

                if($arrWinnings[$i]['NewPrizeDescDollar'] != 0)
                {
                        $allprize_list[$i]['PrizeName'] = $arrWinnings[$i]['PrizeName'];
                }

                if($arrWinnings[$i]['NewPrizeDescKHR'] != 0)
                {
                        //convert $arrWinnings start
                        $nocomma3 =  str_replace( ',', '',$arrWinnings[$i]['PrizeName']);
                        $getcurrency3 =  str_replace( ' ', '',$nocomma3);
                        $khrcurrency3 = convertCurrtodollar($getcurrency3);
                        $allprize_list[$i]['PrizeName'] = '$'.round($khrcurrency3,5);
                        //convert $arrWinnings end
                }
        }
              
              
        //start
        for ($i = 0; $i < count($arrWinnings2); $i++)
        {
            //replace comma and Spaces
            $nocomma =  str_replace( ',', '',$arrWinnings2[$i]['PrizeName']);
            $getcurrency =  str_replace( ' ', '',$nocomma);
            
            // Check for currency
            if(substr($getcurrency, 0,1) == '$') //dollar
            {
                    //replace dollar and conver to integer
                    $dollarcurrency =  (int)str_replace( '$', '',$getcurrency);
                    $allprize_list2[$i]['PrizeName'] += number_format($dollarcurrency);  
            } 
            else if(substr($getcurrency, 0,3) == 'KHR')//for KHR
            {
                    //convert to KHR to dollar
                    $khrcurrency = convertCurrtodollar($getcurrency);
                    $allprize_list2[$i]['PrizeName'] += number_format($khrcurrency);
            }
            else
            {
                    $allprize_list2[$i]['PrizeName'] = $arrWinnings2[$i]['PrizeName'];
            }
        }
        //end
        
      
    if($btnExportCSV->SubmittedValue == "Export to CSV")
    {     
        $arrWinnings = $TMWinnings->SelectAllPrizes2($ddlcardvalue->SubmittedValue,$ddlgamename->SubmittedValue, $ddlgametype->SubmittedValue, $cboPrizeType->SubmittedValue, $pgcon->SelectedItemFrom-1, $itemsperpage);
        $allprize_list = new ArrayList();
        $allprize_list->AddArray($arrWinnings);
          
        for ($i = 0; $i < count($arrWinnings); $i++)
        {
                if($arrWinnings[$i]['PrizeName'] == 'BIKE')
                {
                        $allprize_list[$i]['PrizeName'] = $arrWinnings[$i]['BIKE'];
                }

                if($arrWinnings[$i]['NewPrizeDescDollar'] != 0)
                {
                        $allprize_list[$i]['PrizeName'] = $arrWinnings[$i]['PrizeName'];
                }

                if($arrWinnings[$i]['NewPrizeDescKHR'] != 0)
                {
                        //convert $arrWinnings start
                        $nocomma3 =  str_replace( ',', '',$arrWinnings[$i]['PrizeName']);
                        $getcurrency3 =  str_replace( ' ', '',$nocomma3);
                        $khrcurrency3 = convertCurrtodollar($getcurrency3);
                        $allprize_list[$i]['PrizeName'] = '$'.round($khrcurrency3,5);
                        //convert $arrWinnings end
                }
        }
        
            $arrWC = null;
            for($ctr=0; $ctr < count($allprize_list); $ctr++){
                $arrWC[$ctr]["CP"] = $allprize_list[$ctr]['CardPrice'];
                $arrWC[$ctr]["GI"] = $allprize_list[$ctr]['GameNumber'];
                $arrWC[$ctr]["PN"] = str_replace(',','',$allprize_list[$ctr]['PrizeName']);
                $arrWC[$ctr]["GN"] = $allprize_list[$ctr]['ProductName'];
                $arrWC[$ctr]["TNA"] = $allprize_list[$ctr]['TotalAlloted'];
                $tempGameID = $allprize_list[$ctr]['GameID'];
                // Check if there are claimed prizes..
                for($ctr2 = 0; $ctr2 < count($allprize_list2); $ctr2++){
                    if ($tempGameID == $allprize_list2[$ctr2]['GameID'] && $allprize_list[$ctr]["PrizeID"] == $allprize_list2[$ctr2]['PrizeID'])
                    {
                        $arrWC[$ctr]["TNC"] = $allprize_list2[$ctr2]['TotalClaimed'];
                        $arrWC[$ctr]["RNP"] = $allprize_list[$ctr]['TotalAlloted'] - $allprize_list2[$ctr2]['TotalClaimed'];
                    }
                }
            }
      
            $data1 = "";
            $data2 = "";
            $fp = fopen("../csv/Prize_History.csv","w");
            if($fp)
            {
                    $header1 = "Batch Size,Total Winning Entries,Total Prizes,Prize Left,Prize Redeemed,%\r\n";
                    fwrite($fp,$header1);

                            $data1 .= $total_tickets[0]["Total_Tickets"] . "," . $total_winnings[0]["Total_Winning"] . "," . $total_prizes . "," . $total_prize_left_converted . "," . $total_prize_redeem_converted .",".$percentage. "\r\n\n";

                    fwrite($fp,$data1);

                    $header2 = "Card Value,Game Name,Game Number,Prize Type,Total Quantity of Prize,Total Quantity of Prize Used,Prize Balance,Percentage\r\n";
                    fwrite($fp,$header2);

                    if(count($allprize_list) > 0)
                    {
                            for($ctr=0; $ctr < count($allprize_list); $ctr++)
                            {
                                if($arrWC[$ctr]["PN"] == 'BIKE')
                                { 
                                    $tmparr[$ctr]["PN"] =  $arrWC[$ctr]["PN"];
                                }else{
                                    $tmparr[$ctr]["PN"] =  $arrWC[$ctr]["PN"];
                                }
                                if ($arrWC[$ctr]["TNC"] == ""){
                                    $arrWC[$ctr]["TNC"] = 0;
                                }
                                if ($arrWC[$ctr]["RNP"] == ""){
                                    $tmparr[$ctr]["tmp_RNP"] = $arrWC[$ctr]["TNA"];
                                } else {
                                    $tmparr[$ctr]["tmp_RNP"] = $arrWC[$ctr]["RNP"];
                                }

                                if ($arrWC[$ctr]["TNC"] == ""){
                                    $used =  0;
                                } else {
                                    $used = $arrWC[$ctr]["TNC"];
                                }

                                $tmparr[$ctr]["used"] = $arrWC[$ctr]["TNA"]-$tmparr[$ctr]["tmp_RNP"];
                                $totalquan = $arrWC[$ctr]["TNA"];
                                $tmparr[$ctr]["tqty"] = round((($used / $totalquan) * 100) , 4)  . "%";

                                $data2 .= $arrWC[$ctr]["CP"] . "," .$arrWC[$ctr]["GN"]. "," . $arrWC[$ctr]["GI"] . "," . $tmparr[$ctr]["PN"] . "," . $arrWC[$ctr]["TNA"]
                                    . "," . $tmparr[$ctr]["used"] . "," . $tmparr[$ctr]["tmp_RNP"] . "," . $tmparr[$ctr]["tqty"] . "\r\n";
                            } 
                    }
                    else
                    {
                            $data2 .= "No Records Found\n";
                    }
            
            fwrite($fp,$data2);
            header('Content-type: text/csv');
            header("Content-Disposition: attachment; filename=".'Prize_History.csv');
            header('Pragma: public');
            readfile('../csv/Prize_History.csv');
            exit;
        }
        else
        {
                echo "<script>alert('Cannot create file.')</script>";
        }
    } //sa if          
}//end dont delete
    
	/*
     * Added by: Arlene R. Salazar
     * Date: Feb 29, 2012
     */	
	/*
    if($btnExportCSV->SubmittedValue == "Export to CSVXX")
    {
        $total_tickets = $TMTickets->GetTotalTickets($ddlgametype->SubmittedValue,$ddlcardvalue->SubmittedValue);
        
        //$total_winnings = $TMWinnings->GetTotalWinningCards($ddlgametype->SubmittedValue);        
        $total_winnings = $TMWinnings->GetTotalWinningCardsv2($ddlcardvalue->SubmittedValue,$ddlgamename->SubmittedValue,$ddlgametype->SubmittedValue);        
        //$total_prizes = $TMWinnings->GetTotalPrize($ddlgametype->SubmittedValue);
        $total_prizes = $TMWinnings->GetTotalPrize2($ddlcardvalue->SubmittedValue,$ddlgamename->SubmittedValue,$ddlgametype->SubmittedValue);        
        //$total_prize_redeem = $TMWinnings->GetPrizeRedeemed($ddlgametype->SubmittedValue);
        $total_prize_redeem = $TMWinnings->GetPrizeRedeemed2($ddlcardvalue->SubmittedValue,$ddlgamename->SubmittedValue,$ddlgametype->SubmittedValue);
        //$total_prize_left = $TMWinnings->GetPrizeLeft($ddlgametype->SubmittedValue);
        $total_prize_left = $TMWinnings->GetPrizeLeft2($ddlcardvalue->SubmittedValue,$ddlgamename->SubmittedValue,$ddlgametype->SubmittedValue);
        
//convert $total_prize_redeem start
        $totaldollar = $total_prize_redeem[0]['Prize_RedeemDollar'];
        $totalKHR = $total_prize_redeem[0]['Prize_RedeemKHR'];
        $nocomma =  str_replace( ',', '',$total_prize_redeem[0]['Prize_RedeemKHR']);
        $getcurrency =  str_replace( ' ', '',$nocomma);
        //convert to KHR to dollar
        $khrcurrency = convertCurrtodollar($getcurrency);
        $tmptotal = number_format($khrcurrency , 0 , "" , ",");
       
        $tmptotal += $totaldollar;
                  
        $total_prize_redeem_converted = $tmptotal; 
        //$total_prize_redeem = $total_winnings[0]["Total_Winning"];
        
//convert $total_prize_redeem end
        
        $totaldollar = $total_prizes[0]['Prize_TotalRedeem'];
        $totalKHR = $total_prizes[0]['Prize_TotalKHR'];
        $nocomma =  str_replace( ',', '',$total_prize_redeem[0]['Prize_RedeemKHR']);
        $getcurrency =  str_replace( ' ', '',$nocomma);
        //convert to KHR to dollar
        $khrcurrency = convertCurrtodollar($getcurrency);
        $total_prizes = number_format($khrcurrency , 0 , "" , ",");
       
        $total_prizes += $totaldollar;
                  
        $totalprizes = $total_prizes; 
        $totalwinnings = $total_winnings[0]["Total_Winning"];
        //$total_prize_left = $totalprizes - $totalwinnings;
        
       
        
//        if ($cboPrizeType->SubmittedValue != 0)
//        {
//            $arrWinnings = $TMWinnings->SelectPrizes($ddlgametype->SubmittedValue, $cboPrizeType->SubmittedValue);
//            $prize_list = new ArrayList();
//            $prize_list->AddArray($arrWinnings);
//            $arrClaimed = $TMWinnings->SelectTotalClaimedPrizes($ddlgametype->SubmittedValue, $cboPrizeType->SubmittedValue);
//            $claim_list = new ArrayList();
//            $claim_list->AddArray($arrClaimed);
//        } else {
        
       $totaldollar = $total_prize_redeem[0]['Prize_RedeemDollar'];
       $totalKHR = $total_prize_redeem[0]['Prize_RedeemKHR'];
      
          $nocomma =  str_replace( ',', '',$total_prize_redeem[0]['Prize_RedeemKHR']);
             $getcurrency =  str_replace( ' ', '',$nocomma);
                //convert to KHR to dollar
                $khrcurrency = convertCurrtodollar($getcurrency);
                $total_prize_redeem = number_format($khrcurrency , 0 , "" , ",");
      
          $total_prize_redeem += $totaldollar;
       
        $percentage = round((($total_prize_redeem / $total_prize_left[0]["Prize_Left"]) * 100) , 4);
        
		//1st table
//		$total_tickets = $TMTickets->GetTotalTickets($ddlgametype->SubmittedValue,$ddlBatchID->SubmittedValue);
//		$total_winnings = $TMWinnings->GetTotalWinningCards($ddlgametype->SubmittedValue,$ddlBatchID->SubmittedValue);
//		$total_prizes = $TMWinnings->GetTotalPrize($ddlgametype->SubmittedValue,$ddlBatchID->SubmittedValue);
//		$total_prize_redeem = $TMWinnings->GetPrizeRedeemed($ddlgametype->SubmittedValue,$ddlBatchID->SubmittedValue);
//		$total_prize_left = $TMWinnings->GetPrizeLeft($ddlgametype->SubmittedValue,$ddlBatchID->SubmittedValue);
//		$percentage = round((($total_prize_redeem[0]["Prize_Redeem"] / $total_prize_left[0]["Prize_Left"]) * 100) , 4);
		for($ctr=0; $ctr <1; $ctr++)
		{
			$arrTotal[$ctr]["TLT"] = $total_tickets[$ctr]["Total_Tickets"];
			$arrTotal[$ctr]["TW"] = $total_winnings[$ctr]["Total_Winning"];
			$arrTotal[$ctr]["PT"] = $total_prizes;
			$arrTotal[$ctr]["PL"] = $total_prize_left[0]["Prize_Left"];
			$arrTotal[$ctr]["PR"] = $total_prize_redeem;
			$arrTotal[$ctr]["P"] = $percentage;
		
		}
		$cancel_list = new ArrayList();
		$cancel_list->AddArray($arrTotal);

		// 2nd table 
		$arrWinnings = $TMWinnings->SelectAllPrizesWithLimit($ddlgamename->SubmittedValue, $ddlgametype->SubmittedValue, $cboPrizeType->SubmittedValue, $pgcon->SelectedItemFrom-1, $itemsperpage,$ddlBatchID->SubmittedValue);
		$allprize_list = new ArrayList();
		$allprize_list->AddArray($arrWinnings);
		$arrWinnings2 = $TMWinnings->SelectAllClaimed($ddlgametype->SubmittedValue, $cboPrizeType->SubmittedValue,$ddlBatchID->SubmittedValue);
		$allprize_list2 = new ArrayList();
		$allprize_list2->AddArray($arrWinnings2);


                

		$arrWC = null;
		for($ctr=0; $ctr < count($allprize_list); $ctr++){
                        $arrWC[$ctr]["CP"] = $allprize_list[$ctr]['CardPrice'];
			$arrWC[$ctr]["GB"] = $allprize_list[$ctr]['BatchID'];
			$arrWC[$ctr]["GI"] = $allprize_list[$ctr]['GameNumber'];
//                        $arrWC[$ctr]["PN"] = $allprize_list[$ctr]['PrizeName'];
                        //
                          $nocomma =  str_replace( ',', '',$allprize_list[$ctr]['PrizeName']);
                            $getcurrency =  str_replace( ' ', '',$nocomma);
            // Check for currency
            if(substr($getcurrency, 0,1) == '$') //dollar
            {
               
                        //replace dollar and conver to integer
                        $dollarcurrency =  (int)str_replace( '$', '',$getcurrency);
                        $arrWC[$ctr]["PN"] += $dollarcurrency;  
            } 
            else if(substr($getcurrency, 0,3) == 'KHR')//for KHR
                {
                        //convert to KHR to dollar
                        $khrcurrency = convertCurrtodollar($getcurrency);
                        $arrWC[$ctr]["PN"] += $khrcurrency;
                }  else {
                  $arrWC[$ctr]["PN"] =  $allprize_list[$ctr]['PrizeName'];
                        }
                        //
			$arrWC[$ctr]["GN"] = $allprize_list[$ctr]['ProductName'];
			$arrWC[$ctr]["TNA"] = $allprize_list[$ctr]['TotalAlloted'];
			$arrWC[$ctr]["TA"] = ($allprize_list[$ctr]['TotalAlloted']);
			$tempGameID = $allprize_list[$ctr]['GameID'];
			// Check if there are claimed prizes..
                        
                          //start
        for ($i = 0; $i < count($arrWinnings2); $i++)
        {
             //replace comma and Spaces
             $nocomma =  str_replace( ',', '',$arrWinnings2[$i]['PrizeName']);
             $getcurrency =  str_replace( ' ', '',$nocomma);
            // Check for currency
            if(substr($getcurrency, 0,1) == '$') //dollar
            {
               
                        //replace dollar and conver to integer
                        $dollarcurrency =  (int)str_replace( '$', '',$getcurrency);
                        $allprize_list2[$i]['PrizeName'] += $dollarcurrency;  

            } 
            else if(substr($getcurrency, 0,3) == 'KHR')//for KHR
                {
                      
                        //convert to KHR to dollar
                        $khrcurrency = convertCurrtodollar($getcurrency);
                        $allprize_list2[$i]['PrizeName'] += $khrcurrency;
                   

                }
            
        }//end
                        //end
			for($ctr2 = 0; $ctr2 < count($allprize_list2); $ctr2++)
			{
				if ($tempGameID == $allprize_list2[$ctr2]['GameID'] && $arrWC[$ctr]["PN"] == $allprize_list2[$ctr2]['PrizeName'])
				{               
//					$arrWC[$ctr]["TNC"] = $allprize_list2[$ctr2]['TotalClaimed'];   
					$arrWC[$ctr]["RNP"] = ($allprize_list[$ctr]['TotalAlloted']) -($allprize_list2[$ctr2]['TotalClaimed']);                  
				}
                                
			}
		}

		for($ctr=0; $ctr<count($arrWC); $ctr++)
		{       
                        if($allprize_list2[$ctr]['Status']==2 ||$allprize_list2[$ctr]['Status']==5)
                        {
                            $arrWC[$ctr]["TNC"] = $allprize_list2[$ctr]['TotalClaimed'];
                        }
                        
			if($arrWC[$ctr]['TNC'] == '')
			{
				$arrWC[$ctr]['TNC'] = '0';              
			}            
		   
			if($arrWC[$ctr]['RNP'] == '')
			{
				$arrWC[$ctr]['RNP'] = $arrWC[$ctr]['TNA'];
			}
	
			if ($arrWC[$ctr]["TNC"] == '')
			{
				$used =  0;
			} 
			else
			{
				$used = $arrWC[$ctr]["TNC"];
			 }                
			$totalquan = $arrWC[$ctr]["TA"];            
			$try = $used / $totalquan;
			$pt = round((($used / $totalquan) * 100),4);             
			$arrWC[$ctr]['PT'] = $pt.'%';
		}

		$list = new ArrayList();
		$list->AddArray($arrWC);

		$data1 = "";
		$data2 = "";
		$fp = fopen("../csv/Prize_History.csv","w");
		if($fp)
		{
			$header1 = "Batch Size,Total Winning Entries,Total Prizes,Prize Left,Prize Redeemed,%\r\n";
			fwrite($fp,$header1);

			for($i = 0 ; $i < count($cancel_list) ; $i++)
			{
				$data1 .= $cancel_list[$i]["TLT"] . "," . $cancel_list[$i]["TW"] . "," . $cancel_list[$i]["PT"] . "," . $cancel_list[$i]["PL"] . "," . $cancel_list[$i]["PR"] . "," . $cancel_list[$i]["P"] . "\r\n\n";
			}
			fwrite($fp,$data1);
			
			$header2 = "Card Value,Game Name,Game Number,Prize Type,Total Quantity of Prize,Total Quantity of Prize Used,Prize Balance,Percentage\r\n";
			fwrite($fp,$header2);

			if(count($list) > 0)
			{
				for($i = 0 ; $i < count($list) ; $i++)
				{
                                     if($list[$i]['PN'] == 'BIKE')
                                    {
                                         $prizenme1 = $list[$i]['PN'];
                                     }  else {
                                        $prizenme1 = '$'.$list[$i]['PN'];
                                                }
					$data2 .= $list[$i]['CP'] . "," .$list[$i]['GN'] . "," . $list[$i]['GI'] . "," . $prizenme1 . "," . $list[$i]['TNA'] . "," . $list[$i]['TNC'] . "," . $list[$i]['RNP'] . "," . $list[$i]['PT'] . "\r\n";
				}
			}
			else
			{
				$data2 .= "No Records Found\n";
			}
			fwrite($fp,$data2);
			header('Content-type: text/csv');
	        header("Content-Disposition: attachment; filename=".'Prize_History.csv');
			header('Pragma: public');
	        readfile('../csv/Prize_History.csv');
	        exit;
		}
		else
		{
			echo "<script>alert('Cannot create file.')</script>";
		}
	}*/
    /*Added by: Arlene R. Salazar */ 
}
/*****************Functions  *************/
function convertCurrtodollar($currencyMoney)
{
        App::LoadModuleClass("TicketManagementCM", "TMCurrencies");
        $TMcurrencies = new TMCurrencies();

        $money =  str_replace( 'KHR', '',$currencyMoney);
        $moneycurrency = 'KHR';

        $getconvertmoney = $TMcurrencies->SelectByCurrencySymbol($moneycurrency);
        $fixedrate = $getconvertmoney[0]['FixedRate'];

        if(is_numeric($fixedrate) && is_numeric($money))
        {
                $truevalue = ($money/$fixedrate);
        }
        return $truevalue;
}
 /*****************Functions  *************/ 
?>