<?php
/*
 * Author: Noel Antonio
 * Date Created: 2011-09-05
 * Company: Philweb Corporation
 */
$pagesubmenuid = 12;

App::LoadModuleClass("TicketManagementCM", "TMGameManagement");
App::LoadModuleClass("TicketManagementCM", "TMWinnings");
App::LoadModuleClass("TicketManagementCM", "TMProducts");
App::LoadModuleClass("TicketManagementCM", "TMTickets");
App::LoadControl("TextBox");
App::LoadControl("Button");
App::LoadControl("ComboBox");
App::LoadControl("PagingControl2");
App::LoadControl("Hidden");
$fproc = new FormsProcessor();
$TMWinnings = new TMWinnings();
$TMGames = new TMGameManagement();
$TMProducts = new TMProducts();
$TMTickets = new TMTickets();

$cboGameID = new ComboBox("cboGameID", "cboGameID", "Game Number : ");
$cboGameID->ShowCaption = true;
$options = null;
$options[] = new ListItem("Please Select", "0");
$cboGameID->Items = $options;
//$cboGameID->Args = "onchange = 'javascript: return get_prizetype();'";
$cboGameID->Args = "onchange='javascript: get_batchid(); get_gamename();'";
//$cboGameID->Args = "onchange = 'javascript: return SelectGameName();'";
$arrGames = $TMGames->SelectAllActive();
$gameList = new ArrayList();
$gameList->AddArray($arrGames);
$cboGameID->DataSource = $gameList;
$cboGameID->DataSourceText = "GameNumber";
$cboGameID->DataSourceValue = "GameID";
$cboGameID->DataBind();

$cboPrizeType = new ComboBox("cboPrizeType", "cboPrizeType", "Prize Type : ");
$cboPrizeType->ShowCaption = true;
$options = null;
//$options[] = new ListItem("- - - - - - - - - - - - - -","",true);
//$options[] = new ListItem("- - - - - - - - - - - - - -","0");
$options[] = new ListItem("Please Select","");
$cboPrizeType->Items = $options;

$txtGameName = new TextBox("txtGameName", "txtGameName", "Game Name : ");
$txtGameName->ShowCaption = true;
$txtGameName->Enabled = false;
$txtGameName->Style = "text-align: center";
$txtGameName->Args = "size='15'";

$hidPrizeTypeID = new Hidden("hidPrizeTypeID", "hidPrizeTypeID","");
$hidGameTypeID = new Hidden("hidGameTypeID", "hidGameTypeID", "");

$btnSubmit = new Button("btnSubmit", "btnSubmit", "Submit");
$btnSubmit->IsSubmit = true;
$btnSubmit->Args = "onclick='javascript: return validate();'";

//get list of game names
$gamenames = $TMProducts->SelectAllGameName();
$gamename_list = new ArrayList();
$gamename_list->AddArray($gamenames);

$ddlgamename = new ComboBox("ddlgamename","ddlgamename","Game Name: ");
$ddlgamename->Args = "onchange = 'javascript: get_gamenumber();'";
$gamenameopt = null;
$gamenameopt[] = new ListItem("Please select", "", true);
$ddlgamename->Items = $gamenameopt;
$ddlgamename->DataSource = $gamename_list;
$ddlgamename->DataSourceText = "ProductName";
$ddlgamename->DataSourceValue = "ProductID";
$ddlgamename->DataBind();
$ddlgamename->ShowCaption = true;

$ddlBatchID = new ComboBox("ddlBatchID","ddlBatchID","Game Batch: ");
$gamebatchopt = null;
$gamebatchopt[] = new ListItem("Please select", "0", true);
$ddlBatchID->Items = $gamebatchopt;
$ddlBatchID->ShowCaption = true;
$ddlBatchID->Args = "onchange='javascript: get_prizetype();'";

$hiddenflag = new Hidden("hiddenflag","hiddenflag","Hidden Flag");

$itemsperpage = 50;
$pgcon = new PagingControl2($itemsperpage, 1);
$pgcon->URL = "javascript:ChangePage(%currentpage);";
$pgcon->ShowMoveToFirstPage = true;
$pgcon->ShowMoveToLastPage = true;
$pgcon->PageGroup = 5;

$fproc->AddControl($cboPrizeType);
$fproc->AddControl($cboGameID);
$fproc->AddControl($txtGameName);
$fproc->AddControl($hidGameTypeID);
$fproc->AddControl($hidPrizeTypeID);
$fproc->AddControl($btnSubmit);
$fproc->AddControl($ddlgamename);
$fproc->AddControl($ddlBatchID);
$fproc->AddControl($hiddenflag);
$fproc->ProcessForms();

if ($fproc->IsPostBack)
{
    $arrOperators = $TMWinnings->SelectPrizeType($cboGameID->SubmittedValue,$ddlBatchID->SubmittedValue);//echo $cboPrizeType->SubmittedValue;exit();
    $prizetype_list = new ArrayList();
    $prizetype_list->AddArray($arrOperators);
    $options = null;
    $options[] = new ListItem("ALL","0");
    $cboPrizeType->Items = $options;
    $cboPrizeType->DataSource = $prizetype_list;
    $cboPrizeType->DataSourceText = "Description";
    $cboPrizeType->DataSourceValue = "PrizeID";
    $cboPrizeType->DataBind();
    $cboPrizeType->SetSelectedValue($cboPrizeType->SubmittedValue);
    
	if($hiddenflag->SubmittedValue == 1)
    {
        $cboGameID->Args = "onchange='javascript: get_batchID(); '";
        $cboGameID->ClearItems();

        $gamenumbers = $TMProducts->SelectGameNumPerGameName($ddlgamename->SubmittedValue);

        $gamenumbers_list1 = new ArrayList();
        $gamenumbers_list1->AddArray($gamenumbers);

        $litemgmetype1 = null;
        $litemgmetype1[] = new ListItem("Please select", "0", false);
        $ddlgametype->Items = $litemgmetype1;

        $cboGameID->ClearItems();
        $gamenameopt = null;
        $gamenameopt[] = new ListItem("Please select", "0", true);
        $cboGameID->Items = $gamenameopt;
        $gamenumbers = $TMProducts->SelectGameNumPerGameName($ddlgamename->SubmittedValue);
        $gamename_list = new ArrayList();
        $gamename_list->AddArray($gamenumbers);
        $cboGameID->DataSource = $gamename_list;
        $cboGameID->DataSourceText = "GameNumber";
        $cboGameID->DataSourceValue = "GameID";
        $cboGameID->DataBind();
        $cboGameID->SetSelectedValue($cboGameID->SubmittedValue);
    }

    if($hiddenflag->SubmittedValue == 2)
    {
        $ddlgamename->Args = "onchange='javascript:'";
        $ddlgamename->ClearItems();

        $gamenames = $TMProducts->SelectGameNamePerGameNum($cboGameID->SubmittedValue);

        $gamename_list1 = new ArrayList();
        $gamename_list1->AddArray($gamenames);
        $litemgmetype1 = null;
        $litemgmetype1[] = new ListItem("Please select", "0", false);
        $ddlgamename->Items = $litemgmetype1;

        $ddlgamename->DataSource = $gamename_list1;
        $ddlgamename->DataSourceText = "ProductName";
        $ddlgamename->DataSourceValue = "ProductID";
        $ddlgamename->DataBind();

        $ddlgamename->SetSelectedValue($ddlgamename->SubmittedValue);
    }

	$gamebatches = $TMProducts->SelectBatchIDPerGameNum($cboGameID->SubmittedValue,2);
    $gamebatches_list = new ArrayList();
    $gamebatches_list->AddArray($gamebatches);
    $ddlBatchID->DataSource = $gamebatches_list;
    $ddlBatchID->DataSourceText = "BatchID";
    $ddlBatchID->DataSourceValue = "GameBatchID";
    $ddlBatchID->DataBind();
    $ddlBatchID->SetSelectedValue($ddlBatchID->SubmittedValue);

    if ($btnSubmit->SubmittedValue == "Submit" || $fproc->GetPostVar("pgSelectedPage") != '')
    {
        $total_tickets = $TMTickets->GetTotalTickets($cboGameID->SubmittedValue,$ddlBatchID->SubmittedValue);
        $total_winnings = $TMWinnings->GetTotalWinningCards($cboGameID->SubmittedValue,$ddlBatchID->SubmittedValue);
        $total_prizes = $TMWinnings->GetTotalPrize($cboGameID->SubmittedValue,$ddlBatchID->SubmittedValue);
        $total_prize_redeem = $TMWinnings->GetPrizeRedeemed($cboGameID->SubmittedValue,$ddlBatchID->SubmittedValue);
        $total_prize_left = $TMWinnings->GetPrizeLeft($cboGameID->SubmittedValue,$ddlBatchID->SubmittedValue);
        $percentage = round((($total_prize_redeem[0]["Prize_Redeem"] / $total_prize_left[0]["Prize_Left"]) * 100) , 4);
        //$percentage = round(0.009996501,2);
        if ($cboGameID->SubmittedValue != '0' && $cboPrizeType->SubmittedValue != '0'){
            $arrWinnings = $TMWinnings->SelectPrizes($cboGameID->SubmittedValue, $cboPrizeType->SubmittedValue,$ddlBatchID->SubmittedValue);
            $prize_list = new ArrayList();
            $prize_list->AddArray($arrWinnings);
            $arrClaimed = $TMWinnings->SelectTotalClaimedPrizes($cboGameID->SubmittedValue, $cboPrizeType->SubmittedValue,$ddlBatchID->SubmittedValue);
            $claim_list = new ArrayList();
            $claim_list->AddArray($arrClaimed);
        }
        else if ($cboGameID->SubmittedValue == '0' && $cboPrizeType->SubmittedValue == '0')
        {
            
        }
        else{
            $arrWinnings = $TMWinnings->SelectAllPrizes($cboGameID->SubmittedValue, $cboPrizeType->SubmittedValue,$ddlBatchID->SubmittedValue);
            $reccount = count($arrWinnings);
            if ($btnSubmit->SubmittedValue == "Submit"){$pgcon->SelectedPage = 1;}
            $pgcon->Initialize($itemsperpage, $reccount);
            $pgTransactionHistory = $pgcon->PreRender();
            $arrWinnings = $TMWinnings->SelectAllPrizesWithLimit($cboGameID->SubmittedValue, $cboPrizeType->SubmittedValue, $pgcon->SelectedItemFrom-1, $itemsperpage,$ddlBatchID->SubmittedValue);
            $allprize_list = new ArrayList();
            $allprize_list->AddArray($arrWinnings);
            $arrWinnings2 = $TMWinnings->SelectAllClaimed($cboGameID->SubmittedValue, $cboPrizeType->SubmittedValue,$ddlBatchID->SubmittedValue);
            $allprize_list2 = new ArrayList();
            $allprize_list2->AddArray($arrWinnings2);
        }
    }
    
    /*if ($cboGameID->SubmittedValue != 0)
    {
        $arrgames = $TMProducts->SelectGameNameByID($cboGameID->SubmittedValue);
        if ($arrgames > 0)
        { 
            $txtGameName->Text = $arrgames[0]["ProductName"];
            $cboPrizeType->ClearItems();
            $options = null;
            $options[] = new ListItem("ALL","0");
            $cboPrizeType->Items = $options;
            $arrOperators = $TMWinnings->SelectPrizeType($cboGameID->SubmittedValue);
            $cboPrizeType->SetSelectedValue($_GET['cboPrizeType']);
            $OperatorList = new ArrayList();
            $OperatorList->AddArray($arrOperators);
            $cboPrizeType->DataSource = $OperatorList;
            $cboPrizeType->DataSourceText = "Description";
            $cboPrizeType->DataSourceValue = "PrizeID";
            $cboPrizeType->DataBind();
            $fproc->ProcessForms();
        }
    } */
//    else if(isset($_GET['changecbo'])) 
//    { 
//        $txtGameName->Text = $arrgames[0]["ProductName"];
//        $options = null;
//        $options[] = new ListItem("ALL","0");
//        $cboPrizeType->Items = $options;
//        $arrOperators = $TMWinnings->SelectPrizeType($cboGameID->SubmittedValue);
//        $OperatorList = new ArrayList();
//        $OperatorList->AddArray($arrOperators);
//        $cboPrizeType->DataSource = $OperatorList;
//        $cboPrizeType->DataSourceText = "Description";
//        $cboPrizeType->DataSourceValue = "PrizeID";
//        $cboPrizeType->DataBind();
//    }
}
?>


