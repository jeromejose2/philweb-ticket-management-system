<?php
/*
 * @author 
 * Purpose   : controller for productlist
 */
$pagesubmenuid = 23;
$stylesheets[] = "../css/default.css";
App::LoadModuleClass("TicketManagementCM", "TMProducts");
App::LoadModuleClass("TicketManagementCM", "TMProvider");

App::LoadControl("Hidden");
App::LoadControl("Button");
App::LoadControl("ComboBox");
App::LoadControl("PagingControl2");

$fproc = new FormsProcessor();
$tmproduct = new TMProducts();
$tmprovider = new TMProvider();

$hiddenprodid = new Hidden("hiddenprodid","hiddenprodid","Hidden Product ID");

/*PAGING*/
$itemsperpage = 5;
$pgcon = new PagingControl2($itemsperpage, 1);
$pgcon->URL = "javascript:ChangePage(%currentpage);";
$pgcon->ShowMoveToFirstPage = true;
$pgcon->ShowMoveToLastPage = true;
/*PAGING*/

if(isset($_SESSION['prodaccttype']))
{
    if($_SESSION['prodaccttype'] == 0)
        $where = " ORDER BY pd.ProductName ASC";
    else
        $where = " WHERE pv.ProviderID = " . $_SESSION['prodaccttype'] . " ORDER BY pd.ProductName ASC";
    $products = $tmproduct->SelectProductByWhere($where);
    $arrproducts = $tmproduct->SelectProductByWhereWithLimit(($pgcon->SelectedItemFrom - 1), $itemsperpage,$where);
}
else
{
    $products = $tmproduct->SelectAll();
    $arrproducts = $tmproduct->SelectAllWithLimit(($pgcon->SelectedItemFrom - 1), $itemsperpage);
}
$productcount = count($products);
$pgcon->Initialize($itemsperpage, $productcount);
$pgTransactionHistory = $pgcon->PreRender();
$product_list = new ArrayList();
$product_list->AddArray($arrproducts);

$where = " WHERE Status = 1 ORDER BY Name ASC";
$providers = $tmprovider->SelectByWhere($where);
$providers_list = new ArrayList();
$providers_list->AddArray($providers);

$ddlProviders = new ComboBox("ddlProviders","ddlProviders","Providers: ");
$ddlProviders->ShowCaption = true;
$options = null;
$options[] = new ListItem("Select Provider","",true);
$options[] = new ListItem("All","0");
$ddlProviders->Items = $options;
$ddlProviders->DataSource = $providers_list;
$ddlProviders->DataSourceText = "Name";
$ddlProviders->DataSourceValue = "ProviderID";
$ddlProviders->DataBind();
if(isset($_SESSION['prodaccttype']))
    $ddlProviders->SetSelectedValue ($_SESSION['prodaccttype']);

$hiddenctr = new Hidden("hiddenctr","hiddenctr","Hidden Ctr for submit button");

$btnAddProduct = new Button("btnAddProduct","btnAddProduct","Add Product");
$btnAddProduct->IsSubmit = true;

$btnSubmit = new Button("btnSubmit","btnSubmit","Submit");
$btnSubmit->IsSubmit = true;
$btnSubmit->Args = "onclick='javascript: return checkProviderSubmit()'";

$fproc->AddControl($hiddenprodid);
$fproc->AddControl($btnAddProduct);
$fproc->AddControl($ddlProviders);
$fproc->AddControl($btnSubmit);
$fproc->AddControl($hiddenctr);

$fproc->ProcessForms();

if($fproc->IsPostBack)
{
    $where = " ORDER BY pd.ProductName ASC";
    if($btnAddProduct->SubmittedValue == "Add Product")
    {
        URL::Redirect('addproduct.php');
    }
    if($hiddenctr->SubmittedValue == 1)
    {
        if($ddlProviders->SelectedValue == 0)
            $where = " ORDER BY pd.ProductName ASC";
        else
            $where = " WHERE pv.ProviderID = " . $ddlProviders->SelectedValue . " ORDER BY pd.ProductName ASC";
    }
    if($btnSubmit->SubmittedValue == "Submit")
    {
        $_SESSION['prodaccttype'] = $ddlProviders->SelectedValue;
        $hiddenctr->Text = 1;
        if($ddlProviders->SelectedValue == 0)
            $where = " ORDER BY pd.ProductName ASC";
        else
            $where = " WHERE pv.ProviderID = " . $ddlProviders->SelectedValue . " ORDER BY pd.ProductName ASC";
        $pgcon->SelectedPage = 1;
    }
    $products = $tmproduct->SelectProductByWhere($where);
    $productcount = count($products);
    $pgcon->Initialize($itemsperpage, $productcount);
    $pgTransactionHistory = $pgcon->PreRender();
    $arrproducts = $tmproduct->SelectProductByWhereWithLimit(($pgcon->SelectedItemFrom - 1), $itemsperpage,$where);
    $product_list = new ArrayList();
    $product_list->AddArray($arrproducts);
}
?>