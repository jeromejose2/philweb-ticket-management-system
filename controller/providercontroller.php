<?php

/* * ***************** 
 * Author: Roger Sanchez
 * Date Created: 2011-08-23
 * Company: Philweb
 * ***************** */

App::LoadModuleClass("TicketManagementCM", "TMProvider");

App::LoadControl("TextBox");
App::LoadControl("Button");
App::LoadControl("ComboBox");
$fproc = new FormsProcessor();

$tmprovider = new TMProvider();

$txtName = new TextBox("txtName", "txtName", "Provider Name: ");
$txtName->ShowCaption = true;
$txtName->Length = 30;

$txtDescription = new TextBox("txtDescription", "txtDescription", "Description: ");
$txtDescription->ShowCaption = true;
$txtDescription->Length = 30;

$cboProvider = new ComboBox("cboProvider", "cboProvider", "Select Provider: ");
$cboProvider->ShowCaption = true;

function BindProvider($cboProvider)
{
    $tmprovider = new TMProvider();
    $arrproviders = $tmprovider->SelectAll();
    if ($tmprovider->HasError)
    {
        // Log error
        $lg = new Logging();
        $lg->WriteAppend($tmprovider->getError());
    }
    else
    {
        // Suc
    }
    $providerlist = new ArrayList();
    $providerlist->AddArray($arrproviders);
    $cboProvider->ClearItems();
    $cboProvider->DataSource = $providerlist;
    $cboProvider->DataSourceText = "Name";
    $cboProvider->DataSourceValue = "ProviderID";
    
    $cboProvider->DataBind();
}

BindProvider($cboProvider);

$btnSubmit = new Button("btnSubmit", "btnSubmit", "Submit");
$btnSubmit->IsSubmit = true;

$btnEdit = new Button("btnEdit", "btnEdit", "Edit");
$btnEdit->IsSubmit = true;

$btnUpdate = new Button("btnUpdate", "btnUpdate", "Update");
$btnUpdate->IsSubmit = true;

$fproc->AddControl($txtName);
$fproc->AddControl($txtDescription);
$fproc->AddControl($btnSubmit);
$fproc->AddControl($btnEdit);
$fproc->AddControl($btnUpdate);
$fproc->AddControl($cboProvider);

$fproc->ProcessForms();

if ($fproc->IsPostBack)
{
    if ($btnSubmit->SubmittedValue == "Submit")
    {
        $tmprov["Name"] = $txtName->Text;
        $tmprov["Description"] = $txtDescription->Text;
        $tmprovider->Insert($tmprov);
        /*
        if ($tmprovider->HasError)
        {
            App::Pr($tmprovider->getError());
        }
        else
        {
            App::Pr("Provider successfully created.");
        }
        */
    }

    if ($btnEdit->SubmittedValue == "Edit")
    {
        $tmproviderid = $cboProvider->SubmittedValue;
        $tmprows = $tmprovider->SelectByID($tmproviderid);
        $tmprov = $tmprows[0];
        $txtName->Text = $tmprov["Name"];
        $txtDescription->Text = $tmprov["Description"];
    }

    if ($btnUpdate->SubmittedValue == "Update")
    {
        $tmprov["Name"] = $txtName->Text;
        $tmprov["Description"] = $txtDescription->Text;
        $tmprov["ProviderID"] = $cboProvider->SubmittedValue;
        $tmprovider->UpdateByArray($tmprov);
        if ($tmprovider->HasError)
        {
            App::Pr($tmprovider->getError());
        }
        else
        {
            App::Pr("Provider successfully updated.");
        }
        
        BindProvider($cboProvider);
    }
}
?>