<?php
/*
 * @author 
 * Purpose   : controller for providerlist
 */
$pagesubmenuid = 22;
$stylesheets[] = "css/default.css";

App::LoadModuleClass("TicketManagementCM", "TMProvider");

App::LoadControl("Button");
App::LoadControl("Hidden");
App::LoadControl("PagingControl2");

$fproc = new FormsProcessor();
$tmprovider = new TMProvider();

/*PAGING*/
$itemsperpage = 15;
$pgcon = new PagingControl2($itemsperpage, 1);
$pgcon->URL = "javascript:ChangePage(%currentpage);";
$pgcon->ShowMoveToFirstPage = true;
$pgcon->ShowMoveToLastPage = true;
/*PAGING*/

$providers = $tmprovider->SelectAllProvider();
$providercount = count($providers);
$pgcon->Initialize($itemsperpage, $providercount);
$pgTransactionHistory = $pgcon->PreRender();
$arrproviders = $tmprovider->SelectWithLimit(($pgcon->SelectedItemFrom - 1), $itemsperpage);
$provider_list = new ArrayList();
$provider_list->AddArray($arrproviders);

$hiddenprovid = new Hidden("hiddenprovid","hiddenprovid","Hidden Provider ID");

$btnAddProvider = new Button("btnAddProvider","btnAddProvider","Add Provider");
$btnAddProvider->IsSubmit = true;

$fproc->AddControl($btnAddProvider);
$fproc->AddControl($hiddenprovid);

$fproc->ProcessForms();

if ($fproc->IsPostBack)
{
    if($btnAddProvider->SubmittedValue == "Add Provider")
    {
        URL::Redirect('addprovider.php');
    }
}
?>