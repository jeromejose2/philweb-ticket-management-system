<?php
/*
 * Added By : Arlene R. Salazar
 * Added On : Jan 12, 2012
 * Purpose : Process redemption commission detailed view
 */
$pagesubmenuid = 41;
$stylesheets[] = "css/default.css";
$javascripts[] = "jscripts/validations.js";

App::LoadModuleClass("TicketManagementCM", "TMAccounts");
App::LoadModuleClass("TicketManagementCM", "TMProducts");
App::LoadModuleClass("TicketManagementCM", "TMWinnings");
App::LoadModuleClass("TicketManagementCM", "TMGameManagement");
App::LoadModuleClass("TicketManagementCM", "TMCurrencies");

App::LoadControl("Hidden");
App::LoadControl("PagingControl2");
App::LoadControl("Button");

$redemptioncommissionform = new FormsProcessor();

$currency = App::getParam("currency");

$tmwinnings = new TMWinnings();
$tmcurrencies = new TMCurrencies();

$arrcurr = $tmcurrencies->SelectByCurrencySymbol("KHR");
$fixedrate = $arrcurr[0]["FixedRate"];

//$hiddencardvalue = new Hidden("hiddencardvalue","hiddencardvalue","Hidden Card Value");
$hiddengameid = new Hidden("hiddengameid","hiddengameid","Hidden Game ID");
$hiddenprodid = new Hidden("hiddenprodid","hiddenprodid","Hidden Product ID");
$hiddenuserid = new Hidden("hiddenuserid","hiddenuserid","Hidden User ID");
$hiddenbatchid = new Hidden("hiddenbatchid","hiddenbatchid","Hidden Batch ID");
$hiddenbatchvalidationid = new Hidden("hiddenbatchvalidationid","hiddenbatchvalidationid","Hidden Batch Validation ID");
$hiddenprocessedtag = new Hidden("hiddenprocessedtag","hiddenprocessedtag","Hidden processed tag");
$hiddenselectedgameid = new Hidden("hiddenselectedgameid", "hiddenselectedgameid", "Hidden Selected Game ID");

$btnBack = new Button("btnBack","btnBack","Back");
$btnBack->IsSubmit = true;

$itemsperpage = 50;
$pgcon = new PagingControl2($itemsperpage, 1);
$pgcon->URL = "javascript:ChangePage(%currentpage);";
$pgcon->ShowMoveToFirstPage = true;
$pgcon->ShowMoveToLastPage = true;
$pgcon->PageGroup = 5;

//$redemptioncommissionform->AddControl($hiddencardvalue);
$redemptioncommissionform->AddControl($hiddengameid);
$redemptioncommissionform->AddControl($hiddenprodid);
$redemptioncommissionform->AddControl($hiddenuserid);
$redemptioncommissionform->AddControl($hiddenbatchvalidationid);
$redemptioncommissionform->AddControl($hiddenprocessedtag);
$redemptioncommissionform->AddControl($btnBack);
$redemptioncommissionform->AddControl($hiddenbatchid);
$redemptioncommissionform->AddControl($hiddenselectedgameid);

$redemptioncommissionform->ProcessForms();

if($redemptioncommissionform->IsPostBack)
{
    $incentive = 2;
    if($redemptioncommissionform->GetPostVar("hiddengameid"))
    {
        //$hiddencardvalue->Text = $redemptioncommissionform->GetPostVar("hiddencardvalue");
        $hiddengameid->Text = $redemptioncommissionform->GetPostVar("hiddengameid");
        $hiddenprodid->Text = $redemptioncommissionform->GetPostVar("hiddenprodid");
        $hiddenuserid->Text = $redemptioncommissionform->GetPostVar("hiddenuserid");
        $hiddenbatchvalidationid->Text = $redemptioncommissionform->GetPostVar("hiddenbatchvalidationid");
        $hiddenprocessedtag->Text = $redemptioncommissionform->GetPostVar("hiddenprocessedtag");
        $hiddenbatchid->Text = $redemptioncommissionform->GetPostVar("hiddenbatchid");
        $hiddenselectedgameid->Text = $redemptioncommissionform->GetPostVar("hiddenselectedgameid");
    }
    //$cardvalue = $hiddencardvalue->Text;
    $gameid = $hiddenselectedgameid->Text;
    $gameid2 = $hiddengameid->Text;
    $prodid = $hiddenprodid->Text;
    $acctid = $hiddenuserid->Text;
    $gamebatchid = $hiddenbatchid->Text;
    $batchid = $hiddenbatchvalidationid->Text;
    $processedtag = $hiddenprocessedtag->Text;
    
    //$_SESSION['cardvalue'] = $cardvalue;
    $_SESSION['gameid'] = $gameid2;
    $_SESSION['prodid'] = $prodid;
    $_SESSION['acct_id'] = $acctid;
    $_SESSION['batchid'] = $gamebatchid;

    if($processedtag == 1)
    {
        $all_redemptionsumm = $tmwinnings->GetRedemptionSummaryPerBatchWithLimit($gameid,$prodid,$acctid,$batchid,($pgcon->SelectedItemFrom - 1),$itemsperpage, "2,5", "1,2");
        $redemptionsumm = $tmwinnings->GetRedemptionSummaryPerBatch($gameid,$prodid,$acctid,$batchid, "2,5", "1,2");
        $prizesredeemed = $tmwinnings->GetTotalPrizesRedeemedPerBatchID($gameid,$prodid,$acctid,$batchid, "2,5", "1,2");
    }
    else
    {
        $all_redemptionsumm = $tmwinnings->GetRedemptionSummaryPerBatchWithLimit2($gameid,$prodid,$acctid,$batchid,($pgcon->SelectedItemFrom - 1),$itemsperpage, 0, 0);
        $redemptionsumm = $tmwinnings->GetRedemptionSummaryPerBatch2($gameid,$prodid,$acctid,$batchid, 0, 0);
        $prizesredeemed = $tmwinnings->GetTotalPrizesRedeemedPerBatchID2($gameid,$prodid,$acctid,$batchid, 0, 0);
    }

    $redemptionsumm_list = new ArrayList();
    $redemptionsumm_list->AddArray($all_redemptionsumm);
    
    for ($i = 0; $i < count($redemptionsumm_list); $i++)
    {
        if (strpos($redemptionsumm_list[$i]["PrizeName"], 'KHR') !== false)
        {
            $str_prize = preg_replace('/[\KHR,]/', '', $redemptionsumm_list[$i]["PrizeName"]);
            $usd_prize = $str_prize / $fixedrate;
            $redemptionsumm_list[$i]["PrizeName"] = '$' . number_format($usd_prize, 2);
        }
        else if (strpos($redemptionsumm_list[$i]["PrizeName"], $currency) !== false)
        {
            $usd_prize = preg_replace('/[\"'.$currency.'",]/', '', $redemptionsumm_list[$i]["PrizeName"]);
            $redemptionsumm_list[$i]["PrizeName"] = '$' . number_format($usd_prize, 2);
        }
        $total_prizes_redeemed += $usd_prize;
    }

    //$total_prizes_redeemed = $prizesredeemed[0]["Total"];
    $total_redemption_comm = ($total_prizes_redeemed * ($incentive / 100));

    $recordcount = count($redemptionsumm);
    $pgcon->Initialize($itemsperpage, $recordcount);
    $pgTransactionHistory = $pgcon->PreRender();
    $current_page = $pgcon->SelectedItemFrom;
    $total_page = ceil(($recordcount/$itemsperpage));

    if($btnBack->SubmittedValue == "Back")
    {
        if($hiddenprocessedtag->Text == 1)
        {
            header("Location: commissionsummary.php");
        }
        else
        {
            header("Location: batchredemption.php");
        }
    }
}
else
{
    header("Location: commissionsummary.php");
}
?>