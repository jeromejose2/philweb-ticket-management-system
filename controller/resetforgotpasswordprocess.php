<?php
/*
 * Created By: Noel Antonio on 09/16/2011
 * Purpose: For resetting password controller
 */
$rcode = $_GET["rcode"];
App::LoadModuleClass("TicketManagementCM", "TMAccounts");
App::LoadModuleClass("TicketManagementCM", "TMAuditLog");
App::LoadModuleClass("TicketManagementCM", "TMPasswordUpdateRequests");

App::LoadControl("TextBox");
App::LoadControl("Button");

$frmchgestat = new FormsProcessor();
$account = new TMAccounts();
$passwordupdaterequest = new TMPasswordUpdateRequests();

$where = " WHERE pr.RequestCode = '" . $rcode . "' AND pr.Status = 0";
$acctdtls = $passwordupdaterequest->SelectByHash($where);
if(count($acctdtls) > 0)
{
    $current_password = $acctdtls[0]["Password"];
    $username = $acctdtls[0]["UserName"];
    $id = $acctdtls[0]["AID"];
    $requestid = $acctdtls[0]["PasswordUpdateRequestID"];
}

//$txtoldpword = new TextBox("txtoldpword","txtoldpword","Old Password: ");
//$txtoldpword->Password = true;
//$txtoldpword->ShowCaption = true;

$txtSysGenPass = new TextBox("txtSysGenPass", "txtSysGenPass", "System-Generated Password : ");
$txtSysGenPass->Password = true;
$txtSysGenPass->ShowCaption = true;

$txtnewpword = new TextBox("txtnewpword","txtnewpword","New Password: ");
$txtnewpword->Password = true;
$txtnewpword->ShowCaption = true;
$txtnewpword->Length = 20;
$txtnewpword->Args = "onkeypress='javascript: return disableSpace(event);'size='31';";

$txtcfrmpword = new TextBox("txtcfrmpword","txtcfrmpword","Confirm Password: ");
$txtcfrmpword->Password = true;
$txtcfrmpword->ShowCaption = true;
$txtcfrmpword->Length = 20;
$txtcfrmpword->Args = "onkeypress='javascript: return disableSpace(event);'size='31';";

$btnSubmit = new Button("btnSubmit","btnSubmit","Save");
$btnSubmit->IsSubmit = true;
$btnSubmit->Args = "onclick='javascript: return checkChangePassword();'";

$btnConfirmChangePword = new Button("btnConfirmChangePword","btnConfirmChangePword","Okay");
$btnConfirmChangePword->IsSubmit = true;

$frmchgestat->AddControl($txtSysGenPass);
//$frmchgestat->AddControl($txtoldpword);
$frmchgestat->AddControl($txtnewpword);
$frmchgestat->AddControl($txtcfrmpword);
$frmchgestat->AddControl($btnSubmit);
$frmchgestat->AddControl($btnConfirmChangePword);

$frmchgestat->ProcessForms();

if($frmchgestat->IsPostBack)
{
    if($btnSubmit->SubmittedValue == "Save")
    {      
        if ($current_password == MD5($txtSysGenPass->Text)){
            $okchgepwordmsg = "ok";}
        else {
            $confchangepword_title = "ERROR!";
            $confchangepword_msg = "System Generated Password did not match";
        }
    }
    if($btnConfirmChangePword->SubmittedValue == "Okay")
    {
//        $newpass = trim($txtnewpword->Text);
//        $confpass = trim($txtcfrmpword->Text);
        
	$updatedpword = $account->UpdateAccountPassword($id,MD5($txtnewpword->Text));
        if($account->HasError)
        {
            $confchangepword_title = "ERROR!";
            $confchangepword_msg = "Error has ocurred: " . $account->getError();
        }
        else
        {
            $auditlog = new TMAuditLog();
            $auditdtls["SessionID"] = "Request Code: " . $rcode; 
            $auditdtls["AID"] = $id; 
            $auditdtls["TransDetails"] = "Account ID: " . $id;
            $auditdtls["TransDateTime"] = "now_usec()";
            $auditdtls["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
            $auditdtls["AuditTrailFunctionID"] = 3;
            $insertauditlog = $auditlog->Insert($auditdtls);
            if($auditlog->HasError)
            {
                $confchangepword_title = "ERROR!";
                $confchangepword_msg = "Error has occured: "  . $auditlog->getError();
            }
            else
            {
                $success_redirect = "SUCCESS!";
                $success_redirect_msg = "Password was successfully changed.";
            }
        }
	$updatechangepword = $account->UpdateAccountChangePasswordStatus($id,0);
	if($account->HasError)
        {
	    $confchangepword_title = "ERROR!";
            $confchangepword_msg = "Error has occurred: " . $account->getError();
        }

	$updaterequeststatus = $passwordupdaterequest->UpdateStatus($requestid,1);
	if($passwordupdaterequest->HasError)
        {
	    $confchangepword_title = "ERROR!";
            $confchangepword_msg = "Error has occurred: " . $passwordupdaterequest->getError();
        }
    }
}

?>