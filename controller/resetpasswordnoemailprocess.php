<?php
/* * *****************
 * Author: Sheryl S. Babas
 * Date Created: 2012-01-16
 * Description: Reset Password of Accounts with no Email Address
 * ***************** */
$pagesubmenuid = 39;
$stylesheets[] = "css/default.css";
$javascripts[] = "jscripts/jquery-1.5.2.min.js";
$javascripts[] = "jscripts/validations.js";

App::LoadModuleClass("TicketManagementCM", "TMAccounts");

App::LoadControl("ComboBox");
App::LoadControl("Button");

$formreset = new FormsProcessor();
$account = new TMAccounts();

$users = $account->SelectAllAccountWithNoEmail();
$user_list = new ArrayList();
$user_list->AddArray($users);

$btnSubmit = new Button("Submit","Submit","Submit");
$btnSubmit->Args = "onclick='javascript: return validate();'";
$btnSubmit->IsSubmit = true;
$btnSubmit->ShowCaption = true;

$username = new ComboBox("username","username","Username:");
$username->ShowCaption = false;
$data = null;
$data[] = new ListItem("Please Select", "0",false);
$username->Items = $data;

$username->DataSource = $user_list;
$username->DataSourceText = "UserName";
$username->DataSourceValue = "AID";
$username->DataBind();

$formreset->AddControl($username);
$formreset->AddControl($btnSubmit);
$formreset->ProcessForms();
?>