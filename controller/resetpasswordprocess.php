<?php

/* * **********************************************************
 * Author: Sheryl S. Babas
 * Date Created: 2012-01-16
 * Modified By: Noel Antonio
 * Date Modified: August 13, 2012
 * Description: Reset Password of Accounts with no Email Address
 * *************************************************************/

$pagesubmenuid = 3;
$stylesheets[] = "css/default.css";
$javascripts[] = "jscripts/jquery-1.5.2.min.js";
$javascripts[] = "jscripts/validations.js";

App::LoadModuleClass("TicketManagementCM", "TMAccounts");
App::LoadModuleClass("TicketManagementCM", "TMAuditLog");
App::LoadModuleClass("TicketManagementCM", "TMPasswordUpdateRequests");

App::LoadControl("ComboBox");
App::LoadControl("Button");
App::LoadControl("TextBox");
App::LoadControl("Hidden");
App::LoadCore("PHPMailer.class.php");

$formreset = new FormsProcessor();
$account = new TMAccounts();
$passwordupdaterequest = new TMPasswordUpdateRequests();

$hiddenid = new Hidden('hiddenid','hiddenid','ID');
$regards = App::getParam('country');

$username = new TextBox('txtusername','txtusername','Username:');
$username->ShowCaption = true;
$username->Enabled = false;

$txtname = new TextBox('txtname','txtname','Full Name:');
$txtname->ShowCaption = true;
$txtname->Enabled = false;
$txtname->Size = 80;

$txtaddress = new TextBox('txtaddress','txtaddress','Address:');
$txtaddress->ShowCaption = true;
$txtaddress->Enabled = false;
$txtaddress->Size = 93;

$txtcontactnum = new TextBox('txtcontactnum','txtcontactnum','Contact Number:');
$txtcontactnum->ShowCaption = true;
$txtcontactnum->Enabled = false;

$txtemail = new TextBox('txtemail','txtemail','Email Address:');
$txtemail->ShowCaption = true;
$txtemail->Enabled = false;

$txtstatus = new TextBox('txtstatus','txtstatus','Status:');
$txtstatus->ShowCaption = false;
$txtstatus->Enabled = false;

$txtacctype = new TextBox('txtacctype','txtacctype','Account Type:');
$txtacctype->ShowCaption = true;
$txtacctype->Enabled = false;

$btnReset = new Button('btnreset','btnreset','Reset');
$btnReset->Args = "onclick='javascript: return resetPassword();'";

$btnConfirmResetPword = new Button('btnConfirmResetPword','btnConfirmResetPword','Okay');
$btnConfirmResetPword->IsSubmit = true;

$btnCancel = new Button('btnCancel','btnCancel','Cancel');
$btnCancel->IsSubmit = false;
$btnCancel->Args = "onclick = 'javascript: return redirectToResetPass()'";

/*$btnConfirm = new Button('btnConfirm','btnConfirm','Okay');
$btnConfirm->IsSubmit = false;
$btnConfirm->Args = "onclick = 'javascript:document.getElementById('light3').style.display = none;document.getElementById('fade').style.display = none'"*/

$hiddenpostedid = new Hidden("hiddenpostedid","hiddenpostedid","Hidden Posted Id");

$formreset->AddControl($hiddenid);
$formreset->AddControl($username);
$formreset->AddControl($txtname);
$formreset->AddControl($txtaddress);
$formreset->AddControl($txtcontactnum);
$formreset->AddControl($txtemail);
$formreset->AddControl($txtstatus);
$formreset->AddControl($txtacctype);
$formreset->AddControl($btnReset);
$formreset->AddControl($btnConfirmResetPword);
$formreset->AddControl($hiddenpostedid);
$formreset->AddControl($btnCancel);
$formreset->ProcessForms();
?>

<?php

if($formreset->IsPostBack)
{
    if ($formreset->GetPostVar("username"))
    {
        $selectedaccountid = $formreset->GetPostVar("username");
        $hiddenpostedid->Text = $selectedaccountid;
    }
           
    $postedid = $hiddenpostedid->Text;   
    $acct = $account->SelectByAccountID($postedid);
    for ($i=0; $i<count($acct); $i++)
    {
            $username_val = $acct[$i]["UserName"];
            $fname = $acct[$i]['Name'];
            $accttype = $acct[$i]['AccountName'];
            $pword = $acct[$i]['Password'];
            $email = $acct[$i]['Email'];
            $address = $acct[$i]['Address'];
            $contactno = $acct[$i]['MobileNumber'];
            switch($acct[$i]['Status'])
            {
                case 0:
                    $status = "Pending";
                    break;
                case 1:
                    $status = "Active";
                    break;
                case 2:
                    $status = "Suspended";
                    break;
                    case 3:
                    $status = "Locked - Invalid Attempts";
                    break;
                case 4:
                    $status = "Locked";
                    break;
                case 5:
                    $status = "Terminated";
                    break;
                default :
                    $status = "No status";
                    break;
            }
    }

    $username->Text = $username_val;
    $txtname->Text = $fname;
    $txtaddress->Text = $address;
    $txtstatus->Text = $status;
    $txtcontactnum->Text = $contactno;
    $txtacctype->Text = $accttype;
    $txtemail->Text = $email;
    
    if($btnConfirmResetPword->SubmittedValue == "Okay")
    {
        $requestcode = MD5($txtusername->Text.$hiddenacctid->SubmittedValue.date("Y-m-d H:i:s"));
        $newpass = substr(uniqid(rand(),true),4,12);
        $newpass_encrypt = md5($newpass);
        
        // Update account details
        $array["AID"] = $postedid;
        $array["Password"] = $newpass_encrypt;
        $account->UpdateByArray($array); 

	
        $passwordrequest["AID"] = $postedid;
        $passwordrequest["RequestCode"] = $requestcode;
        $passwordrequest["DateRequested"] = date("Y-m-d H:i:s");
        $passwordrequest["Status"] = 0;
        $changepwordrequest = $passwordupdaterequest->Insert($passwordrequest);
        if($passwordupdaterequest->HasError)
        {
            $errormsgfp = "Error has occurred: " . $passwordupdaterequest->getError();
            $errormsgtitle = "ERROR!";
        }
        
        $pm = new PHPMailer();
        /*if(trim($email) == "")
        {
            $pm->AddAddress("cidalangin@philweb.com.ph","Charles Dalangin");
            $pm->AddBCC("mccalderon@philweb.com.ph", "Tere Calderon");
        }
        else
        {
            $pm->AddAddress($email, $fname);
        }*/
        
        //$pm->AddAddress("cidalangin@philweb.com.ph","Charles Dalangin");
        $pm->AddBCC("mccalderon@philweb.com.ph", "Tere Calderon");
        $pageURL = 'http';
        if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
        $pageURL .= "://";
        $folder = $_SERVER["REQUEST_URI"];
        $folder = substr($folder,0,strrpos($folder,'/') + 1);
        if ($_SERVER["SERVER_PORT"] != "80") 
        {
          $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$folder;
        } 
        else 
        {
          $pageURL .= $_SERVER["SERVER_NAME"].$folder;
        }
            
            /* Modified By: Noel Antonio August 13, 2012 */
            $pm->Body = "Dear $fname, \n\nThis is to inform you that your password has been reset on this date ".date("m/d/Y")." and time ".date("H:i:s").". \nHere are your new Account Details:\n\n\t\t\t\t\tUsername: ".$username_val.
                    "\n\t\t\t\t\tPassword: ".$newpass."\n\n".
                    "For security purposes, please change this system-generated password upon log in at   
                    \n\t\t\t " . $pageURL . "resetforgotpassword.php?rcode=".$requestcode."\n\n".
                    "If you didn't perform this procedure, please report this to our toll free Customer Service hotline at +63 02 338 5599 or email us at customersupport@philweb.com.ph.
                    \n\nRegards,\n\nCustomer Support\n$regards Ticket Management System";
            
            /*$pm->Body = "
            Good Day!
            \n\n This is to inform you that the password of $username_val has been reset on this date $date and time $time. Here are the details:
            \n\n Full Name: $fname
            \n Username: $username_val
            \n Password: $newpass
            \n\n For security purposes, please change this system-generated password upon log in at $pageURL
            \n\n Regards,
            \n Ticket Managment System";*/
            
        $pm->From = "no-reply@goldscratchnwin.com";
        $pm->FromName = "Ticket Management System";
        $pm->Host = "localhost";
        $pm->Subject = "Ticket Management System Change Password";
        $email_sent = $pm->Send();
        
        $auditlog = new TMAuditLog();
        $auditdtls["SessionID"] = $_SESSION['sid'];
        $auditdtls["AID"] = $_SESSION['acctid'];
        $auditdtls["TransDetails"] = "Username: " . $username_val;
        $auditdtls["TransDateTime"] = "now_usec()";
        $auditdtls["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
        $auditdtls["AuditTrailFunctionID"] = 34;
        $auditlog->Insert($auditdtls);
        if($auditlog->HasError)
        {
            $confupdateuser_title = "ERROR!";
            $confupdateuser_msg = "Error has occured: "  . $auditlog->getError();
        }
               
        if($email_sent)
        {
            $emailmsg = "The new password has been sent.";
            $emailtitle = "SUCCESSFULL!";
        }
        else
        {
            if (!update)
            {
                $emailmsg = "Unable to reset password";
                $emailtitle = "ERROR!";                
            }
            $emailmsg = "An error occurred while sending the email to your email address";
            $emailtitle = "ERROR!";
            
        }
    }
}

?>
