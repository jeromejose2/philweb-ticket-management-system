<?php
/*
 * Added By : Noel D. Antonio
 * Added On : February 10, 2012
 * Purpose : Process for retailer reimbursement
 */
$pagesubmenuid = 45;
$stylesheets[] = "css/default.css";
$javascripts[] = "jscripts/validations.js";
$javascripts[] = "jscripts/jquery-1.5.2.min.js";

App::LoadModuleClass("TicketManagementCM", "TMAccounts");
App::LoadModuleClass("TicketManagementCM", "TMProducts");
App::LoadModuleClass("TicketManagementCM", "TMWinnings");
App::LoadModuleClass("TicketManagementCM", "TMGameManagement");
App::LoadModuleClass("TicketManagementCM","Pdf");

App::LoadControl("ComboBox");
App::LoadControl("Button");
App::LoadControl("Hidden");
App::LoadControl("PagingControl2");

App::LoadLibrary("fpdf/fpdf.php");

$fproc = new FormsProcessor();
$tmaccounts = new TMAccounts();
$tmproducts = new TMProducts();
$tmwinnings = new TMWinnings();
$tmgamemngt = new TMGameManagement();

$ddlUsername = new ComboBox("ddlUsername","ddlUsername","Username");
$options = null;
$options[] = new ListItem("Please Select", "0");
$ddlUsername->Items = $options;
$arrRetailers = $tmaccounts->SelectAllActiveRetailer();
$retailer_list = new ArrayList();
$retailer_list->AddArray($arrRetailers);
$ddlUsername->DataSource = $retailer_list;
$ddlUsername->DataSourceText = "UserName";
$ddlUsername->DataSourceValue = "AID";
$ddlUsername->DataBind();

$ddlGameName = new ComboBox("ddlGameName","ddlGameName","Game Name");
$ddlGameName->Args = "onchange = 'javascript: get_gamenumber();'";
$gamenameopt = null;
$gamenameopt[] = new ListItem("Please select", "0", true);
$ddlGameName->Items = $gamenameopt;
$arrGameNames = $tmproducts->SelectAllGameName();
$gamename_list = new ArrayList();
$gamename_list->AddArray($arrGameNames);
$ddlGameName->DataSource = $gamename_list;
$ddlGameName->DataSourceText = "ProductName";
$ddlGameName->DataSourceValue = "ProductID";
$ddlGameName->DataBind();

$ddlGameNum = new ComboBox("ddlGameNum","ddlGameNum","Game Number");
$gamenumopt = null;
$gamenumopt[] = new ListItem("Please select", "0", true);
$ddlGameNum->Items = $gamenameopt;
$ddlGameNum->Args = "onchange='javascript: get_batchid(); get_gamename();'";
$allgames = $tmgamemngt->SelectAllActive();
$gameslist = new ArrayList();
$gameslist->AddArray($allgames);
$ddlGameNum->DataSource = $gameslist;
$ddlGameNum->DataSourceText = "GameNumber";
$ddlGameNum->DataSourceValue = "GameID";
$ddlGameNum->DataBind();

$btnSubmit = new Button("btnSubmit","btnSubmit","Submit");
$btnSubmit->IsSubmit = true;
$btnSubmit->Args = "onclick = 'return checkCommissionSummary();'";

$btnOkay = new Button("btnOkay", "btnOkay", "Okay");
$btnOkay->IsSubmit = true;

$hiddenbatchsessionid = new Hidden("hiddenbatchsessionid", "hiddenbatchsessionid", "Hidden Batch Session ID");
$hiddengameid = new Hidden("hiddengameid","hiddengameid","Hidden Game ID");
$hiddenprodid = new Hidden("hiddenprodid","hiddenprodid","Hidden Product ID");
$hiddenuserid = new Hidden("hiddenuserid","hiddenuserid","Hidden User ID");
$hiddenbatchvalidationid = new Hidden("hiddenbatchvalidationid","hiddenbatchvalidationid","Hidden Batch Validation ID");
$hiddenbatchid = new Hidden("hiddenbatchid","hiddenbatchid","Hidden Batch ID");
$hiddenprocessedtag = new Hidden("hiddenprocessedtag","hiddenprocessedtag","Hidden processed tag");
$hiddenprocessedtag->Text = 1;
$hiddenflag = new Hidden("hiddenflag","hiddenflag","Hidden Flag");

$ddlBatchID = new ComboBox("ddlBatchID","ddlBatchID","Game Batch");
$gamebatchopt = null;
$gamebatchopt[] = new ListItem("Please select", "", true);
$ddlBatchID->Items = $gamebatchopt;

$btnExport = new Button("btnExport","btnExport","Export to PDF");
$btnExport->IsSubmit = true;

$incentive = 2;

$itemsperpage = 50;
$pgcon = new PagingControl2($itemsperpage, 1);
$pgcon->URL = "javascript:ChangePage(%currentpage);";
$pgcon->ShowMoveToFirstPage = true;
$pgcon->ShowMoveToLastPage = true;
$pgcon->PageGroup = 5;

if(isset($_SESSION['prodid']))
{
    $ddlUsername->SetSelectedValue($_SESSION['acctid']);
    $ddlGameName->SetSelectedValue($_SESSION['prodid']);

    $hiddengameid->Text = $_SESSION['gameid'];
    $hiddenprodid->Text = $_SESSION['prodid'];
    $hiddenuserid->Text = $_SESSION['acctid'];
    $hiddenbatchid->Text = $_SESSION['batchid'];

    $gamenum_perproduct = $tmproducts->SelectGameNumPerGameName($_SESSION['prodid']);
    $gamenum_perproduct_list = new ArrayList();
    $gamenum_perproduct_list->AddArray($gamenum_perproduct);
    $ddlGameNum->DataSource = $gamenum_perproduct_list;
    $ddlGameNum->DataSourceText = "GameNumber";
    $ddlGameNum->DataSourceValue = "GameID";
    $ddlGameNum->DataBind();
    $ddlGameNum->SetSelectedValue($_SESSION['gameid']);

    $ddlBatchID->ClearItems();
    $gamebatchopt = null;
    $gamebatchopt[] = new ListItem("ALL", "0", true);
    $ddlBatchID->Items = $gamebatchopt;
    $gamebatches = $tmproducts->SelectBatchIDPerGameNum($_SESSION['gameid'],2);
    $gamebatches_list = new ArrayList();
    $gamebatches_list->AddArray($gamebatches);
    $ddlBatchID->DataSource = $gamebatches_list;
    $ddlBatchID->DataSourceText = "BatchID";
    $ddlBatchID->DataSourceValue = "GameBatchID";
    $ddlBatchID->DataBind();
    $ddlBatchID->SetSelectedValue($_SESSION['batchid']);

    $virnstatus = "2,5";
    $ticketstatus = "1,2";
    $commissionsumm = $tmwinnings->GetCommissionSummaryPerBatchWithLimit($_SESSION['acctid'], $_SESSION['gameid'], $_SESSION['prodid'], ($pgcon->SelectedItemFrom - 1), $itemsperpage , $virnstatus, $ticketstatus, 1, $_SESSION['batchid']);
    $commissionsummlist = new ArrayList();
    $commissionsummlist->AddArray($commissionsumm);
    $recordcount_array = $tmwinnings->GetCommissionSummaryPerBatch($_SESSION['acctid'], $_SESSION['gameid'], $_SESSION['prodid'] , $virnstatus, $ticketstatus, 1, $_SESSION['batchid']);

    $recordcount = count($recordcount_array);
    $pgcon->Initialize($itemsperpage, $recordcount);
    $pgTransactionHistory = $pgcon->PreRender();
}

/*Added by Arlene R. Salazar 03/07/2012*/
$btnExportCSV = new Button("btnExportCSV","btnExportCSV","Export to CSV");
$btnExportCSV->IsSubmit = true;

$fproc->AddControl($ddlUsername);
$fproc->AddControl($ddlGameName);
$fproc->AddControl($ddlGameNum);
$fproc->AddControl($btnSubmit);
$fproc->AddControl($hiddengameid);
$fproc->AddControl($hiddenprodid);
$fproc->AddControl($hiddenuserid);
$fproc->AddControl($hiddenbatchvalidationid);
$fproc->AddControl($hiddenbatchsessionid);
$fproc->AddControl($ddlBatchID);
$fproc->AddControl($btnExport);
$fproc->AddControl($hiddenprocessedtag);
$fproc->AddControl($hiddenbatchid);
$fproc->AddControl($hiddenflag);
$fproc->AddControl($btnOkay);
$fproc->AddControl($btnExportCSV);

$fproc->ProcessForms();

unset($_SESSION['gameid']);
unset($_SESSION['prodid']);
unset($_SESSION['acctid']);
unset($_SESSION['batchid']);

if($fproc->IsPostBack)
{   
    if($btnSubmit->SubmittedValue == "Submit")
    {
        $pgcon->SelectedPage = 1;
    }
    
    if ($btnOkay->SubmittedValue == "Okay")
    {
        $updateIDs = $tmwinnings->GetRedeemedIDs($hiddenbatchsessionid->SubmittedValue);
        for ($i=0;$i<count($updateIDs);$i++)
        {
            $valno = SUBSTR($updateIDs[$i]["ValidationNumber"],3,8);
            $updTicket = $tmwinnings->UpdateTicketsRedeemedtoReimbursed($updateIDs[$i]["BatchValidationTicketID"]);
            $updWinningCard = $tmwinnings->UpdateVIRNRedeemedtoReimbursed($updateIDs[$i]["WinningCardID"]);
            $updBatchFileTickets = $tmwinnings->UpdateBatchTicketsToReimbursed($valno);
            if ($tmwinnings->HasError)
            {
                $errortitle = "ERROR";
                $errormsg = "There is an error occured. Ticket reimbursement failed.";
            }
        }
        $issuccessful = 1;
    }

    $acctid = $ddlUsername->SubmittedValue;
    $prodid = $ddlGameName->SubmittedValue;
    $gameid = $ddlGameNum->SubmittedValue;
    $batchid = $ddlBatchID->SubmittedValue;
    $hiddengameid->Text = $gameid;
    $hiddenprodid->Text = $prodid;
    $hiddenuserid->Text = $acctid;
    $hiddenbatchid->Text = $batchid;

    if($hiddenflag->SubmittedValue == 1)
    {
        $ddlGameNum->Args = "onchange='javascript: get_batchID(); '";
        $ddlGameNum->ClearItems();
        $gamenameopt = null;
        $gamenameopt[] = new ListItem("Please select", "0", true);
        $ddlGameNum->Items = $gamenameopt;
        $gamenum_perproduct = $tmproducts->SelectGameNumPerGameName($prodid);
        $gamenum_perproduct_list = new ArrayList();
        $gamenum_perproduct_list->AddArray($gamenum_perproduct);
        $ddlGameNum->DataSource = $gamenum_perproduct_list;
        $ddlGameNum->DataSourceText = "GameNumber";
        $ddlGameNum->DataSourceValue = "GameID";
        $ddlGameNum->DataBind();
        $ddlGameNum->SetSelectedValue($gameid);
    }

    if($hiddenflag->SubmittedValue == 2)
    {
        $ddlGameName->Args = "onchange='javascript:'";
        $ddlGameName->ClearItems();
        $gamenames = $tmproducts->SelectGameNamePerGameNum($gameid);
        $gamename_list1 = new ArrayList();
        $gamename_list1->AddArray($gamenames);
        $litemgmetype1 = null;
        $litemgmetype1[] = new ListItem("Please select", "0", false);
        $ddlGameName->Items = $litemgmetype1;
        $ddlGameName->DataSource = $gamename_list1;
        $ddlGameName->DataSourceText = "ProductName";
        $ddlGameName->DataSourceValue = "ProductID";
        $ddlGameName->DataBind();
        $ddlGameName->SetSelectedValue($prodid);
    }

    $ddlBatchID->ClearItems();
    $gamebatchopt = null;
    $gamebatchopt[] = new ListItem("ALL", "0", true);
    $ddlBatchID->Items = $gamebatchopt;
    $gamebatches = $tmproducts->SelectBatchIDPerGameNum($gameid,2);
    $gamebatches_list = new ArrayList();
    $gamebatches_list->AddArray($gamebatches);
    $ddlBatchID->DataSource = $gamebatches_list;
    $ddlBatchID->DataSourceText = "BatchID";
    $ddlBatchID->DataSourceValue = "GameBatchID";
    $ddlBatchID->DataBind();
    $ddlBatchID->SetSelectedValue($batchid);

    $virnstatus = "2,5";
    $ticketstatus = "1,2";
    $commissionsumm = $tmwinnings->GetCommissionSummaryPerBatchWithLimit($acctid, $gameid, $prodid, ($pgcon->SelectedItemFrom - 1), $itemsperpage , $virnstatus, $ticketstatus, 1, $batchid);
    $commissionsummlist = new ArrayList();
    $commissionsummlist->AddArray($commissionsumm);
    $totalprizesredeemed = $tmwinnings->GetTotalRedemptionCommision($acctid, $gameid, $prodid , $virnstatus, $ticketstatus, $batchid);
    $recordcount_array = $tmwinnings->GetCommissionSummaryPerBatch($acctid, $gameid, $prodid , $virnstatus, $ticketstatus, 1, $batchid);
    
    $recordcount = count($recordcount_array);
    $pgcon->Initialize($itemsperpage, $recordcount);
    $pgTransactionHistory = $pgcon->PreRender();

    if($btnExport->SubmittedValue == "Export to PDF")
    {
        $pdf = new PDF('L','mm','Legal');
        $pdf->AddPage();
        $pdf->AliasNbPages();

        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(0,5,"Retailer Reimbursement",0,0,'C');
        $pdf->Ln(4);

        $pdf->Ln(8);

        if(count($recordcount_array) > 0)
        {
            for($i = 0 ; $i < count($recordcount_array) ; $i++)
            {
                $pdf->SetFont('Arial','B',9);
                $pdf->SetX(10);
                $pdf->Cell(20,5,'Game Batch',1,0,'C');
                $pdf->SetX(30);
                $pdf->Cell(25,5,'Username',1,0,'C');
                $pdf->SetX(55);
                $pdf->Cell(35,5,'Date & Time Validated',1,0,'C');
                $pdf->SetX(90);
                $pdf->Cell(50,5,'Total Redemption Commission',1,0,'C');
                $pdf->SetX(140);
                $pdf->Cell(40,5,'Date & Time Claimed',1,0,'C');
                $pdf->SetX(180);
                $pdf->Cell(30,5,'Game Number',1,0,'C');
                $pdf->SetX(210);
                $pdf->Cell(30,5,'Game Name',1,0,'C');
                $pdf->SetX(240);
                $pdf->Cell(40,5,'Book Ticket Number',1,0,'C');
                $pdf->SetX(280);
                $pdf->Cell(40,5,'Validation Number',1,0,'C');
                $pdf->SetX(320);
                $pdf->Cell(30,5,'Prize Amount',1,1,'C');

                $pdf->SetFont('Arial','',9);
                $pdf->SetX(10);
                $pdf->Cell(20,5,$commissionsummlist[$i]["BatchID"],1,0,'C');
                $pdf->Cell(25,5,$commissionsummlist[$i]["Username"],1,0,'C');
                $pdf->Cell(35,5,$commissionsummlist[$i]["DateValidated"],1,0,'C');
                $pdf->Cell(50,5,"$" . number_format(($commissionsummlist[$i]["Total"] * ($incentive / 100)) , 2 , "." , ","),1,0,'C');

                $virnstatus = "2,5";
                $ticketstatus = "1,2";
                $prizesredeemed = $tmwinnings->GetRedemptionSummaryPerBatch($commissionsummlist[$i]["GameID"],$prodid,$acctid,$commissionsummlist[$i]["BatchValidationSessionID"], $virnstatus, $ticketstatus);
                $totalprizesredeemed = $tmwinnings->GetTotalPrizesRedeemedPerBatchID($commissionsummlist[$i]["GameID"],$prodid,$acctid,$commissionsummlist[$i]["BatchValidationSessionID"], $virnstatus, $ticketstatus);
                $total_prizes_redeemed = $totalprizesredeemed[0]["Total"];
                $total_redemption_comm = ($total_prizes_redeemed * ($incentive / 100));
                if(count($prizesredeemed) > 0)
                {
                    
                    for($a = 0 ; $a < count($prizesredeemed) ; $a++)
                    {
                        $pdf->SetX(140);
                        $pdf->Cell(40,5,$prizesredeemed[$a]["DateClaimed"],1,0,'C');
                        $pdf->Cell(30,5,$prizesredeemed[$a]["GameNumber"],1,0,'C');
                        $pdf->Cell(30,5,$prizesredeemed[$a]["ProductName"],1,0,'C');
                        $pdf->Cell(40,5,$prizesredeemed[$a]["TicketNumber"],1,0,'C');
                        $pdf->Cell(40,5,$prizesredeemed[$a]["ValidationNumber"],1,0,'C');
                        $pdf->Cell(30,5,$prizesredeemed[$a]["PrizeName"],1,1,'R');
                    }
                    
                    $pdf->SetX(140);
                    $pdf->SetFont('Arial','B',9);
                    $pdf->Cell(180,5,'Total Prizes Redeemed:',1,0,'C');
                    $pdf->Cell(30,5,"$" . number_format($total_prizes_redeemed , 2 , '.' , ','),1,1,'R');
                    $pdf->SetX(140);
                    $pdf->Cell(180,5,'Total Redemption Commission:',1,0,'C');
                    $pdf->Cell(30,5,"$" . number_format($total_redemption_comm , 2 , '.' , ','),1,1,'R');
                }
                else
                {
                    $pdf->SetX(140);
                    $pdf->Cell(200,5,'No records found',1,0,'C');
                }

                $pdf->Ln(8);
            }
        }
        else
        {
            $pdf->SetFont('Arial','',9);
            $pdf->SetX(10);
            $pdf->Cell(330,5,'No records found',0,1,'C');
        }

        $pdf->Output('Retailer Reimbursement','D');
        Header('Content-Type: application/pdf');
    }

	/*
	* Added by: Arlene R. Salazar
	* Date: Mar 07, 2012
	*/
	if($btnExportCSV->SubmittedValue == "Export to CSV")
	{
		$data = "";
		$data2 = "";
		$fp = fopen("../csv/Retailer_Reimbursement.csv","w");
		if($fp)
		{
			if(count($recordcount_array) > 0)
		    {
				for($i = 0 ; $i < count($recordcount_array) ; $i++)
            	{
					$header = "Game Batch;Username;Date & Time Validated;Total Redemption Commission;Date & Time Claimed;Game Number;Game Name;Book Ticket Number;Validation Number;Prize Amount\n";	
					fwrite($fp,$header);
						
					$data .= $commissionsummlist[$i]["BatchID"] . ";" . $commissionsummlist[$i]["Username"] . ";" . $commissionsummlist[$i]["DateValidated"] . ";$" . ($commissionsummlist[$i]["Total"] * ($incentive / 100)) . ";";
					fwrite($fp,$data);

					$virnstatus = "2,5";
		            $ticketstatus = "1,2";
		            $prizesredeemed = $tmwinnings->GetRedemptionSummaryPerBatch($commissionsummlist[$i]["GameID"],$prodid,$acctid,$commissionsummlist[$i]["BatchValidationSessionID"], $virnstatus, $ticketstatus);
		            $totalprizesredeemed = $tmwinnings->GetTotalPrizesRedeemedPerBatchID($commissionsummlist[$i]["GameID"],$prodid,$acctid,$commissionsummlist[$i]["BatchValidationSessionID"], $virnstatus, $ticketstatus);
		            $total_prizes_redeemed = $totalprizesredeemed[0]["Total"];
		            $total_redemption_comm = ($total_prizes_redeemed * ($incentive / 100));
		            if(count($prizesredeemed) > 0)
		            {
						for($a = 0 ; $a < count($prizesredeemed) ; $a++)
		                {
							if($a > 0)
							{
								$data2 .= " " . ";" . " " . ";" . " " . ";" . " " . ";";
							}
							$data2 .= $prizesredeemed[$a]["DateClaimed"] . ";" . $prizesredeemed[$a]["GameNumber"] . ";" . $prizesredeemed[$a]["ProductName"] . ";" . $prizesredeemed[$a]["TicketNumber"] . ";" . $prizesredeemed[$a]["ValidationNumber"] . ";" . $prizesredeemed[$a]["PrizeName"] . "\n";
						}
						$data2 .= " " . ";" . " " . ";" . " " . ";" . " " . ";" . "Total Prizes Redeemed:" . ";$" . $total_prizes_redeemed . "\n" . " " . ";" . " " . ";" . " " . ";" . " " . ";" . "Total Redemption Commission:" . ";$" . $total_redemption_comm . "\n";
						fwrite($fp,$data2);
					}
					else
					{
						$data2 .= "No records found";
					}
					fwrite($fp,"\n\n");
				}
			}
			else
			{
				$data .= "No records found";
			}
			header('Content-type: text/csv');
	        header("Content-Disposition: attachment; filename=".'Retailer_Reimbursement.csv');
			header('Pragma: public');
	        readfile('../csv/Retailer_Reimbursement.csv');
	        exit;
		}
		else
		{
			echo "<script>alert('Cannot create file.')</script>";
		}
	}
}
?>