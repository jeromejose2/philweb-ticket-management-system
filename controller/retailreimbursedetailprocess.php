<?php
/*
 * Added By : Noel D. Antonio
 * Added On : Feb 10, 2012
 * Purpose : Process retailer reimbursement detailed view
 */
$pagesubmenuid = 45;
$stylesheets[] = "css/default.css";
$javascripts[] = "jscripts/validations.js";

App::LoadModuleClass("TicketManagementCM", "TMAccounts");
App::LoadModuleClass("TicketManagementCM", "TMProducts");
App::LoadModuleClass("TicketManagementCM", "TMWinnings");
App::LoadModuleClass("TicketManagementCM", "TMGameManagement");

App::LoadControl("Hidden");
App::LoadControl("PagingControl2");
App::LoadControl("Button");

$redemptioncommissionform = new FormsProcessor();
$tmaccounts = new TMAccounts();
$tmproducts = new TMProducts();
$tmwinnings = new TMWinnings();
$tmgamemngt = new TMGameManagement();

$hiddengameid = new Hidden("hiddengameid","hiddengameid","Hidden Game ID");
$hiddenprodid = new Hidden("hiddenprodid","hiddenprodid","Hidden Product ID");
$hiddenuserid = new Hidden("hiddenuserid","hiddenuserid","Hidden User ID");
$hiddenbatchid = new Hidden("hiddenbatchid","hiddenbatchid","Hidden Batch ID");
$hiddenbatchvalidationid = new Hidden("hiddenbatchvalidationid","hiddenbatchvalidationid","Hidden Batch Validation ID");
$hiddenprocessedtag = new Hidden("hiddenprocessedtag","hiddenprocessedtag","Hidden processed tag");

$btnBack = new Button("btnBack","btnBack","Back");
$btnBack->IsSubmit = true;

$itemsperpage = 50;
$pgcon = new PagingControl2($itemsperpage, 1);
$pgcon->URL = "javascript:ChangePage(%currentpage);";
$pgcon->ShowMoveToFirstPage = true;
$pgcon->ShowMoveToLastPage = true;
$pgcon->PageGroup = 5;

$redemptioncommissionform->AddControl($hiddengameid);
$redemptioncommissionform->AddControl($hiddenprodid);
$redemptioncommissionform->AddControl($hiddenuserid);
$redemptioncommissionform->AddControl($hiddenbatchvalidationid);
$redemptioncommissionform->AddControl($hiddenprocessedtag);
$redemptioncommissionform->AddControl($btnBack);
$redemptioncommissionform->AddControl($hiddenbatchid);

$redemptioncommissionform->ProcessForms();

if($redemptioncommissionform->IsPostBack)
{
    $incentive = 2;
    if($redemptioncommissionform->GetPostVar("hiddengameid"))
    {
        $hiddengameid->Text = $redemptioncommissionform->GetPostVar("hiddengameid");
        $hiddenprodid->Text = $redemptioncommissionform->GetPostVar("hiddenprodid");
        $hiddenuserid->Text = $redemptioncommissionform->GetPostVar("hiddenuserid");
        $hiddenbatchvalidationid->Text = $redemptioncommissionform->GetPostVar("hiddenbatchvalidationid");
        $hiddenprocessedtag->Text = $redemptioncommissionform->GetPostVar("hiddenprocessedtag");
        $hiddenbatchid->Text = $redemptioncommissionform->GetPostVar("hiddenbatchid");
    }
    $gameid = $hiddengameid->Text;
    $prodid = $hiddenprodid->Text;
    $acctid = $hiddenuserid->Text;
    $gamebatchid = $hiddenbatchid->Text;
    $batchid = $hiddenbatchvalidationid->Text;
    $processedtag = $hiddenprocessedtag->Text;
    $_SESSION['gameid'] = $gameid;
    $_SESSION['prodid'] = $prodid;
    $_SESSION['acctid'] = $acctid;
    $_SESSION['batchid'] = $gamebatchid;

    if($processedtag == 1)
    {
        $virnstatus = "2,5";
        $ticketstatus = "1,2";
        $all_redemptionsumm = $tmwinnings->GetRedemptionSummaryPerBatchWithLimit($gameid,$prodid,$acctid,$batchid,($pgcon->SelectedItemFrom - 1),$itemsperpage, $virnstatus, $ticketstatus);
        $redemptionsumm = $tmwinnings->GetRedemptionSummaryPerBatch($gameid,$prodid,$acctid,$batchid, $virnstatus, $ticketstatus);
        $prizesredeemed = $tmwinnings->GetTotalPrizesRedeemedPerBatchID($gameid,$prodid,$acctid,$batchid, $virnstatus, $ticketstatus);
    }
    else
    {
        $all_redemptionsumm = $tmwinnings->GetRedemptionSummaryPerBatchWithLimit2($gameid,$prodid,$acctid,$batchid,($pgcon->SelectedItemFrom - 1),$itemsperpage, 0, 0);
        $redemptionsumm = $tmwinnings->GetRedemptionSummaryPerBatch2($gameid,$prodid,$acctid,$batchid, 0, 0);
        $prizesredeemed = $tmwinnings->GetTotalPrizesRedeemedPerBatchID2($gameid,$prodid,$acctid,$batchid, 0, 0);
    }

    $redemptionsumm_list = new ArrayList();
    $redemptionsumm_list->AddArray($redemptionsumm);

    $total_prizes_redeemed = $prizesredeemed[0]["Total"];
    $total_redemption_comm = ($total_prizes_redeemed * ($incentive / 100));

    $recordcount = count($all_redemptionsumm);
    $pgcon->Initialize($itemsperpage, $recordcount);
    $pgTransactionHistory = $pgcon->PreRender();
    $current_page = $pgcon->SelectedItemFrom;
    $total_page = ceil(($recordcount/$itemsperpage));

    if($btnBack->SubmittedValue == "Back")
    {
        if($hiddenprocessedtag->Text == 1)
        {
            header("Location: retailerreimbursement.php");
        }
        else
        {
            header("Location: batchredemption.php");
        }
    }
}
else
{
    header("Location: retailerreimbursement.php");
}
?>