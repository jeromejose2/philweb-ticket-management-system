<?php
/*
 * Author: Noel Antonio
 * Date Created: 2012-01-06
 * Company: Philweb Corporation
 */

require_once ('../jpgraph/jpgraph.php');
require_once ('../jpgraph/jpgraph_bar.php');

$pagesubmenuid = 40;
$stylesheets[] = "css/default.css";

App::LoadModuleClass("TicketManagementCM", "TMAccounts");
App::LoadModuleClass("TicketManagementCM", "TMDecks");
App::LoadModuleClass("TicketManagementCM", "TMWinnings");
App::LoadModuleClass("TicketManagementCM", "TMProducts");
App::LoadModuleClass("TicketManagementCM", "TMGameBatches");
App::LoadModuleClass("TicketManagementCM", "TMGameManagement");
App::LoadControl("TextBox");
App::LoadControl("Button");
App::LoadControl("ComboBox");
App::LoadControl("Radio");
App::LoadControl("Hidden");

/*Added by Noel D. Antonio 01-27-2012*/
App::LoadLibrary("fpdf/fpdf.php");

$tmaccounts = new TMAccounts();
$tmdecks = new TMDecks();
$tmwinnings = new TMWinnings();
$tmproducts = new TMProducts();
$tmgamebatches = new TMGameBatches();
$tmgamemgt = new TMGameManagement();

$fproc = new FormsProcessor();

$ddlrpttype = new ComboBox("ddlrpttype", "ddlrpttype", "Report Type: ");
$ddlrpttype->ShowCaption=true;
$rpttypeopt = null;
$rpttypeopt[] = new ListItem("Please Select", "0", true);
$rpttypeopt[] = new ListItem("Distributor", "1");
$rpttypeopt[] = new ListItem("Game Number", "2");
$rpttypeopt[] = new ListItem("Game Batch", "3");
$ddlrpttype->Items = $rpttypeopt;
$ddlrpttype->Args = "onchange = 'javascript: cboload();'";

$txtDate = new TextBox("txtDate", "txtDate", "Daily: ");
$txtDate->ShowCaption = true;
$txtDate->ReadOnly = true;
$txtDate->Style = "text-align: center";
$txtDate->Args = "size='9'";
$txtDate->Text = date('m/d/Y');

//get list of game names
$gamenames = $tmproducts->SelectAllGameName();
$gamename_list = new ArrayList();
$gamename_list->AddArray($gamenames);
$ddlgamename = new ComboBox("ddlgamename","ddlgamename","Game Name: ");
$ddlgamename->Args = "onchange = 'javascript: get_gamenumber();'";
$gamenameopt = null;
$gamenameopt[] = new ListItem("Please select", "0", true);
$ddlgamename->Items = $gamenameopt;
$ddlgamename->DataSource = $gamename_list;
$ddlgamename->DataSourceText = "ProductName";
$ddlgamename->DataSourceValue = "ProductID";
$ddlgamename->DataBind();
$ddlgamename->ShowCaption = true;

$ddlgamenum = new ComboBox("ddlgamenum", "ddlgamenum", "Game Number: ");
$ddlgamenum->ShowCaption = true;
$ddlgamenum->Args = "onchange='javascript: get_batchID(); get_gamename();'";
$gamenumbers = $tmgamemgt->SelectByGameType();
$gamename_list = new ArrayList();
$gamename_list->AddArray($gamenumbers);
$litem = null;
$litem[] = new ListItem("Please Select", "0", true);
$ddlgamenum->Items = $litem;
$ddlgamenum->DataSource = $gamename_list;
$ddlgamenum->DataSourceText = "GameNumber";
$ddlgamenum->DataSourceValue = "GameID";
$ddlgamenum->DataBind();

$ddlbatchid = new ComboBox("ddlbatchid", "ddlbatchid", "Game Batch: ");
$ddlbatchid->ShowCaption = true;
$ddlbatchidopt = null;
$ddlbatchidopt[] = new ListItem("Please Select", "", true);
$ddlbatchid->Items = $ddlbatchidopt;

$ddlweekly = new ComboBox("ddlweekly","ddlweekly","Weekly: ");
$ddlweekly->ShowCaption = true;
$weeklyopt = null;
$weeklyopt[] = new ListItem("Please Select", "0", true);
$weeklyopt[] = new ListItem("1 Week", "1");
$weeklyopt[] = new ListItem("2 Weeks", "2");
$weeklyopt[] = new ListItem("1 Month", "3");
$ddlweekly->Items = $weeklyopt;

$rdoDaily = new Radio("rdoPeriod", "rdoDaily", "");
$rdoDaily->Value = 1;
$rdoDaily->Checked = true;
$rdoWeekly = new Radio("rdoPeriod", "rdoWeekly", "");
$rdoWeekly->Value = 2;

$btnSubmit = new Button("btnSubmit", "btnSubmit", "Submit");
$btnSubmit->IsSubmit = true;
$btnSubmit->Args = "onclick='javascript: return validate();'";

$btnExport = new Button("btnExport","btnExport","Export to PDF");
$btnExport->IsSubmit = true;

$hiddenflag = new Hidden("hiddenflag","hiddenflag","Hidden Flag");
$xmltype = new Hidden("xmltype","xmltype","xmltype");

$fproc->AddControl($txtDate);
$fproc->AddControl($ddlgamename);
$fproc->AddControl($ddlgamenum);
$fproc->AddControl($ddlbatchid);
$fproc->AddControl($ddlrpttype);
$fproc->AddControl($ddlweekly);
$fproc->AddControl($rdoDaily);
$fproc->AddControl($rdoWeekly);
$fproc->AddControl($btnSubmit);
$fproc->AddControl($btnExport);
$fproc->AddControl($hiddenflag);
$fproc->AddControl($xmltype);

$fproc->ProcessForms();

$data = array();
$date = array();
    

if ($fproc->IsPostBack)
{
    $gamenumbers = $tmproducts->SelectGameNumPerGameName($ddlgamename->SubmittedValue);
    $gamename_list = new ArrayList();
    $gamename_list->AddArray($gamenumbers);
    $ddlgamenum->DataSource = $gamename_list;
    $ddlgamenum->DataSourceText = "GameNumber";
    $ddlgamenum->DataSourceValue = "GameID";
    $ddlgamenum->DataBind();
    $ddlgamenum->SetSelectedValue($ddlgamenum->SubmittedValue);
    
    $batch = $tmgamebatches->SelectDistinctBatchID($ddlgamenum->SubmittedValue);
    $ddlbatchidopt = null;
    $ddlbatchidopt[] = new ListItem("ALL", "0");
    $ddlbatchid->Items = $ddlbatchidopt;
    $batch_list = new ArrayList();
    $batch_list->AddArray($batch);
    $ddlbatchid->DataSource = $batch_list;
    $ddlbatchid->DataSourceText = "BatchID";
    $ddlbatchid->DataSourceValue = "GameBatchID";
    $ddlbatchid->DataBind();
    $ddlbatchid->SetSelectedValue($ddlbatchid->SubmittedValue);
    
    $ds = new DateSelector("now");
    
    if ($btnSubmit->SubmittedValue == "Submit")
    {
        if ($fproc->GetPostVar("rdoPeriod") == 1) // Daily Sales
        {
            $title = "Sales for ".date("M d, Y", strtotime($txtDate->SubmittedValue)) . " 12:00:00 AM to 11:59:59 PM";
            $datefrom = date("Y-m-d", strtotime($txtDate->SubmittedValue));
            $dateto = date("Y-m-d", strtotime($txtDate->SubmittedValue));
            $_SESSION['datefrom'] = $datefrom;
            $_SESSION['dateto'] = $dateto;
            $alldates[] = date("Y-m-d", strtotime($txtDate->SubmittedValue));
            $rdoDaily->Checked = true;
        }
        else // Weekly Sales
        {
            $rdoWeekly->Checked = true;
            if ($fproc->GetPostVar("ddlweekly"))
            {
                $selectedperiod = $fproc->GetPostVar("ddlweekly");
                $dateto = $ds->CurrentDate;
                $dsfr = new DateSelector($dateto);

                switch ($selectedperiod)
                {
                    case 1: $dsfr->AddWeeks(-1);
                        $title = "1 Week";
                        break;
                    case 2: $dsfr->AddWeeks(-2);
                        $title = "2 Weeks";
                        break;
                    case 3: $dsfr->AddMonths(-1);
                        $title = "1 Month";
                        break;
                }
            }
            $datefrom = date("Y-m-d", strtotime($dsfr->CurrentDate));
            $dateto = date("Y-m-d", strtotime($ds->CurrentDate));
            $_SESSION['datefrom'] = $datefrom;
            $_SESSION['dateto'] = $dateto;
            
            $start = strtotime($datefrom);
            $end = strtotime($dateto);
            $days_between = ceil(abs($end - $start) / 86400);
            $dsall = new DateSelector("now");
            $dsall->SetDate($dateto, "Y-m-d");
            for ($i=0;$i<$days_between;$i++)
            {
                $alldates[] = $dsall->CurrentDate;
                $dsall->AddDays(-1);
            } 
        }
        
        if ($ddlrpttype->SubmittedValue == 1) // Distributor
        {
            $xtitle = "Distributor";
            $sales = $tmdecks->GetSales($datefrom, $dateto, $ddlgamenum->SubmittedValue, $ddlbatchid->SubmittedValue); 
            $salesgraph = $tmdecks->GetDistibutorSalesforGraph($datefrom, $dateto, $ddlgamenum->SubmittedValue, $ddlbatchid->SubmittedValue); 
            foreach($salesgraph as $rec)
            {
                $datay[] = $rec["Sales"];
                $datax[] = $rec["xAxis"];
            }
        }
        elseif ($ddlrpttype->SubmittedValue == 2) // Game Number
        {
            $xtitle = "Date Released";
            $sales = $tmdecks->GetSales($datefrom, $dateto, $ddlgamenum->SubmittedValue, 0); 
            $salesgraph = $tmdecks->GetDateSalesforGraph($datefrom, $dateto, $ddlgamenum->SubmittedValue, 0); 
            
            if ($fproc->GetPostVar("rdoPeriod") == 1) 
            {
                foreach($salesgraph as $rec)
                {
                    $datay[] = $rec["Sales"];
                    $datax[] = $rec["xAxis"];
                }
            }
            else
            {
                foreach($salesgraph as $rec)
                {
                    $data[$rec["xAxis"]] = $rec["Sales"];
                    $date[] = $rec["xAxis"];
                }

                $temp = array(); 
                for ($i=0;$i<count($alldates);$i++)
                {
                    $chkArray = in_array($alldates[$i], $date);
                    if ($chkArray)
                    {
                        $temp[$i]['Sales'] = $data[$alldates[$i]];
                        $temp[$i]['xAxis'] = $alldates[$i];
                    }
                    else
                    {
                        $temp[$i]['Sales'] = 0;
                        $temp[$i]['xAxis'] = $alldates[$i];
                    } 
                }
                $temp1 = array();
                $total_sales = 0;
                $total_count = count($salesgraph);
                $lastdate = '';
                foreach($temp as $sg) 
                {
                    $cntr++;
                    $total_sales += $sg['Sales'];

                    if($lastdate == '') {
                        $lastdate = date("M d", strtotime($sg['xAxis']));
                    }
                    if($cntr % 7 == 0) {
                        $temp1[$cntr]['Sales'] = $total_sales;
                        $temp1[$cntr]['xAxis'] = date("M d", strtotime($sg['xAxis'])) . ' - ' . $lastdate;
                        $total_sales = 0;
                        $lastdate = '';
                    }
                }
                $temp1 = array_reverse($temp1);
                foreach($temp1 as $rec)
                {
                    $datay[] = $rec["Sales"];
                    $datax[] = $rec["xAxis"];
                }
            }
        }
        else // Game Batch
        {
            $xtitle = "Date Released";
            $sales = $tmdecks->GetSales($datefrom, $dateto, $ddlgamenum->SubmittedValue, $ddlbatchid->SubmittedValue); 
            $salesgraph = $tmdecks->GetDateSalesforGraph($datefrom, $dateto, $ddlgamenum->SubmittedValue, $ddlbatchid->SubmittedValue); 
            if ($fproc->GetPostVar("rdoPeriod") == 1) 
            {
                foreach($salesgraph as $rec)
                {
                    $datay[] = $rec["Sales"];
                    $datax[] = $rec["xAxis"];
                }
            }
            else
            {
                foreach($salesgraph as $rec)
                {
                    $data[$rec["xAxis"]] = $rec["Sales"];
                    $date[] = $rec["xAxis"];
                }

                $temp = array(); 
                for ($i=0;$i<count($alldates);$i++)
                {
                    $chkArray = in_array($alldates[$i], $date);
                    if ($chkArray)
                    {
                        $temp[$i]['Sales'] = $data[$alldates[$i]];
                        $temp[$i]['xAxis'] = $alldates[$i];
                    }
                    else
                    {
                        $temp[$i]['Sales'] = 0;
                        $temp[$i]['xAxis'] = $alldates[$i];
                    } 
                }
                $temp1 = array();
                $total_sales = 0;
                $total_count = count($salesgraph);
                $lastdate = '';
                foreach($temp as $sg) 
                {
                    $cntr++;
                    $total_sales += $sg['Sales'];

                    if($lastdate == '') {
                        $lastdate = date("M d", strtotime($sg['xAxis']));
                    }
                    if($cntr % 7 == 0) {
                        $temp1[$cntr]['Sales'] = $total_sales;
                        $temp1[$cntr]['xAxis'] = date("M d", strtotime($sg['xAxis'])) . ' - ' . $lastdate;
                        $total_sales = 0;
                        $lastdate = '';
                    }
                }
                $temp1 = array_reverse($temp1);
                foreach($temp1 as $rec)
                {
                    $datay[] = $rec["Sales"];
                    $datax[] = $rec["xAxis"];
                }
            }
        }
        
        $sales = $sales[0]["Sales"];   
        
        if (count($salesgraph) >= 1) // Setup the graph
        {
            $graph = new Graph(1100,650);
            $graph->SetScale("textlin");
            $theme_class=new UniversalTheme;
            $graph->SetTheme($theme_class);
            $graph->img->SetAntiAliasing(false);
            $graph->title->Set($title);
            $graph->SetBox(false);
            $graph->yaxis->HideTicks(false,false);
            $graph->xgrid->Show();
            $graph->xaxis->SetTickLabels($datax);
            $graph->xaxis->SetTitle($xtitle, 'middle');
            $graph->yaxis->SetTitle('Number of Tickets Sold', 'middle');
            $graph->xgrid->SetColor('#E3E3E3');
            $p1 = new BarPlot($datay);
            $graph->Add($p1);
            $p1->SetColor("#c50909");
            $p1->SetLegend('Total Tickets Sold');
            $graph->legend->SetFrameWeight(1);
            $graph->Stroke('../views/images_sales/salesgraph.png');
            $display = "SUCCESSFUL DISPLAY";
        }
        
    }
    
    if($btnExport->SubmittedValue == "Export to PDF")
    {
        $datefrom = $_SESSION['datefrom'];
        $dateto = $_SESSION['dateto'];
        $sales1 = $tmdecks->GetSales($datefrom, $dateto, $ddlgamenum->SubmittedValue, $ddlbatchid->SubmittedValue); 
        $pdf = new FPDF('L','mm','Legal');
        $pdf->AddPage();
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(0,5,"Sales Summary",0,0,'C');
        $pdf->Ln(8);
        $pdf->SetFont('Arial','B',9);
        $pdf->SetX(140);
        $pdf->Cell(40,5,'Total Tickets Sold :',1,0,'C');
        if ($sales1[0]["Sales"] != 0)
        {
            $pdf->SetFont('Arial','',9);
            $pdf->SetX(180);
            $pdf->Cell(40,5,$sales1[0]["Sales"],1,1,'C');
            $jpg_name = '../views/images_sales/salesgraph'.'.png';
            $image_info = getimagesize($jpg_name);
            $img_width = $image_info[0];
            $img_height = $image_info[1];
            $new_img_height = 117;
            $img_ratio = ($new_img_height/$img_height);
            $new_img_width = $img_width * $img_ratio;
            $pdf->Image($jpg_name, 60, 30, 250, 150);
        }
        else
        {
            $pdf->SetX(180);
            $pdf->Cell(40,5,'No records found',1,1,'C');
        }
        
        $pdf->Output('Sales Summary','D');
        Header('Content-Type: application/pdf');
    }
        

}
?>