<?php
/*
 * Author: Noel Antonio
 * Date Created: 2012-01-06
 * Company: Philweb Corporation
 */

require_once ('../jpgraph/jpgraph.php');
require_once ('../jpgraph/jpgraph_bar.php');

$pagesubmenuid = 40;
$stylesheets[] = "css/default.css";

App::LoadModuleClass("TicketManagementCM", "TMAccounts");
App::LoadModuleClass("TicketManagementCM", "TMDecks");
App::LoadModuleClass("TicketManagementCM", "TMWinnings");
App::LoadModuleClass("TicketManagementCM", "TMProducts");
App::LoadModuleClass("TicketManagementCM", "TMGameBatches");
App::LoadModuleClass("TicketManagementCM", "TMGameManagement");
App::LoadControl("TextBox");
App::LoadControl("Button");
App::LoadControl("ComboBox");
App::LoadControl("Radio");
App::LoadControl("Hidden");

/*Added by Noel D. Antonio 01-27-2012*/
App::LoadLibrary("fpdf/fpdf.php");

$tmaccounts = new TMAccounts();
$tmdecks = new TMDecks();
$tmwinnings = new TMWinnings();
$tmproducts = new TMProducts();
$tmgamebatches = new TMGameBatches();
$tmgamemgt = new TMGameManagement();

$fproc = new FormsProcessor();

$txtDateFr = new TextBox("txtDateFr", "txtDateFr", "From : ");
$txtDateFr->ShowCaption = true;
$txtDateFr->ReadOnly = true;
$txtDateFr->Style = "text-align: center";
$txtDateFr->Args = "size='9'";
$txtDateFr->Text = date('m/d/Y');

$txtDateTo = new TextBox("txtDateTo", "txtDateTo", "To : ");
$txtDateTo->ShowCaption = true;
$txtDateTo->ReadOnly = true;
$txtDateTo->Style = "text-align: center";
$txtDateTo->Args = "size='9'";
$txtDateTo->Text = date('m/d/Y');

//get list of game names
$gamenames = $tmproducts->SelectAllGameName();
$gamename_list = new ArrayList();
$gamename_list->AddArray($gamenames);
$ddlgamename = new ComboBox("ddlgamename","ddlgamename","Game Name: ");
$ddlgamename->Args = "onchange = 'javascript: get_gamenumber();'";
$gamenameopt = null;
$gamenameopt[] = new ListItem("Please select", "0", true);
$ddlgamename->Items = $gamenameopt;
$ddlgamename->DataSource = $gamename_list;
$ddlgamename->DataSourceText = "ProductName";
$ddlgamename->DataSourceValue = "ProductID";
$ddlgamename->DataBind();
$ddlgamename->ShowCaption = true;

$ddlgamenum = new ComboBox("ddlgamenum", "ddlgamenum", "Game Number: ");
$ddlgamenum->ShowCaption = true;
$ddlgamenum->Args = "onchange='javascript: get_batchID(); get_gamename();'";
$gamenumbers = $tmgamemgt->SelectByGameType();
$gamename_list = new ArrayList();
$gamename_list->AddArray($gamenumbers);
$litem = null;
$litem[] = new ListItem("Please Select", "0", true);
$ddlgamenum->Items = $litem;
$ddlgamenum->DataSource = $gamename_list;
$ddlgamenum->DataSourceText = "GameNumber";
$ddlgamenum->DataSourceValue = "GameID";
$ddlgamenum->DataBind();

$ddlbatchid = new ComboBox("ddlbatchid", "ddlbatchid", "Game Batch: ");
$ddlbatchid->ShowCaption = true;
$ddlbatchidopt = null;
$ddlbatchidopt[] = new ListItem("Please Select", "", true);
$ddlbatchid->Items = $ddlbatchidopt;

$btnSubmit = new Button("btnSubmit", "btnSubmit", "Submit");
$btnSubmit->IsSubmit = true;
$btnSubmit->Args = "onclick='javascript: return validate();'";

$btnExport = new Button("btnExport","btnExport","Export to PDF");
$btnExport->IsSubmit = true;

$hiddenflag = new Hidden("hiddenflag","hiddenflag","Hidden Flag");
$xmltype = new Hidden("xmltype","xmltype","xmltype");

$fproc->AddControl($txtDateFr);
$fproc->AddControl($txtDateTo);
$fproc->AddControl($ddlgamename);
$fproc->AddControl($ddlgamenum);
$fproc->AddControl($ddlbatchid);
$fproc->AddControl($btnSubmit);
$fproc->AddControl($btnExport);
$fproc->AddControl($hiddenflag);
$fproc->AddControl($xmltype);

$fproc->ProcessForms();

$data = array();
$date = array();
    

if ($fproc->IsPostBack)
{
    $gamenumbers = $tmproducts->SelectGameNumPerGameName($ddlgamename->SubmittedValue);
    $gamename_list = new ArrayList();
    $gamename_list->AddArray($gamenumbers);
    $ddlgamenum->DataSource = $gamename_list;
    $ddlgamenum->DataSourceText = "GameNumber";
    $ddlgamenum->DataSourceValue = "GameID";
    $ddlgamenum->DataBind();
    $ddlgamenum->SetSelectedValue($ddlgamenum->SubmittedValue);
    
    $batch = $tmgamebatches->SelectDistinctBatchID($ddlgamenum->SubmittedValue);
    $ddlbatchidopt = null;
    $ddlbatchidopt[] = new ListItem("ALL", "0");
    $ddlbatchid->Items = $ddlbatchidopt;
    $batch_list = new ArrayList();
    $batch_list->AddArray($batch);
    $ddlbatchid->DataSource = $batch_list;
    $ddlbatchid->DataSourceText = "BatchID";
    $ddlbatchid->DataSourceValue = "GameBatchID";
    $ddlbatchid->DataBind();
    $ddlbatchid->SetSelectedValue($ddlbatchid->SubmittedValue);
    
    $ds = new DateSelector("now");
    
    if ($btnSubmit->SubmittedValue == "Submit")
    {
        $title = "Sales from ".date("M d, Y", strtotime($txtDateFr->SubmittedValue)) . " to " . date("M d, Y", strtotime($txtDateTo->SubmittedValue))." 12:00:00 AM to 11:59:59 PM";
        $datefrom = date("Y-m-d", strtotime($txtDateFr->SubmittedValue));
        $dateto = date("Y-m-d", strtotime($txtDateTo->SubmittedValue));
        $_SESSION['datefrom'] = $datefrom;
        $_SESSION['dateto'] = $dateto;
        
        $temp = getDates($datefrom, $dateto, $ddlgamenum->SubmittedValue, $ddlbatchid->SubmittedValue);
        
        $temp1 = array();
        $temp2 = array();
        $total_sales = 0;
        $total_count = count($temp);
        $lastdate = '';
        $month = '';
        foreach ($temp as $rec)
        {
            // Getting the daily sales
            $datay1[] = $rec["Sales"];
            $datax1[] = date("M d", strtotime($rec["xAxis"]));
            
            // Getting the weekly sales
            $cntr++;
            $total_sales += $rec['Sales'];
            
            if($lastdate == '') {
                $lastdate = date("M d", strtotime($rec['xAxis']));
            }
            if($cntr % 7 == 0) {
                $temp1[$cntr]['Sales'] = $total_sales;
                $temp1[$cntr]['xAxis'] = $lastdate . ' - ' . date("M d", strtotime($rec['xAxis']));
                $total_sales = 0;
                $lastdate = '';
            }
            else if ($cntr == $total_count)
            {
                $temp1[$cntr]['Sales'] = $total_sales;
                $temp1[$cntr]['xAxis'] = $lastdate . ' - ' . date("M d", strtotime($rec['xAxis']));
            }
            
            $month = date("m", strtotime($rec["xAxis"]));
            switch ($month)
            {
                case "01": $JanSales += $rec["Sales"]; break;
                case "02": $FebSales += $rec["Sales"]; break;
                case "03": $MarSales += $rec["Sales"]; break;
                case "04": $AprSales += $rec["Sales"]; break;
                case "05": $MaySales += $rec["Sales"]; break;
                case "06": $JunSales += $rec["Sales"]; break;
                case "07": $JulSales += $rec["Sales"]; break;
                case "08": $AugSales += $rec["Sales"]; break;
                case "09": $SepSales += $rec["Sales"]; break;
                case "10": $OctSales += $rec["Sales"]; break;
                case "11": $NovSales += $rec["Sales"]; break;
                case "12": $DecSales += $rec["Sales"]; break;
            }
        }
        $datay3 = array($JanSales,$FebSales,$MarSales,$AprSales,$MaySales,$JunSales,$JulSales,$AugSales,$SepSales,$OctSales,$NovSales,$DecSales);
        $datax3 = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
        foreach($temp1 as $rec)
        {
            $datay2[] = $rec["Sales"];
            $datax2[] = $rec["xAxis"];
        }
        
        $sales_list = new Arraylist();
        $sales_list->AddArray($temp);
        
        $ds->SetDate("now");
        $monthtodatesales = $tmdecks->GetSales($ds->GetFirstDayOfMonth() ,$ds->CurrentDate, $ddlgamenum->SubmittedValue, $ddlbatchid->SubmittedValue);
        $year = $ds->GetCurrentDateFormat("Y-");
        $ds->SetDate($year . "01-01");
        $from = $ds->CurrentDate;
        $ds->SetDate("now");
        $yeartodatesales = $tmdecks->GetSales($from, $ds->CurrentDate, $ddlgamenum->SubmittedValue, $ddlbatchid->SubmittedValue);
  
        $graph1 = createGraph($datax1, $datay1, $title, "Date Released", "No. of Tickets Sold", "Total Tickets Sold", "salesgraph1.png");
        $graph2 = createGraph($datax2, $datay2, $title, "Date Released", "No. of Tickets Sold", "Total Tickets Sold", "salesgraph2.png");
        $graph3 = createGraph($datax3, $datay3, $title, "Date Released", "No. of Tickets Sold", "Total Tickets Sold", "salesgraph3.png");
    }
    
    if($btnExport->SubmittedValue == "Export to PDF")
    {
        $datefrom = $_SESSION['datefrom'];
        $dateto = $_SESSION['dateto'];
        $gamename = $ddlgamename->SelectedText;
        $gameno = $ddlgamenum->SelectedText;
        $gamebatch = $ddlbatchid->SelectedText;
        
        $temp = getDates($datefrom, $dateto, $ddlgamenum->SubmittedValue, $ddlbatchid->SubmittedValue);
        
        $sales_list = new ArrayList();
        $sales_list->AddArray($temp);
        
        $ds->SetDate("now");
        $monthtodatesales = $tmdecks->GetSales($ds->GetFirstDayOfMonth() ,$ds->CurrentDate, $ddlgamenum->SubmittedValue, $ddlbatchid->SubmittedValue);
        $year = $ds->GetCurrentDateFormat("Y-");
        $ds->SetDate($year . "01-01");
        $from = $ds->CurrentDate;
        $ds->SetDate("now");
        $yeartodatesales = $tmdecks->GetSales($from, $ds->CurrentDate, $ddlgamenum->SubmittedValue, $ddlbatchid->SubmittedValue);
        
        // Set up the PDF
        $pdf = new FPDF('L','mm','Legal');
        $pdf->AddPage();
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(0,5,"Sales Summary",0,0,'C');
        $pdf->Ln(8);
        $pdf->SetFont('Arial','B',9);
        $pdf->SetX(40);
        $pdf->Cell(40,5,'Game Name:',1,0,'C');
        $pdf->SetFont('Arial','',9);
        $pdf->SetX(80);
        $pdf->Cell(40,5,$gamename,1,0,'C');
        $pdf->SetFont('Arial','B',9);
        $pdf->SetX(120);
        $pdf->Cell(40,5,'Game Number:',1,0,'C');
        $pdf->SetFont('Arial','',9);
        $pdf->SetX(160);
        $pdf->Cell(40,5,$gameno,1,0,'C');
        $pdf->SetFont('Arial','B',9);
        $pdf->SetX(200);
        $pdf->Cell(40,5,'Game Batch:',1,0,'C');
        $pdf->SetFont('Arial','',9);
        $pdf->SetX(240);
        $pdf->Cell(40,5,$gamebatch,1,0,'C');
        
        $pdf->Ln(8);
        $pdf->SetFont('Arial','B',9);
        $pdf->SetX(40);
        $pdf->Cell(80,5,'',1,1,'C');
        $pdf->SetX(40);
        $pdf->Cell(40,5,'Date Released',1,0,'C');
        $pdf->SetX(80);
        $pdf->Cell(40,5,'Tickets Sold',1,1,'C');
        $pdf->SetFont('Arial','',9);
        if(count($sales_list) > 0)
        {
            for($i = 0 ; $i < count($sales_list) ; $i++)
            {
                $pdf->SetX(40);
                $pdf->Cell(40,5,$sales_list[$i]['xAxis'],1,0,'C');
                $pdf->Cell(40,5,number_format($sales_list[$i]['Sales']),1,1,'C');
            }
        }
        else
        {
            $pdf->SetX(40);
            $pdf->Cell(270,5,'No records found',1,1,'C');
        }
        
        $pdf->Ln(8);
        $pdf->SetFont('Arial','B',9);
        $pdf->SetX(40);
        $pdf->Cell(80,5,'',1,1,'C');
        $pdf->SetX(40);
        $pdf->Cell(40,5,'Month to Date Sales :',1,0,'C');
        $pdf->SetFont('Arial','',9);
        $pdf->SetX(80);
        $pdf->Cell(40,5,number_format($monthtodatesales[0]["Sales"]),1,1,'C');
        $pdf->SetFont('Arial','B',9);
        $pdf->SetX(40);
        $pdf->Cell(40,5,'Year to Date Sales :',1,0,'C');
        $pdf->SetFont('Arial','',9);
        $pdf->SetX(80);
        $pdf->Cell(40,5,number_format($yeartodatesales[0]["Sales"]),1,1,'C');

        $pdf->AddPage();
        $img_name1 = '../views/images_sales/salesgraph1'.'.png';
        $pdf->Image($img_name1, 60, 30, 250, 150);
        $pdf->AddPage();
        $img_name2 = '../views/images_sales/salesgraph2'.'.png';
        $pdf->Image($img_name2, 60, 30, 250, 150);
        $pdf->AddPage();
        $img_name3 = '../views/images_sales/salesgraph3'.'.png';
        $pdf->Image($img_name3, 60, 30, 250, 150);
        
        $pdf->Output('Sales Summary','D');
        Header('Content-Type: application/pdf');
    }
        

}

function getDates($datefr, $dateto, $gameno, $batchid)
{
        $tmdecks = new TMDecks();
        $days_between = ceil(abs(strtotime($dateto) - strtotime($datefr)) / 86400);
        $ds = new DateSelector($dateto);
        for ($i=0;$i<=$days_between;$i++)
        {
            $alldates[] = $ds->CurrentDate;
            $ds->AddDays(-1);
        } 
        $sales = $tmdecks->GetDateSalesforGraph($datefr, $dateto, $gameno, $batchid);
        foreach($sales as $rec)
        {
            $data[$rec["xAxis"]] = $rec["Sales"];
            $date[] = $rec["xAxis"];
        }

        $temp = array(); 
        for ($i=0;$i<count($alldates);$i++)
        {
            $chkArray = in_array($alldates[$i], $date);
            if ($chkArray)
            {
                $temp[$i]['Sales'] = $data[$alldates[$i]];
                $temp[$i]['xAxis'] = $alldates[$i];
            }
            else
            {
                $temp[$i]['Sales'] = 0;
                $temp[$i]['xAxis'] = $alldates[$i];
            } 
        }
        $temp = array_reverse($temp);
        return $temp;
}

function createGraph($xdata, $ydata, $title, $xtitle, $ytitle, $legend, $img_name)
{
    $graph = new Graph(1500,650);
    $graph->SetScale("textlin");
    $theme_class=new UniversalTheme;
    $graph->SetTheme($theme_class);
    $graph->img->SetAntiAliasing(false);
    $graph->title->Set($title);
    $graph->SetBox(false);
    $graph->yaxis->HideTicks(false,false);
    $graph->xgrid->Show();
    $graph->xaxis->SetTickLabels($xdata);
    $graph->xaxis->SetTitle($xtitle, 'middle');
    $graph->yaxis->SetTitle($ytitle, 'middle');
    $graph->SetMargin(100, 100, 100, 100);
    $graph->yaxis->SetTitleMargin(70);
    $graph->yaxis->SetLabelFormatCallback("number_format");
    $graph->xgrid->SetColor('#E3E3E3');
    $p1 = new BarPlot($ydata);
    $graph->Add($p1);
    $p1->SetColor("#c50909");
    $p1->SetLegend($legend);
    $graph->legend->SetFrameWeight(1);
    $graph->Stroke('../views/images_sales/'.$img_name);
    return true;
}
?>