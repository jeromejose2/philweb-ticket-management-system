<?php
/*
 * Author: Noel Antonio
 * Date Created: 2012-01-06
 * Company: Philweb Corporation
 */

require_once ('../jpgraph/jpgraph.php');
require_once ('../jpgraph/jpgraph_bar.php');

$pagesubmenuid = 40;
$stylesheets[] = "css/default.css";

App::LoadModuleClass("TicketManagementCM", "TMAccounts");
App::LoadModuleClass("TicketManagementCM", "TMDecks");
App::LoadModuleClass("TicketManagementCM", "TMProducts");
App::LoadModuleClass("TicketManagementCM", "TMGameManagement");
App::LoadControl("TextBox");
App::LoadControl("Button");
App::LoadControl("ComboBox");
App::LoadControl("Radio");
App::LoadControl("Hidden");

$tmaccounts = new TMAccounts();
$tmdecks = new TMDecks();
$tmproducts = new TMProducts();
$tmgamemgt = new TMGameManagement();

$fproc = new FormsProcessor();

$ddlrpttype = new ComboBox("ddlrpttype", "ddlrpttype", "View By: ");
$ddlrpttype->ShowCaption=true;
$rpttypeopt = null;
$rpttypeopt[] = new ListItem("Please Select", "0", true);
$rpttypeopt[] = new ListItem("Daily", "1");
$rpttypeopt[] = new ListItem("Weekly", "2");
$rpttypeopt[] = new ListItem("Monthly", "3");
$rpttypeopt[] = new ListItem("Distributor - Daily", "4");
$rpttypeopt[] = new ListItem("Distributor - Weekly", "5");
$rpttypeopt[] = new ListItem("Distributor - Monthly", "6");
$ddlrpttype->Items = $rpttypeopt;
$ddlrpttype->Args = "onchange = 'javascript: checkreporttype();'";

$ddlcardvalue = new ComboBox('ddlcardvalue','ddlcardvalue', 'Card Value: ');
$ddlcardvalue->ShowCaption = true;
$ddlcardvalue->Args = "onchange='javascript: get_gamename();'";
$cardprize = $tmgamemgt->SelectByCardValue();
$cardprize_list = new ArrayList();
$cardprize_list->AddArray($cardprize);
$cardlist = null;
$cardlist[] = new ListItem("ALL","0",false);
$ddlcardvalue->Items = $cardlist;
$ddlcardvalue->DataSource = $cardprize_list;
$ddlcardvalue->DataSourceText = "CardPrice";
$ddlcardvalue->DataSourceValue = "CardPrice";
$ddlcardvalue->DataBind();

//$gamenames = $tmproducts->SelectAllGameName();
//$gamename_list = new ArrayList();
//$gamename_list->AddArray($gamenames);
$ddlgamename = new ComboBox("ddlgamename","ddlgamename","Game Name: ");
$ddlgamename->Args = "onchange = 'javascript: get_gamenumber();'";
$ddlgamename->ShowCaption = true;
$gamenameopt = null;
$gamenameopt[] = new ListItem("ALL", "0", true);
$ddlgamename->Items = $gamenameopt;
//$ddlgamename->DataSource = $gamename_list;
//$ddlgamename->DataSourceText = "ProductName";
//$ddlgamename->DataSourceValue = "ProductID";
//$ddlgamename->DataBind();

//$games = $tmgamemgt->SelectByGameType();
//$games_list = new ArrayList();
//$games_list->AddArray($games);
$ddlgametype = new ComboBox("ddlgametype", "ddlgametype", "Game Number: ");
$ddlgametype->Args = "onchange='javascript: get_batchID();'";
$litemgmetype = null;
$litemgmetype[] = new ListItem("ALL", "0", true);
$ddlgametype->Items = $litemgmetype;
$ddlgametype->ShowCaption = true;
//$ddlgametype->DataSource = $games_list;
//$ddlgametype->DataSourceText = "GameNumber";
//$ddlgametype->DataSourceValue = "GameID";
//$ddlgametype->DataBind();

$ddlgamebatch = new ComboBox("ddlgamebatch","ddlgamebatch","Game Batch:");
$ddlgamebatch->ShowCaption = true;
$gamebatch = null;
$gamebatch[] = new ListItem("ALL","0", true);
$ddlgamebatch->Items  = $gamebatch;

$flag = new Hidden("flag","flag","flag");
$xmltype = new Hidden("xmltype","xmltype","xmltype");

$btnExport = new Button("export","export","Export to PDF");
$btnExport->IsSubmit = true;

$btnExportCSV = new Button("btnExportCSV","btnExportCSV","Export to CSV");
$btnExportCSV->IsSubmit = true;

$txtDateFr = new TextBox("txtDateFr", "txtDateFr", "From : ");
$txtDateFr->ShowCaption = true;
$txtDateFr->ReadOnly = true;
$txtDateFr->Style = "text-align: center";
$txtDateFr->Args = "size='9'";
$txtDateFr->Text = date('Y-m-d');

$txtDateTo = new TextBox("txtDateTo", "txtDateTo", "To : ");
$txtDateTo->ShowCaption = true;
$txtDateTo->ReadOnly = true;
$txtDateTo->Style = "text-align: center";
$txtDateTo->Args = "size='9'";
$txtDateTo->Text = date('Y-m-d');

$ddlUsername = new ComboBox("ddlUsername","ddlUsername","Distributor : ");
$ddlUsername->ShowCaption = true;
$options = null;
$options[] = new ListItem("ALL", "0");
$ddlUsername->Items = $options;
$arrRetailers = $tmaccounts->SelectAllActiveRetailer();
$retailer_list = new ArrayList();
$retailer_list->AddArray($arrRetailers);
$ddlUsername->DataSource = $retailer_list;
$ddlUsername->DataSourceText = "UserName";
$ddlUsername->DataSourceValue = "AID";
$ddlUsername->DataBind();

$btnSubmit = new Button("btnSubmit", "btnSubmit", "Submit");
$btnSubmit->IsSubmit = true;
$btnSubmit->Args = "onclick='javascript: return validate();'";
$hiddenflag = new Hidden("hiddenflag","hiddenflag","Hidden Flag");

$fproc->AddControl($txtDateFr);
$fproc->AddControl($txtDateTo);
$fproc->AddControl($ddlgamename);
$fproc->AddControl($ddlcardvalue);
$fproc->AddControl($ddlgametype);
$fproc->AddControl($ddlgamebatch);
$fproc->AddControl($ddlUsername);
$fproc->AddControl($ddlrpttype);
$fproc->AddControl($btnSubmit);
$fproc->AddControl($btnExport);
$fproc->AddControl($hiddenflag);
$fproc->AddControl($btnExportCSV);
$fproc->AddControl($xmltype);
$fproc->AddControl($flag);

$fproc->ProcessForms();

$data = array();
$date = array();

if ($fproc->IsPostBack)
{
    $rpttypeval = $ddlrpttype->SubmittedValue;
    $var_cardval = $ddlcardvalue->SubmittedValue;
    $var_prod = $ddlgamename->SubmittedValue;
    $var_gameno = $ddlgametype->SubmittedValue;
    $var_batch = $ddlgamebatch->SubmittedValue;
    $var_user = $ddlUsername->SubmittedValue;
    
    if ($flag->SubmittedValue == 1 && $var_cardval != 0)
    {
        $ddlgamename->ClearItems();
        $gamenames = $tmproducts->SelectGameNamePerCardPrice($var_cardval);
        $gamename_list1 = new ArrayList();
        $gamename_list1->AddArray($gamenames);       
        $litemgmetype1 = null;
        $litemgmetype1[] = new ListItem("ALL", "0", false);
        $ddlgamename->Items = $litemgmetype1;
        $ddlgamename->DataSource = $gamename_list1;
        $ddlgamename->DataSourceText = "ProductName";
        $ddlgamename->DataSourceValue = "ProductID";
        $ddlgamename->DataBind();
        
        $ddlgametype->Args = "onchange='javascript: get_batchID(); '";
        $ddlgametype->ClearItems();
        $gamenumbers = $tmproducts->SelectGameNumPerGameName($var_prod);
        $gamenumbers_list1 = new ArrayList();
        $gamenumbers_list1->AddArray($gamenumbers);       
        $litemgmetype1 = null;
        $litemgmetype1[] = new ListItem("ALL", "0", false);
        $ddlgametype->Items = $litemgmetype1;
        $ddlgametype->DataSource = $gamenumbers_list1;
        $ddlgametype->DataSourceText = "GameNumber";
        $ddlgametype->DataSourceValue = "GameID";
        $ddlgametype->DataBind();
        $ddlgametype->SetSelectedValue($var_gameno);
    }
 
    $ddlgametype->SetSelectedValue($var_gameno);
    $ddlgamename->SetSelectedValue($var_prod);
   
    $gamebatchID = $tmproducts->SelectBatchIDPerGameNum($var_gameno, $xmltype->SubmittedValue);
    $batchlist = new ArrayList();
    $batchlist->AddArray($gamebatchID);
    $gamebatch = null;
    $gamebatch[] = new ListItem("ALL","0", true);
    $ddlgamebatch->Items  = $gamebatch;
    $ddlgamebatch->DataSource = $batchlist;
    $ddlgamebatch->DataSourceText = "BatchID";
    $ddlgamebatch->DataSourceValue = "GameBatchID";
    $ddlgamebatch->DataBind();
    $ddlgamebatch->SetSelectedValue($var_batch);
    
    if ($btnSubmit->SubmittedValue == "Submit")
    {
        $datefrom = date("Y-m-d", strtotime($txtDateFr->SubmittedValue));
        $dateto = date("Y-m-d", strtotime($txtDateTo->SubmittedValue));
        $_SESSION['datefrom'] = $datefrom;
        $_SESSION['dateto'] = $dateto;
        
        if (date("Y", strtotime($datefrom)) < date('Y'))
        {
            $ytd_dateto = date("Y-", strtotime($datefrom)) . '12-31';
        }
        else
        {
            $ytd_dateto = $dateto;
        }
        
        $mtd_dateto = date('Y-m-t', strtotime($datefrom));
        $startmonth = date('Y-m-', strtotime($datefrom)) . '01';
        $from = date("Y-", strtotime($datefrom)) . '01-01';
        
        if ($var_cardval == 0)
            $arrgames = $tmgamemgt->SelectAllGamesActive();
        else
            $arrgames = $tmgamemgt->getGameBySelectedProduct($var_cardval, $var_prod, $var_gameno);
        
        
        if ($var_user == 0)
            $users = $tmaccounts->SelectAllActiveRetailer();
        else
            $users = $tmaccounts->SelectByID($var_user);
        
        
        $sales_list = GetTicketSalesReport($arrgames, $dateto, $datefrom, $var_gameno, $var_prod, $var_batch, $var_user, $var_cardval);
        $sales_list2 = GetTSRDistributorSales($users, $dateto, $datefrom, $var_gameno, $var_prod, $var_batch, $var_user, $var_cardval);
        $temp = getDates($datefrom, $dateto, $var_prod, $var_gameno, $var_batch, $var_user, $var_cardval);
        
        if ($rpttypeval == 1)   // Daily
        {
            $display = 0;
            $title = "Daily Sales";
            foreach ($temp as $rec)
            {
                $datay[] = $rec["Sales"];
                $datax[] = date("M d", strtotime($rec["xAxis"]));
            }      
            $graph1 = createGraph($datax, $datay, $title, "Date Released", "No. of Tickets Sold", "Total Tickets Sold", "salesgraph1.png");       
        }
        
        
        else if ($rpttypeval == 2)   // Weekly
        {
            $display = 0;
            $temp = getDates($datefrom, $dateto, $var_prod, $var_gameno, $var_batch, $users[$x]["AID"], $var_cardval);
            $title = "Weekly Sales";
            $temp1 = converttoWeeks($temp);
            foreach ($temp1 as $rec)
            {
                $datay[] = $rec["Sales"];
                $datax[] = $rec["xAxis"];
            }
            $graph1 = createGraph($datax, $datay, $title, "Date Released", "No. of Tickets Sold", "Total Tickets Sold", "salesgraph1.png");       
            $sales_list = convertTSRtoWeeks($sales_list);
        }
        
        
        else if ($rpttypeval == 3)   // Monthly
        {
            $display = 0;
            $title = "Monthly Sales";
            foreach ($temp as $rec)
            {
                $month = date("m", strtotime($rec["xAxis"]));
                switch ($month)
                {
                    case "01": $JanSales += $rec["Sales"]; break;
                    case "02": $FebSales += $rec["Sales"]; break;
                    case "03": $MarSales += $rec["Sales"]; break;
                    case "04": $AprSales += $rec["Sales"]; break;
                    case "05": $MaySales += $rec["Sales"]; break;
                    case "06": $JunSales += $rec["Sales"]; break;
                    case "07": $JulSales += $rec["Sales"]; break;
                    case "08": $AugSales += $rec["Sales"]; break;
                    case "09": $SepSales += $rec["Sales"]; break;
                    case "10": $OctSales += $rec["Sales"]; break;
                    case "11": $NovSales += $rec["Sales"]; break;
                    case "12": $DecSales += $rec["Sales"]; break;
                }
            }
            $sales_list = convertTSRtoMonthly($arrgames, $dateto, $from, $var_gameno, $var_prod, $var_batch, $var_user, $var_cardval);
            $datay = array($JanSales,$FebSales,$MarSales,$AprSales,$MaySales,$JunSales,$JulSales,$AugSales,$SepSales,$OctSales,$NovSales,$DecSales);
            $datax = array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");         
            $graph1 = createGraph($datax, $datay, $title, "Date Released", "No. of Tickets Sold", "Total Tickets Sold", "salesgraph1.png");       
        }
        
        
        else if ($rpttypeval == 4) // Daily
        {
            $display = 1;
            $array = array();
            
            $graph = myGraph();
            
            if ($var_user == 0)
            {
                $title = "Distributor Daily Sales";
                for ($x=0;$x<count($users);$x++)
                {
                    $temp = getDates($datefrom, $dateto, $var_prod, $var_gameno, $var_batch, $users[$x]["AID"], $var_cardval);
                    foreach ($temp as $rec)
                    {
                        $datay[] = $rec["Sales"];
                        $datax[] = $rec["xAxis"];
                    }
                    $p1 = new BarPlot($datay);
                    $p1->SetColor("#c50909");
                    $p1->SetLegend($users[$x]["UserName"]);
                    $array[] = $p1;
                    $datay = '';
                }
                $gplot = new GroupBarPlot($array);
                $graph->Add($gplot);
            } else {
                $title = "Daily Sales \n".$ddlUsername->SelectedText;
                $temp = getDates($datefrom, $dateto, $var_prod, $var_gameno, $var_batch, $var_user, $var_cardval);
                foreach ($temp as $rec)
                {
                    $datay[] = $rec["Sales"];
                    $datax[] = $rec["xAxis"];
                }
                $p1 = new BarPlot($datay);
                $p1->SetColor("#c50909");
                $p1->SetLegend($ddlUsername->SelectedText);
                $graph->Add($p1);
            }
            $graph->title->Set($title);
            $graph->xaxis->SetTickLabels($datax);
            $graph->legend->SetFrameWeight(1);
            $graph->Stroke('../views/images_sales/'.'salesgraph1.png');
        }
        
        
        else if ($rpttypeval == 5) // Weekly
        {
            $display = 1;
            $sales_list2 = convertDistributorTSRtoWeeks($sales_list2);
            $array = array();
            
            $graph = myGraph();
            
            if ($var_user == 0)
            {
                $title = "Distributor Weekly Sales";
                for ($x=0;$x<count($users);$x++)
                {
                    $temp = getDates($datefrom, $dateto, $var_prod, $var_gameno, $var_batch, $users[$x]["AID"], $var_cardval);
                    $temp1 = converttoWeeks($temp);
                    foreach ($temp1 as $rec)
                    {
                        $datay[] = $rec["Sales"];
                        $datax[] = $rec["xAxis"];
                    }
                    $p1 = new BarPlot($datay);
                    $p1->SetColor("#c50909");
                    $p1->SetLegend($users[$x]["UserName"]);
                    $array[] = $p1;
                    $datay = '';
                }
                $gplot = new GroupBarPlot($array);
                $graph->Add($gplot);
            }
            else
            {
                $title = "Weekly Sales \n".$ddlUsername->SelectedText;
                $temp = getDates($datefrom, $dateto, $var_prod, $var_gameno, $var_batch, $var_user, $var_cardval);
                $temp1 = converttoWeeks($temp);
                foreach ($temp1 as $rec)
                {
                    $datay[] = $rec["Sales"];
                    $datax[] = $rec["xAxis"];
                }
                $p1 = new BarPlot($datay);
                $p1->SetColor("#c50909");
                $p1->SetLegend($ddlUsername->SelectedText);
                $graph->Add($p1);
            }
            
            $graph->title->Set($title);
            $graph->xaxis->SetTickLabels($datax);
            $graph->legend->SetFrameWeight(1);
            $graph->Stroke('../views/images_sales/'.'salesgraph1.png');
        }
        
        
        else if ($rpttypeval == 6)   // Monthly
        {   
            $display = 1;
            $sales_list2 = convertTSRDistributortoMonthly($users, $dateto, $from, $var_gameno, $var_prod, $var_batch, $var_user,$var_cardval);
            $graph = myGraph();
            
            if ($var_user == 0)
            {
                $title = "Distributor Monthly Sales";
                for ($x=0;$x<count($users);$x++)
                {
                    $temp = getDates($from, $dateto, $var_prod, $var_gameno, $var_batch, $users[$x]["AID"], $var_cardval);
                    if (is_array($temp))
                    {
                        foreach ($temp as $rec)
                        {
                            $month = date("m", strtotime($rec["xAxis"]));
                            switch ($month)
                            {
                                case "01": $JanSales += $rec["Sales"]; break;
                                case "02": $FebSales += $rec["Sales"]; break;
                                case "03": $MarSales += $rec["Sales"]; break;
                                case "04": $AprSales += $rec["Sales"]; break;
                                case "05": $MaySales += $rec["Sales"]; break;
                                case "06": $JunSales += $rec["Sales"]; break;
                                case "07": $JulSales += $rec["Sales"]; break;
                                case "08": $AugSales += $rec["Sales"]; break;
                                case "09": $SepSales += $rec["Sales"]; break;
                                case "10": $OctSales += $rec["Sales"]; break;
                                case "11": $NovSales += $rec["Sales"]; break;
                                case "12": $DecSales += $rec["Sales"]; break;
                            }
                            $datay = array($JanSales,$FebSales,$MarSales,$AprSales,$MaySales,$JunSales,$JulSales,$AugSales,$SepSales,$OctSales,$NovSales,$DecSales);
                        }
                        $p1 = new BarPlot($datay);
                        $p1->SetColor("#c50909");
                        $p1->SetLegend($users[$x]["UserName"]);
                        $array[] = $p1;
                        $datay = '';
                        $JanSales=0;$FebSales=0;$MarSales=0;$AprSales=0;$MaySales=0;$JunSales=0;$JulSales=0;$AugSales=0;$SepSales=0;$OctSales=0;$NovSales=0;$DecSales=0;
                    }
                }
                $gplot = new GroupBarPlot($array);
                $graph->Add($gplot);
            } 
            else 
            {
                $title = $ddlUsername->SelectedText."\n (Monthly Sales)";
                foreach ($temp as $rec)
                {
                    $month = date("m", strtotime($rec["xAxis"]));
                    switch ($month)
                    {
                        case "01": $JanSales += $rec["Sales"]; break;
                        case "02": $FebSales += $rec["Sales"]; break;
                        case "03": $MarSales += $rec["Sales"]; break;
                        case "04": $AprSales += $rec["Sales"]; break;
                        case "05": $MaySales += $rec["Sales"]; break;
                        case "06": $JunSales += $rec["Sales"]; break;
                        case "07": $JulSales += $rec["Sales"]; break;
                        case "08": $AugSales += $rec["Sales"]; break;
                        case "09": $SepSales += $rec["Sales"]; break;
                        case "10": $OctSales += $rec["Sales"]; break;
                        case "11": $NovSales += $rec["Sales"]; break;
                        case "12": $DecSales += $rec["Sales"]; break;
                    }
                }
                $datay = array($JanSales,$FebSales,$MarSales,$AprSales,$MaySales,$JunSales,$JulSales,$AugSales,$SepSales,$OctSales,$NovSales,$DecSales);
                $p1 = new BarPlot($datay);
                $p1->SetColor("#c50909");
                $p1->SetLegend($ddlUsername->SelectedText);
                $graph->Add($p1);
            }
            
            $datax = array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");         
            $graph->title->Set($title);
            $graph->xaxis->SetTickLabels($datax);
            $graph->legend->SetFrameWeight(1);
            $graph->Stroke('../views/images_sales/'.'salesgraph1.png');
        }
        
        
        $week = array();
        $mos = array();
        $yr = array();
        for ($a = 0;$a < count($arrgames); $a++)
        {
            $weektodatesales = $tmdecks->GetSales($datefrom ,$dateto, $arrgames[$a]["ProductID"], $arrgames[$a]["GameID"], $var_batch, $var_user, $var_cardval);
            $monthtodatesales = $tmdecks->GetSales($startmonth, $mtd_dateto, $arrgames[$a]["ProductID"], $arrgames[$a]["GameID"], $var_batch, $var_user, $var_cardval);
            $yeartodatesales = $tmdecks->GetSales($from, $ytd_dateto, $arrgames[$a]["ProductID"], $arrgames[$a]["GameID"], $var_batch, $var_user, $var_cardval);
            
            // Tickets Sold per Game
            $week[$a]["Sales"] = $weektodatesales[0]["Sales"];
            $mos[$a]["Sales"] = $monthtodatesales[0]["Sales"];
            $yr[$a]["Sales"] = $yeartodatesales[0]["Sales"];
            
            // Total Tickets Sold
            $total_in_week += $weektodatesales[0]["Sales"];
            $total_in_month += $monthtodatesales[0]["Sales"];
            $total_in_year += $yeartodatesales[0]["Sales"];
            
            // Total Sales (Amount)
            $sales_in_week += $weektodatesales[0]["Sales"] * $arrgames[$a]["CardPrice"];
            $sales_in_month += $monthtodatesales[0]["Sales"] * $arrgames[$a]["CardPrice"];
            $sales_in_year += $yeartodatesales[0]["Sales"] * $arrgames[$a]["CardPrice"];
        }        
    }
    
    /*Added by: Sheryl S. Basbas Feb. 24, 2012 */
    if($btnExportCSV->SubmittedValue == "Export to CSV")
    {
        $datefrom = $_SESSION['datefrom'];
        $dateto = $_SESSION['dateto'];
        $startmonth = date('Y-m-', strtotime($datefrom)) . '01';
        $from = date("Y-", strtotime($datefrom)) . '01-01';
        
        if (date("Y", strtotime($datefrom)) < date('Y'))
        {
            $ytd_dateto = date("Y-", strtotime($datefrom)) . '12-31';
        }
        else
        {
            $ytd_dateto = $dateto;
        }

        $mtd_dateto = date('Y-m-t', strtotime($datefrom));
        
        $temp = getDates($datefrom, $dateto, $var_prod, $var_gameno, $var_batch, $var_user, $var_cardval);
 
        if ($var_cardval == 0)
            $arrgames = $tmgamemgt->SelectAllGamesActive();
        else
            $arrgames = $tmgamemgt->getGameBySelectedProduct($var_cardval, $var_prod, $var_gameno);
        
        
        $sales_list = GetTicketSalesReport($arrgames, $dateto, $datefrom, $var_gameno, $var_prod, $var_batch, $var_user, $var_cardval);
        
        $fp = fopen("../csv/Sales_Summary.csv","w");
        if($fp)
        { 
            $cardprices = array();
            foreach($arrgames as $id => $val)
            {
                    $explode = explode(".", $val["CardPrice"]);
                    if ($explode[1] != "00" || $explode[1] > 00)
                        $strcp = $val["CardPrice"];
                    else
                        $strcp = floor($val["CardPrice"]);
                    
                    $arrgames_sorted[$val["ProductName"]] = $val["ProductName"] . " [$" . $strcp . "]";
                    $cardprices[$val["ProductName"]] = $val["CardPrice"];
                    //ksort($arrgames_sorted);
            }
		
            foreach($arrgames_sorted as $key => $val)
            {
                    $header .= $val.",";
            }
            
            $headerdate .= "Date," . $header . "Total Tickets Sold,Total Sales in USD [$]" . "\n";
            $header = $headerdate;
            fwrite($fp,$header);
            
            if (count($sales_list) > 0):
                if (is_array($sales_list)):
                    foreach ($sales_list as $date => $val) 
                    {                          
                        foreach ($val as $game => $sales) 
                        {
				$csvarray[$date][$game]["Sales"] = $sales["Sales"];
				ksort($csvarray[$date]);
                        }
                    }

                    foreach($csvarray as $date => $val)
                    {
                            $record.= date("d-M-Y", strtotime($date)).',';
                            // foreach($val as $game => $sales)
                            for ($a = 0; $a < count($arrgames); $a++)
                            {
                                    $game = $arrgames[$a]["ProductName"];
                                    $record .= str_replace(',','',number_format($val[$game]["Sales"])).','; 
                                    $total_tickets_sold += $val[$game]["Sales"];
                                    $total_sales += $val[$game]["Sales"] * $cardprices[$game];
                            }
                            $record .= str_replace(',','',number_format($total_tickets_sold)). "," . $total_sales . "\n";
                            $total_tickets_sold = 0;
                            $total_sales = 0;
                    }

                    endif;
            else:
                $record.= "No records found"."\n";
            endif;
            fwrite($fp,$record);
            fwrite($fp,"\n");

            $header = ' ,'.str_replace('Date,','', $header);
            fwrite($fp,$header);
            
            /******************* REPORT SUMMARY ************************/
            $write_w2d .= "Week To Date".",";
            $write_m2d .= "Month To Date".",";
            $write_y2d .= "Year To Date".",";
	    for ($a = 0;$a < count($arrgames); $a++)
            {
                    $weektodatesales = $tmdecks->GetSales($datefrom ,$dateto, $arrgames[$a]["ProductID"], $arrgames[$a]["GameID"], $var_batch, $var_user, $var_cardval);
                    $monthtodatesales = $tmdecks->GetSales($startmonth, $mtd_dateto, $arrgames[$a]["ProductID"], $arrgames[$a]["GameID"], $var_batch, $var_user, $var_cardval);
                    $yeartodatesales = $tmdecks->GetSales($from, $ytd_dateto, $arrgames[$a]["ProductID"], $arrgames[$a]["GameID"], $var_batch, $var_user, $var_cardval);

                    foreach ($weektodatesales as $key => $val)
                    {
                        $arrweek[$val["ProductName"]]["Sales"] = $val["Sales"];
                        //ksort($arrweek);
                        $total_in_week += $val["Sales"];
                        $sales_in_week += $val["Sales"] * $arrgames[$a]["CardPrice"];
                    }
                    
                    foreach ($monthtodatesales as $key => $val)
                    {
                        $arrmos[$val["ProductName"]]["Sales"] = $val["Sales"];
                        //ksort($arrmos);
                        $total_in_month += $val["Sales"];
                        $sales_in_month += $val["Sales"] * $arrgames[$a]["CardPrice"];
                    }
                    
                    foreach ($yeartodatesales as $key => $val)
                    {
                        $arryear[$val["ProductName"]]["Sales"] = $val["Sales"];
                        //ksort($arryear);
                        $total_in_year += $val["Sales"];
                        $sales_in_year += $val["Sales"] * $arrgames[$a]["CardPrice"];
                    }
            }

	     foreach ($arrweek as $key => $val)
	     {
		  $write_w2d .= $val["Sales"] . ",";
	     }

	     foreach ($arrmos as $key => $val)
	     {
		  $write_m2d .= $val["Sales"] . ",";
	     }

	     foreach ($arryear as $key => $val)
	     {
		  $write_y2d .= $val["Sales"] . ",";
	     }

            $write_w2d .= $total_in_week . "," . $sales_in_week . "\n";
            $write_m2d .= $total_in_month . "," . $sales_in_month . "\n";
            $write_y2d .= $total_in_year . "," . $sales_in_year . "\n";
            $write_total_sold = $write_w2d.$write_m2d.$write_y2d;
            fwrite($fp, $write_total_sold);
            /******************* REPORT SUMMARY ************************/
           
            fclose($fp);
            header('Content-type: text/csv');
            header("Content-Disposition: attachment; filename=".'Sales_Summary.csv');
            header('Pragma: public');
            readfile('../csv/Sales_Summary.csv');
            exit;
        }
        else
        {
            echo "<script>alert('ok');</script>";
        }            
    }
    /*Added by :SSB*/
}

function getDates($datefr, $dateto, $prodid, $gameno, $batchid, $user, $cardval)
{
    $tmdecks = new TMDecks();
    $days_between = ceil(abs(strtotime($dateto) - strtotime($datefr)) / 86400);
    $ds = new DateSelector($dateto);
    for ($i=0;$i<=$days_between;$i++)
    {
        $alldates[] = $ds->CurrentDate;
        $ds->AddDays(-1);
    }
    
    $sales = $tmdecks->GetDateSalesforGraph($datefr, $dateto, $prodid, $gameno, $batchid, $user, $cardval);
    if (is_array($sales))
    {
        foreach($sales as $rec)
        {
            $data[$rec["xAxis"]] += $rec["Sales"];
            $date[] = $rec["xAxis"];
        }
    }

    $temp = array(); 
    for ($i=0;$i<count($alldates);$i++)
    {
        if (is_array($date))
        {
            $chkArray = in_array($alldates[$i], $date);
            if ($chkArray)
            {
                $temp[$i]['Sales'] = $data[$alldates[$i]];
                $temp[$i]['xAxis'] = $alldates[$i];
            }
            else
            {
                $temp[$i]['Sales'] = 0;
                $temp[$i]['xAxis'] = $alldates[$i];
            } 
        } else {
            $temp[$i]['Sales'] = 0;
            $temp[$i]['xAxis'] = $alldates[$i];
        }
    }
    $temp = array_reverse($temp);
    return $temp;
}

function converttoWeeks($temp)
{
    $temp1 = array();
    $total_sales = 0;
    $total_count = count($temp);
    $lastdate = '';
    if (is_array($temp))
    {
        foreach ($temp as $rec)
        {
            $cntr++;
            $total_sales += $rec['Sales'];

            if($lastdate == '') {
                $lastdate = date("M d", strtotime($rec['xAxis']));
            }
            if($cntr % 7 == 0) {
                $temp1[$cntr]['Sales'] = $total_sales;
                $temp1[$cntr]['xAxis'] = $lastdate . ' - ' . date("M d", strtotime($rec['xAxis']));
                $total_sales = 0;
                $lastdate = '';
            }
            else if ($cntr == $total_count)
            {
                $temp1[$cntr]['Sales'] = $total_sales;
                $temp1[$cntr]['xAxis'] = $lastdate . ' - ' . date("M d", strtotime($rec['xAxis']));
            }
        }
    }
    return $temp1;
}

function createGraph($xdata, $ydata, $title, $xtitle, $ytitle, $legend, $img_name)
{
    $cntx = count($xdata);
    $width = ($cntx * 10) + 600;
    $graph = new Graph($width,750);
    $graph->SetScale("textlin");
    $theme_class=new UniversalTheme;
    $graph->SetTheme($theme_class);
    $graph->img->SetAntiAliasing(false);
    $graph->title->Set($title);
    $graph->SetBox(false);
    $graph->yaxis->HideTicks(false,false);
    $graph->xgrid->Show();
    $graph->xaxis->SetTickLabels($xdata);
    $graph->xaxis->SetTitle($xtitle, 'middle');
    $graph->yaxis->SetTitle($ytitle, 'middle');
    $graph->SetMargin(100, 100, 100, 100);
    $graph->yaxis->SetTitleMargin(70);
    $graph->yaxis->SetLabelFormatCallback("number_format");
    $graph->xgrid->SetColor('#E3E3E3');
    $p1 = new BarPlot($ydata);
    $graph->Add($p1);
    $p1->SetColor("#c50909");
    $p1->SetLegend($legend);
    $graph->legend->SetFrameWeight(1);
    $graph->Stroke('../views/images_sales/'.$img_name);
    return true;
}

function GetTicketSalesReport($game_list, $dateto, $datefrom, $game, $prod, $batch, $user, $cardval)
{
    $tmdecks = new TMDecks();
    $days_between = ceil(abs(strtotime($dateto) - strtotime($datefrom)) / 86400);
    $ds = new DateSelector($dateto);
    for ($i=0;$i<=$days_between;$i++)
    {
        $alldates[] = $ds->CurrentDate;
        $ds->AddDays(-1);
    }
    $arrNewSales = array();
    $ticketsales = $tmdecks->GetTicketSalesReport2($datefrom, $dateto, $game, $prod, $batch, $user, $cardval);
    if (is_array($ticketsales))
    {
            for ($i = 0; $i < count($ticketsales); $i++)
            {
                $sale = $ticketsales[$i];
                $chkArray = in_array(date('Y-m-d', strtotime($sale["xAxis"])), $alldates);
                if ($chkArray)
                {
                    $arrNewSales[date('Y-m-d', strtotime($sale["xAxis"]))][$sale["ProductName"]]["Sales"] += $sale["Sales"];
                    ksort($arrNewSales[date('Y-m-d', strtotime($sale["xAxis"]))]);
                }
            }
            
            for ($x = 0; $x < count($alldates); $x++)
            {
                $seldate = $alldates[$x];
                for ($b = 0;$b < count($game_list); $b++)
                {
                    if (!isset($arrNewSales[$seldate][$game_list[$b]["ProductName"]]["Sales"]))
                    {
                        $arrNewSales[$seldate][$game_list[$b]["ProductName"]]["Sales"] += 0;
                        ksort($arrNewSales[$seldate]);
                    }
                }
            }
            ksort($arrNewSales);
    }
    
    else
    {
        for ($x = 0; $x < count($alldates); $x++)
        {
            $seldate = $alldates[$x ];
            for ($a = 0; $a < count($game_list); $a++)
            {
                $arrNewSales[$seldate][$game_list[$a]["ProductName"]]["Sales"] += 0;
                ksort($arrNewSales[$seldate]);
            }
        }
        ksort($arrNewSales);
    }
    
    return $arrNewSales;
}

function convertTSRtoWeeks($final_array)
{
    $temp1 = array();
    $total_count = count($final_array);
    $lastdate = '';
    $totalGameSales = array();
    
    if (is_array($final_array))
    {
        foreach ($final_array as $date => $val)
        {
            $cntr++;
            foreach ($val as $game => $rec)
            {
                if(!isset($totalGameSales[$game]["Sales"]))
                    $totalGameSales[$game]["Sales"] = $rec["Sales"];
                else
                    $totalGameSales[$game]["Sales"] += $rec["Sales"];
            }
            
            if($lastdate == '')
                $lastdate = date("M d", strtotime($date));
            
            if($cntr % 7 == 0) 
            {
                foreach($totalGameSales as $key => $val) 
                {
                    $temp1[$lastdate . ' - ' . date("M d", strtotime($date))][$key]["Sales"] = $val["Sales"];
                }
                unset($totalGameSales);
                $lastdate = '';
            }
            
            else if ($cntr == $total_count)
            {
                foreach($totalGameSales as $key => $val)
                {
                    $temp1[$lastdate . ' - ' . date("M d", strtotime($date))][$key]["Sales"] = $val["Sales"];
                }
                unset($totalGameSales);
                $lastdate = '';
            }
        }
    }
    
    return $temp1;
}

function convertTSRtoMonthly($game_list, $dateto, $datefrom, $game, $prod, $batch, $user, $cardval)
{
    $final_array = GetTicketSalesReport($game_list, $dateto, $datefrom, $game, $prod, $batch, $user, $cardval);
    $date = "";
    $temp1 = array();
    $totalGameSales = array();
    if (is_array($final_array))
    {
        foreach ($final_array as $date => $val)
        {
            $month = date("m", strtotime($date));
            switch ($month)
            {
                case "01": $date = "January"; break;
                case "02": $date = "February"; break;
                case "03": $date = "March"; break;
                case "04": $date = "April"; break;
                case "05": $date = "May"; break;
                case "06": $date = "June"; break;
                case "07": $date = "July"; break;
                case "08": $date = "August"; break;
                case "09": $date = "September"; break;
                case "10": $date = "October"; break;
                case "11": $date = "November"; break;
                case "12": $date = "December"; break;
            }
            
            foreach ($val as $game => $rec)
            {
                if(!isset($totalGameSales[$game]["Sales"]))
                    $totalGameSales[$game]["Sales"] = $rec["Sales"];
                else
                    $totalGameSales[$game]["Sales"] += $rec["Sales"];             
            }
                
            foreach ($totalGameSales as $key => $rec2) {
                if(!isset($temp1[$date][$key]["Sales"])) 
                    $temp1[$date][$key]["Sales"] = $rec2["Sales"];
                else 
                    $temp1[$date][$key]["Sales"] += $rec2["Sales"];
            }
            unset($totalGameSales);
        }
    }
    
    return $temp1;
}

function GetTSRDistributorSales($users, $dateto, $datefrom, $gameid, $prodid, $batchid, $user, $cardval)
{
    $tmdecks = new TMDecks();
    $days_between = ceil(abs(strtotime($dateto) - strtotime($datefrom)) / 86400);
    $ds = new DateSelector($dateto);
    for ($i=0;$i<=$days_between;$i++)
    {
        $alldates[] = $ds->CurrentDate;
        $ds->AddDays(-1);
    }

    $distributorsales = $tmdecks->GetSalesReportPerDistributor($datefrom, $dateto, $gameid, $prodid, $batchid, $user, $cardval);
    if (is_array($distributorsales))
    {
        $arrNewSales = "";
        for ($i = 0; $i < count($distributorsales); $i++)
        {
            $sale = $distributorsales[$i];
            for ($a = 0;$a < count($users); $a++)
            {
                $currdstbr = $users[$a]["UserName"];
                if ($currdstbr == $sale["Distributor"])
                {
                    for ($x = 0;$x < count($alldates); $x++)
                    {
                        $seldate = $alldates[$x];
                        if ($seldate == date("Y-m-d", strtotime($sale["xAxis"])))
                        {
                            $arrNewSales[$sale["Distributor"]][date("Y-m-d", strtotime($sale["xAxis"]))]["Sales"] += $sale["Sales"];
                            $arrNewSales[$sale["Distributor"]][date("Y-m-d", strtotime($sale["xAxis"]))]["CardPrice"] += $sale["CardPrice"];
                        } 
                        /* Added by NDA 04-18-2012 - */
                        else {
                            if (!isset($arrNewSales[$currdstbr][$seldate]))
                            {
                                $arrNewSales[$currdstbr][$seldate]["Sales"] += 0;
                                if(is_array($arrNewSales[$currdstbr]) && $arrNewSales[$currdstbr] != null)
                                ksort($arrNewSales[$currdstbr]);
                            }
                        }
                        /* Added by NDA 04-18-2012 */
                    }
                } 
                else {
                    for ($x = 0;$x < count($alldates); $x++)
                    {
                        $seldate = $alldates[$x];
                        if (!isset($arrNewSales[$currdstbr][$seldate]))
                        {
                            $arrNewSales[$currdstbr][$seldate]["Sales"] += 0;
                            if(is_array($arrNewSales[$currdstbr]) && $arrNewSales[$currdstbr] != null)
                            ksort($arrNewSales[$currdstbr]);
                        }
                    }
                }
            }
        }
        
    } else {
            for ($a = 0;$a < count($users); $a++)
            {
                $currdstbr = $users[$a]["UserName"];
                for ($x = 0;$x < count($alldates); $x++)
                {
			if (!isset($arrNewSales[$currdstbr][$alldates[$x]]))
                        {
	                    $arrNewSales[$currdstbr][$alldates[$x]]["Sales"] += 0;
			}
                }
            }
    }
    
    return $arrNewSales;
}

function convertDistributorTSRtoWeeks($final_array)
{
    $temp1 = array();
    $lastdate = '';
    $total_count = 0;
    if (is_array($final_array))
    {
        // For counter purpose only (count total record)
        foreach ($final_array as $distr => $val){
            foreach ($val as $date => $sales) { $total_count++; } break;
        } 
        
        foreach ($final_array as $distr => $val)
        {
            $cntr = 0;
            foreach ($val as $date => $sales)
            {
                $cntr++;
                $total_sales += $sales["Sales"];
                $total_cardprice += $sales["CardPrice"];
                
                if($lastdate == '') {
                    $lastdate = date("M d", strtotime($date));
                }
                if($cntr % 7 == 0) {
                    $temp1[$distr][$lastdate . ' - ' . date("M d", strtotime($date))]["Sales"] = $total_sales;
                    $temp1[$distr][$lastdate . ' - ' . date("M d", strtotime($date))]["CardPrice"] = $total_cardprice;
                    $total_sales = 0;
                    $total_cardprice = 0;
                    $lastdate = '';
                }
                else if ($cntr == $total_count)
                {
                    $temp1[$distr][$lastdate . ' - ' . date("M d", strtotime($date))]["Sales"] = $total_sales;
                    $temp1[$distr][$lastdate . ' - ' . date("M d", strtotime($date))]["CardPrice"] = $total_cardprice;
                    $total_sales = 0;
                    $total_cardprice = 0;
                    $lastdate = '';
                }
            }
        }
    }
    return $temp1;
}

function convertTSRDistributortoMonthly($users, $dateto, $datefrom, $gameid, $prodid, $batchid, $user, $cardvalue)
{
    $final_array = GetTSRDistributorSales($users, $dateto, $datefrom, $gameid, $prodid, $batchid, $user, $cardvalue);
    $temp1= array();
    $totalGameSales = array();
    foreach ($final_array as $distr => $val)
    {
        foreach($val as $date => $sales)
        {
            $month = date("m", strtotime($date));
            switch ($month)
            {
                case "01": $date = "January"; break;
                case "02": $date = "February"; break;
                case "03": $date = "March"; break;
                case "04": $date = "April"; break;
                case "05": $date = "May"; break;
                case "06": $date = "June"; break;
                case "07": $date = "July"; break;
                case "08": $date = "August"; break;
                case "09": $date = "September"; break;
                case "10": $date = "October"; break;
                case "11": $date = "November"; break;
                case "12": $date = "December"; break;
            }
            
            if(!isset($totalGameSales[$date]))
            {
                $totalGameSales[$date]["Sales"] = $sales["Sales"];
                $totalGameSales[$date]["CardPrice"] = $sales["CardPrice"];
            }
            else
            {
                $totalGameSales[$date]["Sales"] += $sales["Sales"];
                $totalGameSales[$date]["CardPrice"] += $sales["CardPrice"];
            }
        }
        
        foreach($totalGameSales as $key => $val)
        {
            if(!isset($temp1[$distr][$key]))
            {
                $temp1[$distr][$key]["Sales"] = $val["Sales"];
                $temp1[$distr][$key]["CardPrice"] = $val["CardPrice"];
            }
            else
            {
                $temp1[$distr][$key]["Sales"] += $val["Sales"];
                $temp1[$distr][$key]["CardPrice"] += $val["CardPrice"];
            }
        }
        unset($totalGameSales);
    }
    return $temp1;
}

function myGraph()
{
        $width = 1040;
        $graph = new Graph($width,750);
        $graph->SetScale("textlin");
        $theme_class = new UniversalTheme;
        $graph->SetTheme($theme_class);
        $graph->img->SetAntiAliasing(false);
        $graph->SetBox(false);
        $graph->yaxis->HideTicks(false,false);
        $graph->xgrid->Show();
        $graph->xaxis->SetTitle("Date Released", 'middle');
        $graph->yaxis->SetTitle("Total Sales", 'middle');
        $graph->SetMargin(100, 100, 100, 100);
        $graph->yaxis->SetTitleMargin(70);
        $graph->yaxis->SetLabelFormatCallback("number_format");
        $graph->xgrid->SetColor('#E3E3E3');
        
        return $graph;
}
?>