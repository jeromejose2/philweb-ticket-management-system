<?php
/*
 * Author: Noel Antonio
 * Date Created: 2011-08-31
 * Company: Philweb Corporation
 */
$pagesubmenuid = 11;
$stylesheets[] = "css/default.css";

App::LoadModuleClass("TicketManagementCM", "TMGameManagement");
App::LoadModuleClass("TicketManagementCM", "TMAccounts");
App::LoadModuleClass("TicketManagementCM", "TMWinnings");
App::LoadModuleClass("TicketManagementCM", "TMProducts");

App::LoadControl("TextBox");
App::LoadControl("Button");
App::LoadControl("ComboBox");
App::LoadControl("PagingControl2");
App::LoadControl("Hidden");

//App::LoadModuleClass("TicketManagementCM","Pdf");
//App::LoadLibrary("fpdf/fpdf.php");

$fproc = new FormsProcessor();

$currency = App::getParam("currency");

$TMAccounts = new TMAccounts();
$TMWinnings = new TMWinnings();
$TMGames = new TMGameManagement();
$TMProducts = new TMProducts();

$gamenames = $TMProducts->SelectAllGameName();
$gamename_list = new ArrayList();
$gamename_list->AddArray($gamenames);
$ddlgamename = new ComboBox("ddlgamename","ddlgamename","Game Name: ");
$ddlgamename->Args = "onchange = 'javascript: get_gamenumber();'";
$ddlgamename->ShowCaption = true;
$gamenameopt = null;
$gamenameopt[] = new ListItem("ALL", "0", true);
$ddlgamename->Items = $gamenameopt;
$ddlgamename->DataSource = $gamename_list;
$ddlgamename->DataSourceText = "ProductName";
$ddlgamename->DataSourceValue = "ProductID";
$ddlgamename->DataBind();

$games = $TMGames->SelectByGameType();
$games_list = new ArrayList();
$games_list->AddArray($games);
$ddlgametype = new ComboBox("ddlgametype", "ddlgametype", "Game Number: ");
$ddlgametype->Args = "onchange='javascript: get_batchID(); get_gamename();'";
$litemgmetype = null;
$litemgmetype[] = new ListItem("ALL", "0", true);
$ddlgametype->Items = $litemgmetype;
$ddlgametype->ShowCaption = true;
$ddlgametype->DataSource = $games_list;
$ddlgametype->DataSourceText = "GameNumber";
$ddlgametype->DataSourceValue = "GameID";
$ddlgametype->DataBind();

$ddlgamebatch = new ComboBox("ddlgamebatch","ddlgamebatch","Game Batch:");
$ddlgamebatch->ShowCaption = true;
$gamebatch = null;
$gamebatch[] = new ListItem("ALL","0", true);
$ddlgamebatch->Items  = $gamebatch;

$flag = new Hidden("flag","flag","flag");
$xmltype = new Hidden("xmltype","xmltype","xmltype");

$btnExport = new Button("export","export","Export to PDF");
$btnExport->IsSubmit = true;

$btnExportCSV = new Button("btnExportCSV","btnExportCSV","Export to CSV");
$btnExportCSV->IsSubmit = true;

$txtDateFr = new TextBox("txtDateFr", "txtDateFr", "From : ");
$txtDateFr->ShowCaption = true;
$txtDateFr->ReadOnly = true;
$txtDateFr->Style = "text-align: center";
$txtDateFr->Args = "size='10'";
$txtDateFr->Text = date('Y-m-d');

$txtDateTo = new TextBox("txtDateTo", "txtDateTo", "To : ");
$txtDateTo->ShowCaption = true;
$txtDateTo->ReadOnly = true;
$txtDateTo->Style = "text-align: center";
$txtDateTo->Args = "size='10'";
$txtDateTo->Text = date('Y-m-d');

$txtGameName = new TextBox("txtGameName", "txtGameName", "Game Name : ");
$txtGameName->ShowCaption = true;
$txtGameName->Enabled = false;
$txtGameName->Style = "text-align: center";
$txtGameName->Args = "size='15'";

$cboAccount = new ComboBox("cboAccount", "cboAccount", "Account : ");
$cboAccount->ShowCaption = true;
$options = null;
$options[] = new ListItem("ALL", "0");
$cboAccount->Items = $options;
$arrOperators = $TMAccounts->SelectAllAccountsOrderBy();
$OperatorList = new ArrayList();
$OperatorList->AddArray($arrOperators);
$cboAccount->DataSource = $OperatorList;
$cboAccount->DataSourceText = "UserName";
$cboAccount->DataSourceValue = "AID";
$cboAccount->DataBind();

$btnSubmit = new Button("btnSubmit", "btnSubmit", "Submit");
$btnSubmit->IsSubmit = true;
$btnSubmit->Args = "onclick='javascript: return validate();'";

$itemsperpage = 50;
$pgcon = new PagingControl2($itemsperpage, 1);
$pgcon->URL = "javascript:ChangePage(%currentpage);";
$pgcon->ShowMoveToFirstPage = true;
$pgcon->ShowMoveToLastPage = true;
$pgcon->PageGroup = 5;

$fproc->AddControl($cboAccount);
$fproc->AddControl($ddlgametype);
$fproc->AddControl($txtGameName);
$fproc->AddControl($txtDateFr);
$fproc->AddControl($txtDateTo);
$fproc->AddControl($btnSubmit);
$fproc->AddControl($ddlgamename);
$fproc->AddControl($ddlgamebatch);
$fproc->AddControl($flag);
$fproc->AddControl($xmltype);
$fproc->AddControl($btnExport);
$fproc->AddControl($btnExportCSV);
$fproc->ProcessForms();

if ($fproc->IsPostBack)
{
    
    /* Added by: Sheryl S. Basbas Date: Feb 1, 2012 */
    if ($flag->SubmittedValue == 1)
    {
        $ddlgametype->Args = "onchange='javascript: get_batchID(); '";
        $ddlgametype->ClearItems();
        $prodid1 = $ddlgamename->SubmittedValue;
        $gamenumbers = $TMProducts->SelectGameNumPerGameName($prodid1);
        $gamenumbers_list1 = new ArrayList();
        $gamenumbers_list1->AddArray($gamenumbers);       
        $litemgmetype1 = null;
        $litemgmetype1[] = new ListItem("ALL", "0", false);
        $ddlgametype->Items = $litemgmetype1;
        $ddlgametype->DataSource = $gamenumbers_list1;
        $ddlgametype->DataSourceText = "GameNumber";
        $ddlgametype->DataSourceValue = "GameID";
        $ddlgametype->DataBind();
        $ddlgametype->SetSelectedValue($ddlgametype->SubmittedValue);
    }
    if ($flag->SubmittedValue == 2)
    {
        $ddlgamename->Args = "onchange='javascript:'";          
        $ddlgamename->ClearItems();
        $gamenum1 = $ddlgametype->SubmittedValue;
        $gamenames = $TMProducts->SelectGameNamePerGameNum($gamenum1);
        $gamename_list1 = new ArrayList();
        $gamename_list1->AddArray($gamenames);       
        $litemgmetype1 = null;
        $litemgmetype1[] = new ListItem("ALL", "0", false);
        $ddlgamename->Items = $litemgmetype1;
        $ddlgamename->DataSource = $gamename_list1;
        $ddlgamename->DataSourceText = "ProductName";
        $ddlgamename->DataSourceValue = "ProductID";
        $ddlgamename->DataBind();
        $ddlgamename->SetSelectedValue($ddlgametype->SubmittedValue);
    }
 
    $ddlgametype->SetSelectedValue($ddlgametype->SubmittedValue);
    $ddlgamename->SetSelectedValue($ddlgamename->SubmittedValue);
   
    $gamebatchID = $TMProducts->SelectBatchIDPerGameNum($ddlgametype->SelectedValue,2);
    $batchlist = new ArrayList();
    $batchlist->AddArray($gamebatchID);
    $gamebatch = null;
    $gamebatch[] = new ListItem("ALL","0", true);
    $ddlgamebatch->Items  = $gamebatch;
    $ddlgamebatch->DataSource = $batchlist;
    $ddlgamebatch->DataSourceText = "BatchID";
    $ddlgamebatch->DataSourceValue = "GameBatchID";
    $ddlgamebatch->DataBind();
    $ddlgamebatch->SetSelectedValue($ddlgamebatch->SubmittedValue);
    /*Added by: Sheryl S. Basbas */ 
    
    $incentive = 2;
    $total_payout_processed = 0;
    $tmp_total_incentive = 0;
    if ($btnSubmit->SubmittedValue == "Submit" || $fproc->GetPostVar("pgSelectedPage") != '')
    {
        $arrWinnings = $TMWinnings->SelectWinnings($cboAccount->SubmittedValue, $txtDateFr->Text, $txtDateTo->Text, $ddlgamename->SubmittedValue, $ddlgametype->SubmittedValue, $ddlgamebatch->SubmittedValue);
        
        // Modified by NDA 2013-01-28
        for ($i = 0; $i < count($arrWinnings); $i++)
        {
            if (strpos($arrWinnings[$i]["PrizeName"], 'KHR') !== false)
            {
                $khrcurrency = preg_replace('/[\KHR,]/', '', $arrWinnings[$i]["PrizeName"]);
                $total_payout_processed += convertCurrtodollar($khrcurrency);  
                if ($arrWinnings[$i]["AccountTypeID"] == 6 && $arrWinnings[$i]["PrizeType"] == 2)
                    $tmp_total_incentive += convertCurrtodollar($khrcurrency);
            }
            else if (strpos($arrWinnings[$i]["PrizeName"], '$') !== false)
            {
                $dollarcurrency = preg_replace('/[\$,]/', '', $arrWinnings[$i]["PrizeName"]);
                $total_payout_processed += $dollarcurrency;  
                if ($arrWinnings[$i]["AccountTypeID"] == 6 && $arrWinnings[$i]["PrizeType"] == 2 )
                    $tmp_total_incentive += $dollarcurrency;
            }
            else
            {
                // no currency in prizename but numeric
                $currency = $arrWinnings[$i]["PrizeName"];
                if (is_numeric($currency))
                {
                    $total_payout_processed += $currency;  
                    if (($arrWinnings[$i]["AccountTypeID"] == 6) && ($arrWinnings[$i]["PrizeType"] == 2))
                        $tmp_total_incentive += $currency;
                }
            }
        }
        
        
        /*
        $arrSummary = $TMWinnings->GetRedemptionSummaryALL($cboAccount->SubmittedValue, $txtDateFr->Text, $txtDateTo->Text, $ddlgamename->SubmittedValue, $ddlgametype->SubmittedValue, $ddlgamebatch->SubmittedValue);
        for ($i = 0; $i < count($arrSummary); $i++)
        {
                //replace comma and Spaces
                $nocomma =  str_replace( ',', '',$arrSummary[$i]['PrizeName']);
                $getcurrency =  str_replace( ' ', '',$nocomma);
                // Check for currency
                if(substr($getcurrency, 0,1) == '$') //dollar
                {
                        //replace dollar and conver to integer
                        $dollarcurrency =  (int)str_replace( '$', '',$getcurrency);
                        $total_payout_processed += $dollarcurrency;  
                        if ($arrSummary[$i]["AccountTypeID"] == 6 && $arrSummary[$i]["PrizeType"] == 2)
                        {
                                $tmp_total_incentive += $dollarcurrency;
                        }
                } 
                else if(substr($getcurrency, 0,3) == 'KHR')//for KHR
                {
                        //convert to KHR to dollar
                        $khrcurrency = convertCurrtodollar($getcurrency);
                        $total_payout_processed += $khrcurrency;
                        if ($arrSummary[$i]["AccountTypeID"] == 6 && $arrSummary[$i]["PrizeType"] == 2)
                        {
                                $tmp_total_incentive += $khrcurrency;
                        }
                }
        }
        */
        
        $totalincentive = number_format(($tmp_total_incentive * ($incentive / 100)) , 2 , "." , ",");
        $reccount = count($arrWinnings);
        if ($btnSubmit->SubmittedValue == "Submit")
        {
            $pgcon->SelectedPage = 1;
        }
        $pgcon->Initialize($itemsperpage, $reccount);
        $pgTransactionHistory = $pgcon->PreRender();
        $arrtmd = $TMWinnings->SelectWinningsWithLimit($cboAccount->SubmittedValue, $txtDateFr->Text, $txtDateTo->Text, $pgcon->SelectedItemFrom-1, $itemsperpage, $ddlgamename->SubmittedValue, $ddlgametype->SubmittedValue, $ddlgamebatch->SubmittedValue);
        $win_list = new ArrayList();
        $win_list->AddArray($arrtmd);
        
        // Modified by NDA 2013-01-28
        for ($i = 0; $i < count($arrtmd); $i++)
        {
            if (strpos($arrtmd[$i]["PrizeName"], 'KHR') !== false)
            {
                $khrcurrency = preg_replace('/[\KHR,]/', '', $arrtmd[$i]["PrizeName"]);
                $khrcurrency = convertCurrtodollar($khrcurrency);
                $win_list[$i]['PrizeName'] = $currency . number_format($khrcurrency , 0 , "" , ",");
            }
            else if (strpos($arrtmd[$i]["PrizeName"], '$') !== false)
            {
                $win_list[$i]['PrizeName'] = $arrtmd[$i]['PrizeName'];
            }
        }
        
       /*
        get Prize Won
        for ($i = 0; $i < count($arrtmd); $i++)
        {
                $nocomma =  str_replace( ',', '',$arrtmd[$i]['PrizeName']);
                $getcurrency =  str_replace( ' ', '',$nocomma);
                // Check for currency
                if(substr($getcurrency, 0,1) == '$') //dollar
                {
                        $win_list[$i]['PrizeName'] = $arrtmd[$i]['PrizeName'];
                } 
                else if(substr($getcurrency, 0,3) == 'KHR')//for KHR
                {
                        //convert to KHR to dollar
                        $khrcurrency = convertCurrtodollar($getcurrency);
                        $win_list[$i]['PrizeName'] = $currency.number_format($khrcurrency , 0 , "" , ",");
                }
        }
        */
    }

        
    
    if ($btnExportCSV->SubmittedValue == "Export to CSV")
    {
        $win_list = new ArrayList();
        $win_list->AddArray($arrWinnings);
        $totalincentive = number_format(($tmp_total_incentive * ($incentive / 100)) , 2 , "." , ",");
        $csvData = array();
        for ($i=0;$i<count($win_list);$i++)
        {
            $dateTime = new DateTime($win_list[$i]["DateClaimed"]);
            $date = $dateTime->format('Y-m-d h:i:s A');
            $acct = $win_list[$i]["UserName"];
            $gameno = $win_list[$i]["GameNumber"];
            $cardprice = $win_list[$i]["CardPrice"];
            $gamename = $win_list[$i]["ProductName"];
            $batchid = $win_list[$i]["BatchID"];
            $bookno = trim(str_replace(array('\r', '\n', '\r\n'), "", $win_list[$i]["TicketNumber"]));
            $virn = trim(str_replace(array('\r', '\n', '\r\n'), "", $win_list[$i]["ValidationNumber"]));
            
            // Modified by NDA 2013-01-28
            if (strpos($win_list[$i]["PrizeName"], 'KHR') !== false)
            {
                $khrcurrency = preg_replace('/[\KHR,]/', '', $win_list[$i]["PrizeName"]);
                $khrcurrency = convertCurrtodollar($khrcurrency);
                $prizename1 = $currency . number_format($khrcurrency , 0 , "" , ",");
            }
            else if (strpos($win_list[$i]["PrizeName"], '$') !== false)
            {
                $prizename1 = $win_list[$i]['PrizeName'];
            }
            else
            {
                $prizename1 = $win_list[$i]['PrizeName'];
            }

            /*
            start to convert
            $nocomma =  str_replace( ',', '',$win_list[$i]['PrizeName']);
            $getcurrency =  str_replace( ' ', '',$nocomma);
            // Check for currency
            if(substr($getcurrency, 0,1) == '$') //dollar
            {
                    $prizename1 = $win_list[$i]['PrizeName'];
            } 
            else if(substr($getcurrency, 0,3) == 'KHR')//for KHR
            {
                    //convert to KHR to dollar
                    $khrcurrency = convertCurrtodollar($getcurrency);
                    $prizename1 = $currency.number_format($khrcurrency , 0 , "" , ",");
            } 
            else
            {
                    $prizename1 = $win_list[$i]['PrizeName'];
            }
            */
            
            $preg_prize = preg_match('#\((.*?)\)#', $prizename1, $match);
            if ($preg_prize >= 1) // with parentheses
            {
                    $csv_prize = str_replace(array(' ', ','), "", $prizename1);
            }
            else
            {
                    $csv_prize = str_replace(",", "", $prizename1);
            }
            
            $csvData[] = $date.",".$acct.",".$cardprice.",".$gameno.",".$gamename.",".$batchid.",".$bookno.",".$virn.",".$csv_prize."\r\n";
        }
        
        $fp = fopen("../csv/Payout_History.csv","w");
        if ($fp)
        {
            $totalpayoutprocessedtitle ='Total Payout Processed';
            $totalpayoutprocesseddata = $currency . " " . str_replace(",", "", $total_payout_processed);
            $arrTotalPayoutProcessed = $totalpayoutprocessedtitle .",". $totalpayoutprocesseddata ."\r\n";
            fwrite($fp,$arrTotalPayoutProcessed);
            
            $incentiveratetitle ='Redemption Incentive Rate';
            $incentiveratedata = $incentive . "%";
            $arrIncentiveRate = $incentiveratetitle .",". $incentiveratedata ."\r\n";
            fwrite($fp,$arrIncentiveRate);
            
            $incentivetitle ='Redemption Incentive';

            $incentivedata = $currency . " " . str_replace(",", "", $totalincentive);
            $arrIncentive = $incentivetitle .",". $incentivedata ."\r\n";
            fwrite($fp,$arrIncentive);
            
            fwrite($fp,"\r\n");
            $header = "Date,Account,Card Value,Game Number,Game Name,Game Batch,Book Ticket Number,Validation Number,Prize Won" . "\r\n";
            fwrite($fp,$header);
             IF ($csvData){
            foreach($csvData as $rc)
            {
                if(count($rc)>0)
                {
                    fwrite($fp,$rc);
                }
            
            }}else
                {
                    $rc = "No Records Found;\r\n";
                    fwrite($fp,$rc);
                }
            
            fclose($fp);
            header('Content-type: text/csv');
            header("Content-Disposition: attachment; filename=".'Payout_History.csv');
            header('Pragma: public');
            readfile('../csv/Payout_History.csv');
            exit;
        } else {
            echo "<script>alert('ok');</script>";
        }
    }
       
       
        
}
/*****************Functions  *************/
function convertCurrtodollar($currencyMoney)
{
    App::LoadModuleClass("TicketManagementCM", "TMCurrencies");
    $TMcurrencies = new TMCurrencies();

    $money =  str_replace( 'KHR', '',$currencyMoney);
    $moneycurrency = 'KHR';

    $getconvertmoney = $TMcurrencies->SelectByCurrencySymbol($moneycurrency);
    $fixedrate = $getconvertmoney[0]['FixedRate'];

    if(is_numeric($fixedrate) && is_numeric($money))
    {
            $truevalue = ($money/$fixedrate);
            $truevalue =   number_format($truevalue,0,'.','');
    }
    
    return $truevalue;
}
 /*****************Functions  *************/
?>
