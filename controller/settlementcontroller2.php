<?php
/*
 * Author: Noel Antonio
 * Date Created: 2011-08-31
 * Company: Philweb Corporation
 */
$pagesubmenuid = 11;
$stylesheets[] = "css/default.css";

App::LoadModuleClass("TicketManagementCM", "TMGameManagement");
App::LoadModuleClass("TicketManagementCM", "TMAccounts");
App::LoadModuleClass("TicketManagementCM", "TMWinnings");
App::LoadModuleClass("TicketManagementCM", "TMProducts");
App::LoadControl("TextBox");
App::LoadControl("Button");
App::LoadControl("ComboBox");
App::LoadControl("PagingControl2");
App::LoadControl("Hidden");

App::LoadModuleClass("TicketManagementCM","Pdf");

App::LoadLibrary("fpdf/fpdf.php");

$fproc = new FormsProcessor();
$TMAccounts = new TMAccounts();
$TMWinnings = new TMWinnings();
$TMGames = new TMGameManagement();
$TMProducts = new TMProducts();

$gamenames = $TMProducts->SelectAllGameName();
$gamename_list = new ArrayList();
$gamename_list->AddArray($gamenames);
$ddlgamename = new ComboBox("ddlgamename","ddlgamename","Game Name: ");
$ddlgamename->Args = "onchange = 'javascript: get_gamenumber();'";
$ddlgamename->ShowCaption = true;
$gamenameopt = null;
$gamenameopt[] = new ListItem("ALL", "0", true);
$ddlgamename->Items = $gamenameopt;
$ddlgamename->DataSource = $gamename_list;
$ddlgamename->DataSourceText = "ProductName";
$ddlgamename->DataSourceValue = "ProductID";
$ddlgamename->DataBind();

$games = $TMGames->SelectByGameType();
$games_list = new ArrayList();
$games_list->AddArray($games);
$ddlgametype = new ComboBox("ddlgametype", "ddlgametype", "Game Number: ");
$ddlgametype->Args = "onchange='javascript: get_batchID(); get_gamename();'";
$litemgmetype = null;
$litemgmetype[] = new ListItem("ALL", "0", true);
$ddlgametype->Items = $litemgmetype;
$ddlgametype->ShowCaption = true;
$ddlgametype->DataSource = $games_list;
$ddlgametype->DataSourceText = "GameNumber";
$ddlgametype->DataSourceValue = "GameID";
$ddlgametype->DataBind();

$ddlgamebatch = new ComboBox("ddlgamebatch","ddlgamebatch","Game Batch:");
$ddlgamebatch->ShowCaption = true;
$gamebatch = null;
$gamebatch[] = new ListItem("ALL","0", true);
$ddlgamebatch->Items  = $gamebatch;

$flag = new Hidden("flag","flag","flag");
$xmltype = new Hidden("xmltype","xmltype","xmltype");

$btnExport = new Button("export","export","Export to PDF");
$btnExport->IsSubmit = true;

$btnExportCSV = new Button("btnExportCSV","btnExportCSV","Export to CSV");
$btnExportCSV->IsSubmit = true;

$txtDateFr = new TextBox("txtDateFr", "txtDateFr", "From : ");
$txtDateFr->ShowCaption = true;
$txtDateFr->ReadOnly = true;
$txtDateFr->Style = "text-align: center";
$txtDateFr->Args = "size='10'";
$txtDateFr->Text = date('m/d/Y');

$txtDateTo = new TextBox("txtDateTo", "txtDateTo", "To : ");
$txtDateTo->ShowCaption = true;
$txtDateTo->ReadOnly = true;
$txtDateTo->Style = "text-align: center";
$txtDateTo->Args = "size='10'";
$txtDateTo->Text = date('m/d/Y');

$txtGameName = new TextBox("txtGameName", "txtGameName", "Game Name : ");
$txtGameName->ShowCaption = true;
$txtGameName->Enabled = false;
$txtGameName->Style = "text-align: center";
$txtGameName->Args = "size='15'";

$cboAccount = new ComboBox("cboAccount", "cboAccount", "Account : ");
$cboAccount->ShowCaption = true;
$options = null;
$options[] = new ListItem("ALL", "0");
$cboAccount->Items = $options;
$arrOperators = $TMAccounts->SelectAllAccountsOrderBy();
$OperatorList = new ArrayList();
$OperatorList->AddArray($arrOperators);
$cboAccount->DataSource = $OperatorList;
$cboAccount->DataSourceText = "UserName";
$cboAccount->DataSourceValue = "AID";
$cboAccount->DataBind();

$btnSubmit = new Button("btnSubmit", "btnSubmit", "Submit");
$btnSubmit->IsSubmit = true;
$btnSubmit->Args = "onclick='javascript: return validate();'";

$itemsperpage = 50;
$pgcon = new PagingControl2($itemsperpage, 1);
$pgcon->URL = "javascript:ChangePage(%currentpage);";
$pgcon->ShowMoveToFirstPage = true;
$pgcon->ShowMoveToLastPage = true;
$pgcon->PageGroup = 5;

$fproc->AddControl($cboAccount);
$fproc->AddControl($ddlgametype);
$fproc->AddControl($txtGameName);
$fproc->AddControl($txtDateFr);
$fproc->AddControl($txtDateTo);
$fproc->AddControl($btnSubmit);
$fproc->AddControl($ddlgamename);
$fproc->AddControl($ddlgamebatch);
$fproc->AddControl($flag);
$fproc->AddControl($xmltype);
$fproc->AddControl($btnExport);
$fproc->AddControl($btnExportCSV);
$fproc->ProcessForms();

if ($fproc->IsPostBack)
{
    
        
    $cbo1 = $cboAccount->SubmittedValue;
    $cboValue = $TMAccounts->SelectAccountType($cbo1);
    $cbo2 = new ArrayList();
    $cbo2->AddArray($cboValue);
    
    /* Added by: Sheryl S. Basbas Date: Feb 1, 2012 */
    if ($flag->SubmittedValue == 1)
    {
        $ddlgametype->Args = "onchange='javascript: get_batchID(); '";
        $ddlgametype->ClearItems();
        $prodid1 = $ddlgamename->SubmittedValue;
        $gamenumbers = $TMProducts->SelectGameNumPerGameName($prodid1);
        $gamenumbers_list1 = new ArrayList();
        $gamenumbers_list1->AddArray($gamenumbers);       
        $litemgmetype1 = null;
        $litemgmetype1[] = new ListItem("ALL", "0", false);
        $ddlgametype->Items = $litemgmetype1;
        $ddlgametype->DataSource = $gamenumbers_list1;
        $ddlgametype->DataSourceText = "GameNumber";
        $ddlgametype->DataSourceValue = "GameID";
        $ddlgametype->DataBind();
        $ddlgametype->SetSelectedValue($ddlgametype->SubmittedValue);
    }
    if ($flag->SubmittedValue == 2)
    {
        $ddlgamename->Args = "onchange='javascript:'";          
        $ddlgamename->ClearItems();
        $gamenum1 = $ddlgametype->SubmittedValue;
        $gamenames = $TMProducts->SelectGameNamePerGameNum($gamenum1);
        $gamename_list1 = new ArrayList();
        $gamename_list1->AddArray($gamenames);       
        $litemgmetype1 = null;
        $litemgmetype1[] = new ListItem("ALL", "0", false);
        $ddlgamename->Items = $litemgmetype1;
        $ddlgamename->DataSource = $gamename_list1;
        $ddlgamename->DataSourceText = "ProductName";
        $ddlgamename->DataSourceValue = "ProductID";
        $ddlgamename->DataBind();
        $ddlgamename->SetSelectedValue($ddlgametype->SubmittedValue);
    }
 
    $ddlgametype->SetSelectedValue($ddlgametype->SubmittedValue);
    $ddlgamename->SetSelectedValue($ddlgamename->SubmittedValue);
   
    $gamebatchID = $TMProducts->SelectBatchIDPerGameNum($ddlgametype->SelectedValue,$xmltype->SubmittedValue);
    $batchlist = new ArrayList();
    $batchlist->AddArray($gamebatchID);
    $gamebatch = null;
    $gamebatch[] = new ListItem("ALL","0", true);
    $ddlgamebatch->Items  = $gamebatch;
    $ddlgamebatch->DataSource = $batchlist;
    $ddlgamebatch->DataSourceText = "BatchID";
    $ddlgamebatch->DataSourceValue = "GameBatchID";
    $ddlgamebatch->DataBind();
    $ddlgamebatch->SetSelectedValue($ddlgamebatch->SubmittedValue);
    /*Added by: Sheryl S. Basbas */ 
    

    if ($btnSubmit->SubmittedValue == "Submit" || $fproc->GetPostVar("pgSelectedPage") != '')
    {
        $arrWinnings = $TMWinnings->SelectWinnings($cboAccount->SubmittedValue, $ddlgametype->SubmittedValue, $txtDateFr->Text, $txtDateTo->Text, $ddlgamename->SubmittedValue, $ddlgamebatch->SubmittedValue);
        $arrSummary = $TMWinnings->GetRedemptionSummary($cboAccount->SubmittedValue, $ddlgametype->SubmittedValue, $txtDateFr->SubmittedValue, $txtDateTo->SubmittedValue, $ddlgamebatch->SubmittedValue);
        $arrperdist = $TMWinnings->TotalPayoutPerDistributor($ddlgametype->SubmittedValue, $txtDateFr->SubmittedValue, $txtDateTo->SubmittedValue, $ddlgamebatch->SubmittedValue);
        $acctype = $arr_orderby[$ctr][AccountTypeID];if ($acctype== 6){$incentive = 2;}else{$incentive = 0;}
        $totalincentive = number_format(($arr_orderby[$ctr]["Total"] * ($incentive / 100)) , 2 , "." , ",");
        $reccount = count($arrWinnings);
        if ($btnSubmit->SubmittedValue == "Submit")
        {
            $pgcon->SelectedPage = 1;
        }
        $pgcon->Initialize($itemsperpage, $reccount);
        $pgTransactionHistory = $pgcon->PreRender();
        $arrtmd = $TMWinnings->SelectWinningsWithLimit($cboAccount->SubmittedValue, $ddlgametype->SubmittedValue, $txtDateFr->Text, $txtDateTo->Text, $pgcon->SelectedItemFrom-1, $itemsperpage, $ddlgamename->SubmittedValue, $ddlgamebatch->SubmittedValue);
        $win_list = new ArrayList();
        $win_list->AddArray($arrtmd);
        $arrpayoutorderby = $TMWinnings->SelectPayoutSummaryOrderBy($cboAccount->SubmittedValue, $ddlgametype->SubmittedValue, $txtDateFr->Text, $txtDateTo->Text, $pgcon->SelectedItemFrom-1, $itemsperpage, $ddlgamename->SubmittedValue);
        $arr_orderby = new ArrayList();
        $arr_orderby->AddArray($arrpayoutorderby);

    }
    
    /*Added by ARS*/
    if($btnExport->SubmittedValue == "Export to PDF")
    {
        $pdf = new PDF('L','mm','Legal');
        $pdf->AddPage();
	$pdf->AliasNbPages();
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(0,5,"Payout History",0,0,'C');
        $pdf->Ln(8);
        $arrWinnings = $TMWinnings->SelectWinnings($cboAccount->SubmittedValue, $ddlgametype->SubmittedValue, $txtDateFr->Text, $txtDateTo->Text, $ddlgamename->SubmittedValue, $ddlgamebatch->SubmittedValue);
        $arrSummary = $TMWinnings->GetRedemptionSummary($cboAccount->SubmittedValue, $ddlgametype->SubmittedValue, $txtDateFr->SubmittedValue, $txtDateTo->SubmittedValue, $ddlgamebatch->SubmittedValue);
        $arrperdist = $TMWinnings->TotalPayoutPerDistributor($ddlgametype->SubmittedValue, $txtDateFr->SubmittedValue, $txtDateTo->SubmittedValue, $ddlgamebatch->SubmittedValue);
       $acctype = $arr_orderby[$ctr][AccountTypeID];if ($acctype== 6){$incentive = 2;}else{$incentive = 0;}
        $totalincentive = number_format(($arr_orderby[$ctr]["Total"] * ($incentive / 100)) , 2 , "." , ",");
        $pdf->SetFont('Arial','B',9);
        $pdf->SetX(10);
        $pdf->Cell(100,5,'Payout Summary',1,1,'C');
        $pdf->Cell(50,5,'Total Payout Processed:',1,0,'C');
        $pdf->SetFont('Arial','',9);
        $pdf->Cell(50,5,"$" . number_format($arrSummary[0]["Total"] , 2 , "." , ","),1,1,'R');
        $pdf->SetFont('Arial','B',9);
        $pdf->Cell(50,5,'Redemption Incentive Rate:',1,0,'C');
        $pdf->SetFont('Arial','',9);
        $pdf->Cell(50,5,$incentive . "%" ,1,1,'R');
        $pdf->SetFont('Arial','B',9);
        $pdf->Cell(50,5,'Redemption Incentive:',1,0,'C');
        $pdf->SetFont('Arial','',9);
        $pdf->Cell(50,5, "$" . $totalincentive ,1,1,'R');
        $pdf->Ln(8);
        $pdf->SetX(10);
        $pdf->SetFont('Arial','B',9);
        $pdf->Cell(50,5,'Date',1,0,'C');
        $pdf->Cell(40,5,'Account',1,0,'C');
        $pdf->Cell(40,5,'Game Number',1,0,'C');
        $pdf->Cell(40,5,'Game Name',1,0,'C');
        $pdf->Cell(40,5,'Game Batch',1,0,'C');
        $pdf->Cell(40,5,'Book Ticket Number',1,0,'C');
        $pdf->Cell(40,5,'Validation Number',1,0,'C');
        $pdf->Cell(40,5,'Prize Won',1,1,'C');
        $pdf->SetFont('Arial','',9);
        $pdf->SetX(10);      
        if(count($arrWinnings) > 0)
        {
            for($i = 0 ; $i < count($arrWinnings) ; $i++)
            {
                $pdf->Cell(50,5,$arrWinnings[$i]['DateClaimed'],1,0,'C');
                $pdf->Cell(40,5,$arrWinnings[$i]['UserName'],1,0,'C');
                $pdf->Cell(40,5,$arrWinnings[$i]['GameNumber'],1,0,'C');
                $pdf->Cell(40,5,$arrWinnings[$i]['ProductName'],1,0,'C');
                $pdf->Cell(40,5,$arrWinnings[$i]['BatchID'],1,0,'C');
                $pdf->Cell(40,5,$arrWinnings[$i]['BookNumber'],1,0,'C');
                $pdf->Cell(40,5,$arrWinnings[$i]['VIRN'],1,0,'C');
                $pdf->Cell(40,5,$arrWinnings[$i]['PrizeName'],1,1,'C');
            }
        }
        else
        {            
            $pdf->Cell(330,5,'No record found',1,1,'C');
        }        
        $pdf->Output('Payout History','D');
        Header('Content-Type: application/pdf');
    }
    
    if ($btnExportCSV->SubmittedValue == "Export to CSV")
    {
        $arrpayoutorderby = $TMWinnings->SelectPayoutSummaryOrderBy($cboAccount->SubmittedValue, $ddlgametype->SubmittedValue, $txtDateFr->Text, $txtDateTo->Text, $pgcon->SelectedItemFrom-1, $itemsperpage, $ddlgamename->SubmittedValue);
        $arr_orderby = new ArrayList();
        $arr_orderby->AddArray($arrpayoutorderby);
        $win_list = new ArrayList();
        $win_list->AddArray($arrWinnings);
        $acctype = $arr_orderby[$ctr][AccountTypeID];if ($acctype== 6){$incentive = 2;}else{$incentive = 0;}
        $totalincentive = number_format(($arr_orderby[$ctr]["Total"] * ($incentive / 100)) , 2 , "." , ",");
        $csvDataTable = array();
        $csvData = array();
     
        
        
        $arrWinnings1 = $TMWinnings->SelectWinningsAutomatedReport($date);
     $win_list1 = new ArrayList();
     $win_list1->AddArray($arrWinnings1); 
     $csvData1 = array();
        
//         for ($i=0;$i<count($win_list1);$i++){   
//            $date1 = $win_list1[$i]["DateClaimed"];
//            $acct1 = $win_list1[$i]["UserName"];
//            $gameno1 = $win_list1[$i]["GameNumber"];
//            $gamename1 = $win_list1[$i]["ProductName"];
//            $bookno1 = $win_list1[$i]["TicketNumber"];
//            $virn1 = $win_list1[$i]["ValidationNumber"];
//            $prizename1 = $win_list1[$i]["PrizeName"];
//            $csvData1[] = $date1.",".$acct1.",".$gameno1.",".$gamename1.",".$bookno1.",".$virn1.",".$prizename1."\r\n";
//        }
  for ($ctr=0;$ctr<count($arr_orderby);$ctr++){
            $date1 = $arr_orderby[$ctr]["DateClaimed"];
            $acct1 = $arr_orderby[$ctr]["UserName"];
             $acct2 = $arr_orderby[$ctr]["Name"];
            $gameno1 = $arr_orderby[$ctr]["ProductName"];
            $gamename1 = $arr_orderby[$ctr]["GameNumber"];
        $acctype = $arr_orderby[$ctr][AccountTypeID];if ($acctype== 6){$incentive = 2;}else{$incentive = 0;}
        $totalincentive = number_format(($arr_orderby[$ctr]["Total"] * ($incentive / 100)) , 2 , "." , ",");
            $totalpayoutprocess = "$" .$arr_orderby[$ctr]["Total"];
            $redemptionincentive = "$" .$incentive;
            $csvDataTable[] = $date1.",".$acct1.",".$acct2.",".$gameno1.",".$gamename1.",".$totalpayoutprocess.",".$redemptionincentive.",".$totalincentive."\r\n";
        }
        for ($i=0;$i<count($win_list);$i++)
        {
            $date = $win_list[$i]["DateClaimed"];
            $acct = $win_list[$i]["UserName"];
            $gameno = $win_list[$i]["GameNumber"];
            $gamename = $win_list[$i]["ProductName"];
            $bookno = $win_list[$i]["TicketNumber"];
            $virn = $win_list[$i]["ValidationNumber"];
            $prizename = $win_list[$i]["PrizeName"];
            $csvData1[] = $date.",".$acct.",".$gameno.",".$gamename.",".$bookno.",".$virn.",".$prizename."\r\n";
        }
        
        $fp = fopen("../csv/Payout_History.csv","w");
        if ($fp)
        {
            $totalpayoutprocessedtitle ='Pay Out Summary';
            $arrTotalPayoutProcessed = $totalpayoutprocessedtitle ."\r\n";
            fwrite($fp,$arrTotalPayoutProcessed);
            
            fwrite($fp,"\r\n");
            $header = "Date, Distributor Account,Account Type, Game Name, Game Number, Total Payout Processed, Redemption Incentive, Total Redemption Incentive" . "\r\n";
            fwrite($fp,$header);
           

//----edit in staging only
 if($csvDataTable){
                     foreach($csvDataTable as $rc){ 
                         if(count($rc)>0)
                             {
                             fwrite($fp,$rc);
                             } 
                             
                             }
                         }else
                             { 
                             $rc = ", ,  , No Records Found\r\n"; 
				fwrite($fp,$rc);
                             
                             }
            
          /*	foreach($csvData as $rc)
            {
                if(count($rc)>0)
                {
                    fwrite($fp,$rc);
                }
                else
                {
                    $rc = "No Records Found;\r\n";
                    fwrite($fp,$rc);
                }
            }*/
            $payouthistorytitle = 'Pay Out History';
            $arrpayouthistoryprocessed = $payouthistorytitle ."\r\n";
            fwrite($fp,$arrpayouthistoryprocessed);
            fwrite($fp,"\r\n");
            $header = "Date, Distributor Account, Game Number, Game Name,  Book Ticket Number , Validation Number, Prize Won" . "\r\n";
            fwrite($fp,$header);

                 if($csvData1){
                    
                     foreach($csvData1 as $rc){
                            if(count($rc)>0)
                             {
                             fwrite($fp,$rc);
                             }   }   }
                             else
                             { 
                        $rc = ", , , No Records Found\r\n";fwrite($fp,$rc);
                             }
                             
                             
                             
                             
            fclose($fp);
            header('Content-type: text/csv');
            header("Content-Disposition: attachment; filename=".'Payout_History.csv');
            header('Pragma: public');
           readfile('../csv/Payout_History.csv');
            exit;
        } else {
            echo "<script>alert('ok');</script>";
        }
    }
}
?>


