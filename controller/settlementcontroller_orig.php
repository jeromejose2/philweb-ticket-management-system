<?php
/*
 * Author: Noel Antonio
 * Date Created: 2011-08-31
 * Company: Philweb Corporation
 */
$pagesubmenuid = 11;
$stylesheets[] = "css/default.css";

App::LoadModuleClass("TicketManagementCM", "TMGameManagement");
App::LoadModuleClass("TicketManagementCM", "TMAccounts");
App::LoadModuleClass("TicketManagementCM", "TMWinnings");
App::LoadModuleClass("TicketManagementCM", "TMProducts");
App::LoadControl("TextBox");
App::LoadControl("Button");
App::LoadControl("ComboBox");
App::LoadControl("PagingControl2");
$fproc = new FormsProcessor();
$TMAccounts = new TMAccounts();
$TMWinnings = new TMWinnings();
$TMGames = new TMGameManagement();
$TMProducts = new TMProducts();

$txtDateFr = new TextBox("txtDateFr", "txtDateFr", "From : ");
$txtDateFr->ShowCaption = true;
$txtDateFr->ReadOnly = true;
$txtDateFr->Style = "text-align: center";
$txtDateFr->Args = "size='10'";
$txtDateFr->Text = date('m/d/Y');

$txtDateTo = new TextBox("txtDateTo", "txtDateTo", "To : ");
$txtDateTo->ShowCaption = true;
$txtDateTo->ReadOnly = true;
$txtDateTo->Style = "text-align: center";
$txtDateTo->Args = "size='10'";
$txtDateTo->Text = date('m/d/Y');

$txtGameName = new TextBox("txtGameName", "txtGameName", "Game Name : ");
$txtGameName->ShowCaption = true;
$txtGameName->Enabled = false;
$txtGameName->Style = "text-align: center";
$txtGameName->Args = "size='15'";

$cboAccount = new ComboBox("cboAccount", "cboAccount", "Account : ");
$cboAccount->ShowCaption = true;
$options = null;
$options[] = new ListItem("ALL", "0");
$cboAccount->Items = $options;
$arrOperators = $TMAccounts->SelectAllAccountsOrderBy();
$OperatorList = new ArrayList();
$OperatorList->AddArray($arrOperators);
$cboAccount->DataSource = $OperatorList;
$cboAccount->DataSourceText = "UserName";
$cboAccount->DataSourceValue = "AID";
$cboAccount->DataBind();

$cboGameID = new ComboBox("cboGameID", "cboGameID", "Game Number : ");
$cboGameID->ShowCaption = true;
$options = null;
//$options[] = new ListItem("ALL", "0");
$options[] = new ListItem("Please Select", "");
$cboGameID->Items = $options;
//$cboGameID->Args = "onchange='javascript: SelectGameName();'";
$arrgames = $TMGames->SelectOrderBy("GameNumber", "ASC");
$gameslist = new ArrayList();
$gameslist->AddArray($arrgames);
/*$cboGameID->DataSource = $gameslist;
$cboGameID->DataSourceText = "GameNumber";
$cboGameID->DataSourceValue = "GameID";
$cboGameID->DataBind();*/

$btnSubmit = new Button("btnSubmit", "btnSubmit", "Submit");
$btnSubmit->IsSubmit = true;
$btnSubmit->Args = "onclick='javascript: return validate();'";

//get list of game names
$gamenames = $TMProducts->SelectAllGameName();
$gamename_list = new ArrayList();
$gamename_list->AddArray($gamenames);

$ddlgamename = new ComboBox("ddlgamename","ddlgamename","Game Name: ");
$ddlgamename->Args = "onchange = 'javascript: get_gamenumber();'";
$gamenameopt = null;
$gamenameopt[] = new ListItem("Please select", "0", true);
$ddlgamename->Items = $gamenameopt;
$ddlgamename->DataSource = $gamename_list;
$ddlgamename->DataSourceText = "ProductName";
$ddlgamename->DataSourceValue = "ProductID";
$ddlgamename->DataBind();
$ddlgamename->ShowCaption = true;

$itemsperpage = 50;
$pgcon = new PagingControl2($itemsperpage, 1);
$pgcon->URL = "javascript:ChangePage(%currentpage);";
$pgcon->ShowMoveToFirstPage = true;
$pgcon->ShowMoveToLastPage = true;
$pgcon->PageGroup = 5;

$fproc->AddControl($cboAccount);
$fproc->AddControl($cboGameID);
$fproc->AddControl($txtGameName);
$fproc->AddControl($txtDateFr);
$fproc->AddControl($txtDateTo);
$fproc->AddControl($btnSubmit);
$fproc->AddControl($ddlgamename);
$fproc->ProcessForms();

if ($fproc->IsPostBack)
{
    $gamenumbers = $TMProducts->SelectGameNumPerGameName($ddlgamename->SubmittedValue);
	$options = null;
    $options[] = new ListItem("ALL", "0");
    $cboGameID->Items = $options;
    $gamename_list = new ArrayList();
    $gamename_list->AddArray($gamenumbers);
    $cboGameID->DataSource = $gamename_list;
    $cboGameID->DataSourceText = "GameNumber";
    $cboGameID->DataSourceValue = "GameID";
    $cboGameID->DataBind();
    $cboGameID->ShowCaption = true;
    $cboGameID->SetSelectedValue($cboGameID->SubmittedValue);
    $incentive = "2";
    if ($btnSubmit->SubmittedValue == "Submit" || $fproc->GetPostVar("pgSelectedPage") != '')
    {
        $arrWinnings = $TMWinnings->SelectWinnings($cboAccount->SubmittedValue, $cboGameID->SubmittedValue, $txtDateFr->Text, $txtDateTo->Text, $ddlgamename->SubmittedValue);
        $arrSummary = $TMWinnings->GetRedemptionSummary($cboAccount->SubmittedValue, $cboGameID->SubmittedValue, $txtDateFr->SubmittedValue, $txtDateTo->SubmittedValue);
        $totalincentive = number_format(($arrSummary[0]["Total"] * ($incentive / 100)) , 2 , "." , ",");
        $reccount = count($arrWinnings);
        if ($btnSubmit->SubmittedValue == "Submit"){$pgcon->SelectedPage = 1;}
        $pgcon->Initialize($itemsperpage, $reccount);
        $pgTransactionHistory = $pgcon->PreRender();
        $arrtmd = $TMWinnings->SelectWinningsWithLimit($cboAccount->SubmittedValue, $cboGameID->SubmittedValue, $txtDateFr->Text, $txtDateTo->Text, $pgcon->SelectedItemFrom-1, $itemsperpage, $ddlgamename->SubmittedValue);
        $win_list = new ArrayList();
        $win_list->AddArray($arrtmd);
    }
    
    if ($cboGameID->SubmittedValue != 0)
    {
        $arrgames = $TMProducts->SelectGameNameByID($cboGameID->SubmittedValue);
        if ($arrgames > 0){ $txtGameName->Text = $arrgames[0]["ProductName"];}
    } 
    else if(isset($_GET['changecbo'])) 
    { 
        $txtGameName->Text = $arrgames[0]["ProductName"];
    }
}
?>


