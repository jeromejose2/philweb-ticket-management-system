<?php
/*
 * Author: Noel Antonio
 * Date Created: 2011-08-31
 * Company: Philweb Corporation
 */
$pagesubmenuid = 5;
$stylesheets[] = "css/default.css";

//include("../init.inc.php");
App::LoadModuleClass("TicketManagementCM", "TMGameManagement");
App::LoadModuleClass("TicketManagementCM", "TMProducts");
App::LoadModuleClass("TicketManagementCM", "TMAccounts");
App::LoadModuleClass("TicketManagementCM", "TMDecks");
App::LoadControl("TextBox");
App::LoadControl("Button");
App::LoadControl("ComboBox");
App::LoadControl("DataTable");
App::LoadControl("PagingControl2");
App::LoadControl("Hidden");
$fproc = new FormsProcessor();
$TMAccounts = new TMAccounts();
$TMDecks = new TMDecks();
$TMGames = new TMGameManagement();
$TMProducts = new TMProducts();

$acctid = $_SESSION['acctid'];
$acctypeID = $_SESSION['accttype'];

$cboAccount = new ComboBox("cboAccount", "cboAccount", "Account : ");
$cboAccount->ShowCaption = true;
$options = null;
$options[] = new ListItem("Please select", "0", true);
$cboAccount->Items = $options;
if ($acctypeID == 2 || $acctypeID == 5 || $acctypeID == 6 || $acctypeID == 4 || $acctypeID == 7)
{
    $arrOperators = $TMAccounts->SelectOperator($acctid, $acctypeID);
    $cboAccount->Enabled = false;
    $cboAccount->SubmittedValue = $acctid;
}
else
{
    $arrOperators = $TMAccounts->SelectAllOperators();
}

$OperatorList = new ArrayList();
$OperatorList->AddArray($arrOperators);
$cboAccount->DataSource = $OperatorList;
$cboAccount->DataSourceText = "UserName";
$cboAccount->DataSourceValue = "AID";
$cboAccount->DataBind();
    
$ddlgametype = new ComboBox("ddlgametype", "ddlgametype", "Game Number : ");
$ddlgametype->Args = "onchange='get_batchID();'";

$ddlgametype->ShowCaption = true;
$options = null;
$options[] = new ListItem("Please select", "");
$ddlgametype->Items = $options;
$arrGames = $TMGames->SelectOrderBy("GameNumber", "ASC");
$gameList = new ArrayList();
$gameList->AddArray($arrGames);
$ddlgametype->DataSource = $gameList;
$ddlgametype->DataSourceText = "GameNumber";
$ddlgametype->DataSourceValue = "GameID";
$ddlgametype->DataBind();

$txtGameName = new TextBox("txtGameName", "txtGameName", "Game Name : ");
$txtGameName->ShowCaption = true;
$txtGameName->ReadOnly = true;
$txtGameName->Style = "text-align: center";
$txtGameName->Args = "size='15'";

$txtBookNo = new TextBox("txtBookNo","txtBookNo","");
$txtBookNo->ShowCaption = true;
$txtBookNo->Style = "text-align: center";
$txtBookNo->Length = 6;
$txtBookNo->Args = "onkeypress='javascript: return isNumber(event)';";

$txtDateFr = new TextBox("txtDateFr", "txtDateFr", "From : ");
$txtDateFr->ShowCaption = true;
$txtDateFr->ReadOnly = true;
$txtDateFr->Style = "text-align: center";
$txtDateFr->Args = "size='10'";
$txtDateFr->Text =date('Y-m-d');

$txtDateTo = new TextBox("txtDateTo", "txtDateTo", "To : ");
$txtDateTo->ShowCaption = true;
$txtDateTo->ReadOnly = true;
$txtDateTo->Style = "text-align: center";
$txtDateTo->Args = "size='10'";
$txtDateTo->Text = date('Y-m-d');

$btnSearch = new Button("btnSearch", "btnSearch", "Search");
$btnSearch->IsSubmit = true;
$btnSearch->Args = "onclick='javascript: return validateBook();'";
$btnSubmit = new Button("btnSubmit", "btnSubmit", "Submit");
$btnSubmit->IsSubmit = true;
$btnSubmit->Args = "onclick='javascript: return validateAll();'";

//get list of game names
$gamenames = $TMProducts->SelectAllGameName();
$gamename_list = new ArrayList();
$gamename_list->AddArray($gamenames);

$ddlgamename = new ComboBox("ddlgamename","ddlgamename","Game Name: ");
$ddlgamename->Args = "onchange = 'javascript: get_gamenumber();'";
$gamenameopt = null;
$gamenameopt[] = new ListItem("Please select", "0", true);
$ddlgamename->Items = $gamenameopt;
$ddlgamename->DataSource = $gamename_list;
$ddlgamename->DataSourceText = "ProductName";
$ddlgamename->DataSourceValue = "ProductID";
$ddlgamename->DataBind();
$ddlgamename->ShowCaption = true;

$btnExportCSV = new Button("btnExportCSV","btnExportCSV","Export to CSV");
$btnExportCSV->IsSubmit = true;
$btnExportCSV->Args = "onclick='javascript: return validateAll();'";

$itemsperpage = 50;
$pgcon = new PagingControl2($itemsperpage, 1);
$pgcon->URL = "javascript:ChangePage(%currentpage);";
$pgcon->ShowMoveToFirstPage = true;
$pgcon->ShowMoveToLastPage = true;
$pgcon->PageGroup = 5;

$ddlgamebatch = new ComboBox("ddlgamebatch", "ddlgamebatch", "Game Batch : ");
$ddlgamebatch->ShowCaption = true;
$options = null;
$options[] = new ListItem("Please select", "");
$ddlgamebatch->Items = $options;



//get list of game card value
$cardprize = $TMGames->SelectByCardValue();
$cardprize_list = new ArrayList();
$cardprize_list->AddArray($cardprize);


$ddlcardvalue = new ComboBox('ddlcardvalue','ddlcardvalue','Card Value:');
$ddlcardvalue->Args = "onchange='javascript: get_gamename();'";
$cardlist = null;
$cardlist[] = new ListItem("Please select"," ",false);
//$cardlist[] = new ListItem("ALL","0",false);
$ddlcardvalue->Items = $cardlist;
$ddlcardvalue->DataSource = $cardprize_list;
$ddlcardvalue->DataSourceText = "CardPrice";
$ddlcardvalue->DataSourceValue = "CardPrice";
$ddlcardvalue->DataBind();

/* Added by: Sheryl S. Basbas
 * 01/25/2011
 */
$flag = new Hidden("flag","flag","flag");
$xmltype = new Hidden("xmltype","xmltype","xmltype");
$hidden = new Hidden("hidden","hidden","hidden");
$hidden->Text = "0"; 
/* -Added by: Sheryl S. Basbas*/

$fproc->AddControl($cboAccount);
$fproc->AddControl($ddlgametype);
$fproc->AddControl($txtGameName);
$fproc->AddControl($txtBookNo);
$fproc->AddControl($txtDateFr);
$fproc->AddControl($txtDateTo);
$fproc->AddControl($btnSearch);
$fproc->AddControl($btnSubmit);
$fproc->AddControl($ddlgamename);
$fproc->AddControl($ddlgamebatch);
$fproc->AddControl($flag);
$fproc->AddControl($xmltype);
$fproc->AddControl($hidden);
$fproc->AddControl($btnExportCSV);
$fproc->AddControl($ddlcardvalue);
$fproc->ProcessForms();

if ($fproc->IsPostBack)
{
    /* Added by: Sheryl S. Basbas
     * 01/25/2012
     */
     if ($flag->SubmittedValue == 1 || $flag->SubmittedValue == 4  )
    {
        //game name
        $ddlgamename->Args = "onchange='javascript: get_gamenumber();'";          
        $ddlgamename->ClearItems();
        
        $gamenum1 = $ddlgametype->SubmittedValue;
        $gamenames = $TMProducts->SelectGameNamePerGameNum($gamenum1);
        $gamename_list1 = new ArrayList();
        $gamename_list1->AddArray($gamenames);       
        $litemgmetype1 = null;
        $litemgmetype1[] = new ListItem("Please select", "0", false);
        $ddlgamename->Items = $litemgmetype1;
       
        $ddlgamename->DataSource = $gamename_list1;
        $ddlgamename->DataSourceText = "ProductName";
        $ddlgamename->DataSourceValue = "ProductID";
        $ddlgamename->DataBind();
        
        $ddlgamename->SetSelectedValue($ddlgametype->SubmittedValue);
        //game num
        $ddlgametype->Args = "onchange='javascript: get_batchID(); '";
        $ddlgametype->ClearItems();
       
        $prodid1 = $ddlgamename->SubmittedValue;
        $gamenumbers = $TMProducts->SelectGameNumPerGameName($prodid1);
   
        $gamenumbers_list1 = new ArrayList();
        $gamenumbers_list1->AddArray($gamenumbers);       
        $litemgmetype12 = null;
        $litemgmetype12[] = new ListItem("Please select", "0", false);
        $ddlgametype->Items = $litemgmetype12;
        
        $ddlgametype->DataSource = $gamenumbers_list1;
        $ddlgametype->DataSourceText = "GameNumber";
        $ddlgametype->DataSourceValue = "GameID";
        $ddlgametype->DataBind();
        $ddlgametype->SetSelectedValue($ddlgametype->SubmittedValue);
    }
    
    $ddlgamename->SetSelectedValue($ddlgamename->SubmittedValue);
    $ddlgametype->SetSelectedValue($ddlgametype->SubmittedValue);    
    $gamebatchID = $TMProducts->SelectBatchIDPerGameNum($ddlgametype->SelectedValue,$xmltype->SubmittedValue);
    $batchlist = new ArrayList();
    $batchlist->AddArray($gamebatchID);
    $ddlgamebatch->DataSource = $batchlist;
    $ddlgamebatch->DataSourceText = "BatchID";
    $ddlgamebatch->DataSourceValue = "GameBatchID";
    $ddlgamebatch->DataBind();
    $ddlgamebatch->SetSelectedValue($ddlgamebatch->SubmittedValue);    
    $itemsperpage = 50;
    
    if($hidden->Text == 1)
    {
        $arrDecks = $TMDecks->SelectDeck($ddlcardvalue->SubmittedValue, $cboAccount->SubmittedValue, $ddlgametype->SubmittedValue, $txtDateFr->Text, $txtDateTo->Text,$ddlgamename->SubmittedValue,$ddlgamebatch->SubmittedValue);
        $reccount = count($arrDecks);
        $arrtmd = $TMDecks->SelectDeckWithLimit($ddlcardvalue->SubmittedValue, $cboAccount->SubmittedValue, $ddlgametype->SubmittedValue, $txtDateFr->Text, $txtDateTo->Text, $pgcon->SelectedItemFrom-1, $itemsperpage ,$ddlgamename->SubmittedValue,$ddlgamebatch->SubmittedValue);
    }
    
    if($hidden->Text == 2)
    {
        if ($cboAccount->SubmittedValue == 0)
        {
            $count = $TMDecks->SelectByBookNo($txtBookNo->Text); 
            $arrtmd = $TMDecks->SelectByBookNoWithLimit($txtBookNo->Text,$pgcon->SelectedItemFrom-1,$itemsperpage);
        }
        else
        {
         
            $count = $TMDecks->SelectByIDBookNo($cboAccount->SubmittedValue,$txtBookNo->Text);
            $arrtmd = $TMDecks->SelectByIDBookNoWithLimit($cboAccount->SubmittedValue,$txtBookNo->Text,$pgcon->SelectedItemFrom-1,$itemsperpage,$ddlgamebatch->SubmittedValue);
        }
        
        $reccount = count($count);
        
    }
    
    if ($btnSubmit->SubmittedValue == "Submit" )
    {   $_SESSION[search] = false;
        $_SESSION[tmp_search]="";
        $hidden->Text = "1";  

        $arrDecks = $TMDecks->SelectDeck($ddlcardvalue->SelectedText,$cboAccount->SubmittedValue, $ddlgametype->SubmittedValue, $txtDateFr->Text, $txtDateTo->Text,$ddlgamename->SubmittedValue,$ddlgamebatch->SubmittedValue);
        $reccount = count($arrDecks);

        $arrtmd = $TMDecks->SelectDeckWithLimit($ddlcardvalue->SelectedText,$cboAccount->SubmittedValue, $ddlgametype->SubmittedValue, $txtDateFr->Text, $txtDateTo->Text, $pgcon->SelectedItemFrom-1, $itemsperpage ,$ddlgamename->SubmittedValue,$ddlgamebatch->SubmittedValue);        
        $temp =1;
        
    }
 
    if ($btnSearch->SubmittedValue == "Search")
    {
        $hidden->Text = "2";
        $page = 0;
//        if ($cboAccount->SubmittedValue == 0)
//        {
//            $count = $TMDecks->SelectByBookNo($txtBookNo->Text); 
//            $arrtmd = $TMDecks->SelectByBookNoWithLimit($txtBookNo->Text,$pgcon->SelectedItemFrom-1,$itemsperpage);
//        }
//        else
//        {
//         
//            $count = $TMDecks->SelectByIDBookNo($cboAccount->SubmittedValue,$txtBookNo->Text);
//            $arrtmd = $TMDecks->SelectByIDBookNoWithLimit($cboAccount->SubmittedValue,$txtBookNo->Text,$page,$itemsperpage,$ddlgamebatch->SubmittedValue);
//        }
        $count = $TMDecks->SelectByBookNo($txtBookNo->Text); 
        $arrtmd = $TMDecks->SelectByBookNoWithLimit($txtBookNo->Text,$pgcon->SelectedItemFrom-1,$itemsperpage);
            
        $reccount = count($count);
        $temp = 0;
        $_SESSION[search] = true;
        $_SESSION[tmp_search] = $arrtmd;
        $btnExportCSV->Args = "";
    }
    
    $site_list = new ArrayList();
    $site_list->AddArray($arrtmd);
    
    if ($btnExportCSV->SubmittedValue == "Export to CSV")
    {   
        $csvData = array();
        $arrDecks = array();
    
        if ($hidden->SubmittedValue == "1")
        {
            $arrDecks = $TMDecks->SelectDeck($ddlcardvalue->SubmittedValue,$cboAccount->SubmittedValue, $ddlgametype->SubmittedValue, $txtDateFr->SubmittedValue, $txtDateTo->SubmittedValue,$ddlgamename->SubmittedValue,$ddlgamebatch->SubmittedValue);
        } 
        else
        {
            if ($cboAccount->SubmittedValue == 0)
            {
                $arrDecks = $TMDecks->SelectByBookNo($txtBookNo->SubmittedValue); 
            }
            else
            {
                $arrDecks = $TMDecks->SelectByIDBookNo($cboAccount->SubmittedValue,$txtBookNo->SubmittedValue);
            }
        }
        

        if($_SESSION[search] == true)
        {
            $arrDecks = $_SESSION[tmp_search];
        }
        
        $site_list = new ArrayList();
        $site_list->AddArray($arrDecks);
        for ($ctr = 0; $ctr < count($site_list); $ctr++)
        {
            if ($site_list[$ctr]['Status'] == 1){$status = "Active";}
            elseif ($site_list[$ctr]['Status'] == 2){ $status =  "On Freight";} 
            elseif ($site_list[$ctr]['Status'] == 3){ $status =  "On Stock";} 
            elseif ($site_list[$ctr]['Status'] == 4){ $status =  "Assigned";} 
            else { $status =  "Cancelled";}
            $dateTime = new DateTime($site_list[$ctr]["DateReleased"]);
            $csvData[] = $dateTime->format('Y-m-d').",".
            $site_list[$ctr]['CardPrice'].",".
            $site_list[$ctr]['ProductName'].",".
            $site_list[$ctr]['BookTicketCount'].",".
            $site_list[$ctr]['GameNumber'].",".
            $site_list[$ctr]['BatchID'].",".
            $site_list[$ctr]['BookNumber'].",".
            $status.",".$site_list[$ctr]['InvoiceNumber']."\r\n";
        }
        
        $fp = fopen("../csv/Distributor_Inventory.csv","w");
        if ($fp)
        {
            $header = "Date,Card Value,Game Name,Book Size,Game Number,Game Batch,Book Number,Status,Invoice Number" . "\r\n";
            fwrite($fp,$header);
            if ($csvData){
                foreach($csvData as $rc)
                {
                    if(count($rc)>0) { fwrite($fp,$rc); }
                }
            }
            else
            {
                $rc = "\nNo Records Found;\n";
                fwrite($fp,$rc);
            }
            fclose($fp);
            header('Content-type: text/csv');
            header("Content-Disposition: attachment; filename=".'Distributor_Inventory.csv');
            header('Pragma: public');
            readfile('../csv/Distributor_Inventory.csv');
            exit;
        } else {
            echo "<script>alert('ok');</script>";
        }
    }
    
    if ($ddlgametype->SubmittedValue != 0)
    {
        $arrgames = $TMProducts->SelectGameNameByID($ddlgametype->SubmittedValue);
        if ($arrgames > 0){ $txtGameName->Text = $arrgames[0]["ProductName"];}
    } 
    
    else if(isset($_GET['changecbo'])) 
    { 
        $txtGameName->Text = $arrgames[0]["ProductName"];
    }
    
    $itemsperpage = 50;
    $pgcon->Initialize($itemsperpage, $reccount);
    $pgTransactionHistory = $pgcon->PreRender();
    if(strlen($_SESSION[search]) > 0){
    $btnExportCSV->Args = "";}
}
?>