<?php
/*
 * Author: Noel Antonio
 * Date Created: 2011-08-31
 * Company: Philweb Corporation
 */
$pagesubmenuid = 5;
$stylesheets[] = "css/default.css";

//include("../init.inc.php");
App::LoadModuleClass("TicketManagementCM", "TMGameManagement");
App::LoadModuleClass("TicketManagementCM", "TMProducts");
App::LoadModuleClass("TicketManagementCM", "TMAccounts");
App::LoadModuleClass("TicketManagementCM", "TMDecks");
App::LoadControl("TextBox");
App::LoadControl("Button");
App::LoadControl("ComboBox");
App::LoadControl("DataTable");
App::LoadControl("PagingControl2");
App::LoadControl("Hidden");
$fproc = new FormsProcessor();
$TMAccounts = new TMAccounts();
$TMDecks = new TMDecks();
$TMGames = new TMGameManagement();
$TMProducts = new TMProducts();

$acctid = $_SESSION['acctid'];
$acctypeID = $_SESSION['accttype'];

$cboAccount = new ComboBox("cboAccount", "cboAccount", "Account : ");
$cboAccount->ShowCaption = true;
$options = null;
$options[] = new ListItem("Please select", "0", true);
$cboAccount->Items = $options;
if ($acctypeID == 5 || $acctypeID == 6)
{
    $arrOperators = $TMAccounts->SelectOperator($acctid, $acctypeID);
    $cboAccount->Enabled = false;
    $cboAccount->SubmittedValue = $acctid;
}
else
{
    $arrOperators = $TMAccounts->SelectAllOperators();
}

    $OperatorList = new ArrayList();
    $OperatorList->AddArray($arrOperators);
    $cboAccount->DataSource = $OperatorList;
    $cboAccount->DataSourceText = "UserName";
    $cboAccount->DataSourceValue = "AID";
    $cboAccount->DataBind();
    
$cboGameID = new ComboBox("cboGameID", "cboGameID", "Game Number : ");
$cboGameID->Args = "onchange='get_batchID(); get_gamename();'";
$cboGameID->ShowCaption = true;
$options = null;
//$options[] = new ListItem("ALL", "0");
$options[] = new ListItem("Please select", "");
$cboGameID->Items = $options;
//$cboGameID->Args = "onchange='javascript: SelectGameName();'";
$arrGames = $TMGames->SelectOrderBy("GameNumber", "ASC");
$gameList = new ArrayList();
$gameList->AddArray($arrGames);
$cboGameID->DataSource = $gameList;
$cboGameID->DataSourceText = "GameNumber";
$cboGameID->DataSourceValue = "GameID";
$cboGameID->DataBind();

$txtGameName = new TextBox("txtGameName", "txtGameName", "Game Name : ");
$txtGameName->ShowCaption = true;
$txtGameName->ReadOnly = true;
$txtGameName->Style = "text-align: center";
$txtGameName->Args = "size='15'";

$txtBookNo = new TextBox("txtBookNo","txtBookNo","");
$txtBookNo->ShowCaption = true;
$txtBookNo->Style = "text-align: center";
$txtBookNo->Length = 6;
$txtBookNo->Args = "onkeypress='javascript: return isNumber(event)';";
//$txtBookNo->Args = "size='10'";

$txtDateFr = new TextBox("txtDateFr", "txtDateFr", "From : ");
$txtDateFr->ShowCaption = true;
$txtDateFr->ReadOnly = true;
$txtDateFr->Style = "text-align: center";
$txtDateFr->Args = "size='10'";
$txtDateFr->Text = date('m/d/Y');

$txtDateTo = new TextBox("txtDateTo", "txtDateTo", "To : ");
$txtDateTo->ShowCaption = true;
$txtDateTo->ReadOnly = true;
$txtDateTo->Style = "text-align: center";
$txtDateTo->Args = "size='10'";
$txtDateTo->Text = date('m/d/Y');

$btnSearch = new Button("btnSearch", "btnSearch", "Search");
$btnSearch->IsSubmit = true;
$btnSearch->Args = "onclick='javascript: return validateBook();'";
$btnSubmit = new Button("btnSubmit", "btnSubmit", "Submit");
$btnSubmit->IsSubmit = true;
$btnSubmit->Args = "onclick='javascript: return validateAll();'";

//get list of game names
$gamenames = $TMProducts->SelectAllGameName();
$gamename_list = new ArrayList();
$gamename_list->AddArray($gamenames);

$ddlgamename = new ComboBox("ddlgamename","ddlgamename","Game Name: ");
$ddlgamename->Args = "onchange = 'javascript: get_gamenumber();get_batchID();'";
$gamenameopt = null;
$gamenameopt[] = new ListItem("Please select", "0", true);
$ddlgamename->Items = $gamenameopt;
$ddlgamename->DataSource = $gamename_list;
$ddlgamename->DataSourceText = "ProductName";
$ddlgamename->DataSourceValue = "ProductID";
$ddlgamename->DataBind();
$ddlgamename->ShowCaption = true;

$itemsperpage = 50;
$pgcon = new PagingControl2($itemsperpage, 1);
$pgcon->URL = "javascript:ChangePage(%currentpage);";
$pgcon->ShowMoveToFirstPage = true;
$pgcon->ShowMoveToLastPage = true;
$pgcon->PageGroup = 5;

$ddlgamebatch = new ComboBox("ddlgamebatch", "ddlgamebatch", "Game Batch : ");
$ddlgamebatch->ShowCaption = true;
$options = null;
//$options[] = new ListItem("ALL", "0");
$options[] = new ListItem("ALL", "0");
$ddlgamebatch->Items = $options;

$flag = new Hidden("flag","flag","flag");
$xmltype = new Hidden("xmltype","xmltype","xmltype");

$fproc->AddControl($cboAccount);
$fproc->AddControl($cboGameID);
$fproc->AddControl($txtGameName);
$fproc->AddControl($txtBookNo);
$fproc->AddControl($txtDateFr);
$fproc->AddControl($txtDateTo);
$fproc->AddControl($btnSearch);
$fproc->AddControl($btnSubmit);
$fproc->AddControl($ddlgamename);
$fproc->AddControl($ddlgamebatch);
$fproc->AddControl($flag);
$fproc->AddControl($xmltype);
$fproc->ProcessForms();

if ($fproc->IsPostBack)
{
    if ($flag->SubmittedValue == 1)
    {
        $cboGameID->Args = "onchange='javascript: get_batchID(); '";
        $cboGameID->ClearItems();
       
        $prodid1 = $ddlgamename->SubmittedValue;
        $gamenumbers = $TMProducts->SelectGameNumPerGameName($prodid1);
    
        $gamenumbers_list1 = new ArrayList();
        $gamenumbers_list1->AddArray($gamenumbers);       
       
        $litemgmetype1 = null;
        $litemgmetype1[] = new ListItem("ALL", "0", false);
        $cboGameID->Items = $litemgmetype1;
        
        $cboGameID->DataSource = $gamenumbers_list1;
        $cboGameID->DataSourceText = "GameNumber";
        $cboGameID->DataSourceValue = "GameID";
        $cboGameID->DataBind();
        
        $cboGameID->SetSelectedValue($cboGameID->SubmittedValue);
       
       
        
    }
     if ($flag->SubmittedValue == 2)
    {
        $ddlgamename->Args = "onchange='javascript:'";          
        $ddlgamename->ClearItems();
       
        $gamenum1 = $cboGameID->SubmittedValue;
        $gamenames = $TMProducts->SelectGameNamePerGameNum($gamenum1);
    
        $gamename_list1 = new ArrayList();
        $gamename_list1->AddArray($gamenames);       
              $litemgmetype1 = null;
        $litemgmetype1[] = new ListItem("Please select", "0", false);
        $ddlgamename->Items = $litemgmetype1;
       
        $ddlgamename->DataSource = $gamename_list1;
        $ddlgamename->DataSourceText = "ProductName";
        $ddlgamename->DataSourceValue = "ProductID";
        $ddlgamename->DataBind();
        
        $ddlgamename->SetSelectedValue($ddlgamename->SubmittedValue);
       
        
        
    }
//    $gamenumbers = $TMProducts->SelectGameNumPerGameName($ddlgamename->SubmittedValue);
//    $options = null;
//    $options[] = new ListItem("ALL", "0");
//    $cboGameID->Items = $options;
//    $gamename_list = new ArrayList();
//    $gamename_list->AddArray($gamenumbers);
//    $cboGameID->DataSource = $gamename_list;
//    $cboGameID->DataSourceText = "GameNumber";
//    $cboGameID->DataSourceValue = "GameID";
//    $cboGameID->DataBind();
//    $cboGameID->ShowCaption = true;
    $cboGameID->SetSelectedValue($cboGameID->SubmittedValue);
    
    $gamebatchID = $TMProducts->SelectBatchIDPerGameNum($cboGameID->SelectedValue,$xmltype->SubmittedValue);
    $batchlist = new ArrayList();
    $batchlist->AddArray($gamebatchID);
    $ddlgamebatch->DataSource = $batchlist;
    $ddlgamebatch->DataSourceText = "BatchID";
    $ddlgamebatch->DataSourceValue = "GameBatchID";
    $ddlgamebatch->DataBind();
    $ddlgamebatch->SetSelectedValue($ddlgamebatch->SubmittedValue);
    
    if ($btnSubmit->SubmittedValue == "Submit" || $fproc->GetPostVar("pgSelectedPage") != '')
    {
        $arrDecks = $TMDecks->SelectDeck($cboAccount->SubmittedValue, $cboGameID->SubmittedValue, $txtDateFr->Text, $txtDateTo->Text,$ddlgamename->SubmittedValue,$ddlgamebatch->SubmittedValue);
        $reccount = count($arrDecks);
        $itemsperpage = 50;
        if ($btnSubmit->SubmittedValue == "Submit"){ $pgcon->SelectedPage = 1; }
        $pgcon->Initialize($itemsperpage, $reccount);
        $pgTransactionHistory = $pgcon->PreRender();
        $arrtmd = $TMDecks->SelectDeckWithLimit($cboAccount->SubmittedValue, $cboGameID->SubmittedValue, $txtDateFr->Text, $txtDateTo->Text, $pgcon->SelectedItemFrom-1, $itemsperpage ,$ddlgamename->SubmittedValue,$ddlgamebatch->SubmittedValue);
        
        $site_list = new ArrayList();
        $site_list->AddArray($arrtmd);
        $temp =1;
    }
    
    if ($btnSearch->SubmittedValue == "Search")
    { 
        if ($cboAccount->SubmittedValue == 0){
            $arrtmd = $TMDecks->SelectByBookNo($txtBookNo->Text);
        } else {
        $arrtmd = $TMDecks->SelectByIDBookNo($cboAccount->SubmittedValue,$txtBookNo->Text,$ddlgamebatch->SubmittedValue);}
        $site_list = new ArrayList();
        $site_list->AddArray($arrtmd);
        $temp = 0;
    }
    
    if ($cboGameID->SubmittedValue != 0)
    {
        $arrgames = $TMProducts->SelectGameNameByID($cboGameID->SubmittedValue);
        if ($arrgames > 0){ $txtGameName->Text = $arrgames[0]["ProductName"];}
    } 
    else if(isset($_GET['changecbo'])) 
    { 
        $txtGameName->Text = $arrgames[0]["ProductName"];
    }
}
?>
