<?php
error_reporting(E_ALL);
/* 
 * Created By: Arlene R. Salazar 03/15/2012
 * Purpose: Process for splitting xml
 */
$pagesubmenuid = 47;
$stylesheets[] = "css/default.css";
$javascripts[] = "jscripts/validations.js";

App::LoadControl("Button");
App::LoadControl("ComboBox");

$tmauditlog = new TMAuditLog();
$frmSplitXml = new FormsProcessor();

$btnSplit = new Button("btnSplit","btnSplit","Split XML");
$btnSplit->IsSubmit = true;
$btnSplit->Args = "onclick='javascript: return showloading();'";

$abspath = App::getParam('appviewdir');
$xmlnaming = App::getParam('xmlnaming');

$btnSuccessSplit = new Button("btnSuccessSplit","btnSuccessSplit","OK");
$btnSuccessSplit->IsSubmit = true;
$btnSuccessSplit->Args = "onclick='javascript: return redirectToSplitXMLPage();'";

$ddlfiletype = new ComboBox("ddlfiletype","ddlfiletype","File Type :");
$ddlfiletype->ShowCaption = true;
$options = null;
$options[0] = new ListItem("Please Select","",true);
$options[1] = new ListItem("Inventory","Inventory");
$options[2] = new ListItem("Winners","Winners");
$ddlfiletype->Items = $options;
$frmSplitXml->AddControl($ddlfiletype);
$frmSplitXml->AddControl($btnSplit);
$frmSplitXml->AddControl($btnSuccessSplit);
$frmSplitXml->ProcessForms();
$loading = "Splitting XML ".$ddlfiletype->SubmittedValue." file.";

if($frmSplitXml->IsPostBack)
{     
    if($btnSplit->SubmittedValue == "Split XML")
    {         
        $loading = "Splitting XML ".$ddlfiletype->SubmittedValue." file.";
        if($ddlfiletype->SubmittedValue == "Inventory")
        {
            if($_FILES['file']['type'] == "")//----if no files found
            {
                $errormsg = "File does not exist. Please enter the directory where the file is located.";
                $errormsgtitle = "ERROR!";
            }
            else if($_FILES['file']['type'] == "text/xml")//------------text or xml
            {
                $filename = $_FILES["file"]["name"];
                $fullpath = "splitted_xml/" . $_FILES["file"]["name"];

                // checking if file format is correct
                $length = 0;
                if ($xmlnaming == "FT"){ $length = 20;} 
                else { $length = 23; }
                
                $fileformatchecking = explode("_", $filename);
                if((count($fileformatchecking) > 2) || (count($fileformatchecking) < 2))
                {
                    $errormsg = "Incorrect file format. Please upload a valid file format.";
                    $errormsgtitle = "ERROR!";
                }
                else
                {
                    if((strlen($fileformatchecking[0]) > $length) || (strlen($fileformatchecking[0]) < $length))
                    {
                        $errormsg = "Incorrect file format. Please upload a valid file format.";
                        $errormsgtitle = "ERROR!";
                    }
                    else if((strlen($fileformatchecking[1]) > 5) || (strlen($fileformatchecking[1]) < 5))
                    {
                        $errormsg = "Incorrect file format. Please upload a valid file format.";
                        $errormsgtitle = "ERROR!";
                    }
                    else
                    {
                        //insert in auditlog table                
                        $tmaudit["SessionID"] = $_SESSION['sid'];
                        $tmaudit["AID"] = $_SESSION['acctid'];
                        $tmaudit["TransDetails"] = 'Split file: '.$ddlfiletype->SubmittedValue;
                        $tmaudit["TransDateTime"] = 'now_usec()';
                        $tmaudit["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
                        $tmaudit["AuditTrailFunctionID"] = '36';    
                        $tmauditlog->Insert($tmaudit);
                        if ($tmauditlog->HasError)
                        {
                            $errormsg = $tmauditlog->getError();
                            $errormsgtitle = "ERROR!";
                        }
                        else
                        {    
                            //read xml file
                            libxml_use_internal_errors(true);
                            move_uploaded_file($_FILES["file"]["tmp_name"], $fullpath);
                            $xml = simplexml_load_file($fullpath);

                            //checking if xml structure is correct
                            if (!$xml)
                            {
                                $errors = libxml_get_errors();
                                foreach ($errors as $error)
                                {
                                    $errormsgtitle = "ERROR!";
                                    $errormsg = display_xml_error($error);
                                }
                                libxml_clear_errors();
                            }
                            else
                            {
                                $filename_parts = explode(".",$filename);
                                $xml = simplexml_load_file($fullpath);
                                $node_count = count($xml->children());

                                $files_to_zip = array();
                                $xml_content = "";
                                $max_filesize = 200000;//209715;//1048576;
                                $i = 1;
                                $a = 1;
                                $b = 1;

                                foreach($xml->children() as $child)
                                {
                                    if(preg_match("/Philweb/", $filename_parts[0]))
                                    {
                                        $filetype = "Inventory";
                                    }
                                    if(strlen($xml_content) > $max_filesize)
                                    {
                                        $xml_content .= "</" . $xml->getName() . ">". "\n";
                                        $new_fullpath = "splitted_xml/" . $filename_parts[0] . "_" . $i . ".xml";
                                        $new_file = fopen($new_fullpath , "a+");
                                        fwrite($new_file , $xml_content);
                                        fclose($new_file);
                                        array_push($files_to_zip, $new_fullpath);
                                        $i++;
                                        $xml_content = "";
                                        $a = 1;
                                    }
                                    if($a == 1)
                                    {
                                        $xml_content .= "<" . $xml->getName() . ">" . "\n";
                                    }
                                    
                                    $xml_content .= "\t" . "<" . $child->getName() . ">" . "\n";
                                    foreach($child->children() as $children)
                                    {
                                        $xml_content .= "\t\t" . "<" . $children->getName() . ">" . $children . "</" . $children->getName() . ">" . "\n";
                                    }
                                    $xml_content .= "\t" . "</" . $child->getName() . ">" . "\n";

                                    if(($b == $node_count) && (strlen($xml_content) < $max_filesize))
                                    {
                                        $xml_content .= "</" . $xml->getName() . ">". "\n";
                                        $new_fullpath = "splitted_xml/" . $filename_parts[0] . "_" . $i . ".xml";
                                        $new_file = fopen($new_fullpath , "a+");
                                        fwrite($new_file , $xml_content);
                                        fclose($new_file);
                                        array_push($files_to_zip, $new_fullpath);
                                    }
                                    $a++;
                                    $b++;
                                }

                                $_SESSION['hiddenfiles'] = implode("," , $files_to_zip);
                                $success_title = "XML Split Successful";
                                $success_msg = create_zip($files_to_zip, $abspath . '/splitted_xml/my-archive.zip',$i,$filetype);
                                unlink($fullpath);
                            }
                        }
                    }
                }
            }
            else
            {
                $errormsg = "Incorrect file format. Please upload a valid file format.";
                $errormsgtitle = "ERROR!";
            }
        }

        //****************************** WINNERS *******************************
        if ($ddlfiletype->SubmittedValue == "Winners")
        {
            if($_FILES['file']['type'] == "")//----if no files found
            {
                $errormsg = "File does not exist. Please enter the directory where the file is located.";
                $errormsgtitle = "ERROR!";
            }
            else if($_FILES['file']['type'] == "text/xml")//------------text or xml
            {
                $filename = $_FILES["file"]["name"];
                $fullpath = "splitted_xml/" . $_FILES["file"]["name"];

                //-------------file checking
                $filechecking = explode("_", $filename);
                if((count($filechecking) < 4) || (count($filechecking) > 4))
                {
                    $errormsg = "Incorrect file format. Please upload a valid file format.";
                    $errormsgtitle = "ERROR!";
                }
                else
                {
                    if((strlen($filechecking[1]) < 5) || (strlen($filechecking[1]) > 5))
                    {
                        $errormsg = "Incorrect file format. Please upload a valid file format.";
                        $errormsgtitle = "ERROR!";
                    }
                    else if((strlen($filechecking[2]) < 4) || (strlen($filechecking[2]) > 4))
                    {
                        $errormsg = "Incorrect file format. Please upload a valid file format.";
                        $errormsgtitle = "ERROR!";
                    }
                    else if((strlen($filechecking[3]) < 8) || (strlen($filechecking[3]) > 8))
                    {
                        $errormsg = "Incorrect file format. Please upload a valid file format.";
                        $errormsgtitle = "ERROR!";
                    }
                    else
                    {
                        //insert in auditlog table                
                        $tmaudit["SessionID"] = $_SESSION['sid'];
                        $tmaudit["AID"] = $_SESSION['acctid'];
                        $tmaudit["TransDetails"] = 'Split file: '.$ddlfiletype->SubmittedValue;
                        $tmaudit["TransDateTime"] = 'now_usec()';
                        $tmaudit["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
                        $tmaudit["AuditTrailFunctionID"] = '36';    
                        $tmauditlog->Insert($tmaudit);
                        if ($tmauditlog->HasError)
                        {
                            $errormsg = $tmauditlog->getError();
                            $errormsgtitle = "ERROR!";
                        }
                        else
                        {                     
                            //read xml file
                            libxml_use_internal_errors(true);
                            move_uploaded_file($_FILES["file"]["tmp_name"], $fullpath);
                            $xml = simplexml_load_file($fullpath);

                            //checking if xml structure is correct
                            if (!$xml)
                            {
                                $errors = libxml_get_errors();
                                foreach ($errors as $error)
                                {
                                    $errormsgtitle = "ERROR!";
                                    $errormsg = display_xml_error($error);
                                }
                                libxml_clear_errors();
                            }
                            else
                            {
                                $filename_parts = explode(".",$filename);
                                $xml = simplexml_load_file($fullpath);
                                $node_count = count($xml->children());

                                $files_to_zip = array();
                                $xml_content = "";
                                $max_filesize = 4000000;//4194304;//1048576;
                                $i = 1;
                                $a = 1;
                                $b = 1;

                                foreach($xml->children() as $child)
                                {
                                    if(preg_match("/WLIST/", $filename_parts[0]))
                                    {
                                        $filetype = "Winners";
                                    }
                                    if(strlen($xml_content) > $max_filesize)
                                    {
                                        $xml_content .= "</" . $xml->getName() . ">". "\n";

                                        $new_fullpath = "splitted_xml/" . $filename_parts[0] . "_" . $i . ".xml";
                                        $new_file = fopen($new_fullpath , "a+");
                                        fwrite($new_file , $xml_content);
                                        fclose($new_file);
                                        array_push($files_to_zip, $new_fullpath);
                                        $i++;
                                        $xml_content = "";
                                        $a = 1;
                                    }
                                    if($a == 1)
                                    {
                                        $xml_content .= "<" . $xml->getName() . ">" . "\n";
                                    }

                                    $xml_content .= "\t" . "<" . $child->getName() . ">" . "\n";
                                    foreach($child->children() as $children)
                                    {
                                        $xml_content .= "\t\t" . "<" . $children->getName() . ">" . $children . "</" . $children->getName() . ">" . "\n";
                                    }
                                    $xml_content .= "\t" . "</" . $child->getName() . ">" . "\n";

                                    if(($b == $node_count) && (strlen($xml_content) < $max_filesize))
                                    {
                                        $xml_content .= "</" . $xml->getName() . ">". "\n";
                                        $new_fullpath = "splitted_xml/" . $filename_parts[0] . "_" . $i . ".xml";
                                        $new_file = fopen($new_fullpath , "a+");
                                        fwrite($new_file , $xml_content);
                                        fclose($new_file);
                                        array_push($files_to_zip, $new_fullpath);
                                    }
                                    $a++;
                                    $b++;
                                }

                                $_SESSION['hiddenfiles'] = implode("," , $files_to_zip);
                                $success_title = "XML Split Successful";
                                $success_msg = create_zip($files_to_zip, $abspath . '/splitted_xml/my-archive.zip',$i,$filetype);
                                unlink($fullpath);
                            }
                        }//end for audittrail
                    }
                }
            }
            else
            {
                $errormsg = "Incorrect file format. Please upload a valid file format.";
                $errormsgtitle = "ERROR!";
            }
        }
    
        if($ddlfiletype->SubmittedValue == "")
        {
            $errormsg = "Please select a file type.";
            $errormsgtitle = "ERROR!";
        }
    
    } //-if submitted value = split XML

    if($btnSuccessSplit->SubmittedValue == "OK")
    {
        download($_SESSION['hiddenfiles']);
    }
}


// Function to Download Zip File
function download($files_in_string)
{
    $abspath = App::getParam('appviewdir');
    header('Content-Disposition: attachment; filename="'.'my-archive.zip'.'"');
    header('Content-Transfer-Encoding: binary');
    header('Content-Type: '.' application/zip');
    header('Expires: 0');
    header('Pragma: no-cache');
    header('Content-Length: '.filesize($abspath . '/splitted_xml/my-archive.zip'));
    readfile($abspath . '/splitted_xml/my-archive.zip');

    $files_to_zip = explode(",",$files_in_string);
    foreach($files_to_zip as $file)
    {
        unlink($file);
    }
    unlink($abspath . '/splitted_xml/my-archive.zip');
    unset($_SESSION['hiddenfiles']);
}

// Function for Checking XML Structure
function display_xml_error($error)
{
    $return = "";

    switch ($error->level) {
        case LIBXML_ERR_WARNING:
            $return .= "Warning $error->code: ";
            break;
         case LIBXML_ERR_ERROR:
            $return .= "Error $error->code: ";
            break;
        case LIBXML_ERR_FATAL:
            $return .= "Fatal Error $error->code: ";
            break;
    }

    $return .= trim($error->message) .
               "<br>  Line: $error->line" .
               "<br/>  Column: $error->column";

    if ($error->file) {
        $return .= "<br/>  File: " . $error->file . "";
    }

    return addslashes("$return");
}

// Function that Creates a Zip File
function create_zip($files = array(),$destination = '',$parts_num = 0,$filetype = '',$overwrite = false)
{
    $valid_files = array();

    if(is_array($files))
    {
        foreach($files as $file)
        {
            if(file_exists($file))
            {
                $valid_files[] = $file;
            }
        }
    }
    
    if(count($valid_files))
    {
        $zip = new ZipArchive();
        if($zip->open($destination,$overwrite ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE) !== true)
        {
            return false;
        }

        foreach($valid_files as $file)
        {
            $zip->addFile($file,$file);
        }

        $zip->close();
    }
    
    return "File Type:&nbsp " . $filetype . "<br/>" .
           "Number of Parts :&nbsp&nbsp" . $parts_num . "<br/>" .
           
           "The XML Files may now be imported in the &nbsp" . $filetype . " page";
} 
?>