<?php
$pagesubmenuid = 8;
$javascripts[] = "jscripts/validations.js";
$stylesheets[] = "css/default.css";
$pagetitle = "TMS Admin Menus";
$modulename = "TicketManagementCM";

App::LoadModuleClass($modulename, "TMTicketValidation");
App::LoadModuleClass($modulename, "TMAccounts");
App::LoadModuleClass($modulename, "TMDecks");
App::LoadModuleClass($modulename, "TMGameManagement");
App::LoadModuleClass($modulename, "TMAuditLog");
App::LoadModuleClass($modulename, "TMTickets");
App::LoadModuleClass($modulename, "TMTicketCancellation");
App::LoadModuleClass($modulename, "TMBatchFileValidationTickets");

App::LoadControl("TextBox");
App::LoadControl("Button");
App::LoadControl("ComboBox");
App::LoadControl("Hidden");

$fproc = new FormsProcessor();

$tmticketvalidation = new TMTicketValidation();
$tmaccounts = new TMAccounts();
$tmdecks = new TMDecks();
$tmgamemgt = new TMGameManagement(); 
$tmauditlog = new TMAuditLog();
$tmtickets = new TMTickets();
$tmticketcancellation = new TMTicketCancellation();
$tmbatchfilevalidationtickets = new TMBatchFileValidationTickets();

$txtBookNumber = new TextBox("txtBookNumber", "txtBookNumber", "Book Number: ");
$txtBookNumber->Args="onkeypress='javascript: return checkKeycode(event);'";
$txtBookNumber->Style = "width:180px;";
$txtBookNumber->ShowCaption = false;
$txtBookNumber->Length = 12;

$ddlModeSelect = new ComboBox("ddlModeSelect", "ddlModeSelect", "Cancel Mode");
$options = null;
$options[] = new ListItem("Please Select","0",true);
$options[] = new ListItem("Void","5");
$options[] = new ListItem("Stolen (Payable)","7");
$options[] = new ListItem("Stolen (Non-Payable)","8");
$options[] = new ListItem("Expired (Payable)","9" );
$options[] = new ListItem("Expired (Non-Payable)","10" );
$ddlModeSelect->Items = $options;
$ddlModeSelect->CssClass = "options";

$txtusername = new TextBox("txtusername","txtusername","Username");
$txtusername->Length = 20;
//$txtusername->Args="onkeypress='javascript: return alphanumeric(event);' autocomplete='off'";

$txtpassword = new TextBox("txtpassword","txtpassword","Password");
$txtpassword->Length = 20;
$txtpassword->Password = true;
$txtpassword->Args="onkeypress='javascript: return disableSpace(event);' autocomplete='off'";

$txtRemarks = new TextBox("txtRemarks", "txtRemarks", "Remarks: ");
$txtRemarks->ShowCaption = false;
$txtRemarks->Multiline = true;
$txtRemarks->Style = "width:300px; height:150px;resize: none; vertical-align:top;";
$txtRemarks->Length = 150;

$btnError = new Button("btnError","btnError","Ok");
$btnError->Args = "onclick = 'javascript: return ShowAdminConfirmation();'";

$btnConfirm = new Button("btnConfirm","btnConfirm","Confirm");
$btnConfirm->IsSubmit = true;
$btnConfirm->Args = "onclick = 'javascript: return ValidateAdminConfirmation();'";

$btnProcess = new Button("btnProcess", "btnProcess", "Process");
$btnProcess->Args="onclick='javascript: return checkticketcancellation();'";
$btnProcess->IsSubmit = true;

$btnOk = new Button("btnOk", "btnOk", "YES");
$btnOk->IsSubmit = true;

$btnOkay =new Button("btnOkay", "btnOkay", "YES");
$btnOkay->IsSubmit = true;

$btnCancel =new Button("btnCancel", "btnCancel", "NO");
$btnCancel->IsSubmit = true;
$btnCancel->Args = "onclick = 'javascript: return donothing();'";

$fproc->AddControl($txtBookNumber);
$fproc->AddControl($ddlModeSelect);
$fproc->AddControl($txtusername);
$fproc->AddControl($txtpassword);
$fproc->AddControl($txtRemarks);
$fproc->Addcontrol($btnConfirm);
$fproc->AddControl($btnProcess);
$fproc->AddControl($btnOkay);
$fproc->AddControl($btnOk);
$fproc->ProcessForms();
if ($fproc->IsPostBack)
{
    $_SESSION['remarks'] = $txtRemarks->Text;
    $_SESSION['ddlmodeselect'] = $ddlModeSelect->SubmittedValue;
    $_SESSION['txtbooknumber'] = $txtBookNumber->Text;
    $accttype = $_SESSION['accttype'];
    
    if ($btnProcess->SubmittedValue == "Process")
    {   
        $fullbook = trim($txtBookNumber->Text);        
        $gameno = substr($fullbook, 0,strlen($fullbook) - 9);
        $ticketno = substr($fullbook,strlen($fullbook) - 3 ,3);
        $bookno = substr($fullbook, strlen($fullbook) - 9,6);
        
        $arrGame = $tmgamemgt->SelectByGameNumber($gameno);
        if (count($arrGame) == 1)
        {
            $arrdtls = $arrGame[0];
            $gameid = $arrdtls["GameID"];
            $bookticketcount = $arrdtls["BookTicketCount"];
        }
        else
        {
            $errormsg = 'Game number does not exist.';
            $errormsgtitle = 'ERROR!';
        }
        
       
        //check book status
        $arrBookStatus = $tmdecks->GetBookStatus($bookno,$gameid);
        if (count($arrBookStatus) == 1)
        {
            $arrdtls = $arrBookStatus[0];
            $bookstatus = $arrdtls["Status"];
            $id = $arrdtls["ID"];
            $bookid = $arrdtls["BookID"];
            
            if(($bookstatus == 5 || $bookstatus == 7 || $bookstatus == 8 || $bookstatus == 9 || $bookstatus == 10) && $ticketno == '000')
            {
                $errormsgtitle = "ERROR!";
                $errormsg = "The entered book number was already cancelled.";  
            }
            
            else if(($bookstatus != 5 || $bookstatus != 7 || $bookstatus != 8 || $bookstatus != 9 || $bookstatus != 10) && $ticketno == '000')
            {
                $confirmation = "true";
                //check ticket status
                $arrTicketDtls = $tmtickets->IsTicketNoValid($ticketno, $bookid);
                if (count($arrTicketDtls) == 1)
                {
                    $arrdtls2 = $arrTicketDtls[0];
                    $ticketstatus = $arrdtls2["Status"];
                    $ticketid = $arrdtls2["TicketID"];
                    if (($ticketstatus == 5 || $ticketstatus == 7 || $ticketstatus == 8 || $ticketstatus == 9 || $ticketstatus == 10))
                    {
                        $errormsg = 'The entered ticket number was already cancelled.';
                        $errormsgtitle = 'ERROR!';
                    }
                    else
                    {
                        $confirmation2 = "true";
                    }
                }
                else
                {
                    $errormsgtitle = "ERROR!";
                    $errormsg = "Ticket number does not exist.";
                }
                
            }
            else if(($bookstatus != 5 || $bookstatus != 7 || $bookstatus != 8 || $bookstatus != 9 || $bookstatus != 10) && $ticketno > '000')
            {
                $arrTicketDtls = $tmtickets->IsTicketNoValid($ticketno, $bookid);
                if (count($arrTicketDtls) == 1)
                {
                    $arrdtls2 = $arrTicketDtls[0];
                    $ticketstatus = $arrdtls2["Status"];
                    $ticketid = $arrdtls2["TicketID"];
                    if (($ticketstatus == 5 || $ticketstatus == 7 || $ticketstatus == 8 || $ticketstatus == 9 || $ticketstatus == 10))
                    {
                        $errormsg = 'The entered ticket number was already cancelled.';
                        $errormsgtitle = 'ERROR!';
                    }
                    else
                    {
                        $confirmation2 = "true";
                    }
                }
                else
                {
                    // check for book ticket size
                    if ($ticketno > $bookticketcount){
                        $errormsg = "Ticket exceeds the maximum book ticket size.";
                    } else {
                        $errormsg = "Ticket number does not exist.";   
                    }
                    $errormsgtitle = "ERROR!";
                }
            }
            else
            {
                $errormsgtitle = "ERROR!";
                $errormsg = "The entered book number was already cancelled.";              
            }
        }
        else
        {
            $errormsgtitle = "ERROR!";
            $errormsg = "Ticket/Book number is invalid.";
        }
    }
    
    if($btnOkay->SubmittedValue == "YES")
    { 
        if($accttype !=1)
        {
            $confirm = "ok";
        }
        else
        {
            if(($bookstatus != 5 || $bookstatus != 7 || $bookstatus != 8 || $bookstatus != 9 || $bookstatus != 10))
            {
                $result = cancelticketbooks();
                $errormsgtitle = $result["title"];
                $errormsg = $result["msg"];
            }
            else
            {
                $successtitle = "ERROR!";
                $successmsg = "The entered book number was already cancelled.";              
            }
        }
     }
     
     if($btnOk->SubmittedValue == "YES")
     { 
        if(($ticketstatus != 5 || $ticketstatus != 7 || $ticketstatus != 8 || $ticketstatus != 9 || $ticketstatus != 10))
        {
            $result = cancelticketbooks();
            $errormsgtitle = $result["title"];
            $errormsg = $result["msg"];
        }
        else
        {
            $successtitle = "ERROR!";
            $successmsg = "The entered ticket number was already cancelled.";              
        }
     }
    
  
    if($btnConfirm->SubmittedValue == "Confirm")
    {
        $username = mysql_escape_string(trim($txtusername->Text));
        $password = mysql_escape_string(trim($txtpassword->Text));
                                
        $isAdmin = $tmaccounts->CheckIfAdminAccount($username,$password);
        if(count($isAdmin) > 0)
        {
            $result = cancelticketbooks();
            $errormsgtitle = $result["title"];
            $errormsg = $result["msg"];
            
        }
        else
        {
            $txtusername->Text = "";
            $txtpassword->Text = "";
            $logintitle = "ERROR!";
            $loginmsg = "Please enter a valid administrator account.";
         }
    } 
}
 
function cancelticketbooks()
{
        App::LoadModuleClass("TicketManagementCM","TMGameManagement");
        App::LoadModuleClass("TicketManagementCM", "TMDecks");
        App::LoadModuleClass("TicketManagementCM", "TMBatchFileValidationTickets");
        App::LoadModuleClass("TicketManagementCM", "TMTickets");
        App::LoadModuleClass("TicketManagementCM", "TMTicketCancellation");
        App::LoadModuleClass("TicketManagementCM", "TMAuditLog");
        App::LoadModuleClass("TicketManagementCM", "TMAccounts");

        $tmbatchfilevalidationtickets = new TMBatchFileValidationTickets();
        $tmgamemgt = new TMGameManagement();
        $tmdecks = new TMDecks();
        $tmtickets = new TMTickets();
        $tmticketcancellation = new TMTicketCancellation();
        $tmauditlog = new TMAuditLog();
        
        //get book status 
        $fullbook = trim($_SESSION['txtbooknumber']);        
        $gameno = substr($fullbook, 0, strlen($fullbook) - 9);
        $ticketno = substr($fullbook, strlen($fullbook) - 3, 3);
        $bookno = substr($fullbook, strlen($fullbook) - 9, 6);
        
        $remarks = $_SESSION['remarks'];        
        $arrGame = $tmgamemgt->SelectByGameNumber($gameno);
        
        if (count($arrGame) == 1)
        {
            $arrdtls = $arrGame[0];
            $gameid = $arrdtls["GameID"];
        }
        else
        {
            $errormsg = 'Game number does not exist.';
            $errormsgtitle = 'ERROR!';
        }
        //check book status
        $arrBookStatus = $tmdecks->GetBookStatus($bookno,$gameid);
        if (count($arrBookStatus) == 1)
        {
            $arrdtls = $arrBookStatus[0];
            $bookstatus = $arrdtls["Status"];
            $id = $arrdtls["ID"];
            $bookid = $arrdtls["BookID"];
            if (($bookstatus == 5 || $bookstatus == 7 || $bookstatus == 8 || $bookstatus == 9 || $bookstatus == 10))
            {
                $errormsg = 'The entered book number was already cancelled.';
                $errormsgtitle = 'ERROR!';
            }
            else
            {
                if ($ticketno == '000')
                {
                        $cancelmodeval = $_SESSION['ddlmodeselect'];
                        //update book status
                        if($cancelmodeval == 5)
                        {
                            $tmbookparam["Status"] = 5;
                            $status = 5;
                        }
                        else if($cancelmodeval == 7)
                        {
                            $tmbookparam["Status"] = 7;
                            $status = 7;
                        }
                        else if($cancelmodeval == 8)
                        {
                            $tmbookparam["Status"] = 8;
                            $status = 8;
                        }
                        else if($cancelmodeval == 9)
                        {
                            $tmbookparam["Status"] = 9;
                            $status = 9;
                        }
                        else
                        {
                            $tmbookparam["Status"] = 10;
                            $status = 10;
                        }
                        $tmbookparam["Remarks"] = $remarks;
                        $tmbookparam["CancelledByAID"] = $_SESSION['acctid'];
                        $tmbookparam["DateCancelled"] = 'now_usec()';
                        $tmbookparam["UpdatedByAID"] = $_SESSION['acctid'];
                        $tmbookparam["ID"] = $id;
                        $tmupdatebook = $tmdecks->UpdateByArray($tmbookparam);
                        if ($tmdecks->HasError)
                        {
                            $errormsg = $tmdecks->getError();
                            $errormsgtitle = 'ERROR!';
                        }
                        else
                        {
                            // update batch file validation tickets
                            $tmbatchfilevalidationtickets->UpdateTicketsToCancelbyBatch($bookno);
                        
                            // update tickets 
                            $tmtickets->UpdateTicketsByBook2($bookid,$status);
                            if ($tmtickets->HasError)
                            {
                                $errormsg = $tmtickets->getError();
                                $errormsgtitle = 'ERROR!';
                            }    
                        
                            //insert in ticket cancellation table
                            $tmticketcancellation->TicketBatchInsert2($_SESSION['acctid'], $remarks, $bookid, $cancelmodeval);
                            if ($tmticketcancellation->HasError)
                            {
                                $errormsg = $tmticketcancellation->getError();
                                $errormsgtitle = 'ERROR!';
                            }
                        
                            //insert into audittrail
                            $tmaudit["SessionID"] = $_SESSION['sid'];
                            $tmaudit["AID"] = $_SESSION['acctid'];
                            $tmaudit["TransDetails"] = 'Book Number: '.$bookno;
                            $tmaudit["TransDateTime"] = 'now_usec()';
                            $tmaudit["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
                            $tmaudit["AuditTrailFunctionID"] = '13';    
                            $tmauditlog->Insert($tmaudit);
                            if ($tmauditlog->HasError)
                            {
                                $errormsg = $tmauditlog->getError();
                                $errormsgtitle = 'ERROR!';
                            }
                            else
                            {
                                $errormsg = 'Scratch card has been successfully cancelled.';
                                $errormsgtitle = 'SUCCESSFUL!';
                            }  
                        
                        }
                }
                else if ($ticketno > '000')
                {
                    //check ticket status 
                    $arrTicketDtls = $tmtickets->IsTicketNoValid($ticketno, $bookid);
                    if (count($arrTicketDtls) == 1)
                    {
                        $arrdtls2 = $arrTicketDtls[0];
                        $ticketstatus = $arrdtls2["Status"];
                        $ticketid = $arrdtls2["TicketID"];
                        if (($ticketstatus == 5 || $ticketstatus == 7 || $ticketstatus == 8 || $ticketstatus == 9 || $ticketstatus == 10))
                        {
                            $errormsg = 'The entered ticket number was already cancelled.';
                            $errormsgtitle = 'ERROR!';
                        }
                        else
                        {
                            $cancelmodeval = $_SESSION['ddlmodeselect'];

                            if ($cancelmodeval == 5)
                            {
                                $tmbatchfilevalidationtickets->UpdateBatchFileToCancelV($_SESSION['txtbooknumber'], $cancelmodeval);                                                                
                            }
                            else if($cancelmodeval == 7)
                            {
                                $tmbatchfilevalidationtickets->UpdateBatchFileToCancelSP($_SESSION['txtbooknumber'],$cancelmodeval);                                                                
                            }
                            else if($cancelmodeval == 8)
                            {
                                $tmbatchfilevalidationtickets->UpdateBatchFileToCancelSP($_SESSION['txtbooknumber'],$cancelmodeval);                                                               
                            }
                            else if($cancelmodeval == 9)
                            {
                                $updateBatchFileTickets = $tmbatchfilevalidationtickets->UpdateBatchFileToCancelSP($_SESSION['txtbooknumber'],$cancelmodeval);                                                                 
                            }
                            else if($cancelmodeval == 10)
                            {
                                $updateBatchFileTickets = $tmbatchfilevalidationtickets->UpdateBatchFileToCancelSP($_SESSION['txtbooknumber'],$cancelmodeval);
                            }
                                
                            //update tickets table
                            $tmticketsparam["Status"] = $cancelmodeval;
                            $tmticketsparam["TicketID"] = $ticketid;
                            $tmupdatebook = $tmtickets->UpdateByArray($tmticketsparam);
                            if ($tmtickets->HasError)
                            {
                                $errormsg = $tmtickets->getError();
                                $errormsgtitle = 'ERROR!';
                            }                            

                            //insert in ticket cancellation table
                            $tmticket["TicketID"] = $ticketid;
                            $tmticket["DateCancelled"] = 'now_usec()';
                            $tmticket["CancelledByAID"] = $_SESSION['acctid'];
                            $tmticket["Remarks"] =  $remarks;
                            $tmticket["CancelType"] =  $cancelmodeval;
                            $tmticketcancellation->Insert($tmticket);
                            if ($tmticketcancellation->HasError)
                            {
                                $errormsg = $tmticketcancellation->getError();
                                $errormsgtitle = 'ERROR!';
                            }
                            else
                            {
                                //insert into audittrail
                                $tmaudit["SessionID"] = $_SESSION['sid'];
                                $tmaudit["AID"] = $_SESSION['acctid'];
                                $tmaudit["TransDetails"] = 'Book Number: '.$bookno;
                                $tmaudit["TransDateTime"] = 'now_usec()';
                                $tmaudit["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
                                $tmaudit["AuditTrailFunctionID"] = '23';    
                                $tmauditlog->Insert($tmaudit);
                                if ($tmauditlog->HasError)
                                {
                                    $errormsg = $tmauditlog->getError();
                                    $errormsgtitle = 'ERROR!';
                                }
                                else
                                {
                                    $errormsg = 'Scratch card has been successfully cancelled.';
                                    $errormsgtitle = 'SUCCESSFUL!';
                                }     
                            }
                        }
                    }    
                    else
                    {
                         $errormsg = "Ticket number does not exist.";
                         $errormsgtitle = 'ERROR!';  
                    }
                               
                }
                else
                {
                    $errormsg = 'Ticket/Book Number is invalid.';
                    $errormsgtitle = 'ERROR!';
                }           
        
            }
        }
        else
        {
            $errormsg = 'Book number does not exist.';
            $errormsgtitle = 'ERROR!';
        }
    
        $array = array('title'=>$errormsgtitle, 'msg'=>$errormsg);
        return $array;
}
?>

