<?php

/* Date : January 7 ,2013
 * Created by Jerome F. Jose 
 * Modified By: Noel Antonio
 * Date Modified: January 14, 2013
 * Purpose : To transfer Ticket
 */
$pagesubmenuid = 49;
$stylesheets[] = "css/default.css";
$javascripts[] = "jscripts/validations.js";
$javascripts[] = "jscripts/jquery-1.5.2.min.js";

App::LoadModuleClass("TicketManagementCM", "TMDecks");
App::LoadModuleClass("TicketManagementCM", "TMAuditLog");
App::LoadModuleClass("TicketManagementCM", "TMGameManagement");
App::LoadModuleClass("TicketManagementCM", "TMAccounts");
App::LoadModuleClass("TicketManagementCM", "TMProducts");
App::LoadModuleClass("TicketManagementCM", "TMTicketTransferHistory");

$tmdecks = new TMDecks();
$tmauditlog = new TMAuditLog();
$tmgames = new TMGameManagement();
$tmaccounts = new TMAccounts();
$tmproducts = new TMProducts();
$tickettranshist = new TMTicketTransferHistory();

App::LoadControl("TextBox");
App::LoadControl("ComboBox");
App::LoadControl("Button");
App::LoadControl("Hidden");
App::LoadControl("PagingControl2");

$formticketreassgnmnt = New FormsProcessor();

//get list of game names
$gamenames = $tmproducts->SelectAllGameName();
$gamename_list = new ArrayList();
$gamename_list->AddArray($gamenames);

$ddlgamename = new ComboBox("ddlgamename","ddlgamename","Game Name: ");
$ddlgamename->Args = "onchange = 'javascript: get_gamenumber();'";
$gamenameopt = null;
$gamenameopt[] = new ListItem("Please select", "0", true);
$ddlgamename->Items = $gamenameopt;
$ddlgamename->DataSource = $gamename_list;
$ddlgamename->DataSourceText = "ProductName";
$ddlgamename->DataSourceValue = "ProductID";
$ddlgamename->DataBind();

//game number
$allgames = $tmgames->SelectAllActive();
$gameslist = new ArrayList();
$gameslist->AddArray($allgames);
$ddlgametype = new ComboBox("ddlgametype","ddlgametype","Game Number: ");
$options = null;
$options[] = new ListItem("Please select","0");
$ddlgametype->Items = $options;
$ddlgametype->Args = "onchange='javascript:get_booknumbers();'";
$ddlgametype->DataSource = $gameslist;
$ddlgametype->DataSourceText = "GameNumber";
$ddlgametype->DataSourceValue = "GameID";
$ddlgametype->DataBind();

//transfer to
$accounts = $tmaccounts->SelectTransfertTicket();
$accounts_list = new ArrayList();
$accounts_list->AddArray($accounts);
$ddlassingedto = new ComboBox("ddlassingedto","ddlassingedto","Transfer To:");
$litem = null;
$litem[] = new ListItem("Please select","0");
$ddlassingedto->Items = $litem;
$ddlassingedto->DataSource = $accounts_list;
$ddlassingedto->DataSourceText = "Name";
$ddlassingedto->DataSourceValue = "AID";
$ddlassingedto->DataBind();

//ticket count
$txtTicketCount = new TextBox('txtTicketCount','txtTicketCount','');
$txtTicketCount->Length = 3;
$txtTicketCount->Args = "size= 7;onkeypress = 'javascript: return isNumberKey(event);'";

//book number
$ddlbooknumber = new ComboBox('ddlbooknumber','ddlbooknumber');
$nullbook = null;
$nullbook[] = new ListItem("Please select", "0", true);
$ddlbooknumber->Items = $nullbook;

$btnsubmit = new Button("btnsubmit","btnsubmit","Transfer");
$btnsubmit->IsSubmit = true;
$btnsubmit->Args = "onclick = 'javascript: return ticketreassignmentvalidation();'";

$hiddenflag = new Hidden("hiddenflag","hiddenflag","Hidden Flag");

$btnYes = new Button('btnYes','btnYes','Yes');
$btnYes->IsSubmit = true;

$formticketreassgnmnt->AddControl($ddlgamename);
$formticketreassgnmnt->AddControl($ddlgametype);
$formticketreassgnmnt->AddControl($ddlassingedto);
$formticketreassgnmnt->AddControl($txtTicketCount);
$formticketreassgnmnt->AddControl($ddlbooknumber);
$formticketreassgnmnt->AddControl($btnsubmit);
$formticketreassgnmnt->AddControl($hiddenflag);
$formticketreassgnmnt->AddControl($btnYes);
$formticketreassgnmnt->ProcessForms();
if($formticketreassgnmnt->IsPostBack)
{
        $gamename = $ddlgamename->SubmittedValue;
        $gamenumber = $ddlgametype->SubmittedValue;
        $booknumber = $ddlbooknumber->SubmittedValue;
        $ticketcount = $txtTicketCount->SubmittedValue;
        $transferto = $ddlassingedto->SubmittedValue;
        $succes = 0;

        if($hiddenflag->SubmittedValue == 3)
        {
            
                $booknumbers = $tmdecks->SelectBookNumbersPerProductIDandGameID($gamename,$gamenumber,1);
                $fromAID = $booknumbers[0]['AssignedToAID'];
                if(count($booknumbers) > 0)
                {
                        for($i = 0 ; $i < count($booknumbers) ; $i++)
                        {
                                $arrbooknum[] = array( "IncrementalID" => $booknumbers[$i]["BookID"] ,
                                        "BookNumber" => $booknumbers[$i]["BookNumber"] ,
                                        "ID" => $booknumbers[$i]["ID"] ,
                                        "GameBatchID" => $booknumbers[$i]["GameBatchID"]
                                );
                        }
                }
                
                $frombooknum_list = new ArrayList();
                $frombooknum_list->AddArray($arrbooknum);
                $ddlbooknumber->DataSource = $frombooknum_list;
                $ddlbooknumber->DataSourceText = "BookNumber";
                $ddlbooknumber->DataSourceValue = "IncrementalID";
                $ddlbooknumber->DataBind();
                $ddlbooknumber->SetSelectedValue($booknumber);
            
        }
        
        if ($btnsubmit->SubmittedValue == "Transfer")
        {
            $confirm_msg = 'Are you sure you want to transfer the ticket/s?';
        }
        
        if($btnYes->SubmittedValue == 'Yes')
        {
                $dateactivated = $tmdecks->SelectByID($booknumber);
                $datereleased = $dateactivated[0]['DateReleased'];
                
                //insert to tickettransferhistory table
                $arrticketranshist['ProductID'] = $gamename;
                $arrticketranshist['GameID'] = $gamenumber;
                $arrticketranshist['BookID'] = $booknumber;
                $arrticketranshist['TicketCount'] = $ticketcount;
                $arrticketranshist['FromAID'] = $fromAID;
                $arrticketranshist['ToAID'] = $transferto;
                $arrticketranshist['DateCreated'] = 'now_usec()';
                $arrticketranshist['TicketsRecalled'] = 200 - $ticketcount;
                $arrticketranshist['DateReleased'] = $datereleased;
                $tickettranshist->Insert($arrticketranshist);
                if($tickettranshist->HasError)
                {
                    $errortitle = "ERROR!";
                    $errormsg = "Error in inserting  TicketTranshist: " . $tickettranshist->getError();
                }
                else
                {
                    $succes ++;
                }
                
                //insert to audit trail
                $auditdtls["AuditTrailFunctionID"] = '39';
                $auditdtls["SessionID"] = $_SESSION['sid'];
                $auditdtls["AID"] = $_SESSION['acctid'];
                $auditdtls["TransDetails"] = "Transferred from " . $transferto . " to " . $gamename . " Book ID: " . $booknumber;
                $auditdtls["TransDateTime"] = 'now_usec()';
                $auditdtls["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
                $tmauditlog->Insert($auditdtls);
                if($tmauditlog->HasError)
                {
                    $errortitle = "ERROR!";
                    $errormsg = "Error in inserting auditlog: " . $tmauditlog->getError();
                }
                else
                {
                        $succes ++;
                }

                if($succes == 2)
                {
                    $succesmsg = "Ticket/s successfully transferred.";
                }
        }
}
?>
