<?php
/**
 * @author      :   Noel Antonio
 * Purpose      :   Controller for ticketvalidation
 */
$pagesubmenuid = 7;
$stylesheets[] = "css/default.css";

App::LoadModuleClass("TicketManagementCM", "TMTicketValidation");
App::LoadModuleClass("TicketManagementCM", "TMDecks");
App::LoadModuleClass("TicketManagementCM", "TMDeckInfo");
App::LoadModuleClass("TicketManagementCM", "TMAccounts");
App::LoadModuleClass("TicketManagementCM", "TMAuditLog");
App::LoadModuleClass("TicketManagementCM", "TMPrizes");
App::LoadModuleClass("TicketManagementCM", "TMProducts");
App::LoadModuleClass("TicketManagementCM", "TMGameManagement");
App::LoadModuleClass("TicketManagementCM", "TMTickets");
App::LoadModuleClass("TicketManagementCM", "TMBatchValidationSession");
App::LoadModuleClass("TicketManagementCM", "TMBatchValidationTickets");
App::LoadModuleClass("TicketManagementCM", "TMBatchFileValidationTickets");
App::LoadModuleClass("TicketManagementCM", "TMBatchTransactionLog");
App::LoadModuleClass("TicketManagementCM", "TMCurrencies");
App::LoadModuleClass("TicketManagementCM", "TMHotTickets");
App::LoadModuleClass("TicketManagementCM", "TMTicketCancellation");
App::LoadModuleClass("TicketManagementCM", "TMDeckStatusHistory");

App::LoadControl("TextBox");
App::LoadControl("Hidden");
App::LoadControl("ComboBox");
App::LoadControl("Button");
App::LoadControl("PagingControl2");

$confirmation = 'false';
$prizetypeid = '0';
$accttypeid = $_SESSION['accttype'];
$aid = $_SESSION['acctid'];

$fproc = new FormsProcessor();

$tmbatchfilevalidationtickets =  new TMBatchFileValidationTickets();
$tmbatchvalidationsession = new TMBatchValidationSession();
$tmbatchvalidationtickets = new TMBatchValidationTickets();
$tmticketvalidation = new TMTicketValidation();
$tmdecks = new TMDecks();
$tmdeckinfo = new TMDeckInfo();
$tmaccount = new TMAccounts();
$tmauditlog = new TMAuditLog();
$tmprizes = new TMPrizes();
$tmgamemgt = new TMGameManagement();
$tmtickets = new TMTickets();
$tmcurrencies = new TMCurrencies();
$tmhottickets = new TMHotTickets();
$tmdeckstatus = new TMDeckStatusHistory();

$arrcurr = $tmcurrencies->SelectByCurrencySymbol("KHR");
$fixedrate = $arrcurr[0]["FixedRate"];

$ddlUsername = new ComboBox("ddlUsername", "ddlUsername", "Account Name: ");
$ddlUsername->TabIndex = 4;
$ddlProduct = new ComboBox("ddlProduct", "ddlProduct", "ProductName: ");
$ddlProduct->Args="onchange='javascript: return loadgames();'";

$txtValidationNumber = new TextBox("txtValidationNumber", "txtValidationNumber", "Validation Number: ");
$txtValidationNumber->TabIndex = 1;
$txtValidationNumber->Args="onkeypress='javascript: return checkKeycode(event);' autocomplete='off'";
$txtValidationNumber->Length = 12;

$txtBookNumber = new TextBox("txtBookNumber", "txtBookNumber", "Book Number: ");
$txtBookNumber->TabIndex = 2;
$txtBookNumber->Args="onkeypress='javascript: return checkKeycode(event);' autocomplete='off'";
$txtBookNumber->Length = 12;

$btnValidate = new Button("btnValidate", "btnValidate", "Validate");
$btnValidate->TabIndex = 3;
$btnValidate->Args="onclick='javascript: return checkvalidation();'";
$btnValidate->IsSubmit = true;

$btnSubmit = new Button("btnSubmit", "btnSubmit", "Submit");
$btnSubmit->IsSubmit = true;

$btnOk = new Button("btnOk", "btnOk", "CLAIM");
$btnOk->IsSubmit = true;

$btnClaimReimburse = new Button("btnClaimReimburse", "btnClaimReimburse", "CLAIM AND REIMBURSE");
$btnClaimReimburse->IsSubmit = true;

$arraccounts = $tmaccount->SelectForValidationModule($aid);
$accountlist = new ArrayList();
$accountlist->AddArray($arraccounts);
$ddlUsername->ClearItems();
$litem = null;
$litem[] = new ListItem("Select Account Name", "0", true);
$ddlUsername->Items = $litem;
$ddlUsername->DataSource = $accountlist;
$ddlUsername->DataSourceText = "UserName";
$ddlUsername->DataSourceValue = "AID";
$ddlUsername->DataBind();

$txtDateFr = new TextBox("txtDateFr", "txtDateFr", "From : ");
$txtDateFr->ShowCaption = true;
$txtDateFr->ReadOnly = true;
$txtDateFr->Style = "text-align: center";
$txtDateFr->Args = "size='9'";
//$txtDateFr->Text = date('Y-m-d');

$txtDateTo = new TextBox("txtDateTo", "txtDateTo", "To : ");
$txtDateTo->ShowCaption = true;
$txtDateTo->ReadOnly = true;
$txtDateTo->Style = "text-align: center";
$txtDateTo->Args = "size='9'";
//$txtDateTo->Text = date('Y-m-d');

$hidProdID = new Hidden("hidProdID","hidProdID","");
$hidchangedate = new Hidden("hidchangedate","hidchangedate","");

$itemsperpage = 10;
$pgcon = new PagingControl2($itemsperpage, 1);
$pgcon->URL = "javascript:ChangePage(%currentpage);";
$pgcon->ShowMoveToFirstPage = true;
$pgcon->ShowMoveToLastPage = true;
$pgcon->PageGroup = 5;

$arrht = $tmhottickets->selectLatestHotTickets();
$reccount = count($arrht);
$pgcon->Initialize($itemsperpage, $reccount);
$pgTransactionHistory = $pgcon->PreRender();
$list_ht = new ArrayList();
$list_ht->AddArray($arrht);

$fproc->AddControl($ddlUsername);
$fproc->AddControl($hidProdID);
$fproc->AddControl($hidchangedate);
$fproc->AddControl($txtValidationNumber);
$fproc->AddControl($txtBookNumber);
$fproc->AddControl($txtDateFr);
$fproc->AddControl($txtDateTo);
$fproc->AddControl($btnValidate);
$fproc->AddControl($btnSubmit);
$fproc->AddControl($btnOk);
$fproc->AddControl($btnClaimReimburse);

$fproc->ProcessForms();

if ($accttypeid == 5 || $accttypeid == 6)
{
    $ddlUsername->Enabled = false;
}

if ($fproc->IsPostBack)
{
    if ($btnSubmit->SubmittedValue == "Submit"){ $pgcon->SelectedPage = 1; }
    
    if ($btnSubmit->SubmittedValue == "Submit" || $hidchangedate->SubmittedValue == 1)
    {
        $continueSearch = false;
        if (trim($txtDateFr->SubmittedValue) == "")
        {
            $errormsg = "Please select Date From.";
            $errormsgtitle = 'ERROR!';
        }
        else if (trim($txtDateTo->SubmittedValue) == "")
        {
            $errormsg = "Please select Date To.";
            $errormsgtitle = 'ERROR!';
        }
        else if (strtotime($txtDateFr->SubmittedValue) > strtotime($txtDateTo->SubmittedValue))
        {
            $errormsg = "Start date is ahead than the End Date.";
            $errormsgtitle = 'ERROR!';
        }
        else if (strtotime($txtDateFr->SubmittedValue) > strtotime(date('Y-m-d')) || strtotime($txtDateTo->SubmittedValue) > strtotime(date('Y-m-d')))
        {
            $errormsg = "The date is ahead of time";
            $errormsgtitle = 'ERROR!';
        }
        else
        {
            $continueSearch = true;
            $txtValidationNumber->Text = $txtValidationNumber->SubmittedValue;
            $txtBookNumber->Text = $txtBookNumber->SubmittedValue;
        }
    }
    
    $fullvirn = trim(str_replace(array('\r', '\n', '\r\n'), "", $txtValidationNumber->Text));
    $fullbook = trim(str_replace(array('\r', '\n', '\r\n'), "", $txtBookNumber->Text));
    $checksum = substr($fullvirn, strlen($fullvirn) - 1, strlen($fullvirn));
    $gameno = substr($fullvirn,0,strlen($fullvirn) - 9);
    $valno = substr($fullvirn, 0,strlen($fullvirn) - 1);
    $valno2 = substr($valno, strlen($valno) - 8, strlen($valno));
    $ticketno = substr($fullbook,strlen($fullbook) - 3,3);
    $book = substr($fullbook, strlen($fullbook) - 9 ,6);
    $gameno1 = substr($fullbook,0,strlen($fullbook) - 9);
    $subvirn = $gameno;
    $subticket = $gameno1;
    
    if ($btnValidate->SubmittedValue == "Validate")
    {
        if ($ddlUsername->Enabled == 'true')
        {
            $unameid = $ddlUsername->SubmittedValue;        
        }
        else
        {
            $unameid = '0';
        }
        
        if ($subvirn != $subticket)
        {
            $errormsg = "Game Number  does not match for the ticket and validation number.";
            $errormsgtitle = 'ERROR!';
        }
        else 
        {
            //check if game number is valid
            $arrGame = $tmgamemgt->SelectByGameNumber($gameno);
            if (count($arrGame) == 1)
            {
                $arrdtls = $arrGame[0];
                $gameid = $arrdtls["GameID"];
                $bookticketcount = $arrdtls["BookTicketCount"];
            }
            else
            {
                $errormsg = 'Game number does not exist.';
                $errormsgtitle = 'ERROR!';
            }

            $checkdigit = CheckDigit($valno);

            if($checkdigit % 10 != $checksum)
            {
                $errormsg = 'Please enter a valid VIRN.';
                $errormsgtitle = 'ERROR!';
            } 
            else
            {
                //check if valid VIRN
                $arrWinning = $tmticketvalidation->IsValidVIRN($valno2,$gameid);
                if (count($arrWinning) == 1)
                {
                    $arrdtls = $arrWinning[0];
                    $winningcardid = $arrdtls["WinningCardID"];
                    $prizetypeid = $arrdtls["PrizeTypeID"];
                    $prizename = $arrdtls["PrizeName"];
                    $claimdate = $arrdtls["ClaimDate"];
                    $claimtime = $arrdtls["ClaimTime"];
                    $status = $arrdtls["Status"];
                    
                    if (strpos($prizename, 'KHR') !== false)
                    {
                        $str_prize = preg_replace('/[\KHR,]/', '', $prizename);
                        $usd_prize = $str_prize / $fixedrate;
                        $prizevalue = "$" . $usd_prize;
                    }
                    else
                    {
                        $prizevalue = $prizename;
                    }
                                    
                    //check if valid book number
                    $arrBookDtls = $tmdecks->IsBookNoValid($book,$gameid);
                    if (count($arrBookDtls) == 1)
                    {
                        $arrdtlsbooks = $arrBookDtls[0];
                        $statusbooks = $arrdtlsbooks["Status"];
                        $bookid = $arrdtlsbooks["BookID"];
                        if ($statusbooks == 1 || $statusbooks == 5 || $statusbooks == 7 || $statusbooks == 8 || $statusbooks == 9 || $statusbooks == 10)
                        {
                            $where = "WHERE BookID='$bookid' ORDER BY DateChanged DESC LIMIT 1";
                            $ds = $tmdeckstatus->SelectByWhere($where);
                            $oldstatus = $ds[0]["OldStatus"];
                            if (($oldstatus == 2 || $oldstatus == 3 || $oldstatus == 4) && ($statusbooks != 1)){
                                $errormsg = "Inactive tickets prior to cancellation are not allowed for validation.";
                                $errormsgtitle = 'ERROR!';
                            }
                            else
                            {
                                //check if ticketno is valid
                                $arrTicketDtls = $tmtickets->IsTicketNoValid($ticketno, $bookid);
                                if (count($arrTicketDtls) == 1)
                                {
                                    $arrdtls2 = $arrTicketDtls[0];
                                    $ticketid = $arrdtls2["TicketID"];
                                    $ticketstatus = $arrdtls2["Status"];
                                    if ($status == 0 || $status == 1)
                                    {
                                        if ($ticketstatus == 1)
                                        {
                                                $continue = 1;
                                        }
                                        elseif ($ticketstatus == 5)
                                        {
                                                saveToHotTickets($ticketid, $_SESSION['acctid'], $fullvirn, $fullbook);
                                                $errormsg = "Cannot process. This card / book has been declared void.";
                                                $errormsgtitle = 'ERROR!';
                                        }
                                        elseif ($ticketstatus == 6)
                                        {
                                                $errormsg = "Ticket was already claimed. Performing the ticket validation will not be allowed.";
                                                $errormsgtitle = 'ERROR!';
                                        }
                                        elseif ($ticketstatus == 7)
                                        {
                                                saveToHotTickets($ticketid, $_SESSION['acctid'], $fullvirn, $fullbook);
                                                $claimcancelmsg = "This card has been reported stolen. Proceed to Claim?";
                                                $continue = 2;
                                        }
                                        elseif ($ticketstatus == 8)
                                        {
                                                saveToHotTickets($ticketid, $_SESSION['acctid'], $fullvirn, $fullbook);
                                                $errormsg = "This card has been reported stolen and declared invalid for prize claiming.";
                                                $errormsgtitle = 'ERROR!';
                                        }
                                        elseif ($ticketstatus == 9)
                                        {
                                                saveToHotTickets($ticketid, $_SESSION['acctid'], $fullvirn, $fullbook);
                                                $claimcancelmsg = "This card has expired. Proceed to Claim?";
                                                $continue = 2;
                                        }
                                        elseif ($ticketstatus == 10)
                                        {
                                                saveToHotTickets($ticketid, $_SESSION['acctid'], $fullvirn, $fullbook);
                                                $errormsg = "This card has expired and declared invalid for prize claiming.";
                                                $errormsgtitle = 'ERROR!';
                                        }
                                    }
                                    else
                                    {
                                        $errormsgtitle = "Claimed Ticket";
                                        $errormsg = "Scratch card prize has already been claimed last ".$claimdate." at ".$claimtime.".";
                                    }
                                }
                                else
                                {
                                        // check for book ticket size
                                        if ($ticketno > $bookticketcount){
                                            $errormsg = "Ticket exceeds the maximum book ticket size.";
                                            $errormsgtitle = 'ERROR!';
                                        } else {
                                            $errormsg = "Scratch card does not exist";
                                            $errormsgtitle = 'ERROR!';    
                                        }
                                }
                            }
                        }
                        else
                        {
                            if ($statusbooks == 4)
                            {
                                $errormsg = "Please enter a ticket number that has already been activated."; //"Book number is assigned.";                    
                            }
                            elseif ($statusbooks == 2)
                            {
                                $errormsg = "Please enter a ticket number that has already been activated."; //"Book number is still on freight.";                    
                            }
                            elseif ($statusbooks == 3)
                            {
                                $errormsg = "Please enter a ticket number that has already been activated."; //"Book number is on stock.";                    
                            }
                            $errormsgtitle = 'ERROR!';  
                        }

                    }
                    else
                    {            
                        $errormsg = "Book number does not exist.";
                        $errormsgtitle = 'ERROR!';                
                    }
                }
                else
                {
                    $errormsg = "Sorry, your ticket is not a winning card.";
                    $errormsgtitle = 'ERROR!';
                }
                
                if (isset($continue)){
                    //get payout level and check if user is allowed to payout the prize
                    $arrprize = $tmprizes->SelectByPrizeStatus($prizename,$gameid);                
                    if (count($arrprize) == 1)
                    {
                        $arrdtls = $arrprize[0];
                        $payoutlevel = $arrdtls["PayoutLevel"];
                    }

                    if ($_SESSION['accttype'] == 2) // Account Manager Validate Only
                    {
                            $errormsg = "Scratch card with prize of " . $prizevalue . " is a winning card.";
                            $errormsgtitle = 'CONFIRMATION!';
                    }
                    else
                    {
                            $arraccounts = $tmaccount->SelectByAccountID($unameid);
                            if (count($arraccounts) == 1)
                            {
                                $arrdtls = $arraccounts[0];
                                $uname = $arrdtls["UserName"];                   
                            }

                            //check ticket status 
                            if ($status == 0 || $status == 1)// new and verified
                            {                 
                                if ($prizetypeid == 1)//major prize
                                {         
                                    if ($accttypeid > 3)
                                    {
                                        $confirmation = 'false';
                                        $errormsgtitle = "Major Prize!";
                                        $errormsg = "Congratulations, your scratch card has won the major prize. Please go to the Head Office to claim your prize.";
                                    }
                                    else
                                    {
                                        $confirmation = 'true';
                                        $errormsgtitle = "Major Prize!";
                                        if ($unameid > 0)
                                        {             
                                            $errormsg = "Your ticket has won a major prize of ".$prizevalue.". Claim prize now using the ".$uname." account?";                                                          
                                        }
                                        else 
                                        {
                                            $errormsg = "Congratulations, your scratch card has won a major prize of ".$prizevalue.". Claim winning prize?";      
                                        }  
                                    }
                                }
                                elseif ($prizetypeid == 2)//minor prize
                                {
                                    // distributor selected and admin/prodmanager logged-in
                                    if ($unameid > 0 && ($_SESSION['accttype'] == 1 || $_SESSION['accttype'] == 3))
                                    {
                                        $chk_claim_reimburse = 1;
                                    }
                                    /*else if ($_SESSION['accttype'] == 6)
                                    {
                                        $chk_claim_reimburse = 1;
                                    }*/

                                    if ($unameid > 0)
                                    {      
                                            $errormsgtitle = "Congratulations!";
                                            $errormsg = "Your ticket has won a consolation prize of ".$prizevalue.". Claim and reimburse prize now using the ".$uname." account?";                                                          
                                            $confirmation = 'false';
                                    }
                                    else
                                    {
                                            $confirmation = 'true';
                                            $errormsgtitle = "Congratulations!";
                                            $errormsg = "Your ticket has won a consolation prize of ".$prizevalue.". Claim prize now using the ".$_SESSION['uname']." account?"; //". Claim winning prize?";      
                                    }   

                                    if (($payoutlevel > 2)  && (($accttypeid == 5) || ($accttypeid == 6)))
                                    {
                                            $confirmation = 'false';
                                            $errormsgtitle = "Congratulations!";
                                            $errormsg = "Congratulations, your scratch card has won a consolation prize of ".$prizevalue.". Your account is not authorized to payout this amount.";      
                                    } 
                                }
                            }
                            elseif ($status == 2) // claimed
                            {
                                    $errormsgtitle = "Claimed Ticket";
                                    $errormsg = "Scratch card prize has already been claimed last ".$claimdate." at ".$claimtime.".";
                            }
                            elseif ($status == 5) //Reimbursed
                            {
                                    $errormsgtitle = "Claimed Ticket";
                                    $errormsg = "Scratch card prize has already been claimed and reimbursed last ".$claimdate." at ".$claimtime.".";
                            }

                            if ($continue == 2){
                                $errormsg = $claimcancelmsg;
                            }

                            //update winning card status to 'verified' 
                            if ($status == '0'){ $tmparam["Status"] = '1';} 
                            elseif ($status == '5'){ $tmparam["Status"] = '5'; }

                            $tmparam["WinningCardID"] = $winningcardid;
                            $tmticketvalidation->UpdateByArray($tmparam);
                            if ($tmticketvalidation->HasError)
                            {
                                    $errormsg = $tmticketvalidation->getError();
                                    $errormsgtitle = 'ERROR!';
                            }
                            $_SESSION['unameid'] = $unameid;
                            $_SESSION['gameid'] = $gameid;
                            $_SESSION['payoutlevel'] = $payoutlevel;
                            $_SESSION['prizevalue'] = $prizevalue;
                            $_SESSION['winningcardid'] = $winningcardid;
                            $_SESSION['prizetypeid'] = $prizetypeid;
                    }
                }               
                unset($continue);
            }
        }
    }

    if ($btnOk->SubmittedValue == "CLAIM" || $btnClaimReimburse->SubmittedValue == "CLAIM AND REIMBURSE")
    {
        $unameid = $_SESSION['unameid'];
        $gameid = $_SESSION['gameid'];
        $payoutlevel = $_SESSION['payoutlevel'];
        $prizevalue = $_SESSION['prizevalue'];
        $winningcardid = $_SESSION['winningcardid'];
        $prizetypeid = $_SESSION['prizetypeid'];

        if(($payoutlevel == 3) && ($accttypeid > 3))
        {
                $errormsgtitle = "ERROR!";
                $errormsg = "Your account is not authorized to payout the prize of ".$prizevalue."."; 
        }
        else
        {
            //update book number        
            $tmparam["BookNumber"] = $book;
            if ($btnClaimReimburse->SubmittedValue == "CLAIM AND REIMBURSE")
            {
                    $bftv_claim_status = '4';
                    $claim_status = '5';
                    $success_reimburse = 'true';
            } 
            else 
            {
                    $bftv_claim_status = '3';
                    $claim_status = '2';
            }

            $tmparam["Status"] = $claim_status;
            $tmparam["WinningCardID"] = $winningcardid;
            $tmparam["PrizeType"] = $prizetypeid;
            $tmparam["DateClaimed"] = 'now_usec()';
            if ($unameid > 0)
            {                    
                    $tmparam["ClaimedByAID"] = $unameid; 
                    $tmparam["Remarks"] = "Claiming of AcctID ".$_SESSION['acctid']." overridden by AcctID ".$unameid;
            }
            else
            {
                    $tmparam["ClaimedByAID"] = $_SESSION['acctid'];
            } 
            $tmticketvalidation->UpdateByArray($tmparam);
            if ($tmticketvalidation->HasError)
            {
                    $errormsg = $tmticketvalidation->getError();
                    $errormsgtitle = 'ERROR!';
            }

            $tmbatchfilevalidationtickets->UpdateTicketsToClaimed($gameid, $valno2, $tmparam["ClaimedByAID"], $bftv_claim_status, $claim_status);

            //update deckinfo
            $arrdeckinfo = $tmdeckinfo->GetDeckInfoByGameID($gameid);
            if (count($arrdeckinfo) == 1)
            {
                    $arrdtls = $arrdeckinfo[0];
                    $usedwinningcardcount = $arrdtls["UsedWinningCardCount"];
                    $claimedwinningcardcount = $arrdtls["ClaimedWinningCardCount"];
                    $deckid = $arrdtls["DeckID"];

                    $usedwinningcardcount = $usedwinningcardcount + 1;
                    $claimedwinningcardcount = $claimedwinningcardcount + 1;

                    $tmdeckinfoparam["UsedWinningCardCount"] = $usedwinningcardcount; 
                    $tmdeckinfoparam["ClaimedWinningCardCount"] = $claimedwinningcardcount; 
                    $tmdeckinfoparam["DeckID"] = $deckid;

                    $tmdeckinfo->UpdateByArray($tmdeckinfoparam);
                    if ($tmdeckinfo->HasError)
                    {
                            $errormsgtitle = "ERROR!";
                    }
            }

            $where = " WHERE WinningCardID = " . $winningcardid;
            $arr_chk = $tmbatchvalidationtickets->SelectByWhere($where);
            if (count($arr_chk) == 0)
            {
                    // Insert into BatchSession Table
                    $batchsession["Status"] = 3;
                    $batchsession["DateCreated"] = 'now_usec()';
                    $batchsession["CreatedByAID"] = $_SESSION['acctid'];
                    $tmbatchvalidationsession->Insert($batchsession);
                    if ($tmbatchvalidationsession->HasError)
                    {
                        $errormsgtitle = "ERROR!";
                    }
                    $lastinsertsessionid = $tmbatchvalidationsession->LastInsertID;

                    // Insert into BatchTickets Table
                    $batchticket["GameID"] = $gameid;
                    $batchticket["TicketNumber"] = trim(str_replace(array('\r', '\n', '\r\n'), "", $txtBookNumber->SubmittedValue));
                    $batchticket["ValidationNumber"] = trim(str_replace(array('\r', '\n', '\r\n'), "", $txtValidationNumber->SubmittedValue));
                    $batchticket["WinningCardID"] = $winningcardid;
                    $batchticket["BatchValidationSessionID"] = $lastinsertsessionid;
                    $batchticket["DateCreated"] = 'now_usec()';
                    if ($unameid > 0) { $batchticket["AssignedToAID"] = $unameid; }
                    else { $batchticket["AssignedToAID"] = $_SESSION['acctid']; }
                    $batchticket["Status"] = 2;
                    $tmbatchvalidationtickets->Insert($batchticket);
                    if ($tmbatchvalidationtickets->HasError)
                    {
                            $errormsgtitle = "ERROR!";
                    }
            }
            else
            {
                    // Overwrite the existing ticket with the current ticket.
                    $newTicket = trim(str_replace(array('\r', '\n', '\r\n'), "", $txtBookNumber->SubmittedValue));
                    $existingTicket = $arr_chk[0]["TicketNumber"];
                    if ($existingTicket == $newTicket){
                        $tmbatchvalidationtickets->UpdateExistingTicket($newTicket, $winningcardid);
                    }
            }

            if ($tmbatchvalidationtickets->HasError){
                $errormsgtitle = "ERROR!";
                $errormsg = $tmbatchvalidationtickets->getErrors();
            }
            else 
            {
                //update ticket number
                $tmtickets->UpdateTicketStatus($bookid, $ticketno);

                //insert in auditlog table                
                $tmaudit["SessionID"] = $_SESSION['sid'];
                $tmaudit["AID"] = $_SESSION['acctid'];
                $tmaudit["TransDetails"] = 'Validation No.: '.$valno;
                $tmaudit["TransDateTime"] = 'now_usec()';
                $tmaudit["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
                $tmaudit["AuditTrailFunctionID"] = '12';    
                $tmauditlog->Insert($tmaudit);
                if ($tmauditlog->HasError)
                {
                        $errormsg = $tmauditlog->getError();
                        $errormsgtitle = "ERROR!";
                }
                else
                {
                    $successful = 'true';
                    if ($payoutlevel < 3)
                    {
                            if ($success_reimburse == 'true')
                            {
                                    $errormsg = "You have successfully claimed and reimbursed the consolation prize for ". $ddlUsername->SelectedText ." account. Thank you.";
                            } else {
                                    $errormsg = "You have successfully claimed the consolation prize. Thank you.";
                            }
                    }
                    else
                    {
                            $errormsg = "You have successfully claimed the major prize. Thank you.";
                    }
                    $errormsgtitle = "SUCCESSFUL!";
                }
            }
        }
        unset($_SESSION['unameid']); 
        unset($_SESSION['gameid']); 
        unset($_SESSION['payoutlevel']); 
        unset($_SESSION['prizevalue']); 
        unset($_SESSION['winningcardid']); 
        unset($_SESSION['prizetypeid']);
    }
}

if ($continueSearch)
{
    $arrht = $tmhottickets->SelectHotTickets($txtDateFr->Text, $txtDateTo->Text);
    $reccount = count($arrht);
    $pgcon->Initialize($itemsperpage, $reccount);
    $pgTransactionHistory = $pgcon->PreRender();
    $arrhtwithlimit = $tmhottickets->SelectHotTicketsWithLimit($txtDateFr->Text, $txtDateTo->Text, $pgcon->SelectedItemFrom - 1, $itemsperpage);
    $list_ht = new ArrayList();
    $list_ht->AddArray($arrhtwithlimit);
    $hidchangedate->Text = 0;
}

function CheckDigit($number) 
{ 
	// Strip any non-digits 
	$number=preg_replace('/\D/', '', $number);
	
	// Set the string length 
	$number_length=strlen($number);
	

	for ($i=1; $i<=$number_length; $i++) 
	{
		$digit = $number[$i-1];
		if($i % 2)
		{
			//step 1
			//odd numbers
			$total_step1 += $digit;
		}
		else
		{
			//step 3
			//even numbers
			$total_step3 += $digit;
		}
	}
	//step2
	$result = $total_step1 * 3;
	//step4
	$result = $result + $total_step3;
	//step5
	$result = 10 - $result % 10;
	return $result;      
}

/**
 * @author Noel Antonio 04-04-2013
 * This function will insert record to hot tickets table once the user validate a ticket
 * which is either stolen or expired and not payable.
 * @param $ticketid the ticket id to be validated
 * @param $acctid the logged-in user
 * @param $virn the validation number
 * @param $ticketno the ticket number
 * @return boolean
 */
function saveToHotTickets($ticketid, $acctid, $virn, $ticketno)
{
    $tmhottickets = new TMHotTickets();
    $tmticketcancellation = new TMTicketCancellation();
    
    $arrtc = $tmticketcancellation->SelectCancelTypeByTicketID($ticketid);
    if (count($arrtc) == 1)
    {
        $canceltype = $arrtc[0]["CancelType"];

        $insertht["AID"] = $acctid;
        $insertht["VIRN"] = $virn;
        $insertht["TicketNumber"] = $ticketno;
        $insertht["DateCreated"] = "now_usec()";
        $insertht["CreatedByAID"] = $acctid;
        $insertht["CancellationMode"] = $canceltype;
        $tmhottickets->Insert($insertht);
        if ($tmhottickets->HasError){
            return false;
        } else {
            return true;
        }
    }
}
?>