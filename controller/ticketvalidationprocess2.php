<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
$pagesubmenuid = 7;
//$javascripts[] = "../jscripts/validations.js";
//$javascripts[] = "../jscripts/jquery-1.5.2.min.js";
$stylesheets[] = "css/default.css";
$pagetitle = "TMS Admin Menus";


App::LoadModuleClass("TicketManagementCM", "TMTicketValidation");
App::LoadModuleClass("TicketManagementCM", "TMDecks");
App::LoadModuleClass("TicketManagementCM", "TMDeckInfo");
App::LoadModuleClass("TicketManagementCM", "TMAccounts");
App::LoadModuleClass("TicketManagementCM", "TMAuditLog");
App::LoadModuleClass("TicketManagementCM", "TMPrizes");
App::LoadModuleClass("TicketManagementCM", "TMProducts");
App::LoadModuleClass("TicketManagementCM", "TMGameManagement");
App::LoadModuleClass("TicketManagementCM", "TMTickets");
App::LoadModuleClass("TicketManagementCM", "TMBatchValidationSession");
App::LoadModuleClass("TicketManagementCM", "TMBatchValidationTickets");
App::LoadModuleClass("TicketManagementCM", "TMBatchTransactionLog");

App::LoadControl("TextBox");
App::LoadControl("Hidden");
App::LoadControl("ComboBox");
App::LoadControl("Button");

$confirmation = 'false';
$prizetypeid = '0';
$accttypeid = $_SESSION['accttype'];
$aid = $_SESSION['acctid'];

$pos = 0;

$fproc = new FormsProcessor();

$tmbatchvalidationsession = new TMBatchValidationSession();
$tmbatchvalidationtickets = new TMBatchValidationTickets();
$tmbatchtransactionlog = new TMBatchTransactionLog();
$tmticketvalidation = new TMTicketValidation();
$tmdecks = new TMDecks();
$tmdeckinfo = new TMDeckInfo();
$tmaccount = new TMAccounts();
$tmauditlog = new TMAuditLog();
$tmprizes = new TMPrizes();
$tmproducts = new TMProducts();
$tmgamemgt = new TMGameManagement();
$tmtickets = new TMTickets();

$ddlUsername = new ComboBox("ddlUsername", "ddlUsername", "Account Name: ");
$ddlUsername->TabIndex = 4;
$ddlProduct = new ComboBox("ddlProduct", "ddlProduct", "ProductName: ");
$ddlProduct->Args="onchange='javascript: return loadgames();'";
$ddlGameType = new ComboBox("ddlGameType", "ddlGameType", "Game: ");
$txtValidationNumber = new TextBox("txtValidationNumber", "txtValidationNumber", "Validation Number: ");
$txtValidationNumber->TabIndex = 1;
$txtValidationNumber->Args="onkeypress='javascript: return checkKeycode(event);'";
$txtValidationNumber->Length = 12;
$txtBookNumber = new TextBox("txtBookNumber", "txtBookNumber", "Book Number: ");
$txtBookNumber->TabIndex = 2;
$txtBookNumber->Args="onkeypress='javascript: return checkKeycode(event);'";
$txtBookNumber->Length = 12;
$hidProdID = new Hidden("hidProdID","hidProdID","");
$btnValidate = new Button("btnValidate", "btnValidate", "Validate");
$btnValidate->TabIndex = 3;
$btnValidate->Args="onclick='javascript: return checkvalidation();'";
$btnValidate->IsSubmit = true;

$btnOk = new Button("btnOk", "btnOk", "CLAIM");
$btnOk->IsSubmit = true;

$arraccounts = $tmaccount->SelectForValidationModule($aid);
$accountlist = new ArrayList();
$accountlist->AddArray($arraccounts);
$ddlUsername->ClearItems();
$litem = null;
$litem[] = new ListItem("Select Account Name", "0", true);
$ddlUsername->Items = $litem;
$ddlUsername->DataSource = $accountlist;
$ddlUsername->DataSourceText = "UserName";
$ddlUsername->DataSourceValue = "AID";
$ddlUsername->DataBind();

$fproc->AddControl($ddlUsername);
$fproc->AddControl($hidProdID);
$fproc->AddControl($txtValidationNumber);
$fproc->AddControl($txtBookNumber);
$fproc->AddControl($btnValidate);
$fproc->AddControl($btnOk);

$fproc->ProcessForms();

if ($accttypeid == 5 || $accttypeid == 6)
{
    $ddlUsername->Enabled = false;
}

if ($fproc->IsPostBack)
{
    if ($ddlUsername->Enabled == 'true')
    {
        $unameid = $ddlUsername->SubmittedValue;        
    }
    else
    {
        $unameid = '0';
    }

    $valno = trim($txtValidationNumber->Text);
    $fullvirn = trim($txtValidationNumber->Text);
    $checksum = substr($fullvirn, strlen($fullvirn) - 1, strlen($fullvirn));
    $book = trim($txtBookNumber->Text);
    $gameno = substr($valno,0,strlen($valno) - 9);
    $valno = substr($valno, 0,strlen($valno) - 1);
    $valno2 = substr($valno, strlen($valno) - 8, strlen($valno));
    $ticketno = substr($book,strlen($book) - 3,3);
    $book = substr($book, strlen($book) - 9 ,6);
    
    //check if game number is valid
    $arrGame = $tmgamemgt->SelectByGameNumber($gameno);
    if (count($arrGame) == 1)
    {
        $arrdtls = $arrGame[0];
        $gameid = $arrdtls["GameID"];
    }
    else
    {
        $errormsg = 'Game number does not exist.';
        $errormsgtitle = 'ERROR!';
    }
    
    $checkdigit = CheckDigit($valno);
    
    if($checkdigit % 10 != $checksum)
    {
        $errormsg = 'Please enter a valid VIRN.';
        $errormsgtitle = 'ERROR!';
    } 
    else
    {
        //check if valid VIRN
        $arrWinning = $tmticketvalidation->IsValidVIRN($valno2,$gameid);
        if (count($arrWinning) == 1)
        {
            //check if valid book number
            $arrBookDtls = $tmdecks->IsBookNoValid($book,$gameid);
            if (count($arrBookDtls) == 1)
            {
                $arrdtlsbooks = $arrBookDtls[0];
                $statusbooks = $arrdtlsbooks["Status"];
                $bookid = $arrdtlsbooks["BookID"];
                if ($statusbooks == 1)
                {     
                    //check if ticketno is valid
                    $arrTicketDtls = $tmtickets->IsTicketNoValid($ticketno, $bookid);
                    if (count($arrTicketDtls) == 1)
                    {
                        $arrdtls2 = $arrTicketDtls[0];
                        $ticketstatus = $arrdtls2["Status"];
                        if ($ticketstatus == 1 || $ticketstatus == 6)
                        {
                            $arrdtls = $arrWinning[0];
                            $winningcardid = $arrdtls["WinningCardID"];
                            $cardno = $arrdtls["CardNumber"];
                            $prizetypeid = $arrdtls["PrizeTypeID"];
                            $prizeid = $arrdtls["PrizeID"];
                            $prizename = $arrdtls["PrizeName"];
                            $claimdate = $arrdtls["ClaimDate"];
                            $claimtime = $arrdtls["ClaimTime"];
                            $status = $arrdtls["Status"];
                        }
                        elseif ($ticketstatus == 5)
                        {
                            $errormsg = "Scratch card has been deemed cancelled. Performing the ticket validation will not be allowed.";
                            $errormsgtitle = 'ERROR!';    
                        }
                    }
                    else
                    {
                        $errormsg = "Scratch card does not exist";//"Invalid book number.";
                        $errormsgtitle = 'ERROR!';    
                    }
                    
                }
                else
                {
                    if ($statusbooks == 4)
                    {
                        $errormsg = "Please enter a book ticket number that has already been activated."; //"Book number is assigned.";                    
                    }
                    elseif ($statusbooks == 2)
                    {
                        $errormsg = "Please enter a book ticket number that has already been activated."; //"Book number is still on freight.";                    
                    }
                    elseif ($statusbooks == 3)
                    {
                        $errormsg = "Please enter a book ticket number that has already been activated."; //"Book number is on stock.";                    
                    }
                    elseif ($statusbooks == 5)
                    {
                        $errormsg = "Scratch card has been deemed cancelled. Performing the ticket validation will not be allowed.";                    
                    }
                    $errormsgtitle = 'ERROR!';  
                }

            }
            else
            {            
                $errormsg = "Book number does not exist.";
                $errormsgtitle = 'ERROR!';                
            }
        }
        else
        {
            $errormsg = "Sorry, your ticket is not a winning card.";
            $errormsgtitle = 'ERROR!';
        }  
        
        //get payout level and check if user is allowed to payout the prize
        $arrprize = $tmprizes->SelectByPrizeStatus($prizename);                
        if (count($arrprize) == 1)
        {
            $arrdtls = $arrprize[0];
            $payoutlevel = $arrdtls["PayoutLevel"];
        }

        //start
        if ($btnValidate->SubmittedValue == "Validate")
        { 
            
            $arraccounts = $tmaccount->SelectByAccountID($unameid);
            if (count($arraccounts) == 1)
            {
                $arrdtls = $arraccounts[0];
                $uname = $arrdtls["UserName"];                   
            }
            
            //check ticket status 
            if ($status == 0 || $status == 1)// new and verified
            {                 
                if ($prizetypeid == 1)//major prize
                {         
                    if ($accttypeid > 3)
                    {
                        $confirmation = 'false';
                        $errormsgtitle = "SUCCESSFUL!";
                        $errormsg = "Congratulations, your scratch card has won the major prize. Please go to the Head Office to claim your prize.";
                    }
                    else
                    {
                        $confirmation = 'true';
                        $errormsgtitle = "SUCCESSFUL!";
                        if ($unameid > 0)
                        {             
                            $errormsg = "Your ticket has won a major prize of ".$prizename.". Claim prize now using the ".$uname." account?";                                                          
                        }
                        else
                        {
                            $errormsg = "Congratulations, your scratch card has won a major prize of ".$prizename.". Claim winning prize?";      
                        }  
                    }
                }
                elseif ($prizetypeid == 2)//minor prize
                {   
                    $confirmation = 'true';
                    if ($unameid > 0)
                    {             
                        $errormsg = "Your ticket has won a consolation prize of ".$prizename.". Claim prize now using the ".$uname." account?";                                                          
                    }
                    else
                    {
                        $errormsg = "Your ticket has won a consolation prize of ".$prizename.". Claim prize now using the ".$_SESSION['uname']." account?"; //". Claim winning prize?";      
                    }   
                    
                    if (($payoutlevel > 2)  && (($accttypeid == 5) || ($accttypeid == 6)))
                    {
                        $confirmation = 'false';
                        $errormsg = "Congratulations, your scratch card has won a consolation prize of ".$prizename.". Your account is not authorized to payout this amount.";      
                    } 
                }
            }
            elseif ($status == 2) // claimed
            {
                $errormsgtitle = "";
                $errormsg = "Scratch card prize has already been claimed last ".$claimdate." at ".$claimtime.".";
            }

            //update winning card status to 'verified' 
            if ($status != '2')
            {
                $tmparam["Status"] = '1';
            }

            $tmparam["WinningCardID"] = $winningcardid;
            $tmupdatewinningcard = $tmticketvalidation->UpdateByArray($tmparam);
            if ($tmticketvalidation->HasError)
            {
                $errormsg = $tmticketvalidation->getError();
                $errormsgtitle = 'ERROR!';
            }
            $_SESSION['unameid'] = $unameid;
            $_SESSION['acctid'] = $aid;
        }
        
        if ($btnOk->SubmittedValue == "CLAIM")
        {
            $unameid = $_SESSION['unameid'];
            $aid = $_SESSION['acctid'];

            if(($payoutlevel == 3) && ($accttypeid > 3))
            {
                $errormsgtitle = "ERROR!";
                $errormsg = "Your account is not authorized to payout the prize of ".$prizename."."; 
            }
            else
            {
                //update book number        
                $tmparam["BookNumber"] = $book;
                $tmparam["Status"] = '2';
                $tmparam["WinningCardID"] = $winningcardid;
                $tmparam["DateClaimed"] = 'now_usec()';
                if ($unameid > 0)
                {                    
                    $tmparam["ClaimedByAID"] = $unameid; 
                    $tmparam["Remarks"] = "Claiming of AcctID ".$_SESSION['acctid']." overridden by AcctID ".$unameid;
                }
                else
                {
                    $tmparam["ClaimedByAID"] = $aid;
                } 
                $tmupdatewinningcard = $tmticketvalidation->UpdateByArray($tmparam);
                if ($tmupdatewinningcard->HasError)
                {
                    $errormsg = $tmupdatewinningcard->getError();
                    $errormsgtitle = 'ERROR!';
                }

                //update deckinfo
                $arrdeckinfo = $tmdeckinfo->GetDeckInfoByGameID($gameid);
                if (count($arrdeckinfo) == 1)
                {
                    $arrdtls = $arrdeckinfo[0];
                    $usedwinningcardcount = $arrdtls["UsedWinningCardCount"];
                    $claimedwinningcardcount = $arrdtls["ClaimedWinningCardCount"];
                    $deckid = $arrdtls["DeckID"];

                    $usedwinningcardcount = $usedwinningcardcount + 1;
                    $claimedwinningcardcount = $claimedwinningcardcount + 1;

                    $tmdeckinfoparam["UsedWinningCardCount"] = $usedwinningcardcount; 
                    $tmdeckinfoparam["ClaimedWinningCardCount"] = $claimedwinningcardcount; 
                    $tmdeckinfoparam["DeckID"] = $deckid;

                    $tmupdatedeckinfo = $tmdeckinfo->UpdateByArray($tmdeckinfoparam);
                    if ($tmdeckinfo->HasError)
                    {
                        $errormsgtitle = "ERROR!";
                    }
                }
                
                // Insert into BatchSession Table
                $batchsession["Status"] = 3;
                $batchsession["DateCreated"] = 'now_usec()';
                $batchsession["CreatedByAID"] = $aid;
                $insertsession = $tmbatchvalidationsession->Insert($batchsession);
                if ($tmbatchvalidationsession->HasError)
                {
                    $errormsgtitle = "ERROR!";
                }
                $lastinsertsessionid = $tmbatchvalidationsession->LastInsertID;
                
                // Insert into BatchTickets Table
                $batchticket["GameID"] = $gameid;
                $batchticket["TicketNumber"] = $txtBookNumber->SubmittedValue;
                $batchticket["ValidationNumber"] = $txtValidationNumber->SubmittedValue;
                $batchticket["WinningCardID"] = $winningcardid;
                $batchticket["BatchValidationSessionID"] = $lastinsertsessionid;
                $batchticket["DateCreated"] = 'now_usec()';
                if ($unameid > 0) { $batchticket["AssignedToAID"] = $unameid; }
                else { $batchticket["AssignedToAID"] = $aid; }
                $batchticket["Status"] = 1;
                $insertticket = $tmbatchvalidationtickets->Insert($batchticket);
                if ($tmbatchvalidationtickets->HasError)
                {
                    $errormsgtitle = "ERROR!";
                }
                
                //update ticket number
                $updTicket = $tmtickets->UpdateTicketStatus($bookid, $ticketno);
                
                //insert in auditlog table                
                $tmaudit["SessionID"] = $_SESSION['sid'];
                $tmaudit["AID"] = $_SESSION['acctid'];
                $tmaudit["TransDetails"] = 'Validation No.: '.$valno;
                $tmaudit["TransDateTime"] = 'now_usec()';
                $tmaudit["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
                $tmaudit["AuditTrailFunctionID"] = '12';    
                $tmauditlog->Insert($tmaudit);
                if ($tmauditlog->HasError)
                {
                    $errormsg = $tmauditlog->getError();
                    $errormsgtitle = "ERROR!";
                }
                else
                {
                    $successful = 'true';
                    if ($payoutlevel < 3)
                    {
                        $errormsg = "You have successfully claimed the consolation prize. Thank you.";
                    }
                    else
                    {
                        $errormsg = "You have successfully claimed the major prize. Thank you.";
                    }
                    $errormsgtitle = "SUCCESSFUL!";
                }
            }
        }
        //end 
    }   
}


function CheckDigit($number) 
{ 
	// Strip any non-digits 
	$number=preg_replace('/\D/', '', $number);
	
	// Set the string length 
	$number_length=strlen($number);
	

	for ($i=1; $i<=$number_length; $i++) 
	{
		$digit = $number[$i-1];
		if($i % 2)
		{
			//step 1
			//odd numbers
			$total_step1 += $digit;
		}
		else
		{
			//step 3
			//even numbers
			$total_step3 += $digit;
		}
	}
	//step2
	$result = $total_step1 * 3;
	//step4
	$result = $result + $total_step3;
	//step5
	$result = 10 - $result % 10;
	
	return $result;      
}
?>
