<?php
/*
 * Created by Arlene R. Salazar on 09-15-2011
 * Purpose: controller for game update
 */
$pagesubmenuid = 18;
$stylesheets[] = "css/default.css";
$javascripts[] = "jscripts/validations.js";
$javascripts[] = "jscripts/jquery-1.5.2.min.js";

App::LoadModuleClass("TicketManagementCM", "TMGameManagement");
App::LoadModuleClass("TicketManagementCM", "TMProvider");
App::LoadModuleClass("TicketManagementCM", "TMProducts");
App::LoadModuleClass("TicketManagementCM", "TMAuditLog");

App::LoadControl("Hidden");
App::LoadControl("TextBox");
App::LoadControl("Button");
App::LoadControl("ComboBox");

$frmUpdateGame = new FormsProcessor();

$tmgames = new TMGameManagement();
$tmproviders = new TMProvider();
$tmproducts = new TMProducts();
$tmauditlog = new TMAuditLog();

$hiddenselectedid = new Hidden("hiddenselectedid","hiddenselectedid","Hidden selected id");

$txtGameNumber = new TextBox("txtGameNumber","txtGameNumber","Game Number");
$txtGameNumber->Length = 3;
$txtGameNumber->Args = "size='13' onkeypress='javascript: return isNumberKey(event);'";

$txtProductName = new TextBox("txtProductName", "txtProductName", "");
$txtProductName->Args = "style='display:none'";

$txtCardValue = new TextBox("txtCardValue","txtCardValue","Card Value");
$txtCardValue->Length = 14;
//$txtCardValue->ReadOnly = true;
$txtCardValue->Args = "size='13' onkeypress='javascript: return verifycardvalue(event);'";

$txtBookSize = new TextBox("txtBookSize","txtBookSize","Card Value");
$txtBookSize->Length = 14;
//$txtCardValue->ReadOnly = true;
$txtBookSize->Args = "size='13' onkeypress='javascript: return isNumberKey(event);'";
$txtBookSize->Enabled = false;

$txtNewGameName = new TextBox("txtNewGameName","txtNewGameName","New Game Name");
$txtNewGameName->Length = 30;
//$txtNewGameName->Args = "size='23'";
$txtNewGameName->Args = "disabled = 'true' size='23' onblur='get_products_cardvalue2();'";

$ddlProductName = new ComboBox("ddlProductName","ddlProductName","Product Name");
$options = null;
$options[] = new ListItem("- - - - - - - - - - - - - -" , "" , true);
$ddlProductName->Items = $options;
//$ddlProductName->Args = "onchange='javascript: return ClearTextbox();'";
$ddlProductName->Args = "onchange='get_products_cardvalue();'";

$btnCancel = new Button("btnCancel","btnCancel","Cancel");
$btnCancel->Args = "onclick='javascript: return redirectToGameList();'";

$btnSave = new Button("btnSave","btnSave","Save");
$btnSave->IsSubmit = true;
$btnSave->Args = "onclick='javascript: return checkUpdateGame();'";

$btnConfirm = new Button("btnConfirm","btnConfirm","Okay");
$btnConfirm->IsSubmit = true;

$btnOkay = new Button("btnOkay","btnOkay","Okay");
$btnOkay->Args = "onclick='javascript: return redirectToGameList();'";

$hiddenproviderid = new Hidden("hiddenproviderid","hiddenproviderid","Hidden Provider ID");
$hiddenproductid = new Hidden("hiddenproductid","hiddenproductid","Hidden Product ID");
$hiddenCardValue = new Hidden("hiddenCardValue","hiddenCardValue","Hidden Card Value");

$frmUpdateGame->AddControl($hiddenselectedid);
$frmUpdateGame->AddControl($txtGameNumber);
$frmUpdateGame->AddControl($txtCardValue);
$frmUpdateGame->AddControl($txtBookSize);
$frmUpdateGame->AddControl($txtNewGameName);
$frmUpdateGame->AddControl($btnCancel);
$frmUpdateGame->AddControl($btnSave);
$frmUpdateGame->AddControl($btnConfirm);
$frmUpdateGame->AddControl($btnOkay);
$frmUpdateGame->AddControl($hiddenproviderid);
$frmUpdateGame->AddControl($hiddenproductid);
$frmUpdateGame->AddControl($hiddenCardValue);
$frmUpdateGame->AddControl($ddlProductName);
$frmUpdateGame->AddControl($txtProductName);

$frmUpdateGame->ProcessForms();

if($frmUpdateGame->IsPostBack)
{
    if ($frmUpdateGame->GetPostVar("hiddengameid"))
    {
        $id = $frmUpdateGame->GetPostVar("hiddengameid");
        $hiddenselectedid->Text = $id;   
    }

    $gamesinfo = $tmgames->SelectByID($hiddenselectedid->Text);
    $provider = $tmproviders->SelectByID($gamesinfo[0]["ProviderID"]);
    $product = $tmproducts->SelectByID($gamesinfo[0]["ProductID"]);
    $providername = $provider[0]["Name"];
    $txtGameNumber->Text = $gamesinfo[0]["GameNumber"];
    $txtNewGameName->Text = $product[0]["ProductName"];
    $txtCardValue->Text = $gamesinfo[0]["CardPrice"];
    $hiddenproductid->Text = $product[0]["ProductID"];
    $hiddenproviderid->Text = $gamesinfo[0]["ProviderID"];
    $txtBookSize->Text = $gamesinfo[0]["BookTicketCount"];
    $productname = $tmproducts->GetAllProductsPerProvider($gamesinfo[0]["ProviderID"]);
    $prodname_list = new ArrayList();
    $prodname_list->AddArray($productname);
    $ddlProductName->DataSource = $prodname_list;
    $ddlProductName->DataSourceText = "ProductName";
    $ddlProductName->DataSourceValue = "ProductID";
    $ddlProductName->DataBind();
    $ddlProductName->SetSelectedValue($gamesinfo[0]["ProductID"]);

    if($btnSave->SubmittedValue == "Save")
    {   
        $txtBookSize->Text = $txtBookSize->Text;
        $_SESSION['NewGameName'] = NULL;
        $txtGameNumber->Text = $txtGameNumber->SubmittedValue;
        $isexists = $tmgames->DoesGameNumberExistsUpdate(trim($txtGameNumber->SubmittedValue),$hiddenselectedid->SubmittedValue,$hiddenproviderid->SubmittedValue);
        if(count($isexists) > 0)
        {
            $errormsg = "Game Number already exists. Please enter a unique Game Number.";
            $errortitle = "ERROR!";
        } else {           
            $ddlProductName->SetSelectedValue($ddlProductName->SubmittedValue);
            if ($ddlProductName->SubmittedValue != ''){ 
                $hiddenproductid->Text = $ddlProductName->SubmittedValue; 
                $txtNewGameName->Text = $ddlProductName->SelectedText;
                $txtCardValue->Text = $txtCardValue->SubmittedValue;
                
                $confirm = 'ok';
            } else {
                $txtNewGameName->Text = trim($txtNewGameName->SubmittedValue);
                $chkProdNameExist = $tmproducts->checkProductNameExist($txtNewGameName->Text);
                if (count($chkProdNameExist) > 0)
                {
                    $errormsg = "Product Name already exists. Please enter a unique Product Name.";
                    $errortitle = "ERROR!";
                } else {
                    $confirm = 'ok';
                    if(strlen($ddlProductName->SubmittedValue)>0){
                    $_SESSION['NewGameName'] = NULL;
                    }else{
                    $_SESSION['NewGameName'] = ($txtNewGameName->SubmittedValue); 
                    $txtCardValue->Text = $txtCardValue->SubmittedValue;
                    }
                    
                }
            }
            
        }
        
    }
    
    if($btnConfirm->SubmittedValue == "Okay")
    {   
        $tmpnewgame = $_SESSION['NewGameName'];
        if($tmpnewgame != NULL)
        {
            //insert product / game name
            $newproduct["ProductName"] = trim($_SESSION['NewGameName']);
            $newproduct["ProductDescription"] = trim($_SESSION['NewGameName']);
            $newproduct["ProviderID"] = $_POST['hiddenproviderid'];
            $insertproduct = $tmproducts->Insert($newproduct);
            
            if($tmproducts->HasError)
            {
                $message = "Error occured: " . $tmproducts->getError();
            }

        }
//        $ddlProductName->SetSelectedValue($ddlProductName->SubmittedValue);
//        if ($ddlProductName->SubmittedValue != ''){ 
//            $hiddenproductid->Text = $ddlProductName->SubmittedValue; 
//            $txtNewGameName->Text = $ddlProductName->SelectedText;
//            $txtCardValue->Text = $txtCardValue->SubmittedValue;
//        } else {
//            $txtNewGameName->Text = trim($txtNewGameName->SubmittedValue);
//        }
//        // Update Product
//        $prod["ProductID"] = $hiddenproductid->SubmittedValue;
//        $prod["ProductName"] = mysql_escape_string($txtNewGameName->SubmittedValue);
//        $prod["ProductDescription"] = mysql_escape_string($txtNewGameName->SubmittedValue);
//        $updateproduct = $tmproducts->UpdateByArray($prod);
        

        // Update Game
	$txtGameNumber->Text = $txtGameNumber->SubmittedValue;        
        $game["GameNumber"] = $txtGameNumber->SubmittedValue;
        $game["ProductID"] = $hiddenproductid->SubmittedValue;
        $game["GameID"] = $hiddenselectedid->SubmittedValue;
        $game["CardPrice"] = $txtCardValue->SubmittedValue;
        $game["BookTicketCount"] = $txtBookSize->SubmittedValue;
        if($tmpnewgame != NULL){
        $game["ProductID"]=NULL;
        $prodout = $tmproducts->GetProdIDByName($tmpnewgame,$_POST['hiddenproviderid']);            
        $game["ProductID"] = $prodout[0][ProductID];
        }

        
        $updategame = $tmgames->UpdateByArray($game);
        
        
        if($tmgames->HasError)
        {
            $errormsg = "Error occurred: " . $tmgames->getError();
            $errortitle = "ERROR!";
        }
        else
        {   
            if($tmpnewgame == NULL){
            $updated = $tmgames->UpdateAllCardPrice($hiddenproductid->SubmittedValue,$txtCardValue->SubmittedValue);
            }
	    //insert in auditlog table
            $tmaudit["SessionID"] = $_SESSION['sid'];
            $tmaudit["AID"] = $_SESSION['acctid'];
            $tmaudit["TransDetails"] = 'Game ID: '.$hiddenselectedid->SubmittedValue;
            $tmaudit["TransDateTime"] = 'now_usec()';
            $tmaudit["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
            $tmaudit["AuditTrailFunctionID"] = '25';    
            $tmauditlog->Insert($tmaudit);
            if ($tmauditlog->HasError)
            {
                $errormsg = "Error occurred: " . $tmgames->getError();
                $errortitle = "ERROR!";
            }
            else
            {
                $successmsg = "The game information has been successfully modified.";
                $successtitle = "SUCCESS";
            } 
        }
    }
}
else
{
    header("Location: gamelist.php");
}
?>