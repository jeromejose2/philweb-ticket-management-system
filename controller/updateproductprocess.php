<?php
/*
 * @author 
 * Purpose   : controller for updateproduct
 */
$pagesubmenuid = 23;
$javascripts[] = "../jscripts/validations.js";
$stylesheets[] = "../css/default.css";
$pagetitle = "";

App::LoadModuleClass("TicketManagementCM", "TMProducts");
App::LoadModuleClass("TicketManagementCM", "TMAuditLog");
App::LoadModuleClass("TicketManagementCM", "TMProvider");

App::LoadControl("TextBox");
App::LoadControl("Button");
App::LoadControl("ComboBox");
App::LoadControl("Hidden");

$fproc = new FormsProcessor();

$tmproduct = new TMProducts(); 
$tmauditlog = new TMAuditLog();
$tmprovider = new TMProvider();

$hiddenselectedid = new Hidden("hiddenselectedid","hiddenselectedid","Hidden Selected ID");  

$ddlProduct = new ComboBox("ddlProduct", "ddlProduct", "Select Product: ");
$ddlProduct->ShowCaption = true;

$arrproviders = $tmprovider->SelectAll();
$providerlist = new ArrayList();
$providerlist->AddArray($arrproviders);

$ddlProviders = new ComboBox("ddlProviders","ddlProviders","Provider: ");
$ddlProviders->ShowCaption = true;
$options = null;
$options[] = new ListItem("Select Provider","");
$ddlProviders->Items = $options;
$ddlProviders->DataSource = $providerlist;
$ddlProviders->DataSourceText = "Name";
$ddlProviders->DataSourceValue = "ProviderID";
$ddlProviders->DataBind();

$txtName = new TextBox("txtName", "txtName", "Product: ");
$txtName->ShowCaption = true;
$txtName->Length = 30;

$txtDescription = new TextBox("txtDescription", "txtDescription", "Description: ");
$txtDescription->ShowCaption = true;
$txtDescription->Length = 50;

$btnUpdate = new Button("btnUpdate", "btnUpdate", "Update");
$btnUpdate->Args="onclick='javascript: return checkupdateproduct();'";
$btnUpdate->IsSubmit = true;

$btnCancel = new Button("btnCancel", "btnCancel", "Cancel");
$btnCancel->Args="onclick='javascript: return redirectToProductList();'";
//$btnCancel->IsSubmit = true;

$btnSearch = new Button("btnSearch", "btnSearch", "Search");
$btnSearch->IsSubmit = true;

$btnChangeStat = new Button("btnChangeStat","btnChangeStat","Save");
$btnChangeStat->IsSubmit = true;
$btnChangeStat->Args = "onclick='javascript: return checkProviderChngeStat()'";

$arrproducts = $tmproduct->SelectAll();
$productlist = new ArrayList();
$productlist->AddArray($arrproducts);
$ddlProduct->ClearItems();
$ddlProduct->DataSource = $productlist;
$ddlProduct->DataSourceText = "ProductName";
$ddlProduct->DataSourceValue = "ProductID";
$ddlProduct->DataBind();

$ddlStatus = new ComboBox("ddlStatus","ddlStatus","New Status: ");
$ddlStatus->ShowCaption = true;
$options = null;
$options[] = new ListItem("","");
$options[] = new ListItem("Active","1");
$options[] = new ListItem("Inactive","2");
$ddlStatus->Items = $options;

$fproc->AddControl($ddlProduct);
$fproc->AddControl($txtName);
$fproc->AddControl($txtDescription);
$fproc->AddControl($btnUpdate);
$fproc->AddControl($btnCancel);
$fproc->AddControl($btnSearch);
$fproc->AddControl($ddlStatus);
$fproc->AddControl($btnChangeStat);
$fproc->AddControl($hiddenselectedid);
$fproc->AddControl($ddlProviders);

$fproc->ProcessForms();

if ($fproc->IsPostBack)
{
    if (($btnUpdate->SubmittedValue != "Update") && ($btnChangeStat->SubmittedValue != "Save"))
    {
        if ($fproc->GetPostVar("hiddenprodid"))
        {
            $id = $fproc->GetPostVar("hiddenprodid");
            $hiddenselectedid->Text = $id;
        }
	$tmprows = $tmproduct->SelectByID($hiddenselectedid->Text);
	$tmprov = $tmprows[0];
	$txtName->Text = $tmprov["ProductName"];
	$txtDescription->Text = $tmprov["ProductDescription"];
	$ddlProviders->SetSelectedValue($tmprov["ProviderID"]);
    }
    $status = $tmprov["Status"] == 1 ? "Active" : "Inactive";
    $statusid = $tmprov["Status"];
    if($btnChangeStat->SubmittedValue == "Save")
    {
        $tmprodchangestat["Status"] = $ddlStatus->SelectedValue;
        $tmprodchangestat["ProductID"] = $hiddenselectedid->SubmittedValue;
        $tmproduct->UpdateByArray($tmprodchangestat);
        if ($tmproduct->HasError)
        {
	    $errormsg = $tmproduct->getError();
        }
        else
        {
            //insert in auditlog table
            $tmaudit["SessionID"] = $_SESSION['sid'];
            $tmaudit["AID"] = $_SESSION['acctid'];
            $tmaudit["TransDetails"] = 'Account ID: '.$hiddenselectedid->SubmittedValue;
            $tmaudit["TransDateTime"] = 'now_usec()';
            $tmaudit["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
            $tmaudit["AuditTrailFunctionID"] = '21';
            $tmauditlog->Insert($tmaudit);
            if ($tmauditlog->HasError)
            {
		$errormsg = $tmauditlog->getError();
            }
            else
            {
		$errormsg = "Product status successfully updated.";
            }
            $status = $ddlStatus->SelectedValue == 1 ? "Active" : "Inactive";
            $statusid = $ddlStatus->SelectedValue;
        }

    }
    if ($btnUpdate->SubmittedValue == "Update")
    {
		$productname = trim($txtName->SubmittedValue);
        $arrProvider = $tmproduct->DoesProductExistsUpdate($productname,$hiddenselectedid->SubmittedValue);
        if (count($arrProvider) > 0)
        {
            $errormsg = "Product name already exists.";
        }
        else
        {
            $tmprod["ProductName"] = trim($txtName->SubmittedValue);
            $tmprod["ProductDescription"] = trim($txtName->SubmittedValue);
            $tmprod["ProductID"] = $hiddenselectedid->SubmittedValue;
	    	$tmprod["ProviderID"] = $ddlProviders->SelectedValue;
            $tmproduct->UpdateByArray($tmprod);
            if ($tmproduct->HasError)
            {
                $errormsg = $tmproduct->getError();
            }
            else
            {
                //insert in auditlog table
                $tmaudit["SessionID"] = $_SESSION['sid'];
                $tmaudit["AID"] = $_SESSION['acctid'];
                $tmaudit["TransDetails"] = 'Account ID: '.$_SESSION['acctid'];
                $tmaudit["TransDateTime"] = 'now_usec()';
                $tmaudit["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
                $tmaudit["AuditTrailFunctionID"] = '20';    
                $tmauditlog->Insert($tmaudit);
                if ($tmauditlog->HasError)
                {
                    $errormsg = $tmauditlog->getError();
                }
                else
                {
                    $successmsg = "Product successfully updated.";
                }              
            }
	}
    } 
    
    if ($btnSearch->SubmittedValue == "Search")
    {
        $tmproductid = $ddlProduct->SubmittedValue;
        $tmprows = $tmproduct->SelectByID($tmproductid);
        $tmprod = $tmprows[0];
        $txtName->Text = $tmprod["ProductName"];
        $txtDescription->Text = $tmprod["ProductDescription"];
    }
}
?>