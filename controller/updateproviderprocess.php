<?php
/*
 * @author 
 * Purpose   : controller for updateprovider
 */
$pagesubmenuid = 22;
$javascripts[] = "jscripts/validations.js";
$stylesheets[] = "css/default.css";
$javascripts[] = "jscripts/jquery-1.5.2.min.js";
$pagetitle = "";

App::LoadModuleClass("TicketManagementCM", "TMProvider");
App::LoadModuleClass("TicketManagementCM", "TMAuditLog");

App::LoadControl("TextBox");
App::LoadControl("Button");
App::LoadControl("Hidden");
App::LoadControl("ComboBox");

$fproc = new FormsProcessor();

$tmprovider = new TMProvider(); 
$tmauditlog = new TMAuditLog();  

$ddlProvider = new ComboBox("ddlProvider", "ddlProvider", "Select Provider: ");
$ddlProvider->ShowCaption = true;

$txtName = new TextBox("txtName", "txtName", "Name: ");
//$txtName->ShowCaption = true;
$txtName->Length = 30;

$txtDescription = new TextBox("txtDescription", "txtDescription", "Description: ");
//$txtDescription->ShowCaption = true;
$txtDescription->Length = 50;

$txtContactNumber = new TextBox("txtContactNumber", "txtContactNumber", "Contact Number: ");
//$txtContactNumber->ShowCaption = true;
$txtContactNumber->Length = 20;
$txtContactNumber->Args = "onkeypress='javascript: return isNumberKey(event)'";

$txtAddress = new TextBox("txtAddress", "txtAddress", "Head Office Address: ");
//$txtAddress->ShowCaption = true;
$txtAddress->Multiline = true;
$txtAddress->Style = "width: 300px;height: 50px;resize: none; vertical-align:top;";
$txtAddress->Args = "maxlength='100'";

$hiddenselectedid = new Hidden("hiddenselectedid","hiddenselectedid","Hidden Selected ID");

$btnUpdate = new Button("btnUpdate", "btnUpdate", "Save");
$btnUpdate->Args="onclick='javascript: return checkupdateprovider();'";
$btnUpdate->IsSubmit = true;

$btnCancel = new Button("btnCancel", "btnCancel", "Cancel");
$btnCancel->Args="onclick='javascript: return redirectToProviderList();'";
//$btnCancel->IsSubmit = true;

$btnSearch = new Button("btnSearch", "btnSearch", "Search");
$btnSearch->IsSubmit = true;

$btnChangeStat = new Button("btnChangeStat","btnChangeStat","Save");
$btnChangeStat->IsSubmit = true;
$btnChangeStat->Args = "onclick='javascript: return checkProviderChngeStat()'";

$ddlStatus = new ComboBox("ddlStatus","ddlStatus","New Status: ");
$ddlStatus->ShowCaption = true;
$options = null;
$options[] = new ListItem("","");
$options[] = new ListItem("Active","1");
$options[] = new ListItem("Inactive","2");
$ddlStatus->Items = $options;

$arrproviders = $tmprovider->SelectAll();
$providerlist = new ArrayList();
$providerlist->AddArray($arrproviders);
$ddlProvider->ClearItems();
$ddlProvider->DataSource = $providerlist;
$ddlProvider->DataSourceText = "Name";
$ddlProvider->DataSourceValue = "ProviderID";
$ddlProvider->DataBind();

$btnConfirm = new Button("btnConfirm","btnConfirm","Okay");
$btnConfirm->IsSubmit = true;

$fproc->AddControl($ddlProvider);
$fproc->AddControl($txtName);
$fproc->AddControl($txtDescription);
$fproc->AddControl($btnUpdate);
$fproc->AddControl($btnSearch);
$fproc->AddControl($btnCancel);
$fproc->AddControl($hiddenselectedid);
$fproc->AddControl($btnChangeStat);
$fproc->AddControl($ddlStatus);
$fproc->AddControl($txtContactNumber);
$fproc->AddControl($txtAddress);
$fproc->AddControl($btnConfirm);

$fproc->ProcessForms();

if ($fproc->IsPostBack)
{
    //if (($btnUpdate->SubmittedValue != "Update") && ($btnChangeStat->SubmittedValue != "Save"))
    if (($btnUpdate->SubmittedValue != "Save") && ($btnChangeStat->SubmittedValue != "Save"))
    {
        if ($fproc->GetPostVar("hiddenprovid"))
        {
            $id = $fproc->GetPostVar("hiddenprovid");
            $hiddenselectedid->Text = $id;
        }
    }
    
    $tmprows = $tmprovider->SelectByID($hiddenselectedid->Text);
    $tmprov = $tmprows[0];
    //$txtName->Text = $tmprov["Name"];
	$txtName = $tmprov["Name"];
	$txtDescription->Text = $tmprov["Description"];
    $txtAddress->Text = $tmprov["Address"];
    $txtContactNumber->Text = $tmprov["ContactNumber"];
	$status = $tmprov["Status"] == 1 ? "Active" : "Inactive";
    $statusid = $tmprov["Status"];
    
    if($btnChangeStat->SubmittedValue == "Save")
    {
        $tmprovchangestat["Status"] = $ddlStatus->SelectedValue;
        $tmprovchangestat["ProviderID"] = $hiddenselectedid->SubmittedValue;
        $tmprovider->UpdateByArray($tmprovchangestat);
        if ($tmprovider->HasError)
        {
            $errormsg = "Error: " . $tmauditlog->getError();
        }
        else
        {
            //insert in auditlog table
            $tmaudit["SessionID"] = $_SESSION['sid'];
            $tmaudit["AID"] = $_SESSION['acctid'];
            $tmaudit["TransDetails"] = 'Account ID: '.$hiddenselectedid->SubmittedValue;
            $tmaudit["TransDateTime"] = 'now_usec()';
            $tmaudit["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
            $tmaudit["AuditTrailFunctionID"] = '10';
            $tmauditlog->Insert($tmaudit);
            if ($tmauditlog->HasError)
            {
                $errormsg = "Error: " . $tmauditlog->getError();
            }
            else
            {
                $errormsg = "Provider status successfully updated.";
            }
	    	$status = $ddlStatus->SelectedValue == 1 ? "Active" : "Inactive";
            $statusid = $ddlStatus->SelectedValue;
        }
    }
    if ($btnUpdate->SubmittedValue == "Save")
    {
		//$providername = trim($txtName->SubmittedValue);
		$providername = mysql_escape_string($txtName);
        $providerdescription = trim($txtDescription->SubmittedValue);
		$provideraddress = trim($txtAddress->SubmittedValue);
        $providercontactnum = trim($txtContactNumber->SubmittedValue);
        $arrProvider = $tmprovider->DoesProviderExistsUpdate($providername,$hiddenselectedid->SubmittedValue);//print_r($arrProvider);exit();

        if (count($arrProvider) > 0)
        {
            $errormsg = "Provider name already exists.";
	    	//$txtName->Text = $txtName->SubmittedValue;
            $txtAddress->Text = $txtAddress->SubmittedValue;
            $txtDescription->Text = $txtAddress->SubmittedValue;
            $txtContactNumber->Text = $txtContactNumber->SubmittedValue;
        }
        else
        {
			$confirm = "ok";
            $txtAddress->Text = $txtAddress->SubmittedValue;
            $txtContactNumber->Text = $txtContactNumber->SubmittedValue;  
        }
    } 
    
    if ($btnSearch->SubmittedValue == "Search")
    {
        $tmproviderid = $ddlProvider->SubmittedValue;
        $tmprows = $tmprovider->SelectByID($tmproviderid);
        $tmprov = $tmprows[0];
        $txtName->Text = $tmprov["Name"];
        $txtDescription->Text = $tmprov["Description"];
    }

	if($btnConfirm->SubmittedValue == "Okay")
    {
        $txtAddress->Text = trim($txtAddress->SubmittedValue);
        $txtContactNumber->Text = trim($txtContactNumber->SubmittedValue);
        $provideraddress = trim($txtAddress->SubmittedValue);
        $providercontactnum = trim($txtContactNumber->SubmittedValue);
        //$tmprovupdate["Name"] = $providername;
        //$tmprovupdate["Description"] = $providername;
        $tmprovupdate["Address"] = $provideraddress;
        $tmprovupdate["ContactNumber"] = $providercontactnum;
        //$tmprov["Description"] = $txtDescription->SubmittedValue;
        //$tmprov["ProviderID"] = $ddlProvider->SubmittedValue;
        $tmprovupdate["ProviderID"] = $hiddenselectedid->SubmittedValue;
        
       //$tmprovider->UpdateByArray($tmprovupdate);
        $tmprovider->UpdateProvider($hiddenselectedid->SubmittedValue, mysql_escape_string(trim($provideraddress)),mysql_escape_string(trim($providercontactnum)));
        if ($tmprovider->HasError)
        {
            $errormsg = $tmprovider->getError();
        }
        else
        {
            //insert in auditlog table
            $tmaudit["SessionID"] = $_SESSION['sid'];
            $tmaudit["AID"] = $_SESSION['acctid'];
            $tmaudit["TransDetails"] = 'Account ID: '.$hiddenselectedid->SubmittedValue;
            $tmaudit["TransDateTime"] = 'now_usec()';
            $tmaudit["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
            $tmaudit["AuditTrailFunctionID"] = '9';
            $tmauditlog->Insert($tmaudit);
            if ($tmauditlog->HasError)
            {
                $errormsg = $tmauditlog->getError();
            }
            else
            {
                
                $successmsg = "Provider information has been successfully modified."; // . App::getParam("EnableMagicQuotes") . "***";
            }
        }
    }
}
else
{
    header("Location: providerlist.php");
}
?>