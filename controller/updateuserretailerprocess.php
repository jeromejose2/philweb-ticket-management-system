<?php
/*
 * Added By : Arlene R. Salazar
 * Added On : Aug 19, 2011
 * Purpose : Process for Updating operator
 */
$pagesubmenuid = 28;
$stylesheets[] = "css/default.css";
$javascripts[] = "jscripts/jquery-1.5.2.min.js";

App::LoadModuleClass("TicketManagementCM", "TMAccountType");
App::LoadModuleClass("TicketManagementCM", "TMAccounts");
//App::LoadModuleClass("TicketManagementCM", "TMProjects");
App::LoadModuleClass("TicketManagementCM", "TMAuditLog");
App::LoadModuleClass("TicketManagementCM", "TMPasswordUpdateRequests");

App::LoadControl("TextBox");
App::LoadControl("ComboBox");
App::LoadControl("Button");
App::LoadControl("Hidden");
App::LoadControl("Radio");
App::LoadControl("RadioGroup");

App::LoadCore("PHPMailer.class.php");
/*Class Declarations*/
$updateform = new FormsProcessor();
$account = new TMAccounts();
$accounttype = new TMAccountType();
$passwordupdaterequest = new TMPasswordUpdateRequests();
//$project = new TMProjects();
/*End of Class Declarations*/

$hiddenpostedid = new Hidden("hiddenpostedid","hiddenpostedid","Hidden Posted Id");

$usernames = $account->SelectAll();
$usernames_list = new ArrayList();
$usernames_list->AddArray($usernames);
$ddlusernames = new ComboBox("ddlusernames","ddlusernames","Usernames:");
$ddlusernames->ShowCaption = true;

//$ddlusernames->Multiple = true;
//$ddlusernames->Size = 10;
$ddlusernames->DataSource = $usernames_list;
$ddlusernames->DataSourceValue = "AID";
$ddlusernames->DataSourceText = "UserName";
$ddlusernames->DataBind();

$txtpassword = new TextBox("txtpassword","txtpassword","Password: ");
$txtpassword->ShowCaption = true;
$txtpassword->Password = true;
$txtpassword->Length = 20;
$txtpassword->Enabled = false;
$txtpassword->Args = "size='33'";

$txtcpassword = new TextBox("txtcpassword","txtcpassword","Confirm Password: ");
$txtcpassword->ShowCaption = true;
$txtcpassword->Password = true;
$txtcpassword->Length = 20;
$txtcpassword->Enabled = false;
$txtcpassword->Args = "size='27'";

$btnView = new Button("btnView","btnView","View Details");
$btnView->IsSubmit = true;

$txtusername = new TextBox("txtusername","txtusername","Username: ");
$txtusername->ShowCaption = true;
$txtusername->Enabled = false;
$txtusername->Args = "size='33'";

$txtfname = new TextBox("txtfname","txtfname","Full Name: ");
$txtfname->ShowCaption = true;
$txtfname->Length = 150;
//$txtfname->Args = "onkeypress='javascript:return isAlphaKey(event)' size='88'";
$txtfname->Args = "size='88'";

$txtmname = new TextBox("txtmname","txtmname","Middle Initial:");
$txtmname->ShowCaption = true;

$txtlname = new TextBox("txtlname","txtlname","Last Name:");
$txtlname->ShowCaption = true;

$txtemail = new TextBox("txtemail","txtemail","Email Address: ");
$txtemail->ShowCaption = true;
//$txtemail->Enabled = false;
$txtemail->Length = 100;
$txtemail->Args = "size='40' onkeypress = 'javascript: return disableSpace(event)'";

$txtcontactnum = new TextBox("txtcontactnum","txtcontactnum","Contact Number: ");
$txtcontactnum->ShowCaption = true;
$txtcontactnum->Args = "onkeypress='javascript:return isNumberKey(event)' size='28'";
$txtcontactnum->Length = 20;

$txtaddress = new TextBox("txtaddress","txtaddress","Address: ");
$txtaddress->ShowCaption = true;
$txtaddress->Length = 150;
$txtaddress->Args = "size='90'";


$hiddenacctid = new Hidden("hiddenacctid","hiddenacctid","Hidden Account Id");
$hiddenacctid->ShowCaption = true;

$rdoStatusAct = new Radio("rdoStatus","rdoStatus","Active: ");
$rdoStatusAct->Value = '1';
$rdoStatusAct->ShowCaption = true;

$rdoStatusInact = new Radio("rdoStatus","rdoStatus","Inactive:");
$rdoStatusInact->Value = '2';
$rdoStatusInact->ShowCaption = true;

$rdoStatusGroup = new RadioGroup("rdoStatus","rdoStatus","Status");
$rdoStatusGroup->RadioControls = array($rdoStatusAct, $rdoStatusInact);
//$rdoStatusGroup->SetSelectedValue($stat);

$where = " WHERE AccountTypeID IN (5,6)";
$accounttypes = $accounttype->SelectByWhere($where);
$accttype_list = new ArrayList();
$accttype_list->AddArray($accounttypes);

$ddlaccttype = new ComboBox("ddlaccttype","ddlaccttype","Account Type:");
$ddlaccttype->ShowCaption = true;
$ddlaccttype->DataSource = $accttype_list;
$ddlaccttype->DataSourceText = "Name";
$ddlaccttype->DataSourceValue = "AccountTypeID";
$ddlaccttype->DataBind();


$ddlstatus = new ComboBox("ddlstatus","ddlstatus","New Status: ");
$ddlstatus->ShowCaption = true;
$options = null;
$options[] = new ListItem("","");
$options[] = new ListItem("Active","1");
$options[] = new ListItem("Locked","4");
$ddlstatus->Items = $options;

/*$projects = $project->SelectAll();
$project_list = new ArrayList();
$project_list->AddArray($projects);
$ddlprjtid = new ComboBox("ddlprjtid","ddlprjtid","Project:");
$ddlprjtid->ShowCaption = true;
$ddlprjtid->DataSource = $project_list;
$ddlprjtid->DataSourceValue = "ID";
$ddlprjtid->DataSourceText = "ProjectName";
$ddlprjtid->DataBind();
*/
$btnUpdate = new Button("btnUpdate","btnUpdate","Save");
$btnUpdate->IsSubmit = true;
$btnUpdate->Args = "onclick='javascript: return checkupdatedata();'";

$btnCancel = new Button("btnCancel","btnCancel","Cancel");
$btnCancel->Args = "onclick='javascript: return redirectToAcctListRetailer();'";

$btnConfirm = new Button("btnConfirm","btnConfirm","Okay");
$btnConfirm->IsSubmit = true;

$btnOkay = new Button("btnOkay","btnOkay","Okay");
$btnOkay->Args = "onclick='javascript: return redirectToAcctListRetailer();'";

$btnResetPword = new Button("btnResetPword","btnResetPword","Reset");
$btnResetPword->Args = "onclick='javascript: return resetPassword();'";

$btnConfirmResetPword = new Button("btnConfirmResetPword","btnConfirmResetPword","Okay");
$btnConfirmResetPword->IsSubmit = true;

$btnChangeStat = new Button("btnChangeStat","btnChangeStat","Change Status");
$btnChangeStat->Args = "onclick='javascript: return changePassword();'";

$btnChange = new Button("btnSave","btnSave","Save");
$btnChange->Args = "onclick='javascript: return checkChangeStatus();'";

$btnConfirmChangeStat = new Button("btnConfirmChangeStat","btnConfirmChangeStat","Okay");
$btnConfirmChangeStat->IsSubmit = true;

$btnCancelChangeStat = new Button("btnCancelChangeStat","btnCancelChangeStat","Cancel");
$btnCancelChangeStat->Args = "onclick = 'javascript: return cancelChangeStatus();'";

$updateform->AddControl($hiddenacctid); 
$updateform->AddControl($hiddenpostedid);
$updateform->AddControl($txtfname);
$updateform->AddControl($txtmname);
$updateform->AddControl($txtlname);
$updateform->AddControl($txtemail);
$updateform->AddControl($txtaddress);
$updateform->AddControl($txtusername);
$updateform->AddControl($txtcontactnum);
$updateform->AddControl($txtpassword);
$updateform->AddControl($txtcpassword);
$updateform->AddControl($ddlaccttype);
//$updateform->AddControl($ddlprjtid);
$updateform->AddControl($ddlusernames);
$updateform->AddControl($ddlstatus);
$updateform->AddControl($btnUpdate);
$updateform->AddControl($btnView);
$updateform->AddControl($btnCancel);
$updateform->AddControl($btnConfirm);
$updateform->AddControl($btnResetPword);
$updateform->AddControl($btnChangeStat);
$updateform->AddControl($btnChange);
$updateform->AddControl($btnConfirmChangeStat);
$updateform->AddControl($btnCancelChangeStat);
$updateform->AddControl($btnConfirmResetPword);
$updateform->AddControl($rdoStatusAct);
$updateform->AddControl($rdoStatusInact);
$updateform->AddControl($rdoStatusGroup);

$updateform->ProcessForms();

if($updateform->IsPostBack)
{
    if ($updateform->GetPostVar("hiddenaccountid"))
    {
        $selectedaccountid = $updateform->GetPostVar("hiddenaccountid");
        $hiddenpostedid->Text = $selectedaccountid;
    }

    $id = $hiddenpostedid->Text;
    $acct = $account->SelectByAccountID($id);
	if(count($acct) > 0)
	    {
		for($i = 0 ; $i < count($acct) ; $i++)
		{
		    $id = $acct[$i]['AID'];
		    $username = $acct[$i]['UserName'];
		    $acctname = $acct[$i]['AccountName'];
		    switch($acct[$i]['Status'])
		    {
		    case 0:
			$status = "Pending";
			break;
		    case 1:
			$status = "Active";
			break;
		    case 2:
			$status = "Suspended";
			break;
			case 3:
			$status = "Locked - Invalid Attempts";
			break;
		    case 4:
			$status = "Locked";
			break;
		    case 5:
			$status = "Terminated";
			break;
		    default :
			$status = "No status";
			break;
		    }
		    $fname = $acct[$i]['Name'];
		    $email = $acct[$i]['Email'];
		    $accttype = $acct[$i]['AccountTypeID'];
		    $pword = $acct[$i]['Password'];
		    $address = $acct[$i]['Address'];
		    $contactnum = $acct[$i]['MobileNumber'];
		    $statusid = $acct[$i]['Status'];

		    //App::Pr("<script>alert('".$accttype."')</script>");
		    $hiddenacctid->Text = $id;
		    //$ddlaccttype->SetSelectedValue($accttype);
		    $txtemail->Text = $email;
		    $txtpassword->Text = $pword;
		    $txtcpassword->Text = $pword;
		    $txtusername->Text = $username;
		    //$txtcontactnum->Text = $contactnum;
		    //$txtaddress->Text = $address;
		    //$txtfname->Text = $fname;
		}
	    }  

    if (($btnUpdate->SubmittedValue != "Save") && ($btnConfirm->SubmittedValue != "Okay") && ($btnConfirmChangeStat->SubmittedValue != "Okay") && ($btnConfirmResetPword->SubmittedValue != "Okay"))
    {
		$txtcontactnum->Text = $contactnum;
		$txtaddress->Text = $address;
		$txtfname->Text = $fname;
		$ddlaccttype->SetSelectedValue($accttype);
		$ddlstatus->SetSelectedValue($statusid);
    }
     
    if($btnUpdate->SubmittedValue == "Save")
    {
		$emailcount = 0;
		if(trim($txtemail->SubmittedValue) == "")
        {
            $emailcount = 0;
        }
        else
        {
			if($email == "")
            {
                $emailchecking = $account->CheckEmail(mysql_escape_string(trim($txtemail->SubmittedValue)));
            }
            else
            {
                $emailchecking = $account->CheckEmailUpdate(mysql_escape_string(trim($txtemail->SubmittedValue)),$hiddenacctid->SubmittedValue);
            }
            $emailcount = $emailchecking[0][0];
        }
		$txtemail->Text = $txtemail->SubmittedValue;
        if($emailcount > 0)
        {
            $updateuser_title = "ERROR!";
            $updateuser_msg = "Email address already taken.";
        }
        $unamechecking = $account->CheckUsernameUpdate(mysql_escape_string(trim($txtusername->Text)),$hiddenacctid->SubmittedValue);
        if($unamechecking[0][0] > 0)
        {
            $updateuser_title = "ERROR!";
            $updateuser_msg = "Username already taken.";
        }
        if(($emailcount == 0) && ($unamechecking[0][0] == 0))
        {
            $okupdateuser_title = "ok";
        }
    }

    if($btnConfirm->SubmittedValue == "Okay")
    {
		$txtemail->Text = $txtemail->SubmittedValue;
        $updateuser = $account->UpdateAccount($hiddenacctid->SubmittedValue,$ddlaccttype->SelectedValue,mysql_escape_string(trim($txtfname->SubmittedValue)),mysql_escape_string(trim($txtaddress->SubmittedValue)),mysql_escape_string(trim($txtcontactnum->SubmittedValue)),mysql_escape_string(trim($txtemail->SubmittedValue)));
        //$updateuser = $account->UpdateAccount($hiddenacctid->SubmittedValue,$ddlaccttype->SelectedValue,mysql_escape_string(trim($txtfname->SubmittedValue)),mysql_escape_string(trim($txtaddress->SubmittedValue)),mysql_escape_string(trim($txtcontactnum->SubmittedValue)));
        if($account->HasError)
        {
            $confupdateuser_title = "ERROR!";
            $confupdateuser_msg = "Error has occured: "  . $account->getError();
        }
        else
        {
            $auditlog = new TMAuditLog();
            $auditdtls["SessionID"] = $_SESSION['sid'];
            $auditdtls["AID"] = $_SESSION['acctid'];
            $auditdtls["TransDetails"] = "Account ID: " . $hiddenacctid->SubmittedValue;
            $auditdtls["TransDateTime"] = "now_usec()";
            $auditdtls["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
            $auditdtls["AuditTrailFunctionID"] = 15;
            $insertauditlog = $auditlog->Insert($auditdtls);
            if($auditlog->HasError)
            {
                $confupdateuser_title = "ERROR!";
                $confupdateuser_msg = "Error has occured: "  . $auditlog->getError();
            }
            $confupdateuser_title = "SUCCESSFUL!";
            $confupdateuser_msg = "The account has been successfully modified. Thank you";
        }
    }
    if($btnConfirmChangeStat->SubmittedValue == "Okay")
    {
        $stat = $account->UpdateAccountStatus($hiddenacctid->SubmittedValue,$ddlstatus->SelectedValue);
		if($status == "Locked - Invalid Attempts")
			$loginattempts = $account->UpdateInvalidAttempts($hiddenacctid->SubmittedValue);
        if($account->HasError)
        {
            $updatestat_title = "ERROR!";
            $updatestat_msg = "Error has occured:" . $account->getError();
        }
        else
        {
	    	$auditlog = new TMAuditLog();
            $auditdtls["SessionID"] = $_SESSION['sid'];
            $auditdtls["AID"] = $_SESSION['acctid'];
            $auditdtls["TransDetails"] = "Account ID: " . $hiddenacctid->SubmittedValue;
            $auditdtls["TransDateTime"] = "now_usec()";
            $auditdtls["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
            $auditdtls["AuditTrailFunctionID"] = 16;
            $insertauditlog = $auditlog->Insert($auditdtls);
            if($auditlog->HasError)
            {
                $confupdateuser_title = "ERROR!";
                $confupdateuser_msg = "Error has occured: "  . $auditlog->getError();
            }
            $updatestat_title = "SUCCESSFULL!";
            $updatestat_msg = "Status was successfully updated";
			switch($ddlstatus->SelectedValue)
			{
				case 1:
				$status = "Active";
				$updatestat_msg = "The account has now been activated.";
				break;
				case 4:
				$status = "Locked";
				$updatestat_msg = "The account has been successfully locked.";
				break;
			}
        }
    }
    if($btnConfirmResetPword->SubmittedValue == "Okay")
    {
		if(trim($email) == "")
        {
            $emailtitle = "ERROR!";
            $emailmsg = "No recorded email address.";
        }
        else
        {
			$requestcode = MD5($txtusername->Text.$hiddenacctid->SubmittedValue.date("Y-m-d H:i:s"));
			$passwordrequest["AID"] = $hiddenacctid->SubmittedValue;
			$passwordrequest["RequestCode"] = $requestcode;
			$passwordrequest["DateRequested"] = date("Y-m-d H:i:s"); 
			$passwordrequest["Status"] = 0;
			$changepwordrequest = $passwordupdaterequest->Insert($passwordrequest);

			if($passwordupdaterequest->HasError)
			{
				$emailmsg = "Error has occurred: " . $passwordupdaterequest->getError();
				    $emailtitle = "ERROR!";
		   	}

		    $pm = new PHPMailer();
		    $pm->AddAddress($txtemail->Text, $txtfname->Text);
			$pageURL = 'http';
		    if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
		    $pageURL .= "://";
		    $folder = $_SERVER["REQUEST_URI"];
		    $folder = substr($folder,0,strrpos($folder,'/') + 1);
		    if ($_SERVER["SERVER_PORT"] != "80") 
		    {
		      $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$folder;
		    } 
		    else 
		    {
		      $pageURL .= $_SERVER["SERVER_NAME"].$folder;
		    }
		    /*$pm->Body = "
		        Hi $username, please click the reset password link below in order to change your current password.  \n" . 
		        $pageURL . "changepassword.php?id=".$hiddenacctid->SubmittedValue."
		    ";*/
			$pm->Body = "
		        Hi $username, \n\t\t\t Please click the reset password link below in order to change your current password.  
		        \n\t\t\t " . $pageURL . "changepassword.php?rcode=".$requestcode."
		    ";
		    $pm->From = "testaccount@philweb.com.ph";
		    $pm->FromName = "Ticket Management System";
		    $pm->Host = "localhost";
		    $pm->Subject = "Ticket Management System Change Password";
		    $email_sent = $pm->Send();
		    if($email_sent)
		    {
		        $emailmsg = "The password reset link has been successfully sent to the email account.";
		        $emailtitle = "SUCCESSFULL!";
		    }
		    else
		    {
		        $emailmsg = "An error occurred while sending the email to your email address";
		        $emailtitle = "ERROR!";
		    }
		    /*$changepword["ID"] = $id;
		    $changepword["ForChangePassword"] = 0;*/
		    $updatechangepword = $account->UpdateAccountChangePasswordStatus($id,1);
		    if($account->HasError)
		    {
		        $updatepwordstat = "Error has occurred: " . $account->getError();
		    }
		}
    }
}
else
{
    header("Location: acctlistretailer.php");
}
?>