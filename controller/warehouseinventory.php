<?php
include("../init.inc.php");
include("../controller/warehouseinvprocess.php");

?>
<link rel="stylesheet" type="text/css" media="screen" href="../css/default.css" />
<script type="text/javascript" src="../jscripts/jquery-1.5.2.min.js" language="Javascript"></script>
<script language="javascript" type="text/javascript">
function ChangePage(pagenum)
{
    selectedindex = document.getElementById("pgSelectedPage");
    selectedindex.value = pagenum;
    document.forms[0].submit();
}

function editDeckChkBox()
{
    /*
    var chkbx = document.getElementById('checkbox').checked;

    if (document.getElementById('checkbox').checked)
    {
        alert("Please select at least one record.");
        return false ;
    }
    else
    {
        document.forms["frmDecks"].action="warehouseinventory_update.php";
        document.forms["frmDecks"].submit();
        return true;
    }
    */
    document.forms["frmDecks"].action="warehouseinventory_update.php";
    document.forms["frmDecks"].submit();
}

function Check(chk)
{
    if(document.forms["frmDecks"].checkbox2.checked==true)
    {
        for (i = 0; i < chk.length; i++)
        chk[i].checked = true ;
    }else
    {
        for (i = 0; i < chk.length; i++)
        chk[i].checked = false ;
    }
}

function check_active()
{
    if(document.getElementById('ddlstatus').value == 1)
    {
        document.getElementById('ddlassignedto').disabled = false;
    }
    else if(document.getElementById('ddlstatus').value == 4)
    {
        document.getElementById('ddlassignedto').disabled = false;
    }
    else
    {
        document.getElementById('ddlassignedto').disabled = true;
    }
}

function validation()
{
    var statusval = document.getElementById('ddlstatus').value;

    if(statusval == 0)
    {
        document.getElementById('title').innerHTML = "ERROR!";
        document.getElementById('msg').innerHTML = "Please select status.";
        document.getElementById('light').style.display = "block";
        document.getElementById('fade').style.display = "block";
        return false;
    }
    else
    {
        return true;
    }
}

function get_product()
{
    var pid = document.getElementById('ddlprodtype').value;

    $.ajax
    ({
        url: '../controller/get_product.php?pid='+pid,
        type : 'post',
        success : function(data)
        {
            $('#ddlprodtype').val(data)
        },
        error: function(e)
        {
            alert("Error");
        }
    });
}

</script>
<?php include("header.php"); ?>
<div id="fade" class="black_overlay"></div>
<div id="loading" class="loading"></div>
<!-- POP UP FOR MESSAGES -->
<div id="light" class="white_content">
    <div id="title" class="light-title"></div>
    <div id="msg" class="light-message"></div>
    <div id="button" class="light-button"><input type="button" onclick="javascript: document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none';" value="OKAY"/></div>
</div>
<!-- POP UP FOR MESSAGES -->
<form id="frmDecks" name="frmDecks" method="post">
<div class="content-page">
    <div style="margin-top: 10px; margin-bottom: 10px; margin-left: 10px;">
        <?php echo $txtsearch; ?><?php echo $btnsearch; ?>
    </div>
    <table style="float: left; margin-left: 10px;">
        <tr>
            <td>Status: </td><td><?php echo $ddldeckstatus; ?></td>
        </tr>
        <tr>
            <td>Assigned To: </td><td><?php echo $ddlassignedto; ?></td>
        </tr>
    </table>
    <table style="margin-left: 50%;">
        <tr>
            <td>Game Number: </td><td><?php echo $ddlgametype; ?></td>
        </tr>
        <tr>
            <td>Product: </td><td><?php echo $ddlprodtype; ?></td>
        </tr>
    </table>
    <div style="margin-top: 10px; margin-bottom: 10px; margin-left: 70%;">
        <?php echo $btnSubmit; ?>
    </div>
    <?php if(isset($tbldata_list)):?>
    <table class="table-list">
        <tr>
            <th><input type="checkbox" id="checkbox2" name="checkbox2" value="yes" onClick="Check(document.forms['frmDecks'].checkbox)" /></th>
            <th>Game Number</th>
            <th>Product</th>
            <th>Carton Number</th>
            <th>Book Number</th>
            <th>Status</th>
            <th>Assigned To</th>
        </tr>
        <?php if(count($tbldata_list) > 0):?>
        <?php for($ctr = 0 ; $ctr < count($tbldata_list) ; $ctr++):?>
        <?php ($ctr % 2) == 0 ? $class = "evenrow" : $class = "oddrow" ; ?>
        <tr class = "<?php echo $class?>">
            <td><input type="checkbox" id="box" name="checkbox[]" value="<?php echo $tbldata_list[$ctr]['ID']?>" /></td>
            <td><?php echo $tbldata_list[$ctr]['GameNumber']?></td>
            <td><?php echo $tbldata_list[$ctr]['ProductName']?></td>
            <td><?php echo $tbldata_list[$ctr]['BoxNumber']?></td>
            <td><?php echo $tbldata_list[$ctr]['BookNumber']?></td>
            <td><?php echo $tbldata_list[$ctr]['Status']?></td>
            <td><?php echo $tbldata_list[$ctr]['AssignedTo']?></td>
        </tr>
        <?php endfor;?>
        <?php else:?>
        <tr class="no-record"><td colspan="6">No records to display</td></tr>
        <?php endif;?>
    </table>
    <?php endif;?>
</div>
<br/>
<div style="float: right;">
<?php echo $btnchngestatus; ?>
</div>
<div style="float: right; margin-right: -12%; margin-top: 30px;">
<?php echo $pgwarehouseinv; ?>
</div>
</form>
<?php include("footer.php"); ?>