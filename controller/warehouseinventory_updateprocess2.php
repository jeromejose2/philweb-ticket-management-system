<?php
/* * *****************
 * Author: J.O. Pormento
 * Date Created: 2011-09-01
 * Description: Update deck status
 * ***************** */
$stylesheets[] = "css/default.css";
$javascripts[] = "jscripts/jquery-1.5.2.min.js";
$javascripts[] = "jscripts/datetimepicker.js";
$stylesheets[] = "css/datepicker.css";
$pagesubmenuid = 3;
$accttypeid = $_SESSION['accttype'];
if ($accttypeid == '2' || $accttypeid == '4'){
//    $redirecttitle = "FORBIDDEN";
//    $redirectmsg = "You are not allowed to view this page.";
    App::Pr("<script> window.location = 'views/warehouseinventory2.php'; </script>");
}

App::LoadModuleClass("TicketManagementCM", "TMAccounts");
App::LoadModuleClass("TicketManagementCM", "TMDecks");
App::LoadModuleClass("TicketManagementCM", "TMAuditLog");

App::LoadControl("TextBox");
App::LoadControl("ComboBox");
App::LoadControl("Button");
App::LoadControl("Hidden");

$auditlog = new TMAuditLog();
$frmwarehouseinvupdate = new FormsProcessor();
$account = new TMAccounts();
$deck = new TMDecks();

$accounts = $account->SelectByAccountType();
$accounts_list = new ArrayList();
$accounts_list->AddArray($accounts);


$ddldeckstatus = new ComboBox("ddlstatus", "ddlstatus", "Status: ");
$litem = null;
$litem[] = new ListItem("Please select", "0");
$litem[] = new ListItem("Active", "1");
//$litem[] = new ListItem("On Freight", "2");
$litem[] = new ListItem("On Stock", "3");
$litem[] = new ListItem("Assigned", "4");
//$litem[] = new ListItem("Cancelled", "5");
$ddldeckstatus->Items = $litem;
$ddldeckstatus->ShowCaption = true;
//$ddldeckstatus->Args = "style='margin-top: 10px;'";
$ddldeckstatus->Args = "onchange='javascript: check_active();'";


$ddlassignedto = new ComboBox("ddlassignedto", "ddlassignedto", "Assigned To: ");
$ddlassignedto->Args = "onchange='javascript: get_address();'";
$litemassto = null;
$litemassto[] = new ListItem("Please select", "0", true);
$ddlassignedto->Items = $litemassto;
$ddlassignedto->ShowCaption = true;
$ddlassignedto->DataSource = $accounts_list;
$ddlassignedto->DataSourceText = "Name";
$ddlassignedto->DataSourceValue = "AID";
$ddlassignedto->DataBind();
$ddlassignedto->Enabled = false;

$txtaddress = new TextBox("txtaddress","txtaddress","Address: ");
$txtaddress->ShowCaption = true;
//$txtaddress->Enabled = false;
$txtaddress->ReadOnly = true;
$txtaddress->Args = "style='width: 500px;'";

$txtreleasedate = new TextBox("txtreleasedate","txtreleasedate","Release Date: ");
$txtreleasedate->ShowCaption = true;
$txtreleasedate->ReadOnly = true;
//$txtreleasedate->Args = "style='margin-left: 10%;'";

$btnprocess = new Button("btnprocess","btnprocess","Process");
$btnprocess->IsSubmit = true;
$btnprocess->Args = "onclick='javascript: return checkupdatestatus()'";

$btncancel = new Button("btncancel","btncancel","Cancel");
$btncancel->IsSubmit = false;
$btncancel->Args = "onclick='javascript: return redirectToWarehouseInventory();'";

//$btnRedirect = new Button("btnRedirect","btnRedirect", "OKAY2");
//$btnRedirect->IsSubmit = true;

$hidden_date = new Hidden("hidden_date","hidden_date","Hidden date");
$hidden_date->Text = date("m-d-Y");

$hidden_assignedaddress = new Hidden("hidden_assignedaddress","hidden_assignedaddress","Hidden Assinged Address");

$hidden_assignedid = new Hidden("hidden_assignedid","hidden_assignedid","Hidden Assinged Id");

$btnConfirm = new Button("btnConfirm","btnConfirm","Okay");
$btnConfirm->IsSubmit = true;

$btnOkay = new Button("btnOkay","btnOkay","Okay");
$btnOkay->IsSubmit = true;
$btnOkay->Args = "onclick='javascript: return redirectToWarehouseInventory();'";

$btnClose = new Button("btnClose","btnClose","Cancel");
$btnClose->Args = "onclick='javascript: return CloseLightBox();'";

$frmwarehouseinvupdate->AddControl($ddldeckstatus);
$frmwarehouseinvupdate->AddControl($ddlassignedto);
$frmwarehouseinvupdate->AddControl($txtaddress);
$frmwarehouseinvupdate->AddControl($txtreleasedate);
$frmwarehouseinvupdate->AddControl($btnprocess);
$frmwarehouseinvupdate->AddControl($btncancel);
//$frmwarehouseinvupdate->AddControl($btnRedirect);
$frmwarehouseinvupdate->AddControl($hidden_date);
$frmwarehouseinvupdate->AddControl($btnConfirm);
$frmwarehouseinvupdate->AddControl($btnOkay);
$frmwarehouseinvupdate->AddControl($hidden_assignedaddress);
$frmwarehouseinvupdate->AddControl($hidden_assignedid);
$frmwarehouseinvupdate->AddControl($btnClose);


$frmwarehouseinvupdate->ProcessForms();

if ($frmwarehouseinvupdate->IsPostBack)
{  
    if($frmwarehouseinvupdate->GetPostVar("checkbox"))
    {
        //get deckid to update
        $deckid = $frmwarehouseinvupdate->GetPostVar("checkbox");
        $changeby = $frmwarehouseinvupdate->GetPostVar("changeby");
        $status = $frmwarehouseinvupdate->GetPostVar("ddlstatus");
      	
        if($changeby == "2")
        {
             $_SESSION['changeby'] = $changeby;
             $_SESSION['arrdeckid'] = $deckid;
             $_SESSION['stat'] = $status;				
             $tbldata = $deck->SelectDeckByBoxID($deckid,$status);
             $tbldata_list = new ArrayList();
             $tbldata_list->AddArray($tbldata);
             $status = $deck->SelectStatusByBoxID($deckid,$status);
             $_SESSION["status"] = $status[0]["Status"];
             $_SESSION['assignedid'] = $status[0]["AssignedToAID"];
            
        }
        else
        {
            $_SESSION['changeby'] = $changeby;
            $_SESSION['arrdeckid'] = $deckid;
			$_SESSION['stat'] = $status;
            $tbldata = $deck->SelectDeckByID($deckid);
            $tbldata_list = new ArrayList();
            $tbldata_list->AddArray($tbldata);
            $status = $deck->SelectStatus($deckid);
            $_SESSION["status"] = $status[0]["Status"];
            $_SESSION['assignedid'] = $status[0]["AssignedToAID"];
        }
        if($frmwarehouseinvupdate->GetPostVar("ddlstatus"))
        {

        }
    }

    switch($_SESSION["status"])
    {
        case 1:
            $btnprocess->Style = "display:none";
            $ddldeckstatus->Enabled = false;
            $assignedtodtls = $deck->GetAssignedToDtls($_SESSION['assignedid']);
            $ddlassignedto->SetSelectedValue($assignedtodtls[0]["AID"]);
            $txtaddress->Text = $assignedtodtls[0]["Address"];
            $hidden_assignedid->Text = $assignedtodtls[0]["AID"];
            $hidden_assignedaddress->Text = $assignedtodtls[0]["Address"];
            break;
        case 2:
            $options = null;
            $options[] = new ListItem("Please select" , "" , true);
            $options[] = new ListItem("On Stock" , "3");
            $ddldeckstatus->Items = $options;
            break;
        case 3:
            $options = null;
            $options[] = new ListItem("Please select" , "" , true);
            $options[] = new ListItem("Assigned" , "4");
			//$options[] = new ListItem("On Freight", "2");
            $ddldeckstatus->Items = $options;
            break;
        case 4:
            $options = null;
            $options[] = new ListItem("Please select" , "" , true);
            $options[] = new ListItem("On Stock" , "3");
            $options[] = new ListItem("Assigned" , "4");
			$options[] = new ListItem("Active" , "1");
            $ddldeckstatus->Items = $options;
            $assignedtodtls = $deck->GetAssignedToDtls($_SESSION['assignedid']);
            $ddlassignedto->SetSelectedValue($assignedtodtls[0]["AID"]);
            $txtaddress->Text = $assignedtodtls[0]["Address"];
            $hidden_assignedid->Text = $assignedtodtls[0]["AID"];
            $hidden_assignedaddress->Text = $assignedtodtls[0]["Address"];
            $ddlassignedto->Enabled = true;
            break;
        case 5:
            $btnprocess->Style = "display:none";
            $ddldeckstatus->Enabled = false;
            break;
        default:
            break;
    }
    if(($btnConfirm->SubmittedValue == "Okay") || ($btnConfirm->SubmittedValue == "Yes"))
    {
        $deckidupdte = $_SESSION['arrdeckid'];
        $changeby = $_SESSION['changeby'];
        $stat = $_SESSION['stat'];
		//var_dump($deckidupdte);
		//echo"<br>";
		//var_dump($deckidupdte);	
		//echo"<br>";
		//var_dump($stat);
		//exit();	
        $tbldata = $deck->SelectDeckByID($_SESSION['arrdeckid']);
        $tbldata_list = new ArrayList();
        $tbldata_list->AddArray($tbldata);
        if($ddldeckstatus->SubmittedValue == 4)
        {
	    $audittrailfunctionid = 31;
            if($changeby == "2")
            {
                $tbldata = $deck->UpdateStatusToAssignedByBoxID($ddldeckstatus->SubmittedValue,$ddlassignedto->SubmittedValue,$deckidupdte,$_SESSION['acctid'],$stat);
            }
            else
            {
                 $tbldata = $deck->UpdateStatusToAssigned($ddldeckstatus->SubmittedValue,$ddlassignedto->SubmittedValue,$deckidupdte,$_SESSION['acctid']);
            }
           
            if($deck->HasError)
            {
                $successtitle = "ERROR";
                $successmsg = "Error has occurred: " . $deck->getError();
            }
            else
            {
                $successtitle = "SUCCESS";
                $successmsg = "The item information has been successfully modified.";
            }
        }
        else if($ddldeckstatus->SubmittedValue == 2)
        {
            $audittrailfunctionid = 29;            
            $tbldata = $deck->UpdateDeckStatusToOnFreight($ddldeckstatus->SubmittedValue,$deckidupdte,$_SESSION['acctid']);
            if($deck->HasError)
            {
                $successtitle = "ERROR";
                $successmsg = "Error has occurred: " . $deck->getError();
            }
            else
            {
                $successtitle = "SUCCESS";
                $successmsg = "The item information has been successfully modified.";
            }
        }
        else if($ddldeckstatus->SubmittedValue == 3)
        {
	    $audittrailfunctionid = 30;
            if($changeby == "2")
            { 
                $tbldata = $deck->UpdateDeckStatusToOnStockByBoxID($ddldeckstatus->SubmittedValue,$deckidupdte,$_SESSION['acctid'],$stat);
            }
            else
            {
                $tbldata = $deck->UpdateDeckStatusToOnStock($ddldeckstatus->SubmittedValue,$deckidupdte,$_SESSION['acctid']);
            }
            if($deck->HasError)
            {
                $successtitle = "ERROR";
                $successmsg = "Error has occurred: " . $deck->getError();
            }
            else
            {
                $successtitle = "SUCCESS";
                $successmsg = "The item information has been successfully modified.";
            }
        }
        else if($ddldeckstatus->SubmittedValue == 1)
        {
	    $audittrailfunctionid = 28;
            if($changeby == "2")
            {
                $tbldata = $deck->UpdateStatusToActiveByBoxID($ddldeckstatus->SubmittedValue,$txtreleasedate->SubmittedValue,$deckidupdte,$_SESSION['acctid'],$stat);
            }
            else
            {
                $tbldata = $deck->UpdateStatusToActive($ddldeckstatus->SubmittedValue,$txtreleasedate->SubmittedValue,$deckidupdte,$_SESSION['acctid']);
            }
            if($deck->HasError)
            {
                $successtitle = "ERROR";
                $successmsg = "Error has occurred: " . $deck->getError();
            }
            else
            {
                $successtitle = "SUCCESS";
                $successmsg = "The item has been tagged as 'Active' and modifications will no longer be allowed.";
            }
        }
        else
        {
	    $audittrailfunctionid = 32;
            $tbldata = $deck->UpdateDeckStatusByStatusOnly($ddldeckstatus->SubmittedValue,$deckidupdte);
            if($deck->HasError)
            {
                $successtitle = "ERROR";
                $successmsg = "Error has occurred: " . $deck->getError();
            }
            else
            {
                $successtitle = "SUCCESS";
                $successmsg = "The item has been tagged as 'Active' and modifications will no longer be allowed.";
            }
        }
		//audit trail log
		$deckidupdte = $_SESSION['arrdeckid'];
        $auditdtls["SessionID"] = $_SESSION['sid'];
        $auditdtls["AID"] = $_SESSION['acctid'];
        $auditdtls["TransDetails"] = "Deck ID/S: " . join(",",$deckidupdte);
        $auditdtls["TransDateTime"] = "now_usec()";
        $auditdtls["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
        $auditdtls["AuditTrailFunctionID"] = $audittrailfunctionid;
        $insertauditlog = $auditlog->Insert($auditdtls);
        if($auditlog->HasError)
        {
            $successtitle = "ERROR!";
            $successmsg = "Error has occured:" . $tmaccount->getError();
        }
    }
    if($btnprocess->SubmittedValue == "Process")
    {
        $tbldata = $deck->SelectDeckByID($_SESSION['arrdeckid']);
        
        if($_SESSION['changeby'] == "2")
        {  
         $tbldata = $deck->SelectDeckByBoxID($_SESSION['arrdeckid'],$_SESSION["stat"]);
         $tbldata_list = new ArrayList();
         $tbldata_list->AddArray($tbldata);
         $status = $deck->SelectStatusByBoxID($deckid,$_SESSION["stat"]);
         $_SESSION["status"] = $status[0]["Status"];
         $_SESSION['assignedid'] = $status[0]["AssignedToAID"];
            
        }
        else
        {           
            $tbldata = $deck->SelectDeckByID($_SESSION['arrdeckid']);
            $tbldata_list = new ArrayList();
            $tbldata_list->AddArray($tbldata);
            $status = $deck->SelectStatus($deckid);
            $_SESSION["status"] = $status[0]["Status"];
            $_SESSION['assignedid'] = $status[0]["AssignedToAID"];
        }
        
        $tbldata_list = new ArrayList();
        $tbldata_list->AddArray($tbldata);
        $confirmtitle = "CONFIRMATION";
        if($ddldeckstatus->SubmittedValue == 1)
        {
            $btnConfirm->Text = "Yes";
            $btnClose->Text = "No";
            $confirmmsg = "Items that are tagged as 'Active' will no longer allowed to be modified.Proceed?";
        }
        else
        {
            $confirmmsg = "Save changes?";
            
        }
        $ddlassignedto->SetSelectedValue($ddlassignedto->SubmittedValue);
        $ddldeckstatus->SetSelectedValue($ddldeckstatus->SubmittedValue);
        $ddlassignedto->Enabled = true;
    }
}
else
{
    App::Pr("<script> window.location = 'warehouseinventory2.php'; </script>");
}
?>
