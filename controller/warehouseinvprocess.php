<?php
/* * *****************
 * Author: J.O. Pormento
 * Date Created: 2011-09-01
 * Description: View booklets and Change status
 * ***************** */
$pagesubmenuid = 3;
$stylesheets[] = "css/default.css";
$javascripts[] = "jscripts/jquery-1.5.2.min.js";
$javascripts[] = "jscripts/validations.js";
/*$stylesheets[] = "";
$javascripts[] = "";*/

App::LoadModuleClass("TicketManagementCM", "TMAccounts");
App::LoadModuleClass("TicketManagementCM", "TMDecks");
App::LoadModuleClass("TicketManagementCM", "TMGameManagement");
App::LoadModuleClass("TicketManagementCM", "TMProducts");

App::LoadControl("TextBox");
App::LoadControl("ComboBox");
App::LoadControl("Button");
App::LoadControl("PagingControl2");
App::LoadControl("Hidden");

$frmwarehouseinv = new FormsProcessor();
$account = new TMAccounts();
$deck = new TMDecks();
$game = new TMGameManagement();
$product = new TMProducts();

//get list of operator accounts
$accounts = $account->SelectByAccountType();
$accounts_list = new ArrayList();
$accounts_list->AddArray($accounts);

//get list of all game types
$games = $game->SelectByGameType();
$games_list = new ArrayList();
$games_list->AddArray($games);

//get list of all product types
$products = $product->SelectAllProducts();
$product_list = new ArrayList();
$product_list->AddArray($products);

//get list of game names
$gamenames = $product->SelectAllGameName();
$gamename_list = new ArrayList();
$gamename_list->AddArray($gamenames);

$ddldeckstatus = new ComboBox("ddlstatus", "ddlstatus", "Status: ");
$litem = null;
$litem[] = new ListItem("Please select", "0");
//$litem[] = new ListItem("On Freight", "2");
$litem[] = new ListItem("On Stock", "3");
$litem[] = new ListItem("Assigned", "4");
$litem[] = new ListItem("Active", "1");
//$litem[] = new ListItem("Cancelled", "5");
$ddldeckstatus->Items = $litem;
//$ddldeckstatus->ShowCaption = true;
$ddldeckstatus->Args = "onchange='get_gamename();'";
//$ddldeckstatus->Args = "style='margin-top: 5%;'";

$btnSubmit = new Button("btnSubmit","btnSubmit","Submit");
$btnSubmit->IsSubmit = true;
$btnSubmit->Args = "onclick='javascript: return validation();'";


$txtsearch = new TextBox("txtsearch","txtsearch","");
$txtsearch->ShowCaption = true;
$txtsearch->Length = 6;
$txtsearch->Args = "onkeypress = 'javascript: return isNumberKey(event);'";


$btnsearch = new Button("btnsearch","btnsearch","Search");
$btnsearch->IsSubmit = true;
$btnsearch->Args = "onclick='javascript: return checksearchdata();'";

$btnExportCSV = new Button("btnExportCSV","btnExportCSV","Export to CSV");
$btnExportCSV->IsSubmit = true;

$ddlassignedto = new ComboBox("ddlassignedto", "ddlassignedto", "Assigned To: ");
$ddlassignedto->Enabled = false;
$litemassto = null;
$litemassto[] = new ListItem("Please select", "0", true);
$ddlassignedto->Items = $litemassto;
//$ddlassignedto->ShowCaption = true;
$ddlassignedto->DataSource = $accounts_list;
$ddlassignedto->DataSourceText = "Name";
$ddlassignedto->DataSourceValue = "AID";
$ddlassignedto->DataBind();


$btnchngestatus = new Button("btnchngestatus","btnchngestatus","Change Status");
$btnchngestatus->IsSubmit = false;
$btnchngestatus->Args = "onclick='javascript: return editDeckChkBox();'";
$btnchngestatus->Style = "display:none;";

$btnviewdtls = new Button("btnviewdtls","btnviewdtls","View Details");
$btnviewdtls->IsSubmit = false;
$btnviewdtls->Args = "onclick='javascript: return editDeckChkBox();'";
$btnviewdtls->Style = "display:none;";


$ddlgametype = new ComboBox("ddlgametype", "ddlgametype", "Game Number: ");
$ddlgametype->Args = "onchange='javascript: get_batchID();'";

$litemgmetype = null;
$litemgmetype[] = new ListItem("Please select", "0", false);
$ddlgametype->Items = $litemgmetype;
//$ddlgametype->ShowCaption = true;
$ddlgametype->DataSource = $games_list;
$ddlgametype->DataSourceText = "GameNumber";
$ddlgametype->DataSourceValue = "GameID";
$ddlgametype->DataBind();

$ddlgamebatch = new Combobox("ddlgamebatch","ddlgamebatch","Game Batch:");
$gamebatch = null;
$gamebatch[] = new ListItem("Please select","0", true);
$ddlgamebatch->Items  = $gamebatch;
$ddlgamebatch->Args = "onchange = 'javascript: get_assigndto();'";

$ddlgamename = new ComboBox("ddlgamename","ddlgamename","Game Name:");
$ddlgamename->Args = "onchange = 'javascript: get_gamenumber();'"; // get_gamenumber();
$gamenameopt = null;
$gamenameopt[] = new ListItem("Please select", "0", false);
$ddlgamename->Items = $gamenameopt;
//$ddlgametype->ShowCaption = true;

$ddlgamename->DataSource = $gamename_list;
$ddlgamename->DataSourceText = "ProductName";
$ddlgamename->DataSourceValue = "ProductID";
$ddlgamename->DataBind();


//get list of game card value
$cardprize = $game->SelectByCardValue();
$cardprize_list = new ArrayList();
$cardprize_list->AddArray($cardprize);


$ddlcardvalue = new ComboBox('ddlcardvalue','ddlcardvalue','');
$ddlcardvalue->Args = "onchange='javascript: get_status();'";
$cardlist = null;
$cardlist[] = new ListItem("Please select"," ",false);
//$cardlist[] = new ListItem("ALL","0",false);
$ddlcardvalue->Items = $cardlist;
$ddlcardvalue->DataSource = $cardprize_list;
$ddlcardvalue->DataSourceText = "CardPrice";
$ddlcardvalue->DataSourceValue = "CardPrice";
$ddlcardvalue->DataBind();

$ddlprodtype = new TextBox("ddlprodtype", "ddlprodtype", "");
$ddlprodtype->ReadOnly = true;

$flag = new Hidden("flag","flag","flag");
$xmltype = new Hidden("xmltype","xmltype","xmltype");

/*Added by ARS*/
$hiddenctrid = new Hidden("hiddenctrid","hiddenctrid","Hidden Counter");

//pagination
$itemsperpage = 50;
$pgcon = new PagingControl2($itemsperpage, 1);
$pgcon->URL = "javascript:ChangePage(%currentpage);";
$pgcon->ShowMoveToFirstPage = true;
$pgcon->ShowMoveToLastPage = true;
$pgcon->PageGroup = 5;

//table display
$selectedstatus = 0;


$frmwarehouseinv->AddControl($ddldeckstatus);
$frmwarehouseinv->AddControl($btnSubmit);
$frmwarehouseinv->AddControl($txtsearch);
$frmwarehouseinv->AddControl($btnsearch);
$frmwarehouseinv->AddControl($ddlassignedto);
$frmwarehouseinv->AddControl($btnchngestatus);
$frmwarehouseinv->AddControl($ddlgametype);
$frmwarehouseinv->AddControl($ddlprodtype);
$frmwarehouseinv->AddControl($hiddenctrid);
$frmwarehouseinv->AddControl($ddlgamename);
$frmwarehouseinv->AddControl($ddlgamebatch);
$frmwarehouseinv->AddControl($flag);
$frmwarehouseinv->AddControl($xmltype);
$frmwarehouseinv->AddControl($btnviewdtls);
$frmwarehouseinv->AddControl($btnExportCSV);
$frmwarehouseinv->AddControl($ddlcardvalue);
$frmwarehouseinv->ProcessForms();


if($frmwarehouseinv->IsPostBack)
{
    if ($flag->SubmittedValue == 1 || $flag->SubmittedValue == 4  )
    {
        //game name
        $ddlgamename->Args = "onchange='javascript: get_gamenumber();'";          
        $ddlgamename->ClearItems();
        
        $gamenum1 = $ddlgametype->SubmittedValue;
        $gamenames = $product->SelectGameNamePerGameNum($gamenum1);
        $gamename_list1 = new ArrayList();
        $gamename_list1->AddArray($gamenames);       
        $litemgmetype1 = null;
        $litemgmetype1[] = new ListItem("Please select", "0", false);
        $ddlgamename->Items = $litemgmetype1;
       
        $ddlgamename->DataSource = $gamename_list1;
        $ddlgamename->DataSourceText = "ProductName";
        $ddlgamename->DataSourceValue = "ProductID";
        $ddlgamename->DataBind();
        
        $ddlgamename->SetSelectedValue($ddlgametype->SubmittedValue);
        //game num
        $ddlgametype->Args = "onchange='javascript: get_batchID(); '";
        $ddlgametype->ClearItems();
       
        $prodid1 = $ddlgamename->SubmittedValue;
        $gamenumbers = $product->SelectGameNumPerGameName($prodid1);
   
        $gamenumbers_list1 = new ArrayList();
        $gamenumbers_list1->AddArray($gamenumbers);       
        $litemgmetype12 = null;
        $litemgmetype12[] = new ListItem("Please select", "0", false);
        $ddlgametype->Items = $litemgmetype12;
        
        $ddlgametype->DataSource = $gamenumbers_list1;
        $ddlgametype->DataSourceText = "GameNumber";
        $ddlgametype->DataSourceValue = "GameID";
        $ddlgametype->DataBind();
        $ddlgametype->SetSelectedValue($ddlgametype->SubmittedValue);
    }
 
  
 
    $ddlgametype->SetSelectedValue($ddlgametype->SubmittedValue);
    $ddlgamename->SetSelectedValue($ddlgamename->SubmittedValue);
   
    $gamebatchID = $product->SelectBatchIDPerGameNum($ddlgametype->SelectedValue,$xmltype->SubmittedValue);
    $batchlist = new ArrayList();
    $batchlist->AddArray($gamebatchID);
    $gamebatch = null;
    $gamebatch[] = new ListItem("Please Select","0", true);
    $ddlgamebatch->Items  = $gamebatch;
    $ddlgamebatch->DataSource = $batchlist;
    $ddlgamebatch->DataSourceText = "BatchID";
    $ddlgamebatch->DataSourceValue = "GameBatchID";
    $ddlgamebatch->DataBind();
    $ddlgamebatch->SetSelectedValue($ddlgamebatch->SubmittedValue);
    
    $selectedstatus = $ddldeckstatus->SubmittedValue;
      
    $page =  ($pgcon->SelectedItemFrom - 1);
    
    if(($ddldeckstatus->SubmittedValue == 1) || ($ddldeckstatus->SubmittedValue == 4))
        {
            $ddlassignedto->Enabled = true;
        }
    	if(($ddldeckstatus->SubmittedValue == 1) || ($ddldeckstatus->SubmittedValue == 5))
        {
            $btnchngestatus->Text = "View Information";
        }
        if($hiddenctrid->SubmittedValue > 0)
        {
            $where = "A.BookNumber LIKE '%".$txtsearch->SubmittedValue."%' ORDER BY A.BookNumber ASC";
        }
        else
        {          
            if ($ddldeckstatus->SubmittedValue != 0 && $ddlassignedto->SubmittedValue != 0 && $ddlgametype->SubmittedValue != 0)
            {                  
                if($ddlcardvalue->SubmittedValue == 0)
                {
                   $where = "  A.Status = ".$ddldeckstatus->SubmittedValue." AND A.TransferredToAID = ".$ddlassignedto->SubmittedValue." AND B.GameID = ".$ddlgametype->SubmittedValue." AND A.GameBatchID = ".$ddlgamebatch->SubmittedValue. " ORDER BY A.BookNumber ASC";
           
                }else
                   $where = " B.CardPrice = ".$ddlcardvalue->SelectedText." AND A.Status = ".$ddldeckstatus->SubmittedValue." AND A.TransferredToAID = ".$ddlassignedto->SubmittedValue." AND B.GameID = ".$ddlgametype->SubmittedValue." AND A.GameBatchID = ".$ddlgamebatch->SubmittedValue. " ORDER BY A.BookNumber ASC";
            }
            
            else if($ddldeckstatus->SubmittedValue != 0 && $ddlgametype->SubmittedValue != 0)
            {   /*Added by ARS 12/16/2011*/
                if($ddlcardvalue->SubmittedValue == 0)
                {
                           $where = " A.Status = ".$ddldeckstatus->SubmittedValue." AND B.GameID = ".$ddlgametype->SubmittedValue." AND A.GameBatchID = ".$ddlgamebatch->SubmittedValue. " ORDER BY A.BookNumber ASC";                                
          
                }else
                $where = "B.CardPrice = ".$ddlcardvalue->SelectedText." AND A.Status = ".$ddldeckstatus->SubmittedValue." AND B.GameID = ".$ddlgametype->SubmittedValue." AND A.GameBatchID = ".$ddlgamebatch->SubmittedValue. " ORDER BY A.BookNumber ASC";                                
            }
            else if($ddldeckstatus->SubmittedValue != 0 && $ddlassignedto->SubmittedValue != 0)
            {          
                if($ddlcardvalue->SubmittedValue == 0)
                {
                       $where = " A.Status = ".$ddldeckstatus->SubmittedValue." AND A.TransferredToAID = ".$ddlassignedto->SubmittedValue." AND A.GameBatchID = ".$ddlgamebatch->SubmittedValue. " ORDER BY A.BookNumber ASC";
              
                }else
                 $where = " B.CardPrice = ".$ddlcardvalue->SelectedText." AND A.Status = ".$ddldeckstatus->SubmittedValue." AND A.TransferredToAID = ".$ddlassignedto->SubmittedValue." AND A.GameBatchID = ".$ddlgamebatch->SubmittedValue. " ORDER BY A.BookNumber ASC";
               
            }
            else
            {
                if($ddlcardvalue->SubmittedValue == 0)
                {
                         $where = "  A.Status = ".$ddldeckstatus->SubmittedValue." AND A.GameBatchID = ".$ddlgamebatch->SubmittedValue. " ORDER BY A.BookNumber ASC";
          
                }else
                $where = " B.CardPrice = ".$ddlcardvalue->SelectedText." AND A.Status = ".$ddldeckstatus->SubmittedValue." AND A.GameBatchID = ".$ddlgamebatch->SubmittedValue. " ORDER BY A.BookNumber ASC";
            }
        }

    if($btnSubmit->SubmittedValue == "Submit")
    {
        $page = 0;
        $tmppage = 0;
        $hiddenctrid->Text = "0";
        if(($ddldeckstatus->SubmittedValue == 1) || ($ddldeckstatus->SubmittedValue == 4))
        {
            $ddlassignedto->Enabled = true;
        }
		if(($ddldeckstatus->SubmittedValue == 1) || ($ddldeckstatus->SubmittedValue == 5))
        {
            $btnchngestatus->Text = "View Information";
        }
        //if ($ddldeckstatus->SubmittedValue != 0 && $ddlassignedto->SubmittedValue != 0 && $ddlgametype->SubmittedValue != 0 && $ddlprodtype->SubmittedValue != "")
        if ($ddldeckstatus->SubmittedValue != 0 && $ddlassignedto->SubmittedValue != 0 && $ddlgametype->SubmittedValue != 0)
        {
            if($ddlcardvalue->SubmittedValue == 0)
                {
                      $where = "  A.Status = ".$ddldeckstatus->SubmittedValue." AND A.TransferredToAID = ".$ddlassignedto->SubmittedValue." AND B.GameID = ".$ddlgametype->SubmittedValue." AND A.GameBatchID = ".$ddlgamebatch->SubmittedValue. " ORDER BY A.BookNumber ASC";
          
                }else
            $where = " B.CardPrice = ".$ddlcardvalue->SelectedText." AND A.Status = ".$ddldeckstatus->SubmittedValue." AND A.TransferredToAID = ".$ddlassignedto->SubmittedValue." AND B.GameID = ".$ddlgametype->SubmittedValue." AND A.GameBatchID = ".$ddlgamebatch->SubmittedValue. " ORDER BY A.BookNumber ASC";
        }
		else if($ddldeckstatus->SubmittedValue != 0 && $ddlgametype->SubmittedValue != 0)
                {
                    if($ddlcardvalue->SubmittedValue == 0)
                {
                        $where = "  A.Status = ".$ddldeckstatus->SubmittedValue." AND B.GameID = ".$ddlgametype->SubmittedValue." AND A.GameBatchID = ".$ddlgamebatch->SubmittedValue. " ORDER BY A.BookNumber ASC";
                 
                }else
                     $where = " B.CardPrice = ".$ddlcardvalue->SelectedText." AND A.Status = ".$ddldeckstatus->SubmittedValue." AND B.GameID = ".$ddlgametype->SubmittedValue." AND A.GameBatchID = ".$ddlgamebatch->SubmittedValue. " ORDER BY A.BookNumber ASC";
                                       
        }
        else if($ddldeckstatus->SubmittedValue != 0 && $ddlassignedto->SubmittedValue != 0)
        //else
        {
            if($ddlcardvalue->SubmittedValue == 0)
                {
                           $where = " A.Status = ".$ddldeckstatus->SubmittedValue." AND A.TransferredToAID = ".$ddlassignedto->SubmittedValue." AND A.GameBatchID = ".$ddlgamebatch->SubmittedValue. " ORDER BY A.BookNumber ASC";
      
                }else
             $where = " B.CardPrice = ".$ddlcardvalue->SelectedText." AND A.Status = ".$ddldeckstatus->SubmittedValue." AND A.TransferredToAID = ".$ddlassignedto->SubmittedValue." AND A.GameBatchID = ".$ddlgamebatch->SubmittedValue. " ORDER BY A.BookNumber ASC";
        }
        else
        {
            if($ddlcardvalue->SubmittedValue == 0)
                {
                       $where = " A.Status = ".$ddldeckstatus->SubmittedValue." ORDER BY A.BookNumber ASC";
         
                }else
            $where = " B.CardPrice = ".$ddlcardvalue->SelectedText." AND A.Status = ".$ddldeckstatus->SubmittedValue." ORDER BY A.BookNumber ASC";
        }
		
        $pgcon->SelectedPage = 1;
    }

    
    if ($btnExportCSV->SubmittedValue == "Export to CSV")
    {
        if($hiddenctrid->SubmittedValue > 0)
        {
            $where = "A.BookNumber LIKE '%".$txtsearch->SubmittedValue."%' ORDER BY A.BookNumber ASC";
        }
        else
        {          
            if ($ddldeckstatus->SubmittedValue != 0 && $ddlassignedto->SubmittedValue != 0 && $ddlgametype->SubmittedValue != 0)
            {       
                if($ddlcardvalue->SubmittedValue == 0)
                {
                     $where = " A.Status = ".$ddldeckstatus->SubmittedValue." AND A.TransferredToAID = ".$ddlassignedto->SubmittedValue." AND B.GameID = ".$ddlgametype->SubmittedValue." AND A.GameBatchID = ".$ddlgamebatch->SubmittedValue. " ORDER BY A.BookNumber ASC";
                
                }else
                 $where = "B.CardPrice = ".$ddlcardvalue->SelectedText." AND A.Status = ".$ddldeckstatus->SubmittedValue." AND A.TransferredToAID = ".$ddlassignedto->SubmittedValue." AND B.GameID = ".$ddlgametype->SubmittedValue." AND A.GameBatchID = ".$ddlgamebatch->SubmittedValue. " ORDER BY A.BookNumber ASC";
                 //Assigned  
            }
            
            else if($ddldeckstatus->SubmittedValue != 0 && $ddlgametype->SubmittedValue != 0)
            {   /*Added by ARS 12/16/2011*/
                if($ddlcardvalue->SubmittedValue == 0)
                {
                   $where = "  A.Status = ".$ddldeckstatus->SubmittedValue." AND B.GameID = ".$ddlgametype->SubmittedValue." AND A.GameBatchID = ".$ddlgamebatch->SubmittedValue. " ORDER BY A.BookNumber ASC";                                
                 
                }else
                $where = " B.CardPrice = ".$ddlcardvalue->SelectedText." AND A.Status = ".$ddldeckstatus->SubmittedValue." AND B.GameID = ".$ddlgametype->SubmittedValue." AND A.GameBatchID = ".$ddlgamebatch->SubmittedValue. " ORDER BY A.BookNumber ASC";                                
                //ON STOCK
            }
            else if($ddldeckstatus->SubmittedValue != 0 && $ddlassignedto->SubmittedValue != 0)
            {       
                if($ddlcardvalue->SubmittedValue == 0)
                {
                         $where = "  A.Status = ".$ddldeckstatus->SubmittedValue." AND A.TransferredToAID = ".$ddlassignedto->SubmittedValue." AND A.GameBatchID = ".$ddlgamebatch->SubmittedValue. " ORDER BY A.BookNumber ASC";
            
                }else
                 $where = " B.CardPrice = ".$ddlcardvalue->SelectedText." AND A.Status = ".$ddldeckstatus->SubmittedValue." AND A.TransferredToAID = ".$ddlassignedto->SubmittedValue." AND A.GameBatchID = ".$ddlgamebatch->SubmittedValue. " ORDER BY A.BookNumber ASC";
               
            }
            else
            {
                if($ddlcardvalue->SubmittedValue == 0)
                {
                         $where = "  A.Status = ".$ddldeckstatus->SubmittedValue." AND A.GameBatchID = ".$ddlgamebatch->SubmittedValue. " ORDER BY A.BookNumber ASC";
           
                }else
                $where = " B.CardPrice = ".$ddlcardvalue->SelectedText." AND A.Status = ".$ddldeckstatus->SubmittedValue." AND A.GameBatchID = ".$ddlgamebatch->SubmittedValue. " ORDER BY A.BookNumber ASC";
            }
        }
        
        //$tbldata = $deck->SelectDeckCondition($where);
        $tbldata = $deck->SelectDeckCondition($_SESSION[where]);
        $tbldata_list = new ArrayList();
        $tbldata_list->AddArray($tbldata);
        $csvData = array();
        for ($ctr = 0; $ctr < count($tbldata_list); $ctr++)
        {
//$litem[] = new ListItem("On Stock", "3");
//$litem[] = new ListItem("Assigned", "4");
//$litem[] = new ListItem("Active", "1");

            $csvData[] = $tbldata_list[$ctr]['CardPrice'].",".
                         $tbldata_list[$ctr]['ProductName'].",".
                         $tbldata_list[$ctr]['BookTicketCount'].",".
                         $tbldata_list[$ctr]['GameNumber'].",".
                         $tbldata_list[$ctr]['BatchID'].",".
                         $tbldata_list[$ctr]['BoxNumber'].",".
                         $tbldata_list[$ctr]['BookNumber'].",".
                         $tbldata_list[$ctr]['Status'].",".
                         $tbldata_list[$ctr]['Username'].",".
                         $tbldata_list[$ctr]['DateReleased'].",".
                         $tbldata_list[$ctr]['InvoiceNumber']."\r\n";
        }
        $Status =  $_SESSION[rptstatus];           
        $fp = fopen("../csv/Head_Office_Inventory.csv","w");
        if ($fp)
        {   
            if($Status == 3){
            $header = "Card Value,Game Name,Book Size,Game Number,Game Batch,Carton Number,Book Number,Status" . "\r\n";
            }else if($Status == 4){
            $header = "Card Value,Game Name,Book Size,Game Number,Game Batch,Carton Number,Book Number,Status,Assigned To" . "\r\n";    
            }else{
            $header = "Card Value,Game Name,Book Size,Game Number,Game Batch,Carton Number,Book Number,Status,Assigned To,Date Activated,Invoice Number" . "\r\n"; 
            }
            
            fwrite($fp,$header);
            if ($csvData){
                foreach($csvData as $rc)
                {
                    if(count($rc)>0) { fwrite($fp,$rc); }
                }
            }
            else
            {
                $rc = "\nNo Records Found;\n";
                fwrite($fp,$rc);
            }
            
            fclose($fp);
            header('Content-type: text/csv');
            header("Content-Disposition: attachment; filename=".'Head_Office_Inventory.csv');
            header('Pragma: public');
            readfile('../csv/Head_Office_Inventory.csv');
            exit;
        } else {
            echo "<script>alert('ok');</script>";
        }
    }
    if($btnsearch->SubmittedValue == "Search")
    {
        $hiddenctrid->Text = "1";
        //$where = "A.BookNumber LIKE '%".$txtsearch->SubmittedValue."%' ORDER BY A.BookNumber ASC";
        $where = "A.BookNumber LIKE '%".$txtsearch->SubmittedValue."' AND A.Status IN (1,2,3,4) ORDER BY A.BookNumber ASC";
        if($_SESSION[tmpbkn]!=$txtsearch->SubmittedValue)
        {
            $_SESSION[where2] = $where;
        }
        $pgcon->SelectedPage = 1;
		$page = 0;
    }    
    
    if($btnSubmit->SubmittedValue == "Submit")
    {
    $wherelimit = " ".$where . " LIMIT " . $page . " , " . $itemsperpage;    
    $count = $deck->SelectDeckCount($where);         
    $_SESSION[where] = $where;
    $_SESSION[rptstatus] = $ddldeckstatus->SelectedValue;
    }else{
        
        if(strlen($_SESSION[where]) == null || $_SESSION[where] != $_SESSION[where2])
        {
            
        $wherelimit = " ".$where . " LIMIT " . $page . " , " . $itemsperpage;    
        $count = $deck->SelectDeckCount($where);
        $_SESSION[where] = $where;
        }else{    
        $wherelimit = " ".$_SESSION[where] . " LIMIT " . $page . " , " . $itemsperpage;
        $count = $deck->SelectDeckCount($_SESSION[where]);
        }
    }    

    $rowcount = $count[0]['Count'];
    $pgcon->Initialize($itemsperpage, $rowcount);
    $pgwarehouseinv = $pgcon->PreRender();
    $tbldata = $deck->SelectDeckCondition($wherelimit);
    $tbldata_list = new ArrayList();
    $tbldata_list->AddArray($tbldata);
       
    $boolean = false;
    $first = $tbldata_list[0]["Status"];
    for ($a=1;$a<count($tbldata_list);$a++)
    {
        $second = $tbldata_list[$a]["Status"];
        $final = $first;
        $first = $second;
        if ($second != $final){ $boolean = true; }
    }
    
}

?>