<?php
/* * *****************
 * Author: J.O. Pormento
 * Date Created: 2011-09-01
 * Description: View booklets and Change status
 * ***************** */
$pagesubmenuid = 3;
$stylesheets[] = "css/default.css";
$javascripts[] = "jscripts/jquery-1.5.2.min.js";
$javascripts[] = "jscripts/validations.js";
/*$stylesheets[] = "";
$javascripts[] = "";*/

App::LoadModuleClass("TicketManagementCM", "TMAccounts");
App::LoadModuleClass("TicketManagementCM", "TMDecks");
App::LoadModuleClass("TicketManagementCM", "TMGameManagement");
App::LoadModuleClass("TicketManagementCM", "TMProducts");

App::LoadControl("TextBox");
App::LoadControl("ComboBox");
App::LoadControl("Button");
App::LoadControl("PagingControl2");
App::LoadControl("Hidden");

$frmwarehouseinv = new FormsProcessor();
$account = new TMAccounts();
$deck = new TMDecks();
$game = new TMGameManagement();
$product = new TMProducts();

//get list of operator accounts
$accounts = $account->SelectByAccountType();
$accounts_list = new ArrayList();
$accounts_list->AddArray($accounts);

//get list of all game types
$games = $game->SelectByGameType();
$games_list = new ArrayList();
$games_list->AddArray($games);

//get list of all product types
$products = $product->SelectAllProducts();
$product_list = new ArrayList();
$product_list->AddArray($products);

//get list of game names
$gamenames = $product->SelectAllGameName();
$gamename_list = new ArrayList();
$gamename_list->AddArray($gamenames);

$ddldeckstatus = new ComboBox("ddlstatus", "ddlstatus", "Status: ");
$litem = null;
$litem[] = new ListItem("Please select", "0");
//$litem[] = new ListItem("On Freight", "2");
$litem[] = new ListItem("On Stock", "3");
$litem[] = new ListItem("Assigned", "4");
$litem[] = new ListItem("Active", "1");
//$litem[] = new ListItem("Cancelled", "5");
$ddldeckstatus->Items = $litem;
//$ddldeckstatus->ShowCaption = true;
$ddldeckstatus->Args = "onchange='check_active();'";
//$ddldeckstatus->Args = "style='margin-top: 5%;'";

$btnSubmit = new Button("btnSubmit","btnSubmit","Submit");
$btnSubmit->IsSubmit = true;
$btnSubmit->Args = "onclick='javascript: return validation();'";


$txtsearch = new TextBox("txtsearch","txtsearch","");
$txtsearch->ShowCaption = true;
$txtsearch->Length = 6;
$txtsearch->Args = "onkeypress = 'javascript: return isNumberKey(event);'";


$btnsearch = new Button("btnsearch","btnsearch","Search");
$btnsearch->IsSubmit = true;
$btnsearch->Args = "onclick='javascript: return checksearchdata();'";


$ddlassignedto = new ComboBox("ddlassignedto", "ddlassignedto", "Assigned To: ");
$ddlassignedto->Enabled = false;
$litemassto = null;
$litemassto[] = new ListItem("Please select", "0", true);
$ddlassignedto->Items = $litemassto;
//$ddlassignedto->ShowCaption = true;
$ddlassignedto->DataSource = $accounts_list;
$ddlassignedto->DataSourceText = "Name";
$ddlassignedto->DataSourceValue = "AID";
$ddlassignedto->DataBind();


$btnchngestatus = new Button("btnchngestatus","btnchngestatus","Change Status");
$btnchngestatus->IsSubmit = false;
//$btnchngestatus->Enabled = false;
$btnchngestatus->Args = "onclick='javascript: return editDeckChkBox();'";
$btnchngestatus->Style = "display:none;";


$ddlgametype = new ComboBox("ddlgametype", "ddlgametype", "Game Number: ");
$ddlgametype->Args = "onchange='javascript: get_batchID(); get_gamename();'";

$litemgmetype = null;
$litemgmetype[] = new ListItem("Please select", "0", false);
$ddlgametype->Items = $litemgmetype;
//$ddlgametype->ShowCaption = true;
$ddlgametype->DataSource = $games_list;
$ddlgametype->DataSourceText = "GameNumber";
$ddlgametype->DataSourceValue = "GameID";
$ddlgametype->DataBind();

$ddlgamebatch = new ComboBox("ddlgamebatch","ddlgamebatch","Game Batch:");
$gamebatch = null;
$gamebatch[] = new ListItem("Please select","0", true);
$ddlgamebatch->Items  = $gamebatch;

$ddlgamename = new ComboBox("ddlgamename","ddlgamename","Game Name:");
$ddlgamename->Args = "onchange = 'javascript: get_gamenumber();get_batchID'";
$gamenameopt = null;
$gamenameopt[] = new ListItem("Please select", "0", false);
$ddlgamename->Items = $gamenameopt;
//$ddlgametype->ShowCaption = true;

$ddlgamename->DataSource = $gamename_list;
$ddlgamename->DataSourceText = "ProductName";
$ddlgamename->DataSourceValue = "ProductID";
$ddlgamename->DataBind();


$ddlprodtype = new TextBox("ddlprodtype", "ddlprodtype", "");
$ddlprodtype->ReadOnly = true;

$flag = new Hidden("flag","flag","flag");
$xmltype = new Hidden("xmltype","xmltype","xmltype");

/*Added by ARS*/
$hiddenctrid = new Hidden("hiddenctrid","hiddenctrid","Hidden Counter");

//pagination
$itemsperpage = 50;
$pgcon = new PagingControl2($itemsperpage, 1);
$pgcon->URL = "javascript:ChangePage(%currentpage);";
$pgcon->ShowMoveToFirstPage = true;
$pgcon->ShowMoveToLastPage = true;
$pgcon->PageGroup = 5;

/* Added by: SSB 01-10-2012 */
$ddlgamebatch = new Combobox("ddlgamebatch","ddlgamebatch","Game Batch:");
$gamebatch = null;
$gamebatch[] = new ListItem("Please select","0", true);
$ddlgamebatch->Items  = $gamebatch;

$flag = new Hidden("flag","flag","flag");
$xmltype = new Hidden("xmltype","xmltype","xmltype");
/* Added by: SSB */

/* Added by: SSB 02-07-2012*/
$changeby = new Combobox("changeby","changeby","Change by:");
$item = null;
$item[] = new ListItem("Please select","0", true);
$item[] = new ListItem("Book","1", false);
$item[] = new ListItem("Carton","2", false);
$changeby->Items  = $item;
/* Added by: SSB */

$frmwarehouseinv->AddControl($ddldeckstatus);
$frmwarehouseinv->AddControl($btnSubmit);
$frmwarehouseinv->AddControl($txtsearch);
$frmwarehouseinv->AddControl($btnsearch);
$frmwarehouseinv->AddControl($ddlassignedto);
$frmwarehouseinv->AddControl($btnchngestatus);
$frmwarehouseinv->AddControl($ddlgametype);
$frmwarehouseinv->AddControl($ddlprodtype);
$frmwarehouseinv->AddControl($hiddenctrid);
$frmwarehouseinv->AddControl($ddlgamename);
$frmwarehouseinv->AddControl($ddlgamebatch);
$frmwarehouseinv->AddControl($flag);
$frmwarehouseinv->AddControl($xmltype);
$frmwarehouseinv->AddControl($changeby);
$frmwarehouseinv->ProcessForms();


if($frmwarehouseinv->IsPostBack)
{
    if ($flag->SubmittedValue == 1)
    {
        $ddlgametype->Args = "onchange='javascript: get_batchID(); '";
        $ddlgametype->ClearItems();
       
        $prodid1 = $ddlgamename->SubmittedValue;
        $gamenumbers = $product->SelectGameNumPerGameName($prodid1);
    
        $gamenumbers_list1 = new ArrayList();
        $gamenumbers_list1->AddArray($gamenumbers);       
       
        $litemgmetype1 = null;
        $litemgmetype1[] = new ListItem("Please select", "0", false);
        $ddlgametype->Items = $litemgmetype1;
        
        $ddlgametype->DataSource = $gamenumbers_list1;
        $ddlgametype->DataSourceText = "GameNumber";
        $ddlgametype->DataSourceValue = "GameID";
        $ddlgametype->DataBind();
        
        $ddlgametype->SetSelectedValue($ddlgametype->SubmittedValue);
       
       
        
    }
     if ($flag->SubmittedValue == 2)
    {
        $ddlgamename->Args = "onchange='javascript:'";          
        $ddlgamename->ClearItems();
       
        $gamenum1 = $ddlgametype->SubmittedValue;
        $gamenames = $product->SelectGameNamePerGameNum($gamenum1);
    
        $gamename_list1 = new ArrayList();
        $gamename_list1->AddArray($gamenames);       
              $litemgmetype1 = null;
        $litemgmetype1[] = new ListItem("Please select", "0", false);
        $ddlgamename->Items = $litemgmetype1;
       
        $ddlgamename->DataSource = $gamename_list1;
        $ddlgamename->DataSourceText = "ProductName";
        $ddlgamename->DataSourceValue = "ProductID";
        $ddlgamename->DataBind();
        
        $ddlgamename->SetSelectedValue($ddlgametype->SubmittedValue);
       
        
        
    }
 
    $ddlgametype->SetSelectedValue($ddlgametype->SubmittedValue);
    $ddlgamename->SetSelectedValue($ddlgamename->SubmittedValue);
    
   
    $gamebatchID = $product->SelectBatchIDPerGameNum($ddlgametype->SelectedValue,$xmltype->SubmittedValue);
    $batchlist = new ArrayList();
    $batchlist->AddArray($gamebatchID);
    $gamebatch = null;
    $gamebatch[] = new ListItem("Please Select","0", true);
    $ddlgamebatch->Items  = $gamebatch;
    $ddlgamebatch->DataSource = $batchlist;
    $ddlgamebatch->DataSourceText = "BatchID";
    $ddlgamebatch->DataSourceValue = "GameBatchID";
    $ddlgamebatch->DataBind();
    $ddlgamebatch->SetSelectedValue($ddlgamebatch->SubmittedValue);
    
    $page =  ($pgcon->SelectedItemFrom - 1);
    //if(($btnSubmit->SubmittedValue != "Submit") || ($btnsearch->SubmittedValue == "Search"))
    //{
		if(($ddldeckstatus->SubmittedValue == 1) || ($ddldeckstatus->SubmittedValue == 4))
        {
            $ddlassignedto->Enabled = true;
        }
    	if(($ddldeckstatus->SubmittedValue == 1) || ($ddldeckstatus->SubmittedValue == 5))
        {
            $btnchngestatus->Text = "View Information";
        }
        if($hiddenctrid->SubmittedValue > 0)
        {
            $where = "A.BookNumber LIKE '%".$txtsearch->SubmittedValue."%' ";
        }
        else
        {
            //if ($ddldeckstatus->SubmittedValue != 0 && $ddlassignedto->SubmittedValue != 0 && $ddlgametype->SubmittedValue != 0 && $ddlprodtype->SubmittedValue != "")
            if ($ddldeckstatus->SubmittedValue != 0 && $ddlassignedto->SubmittedValue != 0 && $ddlgametype->SubmittedValue != 0)
            {       
                    
                   $where = "A.Status = ".$ddldeckstatus->SubmittedValue." AND A.AssignedToAID = ".$ddlassignedto->SubmittedValue." AND B.GameID = ".$ddlgametype->SubmittedValue." AND A.GameBatchID = ".$ddlgamebatch->SubmittedValue. " ";
            }
            
            else if($ddldeckstatus->SubmittedValue != 0 && $ddlgametype->SubmittedValue != 0)
                {/*Added by ARS 12/16/2011*/
                                                   
                    $where = "A.Status = ".$ddldeckstatus->SubmittedValue." AND B.GameID = ".$ddlgametype->SubmittedValue." AND A.GameBatchID = ".$ddlgamebatch->SubmittedValue. " ";                                
                                           
                }
            else if($ddldeckstatus->SubmittedValue != 0 && $ddlassignedto->SubmittedValue != 0)
            //else
            {               
                 $where = "A.Status = ".$ddldeckstatus->SubmittedValue." AND A.AssignedToAID = ".$ddlassignedto->SubmittedValue." AND A.GameBatchID = ".$ddlgamebatch->SubmittedValue. "";
               
            }
            else
		    {
		        $where = "A.Status = ".$ddldeckstatus->SubmittedValue." AND A.GameBatchID = ".$ddlgamebatch->SubmittedValue. " ";
		    }
        }
        //echo $where;exit();
    //}
        $changebyvalue = $changeby->SubmittedValue;
        $status = $ddldeckstatus->SubmittedValue;
    if($btnSubmit->SubmittedValue == "Submit")
    {
        $changebyvalue = $changeby->SubmittedValue;
        $page = 0;
        $hiddenctrid->Text = "0";
        if(($ddldeckstatus->SubmittedValue == 1) || ($ddldeckstatus->SubmittedValue == 4))
        {
            $ddlassignedto->Enabled = true;
        }
		if(($ddldeckstatus->SubmittedValue == 1) || ($ddldeckstatus->SubmittedValue == 5))
        {
            $btnchngestatus->Text = "View Information";
        }
        //if ($ddldeckstatus->SubmittedValue != 0 && $ddlassignedto->SubmittedValue != 0 && $ddlgametype->SubmittedValue != 0 && $ddlprodtype->SubmittedValue != "")
        if ($ddldeckstatus->SubmittedValue != 0 && $ddlassignedto->SubmittedValue != 0 && $ddlgametype->SubmittedValue != 0)
        {
            $where = "A.Status = ".$ddldeckstatus->SubmittedValue." AND A.AssignedToAID = ".$ddlassignedto->SubmittedValue." AND B.GameID = ".$ddlgametype->SubmittedValue." AND A.GameBatchID = ".$ddlgamebatch->SubmittedValue. " ";
        }
		else if($ddldeckstatus->SubmittedValue != 0 && $ddlgametype->SubmittedValue != 0)
                {
                     $where = "A.Status = ".$ddldeckstatus->SubmittedValue." AND B.GameID = ".$ddlgametype->SubmittedValue." AND A.GameBatchID = ".$ddlgamebatch->SubmittedValue. " ";
                                       
        }
        else if($ddldeckstatus->SubmittedValue != 0 && $ddlassignedto->SubmittedValue != 0)
        //else
        {
             $where = "A.Status = ".$ddldeckstatus->SubmittedValue." AND A.AssignedToAID = ".$ddlassignedto->SubmittedValue." AND A.GameBatchID = ".$ddlgamebatch->SubmittedValue. " ";
        }
        else
        {
            $where = "A.Status = ".$ddldeckstatus->SubmittedValue."";
        }
		
        $pgcon->SelectedPage = 1;
    }
    if($btnsearch->SubmittedValue == "Search")
    {
        $changebyvalue = "1";
        $hiddenctrid->Text = "1";
        //$where = "A.BookNumber LIKE '%".$txtsearch->SubmittedValue."%' ORDER BY A.BookNumber ASC";
		$where = "A.BookNumber LIKE '%".$txtsearch->SubmittedValue."%' AND A.Status IN (1,2,3,4)";
        $pgcon->SelectedPage = 1;
		$page = 0;
    }
    //App::Pr("<script>alert('".$where."')</script>");
    $forSearch = $hiddenctrid->SubmittedValue;
    $wherelimit = $where ."ORDER BY A.BookNumber ASC". " LIMIT " . $page . " , " . $itemsperpage;
    
    //var_dump($where);exit; 
    //echo $wherelimit;exit();
        if($forSearch == "1")
    {
        $tbldata = $deck->SelectDeckCondition2Search($wherelimit);
        $count = $deck->SelectDeckCount2Search($where);
    }
    else
    {          
        $tbldata = $deck->SelectDeckCondition2($wherelimit,$changebyvalue,$status);
        $count = $deck->SelectDeckCount2($where,$changebyvalue,$status);
    }
    $rowcount = $count[0]['Count'];
    $pgcon->Initialize($itemsperpage, $rowcount);
    $pgwarehouseinv = $pgcon->PreRender();

    $tbldata_list = new ArrayList();
    $tbldata_list->AddArray($tbldata);
}

?>

