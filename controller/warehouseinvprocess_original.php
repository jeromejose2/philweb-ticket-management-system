<?php
/* * *****************
 * Author: J.O. Pormento
 * Date Created: 2011-09-01
 * Description: View booklets and Change status
 * ***************** */
$pagesubmenuid = 3;
$stylesheets[] = "css/default.css";
$javascripts[] = "jscripts/jquery-1.5.2.min.js";
/*$stylesheets[] = "";
$javascripts[] = "";*/

App::LoadModuleClass("TicketManagementCM", "TMAccounts");
App::LoadModuleClass("TicketManagementCM", "TMDecks");
App::LoadModuleClass("TicketManagementCM", "TMGameManagement");
App::LoadModuleClass("TicketManagementCM", "TMProducts");

App::LoadControl("TextBox");
App::LoadControl("ComboBox");
App::LoadControl("Button");
App::LoadControl("PagingControl2");
App::LoadControl("Hidden");

$frmwarehouseinv = new FormsProcessor();
$account = new TMAccounts();
$deck = new TMDecks();
$game = new TMGameManagement();
$product = new TMProducts();

//get list of operator accounts
$accounts = $account->SelectByAccountType();
$accounts_list = new ArrayList();
$accounts_list->AddArray($accounts);

//get list of all game types
$games = $game->SelectByGameType();
$games_list = new ArrayList();
$games_list->AddArray($games);

//get list of all product types
$products = $product->SelectAllProducts();
$product_list = new ArrayList();
$product_list->AddArray($products);

$ddldeckstatus = new ComboBox("ddlstatus", "ddlstatus", "Status: ");
$litem = null;
$litem[] = new ListItem("Please select", "0");
$litem[] = new ListItem("On Freight", "2");
$litem[] = new ListItem("On Stock", "3");
$litem[] = new ListItem("Assigned", "4");
$litem[] = new ListItem("Active", "1");
//$litem[] = new ListItem("Cancelled", "5");
$ddldeckstatus->Items = $litem;
//$ddldeckstatus->ShowCaption = true;
$ddldeckstatus->Args = "onchange='check_active();'";
//$ddldeckstatus->Args = "style='margin-top: 5%;'";

$btnSubmit = new Button("btnSubmit","btnSubmit","Submit");
$btnSubmit->IsSubmit = true;
$btnSubmit->Args = "onclick='javascript: return validation();'";


$txtsearch = new TextBox("txtsearch","txtsearch","");
$txtsearch->ShowCaption = true;
$txtsearch->Length = 6;
//$txtsearch->Args = "style='margin-left: 35%;'";


$btnsearch = new Button("btnsearch","btnsearch","Search");
$btnsearch->IsSubmit = true;
$btnsearch->Args = "onclick='javascript: return checksearchdata();'";


$ddlassignedto = new ComboBox("ddlassignedto", "ddlassignedto", "Assigned To: ");
$ddlassignedto->Enabled = false;
$litemassto = null;
$litemassto[] = new ListItem("Please select", "0", true);
$ddlassignedto->Items = $litemassto;
//$ddlassignedto->ShowCaption = true;
$ddlassignedto->DataSource = $accounts_list;
$ddlassignedto->DataSourceText = "Name";
$ddlassignedto->DataSourceValue = "AID";
$ddlassignedto->DataBind();


$btnchngestatus = new Button("btnchngestatus","btnchngestatus","Change Status");
$btnchngestatus->IsSubmit = false;
//$btnchngestatus->Enabled = false;
$btnchngestatus->Args = "onclick='javascript: return editDeckChkBox();'";
$btnchngestatus->Style = "display:none;";


$ddlgametype = new ComboBox("ddlgametype", "ddlgametype", "Game Type: ");
$ddlgametype->Args = "onchange='javascript: get_product();'";
$litemgmetype = null;
$litemgmetype[] = new ListItem("Please select", "0", true);
$ddlgametype->Items = $litemassto;
//$ddlgametype->ShowCaption = true;
$ddlgametype->DataSource = $games_list;
$ddlgametype->DataSourceText = "GameNumber";
$ddlgametype->DataSourceValue = "GameID";
$ddlgametype->DataBind();


$ddlprodtype = new TextBox("ddlprodtype", "ddlprodtype", "");
$ddlprodtype->ReadOnly = true;

/*Added by ARS*/
$hiddenctrid = new Hidden("hiddenctrid","hiddenctrid","Hidden Counter");

//pagination
$itemsperpage = 50;
$pgcon = new PagingControl2($itemsperpage, 1);
$pgcon->URL = "javascript:ChangePage(%currentpage);";
$pgcon->ShowMoveToFirstPage = true;
$pgcon->ShowMoveToLastPage = true;
$pgcon->PageGroup = 5;


$frmwarehouseinv->AddControl($ddldeckstatus);
$frmwarehouseinv->AddControl($btnSubmit);
$frmwarehouseinv->AddControl($txtsearch);
$frmwarehouseinv->AddControl($btnsearch);
$frmwarehouseinv->AddControl($ddlassignedto);
$frmwarehouseinv->AddControl($btnchngestatus);
$frmwarehouseinv->AddControl($ddlgametype);
$frmwarehouseinv->AddControl($ddlprodtype);
$frmwarehouseinv->AddControl($hiddenctrid);

$frmwarehouseinv->ProcessForms();

if($frmwarehouseinv->IsPostBack)
{
    $page =  ($pgcon->SelectedItemFrom - 1);
    if(($btnSubmit->SubmittedValue != "Submit") || ($btnsearch->SubmittedValue == "Search"))
    {
        if($hiddenctrid->SubmittedValue > 0)
        {
            $where = "A.BookNumber LIKE '%".$txtsearch->SubmittedValue."%' ORDER BY A.BookNumber ASC";
        }
        else
        {
            if(($ddldeckstatus->SubmittedValue == 1) || ($ddldeckstatus->SubmittedValue == 4))
            {
                $ddlassignedto->Enabled = true;
            }
	    if(($ddldeckstatus->SubmittedValue == 1) || ($ddldeckstatus->SubmittedValue == 5))
            {
                $btnchngestatus->Text = "View Information";
            }
            //if ($ddldeckstatus->SubmittedValue != 0 && $ddlassignedto->SubmittedValue != 0 && $ddlgametype->SubmittedValue != 0 && $ddlprodtype->SubmittedValue != "")
            if ($ddldeckstatus->SubmittedValue != 0 && $ddlassignedto->SubmittedValue != 0 && $ddlgametype->SubmittedValue != 0)
            {
                $where = "A.Status = ".$ddldeckstatus->SubmittedValue." AND A.AssignedToAID = ".$ddlassignedto->SubmittedValue." AND B.GameID = ".$ddlgametype->SubmittedValue." ORDER BY A.BookNumber ASC";
            }
            else if($ddldeckstatus->SubmittedValue != 0 && $ddlassignedto->SubmittedValue != 0)
            //else
            {
                $where = "A.Status = ".$ddldeckstatus->SubmittedValue." AND A.AssignedToAID = ".$ddlassignedto->SubmittedValue." ORDER BY A.BookNumber ASC";
            }
            else
        {
            $where = "A.Status = ".$ddldeckstatus->SubmittedValue." ORDER BY A.BookNumber ASC";
        }
        }
        
    }
    if($btnSubmit->SubmittedValue == "Submit")
    {
        
        $hiddenctrid->Text = "0";
        if(($ddldeckstatus->SubmittedValue == 1) || ($ddldeckstatus->SubmittedValue == 4))
        {
            $ddlassignedto->Enabled = true;
        }
		if(($ddldeckstatus->SubmittedValue == 1) || ($ddldeckstatus->SubmittedValue == 5))
        {
            $btnchngestatus->Text = "View Information";
        }
        //if ($ddldeckstatus->SubmittedValue != 0 && $ddlassignedto->SubmittedValue != 0 && $ddlgametype->SubmittedValue != 0 && $ddlprodtype->SubmittedValue != "")
        if ($ddldeckstatus->SubmittedValue != 0 && $ddlassignedto->SubmittedValue != 0 && $ddlgametype->SubmittedValue != 0)
        {
            $where = "A.Status = ".$ddldeckstatus->SubmittedValue." AND A.AssignedToAID = ".$ddlassignedto->SubmittedValue." AND B.GameID = ".$ddlgametype->SubmittedValue." ORDER BY A.BookNumber ASC";
        }
		else if($ddldeckstatus->SubmittedValue != 0 && $ddlgametype->SubmittedValue != 0)
        {
            $where = "A.Status = ".$ddldeckstatus->SubmittedValue." AND B.GameID = ".$ddlgametype->SubmittedValue." ORDER BY A.BookNumber ASC";
        }
        else if($ddldeckstatus->SubmittedValue != 0 && $ddlassignedto->SubmittedValue != 0)
        //else
        {
            $where = "A.Status = ".$ddldeckstatus->SubmittedValue." AND A.AssignedToAID = ".$ddlassignedto->SubmittedValue." ORDER BY A.BookNumber ASC";
        }
        else
        {
            $where = "A.Status = ".$ddldeckstatus->SubmittedValue." ORDER BY A.BookNumber ASC";
        }

        $pgcon->SelectedPage = 1;
    }
    if($btnsearch->SubmittedValue == "Search")
    {
        $hiddenctrid->Text = "1";
        //$where = "A.BookNumber LIKE '%".$txtsearch->SubmittedValue."%' ORDER BY A.BookNumber ASC";
		$where = "A.BookNumber LIKE '%".$txtsearch->SubmittedValue."%' AND A.Status IN (1,2,3,4) ORDER BY A.BookNumber ASC";
        $pgcon->SelectedPage = 1;
    }
    //App::Pr("<script>alert('".$where."')</script>");
    $wherelimit = $where . " LIMIT " . $page . " , " . $itemsperpage;
    //var_dump($where);exit;
    $count = $deck->SelectDeckCount($where);
    $rowcount = $count[0]['Count'];
    $pgcon->Initialize($itemsperpage, $rowcount);
    $pgwarehouseinv = $pgcon->PreRender();
    $tbldata = $deck->SelectDeckCondition($wherelimit);
    $tbldata_list = new ArrayList();
    $tbldata_list->AddArray($tbldata);
}

?>

