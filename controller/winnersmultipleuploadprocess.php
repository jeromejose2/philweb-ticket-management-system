<?php
error_reporting(E_ALL);
ignore_user_abort(true);                                                        //Ignore user aborts and force the script running until the last part
set_time_limit(0);                                                              //Limits the maximum execution time , if set to 0 no maximum time is imposed. Safe mode should be turned off.
/*
 * Created By: Arlene R. Salazar 03/21/2012
 * Purpose: Controller for multiple winners xml upload
 */

$stylesheets[] = "css/default.css";
$javascripts[] = "jscripts/validations.js";
$pagesubmenuid = 33;

App::LoadModuleClass("TicketManagementCM", "TMGameImport");
App::LoadModuleClass("TicketManagementCM", "TMProducts");
App::LoadModuleClass("TicketManagementCM", "TMGameManagement");
App::LoadModuleClass("TicketManagementCM", "TMAuditLog");
App::LoadModuleClass("TicketManagementCM", "TMWinnings");
App::LoadModuleClass("TicketManagementCM", "TMPrizes");
App::LoadModuleClass("TicketManagementCM", "TMPrizesTemp");
App::LoadModuleClass("TicketManagementCM", "TMProvider");
App::LoadModuleClass("TicketManagementCM", "TMGameBatches");
App::LoadModuleClass("TicketManagementCM", "TMBatchTransactionLog");

App::LoadControl("ComboBox");
App::LoadControl("Button");
App::LoadControl("Hidden");

$tmgameimport = new TMGameImport();
$tmproducts = new TMProducts();
$tmgames = new TMGameManagement();
$auditlog = new TMAuditLog();
$tmwinnings2 = new TMWinnings();
$tmprizes = new TMPrizes();
$tmprizestemp = new TMPrizesTemp();
$tmproviders = new TMProvider();
$tmgamebatches = new TMGameBatches();
$tmbatchtranslog = new TMBatchTransactionLog();

$frmMultipleXMLUpload = new FormsProcessor();

$xmlnaming = App::getParam('xmlnaming');

$hiddenid = new Hidden("hiddenid","hiddenid","Hidden ID");
$hiddenid->Text = "1";
$hiddenUploadedFiles = new Hidden("hiddenUploadedFiles","hiddenUploadedFiles","Hidden Uploaded Files");
$hiddenTag = new Hidden("hiddenTag","hiddenTag","Hidden Tag");
$hiddenSuccessfulUpload = new Hidden("hiddenSuccessfulUpload","hiddenSuccessfulUpload","Hidden Successful Upload");
$hiddenSuccessfulUpload->Text = 0;
$hiddenPrizeCount = new Hidden("hiddenPrizeCount","hiddenPrizeCount","Hidden Prize Count");
$hiddenPrizeCount->Text = "0";
$hiddenVIRNCount = new Hidden("hiddenVIRNCount","hiddenVIRNCount","Hidden VIRN Count");
$hiddenVIRNCount->Text = "0";
$hiddenProviderName = new Hidden("hiddenProviderName","hiddenProviderName","Hidden Provider Name");
$hiddenGameName = new Hidden("hiddenGameName","hiddenGameName","Hidden Game Name");
$hiddenGameNum = new Hidden("hiddenGameNum","hiddenGameNum","Hidden Game Name");
$hiddenfilename = new Hidden("hiddenfilename","hiddenfilename","Hidden file name");
$hiddengameid = new Hidden("hiddengameid","hiddengameid","Hidden gameid");
$hiddenproductid = new Hidden("hiddenproductid","hiddenproductid","Hidden productid");
$hiddenproviderid = new Hidden("hiddenproviderid","hiddenproviderid","Hidden providerid");
$hiddenprovidername = new Hidden("hiddenprovidername","hiddenprovidername","Hidden provider name");
$hiddenprodname = new Hidden("hiddenprodname","hiddenprodname","Hidden prod name");
$hiddengamenum = new Hidden("hiddengamenum","hiddengamenum","Hidden game num");
$hiddengamecode = new Hidden("hiddengamecode","hiddengamecode","Hidden game code");
$hiddenbatchid = new Hidden("hiddenbatchid","hiddenbatchid","Hidden batch id");

$btnConfirmUpload = new Button("btnConfirmUpload","btnConfirmUpload","Yes");
$btnConfirmUpload->IsSubmit = true;
$btnConfirmUpload->Args = "onclick='javascript: return showloadingpage2();'";

$btnDone = new Button("btnDone","btnDone","Upload");
$btnDone->IsSubmit = true;

$btnOkay = new Button("btnOkay","btnOkay","Okay");
$btnOkay->Args = "onclick = 'javascript: return redirecttoMultipleUpload()'";

$providers = $tmproviders->SelectAllProvider();
$ddlProviders = new ComboBox("ddlProviders","ddlProviders","Provider : ");
$ddlProviders->ShowCaption = true;
if(count($providers) > 0)
{
    $providers_list = new ArrayList();
    $providers_list->AddArray($providers);
    $ddlProviders->DataSource = $providers_list;
    $ddlProviders->DataSourceText = "Name";
    $ddlProviders->DataSourceValue = "ProviderID";
    $ddlProviders->DataBind();
	$ddlProviders->SetSelectedValue(1);
}
else
{
    $options = null;
    $options[] = new ListItem("- - - - - - - - - - - - - -","",true);
    $ddlProviders->Items = $options;
}

$frmMultipleXMLUpload->AddControl($hiddenfilename);
$frmMultipleXMLUpload->AddControl($hiddengameid);
$frmMultipleXMLUpload->AddControl($hiddenproductid);
$frmMultipleXMLUpload->AddControl($hiddenproviderid);
$frmMultipleXMLUpload->AddControl($hiddenprovidername);
$frmMultipleXMLUpload->AddControl($hiddenprodname);
$frmMultipleXMLUpload->AddControl($hiddengamenum);
$frmMultipleXMLUpload->AddControl($hiddengamecode);
$frmMultipleXMLUpload->AddControl($hiddenbatchid);
$frmMultipleXMLUpload->AddControl($hiddenid);
$frmMultipleXMLUpload->AddControl($hiddenUploadedFiles);
$frmMultipleXMLUpload->AddControl($hiddenTag);
$frmMultipleXMLUpload->AddControl($hiddenSuccessfulUpload);
$frmMultipleXMLUpload->AddControl($hiddenPrizeCount);
$frmMultipleXMLUpload->AddControl($hiddenVIRNCount);
$frmMultipleXMLUpload->AddControl($hiddenProviderName);
$frmMultipleXMLUpload->AddControl($hiddenGameName);
$frmMultipleXMLUpload->AddControl($hiddenGameNum);
$frmMultipleXMLUpload->AddControl($btnDone);
$frmMultipleXMLUpload->AddControl($ddlProviders);
$frmMultipleXMLUpload->AddControl($btnConfirmUpload);

$frmMultipleXMLUpload->ProcessForms();
$loading = "Uploading Winners XML file.";
if($frmMultipleXMLUpload->IsPostBack)
{
    $filetoupload = $hiddenid->Text;
    $successful_upload = $hiddenSuccessfulUpload->Text;
    
    if(isset($_FILES['file' . $filetoupload]['name']) || isset($_FILES))
    {
        if (!isset($_FILES['file' . $filetoupload]))
        {
            $filetoupload = 1;
        }
        
        if($_FILES['file' . $filetoupload]['type'] == "")
        {
            $errormsg = "File does not exist. Please enter the directory where the file is located.";
            $errormsgtitle = "ERROR!";
        }
        else if($_FILES['file' . $filetoupload]['type'] == "text/xml")
        {
            $filename = $_FILES["file" . $filetoupload]["name"];
            $fullpath = "../xml/winners/" . $filename;
            $filechecking = explode("_", $filename);
            
            if((count($filechecking) < 5) || (count($filechecking) > 5))
            {
                $errormsg = "Incorrect file format. Please upload a valid file format.";
                $errormsgtitle = "ERROR!";
            }
            else
            {
                if((strlen($filechecking[1]) < 5) || (strlen($filechecking[1]) > 5))
                {
                    $errormsg = "Incorrect file format. Please upload a valid file format.";
                    $errormsgtitle = "ERROR!";
                }
                else if((strlen($filechecking[2]) < 4) || (strlen($filechecking[2]) > 4))
                {
                    $errormsg = "Incorrect file format. Please upload a valid file format.";
                    $errormsgtitle = "ERROR!";
                }
                else if((strlen($filechecking[3]) < 4) || (strlen($filechecking[3]) > 4))
                {
                    $errormsg = "Incorrect file format. Please upload a valid file format.";
                    $errormsgtitle = "ERROR!";
                }
                else if(strlen($filechecking[4]) > 7)
                {
                    $errormsg = "Incorrect file format. Please upload a valid file format.";
                    $errormsgtitle = "ERROR!";
                }
                else
                {
                    if($filechecking[1] == "WLIST")
                    {
                        //read xml file
                        libxml_use_internal_errors(true);
                        move_uploaded_file($_FILES["file" . $filetoupload]["tmp_name"], $fullpath);
                        $xml = simplexml_load_file($fullpath);
                        
                        if ($xml)
                        {
                                $xmlname = $xml->getName();
                                $myFile = "../xml/winners/xml_winners_schema.xsd";
                                $fh = fopen($myFile, 'w') or die("can't open file");
                                $str1 = 
                                    "<xs:schema attributeFormDefault=\"unqualified\" elementFormDefault=\"qualified\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\">
                                        <xs:element name=\"$xmlname\">
                                            <xs:complexType>
                                            <xs:sequence>
                                                <xs:element name=\"ticket\" maxOccurs=\"unbounded\" minOccurs=\"0\">
                                                <xs:complexType>
                                                    <xs:sequence>
                                                    <xs:element type=\"xs:string\" name=\"prize_value\"/>
                                                    <xs:element type=\"xs:int\" name=\"VIRN\"/>
                                                    </xs:sequence>
                                                </xs:complexType>
                                                </xs:element>
                                            </xs:sequence>
                                            </xs:complexType>
                                        </xs:element>
                                    </xs:schema>"; 
                                fwrite($fh, $str1);
                                fclose($fh);
                        }
                        
                        //checking for all the contents via xml schema
                        $xdoc = new DomDocument;
                        $xmlschema = "../xml/winners/xml_winners_schema.xsd";
                        //Load the xml document in the DOMDocument object
                        $xdoc->Load($fullpath);
                        //Validate the XML file against the schema
                        if (!$xdoc->schemaValidate($xmlschema))
                        {
                                $errormsgtitle = "ERROR!";
                                $errormsg = "The content of the winners file is invalid.";
                                unlink($myFile);
                        }
                        else
                        {
                                if (!$xml)
                                {
                                    $errors = libxml_get_errors();

                                    foreach ($errors as $error) {
                                        $errormsgtitle = "ERROR!";
                                        $errormsg = display_xml_error($error);
                                    }

                                    libxml_clear_errors();
                                }
                                else
                                {
                                    $xmlUrl = $fullpath;
                                    $xmlStr = file_get_contents($xmlUrl);
                                    $xmlObj = simplexml_load_string($xmlStr);
                                    $arrXml = objectsIntoArray($xmlObj);
                                    $xmlname = $xml->getName();

                                    //checking of ticket node
                                    $ticket_node = $xml->xpath('ticket');
                                    //checking of VIRN node
                                    $virn_node = $xml->xpath('//VIRN');
                                    //checking of prize_value node
                                    $prizeval_node = $xml->xpath('//prize_value');
                                    //checking if there are empty nodes
                                    $emptynodes = in_array_r(Array(), $arrXml);                           

                                    if(!preg_match("/$xmlnaming/", $xmlname))
                                    {
                                        $errormsgtitle = "ERROR!";
                                        $errormsg = "Fatal Error: Missing game number node. <br/> File: $fullpath";
                                    }
                                    else if(count($emptynodes) > 0)
                                    {
                                        $errormsgtitle = "ERROR!";
                                        $errormsg = "Fatal Error: Empty node/s in " . implode(',',$emptynodes) . "<br/> File: $fullpath";
                                    }
                                    else if(count($ticket_node) == 0)
                                    {
                                        $errormsgtitle = "ERROR!";
                                        $errormsg = "Fatal Error: Missing ticket node. <br/> File: $fullpath";
                                    }
                                    else if(count($virn_node) == 0)
                                    {
                                        $errormsgtitle = "ERROR!";
                                        $errormsg = "Fatal Error: Missing VIRN node. <br/> File: $fullpath";
                                    }
                                    else if(count($prizeval_node) == 0)
                                    {
                                        $errormsgtitle = "ERROR!";
                                        $errormsg = "Fatal Error: Missing prize_value node. <br/> File: $fullpath";
                                    }
                                    else if(count($prizeval_node) != count($virn_node))
                                    {
                                        $errormsgtitle = "ERROR!";
                                        $errormsg = "Fatal Error: VIRN node and prize_value node do not match. <br/> File: $fullpath";
                                    }
                                    else if(count($ticket_node) != count($virn_node) || count($ticket_node) != count($prizeval_node) || count($virn_node) != count($prizeval_node))
                                    {
                                        $errormsgtitle = "ERROR!";
                                        $errormsg = "Fatal Error: Ticket node and VIRN node and prize_value node do not match. <br/> File: $fullpath";
                                    }
                                    else
                                    {
                                        //checking if gamenumber exists
                                        $gameno = explode("_" , $filename);
                                        if($gameno[0] == $xmlname)
                                        {
                                            $providerid = $ddlProviders->SelectedValue;
                                            $gameinfo = $tmgames->SelectGameDtlsByWhere(substr($gameno[0], 2, strlen($gameno[0])), $providerid);

                                            if(count($gameinfo) > 1)
                                            {
                                                $errormsgtitle = "ERROR!";
                                                $errormsg = "Fatal Error: Game number has multiple instances. <br/> File: $fullpath";
                                            }
                                            else if(count($gameinfo) == 1)
                                            {
                                                $gameid = $gameinfo[0]["GameID"];
                                                $productgameinfo = $tmproducts->GetProductNameGameNum($gameid);
                                                $providername = $tmproviders->SelectByID($providerid);
                                                $productid = $productgameinfo[0]["ProductID"];
                                                $gamecode = $gameno[0];
                                                if($productid != "")
                                                {
                                                    //checking if the provider assigned to game number is the same as he chosen provider
                                                    if($productgameinfo[0]["ProviderID"] != $providerid)
                                                    {
                                                        $errormsgtitle = "ERROR!";
                                                        $errormsg = "Provider assigned to game number do not match chosen provider.";
                                                    }
                                                    else
                                                    {
                                                        //generate batch id
                                                        $batch = $tmgamebatches->SelectBatchID($gameid,2);
                                                        if(count($batch) > 0)
                                                        {
                                                            $batches = $tmgamebatches->SelectMaxBatchID($gameid,2);
                                                            $gamebatchnum = ($batches[0]["maxbatchid"] + 1);
                                                        }
                                                        else
                                                        {
                                                            $gamebatchnum = 1;
                                                        }

                                                        $checkinventoryfile = $tmgamebatches->CheckInventoryUpload($gameid, $gamebatchnum, 1);
                                                        if($checkinventoryfile[0]["gamebatch"] > 0)
                                                        {
                                                                list($prize,$virn,$msgcode,$msg,$msgtitle) = UploadWinnersXmlFile($filename,$gameid,$productid,$providerid,$arrXml,$xmlname,$providername[0]["Name"],$productgameinfo[0]["ProductName"],$productgameinfo[0]["GameNumber"],$gamecode,$gamebatchnum, $xmlnaming);
                                                                if ($msgcode == 0)
                                                                {                                                   
                                                                        $prizecount = ($hiddenPrizeCount->Text + $prize);
                                                                        $virncount = ($hiddenVIRNCount->Text + $virn);
                                                                        $hiddenPrizeCount->Text = $prizecount;
                                                                        $hiddenVIRNCount->Text = $virncount;

                                                                        $hiddenGameNum->Text = $productgameinfo[0]["GameNumber"];
                                                                        $hiddenProviderName->Text = $providername[0]["Name"];
                                                                        $hiddenGameName->Text = $productgameinfo[0]["ProductName"];

                                                                        $hiddenTag->Text = 1;
                                                                        $files_uploaded = $hiddenUploadedFiles->Text;
                                                                        $files_uploaded = $files_uploaded . $_FILES['file' . $filetoupload]['name'] . "<br/>";
                                                                        $hiddenUploadedFiles->Text = $files_uploaded;

                                                                        $successful_upload++;
                                                                        $hiddenSuccessfulUpload->Text = $successful_upload;                                                               
                                                                } 
                                                                else 
                                                                {
                                                                        $errormsg = $msg;
                                                                        $errormsgtitle = $msgtitle;
                                                                }        
                                                        } 
                                                        else
                                                        {
                                                                $errormsgtitle = "ERROR!";
                                                                $errormsg = "Kindly upload the inventory file first before proceeding.";
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                        $errormsgtitle = "ERROR!";
                                                        $errormsg = "Product id do not exists.";
                                                }
                                            }
                                            else
                                            {
                                                $errormsg = "Game number does not exist. Please ensure that the filename is correct.";
                                                $errormsgtitle = "ERROR!";
                                            }
                                        }
                                        else
                                        {
                                            $errormsg = "Root node and game number in the file name do not match.";
                                            $errormsgtitle = "ERROR!";
                                        }

                                    }
                                }
                        }
                    }
                }
            }
        }
        else
        {
            $errormsg = "Incorrect file format. Please upload a valid file format.";
            $errormsgtitle = "ERROR!";
        }
    }

    if($btnDone->SubmittedValue == "Upload")
    {   
        if ($hiddenVIRNCount->SubmittedValue > 0)
        {
            $counttitle = "Winners File Summary";
            $countmsg = "Provider : " . $hiddenProviderName->SubmittedValue . "<br/>" .
                        "Game Name : " . $hiddenGameName->SubmittedValue . "<br/>" .
                        "Game Number : " . $hiddenGameNum->SubmittedValue. "<br/>" .
                        "Total No. of Prizes : " . number_format((int)$hiddenPrizeCount->SubmittedValue) . "<br/>" .
                        "Total No. of VIRN : " . number_format((int)$hiddenVIRNCount->SubmittedValue);  
        }
    }  
}
//function for checking if xml array have empty nodes
function in_array_r($needle, $haystack) {
    $emptynodeskey = array();
    foreach ($haystack as $key => $item) {
        if ($item === $needle || (is_array($item) && in_array_r($needle, $item))) {
            $emptynodeskey[] = $key;

        }
    }
    //return false;
    return $emptynodeskey;
}

//function for checking xml structure
function display_xml_error($error)
{
    $return = "";
    //$return .= $error->column . "\n";

    switch ($error->level) {
        case LIBXML_ERR_WARNING:
            $return .= "Warning $error->code: ";
            break;
         case LIBXML_ERR_ERROR:
            $return .= "Error $error->code: ";
            break;
        case LIBXML_ERR_FATAL:
            $return .= "Fatal Error $error->code: ";
            break;
    }

    $return .= trim($error->message) .
               "<br/>  Line: $error->line" .
               "<br/>  Column: $error->column";

    if ($error->file) {
        $return .= "</br>  File: $error->file";
    }

    return addslashes("$return<br/>");
}

//function for converting xml to array
function objectsIntoArray($arrObjData, $arrSkipIndices = array())
{
    $arrData = array();

    // if input is object, convert into array
    if (is_object($arrObjData)) {
        $arrObjData = get_object_vars($arrObjData);
    }

    if (is_array($arrObjData)) {
        foreach ($arrObjData as $index => $value) {
            if (is_object($value) || is_array($value)) {
                $value = objectsIntoArray($value, $arrSkipIndices); // recursive call
            }
            if (in_array($index, $arrSkipIndices)) {
                continue;
            }
            $arrData[$index] = $value;
        }
    }
    return $arrData;
}

//function in parsing xml data
function UploadWinnersXmlFile($filename,$gameid,$productid,$providerid,$arrXml,$xmlname,$providername,$productname,$gamenum,$gamecode,$gamebatchnum, $xmlnaming)
{
    $tmgameimport = new TMGameImport();
    $tmproducts = new TMProducts();
    $tmgames = new TMGameManagement();
    $auditlog = new TMAuditLog();
    $tmwinnings2 = new TMWinnings();
    $tmprizes = new TMPrizes();
    $tmprizestemp = new TMPrizesTemp();
    $tmproviders = new TMProvider();
    $tmgamebatches = new TMGameBatches();
    $tmbatchtranslog = new TMBatchTransactionLog();
    $totalprizescount = 0;
    $deletewinningsaffectedrow = 0;
    $deleteprizesaffectedrow = 0;
    $totalwinningscount = 0;
    $msgcode = 0;

    /* STEP 1:
     * Insert game batch id
     */
    $gamebatch["GameID"] = $gameid;
    $gamebatch["BatchID"] = $gamebatchnum;
    $gamebatch["XMLType"] = 2;
    $tmgamebatches->Insert($gamebatch);
    $gamebatchid = $tmgamebatches->LastInsertID;

    /*Added by Arlene R. Salazar 03/08/2012*/
    $fullpath = "../xml/winners/" . $filename;
    $filename_parts = explode(".",$filename);
    $filename_1 = $filename_parts[0] . "_" . $gamebatchid;
    $filename_2 = $filename_parts[1];
    $new_filename = $filename_1 . "." . $filename_2;
    $new_fullpath = "../xml/winners/" . $new_filename;
    rename($fullpath,$new_fullpath);

    /* STEP 2:
     * insert uploaded xml in the table
     */
    $tmimport["ProviderID"] = $providerid;
    $tmimport["GameID"] = $gameid;
    $tmimport["FileName"] = $filename;
    $tmimport["FilePath"] = $fullpath;
    $tmimport["ProductID"] = $productid;
    $tmimport["DateUploaded"] = 'now_usec()';
    $tmimport["Status"] = '0';
    $tmgameimport->Insert($tmimport);
    $gameimportid = $tmgameimport->LastInsertID;
    if ($tmgameimport->HasError)
    {
        $errormsg = $tmgameimport->getError();
        $errormsgtitle = "ERROR!";
        $msgcode = 1;
    }
    $id = $gameimportid;

    /* STEP 3:
     * insert audit trail log
     */
    $auditdtls["SessionID"] = $_SESSION['sid'];
    $auditdtls["AID"] = $_SESSION['acctid'];
    $auditdtls["TransDetails"] = "GameImport ID: " . $tmgameimport->LastInsertID;
    $auditdtls["TransDateTime"] = "now_usec()";
    $auditdtls["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
    $auditdtls["AuditTrailFunctionID"] = 26;
    $insertauditlog = $auditlog->Insert($auditdtls);
    if($auditlog->HasError)
    {
        $errormsgtitle = "ERROR!";
        $errormsg = "Error has occured:" . $tmaccount->getError();
        $msgcode = 1;
    }
    
    $errormsg = $filename. " has been successfully uploaded";
    $errormsgtitle = "UPLOAD SUCCESSFUL";
    $imax1 = 0;
    if($xmlname == "$gamecode")
    {
        $xml_content = $arrXml["ticket"];
        $csvcontent = '';
        do{
            $winnersdtls = null;
            for($xml = 0 ; $xml < count($arrXml) ; $xml++)
            {
                for($i = 0 ; $i < count($arrXml["ticket"]) ; $i++)
                {
                    $winners["ProductID"] = $productid;
                    $winners["AccountAID"] = $_SESSION["acctid"];
                    $winners["Description"] = trim($xml_content[$i]["prize_value"]);
                    $winners["VIRN"] = trim($xml_content[$i]["VIRN"]);
                    $winners["IsExists"] = "0";
                    $winners["GameNumber"] = substr($gamecode, 2, strlen($gamecode));
                    $winners["GameBatchID"] = $gamebatchid;
                    $csvcontent .= implode(";", $winners) . "<br>";
                    $winnersdtls++;
                }
            }
            $imax1++;
                
            if ($imax1 > 2)
            {
                    $status = 2;
                    break;
            }
            else
            {
                    $status = 1;
            }
            } while($winnersdtls != count($xml_content));
            $imax = 0;
            $winnersdtls_count = $winnersdtls;
            if($status != 2)
            {
                do{
                    /* STEP 4:
                     * delete from prizetemp where AccountAID == $_SESSION["acctid"]
                     * insert into prizetemp
                     */
                    $tmprizestemp->TruncatePrizesTemp();
                    
                    //insert into CSV file
                    $fp = fopen('../csv/winners.csv', 'w');
                    fwrite($fp,$csvcontent);
                    fclose($fp);
                    $csvcontent = null;
                    
                    //insert to inventorytemp using csv file
                    $tmprizestemp->InsertWinnersUsingCSVFile('../csv/winners.csv', ';', '<br>');

                    $isVIRNExist = $tmprizestemp->CheckDuplicateVIRN($gameid);
                    if ($isVIRNExist[0]["count"] > 0)
                    {
                        $msgcode = 2;
                    } else {
                        
                    /* STEP 5:
                     * check if prize exists
                     */
                    $tmprizestemp->CheckIfPrizeAlreadyExist($productid);
                    //App::Pr($tmprizestemp->AffectedRows);

                    /* Step 6:
                     * Get all consolation prizes that does not yet exists and insert to prizes
                     */
                    $tmprizes->InsertNewConsolationPrizes($productid,$_SESSION['acctid'],$winnersdtls_count,$gamebatchid,$xmlnaming);
                    if($tmwinnings2->HasError)
                    {
                        $errormsgtitle = "ERROR!";
                        $errormsg = "Error while inserting winning cards: " . $tmwinnings2->getError();
                        $msgcode = 1;
                    }
                    $firstprizebatch = $tmprizes->AffectedRows;

                    /* Step 7:
                     * Get all jackpot prizes that does not yet exists and insert to prizes
                     */
                    $secondprizebatch = 0;
                        
                    /* Step 8:
                     * Insert to winning cards all prizes on step 3 and 4
                     */
                    $tmwinnings2->InsertWinnings(0,$productid,$_SESSION['acctid'],$winnersdtls_count,$gamebatchid);
                    if($tmwinnings2->HasError)
                    {
                        $errormsgtitle = "ERROR!";
                        $errormsg = "Error while inserting winning cards: " . $tmwinnings2->getError();
                        $msgcode = 1;
                    }
                    $firstbatch = $tmwinnings2->AffectedRows;

                    /* Step 9:
                     * Insert to winning cards all existing prizes
                     */
                    $tmwinnings2->InsertWinnings(1,$productid,$_SESSION['acctid'],$winnersdtls_count,$gamebatchid);
                    if($tmwinnings2->HasError)
                    {
                        $errormsgtitle = "ERROR!";
                        $errormsg = "Error while inserting winning cards: " . $tmwinnings2->getError();
                        $msgcode = 1;
                    }
                    $secondbatch = $tmwinnings2->AffectedRows;

                    /* Step 10:
                     * Delete from prizetemp|| ($imax < 3)
                     */
                    $tmprizestemp->TruncatePrizesTemp();

                    /* Step 11:
                     * Delete all inserted winningcards and prizes if total inserted is not equal to xml count
                     */
                    //count of all winnings uploaded
                    $totalwinningscount = ($firstbatch + $secondbatch);
                    $lastrecord = $tmwinnings2->GetLastWinningCardID();
                    
                    //count of all prizes uploaded
                    $totalprizescount = ($firstprizebatch + $secondprizebatch);
                    $lastprizerecord = $tmprizes->GetLastWinningCardID();
				
                    if($totalwinningscount != count($xml_content))
                    {
                        if($lastrecord[0]["LastID"] != NULL)
                        {
                            $start = ($lastrecord[0]["LastID"] - $totalwinningscount);
                            $deletealltransactions = $tmwinnings2->DeleteAllTransactions($start,$lastrecord[0]["LastID"]);
                            $deletewinningsaffectedrow = ($deletewinningsaffectedrow + $tmwinnings2->AffectedRows);
                        }

                        if($lastprizerecord[0]["LastID"] != NULL)
                        {
                            $start = ($lastprizerecord[0]["LastID"] - $totalprizescount);
                            $deletealltransactions = $tmprizes->DeleteAllTransactions($start,$lastprizerecord[0]["LastID"]);
                            $deleteprizesaffectedrow = ($deleteprizesaffectedrow + $tmprizes->AffectedRows);
                        }

                        //delete game batches info
                        $tmgamebatches->DeleteByGameBatchesID($gamebatchid);
                        $gamebatchid = 0;
                    }
                    $imax++;
		
                    if ($imax > 2)
                    {
                            $status = 2;
                            break;
                    }
                    else
                    {
                            $status = 1;
                    }
                }
                }while($totalwinningscount != count($xml_content));
            }


            if ($msgcode != 2)
            {
                $xmlstatus["Status"] = $status;
                $xmlstatus["DateImported"] = "now_usec()";
                $xmlstatus["GameImportID"] = $id;
                $xmlstatus["GameBatchID"] = $gamebatchid;
                $updatestatus = $tmgameimport->UpdateByArray($xmlstatus);
                if($tmgameimport->HasError)
                {
                        $errormsgtitle = "ERROR!";
                        $errormsg = "Error:" . $tmgameimport->getError();
                }

                /* Step 12:
                 * Insert to audit trail log
                 */
                $auditdtls["SessionID"] = session_id();
                $auditdtls["AID"] = $_SESSION["acctid"]; //since this will be executed by a cron job, there will be no $_SESSION['acctid'] created
                $auditdtls["TransDetails"] = "GameImport ID: " . $id;
                $auditdtls["TransDateTime"] = "now_usec()";
                $auditdtls["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
                $auditdtls["AuditTrailFunctionID"] = 27;
                $insertauditlog = $auditlog->Insert($auditdtls);
                if($auditlog->HasError)
                {
                        $errormsgtitle = "ERROR!";
                        $errormsg = "Error has occured:" . $tmaccount->getError();
                        $msgcode = 1;
                }

                /* Step 13:
                * Insert to batch transaction log
                */
                $batchtrans["GameID"] = $gameid;
                $batchtrans["BatchID"] = $gamebatchnum;
                $batchtrans["FileName"] = $filename;
                $batchtrans["TransactionDate"] = "now_usec()";
                $batchtrans["TransactionType"] = 1;
                $batchtrans["ProcessedByAID"] = $_SESSION["acctid"];
                $batchtrans["Status"] = $status;
                $tmbatchtranslog->Insert($batchtrans);
                if ($tmbatchtranslog->HasError) {
                    $errormsgtitle = "ERROR!";
                    $errormsg = "Error has occured:" . $tmbatchtranslog->getError();
                    $msgcode = 1;
                }
            }
           
            if($status != 2 && $msgcode != 2)
            {
                $counttitle = "Winners File Summary";
                $countmsg = "Provider : " . $providername . "<br/>" .
                            "Game Name : " . $productname . "<br/>" .
                            "Game Number : " . $gamenum. "<br/>" .
                            "Total No. of Prizes : " . number_format($totalprizescount) . "<br/>" .
                            "Total No. of VIRN : " . number_format($totalvirn);
            }
            else
            {
                if ($msgcode == 2)
                {
                    $errormsgtitle = "ERROR!";
                    $errormsg = "Duplicate entry of VIRN not allowed.";
                } else {
                    $errormsgtitle = "ERROR!";
                    $errormsg = "Uploading was unsuccessful . Please try again.";
                }
            }
	}
	else
	{
            $errormsgtitle = "ERROR!";
            $errormsg = $filename . " could not be processed.";
	}
	/*End of parse data*/

	if($msgcode == 0)
        {
            $msgtitle = $counttitle;
            $msg = $countmsg;
        }
        else
        {
            $msgtitle = $errormsgtitle;
            $msg = $errormsg;
        }
        
        return array($totalprizescount, $totalwinningscount, $msgcode, $msg, $msgtitle);
}
?>