<?php
/*
 * @author 
 * Purpose   : controller for winnersxmlupload
 */
ignore_user_abort(true);                                                        //Ignore user aborts and force the script running until the last part
set_time_limit(0);                                                              //Limits the maximum execution time , if set to 0 no maximum time is imposed. Safe mode should be turned off.

$stylesheets[] = "css/default.css";
$javascripts[] = "jscripts/validations.js";
$pagesubmenuid = 33;

App::LoadModuleClass("TicketManagementCM", "TMGameImport");
App::LoadModuleClass("TicketManagementCM", "TMProducts");
App::LoadModuleClass("TicketManagementCM", "TMGameManagement");
App::LoadModuleClass("TicketManagementCM", "TMAuditLog");
App::LoadModuleClass("TicketManagementCM", "TMWinnings");
//App::LoadModuleClass("TicketManagementCM", "TMPrizes");
App::LoadModuleClass("TicketManagementCM", "TMPrizes");
App::LoadModuleClass("TicketManagementCM", "TMPrizesTemp");
App::LoadModuleClass("TicketManagementCM", "TMProvider");
App::LoadModuleClass("TicketManagementCM", "TMGameBatches");
App::LoadModuleClass("TicketManagementCM", "TMBatchTransactionLog");

App::LoadControl("ComboBox");
App::LoadControl("Button");
App::LoadControl("Hidden");

$tmgameimport = new TMGameImport();
$tmproducts = new TMProducts();
$tmgames = new TMGameManagement();
$auditlog = new TMAuditLog();
$tmwinnings2 = new TMWinnings();
$tmprizes = new TMPrizes();
$tmprizestemp = new TMPrizesTemp();
$tmproviders = new TMProvider();
$tmgamebatches = new TMGameBatches();
$tmbatchtranslog = new TMBatchTransactionLog();

$frmWinnerXmlUpload = new FormsProcessor();

/*$where = " ORDER BY ProductName";
$products = $tmproducts->SelectByWhere($where);
$products_list = new ArrayList();
$products_list->AddArray($products);

$ddlProducts = new ComboBox("ddlProducts","ddlProducts","Products: ");
$ddlProducts->ShowCaption = true;
$ddlProducts->DataSource = $products_list;
$ddlProducts->DataSourceText = "ProductName";
$ddlProducts->DataSourceValue = "ProductID";
$ddlProducts->DataBind();*/

$where = " WHERE Status = 1 ORDER BY Name";
$providers = $tmproviders->SelectAllProvider();

$ddlProviders = new ComboBox("ddlProviders","ddlProviders","Provider: ");
$ddlProviders->ShowCaption = true;
if(count($providers) > 0)
{
    $providers_list = new ArrayList();
    $providers_list->AddArray($providers);
    $ddlProviders->DataSource = $providers_list;
    $ddlProviders->DataSourceText = "Name";
    $ddlProviders->DataSourceValue = "ProviderID";
    $ddlProviders->DataBind();
	$ddlProviders->SetSelectedValue(1);
}
else
{
    $options = null;
    $options[] = new ListItem("- - - - - - - - - - - - - -","",true);
    $ddlProviders->Items = $options;
}


$btnUpload = new Button("btnUpload","btnUpload","Upload");
$btnUpload->IsSubmit = true;
$btnUpload->Args = "onclick='javascript: return showloadingpage();'";

$btnConfirmUpload = new Button("btnConfirmUpload","btnConfirmUpload","Yes");
$btnConfirmUpload->IsSubmit = true;
$btnConfirmUpload->Args = "onclick='javascript: return showloadingpage2();'";

$hiddenfilename = new Hidden("hiddenfilename","hiddenfilename","Hidden file name");
$hiddengameid = new Hidden("hiddengameid","hiddengameid","Hidden gameid");
$hiddenproductid = new Hidden("hiddenproductid","hiddenproductid","Hidden productid");
$hiddenproviderid = new Hidden("hiddenproviderid","hiddenproviderid","Hidden providerid");
$hiddenprovidername = new Hidden("hiddenprovidername","hiddenprovidername","Hidden provider name");
$hiddenprodname = new Hidden("hiddenprodname","hiddenprodname","Hidden prod name");
$hiddengamenum = new Hidden("hiddengamenum","hiddengamenum","Hidden game num");
$hiddengamecode = new Hidden("hiddengamecode","hiddengamecode","Hidden game code");
$hiddenbatchid = new Hidden("hiddenbatchid","hiddenbatchid","Hidden batch id");

//$frmWinnerXmlUpload->AddControl($ddlProducts);
$frmWinnerXmlUpload->AddControl($btnUpload);
$frmWinnerXmlUpload->AddControl($ddlProviders);
$frmWinnerXmlUpload->AddControl($btnConfirmUpload);
$frmWinnerXmlUpload->AddControl($hiddenfilename);
$frmWinnerXmlUpload->AddControl($hiddengameid);
$frmWinnerXmlUpload->AddControl($hiddenproductid);
$frmWinnerXmlUpload->AddControl($hiddenproviderid);
$frmWinnerXmlUpload->AddControl($hiddenprovidername);
$frmWinnerXmlUpload->AddControl($hiddenprodname);
$frmWinnerXmlUpload->AddControl($hiddengamenum);
$frmWinnerXmlUpload->AddControl($hiddengamecode);
$frmWinnerXmlUpload->AddControl($hiddenbatchid);

$frmWinnerXmlUpload->ProcessForms();

if($frmWinnerXmlUpload->IsPostBack)
{
	if($btnUpload->SubmittedValue == "Upload")
    {
		if($_FILES['file']['type'] == "")
		{
		    $errormsg = "Please specify file.";
		    $errormsgtitle = "ERROR!";
		}
		else if (($_FILES['file']['type'] == "text/xml"))
		{
			if ($_FILES["file"]["error"] > 0)
		    {
		        $errormsg = "Return Code: " . $_FILES["file"]["error"] . "<br />";
		        $errormsgtitle = "ERROR!";
		    }
		    else
		    {
				$filename = $_FILES["file"]["name"];
		        //$fullpath = "../xml/winners/" . $_FILES["file"]["name"];
				$fullpath = "../xml/winners/" . $_FILES["file"]["name"];

		        // check if file exists
		        $fileExixts = $tmgameimport->CheckFile($fullpath);
		        //if (file_exists($fullpath))
				/* Removed by ARS 12-29-2011
		         * Purpose: To allow the uploading of xml files with the same game number
		         */
		        /*if(count($fileExixts) > 0)
		        {
		            $errormsg = "File was already imported. Please enter a new batch of winners file.";
		            //$errormsg = $filename . " already exists. ";
		            $errormsgtitle = "ERROR!";
		        }
		        else
		        {*/
					// checking if the corect file is being uploaded
		            $filechecking = explode("_", $filename);
		            
						//checking if filename is composed of 4 underscores
		                if((count($filechecking) < 4) || (count($filechecking) > 4))
		                {
		                    $errormsg = "Incorrect file format. Please upload a valid file format.";
		                    $errormsgtitle = "ERROR!";
		                }
		                else
		                {
		                    /* Removed by ARS 12-28-2011 Purpose: To allow the uploading of xml files with more than 3 game number in the filename
							if((strlen($filechecking[0]) < 5) || (strlen($filechecking[0]) > 5))
		                    {
		                        $errormsg = "Incorrect file format. Please upload a valid file format.";
		                        $errormsgtitle = "ERROR!";
		                    }
		                    else*/ if((strlen($filechecking[1]) < 5) || (strlen($filechecking[1]) > 5))
		                    {
		                        $errormsg = "Incorrect file format. Please upload a valid file format.";
		                        $errormsgtitle = "ERROR!";
		                    }
		                    else if((strlen($filechecking[2]) < 4) || (strlen($filechecking[2]) > 4))
		                    {
		                        $errormsg = "Incorrect file format. Please upload a valid file format.";
		                        $errormsgtitle = "ERROR!";
		                    }
		                    else if((strlen($filechecking[3]) < 8) || (strlen($filechecking[3]) > 8))
		                    {
		                        $errormsg = "Incorrect file format. Please upload a valid file format.";
		                        $errormsgtitle = "ERROR!";
		                    }
		                    else
		                    {
								if($filechecking[1] == "WLIST")
		            			{
									//read xml file
		                        libxml_use_internal_errors(true);
		                        move_uploaded_file($_FILES["file"]["tmp_name"], $fullpath);
		                        $xml = simplexml_load_file($fullpath);

		                        //checking if xml structure is correct
		                        if (!$xml) {
		                            $errors = libxml_get_errors();

		                            foreach ($errors as $error) {
		                                $errormsgtitle = "ERROR!";
		                                $errormsg = display_xml_error($error);
		                            }

		                            libxml_clear_errors();
		                        }
		                        else
		                        {
		                            $xmlUrl = $fullpath;
		                            $xmlStr = file_get_contents($xmlUrl);
		                            $xmlObj = simplexml_load_string($xmlStr);
									$arrXml = objectsIntoArray($xmlObj);
									$xmlname = $xml->getName();

		                            //checking of ticket node
		                            $ticket_node = $xml->xpath('ticket');
		                            //checking of VIRN node
		                            $virn_node = $xml->xpath('//VIRN');
		                            //checking of prize_value node
		                            $prizeval_node = $xml->xpath('//prize_value');
									//checking if there are empty nodes
		                            $emptynodes = in_array_r(Array(), $arrXml);

									if(!preg_match("/FT/", $xmlname))
		                            {
		                                $errormsgtitle = "ERROR!";
		                                $errormsg = "Fatal Error: Missing game number node. <br/> File: $fullpath";
		                            }
									else if(count($emptynodes) > 0)
		                            {
		                                $errormsgtitle = "ERROR!";
		                                $errormsg = "Fatal Error: Empty node/s in " . implode(',',$emptynodes) . "<br/> File: $fullpath";
		                            }
		                            else if(count($ticket_node) == 0)
		                            {
		                                $errormsgtitle = "ERROR!";
		                                $errormsg = "Fatal Error: Missing ticket node. <br/> File: $fullpath";
		                            }
		                            else if(count($virn_node) == 0)
		                            {
		                                $errormsgtitle = "ERROR!";
		                                $errormsg = "Fatal Error: Missing VIRN node. <br/> File: $fullpath";
		                            }
		                            else if(count($prizeval_node) == 0)
		                            {
		                                $errormsgtitle = "ERROR!";
		                                $errormsg = "Fatal Error: Missing prize_value node. <br/> File: $fullpath";
		                            }
		                            else if(count($prizeval_node) != count($virn_node))
		                            {
		                                $errormsgtitle = "ERROR!";
		                                $errormsg = "Fatal Error: VIRN node and prize_value node do not match. <br/> File: $fullpath";
		                            }
		                            else if(count($ticket_node) != count($virn_node) || count($ticket_node) != count($prizeval_node) || count($virn_node) != count($prizeval_node))
		                            {
		                                $errormsgtitle = "ERROR!";
		                                $errormsg = "Fatal Error: Ticket node and VIRN node and prize_value node do not match. <br/> File: $fullpath";
		                            }
		                            else
		                            {
										//==================
										//checking if gamenumber exists
										$gameno = explode("_" , $filename);
										if($gameno[0] == $xmlname)
		                                {
											$providerid = $ddlProviders->SelectedValue;
											//$gameinfo = $tmgames->SelectGameDtlsByWhere(substr($gameno[0] , 2 , 3),$providerid);
											$gameinfo = $tmgames->SelectGameDtlsByWhere(substr($gameno[0], 2, strlen($gameno[0])), $providerid);

											if(count($gameinfo) > 1)
											{
												$errormsgtitle = "ERROR!";
				                            	$errormsg = "Fatal Error: Game number has multiple instances. <br/> File: $fullpath";
											}
											else if(count($gameinfo) == 1)
											{
										
												//$gameno = substr($filename, 0,strpos($filename,'_'));
												//$gameno = explode("_", $filename);
												//$where = " WHERE GameNumber = " . substr($gameno[0] , 2 , 3);
												//$gameid = $tmgames->SelectByWhere($where);

												//header("Location: ../views/winnersxmlupload.php");

												/*Parse data*/
										
												//$productid = $ddlProducts->SelectedValue;
												$gameid = $gameinfo[0]["GameID"];
												$productgameinfo = $tmproducts->GetProductNameGameNum($gameid);
												$providername = $tmproviders->SelectByID($providerid);
												$productid = $productgameinfo[0]["ProductID"];

												//read xml file
												$gamecode = $gameno[0];
												//$xml = simplexml_load_file($fullpath);
												//$xmlUrl = $fullpath;
												//$xmlStr = file_get_contents($xmlUrl);
												//$xmlObj = simplexml_load_string($xmlStr);
										
												//======================
												// check if game id is already uploaded
												$isUploaded = $tmgameimport->CheckIfGameIdIsUploaded($gameid,"winners");
												/* Removed by ARS 12-29-2011
				                                 * Purpose: To allow the uploading of xml files with the same game number
				                                 */
												/*if(count($isUploaded) == 0)
												{*/
													if($productid != "")
													{
														//checking if the provider assigned to game number is the same as he chosen provider
				                                        if($productgameinfo[0]["ProviderID"] != $providerid)
				                                        {
				                                            $errormsgtitle = "ERROR!";
				                                            $errormsg = "Provider assigned to game number do not match chosen provider.";
				                                        }
				                                        else
				                                        {
															//generate batch id
                                                            $batch = $tmgamebatches->SelectBatchID($gameid,2);
                                                            if(count($batch) > 0)
                                                            {
                                                                $batches = $tmgamebatches->SelectMaxBatchID($gameid,2);
                                                                $gamebatchnum = ($batches[0]["maxbatchid"] + 1);
                                                            }
                                                            else
                                                            {
                                                                $gamebatchnum = 1;
                                                            }
                                                           
                                                            $checkinventoryfile = $tmgamebatches->CheckInventoryUpload($gameid, $gamebatchnum, 1);
                                                            if($checkinventoryfile[0]["gamebatch"] > 0)
                                                            {
                                                                $hiddenfilename->Text = $filename;
                                                                $hiddengameid->Text = $gameid;
                                                                $hiddenproductid->Text = $productid;
                                                                $hiddenproviderid->Text = $providerid;
                                                                $hiddenprodname->Text = $productgameinfo[0]["ProductName"];
                                                                $hiddenprovidername->Text = $providername[0]["Name"];
                                                                $hiddengamenum->Text = $productgameinfo[0]["GameNumber"];
                                                                $hiddengamecode->Text = $gamecode;
                                                                $hiddenbatchid->Text = $gamebatchnum;
                                                                if (count($isUploaded) > 0)
                                                                {
                                                                    $confirmuploadmsg = "Are you sure you want to upload another file for this game number?";
                                                                    $confirmuploadtitle = "CONFIRMATION";
                                                                }
                                                                else
                                                                {
                                                                    list($msgtitle,$msg) = UploadWinnersXmlFile($filename,$gameid,$productid,$providerid,$arrXml,$xmlname,$providername[0]["Name"],$productgameinfo[0]["ProductName"],$productgameinfo[0]["GameNumber"],$gamecode,$gamebatchnum);
                                                                    $countmsgtitle = $msgtitle;
                                                                    $countmsg = $msg;
                                                                }
                                                            }
                                                            else
                                                            {
                                                                $errormsgtitle = "ERROR!";
                                                                $errormsg = "Kindly upload the inventory file first before proceeding.";
                                                            }
														}
													}
													else
													{
														$errormsgtitle = "ERROR!";
														$errormsg = "Product id do not exists.";
													}
												/*}
												else
												{
													$errormsgtitle = "ERROR!";
													$errormsg = "You cannot proceed because winning cards were already uploaded for this game number.";
												}*/
												//=============================
											}
											else
											{
												$errormsg = "Game number does not exist. Please ensure that the filename is correct.";
												$errormsgtitle = "ERROR!";
											}
											//=====================DITO
										}
				                        else
				                        {
				                            $errormsg = "Root node and game number in the file name do not match.";
				                            $errormsgtitle = "ERROR!";
				                        }
									}
								}
							}
							else
							{
								$errormsg = "Incorrect file name format. Please upload a valid file name format.";
								//$errormsg = "I suppose you are uploading a wrong file.";
								$errormsgtitle = "ERROR!";
							} 
						}
		            } 
				//}
			}
		}
		else
		{
			$errormsg = "Incorrect file format. Please upload a valid file format.";
		    $errormsgtitle = "ERROR!";
		}
	}

    if($btnConfirmUpload->SubmittedValue == "Yes")
    {
        $fullpath = "../xml/winners/" . $hiddenfilename->SubmittedValue;
        $xmlUrl = $fullpath;
        $xml = simplexml_load_file($fullpath);
        $xmlStr = file_get_contents($xmlUrl);
        $xmlObj = simplexml_load_string($xmlStr);
        $arrXml = objectsIntoArray($xmlObj);
        $xmlname = $xml->getName();

        list($msgtitle,$msg) = UploadWinnersXmlFile($hiddenfilename->SubmittedValue,$hiddengameid->SubmittedValue,$hiddenproductid->SubmittedValue,$hiddenproviderid->SubmittedValue,$arrXml,$xmlname,$hiddenprovidername->SubmittedValue,$hiddenprodname->SubmittedValue,$hiddengamenum->SubmittedValue,$hiddengamecode->SubmittedValue,$hiddenbatchid->SubmittedValue);
        $countmsgtitle = $msgtitle;
        $countmsg = $msg;
    }
}

//function for checking if xml array have empty nodes
function in_array_r($needle, $haystack) {
    $emptynodeskey = array();
    foreach ($haystack as $key => $item) {
        if ($item === $needle || (is_array($item) && in_array_r($needle, $item))) {
            //return true;
            $emptynodeskey[] = $key;

        }
    }
    //return false;
    return $emptynodeskey;
}

//function for checking xml structure
function display_xml_error($error)
{
    $return = "";
    //$return .= $error->column . "\n";

    switch ($error->level) {
        case LIBXML_ERR_WARNING:
            $return .= "Warning $error->code: ";
            break;
         case LIBXML_ERR_ERROR:
            $return .= "Error $error->code: ";
            break;
        case LIBXML_ERR_FATAL:
            $return .= "Fatal Error $error->code: ";
            break;
    }

    $return .= trim($error->message) .
               "<br/>  Line: $error->line" .
               "<br/>  Column: $error->column";

    if ($error->file) {
        $return .= "</br>  File: $error->file";
    }

    return addslashes("$return<br/>");
}

//function for converting xml to array
function objectsIntoArray($arrObjData, $arrSkipIndices = array())
{
    $arrData = array();

    // if input is object, convert into array
    if (is_object($arrObjData)) {
        $arrObjData = get_object_vars($arrObjData);
    }

    if (is_array($arrObjData)) {
        foreach ($arrObjData as $index => $value) {
            if (is_object($value) || is_array($value)) {
                $value = objectsIntoArray($value, $arrSkipIndices); // recursive call
            }
            if (in_array($index, $arrSkipIndices)) {
                continue;
            }
            $arrData[$index] = $value;
        }
    }
    return $arrData;
}

//function in parsing xml data
function UploadWinnersXmlFile($filename,$gameid,$productid,$providerid,$arrXml,$xmlname,$providername,$productname,$gamenum,$gamecode,$gamebatchnum)
{
    $tmgameimport = new TMGameImport();
    $tmproducts = new TMProducts();
    $tmgames = new TMGameManagement();
    $auditlog = new TMAuditLog();
    $tmwinnings2 = new TMWinnings();
    $tmprizes = new TMPrizes();
    $tmprizestemp = new TMPrizesTemp();
    $tmproviders = new TMProvider();
	$tmgamebatches = new TMGameBatches();
    $tmbatchtranslog = new TMBatchTransactionLog();

    $totalprizescount = 0;
    $deletewinningsaffectedrow = 0;
    $deleteprizesaffectedrow = 0;
    $totalwinningscount = 0;
	$msgcode = 0;

	/* STEP 1:
     * Insert game batch id
     */
    $gamebatch["GameID"] = $gameid;
    $gamebatch["BatchID"] = $gamebatchnum;
    $gamebatch["XMLType"] = 2;
    $tmgamebatches->Insert($gamebatch);
    $gamebatchid = $tmgamebatches->LastInsertID;

	/*Added by Arlene R. Salazar 03/08/2012*/
	$fullpath = "../xml/winners/" . $filename;
	$filename_parts = explode(".",$filename);
	$filename_1 = $filename_parts[0] . "_" . $gamebatchid;
	$filename_2 = $filename_parts[1];
	$new_filename = $filename_1 . "." . $filename_2;
	$new_fullpath = "../xml/winners/" . $new_filename;
	rename($fullpath,$new_fullpath);

	/* STEP 2:
	 * insert uploaded xml in the table
	 */
	$tmimport["ProviderID"] = $providerid;
	$tmimport["GameID"] = $gameid;
	$tmimport["FileName"] = $filename;
	$tmimport["FilePath"] = $fullpath;
	$tmimport["ProductID"] = $productid;
	$tmimport["DateUploaded"] = 'now_usec()';
	$tmimport["Status"] = '0';
	//$tmimport["GameBatchID"] = $gamebatchid;
	$tmgameimport->Insert($tmimport);
	$gameimportid = $tmgameimport->LastInsertID;
	if ($tmgameimport->HasError)
	{
		$errormsg = $tmgameimport->getError();
		$errormsgtitle = "ERROR!";
		$msgcode = 1;
	}
	$id = $gameimportid;

	/* STEP 3:
	 * insert audit trail log
	 */
	$auditdtls["SessionID"] = $_SESSION['sid'];
	$auditdtls["AID"] = $_SESSION['acctid'];
	$auditdtls["TransDetails"] = "GameImport ID: " . $tmgameimport->LastInsertID;
	$auditdtls["TransDateTime"] = "now_usec()";
	$auditdtls["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
	$auditdtls["AuditTrailFunctionID"] = 26;
	$insertauditlog = $auditlog->Insert($auditdtls);
	if($auditlog->HasError)
	{
		$errormsgtitle = "ERROR!";
		$errormsg = "Error has occured:" . $tmaccount->getError();
		$msgcode = 1;
	}

	$errormsg = $filename. " has been successfully uploaded";
	$errormsgtitle = "UPLOAD SUCCESSFUL";
	$imax1 = 0;
	if($xmlname == "$gamecode")
	{
            $xml_content = $arrXml["ticket"];
            //foreach ($arrXml as $key => $value)
            do{
                $winnersdtls = null;
                for($xml = 0 ; $xml < count($arrXml) ; $xml++)
                {
                    for($i = 0 ; $i < count($arrXml["ticket"]) ; $i++)
                    {
                        $winners["ProductID"] = $productid;
                        $winners["AccountAID"] = $_SESSION["acctid"];
                        $winners["Description"] = trim($xml_content[$i]["prize_value"]);
                        $winners["VIRN"] = trim($xml_content[$i]["VIRN"]);
                        $winners["IsExists"] = "0";
                        //$winners["GameNumber"] = substr($gameno[0] , 2 , 3);
                        //$winners["GameNumber"] = substr($gameno[0], 2, strlen($gameno[0]));
                        $winners["GameNumber"] = substr($gamecode, 2, strlen($gamecode));
                        $winners["GameBatchID"] = $gamebatchid;
                        //$winnersdtls[] = $winners;
                        $csv_content .= implode(";", $winners) . "<br>";
                        $winnersdtls++;
                    }
                }
                $imax1++;
                
                if ($imax1 > 2)
                {
                        $status = 2;
                        break;
                }
                else
                {
                        $status = 1;
                }
            //}while(count($winnersdtls) != count($xml_content));
            }while($winnersdtls != count($xml_content));

            $imax = 0;
		
            //$winnersdtls_count = count($winnersdtls)
            $winnersdtls_count = $winnersdtls;
            if($status != 2)
            {
                do{
                    /* STEP 4:
                     * delete from prizetemp where AccountAID == $_SESSION["acctid"]
                     * insert into prizetemp
                     */
                    //$tmprizestemp->DeleteRecords($_SESSION["acctid"]);
                    $tmprizestemp->TruncatePrizesTemp();
                    //insert into CSV file
                    /*$array_1dimension = array_map("implode_inner_array", $winnersdtls);
                    $csv_content = implode('<br>', $array_1dimension);
                    $winnersdtls = null;*/
                    $fp = fopen('../csv/winners.csv', 'w');
                    fwrite($fp,$csv_content);
                    fclose($fp);
                    $csv_content = null;
                    //insert to inventorytemp using csv file
                    $tmprizestemp->InsertWinnersUsingCSVFile('../csv/winners.csv', ';', '<br>');
                    //$prizes = $tmprizestemp->InsertMultiple($winnersdtls);

                    /* STEP 5:
                     * check if prize exists
                     */
                    $tmprizestemp->CheckIfPrizeAlreadyExist($productid);
                    //App::Pr($tmprizestemp->AffectedRows);

                    /* Step 6:
                     * Get all consolation prizes that does not yet exists and insert to prizes
                     */
                    $tmprizes->InsertNewConsolationPrizes($productid,$_SESSION['acctid'],$winnersdtls_count,$gamebatchid);
                    if($tmwinnings2->HasError)
                    {
                        $errormsgtitle = "ERROR!";
                        $errormsg = "Error while inserting winning cards: " . $tmwinnings2->getError();
                        $msgcode = 1;
                    }
                    $firstprizebatch = $tmprizes->AffectedRows;

                    /* Step 7:
                     * Get all jackpot prizes that does not yet exists and insert to prizes
                     */
                    //$tmprizes->InsertNewJackpotPrizes($ddlProducts->SubmittedValue,$_SESSION['acctid']);
                    //$secondprizebatch = $tmprizes->AffectedRows;
                    $secondprizebatch = 0;

                    $isVIRNExist = $tmprizestemp->CheckDuplicateVIRN();
                    if ($isVIRNExist[0]["count"] > 0)
                    {
                        $msgcode = 2;
                    } else {
                        
                    /* Step 8:
                     * Insert to winning cards all prizes on step 3 and 4
                     */
                    $tmwinnings2->InsertWinnings(0,$productid,$_SESSION['acctid'],$winnersdtls_count,$gamebatchid);
                    if($tmwinnings2->HasError)
                    {
                        $errormsgtitle = "ERROR!";
                        $errormsg = "Error while inserting winning cards: " . $tmwinnings2->getError();
                        $msgcode = 1;
                    }
                    $firstbatch = $tmwinnings2->AffectedRows;

                    /* Step 9:
                     * Insert to winning cards all existing prizes
                     */
                    $tmwinnings2->InsertWinnings(1,$productid,$_SESSION['acctid'],$winnersdtls_count,$gamebatchid);
                    if($tmwinnings2->HasError)
                    {
                        $errormsgtitle = "ERROR!";
                        $errormsg = "Error while inserting winning cards: " . $tmwinnings2->getError();
                        $msgcode = 1;
                    }
                    $secondbatch = $tmwinnings2->AffectedRows;

                    /* Step 10:
                     * Delete from prizetemp|| ($imax < 3)
                     */
                    //$tmprizestemp->DeleteRecords($_SESSION["acctid"]);
                    $tmprizestemp->TruncatePrizesTemp();

                    /* Step 11:
                     * Delete all inserted winningcards and prizes if total inserted is not equal to xml count
                     */
                    //count of all winnings uploaded
                    $totalwinningscount = ($firstbatch + $secondbatch);
                    $lastrecord = $tmwinnings2->GetLastWinningCardID();
                    //count of all prizes uploaded
                    $totalprizescount = ($firstprizebatch + $secondprizebatch);
                    $lastprizerecord = $tmprizes->GetLastWinningCardID();
				
                    if($totalwinningscount != count($xml_content))
                    {
                        if($lastrecord[0]["LastID"] != NULL)
                        {
                            //do{
                            $start = ($lastrecord[0]["LastID"] - $totalwinningscount);
                            $deletealltransactions = $tmwinnings2->DeleteAllTransactions($start,$lastrecord[0]["LastID"]);
                            $deletewinningsaffectedrow = ($deletewinningsaffectedrow + $tmwinnings2->AffectedRows);
                            //}while($deletewinningsaffectedrow != $totalwinningscount);
                        }

                        if($lastprizerecord[0]["LastID"] != NULL)
                        {
                            //do{
                            $start = ($lastprizerecord[0]["LastID"] - $totalprizescount);
                            $deletealltransactions = $tmprizes->DeleteAllTransactions($start,$lastprizerecord[0]["LastID"]);
                            //$deleteprizesaffectedrow = ($deleteprizesaffectedrow + $tmwinnings2->AffectedRows);
                            $deleteprizesaffectedrow = ($deleteprizesaffectedrow + $tmprizes->AffectedRows);
                            //}while($deleteprizesaffectedrow != $totalprizescount);
                        }

                        //delete game batches info
                        $tmgamebatches->DeleteByGameBatchesID($gamebatchid);
                        $gamebatchid = 0;
                    }
                    $imax++;
		
                    if ($imax > 2)
                    {
                            $status = 2;
                            break;
                    }
                    else
                    {
                            $status = 1;
                    }
                }
                }while($totalwinningscount != count($xml_content));
            }

            $xmlstatus["Status"] = $status;
            $xmlstatus["DateImported"] = "now_usec()";
            $xmlstatus["GameImportID"] = $id;
            $xmlstatus["GameBatchID"] = $gamebatchid;
            $updatestatus = $tmgameimport->UpdateByArray($xmlstatus);
            if($tmgameimport->HasError)
            {
                    $errormsgtitle = "ERROR!";
                    $errormsg = "Error:" . $tmgameimport->getError();
            }

            /* Step 12:
             * Insert to audit trail log
             */
            $auditdtls["SessionID"] = session_id();
            $auditdtls["AID"] = $_SESSION["acctid"]; //since this will be executed by a cron job, there will be no $_SESSION['acctid'] created
            $auditdtls["TransDetails"] = "GameImport ID: " . $id;
            $auditdtls["TransDateTime"] = "now_usec()";
            $auditdtls["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
            $auditdtls["AuditTrailFunctionID"] = 27;
            $insertauditlog = $auditlog->Insert($auditdtls);
            if($auditlog->HasError)
            {
                    $errormsgtitle = "ERROR!";
                    $errormsg = "Error has occured:" . $tmaccount->getError();
                    $msgcode = 1;
            }

            /* Step 13:
            * Insert to batch transaction log
            */
            $batchtrans["GameID"] = $gameid;
            $batchtrans["BatchID"] = $gamebatchnum;
            $batchtrans["FileName"] = $filename;
            $batchtrans["TransactionDate"] = "now_usec()";
            $batchtrans["TransactionType"] = 1;
            $batchtrans["ProcessedByAID"] = $_SESSION["acctid"];
            $batchtrans["Status"] = $status;
            $tmbatchtranslog->Insert($batchtrans);
            if ($tmbatchtranslog->HasError) {
                $errormsgtitle = "ERROR!";
                $errormsg = "Error has occured:" . $tmbatchtranslog->getError();
                $msgcode = 1;
            }

            if($status != 2)
            {
                    $countmsgtitle = "Winners File Summary";
                    $countmsg = "Provider: " . $providername . "<br/>" .
                                        "Game Name: " . $productname . "<br/>" .
                                        "Game Number: " . $gamenum. "<br/>" .
                                        "Total No. of Prizes: " . number_format($totalprizescount) . "<br/>" .
                                        "Total No. of VIRN: " . number_format($totalwinningscount);
            }
            else
            {
                if ($msgcode == 2)
                {
                    $countmsgtitle = "ERROR!";
                    $countmsg = "Duplicate entry of VIRN not allowed.";
                } else {
                    $countmsgtitle = "ERROR!";
                    $countmsg = "Uploading was unsuccessful . Please try again.";
                }
            }
	}
	else
	{
		$errormsgtitle = "ERROR!";
		$errormsg = $filename . " could not be processed.";
	}
	/*End of parse data*/

	if($msgcode == 0)
        {
            $msgtitle = $countmsgtitle;
            $msg = $countmsg;
        }
        else
        {
            $msgtitle = $errormsgtitle;
            $msg = $errormsg;
        }
        $msgtitle = $countmsgtitle;
        $msg = $countmsg;
        return array($msgtitle,$msg);
}

//function to convert inner array into string
function implode_inner_array($n)
{
    return implode(";", $n);
}
?>