<?php
ignore_user_abort(true);                                                        //Ignore user aborts and force the script running until the last part
set_time_limit(0);                                                              //Limits the maximum execution time , if set to 0 no maximum time is imposed. Safe mode should be turned off.

$stylesheets[] = "css/default.css";
$javascripts[] = "jscripts/validations.js";
$pagesubmenuid = 33;

App::LoadModuleClass("TicketManagementCM", "TMGameImport");
App::LoadModuleClass("TicketManagementCM", "TMProducts");
App::LoadModuleClass("TicketManagementCM", "TMGameManagement");
App::LoadModuleClass("TicketManagementCM", "TMAuditLog");
App::LoadModuleClass("TicketManagementCM", "TMWinnings");
//App::LoadModuleClass("TicketManagementCM", "TMPrizes");
App::LoadModuleClass("TicketManagementCM", "TMPrizes");
App::LoadModuleClass("TicketManagementCM", "TMPrizesTemp");
App::LoadModuleClass("TicketManagementCM", "TMProvider");

App::LoadControl("ComboBox");
App::LoadControl("Button");

$tmgameimport = new TMGameImport();
$tmproducts = new TMProducts();
$tmgames = new TMGameManagement();
$auditlog = new TMAuditLog();
$tmwinnings2 = new TMWinnings();
$tmprizes = new TMPrizes();
$tmprizestemp = new TMPrizesTemp();
$tmproviders = new TMProvider();

$frmWinnerXmlUpload = new FormsProcessor();

/*$where = " ORDER BY ProductName";
$products = $tmproducts->SelectByWhere($where);
$products_list = new ArrayList();
$products_list->AddArray($products);

$ddlProducts = new ComboBox("ddlProducts","ddlProducts","Products: ");
$ddlProducts->ShowCaption = true;
$ddlProducts->DataSource = $products_list;
$ddlProducts->DataSourceText = "ProductName";
$ddlProducts->DataSourceValue = "ProductID";
$ddlProducts->DataBind();*/

$where = " WHERE Status = 1 ORDER BY Name";
$providers = $tmproviders->SelectAllProvider();

$ddlProviders = new ComboBox("ddlProviders","ddlProviders","Provider: ");
$ddlProviders->ShowCaption = true;
if(count($providers) > 0)
{
    $providers_list = new ArrayList();
    $providers_list->AddArray($providers);
    $ddlProviders->DataSource = $providers_list;
    $ddlProviders->DataSourceText = "Name";
    $ddlProviders->DataSourceValue = "ProviderID";
    $ddlProviders->DataBind();
}
else
{
    $options = null;
    $options[] = new ListItem("- - - - - - - - - - - - - -","",true);
    $ddlProviders->Items = $options;
}


$btnUpload = new Button("btnUpload","btnUpload","Upload");
$btnUpload->IsSubmit = true;
$btnUpload->Args = "onclick='javascript: return showloadingpage();'";

//$frmWinnerXmlUpload->AddControl($ddlProducts);
$frmWinnerXmlUpload->AddControl($btnUpload);
$frmWinnerXmlUpload->AddControl($ddlProviders);

$frmWinnerXmlUpload->ProcessForms();

if($frmWinnerXmlUpload->IsPostBack)
{
//App::Pr($_FILES);
//exit();
	if($_FILES['file']['type'] == "")
    {
        $errormsg = "Please specify file.";
        $errormsgtitle = "ERROR!";
    }
    else if (($_FILES['file']['type'] == "text/xml"))
    {
		if ($_FILES["file"]["error"] > 0)
        {
            $errormsg = "Return Code: " . $_FILES["file"]["error"] . "<br />";
            $errormsgtitle = "ERROR!";
        }
        else
        {
			//reading countrycode.txt
            $myFile = "../countrycode.txt";
            $fileopen = fopen($myFile,'r');
            if(filesize($myFile) > 0)
            {
                $countrycode = trim(fread($fileopen,filesize($myFile)));
            }
            else
            {
                $countrycode = "";
            }
            fclose($fileopen);

			//reading proper filename format
            $filenamechecking = "../winnersfile.txt";
            $filenameopen = fopen($filenamechecking,'r');
            if(filesize($filenamechecking) > 0)
            {
                $formatfromtxt = trim(fread($filenameopen,filesize($filenamechecking)));
                if(strlen($formatfromtxt) > 0)
                {
                    $filenameformat = explode("_", $formatfromtxt);
                    $fntotal = count($filenameformat);
                    $fn1 = strlen($filenameformat[0]);
                    $fn2 = strlen($filenameformat[1]);
                    $fn3 = strlen($filenameformat[2]);
                    $fn4 = strlen($filenameformat[3]);
					$fnstring1 = $filenameformat[1]; //WLIST is the only word that do not change
                }
                else
                {
                    $fntotal = 0;
                    $fn1 = 0;
                    $fn2 = 0;
                    $fn3 = 0;
                    $fn4 = 0;
					$fnstring1 = "";
                }
            }
            else
            {
                $fntotal = 0;
                $fn1 = 0;
                $fn2 = 0;
                $fn3 = 0;
                $fn4 = 0;
				$fnstring1 = "";
            }
            fclose($filenameopen);

			if(strlen($countrycode) > 0)
            {
                $filename = $_FILES["file"]["name"];
                //$fullpath = "../xml/winners/" . $_FILES["file"]["name"];
                $fullpath = "../xml/winners/" . $_FILES["file"]["name"];

                // check if file exists
                $fileExixts = $tmgameimport->CheckFile($fullpath);
                //if (file_exists($fullpath))
                if (count($fileExixts) > 0) {
                    $errormsg = "File was already imported. Please enter a new batch of winners file.";
                    //$errormsg = $filename . " already exists. ";
                    $errormsgtitle = "ERROR!";
                } else {
                    // checking if the corect file is being uploaded
                    $filechecking = explode("_", $filename);

                    //checking if filename is composed of 4 underscores
                    if ((count($filechecking) < $fntotal) || (count($filechecking) > $fntotal)) {
                        $errormsg = "Incorrect file format. Please upload a valid file format.";
                        $errormsgtitle = "ERROR!";
                    } else {
                        if ((strlen($filechecking[0]) < $fn1) || (strlen($filechecking[0]) > $fn1)) {
                            $errormsg = "Incorrect file format. Please upload a valid file format.";
                            $errormsgtitle = "ERROR!";
                        } else if ((strlen($filechecking[1]) < $fn2) || (strlen($filechecking[1]) > $fn2) || ($filechecking[1] != $fnstring1)) {
                            $errormsg = "Incorrect file format. Please upload a valid file format.";
                            $errormsgtitle = "ERROR!";
                        } else if ((strlen($filechecking[2]) < $fn3) || (strlen($filechecking[2]) > $fn3)) {
                            $errormsg = "Incorrect file format. Please upload a valid file format.";
                            $errormsgtitle = "ERROR!";
                        } else if ((strlen($filechecking[3]) < $fn4) || (strlen($filechecking[3]) > $fn4)) {
                            $errormsg = "Incorrect file format. Please upload a valid file format.";
                            $errormsgtitle = "ERROR!";
                        } else {
                            if ($filechecking[1] == "WLIST") {
                                //read xml file
                                libxml_use_internal_errors(true);
                                move_uploaded_file($_FILES["file"]["tmp_name"], $fullpath);
                                $xml = simplexml_load_file($fullpath);

                                //checking if xml structure is correct
                                if (!$xml) {
                                    $errors = libxml_get_errors();

                                    foreach ($errors as $error) {
                                        $errormsgtitle = "ERROR!";
                                        $errormsg = display_xml_error($error);
                                    }

                                    libxml_clear_errors();
                                } else {
                                    $xmlUrl = $fullpath;
                                    $xmlStr = file_get_contents($xmlUrl);
                                    $xmlObj = simplexml_load_string($xmlStr);
                                    $arrXml = objectsIntoArray($xmlObj);
                                    $xmlname = $xml->getName();

                                    //checking of ticket node
                                    $ticket_node = $xml->xpath('ticket');
                                    //checking of VIRN node
                                    $virn_node = $xml->xpath('//VIRN');
                                    //checking of prize_value node
                                    $prizeval_node = $xml->xpath('//prize_value');
                                    //checking if there are empty nodes
                                    $emptynodes = in_array_r(Array(), $arrXml);

                                    if (!preg_match("/" . $countrycode . "/", $xmlname)) {
                                    //if (!preg_match("/FT/", $xmlname)) {                                        /*---> for timor*/
                                    //if (!preg_match("/FC/", $xmlname)) {                                      /*---> for cambodia*/
                                        $errormsgtitle = "ERROR!";
                                        $errormsg = "Fatal Error: Missing game number node. <br/> File: $fullpath";
                                    } else if (count($emptynodes) > 0) {
                                        $errormsgtitle = "ERROR!";
                                        $errormsg = "Fatal Error: Empty node/s in " . implode(',', $emptynodes) . "<br/> File: $fullpath";
                                    } else if (count($ticket_node) == 0) {
                                        $errormsgtitle = "ERROR!";
                                        $errormsg = "Fatal Error: Missing ticket node. <br/> File: $fullpath";
                                    } else if (count($virn_node) == 0) {
                                        $errormsgtitle = "ERROR!";
                                        $errormsg = "Fatal Error: Missing VIRN node. <br/> File: $fullpath";
                                    } else if (count($prizeval_node) == 0) {
                                        $errormsgtitle = "ERROR!";
                                        $errormsg = "Fatal Error: Missing prize_value node. <br/> File: $fullpath";
                                    } else if (count($prizeval_node) != count($virn_node)) {
                                        $errormsgtitle = "ERROR!";
                                        $errormsg = "Fatal Error: VIRN node and prize_value node do not match. <br/> File: $fullpath";
                                    } else if (count($ticket_node) != count($virn_node) || count($ticket_node) != count($prizeval_node) || count($virn_node) != count($prizeval_node)) {
                                        $errormsgtitle = "ERROR!";
                                        $errormsg = "Fatal Error: Ticket node and VIRN node and prize_value node do not match. <br/> File: $fullpath";
                                    } else {
                                        //==================
                                        //checking if gamenumber exists
                                        $gameno = explode("_", $filename);
                                        $providerid = $ddlProviders->SelectedValue;
                                        $gameinfo = $tmgames->SelectGameDtlsByWhere(substr($gameno[0], 2, 3),$providerid);
                                        if(count($gameinfo) > 1)
                                        {
                                            $errormsgtitle = "ERROR!";
                                            $errormsg = "Fatal Error: Game number has multiple instances. <br/> File: $fullpath";
                                        }
                                        else if (count($gameinfo) == 1) {
                                            $totalprizescount = 0;
                                            $deletewinningsaffectedrow = 0;
                                            $deleteprizesaffectedrow = 0;
                                            $totalwinningscount = 0;

                                            //$gameno = substr($filename, 0,strpos($filename,'_'));
                                            //$gameno = explode("_", $filename);
                                            //$where = " WHERE GameNumber = " . substr($gameno[0] , 2 , 3);
                                            //$gameid = $tmgames->SelectByWhere($where);
                                            //header("Location: ../views/winnersxmlupload.php");

                                            /* Parse data */
                                            
                                            //$productid = $ddlProducts->SelectedValue;
                                            $gameid = $gameinfo[0]["GameID"];
                                            $productgameinfo = $tmproducts->GetProductNameGameNum($gameid);
                                            $providername = $tmproviders->SelectByID($providerid);
                                            $productid = $productgameinfo[0]["ProductID"];
                                            //echo $productgameinfo[0]["ProviderID"] . "========" . $providerid . "gameid" . $gameid;exit();
                                            //read xml file
                                            $gamecode = $gameno[0];
                                            //$xml = simplexml_load_file($fullpath);
                                            //$xmlUrl = $fullpath;
                                            //$xmlStr = file_get_contents($xmlUrl);
                                            //$xmlObj = simplexml_load_string($xmlStr);
                                            //======================
                                            // check if game id is already uploaded
                                            $isUploaded = $tmgameimport->CheckIfGameIdIsUploaded($gameid, "winners");
                                            if (count($isUploaded) == 0) {
                                                if ($productid != "") {
                                                    //checking if the provider assigned to game number is the same as he chosen provider
                                                    if ($productgameinfo[0]["ProviderID"] != $providerid) {
                                                        $errormsgtitle = "ERROR!";
                                                        $errormsg = "Provider assigned to game number do not match chosen provider.";
                                                    } else {
                                                        /* STEP 1:
                                                         * insert uploaded xml in the table
                                                         */
                                                        $tmimport["ProviderID"] = $providerid;
                                                        $tmimport["GameID"] = $gameid;
                                                        $tmimport["FileName"] = $filename;
                                                        $tmimport["FilePath"] = $fullpath;
                                                        $tmimport["ProductID"] = $productid;
                                                        $tmimport["DateUploaded"] = 'now_usec()';
                                                        $tmimport["Status"] = '0';
                                                        $tmgameimport->Insert($tmimport);
                                                        $gameimportid = $tmgameimport->LastInsertID;
                                                        if ($tmgameimport->HasError) {
                                                            $errormsg = $tmgameimport->getError();
                                                            $errormsgtitle = "ERROR!";
                                                        }
                                                        $id = $gameimportid;

                                                        /* STEP 2:
                                                         * insert audit trail log
                                                         */
                                                        $auditdtls["SessionID"] = $_SESSION['sid'];
                                                        $auditdtls["AID"] = $_SESSION['acctid'];
                                                        $auditdtls["TransDetails"] = "GameImport ID: " . $tmgameimport->LastInsertID;
                                                        $auditdtls["TransDateTime"] = "now_usec()";
                                                        $auditdtls["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
                                                        $auditdtls["AuditTrailFunctionID"] = 26;
                                                        $insertauditlog = $auditlog->Insert($auditdtls);
                                                        if ($auditlog->HasError) {
                                                            $errormsgtitle = "ERROR!";
                                                            $errormsg = "Error has occured:" . $tmaccount->getError();
                                                        }

                                                        $errormsg = $filename . " has been successfully uploaded";
                                                        $errormsgtitle = "UPLOAD SUCCESSFUL";
														$imax1 = 0;
                                                        if ($xmlname == "$gamecode") {
                                                            $winnersdtls = null;
                                                            $xml_content = $arrXml["ticket"];
                                                            //foreach ($arrXml as $key => $value)
                                                            do {
                                                                for ($xml = 0; $xml < count($arrXml); $xml++) {
                                                                    for ($i = 0; $i < count($arrXml["ticket"]); $i++) {
                                                                        $winners["ProductID"] = $productid;
                                                                        $winners["Description"] = trim($xml_content[$i]["prize_value"]);
                                                                        $winners["VIRN"] = trim($xml_content[$i]["VIRN"]);
                                                                        $winners["IsExists"] = "0";
                                                                        $winners["GameNumber"] = substr($gameno[0], 2, 3);
                                                                        $winners["AccountAID"] = $_SESSION["acctid"];
                                                                        $winnersdtls[] = $winners;
                                                                    }
                                                                }

																$imax1++;
															
																if ($imax1 > 2)
																{
																	$status = 2;
																	break;
																}
																else
																{
																	$status = 1;
																}
                                                            } while (count($winnersdtls) != count($xml_content));
                                                            //App::Pr(count($winnersdtls) . "------" . count($xml_content));
                                                            $imax = 0;

															if($status != 2)
															{
		                                                        do {
		                                                            /* STEP 3:
		                                                             * delete from prizetemp where AccountAID == $_SESSION["acctid"]
		                                                             * insert into prizetemp
		                                                             */
		                                                            $tmprizestemp->DeleteRecords($_SESSION["acctid"]);
		                                                            $prizes = $tmprizestemp->InsertMultiple($winnersdtls);

		                                                            /* STEP 4:
		                                                             * check if prize exists
		                                                             */
		                                                            $tmprizestemp->CheckIfPrizeAlreadyExist($productid);
		                                                            //App::Pr($tmprizestemp->AffectedRows);

		                                                            /* Step 5:
		                                                             * Get all consolation prizes that does not yet exists and insert to prizes
		                                                             */
		                                                            $tmprizes->InsertNewConsolationPrizes($productid, $_SESSION['acctid'], count($winnersdtls), count($winnersdtls));
		                                                            $firstprizebatch = $tmprizes->AffectedRows;
		                                                            //App::Pr($tmprizes->getError());
		                                                            /* Step 6:
		                                                             * Get all jackpot prizes that does not yet exists and insert to prizes
		                                                             */
		                                                            //$tmprizes->InsertNewJackpotPrizes($ddlProducts->SubmittedValue,$_SESSION['acctid']);
		                                                            //$secondprizebatch = $tmprizes->AffectedRows;
		                                                            $secondprizebatch = 0;

		                                                            /* Step 7:
		                                                             * Insert to winning cards all prizes on step 3 and 4
		                                                             */
		                                                            $tmwinnings2->InsertWinnings(0, $productid, $_SESSION['acctid'], count($winnersdtls));
		                                                            $firstbatch = $tmwinnings2->AffectedRows;

		                                                            /* Step 8:
		                                                             * Insert to winning cards all existing prizes
		                                                             */
		                                                            $tmwinnings2->InsertWinnings(1, $productid, $_SESSION['acctid'], count($winnersdtls));
		                                                            $secondbatch = $tmwinnings2->AffectedRows;

		                                                            /* Step 9:
		                                                             * Delete from prizetemp|| ($imax < 3)
		                                                             */
		                                                            $tmprizestemp->DeleteRecords($_SESSION["acctid"]);

		                                                            /* Step 10:
		                                                             * Delete all inserted winningcards and prizes if total inserted is not equal to xml count
		                                                             */
		                                                            //count of all winnings uploaded
		                                                            $totalwinningscount = ($firstbatch + $secondbatch);
		                                                            $lastrecord = $tmwinnings2->GetLastWinningCardID();
		                                                            //count of all prizes uploaded
		                                                            $totalprizescount = ($firstprizebatch + $secondprizebatch);
		                                                            $lastprizerecord = $tmprizes->GetLastWinningCardID();

		                                                            if ($totalwinningscount != count($xml_content)) {
		                                                                if ($lastrecord[0]["LastID"] != NULL) {
		                                                                    //do {
		                                                                        $start = ($lastrecord[0]["LastID"] - $totalwinningscount);
		                                                                        $deletealltransactions = $tmwinnings2->DeleteAllTransactions($start, $lastrecord[0]["LastID"]);
		                                                                        $deletewinningsaffectedrow = ($deletewinningsaffectedrow + $tmwinnings2->AffectedRows);
		                                                                    //} while ($deletewinningsaffectedrow != $totalwinningscount);
		                                                                }

		                                                                if ($lastprizerecord[0]["LastID"] != NULL) {
		                                                                    //do {
		                                                                        $start = ($lastprizerecord[0]["LastID"] - $totalprizescount);
		                                                                        $deletealltransactions = $tmprizes->DeleteAllTransactions($start, $lastprizerecord[0]["LastID"]);
		                                                                        $deleteprizesaffectedrow = ($deleteprizesaffectedrow + $tmprizes->AffectedRows);
		                                                                    //} while ($deleteprizesaffectedrow != $totalprizescount);
		                                                                }
		                                                            }

		                                                            $imax++;
		                                                            //App::Pr($imax);
		                                                            if ($imax > 2) {
		                                                                $status = 2;
		                                                                break;
		                                                            } else {
		                                                                $status = 1;
		                                                            }
		                                                        } while ($totalwinningscount != count($xml_content));
															}

                                                            $xmlstatus["Status"] = $status;
                                                            $xmlstatus["DateImported"] = "now_usec()";
                                                            $xmlstatus["GameImportID"] = $id;
                                                            $updatestatus = $tmgameimport->UpdateByArray($xmlstatus);
                                                            if ($tmgameimport->HasError) {
                                                                $errormsgtitle = "ERROR!";
                                                                $errormsg = "Error:" . $tmgameimport->getError();
                                                            }

                                                            /* Step 11:
                                                             * Insert to audit trail log
                                                             */
                                                            $auditdtls["SessionID"] = session_id();
                                                            $auditdtls["AID"] = $_SESSION["acctid"]; //since this will be executed by a cron job, there will be no $_SESSION['acctid'] created
                                                            $auditdtls["TransDetails"] = "GameImport ID: " . $id;
                                                            $auditdtls["TransDateTime"] = "now_usec()";
                                                            $auditdtls["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
                                                            $auditdtls["AuditTrailFunctionID"] = 27;
                                                            $insertauditlog = $auditlog->Insert($auditdtls);
                                                            if ($auditlog->HasError) {
                                                                $errormsgtitle = "ERROR!";
                                                                $errormsg = "Error has occured:" . $tmaccount->getError();
                                                            }

                                                            if ($status != 2) {
                                                                $countmsgtitle = "Winners File Summary";
                                                                $countmsg = "Provider: " . $providername[0]["Name"] . "<br/>" .
                                                                        "Game Name: " . $productgameinfo[0]["ProductName"] . "<br/>" .
                                                                        "Game Number: " . $productgameinfo[0]["GameNumber"] . "<br/>" .
                                                                        "Total No. of Prizes: " . number_format($totalprizescount) . "<br/>" .
                                                                        "Total No. of VIRN: " . number_format($totalwinningscount);
                                                            } else {
                                                                $countmsgtitle = "ERROR!";
                                                                $countmsg = "Uploading was unsuccessful . Please try again.";
                                                            }
                                                            //$countmsg = $totalwinningscount . " prizes was successfully uploaded.";
                                                        } else {
                                                            $errormsgtitle = "ERROR!";
                                                            $errormsg = $filename . " could not be processed.";
                                                        }
                                                        /* End of parse data */
                                                        //here
                                                    }
                                                } else {
                                                    $errormsgtitle = "ERROR!";
                                                    $errormsg = "Product id do not exists.";
                                                }
                                            } else {
                                                $errormsgtitle = "ERROR!";
                                                $errormsg = "You cannot proceed because winning cards were already uploaded for this game number.";
                                            }
                                            //=============================
                                        } else {
                                            $errormsg = "Game number do not exists. Please ensure that the filename is correct.";
                                            $errormsgtitle = "ERROR!";
                                        }
                                        //=====================
                                    }
                                }
                            } else {
                                $errormsg = "Incorrect file name format. Please upload a valid file name format.";
                                //$errormsg = "I suppose you are uploading a wrong file.";
                                $errormsgtitle = "ERROR!";
                            } //wlist
                        }
                    }
                }
            }
            else
            {
                $errormsg = "You cannot proceed because country code text file is empty.";
                $errormsgtitle = "ERROR!";
            }
		}
	}
    else
    {
		$errormsg = "Incorrect file format. Please upload a valid file format.";
        //$errormsg = "Invalid file ";
        $errormsgtitle = "ERROR!";
    }
}

//function for checking if xml array have empty nodes
function in_array_r($needle, $haystack) {
    $emptynodeskey = array();
    foreach ($haystack as $key => $item) {
        if ($item === $needle || (is_array($item) && in_array_r($needle, $item))) {
            //return true;
            $emptynodeskey[] = $key;

        }
    }
    //return false;
    return $emptynodeskey;
}

//function for checking xml structure
function display_xml_error($error)
{
    $return = "";
    //$return .= $error->column . "\n";

    switch ($error->level) {
        case LIBXML_ERR_WARNING:
            $return .= "Warning $error->code: ";
            break;
         case LIBXML_ERR_ERROR:
            $return .= "Error $error->code: ";
            break;
        case LIBXML_ERR_FATAL:
            $return .= "Fatal Error $error->code: ";
            break;
    }

    $return .= trim($error->message) .
               "<br/>  Line: $error->line" .
               "<br/>  Column: $error->column";

    if ($error->file) {
        $return .= "</br>  File: $error->file";
    }

    return addslashes("$return<br/>");
}

//function for converting xml to array
function objectsIntoArray($arrObjData, $arrSkipIndices = array())
{
    $arrData = array();

    // if input is object, convert into array
    if (is_object($arrObjData)) {
        $arrObjData = get_object_vars($arrObjData);
    }

    if (is_array($arrObjData)) {
        foreach ($arrObjData as $index => $value) {
            if (is_object($value) || is_array($value)) {
                $value = objectsIntoArray($value, $arrSkipIndices); // recursive call
            }
            if (in_array($index, $arrSkipIndices)) {
                continue;
            }
            $arrData[$index] = $value;
        }
    }
    return $arrData;
}
?>