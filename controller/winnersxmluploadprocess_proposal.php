<?php
/*
 * @author 
 * Purpose   : controller for winnersxmlupload_proposal
 */
ignore_user_abort(true);                                                        //Ignore user aborts and force the script running until the last part
set_time_limit(0);                                                              //Limits the maximum execution time , if set to 0 no maximum time is imposed. Safe mode should be turned off.

$stylesheets[] = "css/default.css";
$javascripts[] = "jscripts/validations.js";
$pagesubmenuid = 33;

App::LoadModuleClass("TicketManagementCM", "TMGameImport");
App::LoadModuleClass("TicketManagementCM", "TMProducts");
App::LoadModuleClass("TicketManagementCM", "TMGameManagement");
App::LoadModuleClass("TicketManagementCM", "TMAuditLog");
App::LoadModuleClass("TicketManagementCM", "TMWinnings");
App::LoadModuleClass("TicketManagementCM", "TMPrizes");
App::LoadModuleClass("TicketManagementCM", "TMPrizesTemp");
App::LoadModuleClass("TicketManagementCM", "TMProvider");
App::LoadModuleClass("TicketManagementCM", "TMGameBatches");
App::LoadModuleClass("TicketManagementCM", "TMBatchTransactionLog");

App::LoadControl("ComboBox");
App::LoadControl("Button");
App::LoadControl("Hidden");

list($tmgameimport,$tmproducts,$tmgames,$auditlog,$tmwinnings2,$tmprizes,$tmprizestemp,$tmproviders,$tmgamebatches,$tmbatchtranslog) = DeclareClasses();

$frmWinnerXmlUpload = new FormsProcessor();

$where = " WHERE Status = 1 ORDER BY Name";
$providers = $tmproviders->SelectAllProvider();

$ddlProviders = new ComboBox("ddlProviders","ddlProviders","Provider: ");
$ddlProviders->ShowCaption = true;
if(count($providers) > 0)
{
    $providers_list = new ArrayList();
    $providers_list->AddArray($providers);
    $ddlProviders->DataSource = $providers_list;
    $ddlProviders->DataSourceText = "Name";
    $ddlProviders->DataSourceValue = "ProviderID";
    $ddlProviders->DataBind();
    $ddlProviders->SetSelectedValue(1);
}
else
{
    $options = null;
    $options[] = new ListItem("- - - - - - - - - - - - - -","",true);
    $ddlProviders->Items = $options;
}

$btnUpload = new Button("btnUpload","btnUpload","Upload");
$btnUpload->IsSubmit = true;
$btnUpload->Args = "onclick='javascript: return showwinnersloadingpage();'";

$btnConfirmUpload = new Button("btnConfirmUpload","btnConfirmUpload","Yes");
$btnConfirmUpload->IsSubmit = true;
$btnConfirmUpload->Args = "onclick='javascript: return showwinnersloadingpage2();'";

$hiddenfilename = new Hidden("hiddenfilename","hiddenfilename","Hidden file name");
$hiddengameid = new Hidden("hiddengameid","hiddengameid","Hidden gameid");
$hiddenproductid = new Hidden("hiddenproductid","hiddenproductid","Hidden productid");
$hiddenproviderid = new Hidden("hiddenproviderid","hiddenproviderid","Hidden providerid");
$hiddenprovidername = new Hidden("hiddenprovidername","hiddenprovidername","Hidden provider name");
$hiddenprodname = new Hidden("hiddenprodname","hiddenprodname","Hidden prod name");
$hiddengamenum = new Hidden("hiddengamenum","hiddengamenum","Hidden game num");
$hiddengamecode = new Hidden("hiddengamecode","hiddengamecode","Hidden game code");
$hiddenbatchid = new Hidden("hiddenbatchid","hiddenbatchid","Hidden batch id");

$frmWinnerXmlUpload->AddControl($btnUpload);
$frmWinnerXmlUpload->AddControl($ddlProviders);
$frmWinnerXmlUpload->AddControl($btnConfirmUpload);
$frmWinnerXmlUpload->AddControl($hiddenfilename);
$frmWinnerXmlUpload->AddControl($hiddengameid);
$frmWinnerXmlUpload->AddControl($hiddenproductid);
$frmWinnerXmlUpload->AddControl($hiddenproviderid);
$frmWinnerXmlUpload->AddControl($hiddenprovidername);
$frmWinnerXmlUpload->AddControl($hiddenprodname);
$frmWinnerXmlUpload->AddControl($hiddengamenum);
$frmWinnerXmlUpload->AddControl($hiddengamecode);
$frmWinnerXmlUpload->AddControl($hiddenbatchid);

$frmWinnerXmlUpload->ProcessForms();

if($frmWinnerXmlUpload->IsPostBack)
{
    if($btnUpload->SubmittedValue == "Upload")
    {
        if($_FILES['file']['type'] == "")
        {
            $errormsg = "Please specify file.";
            $errormsgtitle = "ERROR!";
        }
        else if (($_FILES['file']['type'] == "text/xml"))
        {
            if ($_FILES["file"]["error"] > 0)
            {
                $errormsg = "Return Code: " . $_FILES["file"]["error"] . "<br />";
                $errormsgtitle = "ERROR!";
            }
            else
            {
                $filename = $_FILES["file"]["name"];
                $fullpath = "../xml/winners/" . $_FILES["file"]["name"];

                // check if file exists
                $fileExixts = $tmgameimport->CheckFile($fullpath);
                //if (file_exists($fullpath))
                /*if(count($fileExixts) > 0)
                {
                    $errormsg = "File was already imported. Please enter a new batch of winners file.";
                    //$errormsg = $filename . " already exists. ";
                    $errormsgtitle = "ERROR!";
                }
                else
                {*/
                    // checking if the corect file is being uploaded
                    $filechecking = explode("_", $filename);

                        //checking if filename is composed of 4 underscores
                        if((count($filechecking) < 4) || (count($filechecking) > 4))
                        {
                            $errormsg = "Incorrect file format. Please upload a valid file format.";
                            $errormsgtitle = "ERROR!";
                        }
                        else
                        {
                            if((strlen($filechecking[0]) < 5) || (strlen($filechecking[0]) > 5))
                            {
                                $errormsg = "Incorrect file format. Please upload a valid file format.";
                                $errormsgtitle = "ERROR!";
                            }
                            else if((strlen($filechecking[1]) < 5) || (strlen($filechecking[1]) > 5))
                            {
                                $errormsg = "Incorrect file format. Please upload a valid file format.";
                                $errormsgtitle = "ERROR!";
                            }
                            else if((strlen($filechecking[2]) < 4) || (strlen($filechecking[2]) > 4))
                            {
                                $errormsg = "Incorrect file format. Please upload a valid file format.";
                                $errormsgtitle = "ERROR!";
                            }
                            else if((strlen($filechecking[3]) < 8) || (strlen($filechecking[3]) > 8))
                            {
                                $errormsg = "Incorrect file format. Please upload a valid file format.";
                                $errormsgtitle = "ERROR!";
                            }
                            else
                            {
                                if($filechecking[1] == "WLIST")
                                {

                                //read xml file
                                libxml_use_internal_errors(true);
                                move_uploaded_file($_FILES["file"]["tmp_name"], $fullpath);
                                $xml = simplexml_load_file($fullpath);
                                
                                //checking if xml structure is correct
                                if (!$xml)
                                {
                                    $errors = libxml_get_errors();

                                    foreach ($errors as $error)
                                    {
                                        $errormsgtitle = "ERROR!";
                                        $errormsg = display_xml_error($error);
                                    }

                                    libxml_clear_errors();
                                }
                                else
                                {
                                    $xmlUrl = $fullpath;
                                    $xmlStr = file_get_contents($xmlUrl);
                                    $xmlObj = simplexml_load_string($xmlStr);
                                    $arrXml = objectsIntoArray($xmlObj);
                                    $xmlname = $xml->getName();

                                    //checking of ticket node
                                    $ticket_node = $xml->xpath('ticket');
                                    //checking of VIRN node
                                    $virn_node = $xml->xpath('//VIRN');
                                    //checking of prize_value node
                                    $prizeval_node = $xml->xpath('//prize_value');
                                    //checking if there are empty nodes
                                    $emptynodes = in_array_r(Array(), $arrXml);

                                    if(!preg_match("/FT/", $xmlname))
                                    {
                                        $errormsgtitle = "ERROR!";
                                        $errormsg = "Fatal Error: Missing game number node. <br/> File: $fullpath";
                                    }
                                    else if(count($emptynodes) > 0)
                                    {
                                        $errormsgtitle = "ERROR!";
                                        $errormsg = "Fatal Error: Empty node/s in " . implode(',',$emptynodes) . "<br/> File: $fullpath";
                                    }
                                    else if(count($ticket_node) == 0)
                                    {
                                        $errormsgtitle = "ERROR!";
                                        $errormsg = "Fatal Error: Missing ticket node. <br/> File: $fullpath";
                                    }
                                    else if(count($virn_node) == 0)
                                    {
                                        $errormsgtitle = "ERROR!";
                                        $errormsg = "Fatal Error: Missing VIRN node. <br/> File: $fullpath";
                                    }
                                    else if(count($prizeval_node) == 0)
                                    {
                                        $errormsgtitle = "ERROR!";
                                        $errormsg = "Fatal Error: Missing prize_value node. <br/> File: $fullpath";
                                    }
                                    else if(count($prizeval_node) != count($virn_node))
                                    {
                                        $errormsgtitle = "ERROR!";
                                        $errormsg = "Fatal Error: VIRN node and prize_value node do not match. <br/> File: $fullpath";
                                    }
                                    else if(count($ticket_node) != count($virn_node) || count($ticket_node) != count($prizeval_node) || count($virn_node) != count($prizeval_node))
                                    {
                                        $errormsgtitle = "ERROR!";
                                        $errormsg = "Fatal Error: Ticket node and VIRN node and prize_value node do not match. <br/> File: $fullpath";
                                    }
                                    else
                                    {
                                        //checking if gamenumber exists
                                        $gameno = explode("_" , $filename);
                                        if($gameno[0] == $xmlname)
                                        {
                                            $providerid = $ddlProviders->SelectedValue;
                                            $gameinfo = $tmgames->SelectGameDtlsByWhere(substr($gameno[0] , 2 , 3),$providerid);
                                            if(count($gameinfo) > 1)
                                            {
                                                $errormsgtitle = "ERROR!";
                                                $errormsg = "Fatal Error: Game number has multiple instances. <br/> File: $fullpath";
                                            }
                                            else if(count($gameinfo) == 1)
                                            {
                                                /*Parse data*/

                                                //$productid = $ddlProducts->SelectedValue;
                                                $gameid = $gameinfo[0]["GameID"];
                                                $productgameinfo = $tmproducts->GetProductNameGameNum($gameid);
                                                $providername = $tmproviders->SelectByID($providerid);
                                                $productid = $productgameinfo[0]["ProductID"];

                                                //read xml file
                                                $gamecode = $gameno[0];

                                                // check if game id is already uploaded
                                                $isUploaded = $tmgameimport->CheckIfGameIdIsUploaded($gameid,"winners");
                                                /*if(count($isUploaded) == 0)
                                                {*/
                                                    if($productid != "")
                                                    {
                                                        //checking if the provider assigned to game number is the same as he chosen provider
                                                        if($productgameinfo[0]["ProviderID"] != $providerid)
                                                        {
                                                            $errormsgtitle = "ERROR!";
                                                            $errormsg = "Provider assigned to game number do not match chosen provider.";
                                                        }
                                                        else
                                                        {
                                                            //generate batch id
                                                            $batch = $tmgamebatches->SelectBatchID($gameid,2);
                                                            if(count($batch) > 0)
                                                            {
                                                                $batches = $tmgamebatches->SelectMaxBatchID($gameid,2);
                                                                $gamebatchnum = ($batches[0]["maxbatchid"] + 1);
                                                            }
                                                            else
                                                            {
                                                                $gamebatchnum = 1;
                                                            }

                                                            $checkinventoryfile = $tmgamebatches->CheckInventoryUpload($gameid, $gamebatchnum, 1);
                                                            if($checkinventoryfile[0]["gamebatch"] > 0)
                                                            {
                                                                $hiddenfilename->Text = $filename;
                                                                $hiddengameid->Text = $gameid;
                                                                $hiddenproductid->Text = $productid;
                                                                $hiddenproviderid->Text = $providerid;
                                                                $hiddenprodname->Text = $productgameinfo[0]["ProductName"];
                                                                $hiddenprovidername->Text = $providername[0]["Name"];
                                                                $hiddengamenum->Text = $productgameinfo[0]["GameNumber"];
                                                                $hiddengamecode->Text = $gamecode;
                                                                $hiddenbatchid->Text = $gamebatchnum;
                                                                if (count($isUploaded) > 0)
                                                                {
                                                                    $confirmuploadmsg = "Are you sure you want to upload another file for this game number?";
                                                                    $confirmuploadtitle = "CONFIRMATION";
                                                                }
                                                                else
                                                                {
                                                                    list($msgtitle,$msg) = UploadWinnersXmlFile($filename,$gameid,$productid,$providerid,$arrXml,$xmlname,$providername[0]["Name"],$productgameinfo[0]["ProductName"],$productgameinfo[0]["GameNumber"],$gamecode,$gamebatchnum);
                                                                    $countmsgtitle = $msgtitle;
                                                                    $countmsg = $msg;
                                                                    echo $countmsg;
                                                                }
                                                            }
                                                            else
                                                            {
                                                                $errormsgtitle = "ERROR!";
                                                                $errormsg = "Kindly upload the inventory file first before proceeding.";
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                            $errormsgtitle = "ERROR!";
                                                            $errormsg = "Product id do not exists.";
                                                    }
                                                /*}
                                                else
                                                {
                                                    $errormsgtitle = "ERROR!";
                                                    $errormsg = "You cannot proceed because winning cards were already uploaded for this game number.";
                                                }*/
                                            }
                                            else
                                            {
                                                    $errormsg = "Game number do not exists. Please ensure that the filename is correct.";
                                                    $errormsgtitle = "ERROR!";
                                            }
                                        }
                                        else
                                        {
                                            $errormsg = "Root node and game number in the file name do not match.";
                                            $errormsgtitle = "ERROR!";
                                        }
                                    }
                                }
                                }
                                else
                                {
                                    $errormsg = "Incorrect file name format. Please upload a valid file name format.";
                                    $errormsgtitle = "ERROR!";
                                } 
                            }
                        }
                //}
            }
        }
        else
        {
            $errormsg = "Incorrect file format. Please upload a valid file format.";
            $errormsgtitle = "ERROR!";
        }
    }

    if($btnConfirmUpload->SubmittedValue == "Yes")
    {
        $fullpath = "../xml/winners/" . $hiddenfilename->SubmittedValue;
        $xmlUrl = $fullpath;
        $xml = simplexml_load_file($fullpath);
        $xmlStr = file_get_contents($xmlUrl);
        $xmlObj = simplexml_load_string($xmlStr);
        $arrXml = objectsIntoArray($xmlObj);
        $xmlname = $xml->getName();

        list($msgtitle,$msg) = UploadWinnersXmlFile($hiddenfilename->SubmittedValue,$hiddengameid->SubmittedValue,$hiddenproductid->SubmittedValue,$hiddenproviderid->SubmittedValue,$arrXml,$xmlname,$hiddenprovidername->SubmittedValue,$hiddenprodname->SubmittedValue,$hiddengamenum->SubmittedValue,$hiddengamecode->SubmittedValue,$hiddenbatchid->SubmittedValue);
        $countmsgtitle = $msgtitle;
        $countmsg = $msg;
    }
}

//function for checking if xml array have empty nodes
function in_array_r($needle, $haystack) {
    $emptynodeskey = array();
    foreach ($haystack as $key => $item) {
        if ($item === $needle || (is_array($item) && in_array_r($needle, $item))) {
            //return true;
            $emptynodeskey[] = $key;

        }
    }
    //return false;
    return $emptynodeskey;
}

//function for checking xml structure
function display_xml_error($error)
{
    $return = "";
    //$return .= $error->column . "\n";

    switch ($error->level) {
        case LIBXML_ERR_WARNING:
            $return .= "Warning $error->code: ";
            break;
         case LIBXML_ERR_ERROR:
            $return .= "Error $error->code: ";
            break;
        case LIBXML_ERR_FATAL:
            $return .= "Fatal Error $error->code: ";
            break;
    }

    $return .= trim($error->message) .
               "<br/>  Line: $error->line" .
               "<br/>  Column: $error->column";

    if ($error->file) {
        $return .= "</br>  File: $error->file";
    }

    return addslashes("$return<br/>");
}

//function for converting xml to array
function objectsIntoArray($arrObjData, $arrSkipIndices = array())
{
    $arrData = array();

    // if input is object, convert into array
    if (is_object($arrObjData)) {
        $arrObjData = get_object_vars($arrObjData);
    }

    if (is_array($arrObjData)) {
        foreach ($arrObjData as $index => $value) {
            if (is_object($value) || is_array($value)) {
                $value = objectsIntoArray($value, $arrSkipIndices); // recursive call
            }
            if (in_array($index, $arrSkipIndices)) {
                continue;
            }
            $arrData[$index] = $value;
        }
    }
    return $arrData;
}

//function in parsing xml data
function UploadWinnersXmlFile($filename,$gameid,$productid,$providerid,$arrXml,$xmlname,$providername,$productname,$gamenum,$gamecode,$gamebatchnum)
{
    $fullpath = "../xml/winners/" . $filename;
    
    list($tmgameimport,$tmproducts,$tmgames,$auditlog,$tmwinnings2,$tmprizes,$tmprizestemp,$tmproviders,$tmgamebatches,$tmbatchtranslog) = DeclareClasses();

    $totalprizescount = 0;
    $deletewinningsaffectedrow = 0;
    $deleteprizesaffectedrow = 0;
    $totalwinningscount = 0;
    $msgcode = 0;

    /* STEP 1:
     * Insert game batch id
     */
    $gamebatchid = InsertGameBatchID($gameid,$gamebatchnum);
    
    /* STEP 2:
     * insert uploaded xml in the table
     */
    list($gameimportid,$msg,$stat) = CreateGameImport($providerid,$gameid,$filename,$fullpath,$productid);
    if ($stat == 1)
    {
        $errormsg = $msg;
        $errormsgtitle = "ERROR!";
        $msgcode = 1;
    }
    $id = $gameimportid;

    /* STEP 3:
     * insert audit trail log
     */
    list($msg,$stat) = InsertAuditTrailLog($gameimportid,26);
    if($stat == 1)
    {
        $errormsgtitle = "ERROR!";
        $errormsg = $msg;
        $msgcode = 1;
    }
    
    if($xmlname == "$gamecode")
    {
        list($status,$csv_content,$winnersdtls,$xml_content_count) = CreateCSVContent($arrXml,$productid,$gamecode,$gamebatchid);
        
        $imax = 0;

        $winnersdtls_count = $winnersdtls;
        if($status != 2)
        {
            do{
                /* STEP 4:
                 * truncate prizetemp
                 * insert into CSV file
                 */
                $tmprizestemp->TruncatePrizesTemp();
                $fp = fopen('../csv/winners.csv', 'w');
                fwrite($fp,$csv_content);
                fclose($fp);
                $csv_content = null;

                //insert to inventorytemp using csv file
                $tmprizestemp->InsertWinnersUsingCSVFile('../csv/winners.csv', ';', '<br>');

                /* STEP 5:
                 * check if prize exists
                 */
                $tmprizestemp->CheckIfPrizeAlreadyExist($productid);
                
                /* Step 6:
                 * Get all consolation prizes that does not yet exists and insert to prizes
                 */
                $tmprizes->InsertNewConsolationPrizes($productid,$_SESSION['acctid'],$winnersdtls_count,$gamebatchid);
                if($tmprizes->HasError)
                {
                    $errormsgtitle = "ERROR!";
                    $errormsg = "Error while inserting winning cards: " . $tmprizes->getError();
                    $msgcode = 1;
                }
                $firstprizebatch = $tmprizes->AffectedRows;
                
                /* Step 7:
                 * Get all jackpot prizes that does not yet exists and insert to prizes
                 */
                //$tmprizes->InsertNewJackpotPrizes($ddlProducts->SubmittedValue,$_SESSION['acctid']);
                //$secondprizebatch = $tmprizes->AffectedRows;
                $secondprizebatch = 0;

                /* Step 8:
                 * Insert to winning cards all prizes on step 3 and 4
                 */
                $tmwinnings2->InsertWinnings(0,$productid,$_SESSION['acctid'],$winnersdtls_count,$gamebatchid);
                if($tmwinnings2->HasError)
                {
                    $errormsgtitle = "ERROR!";
                    $errormsg = "Error while inserting winning cards: " . $tmwinnings2->getError();
                    $msgcode = 1;
                }
                $firstbatch = $tmwinnings2->AffectedRows;
                
                /* Step 9:
                 * Insert to winning cards all existing prizes
                 */
                $tmwinnings2->InsertWinnings(1,$productid,$_SESSION['acctid'],$winnersdtls_count,$gamebatchid);
                if($tmwinnings2->HasError)
                {
                    $errormsgtitle = "ERROR!";
                    $errormsg = "Error while inserting winning cards: " . $tmwinnings2->getError();
                    $msgcode = 1;
                }
                $secondbatch = $tmwinnings2->AffectedRows;
                
                /* Step 10:
                 * truncate prizetemp
                 */
                $tmprizestemp->TruncatePrizesTemp();

                /* Step 11:
                 * Delete all inserted winningcards and prizes if total inserted is not equal to xml count
                 */
                //count of all winnings uploaded
                $totalwinningscount = ($firstbatch + $secondbatch);
                
                //count of all prizes uploaded
                $totalprizescount = ($firstprizebatch + $secondprizebatch);
                
                if($totalwinningscount != $xml_content_count)
                {
                    $gamebatchid = DeleteAllAffectedRows($totalwinningscount,$totalprizescount,$gamebatchid);
                }

                $imax++;

                if ($imax > 2)
                {
                        $status = 2;
                        break;
                }
                else
                {
                        $status = 1;
                }

            }while($totalwinningscount != $xml_content_count);
        }
      
        list($msg,$stat) = UpdateGameImport($status,$id,$gamebatchid);
        if($stat == 1)
        {
            $errormsgtitle = "ERROR!";
            $errormsg = $msg;
            $msgcode = 1;
        }

        /* Step 12:
         * Insert to audit trail log
         */
        list($msg,$stat) = InsertAuditTrailLog($id,27);
        if($stat == 1)
        {
            $errormsgtitle = "ERROR!";
            $errormsg = $msg;
            $msgcode = 1;
        }

        /* Step 13:
         * Insert to batch transaction log
         */
        list($msg,$stat) = InsertBatchTransactionLog($gameid,$gamebatchnum,$filename,$status);
        if ($stat == 1)
        {
            $errormsgtitle = "ERROR!";
            $errormsg = $msg;
            $msgcode = 1;
        }

        if($status != 2)
        {
            $countmsgtitle = "Winners File Summary";
            $countmsg = "Provider: " . $providername . "<br/>" .
                            "Game Name: " . $productname . "<br/>" .
                            "Game Number: " . $gamenum. "<br/>" .
                            "Total No. of Prizes: " . number_format($totalprizescount) . "<br/>" .
                            "Total No. of VIRN: " . number_format($totalwinningscount);
        }
        else
        {
            $countmsgtitle = "ERROR!";
            $countmsg = "Uploading was unsuccessful . Please try again.";
        }
        
    }
    else
    {
        $errormsgtitle = "ERROR!";
        $errormsg = $filename . " could not be processed.";
    }
    /*End of parse data*/

    if($msgcode == 0)
    {
        $msgtitle = $countmsgtitle;
        $msg = $countmsg;
    }
    else
    {
        $msgtitle = $errormsgtitle;
        $msg = $errormsg;
    }
    $msgtitle = $countmsgtitle;
    $msg = $countmsg;
    return array($msgtitle,$msg);
}

function DeclareClasses()
{
    $tmgameimport = new TMGameImport();
    $tmproducts = new TMProducts();
    $tmgames = new TMGameManagement();
    $auditlog = new TMAuditLog();
    $tmwinnings2 = new TMWinnings();
    $tmprizes = new TMPrizes();
    $tmprizestemp = new TMPrizesTemp();
    $tmproviders = new TMProvider();
    $tmgamebatches = new TMGameBatches();
    $tmbatchtranslog = new TMBatchTransactionLog();

    return array($tmgameimport,$tmproducts,$tmgames,$auditlog,$tmwinnings2,$tmprizes,$tmprizestemp,$tmproviders,$tmgamebatches,$tmbatchtranslog);
}

function InsertGameBatchID($gameid,$gamebatchnum)
{
    list($tmgameimport,$tmproducts,$tmgames,$auditlog,$tmwinnings2,$tmprizes,$tmprizestemp,$tmproviders,$tmgamebatches,$tmbatchtranslog) = DeclareClasses();

    $gamebatch["GameID"] = $gameid;
    $gamebatch["BatchID"] = $gamebatchnum;
    $gamebatch["XMLType"] = 2;
    $tmgamebatches->Insert($gamebatch);
    $gamebatchid = $tmgamebatches->LastInsertID;

    return $gamebatchid;
}

function CreateGameImport($providerid,$gameid,$filename,$fullpath,$productid)
{
    list($tmgameimport,$tmproducts,$tmgames,$auditlog,$tmwinnings2,$tmprizes,$tmprizestemp,$tmproviders,$tmgamebatches,$tmbatchtranslog) = DeclareClasses();

    $tmimport["ProviderID"] = $providerid;
    $tmimport["GameID"] = $gameid;
    $tmimport["FileName"] = $filename;
    $tmimport["FilePath"] = $fullpath;
    $tmimport["ProductID"] = $productid;
    $tmimport["DateUploaded"] = 'now_usec()';
    $tmimport["Status"] = '0';
    $tmgameimport->Insert($tmimport);
    $gameimportid = $tmgameimport->LastInsertID;
    if ($tmgameimport->HasError)
    {
        $msg = $tmgameimport->getError();
        $stat = 1;
    }
    else
    {
        $msg = "";
        $stat = 0;
    }

    return array($gameimportid,$msg,$stat);
}

function InsertAuditTrailLog($gameimportid,$audittrailfunctionid)
{
    list($tmgameimport,$tmproducts,$tmgames,$auditlog,$tmwinnings2,$tmprizes,$tmprizestemp,$tmproviders,$tmgamebatches,$tmbatchtranslog) = DeclareClasses();

    $auditdtls["SessionID"] = $_SESSION['sid'];
    $auditdtls["AID"] = $_SESSION['acctid'];
    $auditdtls["TransDetails"] = "GameImport ID: " . $gameimportid;
    $auditdtls["TransDateTime"] = "now_usec()";
    $auditdtls["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
    $auditdtls["AuditTrailFunctionID"] = $audittrailfunctionid;
    $insertauditlog = $auditlog->Insert($auditdtls);
    if($auditlog->HasError)
    {
        $msg = "Error has occured:" . $auditlog->getError();
        $stat = 1;
    }
    else
    {
        $msg = "";
        $stat = 0;
    }

    return array($msg,$stat);
}

function CreateCSVContent($arrXml,$productid,$gamecode,$gamebatchid)
{
    list($tmgameimport,$tmproducts,$tmgames,$auditlog,$tmwinnings2,$tmprizes,$tmprizestemp,$tmproviders,$tmgamebatches,$tmbatchtranslog) = DeclareClasses();

    $imax1 = 0;
    $csv_content = "";
    $xml_content = $arrXml["ticket"];
    $xml_content_count = count($xml_content);
    do{
        $winnersdtls = null;
        for($xml = 0 ; $xml < count($arrXml) ; $xml++)
        {
            for($i = 0 ; $i < count($arrXml["ticket"]) ; $i++)
            {
                $winners["ProductID"] = $productid;
                $winners["AccountAID"] = $_SESSION["acctid"];
                $winners["Description"] = trim($xml_content[$i]["prize_value"]);
                $winners["VIRN"] = trim($xml_content[$i]["VIRN"]);
                $winners["IsExists"] = "0";
                $winners["GameNumber"] = substr($gamecode, 2, strlen($gamecode));
                $winners["GameBatchID"] = $gamebatchid;
                $csv_content .= implode(";", $winners) . "<br>";
                $winnersdtls++;
            }
        }

        $imax1++;

        if ($imax1 > 2)
        {
            $status = 2;
            break;
        }
        else
        {
            $status = 1;
        }
    }while($winnersdtls != count($xml_content));

    return array($status,$csv_content,$winnersdtls,$xml_content_count);
}

function DeleteAllAffectedRows($totalwinningscount,$totalprizescount,$gamebatchid)
{
    list($tmgameimport,$tmproducts,$tmgames,$auditlog,$tmwinnings2,$tmprizes,$tmprizestemp,$tmproviders,$tmgamebatches,$tmbatchtranslog) = DeclareClasses();

    $lastrecord = $tmwinnings2->GetLastWinningCardID();
    $lastprizerecord = $tmprizes->GetLastWinningCardID();
    if($lastrecord[0]["LastID"] != NULL)
    {
        $start = ($lastrecord[0]["LastID"] - $totalwinningscount);
        $deletealltransactions = $tmwinnings2->DeleteAllTransactions($start,$lastrecord[0]["LastID"]);
    }

    if($lastprizerecord[0]["LastID"] != NULL)
    {
        $start = ($lastprizerecord[0]["LastID"] - $totalprizescount);
        $deletealltransactions = $tmprizes->DeleteAllTransactions($start,$lastprizerecord[0]["LastID"]);
    }

    //delete game batches info
    $tmgamebatches->DeleteByGameBatchesID($gamebatchid);
    $gamebatchid = 0;

    return $gamebatchid;
}

function UpdateGameImport($status,$id,$gamebatchid)
{
    list($tmgameimport,$tmproducts,$tmgames,$auditlog,$tmwinnings2,$tmprizes,$tmprizestemp,$tmproviders,$tmgamebatches,$tmbatchtranslog) = DeclareClasses();

    $xmlstatus["Status"] = $status;
    $xmlstatus["DateImported"] = "now_usec()";
    $xmlstatus["GameImportID"] = $id;
    $xmlstatus["GameBatchID"] = $gamebatchid;
    $updatestatus = $tmgameimport->UpdateByArray($xmlstatus);
    if($tmgameimport->HasError)
    {
        $msg = "Error:" . $tmgameimport->getError();
        $stat = 1;
    }
    else
    {
        $msg = "";
        $stat = 0;
    }

    return array($msg,$stat);
}

function InsertBatchTransactionLog($gameid,$gamebatchnum,$filename,$status)
{
    list($tmgameimport,$tmproducts,$tmgames,$auditlog,$tmwinnings2,$tmprizes,$tmprizestemp,$tmproviders,$tmgamebatches,$tmbatchtranslog) = DeclareClasses();

    $batchtrans["GameID"] = $gameid;
    $batchtrans["BatchID"] = $gamebatchnum;
    $batchtrans["FileName"] = $filename;
    $batchtrans["TransactionDate"] = "now_usec()";
    $batchtrans["TransactionType"] = 1;
    $batchtrans["ProcessedByAID"] = $_SESSION["acctid"];
    $batchtrans["Status"] = $status;
    $tmbatchtranslog->Insert($batchtrans);
    if($tmbatchtranslog->HasError)
    {
        $msg = "Error has occured:" . $tmbatchtranslog->getError();
        $stat = 1;
    }
    else
    {
        $msg = "";
        $stat = 0;
    }

    return array($msg,$stat);
}
?>