<?php
/* 
 * Created by Arlene R. Salazar on Sept. 12 , 2011
 * Purpose: For Winners XML parsing
 */
$pagesubmenuid = 32;
$stylesheets[] = "../css/default.css";
$javascripts[] = "../jscripts/validations.js";

App::LoadModuleClass("TicketManagementCM", "TMGameImport");
App::LoadModuleClass("TicketManagementCM", "TMWinnings2");
App::LoadModuleClass("TicketManagementCM", "TMPrizes");
App::LoadModuleClass("TicketManagementCM", "TMAuditLog");

$tmgameimport = new TMGameImport();
$tmwinnings2 = new TMWinnings2();
$tmprizes = new TMPrizes();
$auditlog = new TMAuditLog();

$where = " WHERE Status = 0";
$xmlfiles = $tmgameimport->SelectByWhere($where);

if(count($xmlfiles) > 0)
{
    $totalprizescount = 0;
    for($ctr = 0 ; $ctr < count($xmlfiles) ; $ctr++)
    {
        $filename = explode("_", $xmlfiles[$ctr]["FileName"]);
        if($filename[1] == "WLIST")
        {
            $productid = $xmlfiles[$ctr]["ProductID"];
            $gameid = $xmlfiles[$ctr]["GameID"];
            $id = $xmlfiles[$ctr]["GameImportID"];
            
            //read xml file
            $gamecode = $filename[0];
            $xml = simplexml_load_file($xmlfiles[$ctr]["FilePath"]);
            $xmlUrl = $xmlfiles[$ctr]["FilePath"];
            $xmlStr = file_get_contents($xmlUrl);
            $xmlObj = simplexml_load_string($xmlStr);
            $arrXml = objectsIntoArray($xmlObj);
            $xmlname = $xml->getName();

            if($xmlname == "$gamecode")
            {        
                //foreach ($arrXml as $key => $value)
                for($xml = 0 ; $xml < count($arrXml) ; $xml++)
                {
                    $success_ctr = 0;
                    $xml_content = $arrXml["TICKET"];
                    do{
                        for($i = 0 ; $i < count($arrXml["TICKET"]) ; $i++)
                        {
			    $where = " WHERE Description = '" . $xml_content[$i]["PRIZE_VALUE"] . "'";
			    $prizeid = $tmprizes->SelectByWhere($where);
			    $winnings["ProductID"] = $productid;
			    $winnings["GameID"] = $gameid;
			    $winnings["PrizeID"] = $prizeid[0]["PrizeID"];
			    $winnings["PrizeName"] = $xml_content[$i]["PRIZE_VALUE"];
			    $winnings["VIRN"] = $xml_content[$i]["VIRN"];
                            $winnings_dtls = $tmwinnings2->Insert($winnings);
                            if($tmwinnings2->HasError)
                            {

                            }
                            else
                            {
  
                                $success_ctr++;
                            }
                        }

                        if($success_ctr != count($arrXml["TICKET"]))
                        {
                            do{
                                $start = ($tmwinnings2->LastInsertID - $success_ctr);
                                $deletealltransactions = $tmwinnings2->DeleteAllTransactions($start,$tmwinnings2->LastInsertID);
                                $deletewinningsaffectedrow = ($success_ctr - $tmwinnings2->AffectedRows);
                            //}while($tmwinnings2->AffectedRows != $success_ctr);
                            }while($deletewinningsaffectedrow != 0);
                        }
                    }while($success_ctr != count($xml_content));
                }

	        //count of all prizes uploaded
                $totalprizescount = ($totalprizescount + $success_ctr);

		$status["Status"] = "1";
		$status["DateImported"] = "now_usec()";
		$status["GameImportID"] = $id;
		$updatestatus = $tmgameimport->UpdateByArray($status);
                if($tmgameimport->HasError)
                {
                    $message = "Error:" . $tmgameimport->getError();
                }

		//audit trail log
		$auditdtls["SessionID"] = session_id();
		$auditdtls["AID"] = 1; //since this will be executed by a cron job, there will be no $_SESSION['acctid'] created
		$auditdtls["TransDetails"] = "GameImport ID: " . $id;
		$auditdtls["TransDateTime"] = "now_usec()";
		$auditdtls["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
		$auditdtls["AuditTrailFunctionID"] = 22;
		$insertauditlog = $auditlog->Insert($auditdtls);
                if($auditlog->HasError)
                {
		    $confadduser_title = "ERROR!";
		    $confadduser_msg = "Error has occured:" . $tmaccount->getError();
                }
	    }
	    else
            {
                $message = $xmlfiles[$ctr]["FileName"] . " could not be processed.";
            }
        }
    }
    $countmessage = $totalprizescount . " prizes was successfully uploaded.";
}


function objectsIntoArray($arrObjData, $arrSkipIndices = array())
{
    $arrData = array();

    // if input is object, convert into array
    if (is_object($arrObjData)) {
        $arrObjData = get_object_vars($arrObjData);
    }

    if (is_array($arrObjData)) {
        foreach ($arrObjData as $index => $value) {
            if (is_object($value) || is_array($value)) {
                $value = objectsIntoArray($value, $arrSkipIndices); // recursive call
            }
            if (in_array($index, $arrSkipIndices)) {
                continue;
            }
            $arrData[$index] = $value;
        }
    }
    return $arrData;
}
?>