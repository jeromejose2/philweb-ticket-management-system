<?php
/*
 * @author
 * Purpose   : view for accountslist
 */
include("../init.inc.php");
include_once("../controller/managesession.php");
$pagesubmenuid = 1;
?>
<html>
    <head>
        <link rel="stylesheet" type="text/css" media="screen" href="../css/default.css" />
    </head>
    <body>
        <div id="fade" class="black_overlay"></div>
        <!-- <table width="1000" cellpadding="0" cellspacing="0" border="1">
            <tr>
                <td colspan="2" align="right">
                    <?php include("header.php"); ?>
                </td>
                <td> -->
		    <div class="content-page">
                    <?php include("acctlist.php"); ?>
  		    </div>
               <!--  </td>
            </tr>
        </table>	-->
	<!-- POP UP FOR MESSAGES -->
        <div id="light5" class="white_content">
            <div id="title5" class="light-title"></div>
            <div id="msg5" class="light-message"></div>
            <div id="button" class="light-button"><input type="button" onclick="javascript: document.getElementById('light5').style.display='none';document.getElementById('fade').style.display='none';" value="OKAY"/></div>
	</div>
        <!-- POP UP FOR MESSAGES -->
    </body>
</html>

