<?php
/*
 * Added By : Arlene R. Salazar
 * Added On : Aug 24, 2011
 * Purpose : View for account list
 */
require_once ("init.inc.php");
$pagesubmenuid = 1;
include_once("../controller/managesession.php");
include("../controller/acctlistprocess.php");

?>
<script type="text/javascript" src="jscripts/validations.js"></script>
<script language="javascript">
    function SelectedIndexChange(index)
    {
        selectedindex = document.getElementById("hdnSelectedProviderID");
        selectedindex.value = index;
        document.forms[0].submit();
    }
    function ChangePage(pagenum)
    {
        selectedindex = document.getElementById("pgSelectedPage");
        selectedindex.value = pagenum;
        document.forms[0].submit();
    }
</script>
<?php include("header.php"); ?>
<div id="fade" class="black_overlay"></div>
<div id="loading" class="loading"></div>
<!-- POP UP FOR MESSAGES -->
<div id="light5" class="white_content">
    <div id="title5" class="light-title"></div>
    <div id="msg5" class="light-message"></div>
    <div id="button" class="light-button"><input type="button" onclick="javascript: document.getElementById('light5').style.display='none';document.getElementById('fade').style.display='none';" value="OKAY"/></div>
<div class="light-footer"></div>
</div>
<!-- POP UP FOR MESSAGES -->
<form method="post" name="frmAcctList">
    <div class="titleCont">
            <div class="titleCont_left"></div>
            <div class="titleCont_body">Account List</div>
            <div class="titleCont_right"></div>
    </div>
    <div class="content-page">
		<div class="search-container">
		    <div class="form-view">
		        &nbsp;&nbsp;<?php echo $ddlsrchaccttype;?> &nbsp;<?php echo $ddlStatus;?>&nbsp;<?php echo $btnSubmit;?>
		    </div>
		    <div class="form-search">
		        <?php echo $txtsrchuname;?><?php echo $btnSearch;?>
		    </div>
		</div>
		<div class="form-actions">
		    <div class="form-button"><?php echo $btnAddAcct;?></div>
		</div>
    	<?php echo $hiddenctr;?>
		<div class="form-page">
		    <?php echo isset($accounts_list) ? $pgTransactionHistory : "";?>
		</div>
    </div>
    </form>
<?php if(isset($accounts_list)):?>
<form method="post" name="frmAccountList" id="frmAccountList" action="updateuser.php">
    <div class="content-page">
    <?php echo $hiddenaccountid;?>
    <br/><table class="table-list">
    <tr>
	<th>Edit</th>
        <th>Username</th>
        <th>Account Type</th>
        <th>Full Name</th>
        <th>Address</th>
        <th>Contact Number</th>
        <th>Email Address</th>
	<th>Status</th>
    </tr>
    <?php if(count($accounts_list) > 0):?>
    <?php for($ctr = 0 ; $ctr < count($accounts_list) ; $ctr++):?>
    <?php ($ctr % 2) == 0 ? $class = "evenrow" : $class = "oddrow" ; ?>
    <tr class = "<?php echo $class?>">
	<td><a href="#" onclick="javascript: return editAccount(<?php echo $accounts_list[$ctr]['AID']?>);">Edit</a></td>
        <td><?php echo $accounts_list[$ctr]['UserName']?></td>
        <td><?php echo $accounts_list[$ctr]['AccountName']?></td>
        <td><?php echo $accounts_list[$ctr]['Name']?></td>
        <td><?php echo $accounts_list[$ctr]['Address']?></td>
        <td><?php echo $accounts_list[$ctr]['MobileNumber']?></td>
        <td><?php echo $accounts_list[$ctr]['Email']?></td>
	<td><?php if ($accounts_list[$ctr]['Status'] == 1) { echo "Active"; }
                  else if ($accounts_list[$ctr]['Status'] == 4) { echo "Locked"; }
                  else if ($accounts_list[$ctr]['Status'] == 3) { echo "Locked Invalid Attempts"; }?></td>
    </tr>
    <?php endfor;?>
    <?php else:?>
    <tr class="no-record"><td colspan="8">No records to display</td></tr>
    <?php endif;?>
</table>
<div class="form-page">
    <?php echo $pgTransactionHistory;?>
</div>
<?php if(isset($_SESSION['updateuser_message'])): ?>
    <script>
        document.getElementById('title5').innerHTML = "<?php echo $_SESSION['updateuser_title'];?>";
        document.getElementById('msg5').innerHTML = "<?php echo $_SESSION['updateuser_message'];?>";
        document.getElementById('light5').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
    </script>
<?php endif; ?>
<?php unset($_SESSION['updateuser_message']);?>
</div>
</form>
<?php endif;?>
<?php include("footer.php"); ?>