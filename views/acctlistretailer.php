<?php
/* 
 * Added By: Arlene R. Salazar on 09/05/2011
 * Purpose: For account list of Product Manager
 */
require_once ("init.inc.php");
$pagesubmenuid = 28;
include_once("../controller/managesession.php");
include("../controller/acctlistretailerprocess.php");
?>
<script type="text/javascript" src="jscripts/validations.js"></script>
<script language="javascript">
    function SelectedIndexChange(index)
    {
        selectedindex = document.getElementById("hdnSelectedProviderID");
        selectedindex.value = index;
        document.forms[0].submit();
    }
    function ChangePage(pagenum)
    {
        selectedindex = document.getElementById("pgSelectedPage");
        selectedindex.value = pagenum;
        document.forms[0].submit();
    }
</script>
<div id="fade" class="black_overlay"></div>
<!-- POP UP FOR CONFIRMATION MESSAGES -->
<div id="light5" class="white_content">
    <div id="title5" class="light-title"></div>
    <div id="msg5" class="light-message"></div>
    <div id="button5" class="light-button">
        <input type="button" id="btnOk" name="btnOk" value="Okay" onclick="javascript: document.getElementById('light5').style.display = 'none';document.getElementById('fade').style.display = 'none';"/>
    </div>
    <div class="light-footer"></div>
</div>
<!-- POP UP FOR CONFIRMATION MESSAGES -->
<?php include("header.php"); ?>
    <div class="titleCont">
            <div class="titleCont_left"></div>
            <div class="titleCont_body">Account List</div>
            <div class="titleCont_right"></div>
    </div>
    <div class="content-page">
        <form method="post" name="frmAcctListRetailer">
       	<div class="search-container">
			<div class="form-view">
				<?php echo $ddlsrchaccttype;?> &nbsp;<?php echo $ddlStatus;?>&nbsp;<?php echo $btnSubmit;?>
			</div>
         	<div class="form-search">
                <?php echo $txtsrchuname;?><?php echo $btnSearch;?>
            </div>
       </div>
            <div class="form-actions">
                <div class="form-button"><?php echo $btnAddAcct;?></div>
            </div>
            <?php echo $hiddenctr;?>
            <div class="form-page">
                <?php echo isset($accounts_list) ? $pgTransactionHistory : "";?>
            </div>
        </form>
   </div>
<?php if(isset($accounts_list)):?>
<form method="post" name="frmAccountList" id="frmAccountList" action="updateuserretailer.php">
    <?php echo $hiddenaccountid;?>
    <br/><table class="table-list">
        <tr>
	    <th>Edit</th>
            <th>Username</th>
            <th>Account Type</th>
            <th>Full Name</th>
            <th>Address</th>
            <th>Contact Number</th>
            <th>Email Address</th>
	    <th>Status</th>
        </tr>
        <?php if(count($accounts_list) > 0):?>
        <?php for($ctr = 0 ; $ctr < count($accounts_list) ; $ctr++):?>
        <?php ($ctr % 2) == 0 ? $class = "evenrow" : $class = "oddrow" ; ?>
        <tr class = "<?php echo $class?>">
	    <td><a href="#" onclick="javascript: return editAccount(<?php echo $accounts_list[$ctr]['AID']?>);">Edit</a></td>
            <td><?php echo $accounts_list[$ctr]['UserName']?></td>
            <td><?php echo $accounts_list[$ctr]['AccountName']?></td>
            <td><?php echo $accounts_list[$ctr]['Name']?></td>
            <td><?php echo $accounts_list[$ctr]['Address']?></td>
            <td><?php echo $accounts_list[$ctr]['MobileNumber']?></td>
            <td><?php echo $accounts_list[$ctr]['Email']?></td>
	    <td><?php echo ($accounts_list[$ctr]['Status'] == 1) ? "Active" : "Locked"; ?></td>
        </tr>
        <?php endfor;?>
        <?php else:?>
        <tr class="no-record"><td colspan="8">No records to display</td></tr>
        <?php endif;?>
    </table>
	<div class="content-page">
		<div class="form-page">
			<?php echo $pgTransactionHistory;?>
		</div>
	</div>
    <?php if(isset($_SESSION['updateuser_message'])): ?>
        <script>
            document.getElementById('title5').innerHTML = "<?php echo $_SESSION['updateuser_title'];?>";
            document.getElementById('msg5').innerHTML = "<?php echo $_SESSION['updateuser_message'];?>";
            document.getElementById('light5').style.display = 'block';
            document.getElementById('fade').style.display = 'block';
        </script>
    <?php endif; ?>
    <?php unset($_SESSION['updateuser_message']);?>
    </form>
<?php endif;?>
</div>
<?php include("footer.php"); ?>