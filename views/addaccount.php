<?php
/*
 * @author
 * Purpose   : view for addaccount
 */
include("../init.inc.php");
include_once("../controller/managesession.php");
$pagesubmenuid = 1;
?>
<html>
    <head>
        
    </head>
    <body>
	<div id="fade" class="black_overlay"></div>
        <table width="1000" cellpadding="0" cellspacing="0" border="1">
            <tr>
                <td colspan="2" align="right">
                    <?php include("header.php"); ?>
                </td>
            </tr>
            <tr>
                <td align="top" width="250">
                    Left Navigation
                    <?php include("navigation.php"); ?>
                </td>
                <td>
                    <?php include("adduser.php"); ?>
                </td>
            </tr>
        </table>
    </body>
</html>

