<?php
/*
 * Created by Arlene R. Salazar on 09-15-2011
 * Purpose: controller for add game
 */
require_once ("init.inc.php");
$pagesubmenuid = 18;
include_once("../controller/managesession.php");
include("../controller/addgameprocess.php");
?>
<?php include("header.php"); ?>
<script>
        function enabletxtcardval()
        {
            $("#txtCardValue").attr('disabled', false);  
            $("#txtBookSize").attr('disabled', false);  
        }
        
	function get_products()
        {
            var provid = document.getElementById('ddlProvider').options[document.getElementById('ddlProvider').selectedIndex].value;
            if(provid == "")
            {   
                document.getElementById('txtGameNumber').value = "";
                document.getElementById("txtCardValue").value = "";  
                document.getElementById('txtNewGameName').value = "";
                document.getElementById('txtBookSize').value = "";
                document.getElementById('txtNewGameName').disabled = true;
                document.getElementById("txtCardValue").disabled = true;
                document.getElementById('txtBookSize').disabled = true;
            }
            else
            {
                document.getElementById('txtNewGameName').disabled = false;
                $("#txtCardValue").attr('disabled', false);
                $("#txtBookSize").attr('disabled', false);
            }
            
            $('#ddlProduct').load(
                '../controller/get_product.php?provid=' + provid,
                function(r, status,xhr)
                {
                    if (status == "error") {
                            document.getElementById('title').innerHTML= 'ERROR!';
                            document.getElementById('msg').innerHTML = "Sorry but an error has occured : " + xhr.status + " " + xhr.statusText;
                            document.getElementById('light').style.display = 'block';
                            document.getElementById('fade').style.display = 'block';
                    }
                }
            );
                    
        }
        
        function get_products_cardvalue()
        {
            var prodid = document.getElementById('ddlProduct').options[document.getElementById('ddlProduct').selectedIndex].value;
             
            $("#txtCardValue").load(
                    "../controller/get_gamecardvalue.php",
                    {
                        gamename: prodid,
                        flag: 'addgame'
                    },

                    function(r, status, xhr)
                    {
                        if (status == "success")
                        {
                            var json = $.parseJSON(r);
                            $('#txtCardValue').val(json.CardPrice);
                            $('#txtBookSize').val(json.BookTicketCount);
                            $('#hiddenCardValue').val(json.CardPrice);
                            $('#hiddenBookSize').val(json.BookTicketCount);
                            $('#hiddenGameNumber').val($('#txtGameNumber').val());
                            
                            var value = document.getElementById('txtCardValue').value;
                            if (value > 0)
                            {
                                $("#txtCardValue").attr('disabled',true);
                                $("#txtBookSize").attr('disabled',true);
                                $("#txtNewGameName").attr('disabled',true);
                            }
                        }
                        
                        if (status == "error")
                        {
                            document.getElementById('title').innerHTML = "ERROR!";
                            document.getElementById('msg').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
                            document.getElementById('light').style.display = "block";
                            document.getElementById('fade').style.display = "block";
                        }
                    }
            ); 
                
            if (prodid == "")
            {
                $("#txtCardValue").attr('disabled', false);
                $("#txtNewGameName").attr('disabled', false);
                $("#txtBookSize").attr('disabled', false);
            }
        }
        
        function get_products_cardvalue2()
        {   
            var provid = document.getElementById('ddlProvider').options[document.getElementById('ddlProvider').selectedIndex].value;
            var prodid = document.getElementById('txtNewGameName').value;
            var cardval = document.getElementById('txtCardValue').value;
            var bookcount = document.getElementById('txtBookSize').value;
            
            $("#txtCardValue").load(
                    "../controller/get_gamecardvaluebyname.php",
                    {
                        gamename: prodid,
                        provid: provid,
                        cardval: cardval,
                        bookcount: bookcount,
                        flag: 'addgame'
                    },

                    function(r,status, xhr)
                    {
                        if (status == "success")
                        {
                            var json = $.parseJSON(r);
                            var cardvalue = json.CardPrice;
                            var booksize = json.BookTicketCount;
                            
                            if (cardvalue != '' || booksize != '')
                            {
                                $('#txtCardValue').val(cardvalue);
                                $('#txtBookSize').val(booksize);
                                
                                $('#hiddenCardValue').val($('#txtCardValue').val());
                                $('#hiddenBookSize').val($('#txtBookSize').val());
                                
                                $("#txtCardValue").attr('disabled', true);
                                $("#txtBookSize").attr('disabled', true);
                                
                                $('#hidFlag').val(0);
                            }
                            else
                            {
                                $('#hidFlag').val(1);
                            }
                            
                            $('#hiddenGameNumber').val($('#txtGameNumber').val());
                            
                            /*var n = cardvalue.search("x");
                            var o = booksize.search("x");
                            if (n > 0 || o > 0)
                            {
                                cardvalue = cardvalue.replace("x","");
                                booksize = booksize.replace("x", "");
                            }

                            var value = document.getElementById('txtCardValue').value;
                            if (n < 0)
                            {    
                                if (value > 0)
                                {
                                    $("#txtCardValue").attr('disabled', true);
                                    $("#txtBookSize").attr('disabled', true);
                                    $("#txtNewGameName").attr('disabled', true);
                                    document.getElementById("txtNewGameName").disabled = false;
                                }
                                else {
                                    document.getElementById("txtCardValue").disabled = false;
                                    document.getElementById("txtBookSize").disabled = false;
                                }
                            }*/
                        }
                        if (status == "error")
                        {
                            document.getElementById('title').innerHTML = "ERROR!";
                            document.getElementById('msg').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
                            document.getElementById('light').style.display = "block";
                            document.getElementById('fade').style.display = "block";
                        } 
                    }
                );
                    
                if(prodid == "")
                {
                    $('#hiddenCardValue').val($('#txtCardValue').val());
                    $('#hiddenBookSize').val($('#txtBookSize').val());
                    $('#hiddenGameNumber').val($('#txtGameNumber').val());
                    document.getElementById("txtCardValue").disabled = false;
                    document.getElementById("txtBookSize").disabled = false;
                    document.getElementById('txtCardValue').value = "";
                    document.getElementById('txtBookSize').value = "";
                }        
            
        }  
        
	function ClearTextbox()
	{
		var product = document.getElementById("ddlProduct").options[document.getElementById('ddlProduct').selectedIndex].value;
		if (product == "")
		{
			document.getElementById("txtNewGameName").disabled = false;
		}
		else
		{
			document.getElementById("txtNewGameName").value = "";
                        document.getElementById("txtCardValue").value = "";
                        document.getElementById("txtBookSize").value = "";
                        $("#txtCardValue").attr('disabled',true);
                        $("#txtBookSize").attr('disabled',true);
                        $("#txtNewGameName").attr('disabled',true);
		}
	}      


	$(document).ready(function(){
            
            $('#txtGameNumber').bind("cut copy paste",function(e) {
                e.preventDefault();
            });
            
            $('#txtBookSize').bind("cut copy paste",function(e) {
                e.preventDefault();
            });
        });

</script>
<div id="fade" class="black_overlay"></div>
<!-- POP UP FOR MESSAGES -->
<div id="light" class="white_content">
    <div id="title" class="light-title"></div>
    <div id="msg" class="light-message"></div>
    <div id="button" class="light-button"><input type="button" onclick="javascript: document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none';" value="Okay"/></div>
    <div class="light-footer"></div>
</div>
<!-- POP UP FOR MESSAGES -->
<!-- POP UP FOR MESSAGES -->
<div id="light3" class="white_content">
    <div id="title3" class="light-title"></div>
    <div id="msg3" class="light-message"></div>
    <div id="button3" class="light-button">
        <input type="button" onclick="javascript: return redirectToGameList();" value="Okay"/>
    </div>
    <div class="light-footer"></div>
</div>
<!-- POP UP FOR MESSAGES -->
<div class="titleCont">
            <div class="titleCont_left"></div>
            <div class="titleCont_body">Games</div>
            <div class="titleCont_right"></div>
    </div>
<form name="frmAddGame" method="post">
    <?php echo $hiddenCardValue; echo $hiddenBookSize; echo $hiddenGameNumber; echo $hidFlag; ?>
    <div class="content-page">
    <table class="form-add-provider">
        <tr>
            <th colspan="3">Game Profile: </th>
        </tr>
        <tr>
            <td>Provider: </td>
            <td><?php echo $ddlProvider; ?></td>
            <td></td>
        </tr>
        <tr>
            <td>Game Number: </td>
            <td><?php echo $txtGameNumber; ?></td>
            <td></td>
        </tr>
        <tr>
            <td>Game Name: </td>
            <td><?php echo $ddlProduct; ?></td>
            <td><?php echo $txtNewGameName; ?></td>
        </tr>
        <tr>
            <td>Card Value: </td>
            <td><?php echo $txtCardValue; ?></td>
        </tr>
        <tr>
            <td>Book Size: </td>
            <td><?php echo $txtBookSize; ?></td>
        </tr>        
        <tr>
            <td colspan="3">
                <div class="form-button">
                    <div><?php echo $btnSave?> &nbsp; <?php echo $btnCancel;?></div>
                </div>
            </td>
        </tr>
    </table>
    </div>
<!-- POP UP FOR CONFIRMATION MESSAGES -->
<div id="light2" class="white_content">
    <div id="title2" class="light-title">Confirmation</div>
    <div id="msg2" class="light-message">
        Create new game?
    </div>
    <div id="button2" class="light-button">
        <?php echo $btnConfirm;?>
        <input type="button" onclick="javascript: document.getElementById('light2').style.display='none';document.getElementById('fade').style.display='none';" value="Cancel"/>
    </div>
    <div class="light-footer"></div>
</div>
<!-- POP UP FOR CONFIRMATION MESSAGES -->
</form>
<?php if(isset($message)):?>
    <script>
    document.getElementById('title').innerHTML = "<?php echo $title;?>";
    document.getElementById('msg').innerHTML = "<?php echo $message;?>";
    document.getElementById('light').style.display = "block";
    document.getElementById('fade').style.display = "block";
    </script>
<?php endif;?>
<?php if(isset($product_success)):?>
    <script>
    document.getElementById('light2').style.display = "block";
    document.getElementById('fade').style.display = "block";
    </script>
<?php endif; ?>
<?php if(isset($successmsg)):?>
    <script>
    document.getElementById('light2').style.display = "none";
    document.getElementById('title3').innerHTML = "<?php echo $successtitle;?>";
    document.getElementById('msg3').innerHTML = "<?php echo $successmsg;?>";
    document.getElementById('light3').style.display = "block";
    document.getElementById('fade').style.display = "block";
    </script>
<?php endif;?>

<?php include("footer.php"); ?>