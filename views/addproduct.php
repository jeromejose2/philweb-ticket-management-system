<?php
/*
 * @author
 * Purpose   : view for addproduct
 */
include("../init.inc.php");
$pagesubmenuid = 23;
include_once("../controller/managesession.php");
include("../controller/addproductprocess.php");

?>
<?php include("header.php"); ?>
        <form name="frmProduct" method="post">
        <div id="fade" class="black_overlay"></div>
        <div id="loading" class="loading"></div>
	<!-- POP UP FOR MESSAGES -->
        <div id="light" class="white_content">
        <div id="title" class="light-title"></div>
        <div id="msg" class="light-message"></div>
        <div id="button" class="light-button"><input id="btnOk" name="btnOk" type="button" onclick="javascript: document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none';" value="OKAY"/></div>
	</div>        
        <!-- POP UP FOR MESSAGES -->
	<!-- POP UP FOR MESSAGES -->
        <div id="light2" class="white_content">
        <div id="title2" class="light-title"></div>
        <div id="msg2" class="light-message"></div>
        <div id="button2" class="light-button"><input id="btnOk" name="btnOk" type="button" onclick="javascript: window.location = 'productlist.php'" value="OKAY"/></div>
	</div>        
        <!-- POP UP FOR MESSAGES -->
            <br />
            <div>
                <!--<div><?php echo $txtCode; ?></div>-->
		<div style="text-decoration: underline; font-weight: bold;">Product Information</div><br/>
		<div><?php echo $ddlProviders; ?></div>
                <div><?php echo $txtName; ?></div>
                <!-- <div><?php echo $txtDescription; ?></div> -->
                <div><?php echo $btnSave; ?>&nbsp;<?php echo $btnCancel; ?></div>
                    <?php if (isset($errormsg)): ?>
                    <script>
                        document.getElementById('title').innerHTML = "";
                        document.getElementById('msg').innerHTML = "<?php echo $errormsg;?>";
                        document.getElementById('light').style.display = 'block';
                        document.getElementById('fade').style.display = 'block';
                    </script>   

                    <div  class="labelboldred"><?php //echo $errormsg; ?></div><br />
                    <?php endif; ?>
		    <?php if (isset($successmsg)): ?>
                    <script>
                        document.getElementById('title2').innerHTML = "";
                        document.getElementById('msg2').innerHTML = "<?php echo $successmsg;?>";
                        document.getElementById('light2').style.display = 'block';
                        document.getElementById('fade').style.display = 'block';
                    </script>   

                    <div  class="labelboldred"><?php //echo $errormsg; ?></div><br />
                    <?php endif; ?>
            </div>   
            </form>
<?php include("footer.php"); ?>