<?php
/*
 * @author
 * Purpose   : view for addprovider
 */
include("init.inc.php");
$pagesubmenuid = 22;
include_once("../controller/managesession.php");
include("../controller/addproviderprocess.php");

?>
<?php include("header.php"); ?>
<script>
    $(document).ready(function(){
        $('#txtContactNumber').bind("cut copy paste",function(e) {
          e.preventDefault();
        });

		$('#txtName').live('keypress',function(e)
        {
            if(e.keyCode == 13)
            {
                $('#btnSave').click();
                return false;
            }
        });

        $('#txtContactNumber').live('keypress',function(e)
        {
            if(e.keyCode == 13)
            {
                $('#btnSave').click();
                return false;
            }
        });

        $('#txtAddress').live('keypress',function(e)
        {
            if(e.keyCode == 13)
            {
                $('#btnSave').click();
                return false;
            }
        });
    });
</script>
<form name="frmProvider" method="post">
	<div id="fade" class="black_overlay"></div>
	<div id="loading" class="loading"></div>
	<!-- POP UP FOR MESSAGES -->
	<div id="light" class="white_content">
		<div id="title" class="light-title"></div>
		<div id="msg" class="light-message"></div>
		<div id="button" class="light-button">
		<input id="btnOk" name="btnOk" type="button" onclick="javascript: document.getElementById('light').style.display = 'none';document.getElementById('fade').style.display = 'none';" value="Okay"/>
	</div>
		<div class="light-footer"></div>
	</div>        
	<!-- POP UP FOR MESSAGES -->
	<!-- POP UP FOR MESSAGES -->
	<div id="light2" class="white_content">
		<div id="title2" class="light-title"></div>
		<div id="msg2" class="light-message"></div>
		<div id="button2" class="light-button">
		<input id="btnOk" name="btnOk" type="button" onclick="javascript: window.location = 'providerlist.php'; " value="Okay"/>
	</div>
		<div class="light-footer"></div>
	</div>        
	<!-- POP UP FOR MESSAGES -->
	<!-- POP UP FOR CONFIRMATION MESSAGES -->
	<div id="light3" class="white_content">
		<div id="title3" class="light-title"></div>
		<div id="msg3" class="light-message">
		    Add new provider?
		</div>
		<div id="button3" class="light-button">
		    <?php echo $btnConfirm;?>
			<input id="btnOk3" name="btnOk" type="button" onclick="javascript: document.getElementById('light3').style.display = 'none';document.getElementById('fade').style.display = 'none';" value="Cancel"/>
		</div>
		<div class="light-footer"></div>
	</div>
	<!-- POP UP FOR CONFIRMATION MESSAGES -->
		<div class="titleCont">
		<div class="titleCont_left"></div>
		<div class="titleCont_body">Provider</div>
		<div class="titleCont_right"></div>
		</div>
	<table class="form-add-provider" style="margin-left: 1%;">
		        <tr>
		            <th colspan="2">Provider Profile: </th>
		        </tr>
		        <tr>
		            <td class="form-label">Provider: </td>
		            <td><?php echo $txtName; ?></td>
		        </tr>
		        <tr>
		            <td class="form-label">Contact Number: </td>
		            <td><?php echo $txtContactNumber; ?></td>
		        </tr>
		        <tr>
		            <td class="form-label">Head Office Address: </td>
		            <td><?php echo $txtAddress; ?></td>
		        </tr>
		        <tr>
		            <td colspan="2">
		                <div class="form-button">
		                    <?php echo $btnSave; ?>&nbsp;<?php echo $btnCancel; ?>
		                </div>
		            </td>
		        </tr>
		    </table>
		<?php if(isset($confirm)):?>
		<script>
		    document.getElementById('title3').innerHTML = "Confirmation";
		    document.getElementById('light3').style.display = 'block';
		    document.getElementById('fade').style.display = 'block';
		</script>
		<?php endif;?>
		<?php if (isset($errormsg)): ?>
		<script>
		    document.getElementById('title').innerHTML = "ERROR";
		    document.getElementById('msg').innerHTML = "<?php echo $errormsg;?>";
		    document.getElementById('light').style.display = 'block';
		    document.getElementById('fade').style.display = 'block';
		</script>   
		     
		<div  class="labelboldred"><?php //echo $errormsg; ?></div><br />
		<?php endif; ?> 
	<?php if (isset($successmsg)): ?>
		<script>
		    document.getElementById('title2').innerHTML = "SUCCESS";
		    document.getElementById('msg2').innerHTML = "<?php echo $successmsg;?>";
		    document.getElementById('light2').style.display = 'block';
		    document.getElementById('fade').style.display = 'block';
		</script>   
		     
		<div  class="labelboldred"><?php //echo $errormsg; ?></div><br />
		<?php endif; ?> 
</form>
<?php include("footer.php"); ?>