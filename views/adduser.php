<?php
/*
Added By : Arlene R. Salazar
Added On : Aug 18, 2011
Purpose: For adding operator
*/
require_once ("init.inc.php");
$pagesubmenuid = 1;
include_once("../controller/managesession.php");
include("../controller/adduserprocess.php");
?>

<script type="text/javascript" src="jscripts/validations.js"></script>
<?php include("header.php"); ?>
<script>
    $(document).ready(function(){
        $('#txtfname').bind("cut copy paste",function(e) {
          e.preventDefault();
        });
        $('#txtcontactnum').bind("cut copy paste",function(e) {
          e.preventDefault();
        });
        $('#txtemail').bind("cut copy paste",function(e) {
          e.preventDefault();
        });
    });
</script>
<div id="fade" class="black_overlay"></div>
<div class="titleCont">
            <div class="titleCont_left"></div>
            <div class="titleCont_body">Account List</div>
            <div class="titleCont_right"></div>
</div>
<form name="frmAddUser" method="post">
    <table class="form-add-account">
        <tr><th colspan="2">Account Information</th></tr>
        <tr>
            <td><?php echo $txtusername; ?></td>
            <td><?php echo $ddlaccttype; ?></td>
        </tr>
        <tr>
            <td><?php echo $txtpassword; ?></td>
            <td>Status : Active</td>
        </tr>
        <tr>
            <td><?php echo $txtcpassword; ?></td>
            <!-- <td>
                <?php echo $btnResetPword; ?> &nbsp; <?php echo $btnChangeStat;?>
            </td> -->
        </tr>
        <tr><th colspan="2">Personal Information</th></tr>
        <tr>
            <td colspan="2"><?php echo $txtfname;?></td>
        </tr>
        <tr>
            <td colspan="2"><?php echo $txtaddress;?></td>
        </tr>
        <tr><th colspan="2">Contact Information</th></tr>
        <tr>
            <td><?php echo $txtcontactnum;?></td>
            <td><?php echo $txtemail;?></td>
        </tr>
        <tr>
            <td colspan="2">
                <div class="form-button">
                    <div><?php echo $btnSubmit;?>&nbsp;<?php echo $btnCancelForm;?></div>
                </div>
            </td>
        </tr>
    </table>
<!-- POP UP FOR MESSAGES -->
<div id="light" class="white_content">
	<div id="title" class="light-title"></div>
	<div id="msg" class="light-message"></div>
        <div id="button" class="light-button">
	<input id="btnOk" type="button" value="Okay" onclick="document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none';" />
        </div>
        <div class="light-footer"></div>
</div>
<!-- POP UP FOR MESSAGES -->
<!-- POP UP FOR CONFIRMATION MESSAGES -->
<div id="light5" class="white_content">
    <div id="title5" class="light-title">CONFIRMATION</div>
    <div id="msg5" class="light-message">Proceed in creating the account?</div>
    <div id="button5" class="light-button">
        <?php echo $btnConfirm;?>
        <input type="button" id="btnOk" name="btnOk" value="Cancel" onclick="javascript: document.getElementById('light5').style.display = 'none';document.getElementById('fade').style.display = 'none';"/>
    </div>
    <div class="light-footer"></div>
</div>
<!-- POP UP FOR CONFIRMATION MESSAGES -->
<?php if(isset($adduser_msg)):?>
<script>
    document.getElementById('title').innerHTML = "<?php echo $adduser_title;?>";
    document.getElementById('msg').innerHTML = "<?php echo $adduser_msg;?>";
    document.getElementById('light').style.display = 'block';
    document.getElementById('fade').style.display = 'block';
</script>
<?php endif; ?>
<?php $adduser_title = null;$adduser_msg = null;?>
<?php if(isset($okadduser_msg)): ?>
<script>
    document.getElementById('light5').style.display = 'block';
    document.getElementById('fade').style.display = 'block';
</script>
<?php endif; ?>
<?php $okadduser_msg = null;?>
<?php if(isset($confadduser_title)): ?>
<script>
    document.getElementById('title').innerHTML = "<?php echo $confadduser_title;?>";
    document.getElementById('msg').innerHTML = "<?php echo $confadduser_msg;?>";
    document.getElementById('light').style.display = 'block';
    document.getElementById('fade').style.display = 'block';
    document.getElementById('button').innerHTML = "<?php echo $btnCancel;?>";
</script>
<?php endif;?>
<?php $confadduser_title = null;$confadduser_msg = null;?>
</form>
<?php include("footer.php"); ?>