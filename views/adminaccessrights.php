<?php
/* * ***************** 
 * Author: Roger Sanchez
 * Date Created: 2011-08-10
 * Company: Philweb
 * ***************** */
require_once("init.inc.php");
include_once("../controller/managesession.php");
$pagesubmenuid = 15;
$stylesheets[] = "../css/default.css";

// Load Controls
App::LoadControl("DataTable");
App::LoadControl("Button");
App::LoadControl("TextBox");
App::LoadControl("ComboBox");
App::LoadControl("Hidden");
$fproc = new FormsProcessor();

// Load Objects/Modules
App::LoadModuleClass("TicketManagementCM", "TMMenus");
App::LoadModuleClass("TicketManagementCM", "TMSubMenus");
App::LoadModuleClass("TicketManagementCM", "TMAccessRights");
App::LoadModuleClass("TicketManagementCM", "TMAccountTypes");

// Get Query Strings
// Initialize variables
$menuid = 0;
$showcreateform = true;
$showeditform = false;
$errormessage = "";
// Initialize Controls

$dbaccounttype = new TMAccountTypes();
$arraccounttypes = $dbaccounttype->SelectAll();

$cboxmenu = new ComboBox("cboMenu", "cboMenu", "Menu ");
$litem = null;
for ($i = 0; $i < count($arraccounttypes); $i++)
{
    $menu = $arraccounttypes[$i];
    $litem[] = new ListItem($menu["Name"], $menu["AccountTypeID"]);
}
$cboxmenu->Items = $litem;
$cboxmenu->ShowCaption = false;
$cboxmenu->Multiple = true;
$cboxmenu->Size = 10;
$cboxmenu->Style = "width: 250px; height:200px;";

$cboxallowedsubmenus = new ComboBox("cboAllowedSubmenus", "cboAllowedSubmenus", "Accessible Submenus ");
$litem = null;
$litem[] = new ListItem("Select Account Type", 1);
$cboxallowedsubmenus->Items = $litem;
$cboxallowedsubmenus->ShowCaption = false;
$cboxallowedsubmenus->Multiple = true;
$cboxallowedsubmenus->Size = 10;
$cboxallowedsubmenus->Style = "width: 250px; height:200px;";

$cboxdefaultsubmenus = new ComboBox("cboDefaultSubmenus", "cboDefaultSubmenus", "Default Submenus ");
$litem = null;
$litem[] = new ListItem("Select Account Type", 1);
$cboxdefaultsubmenus->Items = $litem;
$cboxdefaultsubmenus->ShowCaption = false;
$cboxdefaultsubmenus->Multiple = true;
$cboxdefaultsubmenus->Size = 10;
$cboxdefaultsubmenus->Style = "width: 250px; height:200px;";

$cboxavailablesubmenus = new ComboBox("cboAvailableSubmenus", "cboAvailableSubmenus", "Available Submenus ");
$litem = null;
$litem[] = new ListItem("Select Account Type", 1);
$cboxavailablesubmenus->Items = $litem;
$cboxavailablesubmenus->ShowCaption = false;
$cboxavailablesubmenus->Multiple = true;
$cboxavailablesubmenus->Size = 10;
$cboxavailablesubmenus->Style = "width: 250px; height:200px;";

$txtMenuName = new TextBox("txtMenuName", "txtMenuName", "Title ");
$txtMenuName->Length = 40;
$txtMenuName->Attributes = "width='10'";
$txtMenuName->ShowCaption = true;

$hdnMenu = new Hidden("hdnSubmenu", "hdnSubmenu", "");

$btnSubmit = new Button("btnSubmit", "btnSubmit", "Go");
$btnSubmit->IsSubmit = true;
$btnSubmit->CssClass = "btnDefault roundedcorners";

$btnSubmitRemove = new Button("btnSubmitRemove", "btnSubmitRemove", "Remove");
$btnSubmitRemove->IsSubmit = true;
$btnSubmitRemove->CssClass = "btnDefault roundedcorners";
$btnSubmitRemove->Enabled = false;

$btnSubmitAdd = new Button("btnSubmitAdd", "btnSubmitAdd", "Add");
$btnSubmitAdd->IsSubmit = true;
$btnSubmitAdd->CssClass = "btnDefault roundedcorners";
$btnSubmitAdd->Enabled = false;

$fproc->AddControl($cboxmenu);
$fproc->AddControl($cboxallowedsubmenus);
$fproc->AddControl($cboxavailablesubmenus);
$fproc->AddControl($btnSubmit);
$fproc->AddControl($btnSubmitRemove);
$fproc->AddControl($btnSubmitAdd);
$fproc->AddControl($hdnMenu);

$fproc->ProcessForms();

if ($fproc->IsPostBack)
{
    $dbsubmenu = new TMSubMenus();
    $dashboardmenu = new TMMenus();

    $dbaccessrights = new TMAccessRights();

    if ($btnSubmitAdd->SubmittedValue == "Add" && $cboxavailablesubmenus->SubmittedValue != "")
    {
        $submenuid = $cboxavailablesubmenus->SubmittedValue;
        $accounttypeid = $cboxmenu->SelectedValue;

        $submenu = new TMSubMenus();
        $arrsubmenu = $submenu->GetByID($submenuid);
        $arrsubmenu = $arrsubmenu[0];

        $arrEntries = "";
        if ($fproc->GetPostVar("chkDefault"))
        {
            $dbaccessrights->ClearDefaultSubmenu($accounttypeid);
            $arrEntries["`Default`"] = 1;
        }
        
        $arrEntries["MenuLandingSubMenuID"] = $fproc->GetPostVar("chkLanding");
        
        $arrEntries["SubMenuID"] = $submenuid;
        $arrEntries["AccountTypeID"] = $accounttypeid;
        $arrEntries["MenuID"] = $arrsubmenu["MenuID"];
        
        $dbaccessrights->Insert($arrEntries);
        
        if ($dbaccessrights->HasError)
        {
            App::Pr($arrEntries);
            App::Pr($dbaccessrights->getError());
        }
    }

    if ($btnSubmitRemove->SubmittedValue == "Remove" && $cboxallowedsubmenus->SubmittedValue != "")
    {
        $accessrightsid = $cboxallowedsubmenus->SubmittedValue;
        $dbaccessrights->RemoveAccessibleSubmenu($accessrightsid);
    }

    if ($cboxmenu->SelectedValue != "")
    {
        $accounttypeid = $cboxmenu->SelectedValue;
        $arrdbaccessrights = $dbaccessrights->GetAllowedSubmenusByAccountTypeID($accounttypeid);
        $litem = null;
        for ($i = 0; $i < count($arrdbaccessrights); $i++)
        {
            $menu = $arrdbaccessrights[$i];
            if ($menu["Default"] == 1)
            {
                $menu["Name"] = "[D] " . $menu["Name"];
            }
            
            if ($menu["MenuLandingSubMenuID"] == 1)
            {
                $menu["Name"] = "[L] " . $menu["Name"];
            }
            $litem[] = new ListItem($menu["Name"], $menu["AccessRightsID"]);
        }
        $cboxallowedsubmenus->Items = $litem;

        $arravailablesubmenus = $dbaccessrights->GetAvailableSubmenusByAccountTypeID($accounttypeid);
        $litem = null;
        for ($i = 0; $i < count($arravailablesubmenus); $i++)
        {
            $menu = $arravailablesubmenus[$i];
            $litem[] = new ListItem($menu["Name"], $menu["SubMenuID"]);
        }
        $cboxavailablesubmenus->Items = $litem;

        $btnSubmitRemove->Enabled = true;
        $btnSubmitAdd->Enabled = true;
    }

    if ($btnSubmit->SubmittedValue == "Submit")
    {
        
    }
}
?>

<?php include("header.php"); ?>
<div class="titleCont">
            <div class="titleCont_left"></div>
            <div class="titleCont_body">Access Rights Management</div>
            <div class="titleCont_right"></div>
</div>
<form action="" method="post" name="MainForm" id="MainForm">
    <div align="center">
        <div class="contentcontainer">
            <div class="content">

                <table cellpadding="5" cellspacing="0">
                    <tr valign="top">
                        <td class="third" rowspan="2">
                            <div class="thirdpadded formcontainer formstyle2 roundedcorners">
                                <div class="title">Account Types</div><br/>
                                <center>
                                    <?php echo $cboxmenu; ?><br/>
                                    <?php echo $hdnMenu; ?>
                                    <?php echo $btnSubmit; ?>
                                </center>
                            </div>
                        </td>
                        <td class="third">
                            <div class="thirdpadded formcontainer formstyle2 roundedcorners">
                                <div class="title">Accessible Submenus</div><br/>
                                <center>
                                    <?php echo $cboxallowedsubmenus; ?><br/>
                                    <?php echo $btnSubmitRemove; ?></center>
                            </div>
                        </td>
                        <td class="third">
                            <div class="thirdpadded formcontainer formstyle2 roundedcorners">
                                <div class="title">Available Submenus</div><br/>
                                <center><?php echo $cboxavailablesubmenus; ?><br/><br/>
                                    <input type="checkbox" name="chkDefault" id="chkDefault" value="1"><label for="chkDefault" style="width:170px; text-align: left;">Set as login default page</label><br/><br/>
                                    <input type="checkbox" name="chkLanding" id="chkLanding" value="1"><label for="chkLanding" style="width:170px; text-align: left;">Set as menu landing page</label><br/><br/>
                                    <?php echo $btnSubmitAdd; ?></center>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</form>
<?php include ("footer.php");?>