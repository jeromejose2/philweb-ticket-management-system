<?php
/* * ***************** 
 * Author: Roger Sanchez
 * Date Created: 2011-08-23
 * Company: Philweb
 * ***************** */
require_once("include/core/init.inc.php");
include_once("../controller/managesession.php");
$pagesubmenuid = 16;
$stylesheets[] = "../css/default.css";

// Load Controls
App::LoadControl("DataTable");
App::LoadControl("Button");
App::LoadControl("TextBox");
App::LoadControl("ComboBox");
App::LoadControl("Hidden");
$fproc = new FormsProcessor();

// Load Objects/Modules
App::LoadModuleClass("TicketManagementCM", "TMAccountTypes");

// Get Query Strings
// Initialize variables
$accounttypeid = 0;
$showcreateform = true;
$showeditform = false;
$errormessage = "";
// Initialize Controls

$dbaccounttype = new TMAccountTypes();
$arrdbaccounttypes = $dbaccounttype->SelectAll();

$cboxaccounttype = new ComboBox("cboAccountType", "cboAccountType", "AccountType ");
$litem = null;
for ($i = 0; $i < count($arrdbaccounttypes); $i++)
{
    $accounttype = $arrdbaccounttypes[$i];
    $litem[] = new ListItem($accounttype["Name"], $accounttype["AccountTypeID"]);
}
$cboxaccounttype->Items = $litem;
$cboxaccounttype->ShowCaption = false;
$cboxaccounttype->Multiple = true;
$cboxaccounttype->Size = 10;
$cboxaccounttype->Style = "width: 250px; height:200px;";


$txtAccountTypeName = new TextBox("txtAccountTypeName", "txtAccountTypeName", "Name ");
$txtAccountTypeName->Length = 40;
$txtAccountTypeName->Attributes = "width='10'";
$txtAccountTypeName->ShowCaption = true;

$txtDescription = new TextBox("txtDescription", "txtDescription", "Info ");
$txtDescription->Length = 100;
$txtDescription->Style = " width:200px;";
$txtDescription->ShowCaption = true;

$cboxStatus = new ComboBox("cboStatus", "cboStatus", "Status ");
$litem = null;
$litem[] = new ListItem("Active", 1);
$litem[] = new ListItem("Deleted", 2);
$cboxStatus->Items = $litem;
$cboxStatus->ShowCaption = true;

$hdnAccountType = new Hidden("hdnSubaccounttype", "hdnSubaccounttype", "");

$btnSubmit = new Button("btnSubmit", "btnSubmit", "Go");
$btnSubmit->IsSubmit = true;
$btnSubmit->CssClass = "btnDefault roundedcorners";

$btnSubmitCreate = new Button("btnSubmitCreate", "btnSubmitCreate", "Submit");
$btnSubmitCreate->IsSubmit = true;
$btnSubmitCreate->CssClass = "btnDefault roundedcorners";

$btnSubmitUpdate = new Button("btnSubmitUpdate", "btnSubmitUpdate", "Update");
$btnSubmitUpdate->IsSubmit = true;
$btnSubmitUpdate->CssClass = "btnDefault roundedcorners";

$btnSubmitEdit = new Button("btnSubmitEdit", "btnSubmitEdit", "Edit");
$btnSubmitEdit->IsSubmit = true;
$btnSubmitEdit->CssClass = "btnDefault roundedcorners";

$btnSubmitCancel = new Button("btnSubmitCancel", "btnSubmitCancel", "Cancel");
$btnSubmitCancel->IsSubmit = true;
$btnSubmitCancel->CssClass = "btnDefault roundedcorners";

$fproc->AddControl($cboxaccounttype);
$fproc->AddControl($cboxStatus);
$fproc->AddControl($txtAccountTypeName);
$fproc->AddControl($txtDescription);
$fproc->AddControl($btnSubmitEdit);
$fproc->AddControl($btnSubmit);
$fproc->AddControl($btnSubmitCreate);
$fproc->AddControl($btnSubmitUpdate);
$fproc->AddControl($hdnAccountType);

$fproc->ProcessForms();

if ($fproc->IsPostBack)
{

    if ($cboxaccounttype->SelectedValue != "")
    {
        $accounttypeid = $cboxaccounttype->SelectedValue;
    }

    $selectedaccounttype = $hdnAccountType->Text;

    if ($btnSubmitEdit->SubmittedValue == "Edit")
    {
        $accounttypeedit = $dbaccounttype->SelectByID($accounttypeid);
        if (count($accounttypeedit) == 1)
        {
            $accounttypeedit = $accounttypeedit[0];
            $txtAccountTypeName->Text = $accounttypeedit["Name"];
            $txtDescription->Text = $accounttypeedit["Description"];
            $cboxStatus->SelectedValue = $accounttypeedit["Status"];
            $hdnAccountType->Text = $accounttypeedit["AccountTypeID"];
        }

        $showcreateform = false;
        $showeditform = true;
    }

    if ($btnSubmitCreate->SubmittedValue)
    {
        $subaccounttypeentry["Name"] = $txtAccountTypeName->Text;
        $subaccounttypeentry["Description"] = $txtDescription->Text;
        $subaccounttypeentry["Status"] = 1;
        $dbaccounttype->Insert($subaccounttypeentry);

        if ($dbaccounttype->HasError)
        {
            $errormessage = $dbaccounttype->getError();
        }
        else
        {
            $errormessage = "Subaccounttype successfully created.";
        }

        $txtAccountTypeName->Text = "";
        $txtDescription->Text = "";
        $arrdbaccounttypes = $dbaccounttype->SelectAll();

        $litem = null;
        for ($i = 0; $i < count($arrdbaccounttypes); $i++)
        {
            $accounttype = $arrdbaccounttypes[$i];
            $litem[] = new ListItem($accounttype["Name"], $accounttype["AccountTypeID"]);
        }
        $cboxaccounttype->Items = $litem;
    }

    if ($btnSubmitUpdate->SubmittedValue == "Update")
    {
        $selectedaccounttype = $hdnAccountType->Text;
        $subaccounttypeentry["Name"] = $txtAccountTypeName->Text;
        $subaccounttypeentry["Description"] = $txtDescription->Text;
        $subaccounttypeentry["Status"] = $cboxStatus->SelectedValue;
        $subaccounttypeentry["AccountTypeID"] = $selectedaccounttype;
        $dbaccounttype->Update($subaccounttypeentry);

        if ($dbaccounttype->HasError)
        {
            $errormessage = $dbaccounttype->getError();
        }
        else
        {
            $errormessage = "Account Group Successfully Updated.";
        }

        $txtAccountTypeName->Text = "";
        $txtDescription->Text = "";
        $showcreateform = true;
        $showeditform = false;
        $arrdbaccounttypes = $dbaccounttype->SelectAll();

        $litem = null;
        for ($i = 0; $i < count($arrdbaccounttypes); $i++)
        {
            $accounttype = $arrdbaccounttypes[$i];
            $litem[] = new ListItem($accounttype["Name"], $accounttype["AccountTypeID"]);
        }
        $cboxaccounttype->Items = $litem;
    }

    if ($fproc->GetPostVar("btnSubmit") && $fproc->GetPostVar("btnSubmit") == "Go")
    {
        $txtAccountTypeName->Text = "";
        $txtDescription->Text = "";
        $showcreateform = true;
        $showeditform = false;
    }
}
?>
<?php include("header.php"); ?>
<div class="titleCont">
            <div class="titleCont_left"></div>
            <div class="titleCont_body">Group Management</div>
            <div class="titleCont_right"></div>
</div>
<form action="" method="post" name="MainForm" id="MainForm">
    <div align="center">
        <div class="contentcontainer">
            <div class="content">
                <table cellpadding="5" cellspacing="0">
                    <tr valign="top">
                        <td class="third">
                            <div class="thirdpadded formcontainer formstyle2 roundedcorners">
                                <div class="title">Account Groups</div><br/>
                                <?php echo $cboxaccounttype; ?><br/><br/>
                                <?php echo $hdnAccountType; ?>
                                <?php echo $btnSubmitEdit; ?>
                            </div>
                        </td>
                        <td class="twothird">
                            <?php
                            if ($errormessage != "")
                            {
                                ?>
                                <div class="twothirdpadded formcontainer formstyle2 roundedcorners title"><?php echo $errormessage; ?></div><br/><br/>
                                <?php
                            }
                            ?>

                            <?php if ($showcreateform)
                            { ?>
                                <div class="twothirdpadded formcontainer formstyle2 roundedcorners">
                                    <div class="title">Create Account Group</div><br/>
                                    <?php echo $txtAccountTypeName; ?><br/>
                                    <?php echo $txtDescription; ?><br/><br/>
                                    <center><?php echo $btnSubmitCreate; ?></center>
                                </div><br/><br/>
                            <?php } ?>
                            <?php if ($showeditform)
                            { ?>
                                <div class="twothirdpadded formcontainer formstyle2 roundedcorners">
                                    <div class="title">Edit Account Group</div><br/>
                                    <?php echo $txtAccountTypeName; ?><br/>
                                    <?php echo $txtDescription; ?><br/>
                                    <?php echo $cboxStatus; ?><br/><br/>
                                    <table cellpadding="0" cellspacing="0" class="twothirdpadded" align="center">
                                        <tr>
                                            <td align="right"><?php echo $btnSubmitUpdate; ?></td>
                                            <td align="left"><?php echo $btnSubmitCancel; ?></td>
                                        </tr>
                                    </table>

                                </div><br/><br/>
                            <?php } ?>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</form>
<?php include("footer.php"); ?>