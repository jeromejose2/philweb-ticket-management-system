<?php
/* * ***************** 
 * Author: Roger Sanchez
 * Date Created: 2011-08-10
 * Company: Philweb
 * ***************** */
require_once("init.inc.php");
include_once("../controller/managesession.php");
$pagesubmenuid = 13;
$javascripts[] = "../jscripts/validations.js";
$stylesheets[] = "../css/default.css";
$pagetitle = "TMS Admin Menus";

// Load Controls
App::LoadControl("DataTable");
App::LoadControl("Button");
App::LoadControl("TextBox");
App::LoadControl("ComboBox");
App::LoadControl("Hidden");
$fproc = new FormsProcessor();

// Load Objects/Modules
App::LoadModuleClass("TicketManagementCM", "TMMenus");
App::LoadModuleClass("TicketManagementCM", "TMSubMenus");

// Get Query Strings
// Initialize variables
$menuid = 0;
$showcreateform = true;
$showeditform = false;
$errormessage = "";
// Initialize Controls

$dbmenu = new TMMenus();
$arrdbmenus = $dbmenu->SelectAllMenus();

$cboxmenu = new ComboBox("cboMenu", "cboMenu", "Menu ");
$litem = null;
for ($i = 0; $i < count($arrdbmenus); $i++)
{
    $menu = $arrdbmenus[$i];
    $litem[] = new ListItem($menu["Name"], $menu["MenuID"]);
}
$cboxmenu->Items = $litem;
$cboxmenu->ShowCaption = false;
$cboxmenu->Multiple = true;
$cboxmenu->Size = 10;
$cboxmenu->Style = "width: 250px; height:200px;";


$txtMenuName = new TextBox("txtMenuName", "txtMenuName", "Title ");
$txtMenuName->Length = 40;
$txtMenuName->Attributes = "width='10'";
$txtMenuName->ShowCaption = true;

$txtLink = new TextBox("txtLink", "txtLink", "Link ");
$txtLink->Length = 100;
$txtLink->Style = " width:200px;";
$txtLink->ShowCaption = true;

$cboxStatus = new ComboBox("cboStatus", "cboStatus", "Status ");
$litem = null;
$litem[] = new ListItem("Active", 1);
$litem[] = new ListItem("Deleted", 2);
$cboxStatus->Items = $litem;
$cboxStatus->ShowCaption = true;

$hdnMenu = new Hidden("hdnSubmenu", "hdnSubmenu", "");

$btnSubmit = new Button("btnSubmit", "btnSubmit", "Go");
$btnSubmit->IsSubmit = true;
$btnSubmit->CssClass = "btnDefault roundedcorners";

$btnSubmitCreate = new Button("btnSubmitCreate", "btnSubmitCreate", "Submit");
$btnSubmitCreate->IsSubmit = true;
$btnSubmitCreate->CssClass = "btnDefault roundedcorners";

$btnSubmitUpdate = new Button("btnSubmitUpdate", "btnSubmitUpdate", "Update");
$btnSubmitUpdate->IsSubmit = true;
$btnSubmitUpdate->CssClass = "btnDefault roundedcorners";

$btnSubmitEdit = new Button("btnSubmitEdit", "btnSubmitEdit", "Edit");
$btnSubmitEdit->IsSubmit = true;
$btnSubmitEdit->CssClass = "btnDefault roundedcorners";

$btnSubmitCancel = new Button("btnSubmitCancel", "btnSubmitCancel", "Cancel");
$btnSubmitCancel->IsSubmit = true;
$btnSubmitCancel->CssClass = "btnDefault roundedcorners";

$fproc->AddControl($cboxmenu);
$fproc->AddControl($cboxStatus);
$fproc->AddControl($txtMenuName);
$fproc->AddControl($txtLink);
$fproc->AddControl($btnSubmitEdit);
$fproc->AddControl($btnSubmit);
$fproc->AddControl($btnSubmitCreate);
$fproc->AddControl($btnSubmitUpdate);
$fproc->AddControl($hdnMenu);

$fproc->ProcessForms();

if ($fproc->IsPostBack)
{

    if ($cboxmenu->SelectedValue != "")
    {
        $menuid = $cboxmenu->SelectedValue;
    }

    $selectedmenu = $hdnMenu->Text;

    if ($btnSubmitEdit->SubmittedValue == "Edit")
    {
        $menuedit = $dbmenu->GetByID($menuid);
        if (count($menuedit) == 1)
        {
            $menuedit = $menuedit[0];
            $txtMenuName->Text = $menuedit["Name"];
            $txtLink->Text = $menuedit["Link"];
            $cboxStatus->SelectedValue = $menuedit["Status"];
            $hdnMenu->Text = $menuedit["MenuID"];
        }

        $showcreateform = false;
        $showeditform = true;
    }

    if ($btnSubmitCreate->SubmittedValue)
    {
        $submenuentry["Name"] = $txtMenuName->Text;
        $submenuentry["Link"] = $txtLink->Text;
        $submenuentry["Status"] = 1;
        $dbmenu->Insert($submenuentry);

        if ($dbmenu->HasError)
        {
            $errormessage = $dbmenu->getError();
        }
        else
        {
            $errormessage = "Submenu successfully created.";
        }

        $txtMenuName->Text = "";
        $txtLink->Text = "";
        $arrdbmenus = $dbmenu->SelectAllMenus();

        $litem = null;
        for ($i = 0; $i < count($arrdbmenus); $i++)
        {
            $menu = $arrdbmenus[$i];
            $litem[] = new ListItem($menu["Name"], $menu["MenuID"]);
        }
        $cboxmenu->Items = $litem;
    }

    if ($btnSubmitUpdate->SubmittedValue == "Update")
    {
        $selectedmenu = $hdnMenu->Text;
        $submenuentry["Name"] = $txtMenuName->Text;
        $submenuentry["Link"] = $txtLink->Text;
        $submenuentry["Status"] = $cboxStatus->SelectedValue;
        $submenuentry["MenuID"] = $selectedmenu;
        $dbmenu->Update($submenuentry);

        if ($dbmenu->HasError)
        {
            $errormessage = $dbmenu->getError();
        }
        else
        {
            $errormessage = "Submenu successfully updated.";
        }

        $txtMenuName->Text = "";
        $txtLink->Text = "";
        $showcreateform = true;
        $showeditform = false;
        $arrdbmenus = $dbmenu->SelectAllMenus();

        $litem = null;
        for ($i = 0; $i < count($arrdbmenus); $i++)
        {
            $menu = $arrdbmenus[$i];
            $litem[] = new ListItem($menu["Name"], $menu["MenuID"]);
        }
        $cboxmenu->Items = $litem;
    }

    if ($fproc->GetPostVar("btnSubmit") && $fproc->GetPostVar("btnSubmit") == "Go")
    {
        $txtMenuName->Text = "";
        $txtLink->Text = "";
        $showcreateform = true;
        $showeditform = false;
    }
}
?>
<?php include("header.php"); ?>
<div class="titleCont">
            <div class="titleCont_left"></div>
            <div class="titleCont_body">Menu Management</div>
            <div class="titleCont_right"></div>
</div>
<form action="" method="post" name="MainForm" id="MainForm">
    <div align="center">
        <div class="contentcontainer">
            <div class="content">
                <table cellpadding="5" cellspacing="0">
                    <tr valign="top">
                        <td class="third">
                            <div class="thirdpadded formcontainer formstyle2 roundedcorners">
                                <div class="title">Main Menus</div><br/>
                                <?php echo $cboxmenu; ?><br/><br/>
                                <?php echo $hdnMenu; ?>
                                <?php echo $btnSubmitEdit; ?>
                            </div>
                        </td>
                        <td class="twothird">
                            <?php
                            if ($errormessage != "")
                            {
                                ?>
                                <div class="twothirdpadded formcontainer formstyle2 roundedcorners title"><?php echo $errormessage; ?></div><br/><br/>
                                <?php
                            }
                            ?>

                            <!-- div class="twothirdpadded formcontainer formstyle2 roundedcorners title">Menu Creation</div><br/><br/ -->

                            <?php if ($showcreateform)
                            { ?>
                                <div class="twothirdpadded formcontainer formstyle2 roundedcorners">
                                    <div class="title">Create Menu</div><br/>
                                    <?php echo $txtMenuName; ?><br/>
                                    <?php echo $txtLink; ?><br/><br/>
                                    <center><?php echo $btnSubmitCreate; ?></center>
                                </div><br/><br/>
                            <?php } ?>
                            <?php if ($showeditform)
                            { ?>
                                <div class="twothirdpadded formcontainer formstyle2 roundedcorners">
                                    <div class="title">Update Menu</div><br/>
                                    <?php echo $txtMenuName; ?><br/>
                                    <?php echo $txtLink; ?><br/>
                                    <?php echo $cboxStatus; ?><br/><br/>
                                    <table cellpadding="0" cellspacing="0" class="twothirdpadded" align="center">
                                        <tr>
                                            <td align="right"><?php echo $btnSubmitUpdate; ?></td>
                                            <td align="left"><?php echo $btnSubmitCancel; ?></td>
                                        </tr>
                                    </table>

                                </div><br/><br/>
                            <?php } ?>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</form>
<?php include_once 'footer.php'; ?>