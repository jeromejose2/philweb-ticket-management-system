<?php
/* * ***************** 
 * Author: Roger Sanchez
 * Date Created: 2011-08-10
 * Company: Philweb
 * ***************** */
require_once("init.inc.php");
include_once("../controller/managesession.php");
$pagesubmenuid = 14;
$stylesheets[] = "../css/default.css";

// Load Controls
App::LoadControl("DataTable");
App::LoadControl("Button");
App::LoadControl("TextBox");
App::LoadControl("ComboBox");
App::LoadControl("Hidden");
$fproc = new FormsProcessor();

// Load Objects/Modules
App::LoadModuleClass("TicketManagementCM", "TMMenus");
App::LoadModuleClass("TicketManagementCM", "TMSubMenus");

// Get Query Strings
// Initialize variables
$menuid = 0;
$showcreateform = false;
$showeditform = false;
$errormessage = "";
// Initialize Controls

$dbmenu = new TMMenus();
$arrdm = $dbmenu->SelectAllMenus();
$cboxmenu = new ComboBox("cboMenu", "cboMenu", "Menu ");
$litem = null;
for ($i = 0; $i < count($arrdm); $i++)
{
    $menu = $arrdm[$i];
    $litem[] = new ListItem($menu["Name"], $menu["MenuID"]);
}
$cboxmenu->Items = $litem;
$cboxmenu->ShowCaption = false;
$cboxmenu->Style = "width: 250px;";


$txtMenuName = new TextBox("txtMenuName", "txtMenuName", "Title ");
$txtMenuName->Length = 40;
$txtMenuName->Attributes = "width='10'";
$txtMenuName->ShowCaption = true;

$txtLink = new TextBox("txtLink", "txtLink", "Link ");
$txtLink->Length = 100;
$txtLink->Attributes = "width='10'";
$txtLink->ShowCaption = true;

$cboxStatus = new ComboBox("cboStatus", "cboStatus", "Status ");
$litem = null;
$litem[] = new ListItem("Active", 1);
$litem[] = new ListItem("Deleted", 2);
$cboxStatus->Items = $litem;
$cboxStatus->ShowCaption = true;

$hdnSubmenu = new Hidden("hdnSubmenu", "hdnSubmenu", "");

$btnSubmit = new Button("btnSubmit", "btnSubmit", "Go");
$btnSubmit->IsSubmit = true;
$btnSubmit->CssClass = "btnDefault roundedcorners";

$btnSubmitCreate = new Button("btnSubmitCreate", "btnSubmitCreate", "Submit");
$btnSubmitCreate->IsSubmit = true;
$btnSubmitCreate->CssClass = "btnDefault roundedcorners";

$btnSubmitUpdate = new Button("btnSubmitUpdate", "btnSubmitUpdate", "Update");
$btnSubmitUpdate->IsSubmit = true;
$btnSubmitUpdate->CssClass = "btnDefault roundedcorners";

$btnSubmitCancel = new Button("btnSubmitCancel", "btnSubmitCancel", "Cancel");
$btnSubmitCancel->IsSubmit = true;
$btnSubmitCancel->CssClass = "btnDefault roundedcorners";

$fproc->AddControl($cboxmenu);
$fproc->AddControl($cboxStatus);
$fproc->AddControl($txtMenuName);
$fproc->AddControl($txtLink);
$fproc->AddControl($btnSubmit);
$fproc->AddControl($btnSubmitCreate);
$fproc->AddControl($btnSubmitUpdate);
$fproc->AddControl($hdnSubmenu);

$fproc->ProcessForms();

if ($fproc->IsPostBack)
{
    $dbsubmenu = new TMSubMenus();
    
    if ($cboxmenu->SelectedValue != "")
    {
        $menuid = $cboxmenu->SelectedValue;
    }
    
    if ($hdnSubmenu->Text != '' && ($fproc->GetPostVar("btnSubmitUpdate") != "Update"))
    {
        
        $selectedsubmenu = $hdnSubmenu->Text;
        $submenuedit = $dbsubmenu->GetByID($selectedsubmenu);
        if (count($submenuedit) == 1)
        {
            $submenuedit = $submenuedit[0];
            $txtMenuName->Text = $submenuedit["Name"];
            $txtLink->Text = $submenuedit["Link"];
            $cboxStatus->SelectedValue = $submenuedit["Status"];
        }
        
        $showcreateform = false;
        $showeditform = true;
    }

    
    if ($fproc->GetPostVar("btnSubmitCreate") && $fproc->GetPostVar("btnSubmitCreate") == "Submit")
    {
        $submenuentry["Name"] = $txtMenuName->Text;
        $submenuentry["Link"] = $txtLink->Text;
        $submenuentry["Status"] = 1;
        $submenuentry["MenuID"] = $menuid;
        $dbsubmenu->Insert($submenuentry);

        if ($dbsubmenu->HasError)
        {
            $errormessage = $dbsubmenu->getError();
        }
        else
        {
            $errormessage = "Submenu successfully created.";
        }
        
        $txtMenuName->Text = "";
        $txtLink->Text = "";
    }
    
    if ($fproc->GetPostVar("btnSubmitUpdate") && $fproc->GetPostVar("btnSubmitUpdate") == "Update")
    {
        $selectedsubmenu = $hdnSubmenu->Text;
        $submenuentry["Name"] = $txtMenuName->Text;
        $submenuentry["Link"] = $txtLink->Text;
        $submenuentry["Status"] = $cboxStatus->SelectedValue;
        $submenuentry["MenuID"] = $menuid;
        $submenuentry["SubMenuID"] = $selectedsubmenu;
        $dbsubmenu->Update($submenuentry);

        if ($dbsubmenu->HasError)
        {
            $errormessage = $dbsubmenu->getError();
        }
        else
        {
            $errormessage = "Submenu successfully updated.";
        }
        
        $txtMenuName->Text = "";
        $txtLink->Text = "";
        $showcreateform = false;
        $showeditform = false;
    }

    if ($fproc->GetPostVar("btnSubmit") && $fproc->GetPostVar("btnSubmit") == "Go")
    {
        $txtMenuName->Text = "";
        $txtLink->Text = "";
        $showcreateform = true;
        $showeditform = false;
    }
    
    $arrdsubmenus = $dbsubmenu->GetByMenuID($menuid);

    $chs = null;
    $dth = null;
    $dic = null;
    $dtf = null;
    $chs[] = new ColumnHeader('SubMenuID', 1, 'whatdate');
    $chs[] = new ColumnHeader('Name', 1, 'whatdate');
    $chs[] = new ColumnHeader('Link', 1, 'whatdate');
    $dth[] = new DataTableHeader($chs, 'bg_tableheader2');
    $dic[] = new DataItemColumn('', '', "SubMenuID", '', '', '', false, false);
    $dic[] = new DataItemColumn('', '', "Name", '', '', '', false, true);
    $dic[] = new DataItemColumn('', '', "Link", '', '', '', false, false);

    $dtf[] = new DataTableFooter($dic, '', 'font-weight:bold;');

    $dtable = new DataTable();
    $dtable->DataTableHeaders = $dth;
    $dtable->DataItemColumns = $dic;
    $dtable->AlternatingClass = "bg_alternate";
    $dtable->Class = "reporttable twothirdpadded";
    $dtable->DataItems = $arrdsubmenus;
    $dtable->SelectIdentity = "SubMenuID";
    

    $subjtable = $dtable->Render();
}
?>
<?php include("header.php"); ?>
<div class="titleCont">
            <div class="titleCont_left"></div>
            <div class="titleCont_body">Submenus Management</div>
            <div class="titleCont_right"></div>
</div>
<form action="" method="post" name="MainForm" id="MainForm">
    <div align="center">
        <div class="contentcontainer">
            <div class="content">
                <table cellpadding="5" cellspacing="0">
                    <tr valign="top">
                        <td class="third">
                            <div class="thirdpadded formcontainer formstyle2 roundedcorners">
                                <div class="title">Select Menu</div><br/>
                                <?php echo $cboxmenu; ?><br/><br/>
                                <?php echo $hdnSubmenu; ?>
                                <center><?php echo $btnSubmit; ?></center>

                            </div><br/><br/>

                            <?php if ($showcreateform) { ?>
                            <div class="thirdpadded formcontainer formstyle2 roundedcorners">
                                <div class="title">Create Submenu</div><br/>
                                <?php echo $txtMenuName; ?><br/>
                                <?php echo $txtLink; ?><br/><br/>
                                <center><?php echo $btnSubmitCreate; ?></center>
                            </div><br/><br/>
                            <?php } ?>
                            <?php if ($showeditform) { ?>
                            <div class="thirdpadded formcontainer formstyle2 roundedcorners">
                                <div class="title">Update Submenu</div><br/>
                                <?php echo $txtMenuName; ?><br/>
                                <?php echo $txtLink; ?><br/>
                                <?php echo $cboxStatus; ?><br/><br/>
                                <table cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td><?php echo $btnSubmitUpdate; ?></td>
                                        <td><?php echo $btnSubmitCancel; ?></td>
                                    </tr>
                                </table>

                            </div><br/><br/>
                            <?php } ?>

                        </td>
                        <td class="twothird">
                            <?php
                            if ($errormessage != "")
                            {
                                ?>
                                <div class="twothirdpadded formcontainer formstyle2 roundedcorners title"><?php echo $errormessage; ?></div><br/><br/>
                                <?php
                            }
                            ?>

                            <div class="twothirdpadded formcontainer formstyle2 roundedcorners title">Submenu List</div><br/><br/>
                            <?php
                            if ($menuid == 0)
                            {
                                ?>
                                No selected menu. Please select a menu on the left side.
                                <?php
                            }
                            ?>
                            <?php echo $subjtable; ?>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</form>
<?php include("footer.php");?>