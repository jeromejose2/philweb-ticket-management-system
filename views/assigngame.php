<?php

/*
 * Author: Noel Antonio
 * Date Created: 2011-08-26
 * Company: Philweb Corporation
 */

require_once ("init.inc.php");
$pagesubmenuid = 18;
include_once("../controller/managesession.php");
include("../controller/assigngamecontroller.php");
?>

<?php include("header.php"); ?>
        <link rel="stylesheet" type="text/css" media="screen" href="../css/default.css" />
        <script language="javascript" type="text/javascript">
            function SelectProduct()
            {
                document.forms[0].submit();
            }
        </script>
        <form action="assigngame" method="POST">
	<div id="fade" class="black_overlay"></div>
           <table>
                    <tr><td>Provider : </td><td><?php echo $cboProvider;?></td></tr>
                    <tr><td>Product : </td><td><?php echo $cboProduct;?></td></tr>
                    <tr><td>Game Number : </td><td><?php echo $txtGameID;?></td></tr>
                <tr>
                    <td></td>
                    <td align="right">
                        <?php echo $btnSave;?>
                        <?php echo $btnCancel;?>
                    </td>
                </tr>
           </table>
        <!-- POP UP FOR CONFIRMATION -->
        <div id="light" class="white_content">
            <div id="title" class="light-title"></div>
            <br /><br />
            <div id="msg" class="light-message"></div>
            <br/><br/><br />
            <div id="button" class="light-button">
                <?php echo $btnConfirm;?>
		<input type="button" onclick="javascript: document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none';" value="CANCEL"/>
	    </div>
	</div>
        <!-- POP UP FOR CONFIRMATION -->
        <!-- POP UP FOR MESSAGES -->
        <div id="light5" class="white_content">
            <div id="title5" class="light-title"></div>
            <br /><br />
            <div id="msg5" class="light-message"></div>
            <br/><br/><br />
            <div id="button" class="light-button">
                <?php if (isset($added)) { echo $btnOkay; } else {?>
		<input type="button" onclick="javascript: document.getElementById('light5').style.display='none';document.getElementById('fade').style.display='none';" value="OKAY"/>
                <?php } ?>
            </div>
	</div>
        <!-- POP UP FOR MESSAGES -->
        </form>
<?php include("footer.php"); ?>
        
<?php if (isset($confirm)) : ?>
        <script>
            document.getElementById('title').innerHTML = "CONFIRMATION";
            document.getElementById('msg').innerHTML = "Proceed in assigning new game.";
            document.getElementById('light').style.display = 'block';
            document.getElementById('fade').style.display = 'block';
        </script>
<?php endif;?>
<?php $confirm = null;?>
<?php if (isset($added)) : ?>
        <script>
            document.getElementById('title5').innerHTML = "SUCCESSFUL";
            document.getElementById('msg5').innerHTML = "Game Assignment succeed.";
            document.getElementById('light5').style.display = 'block';
            document.getElementById('fade').style.display = 'block';
        </script>
<?php endif;?>
<?php $added = null;?>

<script type="text/javascript">
    function validate(e) 
    { 
        var k; document.all ? k = e.keyCode : k = e.which;
        return ((k > 47 && k < 58) || (k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k==13 || k==32); 
    }
    
    function chkInputs()
    {
        var cboProv = document.getElementById("cboProvider");
        var strProv = cboProv.options[cboProv.selectedIndex].value;
        var cboProd = document.getElementById("cboProduct");
        var strProd = cboProd.options[cboProd.selectedIndex].value;
        
        if (strProv == 0){
            document.getElementById('title5').innerHTML = "ERROR";
            document.getElementById('msg5').innerHTML = "Please select a provider.";
            document.getElementById('light5').style.display = 'block';
            document.getElementById('fade').style.display = 'block';
            return false;
        }
        else if (strProd == 0){
            document.getElementById('title5').innerHTML = "ERROR";
            document.getElementById('msg5').innerHTML = "No product under this Provider.";
            document.getElementById('light5').style.display = 'block';
            document.getElementById('fade').style.display = 'block';
            return false;
        }
        else if (document.getElementById("txtGameID").value.trim() == ""){
            document.getElementById('title5').innerHTML = "ERROR";
            document.getElementById('msg5').innerHTML = "Enter your assigned Game Number.";
            document.getElementById('light5').style.display = 'block';
            document.getElementById('fade').style.display = 'block';
            return false;
        } else { return true;}
    }
    
    function isNumber(evt) {
      var charCode = (evt.which) ? evt.which : event.keyCode;
      if (charCode > 31 && (charCode < 48 || charCode > 57)) {
         return false;
      }
      return true;
   }
</script>