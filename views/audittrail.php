<?php
/*
 * @author
 * Purpose   : view for audittrail
 */
require_once ("init.inc.php");
$pagesubmenuid = 17;
include_once("../controller/managesession.php");
include("../controller/audittrailprocess.php");
?>
<html>
    <link rel="stylesheet" href="../css/black.css" />
     <script language="javascript">
        function ChangePage(pagenum)
        {
            selectedindex = document.getElementById("pgSelectedPage");
            selectedindex.value = pagenum;
            document.forms[0].submit();
        }
    </script>
    <body>
        <form name="frmAuditTrail" method="post">
            <div>
                <div>Audit Trail</div>
                <div><?php echo $txtDate; ?>&nbsp;&nbsp;<?php echo $btnQuery; ?></div>
                <div><?php echo $trailtable; ?></div>
            </div>          
        </form>
    </body>
</html>
