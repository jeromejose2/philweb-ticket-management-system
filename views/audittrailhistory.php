<?php
/*
 * @author
 * Purpose   : view for audittrailhistory
 */
include("init.inc.php");
$pagesubmenuid = 17;
include_once("../controller/managesession.php");
include("../controller/audittrailprocess.php");
?>
<link rel="stylesheet" type="text/css" href="css/datepicker.css" />
<script type="text/javascript" src="jscripts/datetimepicker.js"></script>
<script language="javascript" type="text/javascript">
    function ChangePage(pagenum)
    {
        selectedindex = document.getElementById("pgSelectedPage");
        selectedindex.value = pagenum;
        document.forms[0].submit();
    }
</script>
<?php include("header.php"); ?>
<form name="frmAuditTrail" method="post">
<div class="titleCont">
            <div class="titleCont_left"></div>
            <div class="titleCont_body">Audit Trail Logs</div>
            <div class="titleCont_right"></div>
</div>
<!-- POP UP FOR MESSAGES -->
<div id="fade" class="black_overlay"></div>
<div id="light" class="white_content">
<div id="title" class="light-title"></div>
<div id="msg" class="light-message"></div>
<div id="button" class="light-button"><input id="btnOk" name="btnOk" type="button" onclick="javascript: document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none';" value="OKAY"/></div>
<div class="light-footer"></div>
</div>    
<!-- POP UP FOR MESSAGES -->

<br />
<div class="content-page">
<?php echo $txtFromDate; ?><img name="cal" src="images/close.gif" width="16" height="16" border="0" alt="Pick a date" onClick="displayDatePicker('txtFromDate', false, 'ymd', '-');"/>
&nbsp;<?php echo $ddlFunction; ?>&nbsp;<?php echo $btnSubmit; ?>
<br/>
<br/>
<div class="form-page">
	<?php echo $pgTransactionHistory;?>
</div>   
<?php if(isset($tbldata_list)):?>  
<div class="form-page">
    <?php //echo $btnExport;?>
</div>
<div class="form-page">
	<?php echo $btnExportCSV;?>
</div> 
<table class="table-list">
    <tr>
        <th align="left">Date</th>
        <th align="left">Username</th>
        <!--<th align="left">Transaction Details</th>-->
        <th align="left">IP Address</th>
        <th align="left">Function</th>
    </tr>
    <?php if(count($tbldata_list) > 0):?>
    <?php for($ctr = 0 ; $ctr < count($tbldata_list) ; $ctr++):?>
    <?php ($ctr % 2) == 0 ? $class = "evenrow" : $class = "oddrow" ; ?>
    <tr class = "<?php echo $class?>">
        <?php $dateTime = new DateTime($tbldata_list[$ctr]['TransDateTime']);?>
        <td><?php echo $dateTime->format('Y-m-d h:i:s A') ?></td>
        <td><?php echo $tbldata_list[$ctr]['UserName']?></td>
        <!--<td><?php echo $tbldata_list[$ctr]['TransDetails']?></td>-->
        <td><?php echo $tbldata_list[$ctr]['RemoteIP']?></td>
        <td><?php echo $tbldata_list[$ctr]['AuditFunctionName']?></td>
    </tr>
    <?php endfor;?>
    <?php else:?>
    <tr class="no-record"><td colspan="4">No records to display</td></tr>
    <?php endif;?>
</table>
<?php endif;?>    
</div>
</form>
<div class="content-page">
	<div class="form-page">
		<?php echo $pgTransactionHistory;?>
	</div>
</div>  
<?php include("footer.php"); ?>
<script language="javascript" type="text/javascript">
function checkaudittrail()
{
    var sDate = document.getElementById("txtFromDate").value;
    var selDate = new Date(sDate);
    var currDate = new Date();
    if (sDate.length == 0)
    {
        document.getElementById('title').innerHTML= 'ERROR!';
        document.getElementById('msg').innerHTML = 'Please select the date to be displayed.';
        document.getElementById('light').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
//    if (selDate > currDate)
//    {
//        document.getElementById('title').innerHTML = "ERROR";
//        document.getElementById('msg').innerHTML = "The date is ahead of time.";
//        document.getElementById('light').style.display = 'block';
//        document.getElementById('fade').style.display = 'block';
//        return false;
//    }
    return true;
}
</script>