<?php
/*
 * Created By       :       Noel D. Antonio
 * Date Created     :       June 29, 2012
 * Purpose          :       To send log emails to authorized recipients
 */

include("init.inc.php");

App::LoadCore("PHPMailer.class.php");
$email_list = App::getParam('email');
$email_name = App::getParam('name');
$csvnaming = App::getParam('csvnaming');
$path = dirname(__FILE__);

$pm = new PHPMailer();

/*   
for ($i=0; $i < count($email_list); $i++)
{
    $pm->AddAddress($email_list[$i], $email_name[$i]);
}
*/
$pm->AddAddress("mccalderon@philweb.com.ph", "Tere Calderon");


$pm->Body = "Dear Everyone, <br /><br />
             Please see the log file as of current date " . date("m/d/Y") . "<br /><br />Thank you.";
$pm->IsHTML(true);
$pm->AddEmbeddedImage($path.'/logs/logs_' . date("Ymd") . '.txt','logs','logs_' . date("Ymd") . '.txt');
$pm->From = "no-reply";
$pm->FromName = "Ticket Management System";
$pm->Host = "localhost";
$pm->Subject = $csvnaming . " - Gold Scratch N' Win - Daily Logs for " . date("m/d/Y");
if(!$pm->Send())
{
    echo "Email Sending Failed!";
}
else
{
    echo "Email Sent!";
}
?>
