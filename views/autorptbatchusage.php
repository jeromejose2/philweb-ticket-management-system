<?php
/*
 * Author: Noel Antonio
 * Date Created: 2012-01-19
 * Company: Philweb Corporation
 */

$path = dirname(__FILE__);
include("init.inc.php");
include($path.'../jpgraph/jpgraph.php');
include($path.'../jpgraph/jpgraph_bar.php');

App::LoadModuleClass("TicketManagementCM", "TMGameManagement");
App::LoadModuleClass("TicketManagementCM", "TMProducts");
App::LoadModuleClass("TicketManagementCM", "TMDecks");
App::LoadModuleClass("TicketManagementCM", "TMWinnings");
App::LoadModuleClass("TicketManagementCM", "TMTickets");
App::LoadModuleClass("TicketManagementCM", "TMGameImport");
App::LoadCore("PHPMailer.class.php");

$TMDecks = new TMDecks();
$TMGames = new TMGameManagement();
$TMProducts = new TMProducts();
$TMWinnings = new TMWinnings();
$TMTickets = new TMTickets();
$TMGameImport = new TMGameImport();

//PHPMAILER -------------------------------------------------------------------
$pm = new PHPMailer();

//$pm->AddAddress("mtgrandinetti@philweb.com.ph", "Michael T. Grandinetti");
//$pm->AddAddress("sasproule@philweb.com.ph", "Scott A. Sproule");
//$pm->AddAddress("flmariano@philweb.com.ph", "Ferdimark L. Mariano");
//$pm->AddAddress("akgarcia@philweb.com.ph", "Antonio Jose K. Garcia");
//$pm->AddAddress("mvtayamen@philweb.com.ph", "Mazy V. Tayamen");
//$pm->AddAddress("zmprieto@philweb.com.ph", "Zaldy M. Prieto");
//$pm->AddAddress("rmbuenaventura@philweb.com.ph", "Rocky M. Buenaventura");
//$pm->AddAddress("magatdula@philweb.com.ph", "Marie Abigael A. Gatdula");
//$pm->AddAddress("aajalla@philweb.com.ph", "Al Rashid A. Jalla");

$pm->AddAddress("ndantonio@philweb.com.ph", "Noel D. Antonio");
$pm->AddAddress("mccalderon@philweb.com.ph", "Ma. Theresa C. Calderon");

$pageURL = 'http';
if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
    $pageURL .= "://";
    $folder = $_SERVER["REQUEST_URI"];
    $folder = substr($folder,0,strrpos($folder,'/') + 1);
if ($_SERVER["SERVER_PORT"] != "80") 
{
    $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$folder;
} 
else 
{
    $pageURL .= $_SERVER["SERVER_NAME"].$folder;
}

$pm->Body="Dear Everyone, <br/><br/>";

$activegames = $TMGames->SelectStatusActive();
for ($cntr=0;$cntr<count($activegames);$cntr++)
{
    $gameid = $activegames[$cntr]["GameID"];
    // Query for Date Imported
    $di = $TMGameImport->GetDateImported($gameid,0);
    $di = $di[0]["DateImported"];
    // Query  for Batch Consumption
    $batchsize = $TMDecks->GetBatchSize($gameid,0);
    $batchsize = $batchsize[0]['DeckSize'];
    $twe = $TMWinnings->GetTotalWinningEntries($gameid,0);
    $twe = $twe[0]['WinningsCount'];
    $teu = $TMDecks->GetTotalEntriesUsed($gameid,0);
    $teu = $teu[0]['EntriesUsed'];
    $tce = $TMTickets->GetCancelledEntries($gameid,0);
    $tce = $tce[0]['CancelledTicketCount'];
    $be = (($batchsize - $teu) - $tce);
    $bcp = round((($teu / $batchsize) * 100), 4) . "%";

    // Query for Prize Amount Consumption
    $tp = $TMWinnings->GetTotalPrizeAmount($gameid,0);
    $tp = $tp[0]['TotalPrize'];
    $pu = $TMWinnings->GetTotalPrizeUsed($gameid,0);
    $pu = $pu[0]['TotalPrizeUsed'];
    $pl = ($tp - $pu);
    $pcp = round((($pu / $tp) * 100), 4) . "%";

    // Query for Prize Counter
    $getprize = $TMWinnings->GetPrizeCounter($gameid,0);
    $getprize_list = new ArrayList();
    $getprize_list->AddArray($getprize);
    $getprizeclaimed = $TMWinnings->GetPrizeCounterClaimed($gameid,0);
    $getprizeclaimed_list = new ArrayList();
    $getprizeclaimed_list->AddArray($getprizeclaimed);

    $pm->IsHTML(true);
        
            $body="<table border=\"1\" style=\"text-align: center;\">
            <tr align=\"center\">
                <th style=\"background-color: #F2B523;\">Date Imported</th>
                <td>".$di."</td>
            </tr></table><br/>
                
            <table border=\"1\" style=\"text-align: center;\">
            <th style=\"background-color: #F2B523;\" colspan=\"6\">BATCH CONSUMPTION</th>
            <tr align=\"center\">
                <th style=\"background-color: #F2B523;\">Batch Size</th>
                <th style=\"background-color: #F2B523;\">Total Winning Entries</th>
                <th style=\"background-color: #F2B523;\">Total Entries Used</th>
                <th style=\"background-color: #F2B523;\">Total Canceled Entries</th>
                <th style=\"background-color: #F2B523;\">Balance Entries</th>
                <th style=\"background-color: #F2B523;\">Batch Consumption Percentage</th>
            </tr>".
            "<tr>".
                "<td>". $batchsize ."</td>" .
                "<td>". $twe ."</td>" .
                "<td>". $teu ."</td>" .
                "<td>". $tce ."</td>".
                "<td>". $be ."</td>".
                "<td>". $bcp ."</td></tr>".
            "</table><br/>
                
            <table border=\"1\" style=\"text-align: center;\">
            <th style=\"background-color: #F2B523;\" colspan=\"4\">PRIZE AMOUNT CONSUMPTION</th>
            <tr align=\"center\">
                <th style=\"background-color: #F2B523;\">Total Prize</th>
                <th style=\"background-color: #F2B523;\">Prize Used</th>
                <th style=\"background-color: #F2B523;\">Prize Left</th>
                <th style=\"background-color: #F2B523;\">Prize Consumption Percentage</th>
            </tr>".
            "<tr>".
                "<td>". $tp ."</td>" .
                "<td>". $pu ."</td>" .
                "<td>". $pl ."</td>" .
                "<td>". $pcp ."</td></tr>".
            "</table><br/>".
            "<table border=\"1\" style=\"text-align: center;\">
            <th style=\"background-color: #F2B523;\" colspan=\"5\">PRIZE COUNTER</th>
            <tr align=\"center\">
                <th style=\"background-color: #F2B523;\">Prize Type</th>
                <th style=\"background-color: #F2B523;\">Total Quantity of Prize</th>
                <th style=\"background-color: #F2B523;\">Total Quantity Used</th>
                <th style=\"background-color: #F2B523;\">Prize Balance Entries</th>
                <th style=\"background-color: #F2B523;\">Consumption Percentage</th>
            </tr>";
             
        for ($i=0;$i<count($getprize_list);$i++)
        {
            if ($getprize_list[$i]['TQP'] == ""){ $getprize_list[$i]['TQP'] = 0;}
            if ($getprizeclaimed_list[$i]['TQU'] == ""){ $getprizeclaimed_list[$i]['TQU'] = 0;}
            $pbe = $getprize_list[$i]['TQP'] - $getprizeclaimed_list[$i]['TQU'];
            $cp = round((($getprizeclaimed_list[$i]['TQU'] / $getprize_list[$i]['TQP']) * 100), 4) . "%";
            
            $temp = "<tr>".
                "<td>". $getprize_list[$i]['PrizeName'] ."</td>" .
                "<td>". $getprize_list[$i]['TQP'] ."</td>" .
                "<td>". $getprizeclaimed_list[$i]['TQU'] ."</td>" .
                "<td>". $pbe ."</td>".
                "<td>". $cp ."</td></tr>";
            $body.=$temp;
        };
    $body.="</table><br/>";   
    $pm->Body.=$body;
}
$pm->From = "testaccount@philweb.com.ph";
$pm->FromName = "Ticket Management System";
$pm->Host = "localhost";
$pm->Subject = "Scratch Card Batch Usage Report";
$email_sent = $pm->Send();

//PHPMAILER -------------------------------------------------------------------
?>