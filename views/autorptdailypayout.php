<?php
/**
 * @author: Noel Antonio
 * @datecreated: 2011-08-31
 * @copyright: 2011 @ Philweb Corporation
 * 
 * lastupdatedby: Noel Antonio
 * dateupdated: Feb. 13, 2013
 * purpose: fix string errors in displaying the report.
 */

include("init.inc.php");

App::LoadModuleClass("TicketManagementCM", "TMGameManagement");
App::LoadModuleClass("TicketManagementCM", "TMWinnings");
App::LoadCore("PHPMailer.class.php");

$TMWinnings = new TMWinnings();
$TMGames = new TMGameManagement();
$currdate = "now";
$body = "";
$regards = "";
$array = array();
$array2 = array();

$currency = App::getParam('currency');
$email_list = App::getParam('email');
$email_name = App::getParam('name');
$regards = App::getParam('country');
$csvnaming = App::getParam('csvnaming');

$ds = new DateSelector($currdate);
$date = $ds->PreviousDate;
$arrgames = $TMGames->SelectAllGamesActive();
$game_list = new ArrayList();
$game_list->AddArray($arrgames);

$getNonMonetaryPrizeName = $TMWinnings->SelectNonMonetaryPrizeName();
$bike = $getNonMonetaryPrizeName[0][0];

$arr_monetary = $TMWinnings->SelectMonetaryPayoutbyKHR($date, $bike);
$list_monetary = new ArrayList();
$list_monetary->AddArray($arr_monetary);

$arr_nonmonetary = $TMWinnings->SelectNonMonetaryPayout($date, $bike);
$list_nonmonetary = new ArrayList();
$list_nonmonetary->AddArray($arr_nonmonetary);

$overall_bike = 0;
$m_norecords = false;
$nm_norecords = false;

$pm = new PHPMailer();

for ($i = 0 ; $i < count($email_list); $i++)
{
    $pm->AddAddress($email_list[$i], $email_name[$i]);
}
    $pm->AddBCC("mccalderon@philweb.com.ph", "Ma. Theresa C. Calderon");
    $pm->AddBCC("ndantonio@philweb.com.ph", "Noel Antonio");
    $pm->AddBCC("rpsanchez@philweb.com.ph", "Roger Sanchez");
$pm->Body = "Dear Everyone, <br/><br/>
             Please see the payout report for " . date("m/d/Y", strtotime($ds->PreviousDate)) . ".<br/><br/>";
$pm->IsHTML(true);

$numbercolspan = number_format(count($game_list) + 4);
$body.='<table width="100%">';
$body.='<table width="100%" style="text-align: center; padding:3px; border-color: #FFFFFF; border-style-inner: solid;">';
$body.='<tr>';
$body.='<th style="background-color: #F2B523;"colspan='.$numbercolspan.'>Payout Summary</th>';
$body.='</tr>';
$body.='<tr style="background-color: #F2B523;" align="center">
        <th>Distributor Account</th>
        <th>Account Type</th>';
        for ($i = 0;$i < count($game_list); $i++)
        {
            $explode = explode(".", $game_list[$i]["CardPrice"]);
            if ($explode[1] != "00" || $explode[1] > 00)
                $strcp = $game_list[$i]["CardPrice"];
            else
                $strcp = floor($game_list[$i]["CardPrice"]);
            
            $body .= '<th style="background-color: #F2B523;">' . $game_list[$i]["ProductName"] . '<br>' . '[$' . $strcp . ']' . '</th>';
        }
        
        $body.='<th>Total Payout Processed</th>
                <th>Total Redemption Incentive</th></tr>';

        /* --- Start of MONETARY PAYOUT --- */
        if (count($list_monetary) > 0)
        {
                for ($i=0; $i < count($list_monetary); $i++)
                {
                        $distributor = $list_monetary[$i]["UserName"];
                        $name = $list_monetary[$i]["Name"];
                        $currentgame = $list_monetary[$i]["ProductID"];
                        $accttype = $list_monetary[$i]["AccountTypeID"];
                        $prizetypeid = $list_monetary[$i]["PrizeType"];
                        
                        if (strpos($list_monetary[$i]["PrizeName"], 'KHR') !== false)
                        {
                            $khrcurrency = preg_replace('/[\KHR,]/', '', $list_monetary[$i]["PrizeName"]);
                            $payout = convertCurrtodollar($khrcurrency);
                        }
                        else if (strpos($list_monetary[$i]["PrizeName"], '$') !== false)
                        {
                            $dollarcurrency = preg_replace('/[\$,]/', '', $list_monetary[$i]["PrizeName"]);
                            $payout = $dollarcurrency;
                        }
                        
                        for ($a = 0; $a < count($game_list); $a++)
                        {
                                if ($game_list[$a]["ProductID"] == $currentgame)
                                {
                                        if ($prizetypeid == 1)
                                        {
                                                $array[$distributor][$game_list[$a]["ProductName"]][1]["payout"] += $payout;
                                                $array2[$distributor]["name"] = $name;
                                                $array[$distributor][$game_list[$a]["ProductName"]][1]["accttype"] = $accttype;
                                        }
                                        else
                                        {
                                                $array[$distributor][$game_list[$a]["ProductName"]][2]["payout"] += $payout;
                                                $array2[$distributor]["name"] = $name;
                                                $array[$distributor][$game_list[$a]["ProductName"]][2]["accttype"] = $accttype;
                                        }
                                }
                        }
                }
                
                $distr_key = array_keys($array);
                $redemption_incentive = 0;
                for ($i = 0; $i < count($distr_key); $i++)
                {
                    ($i % 2) == 0 ? $bgcolor = "#fdeb95" : $bgcolor = "#fbf4de" ;
                    $body .= '<tr style="background-color:' . $bgcolor . '; padding:3px; border-color: #FFFFFF; border-style-inner: solid; border-bottom-style: solid; border-left-style: solid; border-right-style: solid; border-top-style: solid;">';
                    $body .= '<td>' . $distr_key[$i] . '</td>';
                    $body .= '<td>' . $array2[$distr_key[$i]]["name"] . '</td>';
                    
                    $game_key = array_values($array);
                    for ($a = 0; $a < count($game_list); $a++)
                    {
                        if (array_key_exists($game_list[$a]["ProductName"], $game_key[$i]))
                        {
                            $major_payout = $game_key[$i][$game_list[$a]["ProductName"]][1]["payout"];
                            $minor_payout = $game_key[$i][$game_list[$a]["ProductName"]][2]["payout"];
                            $total_payout = $major_payout + $minor_payout;
                            $body .= '<td>';
                            $body .= $currency.' '.number_format($total_payout, 2);
                            $body .= '</td>';
                            
                            if ($game_key[$i][$game_list[$a]["ProductName"]][2]["accttype"] == 6)
                            {
                                $redemption_incentive += $minor_payout;
                            }
                        }
                        else
                        {
                            $body .= '<td>';
                            $body .= $currency.' '.number_format(0, 2);
                            $body .= '</td>';
                        }
                        
                        $total_payout_processed += $total_payout;
                        $total_payout = 0;
                    }
                    
                    $redemption_incentive = $redemption_incentive * (2 / 100);
                    $body .= '<td>' . $currency.' '.number_format($total_payout_processed, 2) . '</td>';
                    $body .= '<td>' . $currency.' '.number_format($redemption_incentive, 2) . '</td>';
                    $overall_monetary += $total_payout_processed;
                    $overall_incentive += $redemption_incentive;
                    $total_payout_processed = 0;
                    $redemption_incentive = 0;
                }
                unset($array); unset($array2);
        }
        else
        {
                $m_norecords = true;
                $overall_monetary = 0;
                $overall_bike = 0;
                $overall_incentive = 0;
        }
        /* --- End of MONETARY PAYOUT --- */
        
        
        /* --- Start of NON-MONETARY PAYOUT --- */
        if (count($list_nonmonetary) > 0)
        {
                for ($i=0; $i < count($list_nonmonetary); $i++)
                {
                        $distributor = $list_nonmonetary[$i]["UserName"];
                        $name = $list_nonmonetary[$i]["Name"];
                        $currentgame = $list_nonmonetary[$i]["ProductID"];
                        $payout = $list_nonmonetary[$i]["TotalPrizeName"];
                        $prizename = $list_nonmonetary[$i]["PrizeName"];
                        $total = $list_nonmonetary[$i]["Total"];
                        
                        for ($a=0; $a < count($game_list); $a++)
                        {
                                if ($game_list[$a]["ProductID"] == $currentgame)
                                {
                                        if (!isset($array[$distributor][$game_list[$a]["ProductName"]]))
                                        {
                                               $array[$distributor][$game_list[$a]["ProductName"]] = $payout;
                                               $array2[$distributor]["name"] = $name;
                                               $array2[$distributor]["total"] += $total;
                                        }
                                }
                                else
                                {
                                        if ($array[$distributor][$game_list[$a]["ProductName"]] == '')
                                                unset($array[$distributor][$game_list[$a]["ProductName"]]);
                                }
                        }
                }

                $arr_keys = array_keys($array);
                for ($i = 0; $i < count($arr_keys); $i++)
                {      
                        $total_payout_processed = 0;
                        ($i % 2) == 0 ? $bgcolor = "#fdeb95" : $bgcolor = "#fbf4de" ;
                        $body.='<tr style="background-color:' . $bgcolor . '; padding:3px; border-color: #FFFFFF; border-style-inner: solid; border-bottom-style: solid; border-left-style: solid; border-right-style: solid; border-top-style: solid;">';
                        $body.='<td>'.$arr_keys[$i].'</td>';
                        $body.='<td>'.$array2[$arr_keys[$i]]["name"].'</td>';

                        $key = array_values($array);
                        for ($a=0; $a < count($game_list); $a++)
                        {
                                if (array_key_exists($game_list[$a]["ProductName"], $key[$i]))
                                {
                                        $body.= '<td>'.$key[$i][$game_list[$a]["ProductName"]].'</td>';
                                        $total_payout_processed += $key[$i][$game_list[$a]["ProductName"]];
                                }
                                else                                
                                        $body.= '<td>--</td>';
                        }

                        $total_payout_processed += $array2[$arr_keys[$i]]["total"]; 
                        $overall_bike += $total_payout_processed;

                        $body.= '<td>[' . $total_payout_processed . '] '. $prizename .'</td>';
                        $body.= '<td>--</td>';
                }
        }
        else
        {
                $nm_norecords = true;
        }
        /* --- End of NON-MONETARY PAYOUT --- */
        
    if ($m_norecords && $nm_norecords)
    {
            $body.='<tr style="background-color:' . $bgcolor . '; padding:3px; border-color: #FFFFFF; border-style-inner: solid; border-bottom-style: solid; border-left-style: solid; border-right-style: solid; border-top-style: solid;">';
            $body.='<td colspan=' . $numbercolspan . '>No Records Found.</td>';
    }
     
    $body.='<tr style="background-color: #fdeb95; padding:3px; border-color: #FFFFFF; border-style-inner: solid; border-bottom-style: solid; border-left-style: solid; border-right-style: solid; border-top-style: solid;">';
    $body.='<th colspan='.(($numbercolspan)-1).' align="right"> Overall Monetary Payout Processed :</th>';
    $body.='<td>'. $currency . ' ' . number_format($overall_monetary, 2) . '</td>';
    $body.=' <tr style="background-color: #fbf4de; padding:3px; border-color: #FFFFFF; border-style-inner: solid; border-bottom-style: solid; border-left-style: solid; border-right-style: solid; border-top-style: solid;">';
    $body.='<th colspan='.(($numbercolspan)-1).' align="right"> Total Motorbike Processed :</th>';
    $body.='<td>'. $overall_bike .'</td>';
    $body.='<tr style="background-color: #fdeb95; padding:3px; border-color: #FFFFFF; border-style-inner: solid; border-bottom-style: solid; border-left-style: solid; border-right-style: solid; border-top-style: solid;">';
    $body.='<th colspan='.(($numbercolspan)-1).' align="right"> Overall Redemption Incentive :</th>';
    $body.='<td>'. $currency . ' ' . number_format($overall_incentive, 2).'</td>';
    $body.='</tr></table><br></table>';
    $body.='<br/>Regards,<br/><br/>'. $regards . ' Ticket Management System';
    
    
    /* --- CSV Attachment --- */
    $path = dirname(__FILE__) . "/";
    $arrWinnings1 = $TMWinnings->SelectWinningsAutomatedReport($date);
    $win_list1 = new ArrayList();
    $win_list1->AddArray($arrWinnings1); 

    $csvData1 = array();
    for ($i=0;$i<count($win_list1);$i++)
    {   
            $date1 = $win_list1[$i]["DateClaimed"];
            $acct1 = $win_list1[$i]["UserName"];
            $gameno1 = $win_list1[$i]["GameNumber"];
            $gamename1 = $win_list1[$i]["ProductName"];
            $bookno1 = trim(str_replace(array('\r', '\n', '\r\n'), "", $win_list1[$i]["TicketNumber"]));
            $virn1 = trim(str_replace(array('\r', '\n', '\r\n'), "", $win_list1[$i]["ValidationNumber"]));
            $cardprice = $win_list1[$i]["CardPrice"];
            
            //start
            $nocomma =  str_replace( ',', '',$win_list1[$i]['PrizeName']);
            $getcurrency =  str_replace( ' ', '',$nocomma);
            // Check for currency
            if(substr($getcurrency, 0,1) == '$') //dollar
            {
                    $prizename1 = $win_list1[$i]['PrizeName'];
            } 
            else if(substr($getcurrency, 0,3) == 'KHR')//for KHR
            {
                    //convert to KHR to dollar
                    $khrcurrency = convertCurrtodollar($getcurrency);
                    $prizename1 = $currency.number_format($khrcurrency , 0 , "" , ",");
            }
            else 
            {
                    $prizename1 = $win_list1[$i]["PrizeName"];
            }
            //end   

            if ($win_list1[$i]['Status'] == '2')
                $status1 = 'Claimed';
            else
                $status1 = 'Re-Imbursed';

            $preg_prize = preg_match('#\((.*?)\)#', $prizename1, $match);
            if ($preg_prize >= 1) // with parentheses
            {
                    $csv_prize = str_replace(array(' ', ','), "", $prizename1);
            }
            else
            {
                    $csv_prize = str_replace(",", "", $prizename1);
            }

            $csvData1[] = $date1.",".$acct1.",".$cardprice.",".$gameno1.",".$gamename1.",".$bookno1.",".$virn1.",".$csv_prize.",".$status1."\r\n";
    }

    $fp = fopen($path . "../csv/Payout_History_Email.csv","w");
    if ($fp)
    {
            $payouthistorytitle = 'Payout History';
            $arrpayouthistoryprocessed = $payouthistorytitle ."\r\n";
            fwrite($fp,$arrpayouthistoryprocessed);
            fwrite($fp,"\r\n");
            $header = "Date, Distributor Account, Card Value, Game Number, Game Name,  Book Ticket Number , Validation Number, Prize Won,  Status " . "\r\n";
            fwrite($fp,$header);
            if($csvData1)
            {
                    foreach($csvData1 as $rc)
                    {
                            if(count($rc)>0)
                            {
                                    fwrite($fp,$rc);
                            }
                    }
            }
            else
            { 
                    $rc = "No Records Found\r\n";
                    fwrite($fp,$rc);
            }
    }
    fclose($fp);
    $pm->AddAttachment($path . '../csv/Payout_History_Email.csv', 'Payout_History.csv');

    $pm->Body.=$body;          
    $pm->From = "no-reply@goldscratchnwin.com";
    $pm->FromName = "Ticket Management System";
    $pm->Host = "localhost";
    $pm->Subject = $csvnaming . " - Gold Scratch N' Win - Daily Payout for ".date("M d, Y", strtotime($ds->PreviousDate));
    
    $filename = $path . "/logs/logs_" . date("Ymd") . ".txt";
    $fp = fopen($filename , "a");  
    
    if(!$pm->Send())
    {
        fwrite($fp, $csvnaming . " Payout - Sent Date & Time: " . date('Y-m-d H:i:s') . "\r\n"); 
        fwrite($fp,"Error sending: " . $pm->ErrorInfo . ">\r\n");
    }
    else
    {        
        fwrite($fp, $csvnaming . " Payout - Sent Date & Time: " . date('Y-m-d H:i:s') . "\r\n");
        for ($i=0; $i < count($email_list); $i++)
        {            
            fwrite($fp,"Sent To: " . $email_name[$i]. " <" . $email_list[$i] . ">\r\n");
        }
    }
    fclose($fp);
    
/*****************Functions  *************/
function convertCurrtodollar($currencyMoney)
{
    App::LoadModuleClass("TicketManagementCM", "TMCurrencies");
    $TMcurrencies = new TMCurrencies();

    $money =  str_replace( 'KHR', '',$currencyMoney);
    $moneycurrency = 'KHR';

    $getconvertmoney = $TMcurrencies->SelectByCurrencySymbol($moneycurrency);
    $fixedrate = $getconvertmoney[0]['FixedRate'];

    if(is_numeric($fixedrate) && is_numeric($money))
    {
        $truevalue = ($money/$fixedrate);
        $truevalue =   number_format($truevalue,0,'.','');
    }
    return $truevalue;
}
/*****************Functions  *************/   
?>
