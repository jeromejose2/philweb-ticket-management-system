<?php
/*
 * Author: Noel Antonio
 * Date Created: 2011-08-31
 * Company: Philweb Corporation
 * 
 * Modified By: Noel Antonio - Jan. 31, 2013
 */

$path = dirname(__FILE__);
include("init.inc.php");
include($path.'/../jpgraph/jpgraph.php');
include($path.'/../jpgraph/jpgraph_bar.php');

App::LoadModuleClass("TicketManagementCM", "TMDecks");
App::LoadModuleClass("TicketManagementCM", "TMGameManagement");
App::LoadModuleClass("TicketManagementCM", "TMAccounts");
App::LoadModuleClass("TicketManagementCM", "TMTicketTransferHistory");
App::LoadCore("PHPMailer.class.php");

$tmdecks = new TMDecks();
$tmgames = new TMGameManagement();
$tmaccounts = new TMAccounts();
$tmtickettransfer = new TMTicketTransferHistory();

$currdate = "now";
$cntr = 0;
$JanSales = 0;
$FebSales = 0;
$MarSales = 0;
$AprSales = 0;
$MaySales = 0;
$JunSales = 0;
$JulSales = 0;
$AugSales = 0;
$SepSales = 0;
$OctSales = 0;
$NovSales = 0;
$DecSales = 0;
$date = '';
$data = '';
$final_array = '';
$total_in_week = 0;
$total_in_month = 0;
$total_in_year = 0;
$total_tickets_sold = 0;
$total_sales_amount = 0;
$total_tickets_transfer = 0;
$total_tickets_recalled = 0;
$regards = "";

$email_list = App::getParam('email');
$email_name = App::getParam('name');
$regards = App::getParam('country');
$csvnaming = App::getParam('csvnaming');

$ds = new DateSelector($currdate);
$chkday = $ds->GetCurrentDateFormat("N");
$ds->SetDate($ds->GetFirstDayOfMonth());
$chkday_firstdayofmonth = $ds->GetCurrentDateFormat("N");
$ds->SetDate($currdate);
if ($chkday == 1)
{
    $ds->AddWeeks(-1);
    $start = $ds->CurrentDate;
    $ds->SetDate($currdate);
    $end = $ds->PreviousDate;
} else {
    do {
        $ds->AddDays(-1);
        $chkday = $chkday - 1;
    } while ($chkday != 1);
    $start = $ds->CurrentDate;
    $ds->SetDate($currdate);
    $end = $ds->PreviousDate;
}

if ($chkday_firstdayofmonth != 1)
{
    $ds->SetDate($ds->GetFirstDayOfMonth());
    do {
        $ds->AddDays(-1);
        $chkday_firstdayofmonth = $chkday_firstdayofmonth - 1;
    } while ($chkday_firstdayofmonth != 1);
    $startoftheweek = $ds->CurrentDate;
    $ds->SetDate($currdate);
    $end = $ds->PreviousDate;
}

if ($ds->GetFirstDayOfMonth() == $ds->CurrentDate)
{
    $ds->AddDays(-1);
    $startmonth = $ds->GetFirstDayOfMonth();
    $endmonth = $ds->CurrentDate;
    $ds->SetDate($currdate);
} else {
    $startmonth = $ds->GetFirstDayOfMonth();
    $endmonth = $ds->PreviousDate;
}

$ds->SetDate($currdate);

if (date("Y", strtotime($ds->CurrentDate)) < date('Y'))
{
    $firstdayofyear = date("Y-", strtotime($ds->CurrentDate)) . '01-01';
    $ytd_dateto = date("Y-", strtotime($ds->CurrentDate)) . '12-31';
}
else
{
    $firstdayofyear = date("Y-", strtotime($end)) . '01-01';
    $ytd_dateto = $end;
}
                
$alldates = getDaysRange($start, $end);
$dailysales = $tmdecks->GetAutoEmailDateSalesGraph($start, $end, 0);
if(is_array($dailysales))
{
    foreach($dailysales as $rec)
    {
        $data[$rec["xAxis"]] += $rec["Sales"];
        $date[] = $rec["xAxis"];
    }
}

$temp = getCombinedDates($alldates, $date, $data);
$temp = array_reverse($temp);
foreach ($temp as $rec)
{
    $datay1[] = $rec["Sales"];
    $datax1[] = date("M d", strtotime($rec["xAxis"]));
}
$graph1 = createGraph($datax1, $datay1, "Daily Sales", "Date Released", "No. of Tickets Sold", "Total Tickets Sold", "dailysalesgraph.jpg");


$alldates = getDaysRange($startoftheweek, $end);
$weeklysales = $tmdecks->GetAutoEmailDateSalesGraph($startoftheweek, $end, 0);
if(is_array($weeklysales))
{
    foreach($weeklysales as $rec)
    {
        $data[$rec["xAxis"]] += $rec["Sales"];
        $date[] = $rec["xAxis"];
    }
}


$temp = getCombinedDates($alldates, $date, $data);
$temp = array_reverse($temp);
$temp1 = array();
$total_sales = 0;
$total_count = count($temp);
$lastdate = '';
foreach ($temp as $rec)
{
    $cntr++;
    $total_sales += $rec['Sales'];

    if($lastdate == '') {
        $lastdate = date("M d", strtotime($rec['xAxis']));
    }
    if($cntr % 7 == 0) {
        $temp1[$cntr]['Sales'] = $total_sales;
        $temp1[$cntr]['xAxis'] = $lastdate . ' - ' . date("M d", strtotime($rec['xAxis']));
        $total_sales = 0;
        $lastdate = '';
    }
    else if ($cntr == $total_count)
    {
        $temp1[$cntr]['Sales'] = $total_sales;
        $temp1[$cntr]['xAxis'] = $lastdate . ' - ' . date("M d", strtotime($rec['xAxis']));
    }
}

foreach ($temp1 as $rec)
{
    $datay2[] = $rec["Sales"];
    $datax2[] = $rec["xAxis"];
}
$graph2 = createGraph($datax2, $datay2, "Weekly Sales", "Date Released", "No. of Tickets Sold", "Total Tickets Sold", "weeklysalesgraph.jpg");


$alldates = getDaysRange($firstdayofyear, $end);
$monthlysales = $tmdecks->GetAutoEmailDateSalesGraph($firstdayofyear, $end, 0);
if(is_array($monthlysales))
{
    foreach($monthlysales as $rec)
    {
        $data[$rec["xAxis"]] += $rec["Sales"];
        $date[] = $rec["xAxis"];
    }
}

$temp = getCombinedDates($alldates, $date, $data);
$temp = array_reverse($temp);
foreach ($temp as $rec)
{
    $month = date("m", strtotime($rec["xAxis"]));
    if ($rec["Sales"] == NULL || $rec["Sales"] == ''){ $rec["Sales"] = 0;}
    switch ($month)
    {
        case "01": $JanSales += $rec["Sales"]; break;
        case "02": $FebSales += $rec["Sales"]; break;
        case "03": $MarSales += $rec["Sales"]; break;
        case "04": $AprSales += $rec["Sales"]; break;
        case "05": $MaySales += $rec["Sales"]; break;
        case "06": $JunSales += $rec["Sales"]; break;
        case "07": $JulSales += $rec["Sales"]; break;
        case "08": $AugSales += $rec["Sales"]; break;
        case "09": $SepSales += $rec["Sales"]; break;
        case "10": $OctSales += $rec["Sales"]; break;
        case "11": $NovSales += $rec["Sales"]; break;
        case "12": $DecSales += $rec["Sales"]; break;
    }
}

$yeartitle = $ds->GetCurrentDateFormat("Y");
$ds = new DateSelector($currdate);
$datay3 = array($JanSales,$FebSales,$MarSales,$AprSales,$MaySales,$JunSales,$JulSales,$AugSales,$SepSales,$OctSales,$NovSales,$DecSales);
$datax3 = array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
$graph3 = createGraph($datax3, $datay3, "Monthly Sales", $yeartitle, "No. of Tickets Sold", "Total Tickets Sold", "monthlysalesgraph.jpg");
$JanSales=0;$FebSales=0;$MarSales=0;$AprSales=0;$MaySales=0;$JunSales=0;$JulSales=0;$AugSales=0;$SepSales=0;$OctSales=0;$NovSales=0;$DecSales=0;


/* ------------------------ START OF GRAPH FOR DISTRIBUTOR SALES ------------------------ */
$datax4 = array();
$array = array();
$width = (20 * 10) + 1000;
$graph = new Graph($width,750);
$graph->SetScale("textlin");
$theme_class=new UniversalTheme;
$graph->SetTheme($theme_class);
$graph->img->SetAntiAliasing(false);
$graph->SetBox(false);
$graph->yaxis->HideTicks(false,false);
$graph->xgrid->Show();
$graph->xaxis->SetTitle("Date Released", 'middle');
$graph->yaxis->SetTitle("No. of Tickets Sold", 'middle');
$graph->SetMargin(100, 100, 100, 100);
$graph->yaxis->SetTitleMargin(70);
$graph->yaxis->SetLabelFormatCallback("number_format");
$graph->xgrid->SetColor('#E3E3E3');

$alldates = getDaysRange($firstdayofyear, $end);
$users = $tmaccounts->SelectAllActiveRetailer();
for ($x=0;$x<count($users);$x++)
{
    $dailysales = $tmdecks->GetAutoEmailDateSalesGraph($firstdayofyear, $end, $users[$x]["AID"]);
    if(is_array($dailysales))
    {
        foreach($dailysales as $rec)
        {
            $data[$rec["xAxis"]] += $rec["Sales"];
            $date[] = $rec["xAxis"];
        }
    } else {
        for ($a=0;$a<count($alldates);$a++)
        {
            $data[$alldates[$a]] += 0;
            $date[] = $alldates[$a];
        }
    }
    $temp = getCombinedDates($alldates, $date, $data);
    $temp = array_reverse($temp);
    foreach ($temp as $rec)
    {
        $month = date("m", strtotime($rec["xAxis"]));
        switch ($month)
        {
            case "01": $JanSales += (isset($rec["Sales"])) ? $rec["Sales"] : 0; break;
            case "02": $FebSales += (isset($rec["Sales"])) ? $rec["Sales"] : 0; break;
            case "03": $MarSales += (isset($rec["Sales"])) ? $rec["Sales"] : 0; break;
            case "04": $AprSales += (isset($rec["Sales"])) ? $rec["Sales"] : 0; break;
            case "05": $MaySales += (isset($rec["Sales"])) ? $rec["Sales"] : 0; break;
            case "06": $JunSales += (isset($rec["Sales"])) ? $rec["Sales"] : 0; break;
            case "07": $JulSales += (isset($rec["Sales"])) ? $rec["Sales"] : 0; break;
            case "08": $AugSales += (isset($rec["Sales"])) ? $rec["Sales"] : 0; break;
            case "09": $SepSales += (isset($rec["Sales"])) ? $rec["Sales"] : 0; break;
            case "10": $OctSales += (isset($rec["Sales"])) ? $rec["Sales"] : 0; break;
            case "11": $NovSales += (isset($rec["Sales"])) ? $rec["Sales"] : 0; break;
            case "12": $DecSales += (isset($rec["Sales"])) ? $rec["Sales"] : 0; break;
        }
    }
    $datay4 = array($JanSales,$FebSales,$MarSales,$AprSales,$MaySales,$JunSales,$JulSales,$AugSales,$SepSales,$OctSales,$NovSales,$DecSales);
    $p1 = new BarPlot($datay4);
    $p1->SetColor("#c50909");
    $p1->SetLegend($users[$x]["UserName"]);
    $array[] = $p1;
    unset($datay4);unset($data);unset($temp);unset($p1);unset($dailysales);
    $JanSales=0;$FebSales=0;$MarSales=0;$AprSales=0;$MaySales=0;$JunSales=0;$JulSales=0;$AugSales=0;$SepSales=0;$OctSales=0;$NovSales=0;$DecSales=0;
}
$gplot = new GroupBarPlot($array);
$graph->Add($gplot);
$datax4 = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
$graph->title->Set("Distributor Sales");
$graph->xaxis->SetTickLabels($datax4);
$graph->legend->SetFrameWeight(1);
$graph->Stroke($path.'/images_sales/'.'dailysalesgraphperdistributor.jpg');
/* ------------------------ END OF GRAPH FOR DISTRIBUTOR SALES ------------------------ */


$arrgames = $tmgames->SelectAllGamesActive();
$game_list = new ArrayList();
$game_list->AddArray($arrgames);


/* ------------------------ START OF PHPMAILER ------------------------ */
$pm = new PHPMailer();

/*for ($i=0; $i < count($email_list); $i++)
{
    $pm->AddAddress($email_list[$i], $email_name[$i]);
}
$pm->AddBCC("mccalderon@philweb.com.ph", "Ma. Theresa C. Calderon");
$pm->AddBCC("rpsanchez@philweb.com.ph", "Roger Sanchez");
$pm->AddBCC("kvcapala@philweb.com.ph", "Kim V. Capala");
*/
$pm->AddBCC("mccalderon@philweb.com.ph", "Ma. Theresa C. Calderon");

$pm->Body = "Dear Everyone, <br/><br/>
            Please see the weekly and monthly sales report. As of current date " . date("m/d/Y", strtotime($ds->PreviousDate)) . " the number of cards sold are as follow:<br/><br/>";

$pm->IsHTML(true);
$pm->AddEmbeddedImage($path.'/images_sales/dailysalesgraph.jpg','pic1','dailysalesgraph.jpg');
$pm->AddEmbeddedImage($path.'/images_sales/weeklysalesgraph.jpg','pic2','weeklysalesgraph.jpg');
$pm->AddEmbeddedImage($path.'/images_sales/monthlysalesgraph.jpg','pic3','monthlysalesgraph.jpg');
$pm->AddEmbeddedImage($path.'/images_sales/dailysalesgraphperdistributor.jpg','pic4','dailysalesgraphperdistributor.jpg');


/* ------------------------ START OF TICKET SALES REPORT ------------------------ */
$colspan = count($game_list) + 5;
    
$body = '<table white-space="nowrap" border="1" style="text-align: center;">
        <th style="background-color: #F2B523;" colspan=' . $colspan . '>TICKET SALES REPORT</th>
        <tr align="center"><th style="background-color: #F2B523;">Date</th>';
        $alldates = getDaysRange($start, $end);
        $ticketsales = $tmdecks->GetAutoEmailTicketSalesReport2($start, $end);
        for ($x = 0;$x < count($alldates); $x++)
        {
            $seldate = $alldates[$x];
            for ($i = 0; $i < count($ticketsales); $i++)
            {
                $sale = $ticketsales[$i];
                if ($seldate == $sale["xAxis"])
                {
                    $final_array[$seldate][$sale["ProductName"]]["Sales"] += $sale["Sales"];
                    $final_array[$seldate][$sale["ProductName"]]["Total"] += $sale["Total"];
                    $final_array[$seldate][$sale["ProductName"]]["Transfer"] += 0;
                    ksort($final_array[$seldate]);
                }
            }
        }
            
        for ($c = 0;$c < count($alldates); $c++)
        {
            $seldate = $alldates[$c];
            for ($d = 0;$d < count($game_list); $d++)
            {
                if (!isset($final_array[$seldate][$game_list[$d]["ProductName"]]))
                {
                    $final_array[$seldate][$game_list[$d]["ProductName"]]["Sales"] += 0;
                    $final_array[$seldate][$game_list[$d]["ProductName"]]["Total"] += 0;
                    $final_array[$seldate][$game_list[$d]["ProductName"]]["Transfer"] += 0;
                    $final_array[$seldate][$game_list[$d]["ProductName"]]["Recalled"] += 0;
                    ksort($final_array[$seldate]);
                }
            }
        }
        ksort($final_array);

        // Tickets Transferred
        $tickettransfer = $tmtickettransfer->GetTotalTicketsTransferred($start, $end, 0, 0, 0, 0, 0);
        if (is_array($tickettransfer))
        {
            for ($i = 0; $i < count($tickettransfer); $i++)
            {
                $seldate = date('Y-m-d', strtotime($tickettransfer[$i]["xAxis"]));
                $final_array[$seldate][$tickettransfer[$i]["ProductName"]]["Transfer"] += $tickettransfer[$i]["Transfer"];
            }
        }
            
        // Tickets Recalled
        $ticketsrecalled = $tmtickettransfer->GetTicketsRecalledReport($start, $end, 0, 0, 0, 0, 0);
        if (is_array($ticketsrecalled))
        {
            for ($i = 0; $i < count($ticketsrecalled); $i++)
            {
                $seldate = date('Y-m-d', strtotime($ticketsrecalled[$i]["xAxis"]));
                $final_array[$seldate][$ticketsrecalled[$i]["ProductName"]]["Recalled"] += $ticketsrecalled[$i]["Recalled"];
            }
        }

        // Display
        if (count($game_list) > 0)
        {
            for ($i=0;$i<count($game_list);$i++)
            {
                $explode = explode(".", $game_list[$i]["CardPrice"]);
                if ($explode[1] != "00" || $explode[1] > 00)
                    $strcp = $game_list[$i]["CardPrice"];
                else
                    $strcp = floor($game_list[$i]["CardPrice"]);

                $body .= '<th style="background-color: #F2B523;">' . $game_list[$i]["ProductName"] . "<br>" . '[$' . $strcp . ']' . '</th>';
            }
        }
        $body .= '<th style="background-color: #F2B523;">Total Tickets Sold</th>
                    <th style="background-color: #F2B523;">Total Tickets Transferred</th>
                    <th style="background-color: #F2B523;">Total Tickets Recalled</th>
                    <th style="background-color: #F2B523;">Total Sales</th></tr>';

    foreach ($final_array as $date => $val)
    {
        ($i % 2) == 0 ? $bgcolor = "#FDEB95" : $bgcolor = "#FBF4DE" ;
        $body .= '<tr style="background-color:' . $bgcolor . '"><td>'. date("d-M-Y", strtotime($date)) .'</td>';
        //foreach ($val as $game => $sales)
        for ($a = 0;$a < count($game_list); $a++)
        {
            $game = $game_list[$a]["ProductName"];
            $body .= '<td>';
            $body .= number_format($val[$game]["Sales"]);
            $body .= '</td>';

            $total_tickets_sold += $val[$game]["Sales"];
            $total_sales_amount += $val[$game]["Total"];
            $total_tickets_transfer += $val[$game]["Transfer"];
            $total_tickets_recalled += $val[$game]["Recalled"];
        }
        $body .= '<td>'. number_format($total_tickets_sold) .'</td>
                  <td>'. number_format($total_tickets_transfer) .'</td>
                  <td>'. number_format($total_tickets_recalled) .'</td>
                  <td>'. number_format($total_sales_amount) .'</td>';
        $body .= '</tr>';
        $i++;
        $total_tickets_sold = 0;
        $total_sales_amount = 0;
        $total_tickets_transfer = 0;
        $total_tickets_recalled = 0;
    }
    
$body .= '</table><br/>';
/* ------------------------ END OF TICKET SALES REPORT ------------------------ */

    
    
/* ------------------------ START OF REPORT SUMMARY ------------------------ */
$body .= '<table white-space="nowrap" border="1" style="text-align: center;">
        <th style="background-color: #F2B523;" colspan=' . $colspan . '>REPORT SUMMARY</th>
        <tr align="center"><th style="background-color: #F2B523;"></th>';
    
        if (count($game_list) > 0)
        {
            for ($i=0;$i<count($game_list);$i++)
            {
                $explode = explode(".", $game_list[$i]["CardPrice"]);
                if ($explode[1] != "00" || $explode[1] > 00)
                    $strcp = $game_list[$i]["CardPrice"];
                else
                    $strcp = floor($game_list[$i]["CardPrice"]);

                $body .= '<th style="background-color: #F2B523;">' . $game_list[$i]["ProductName"] . "<br>" . '[$' . $strcp . ']' . '</th>';
            }
        }
        $body .= '<th style="background-color: #F2B523;">Total Tickets Sold</th>
                <th style="background-color: #F2B523;">Total Tickets Transferred</th>
                <th style="background-color: #F2B523;">Total Tickets Recalled</th>
                <th style="background-color: #F2B523;">Total Sales</th></tr>';
    
        // Tickets Transfer (to Date)
        $wtdtransfer = $tmtickettransfer->GetTicketTransferred($start, $end, 0, 0, 0, 0, 0);
        $total_transfer_week = 0 + $wtdtransfer[0]["Transfer"];
        $mtdtransfer = $tmtickettransfer->GetTicketTransferred($startmonth, $endmonth, 0, 0, 0, 0, 0);
        $total_transfer_mos = 0 + $mtdtransfer[0]["Transfer"];
        $ytdtransfer = $tmtickettransfer->GetTicketTransferred($firstdayofyear, $ytd_dateto, 0, 0, 0, 0, 0);
        $total_transfer_yr = 0 + $ytdtransfer[0]["Transfer"];

        // Tickets Recalled (to Date)
        $wtdtransfer = $tmtickettransfer->GetRecalled($start, $end, 0, 0, 0, 0, 0);
        $total_recalled_week = 0 + $wtdtransfer[0]["Recalled"];
        $mtdtransfer = $tmtickettransfer->GetRecalled($startmonth, $endmonth, 0, 0, 0, 0, 0);
        $total_recalled_mos = 0 + $mtdtransfer[0]["Recalled"];
        $ytdtransfer = $tmtickettransfer->GetRecalled($firstdayofyear, $ytd_dateto, 0, 0, 0, 0, 0);
        $total_recalled_yr = 0 + $ytdtransfer[0]["Recalled"];
    
        $body .= '<tr align="center" style="background-color: #FDEB95;"><th>Week to Date</th>';
        for ($i=0;$i<count($game_list);$i++)
        {
            $prodID = $game_list[$i]["ProductID"];
            $gameID = $game_list[$i]["GameID"];
            $weektodatesales = $tmdecks->GetAutoEmailTotalSales($start, $end, $gameID, $prodID);
            $body .= '<td>';
            $body .= number_format($weektodatesales[0]["Sales"]);
            $body .= '</td>';
            $total_in_week += $weektodatesales[0]["Sales"];
            $total_sales_week += $weektodatesales[0]["Total"];
        }
    
        $body .= '<td>' . number_format($total_in_week) . '</td>
                <td>' . number_format($total_transfer_week) . '</td>
                <td>' . number_format($total_recalled_week) . '</td>
                <td>' . number_format($total_sales_week) . '</td></tr>';

        $body .= '<tr align="center" style="background-color: #FBF4DE;"><th>Month to Date</th>';
        for ($i=0;$i<count($game_list);$i++)
        {
            $prodID = $game_list[$i]["ProductID"];
            $gameID = $game_list[$i]["GameID"];
            $monthtodatesales = $tmdecks->GetAutoEmailTotalSales($startmonth ,$endmonth, $gameID, $prodID);
            $body .= '<td>';
            $body .= number_format($monthtodatesales[0]["Sales"]);
            $body .= '</td>';
            $total_in_month += $monthtodatesales[0]["Sales"];
            $total_sales_month += $monthtodatesales[0]["Total"];
        }
        $body .= '<td>' . number_format($total_in_month) . '</td>
                <td>' . number_format($total_transfer_mos) . '</td>
                <td>' . number_format($total_recalled_mos) . '</td>
                <td>' . number_format($total_sales_month) . '</td></tr>';

        $body .= '<tr align="center" style="background-color: #FDEB95;"><th>Year to Date</th>';
        for ($i=0;$i<count($game_list);$i++)
        {
            $prodID = $game_list[$i]["ProductID"];
            $gameID = $game_list[$i]["GameID"];
            $yeartodatesales = $tmdecks->GetAutoEmailTotalSales($firstdayofyear, $ytd_dateto, $gameID, $prodID);
            $body .= '<td>';
            $body .= number_format($yeartodatesales[0]["Sales"]);
            $body .= '</td>';
            $total_in_year += $yeartodatesales[0]["Sales"];
            $total_sales_year += $yeartodatesales[0]["Total"];
        }
        $body .= '<td>' . number_format($total_in_year) . '</td>
                <td>' . number_format($total_transfer_yr) . '</td>
                <td>' . number_format($total_recalled_yr) . '</td>
                <td>' . number_format($total_sales_year) . '</td></tr>';
/* ------------------------ END OF REPORT SUMMARY ------------------------ */

    
$body .= '</table><br/>
        <img src="cid:pic1" alt="pic1"><br/>
        <img src="cid:pic2" alt="pic2"><br/>
        <img src="cid:pic3" alt="pic3">
        <img src="cid:pic4" alt="pic4"><br/><br/>
        Regards,<br/><br/>' . $regards . ' Ticket Management System';

$pm->Body .= $body;

$pm->From = "no-reply@goldscratchnwin.com";
$pm->FromName = "Ticket Management System";
$pm->Host = "localhost";
$pm->Subject = $csvnaming . " - Gold Scratch N' Win - Daily Sales Report for ".date("M d, Y", strtotime($ds->PreviousDate));

$filename = $path . "/logs/logs_" . date("Ymd") . ".txt";
$fp = fopen($filename , "a");

if(!$pm->Send())
{
    fwrite($fp, $csvnaming . " Sales - Sent Date & Time: " . date('Y-m-d H:i:s') . "\r\n");
    fwrite($fp,"Error sending: " . $pm->ErrorInfo . ">\r\n");
}
else
{

    fwrite($fp,$csvnaming . " Sales - Sent Date & Time: " . date('Y-m-d H:i:s') . "\r\n");
    for ($i=0; $i < count($email_list); $i++)
    {
        fwrite($fp,"Sent To: " . $email_name[$i]. " <" . $email_list[$i] . ">\r\n");
    }
}
fclose($fp);
/* ------------------------ END OF PHPMAILER ------------------------ */


function getDaysRange($start, $end)
{
    $ds = new DateSelector($end);
    $days_between = ceil(abs(strtotime($end) - strtotime($start)) / 86400);
    for ($i=0;$i<=$days_between;$i++)
    {
        $alldates[] = $ds->CurrentDate;
        $ds->AddDays(-1);
    }
    return $alldates;
}

function getCombinedDates($alldates, $date, $data)
{
    $chkdate = '';
    $temp = array();
    for ($i=0;$i<count($alldates);$i++)
    {
        $boolean = false;
        $chkdate = $alldates[$i];
        if (is_array($date))
        {
                for ($x=0;$x<count($date);$x++)
                {
                        $mydate = $date[$x];
                        if ($chkdate == $mydate)
                        {
                                $boolean = true;
                        }
                }
        }
        
        if ($boolean == true)
        {
            if (isset($data[$chkdate]) || !empty($data[$chkdate]))
            {
                $temp[$i]['Sales'] = $data[$chkdate];
            }
            $temp[$i]['xAxis'] = $chkdate;
        }
        else
        {
            $temp[$i]['Sales'] = 0;
            $temp[$i]['xAxis'] = $chkdate;
        }
        $chkdate = "";
    }
    return $temp;
}

function createGraph($xdata, $ydata, $title, $xtitle, $ytitle, $legend, $img_name)
{
    $path = dirname(__FILE__);
    $cntx = count($xdata);
    $width = ($cntx * 10) + 600;
    $graph = new Graph($width,750);
    $graph->SetScale("textlin");
    $theme_class=new UniversalTheme;
    $graph->SetTheme($theme_class);
    $graph->img->SetAntiAliasing(false);
    $graph->title->Set($title);
    $graph->SetBox(false);
    $graph->yaxis->HideTicks(false,false);
    $graph->xgrid->Show();
    $graph->xaxis->SetTickLabels($xdata);
    $graph->xaxis->SetTitle($xtitle, 'middle');
    $graph->yaxis->SetTitle($ytitle, 'middle');
    $graph->SetMargin(100, 100, 100, 100);
    $graph->yaxis->SetTitleMargin(70);
    $graph->yaxis->SetLabelFormatCallback("number_format");
    $graph->xgrid->SetColor('#E3E3E3');
    $p1 = new BarPlot($ydata);
    $graph->Add($p1);
    $p1->SetColor("#c50909");
    $p1->SetLegend($legend);
    $graph->legend->SetFrameWeight(1);
    $graph->Stroke($path.'/images_sales/'.$img_name);
    return true;
}
?>
