<?php

/*
 * Created By: Noel Antonio
 * Date: March 8, 2012
 * Purpose: Record history of batch file uploads.
 */
include("init.inc.php");
$pagesubmenuid = 46;
include_once("../controller/managesession.php");
include("../controller/batchfilehistoryprocess.php");
?>

<script language ="javascript" type="text/javascript">
        function get_gamenumber()
        {
            document.getElementById("ddlgametype").onchange = null;
            document.getElementById("flag").value = 1;
            document.getElementById("xmltype").value = 1;
            var prodid = document.getElementById('ddlgamename').options[document.getElementById('ddlgamename').selectedIndex].value;
            if (prodid == 0)
            {
               document.getElementById("ddlgametype").onchange= function(){get_gamename();};        
            }
            $("#ddlgametype").load(
                "../controller/get_gamenumwithdefall.php",
                {
                    prodid: prodid
                },
                function(status, xhr)
                {
                    if(status == "error")
                    {
                        document.getElementById('title5').innerHTML = "ERROR!";
                        document.getElementById('msg5').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
                        document.getElementById('light5').style.display = "block";
                        document.getElementById('fade').style.display = "block";
                    }
                }
            );
        }
        function get_gamename()
        {
           document.getElementById("ddlgamename").onchange = null;
           document.getElementById("xmltype").value = 1;  
           document.getElementById("flag").value = 2;
           var gamenum = document.getElementById('ddlgametype').options[document.getElementById('ddlgametype').selectedIndex].value;
           if (gamenum ==0)
               {
                  document.getElementById("ddlgamename").onchange = get_gamenumber; 
               }
            $("#ddlgamename").load(
                "../controller/get_gamenamewithdefall.php",
                {
                    gamenum: gamenum
                },

                function(status, xhr)
                {
                    if(status == "error")
                    {
                        document.getElementById('title').innerHTML = "ERROR!";
                        document.getElementById('msg').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
                        document.getElementById('light').style.display = "block";
                        document.getElementById('fade').style.display = "block";
                    }
                }
            );
        }
        
        function ChangePage(pagenum)
        {
            selectedindex = document.getElementById("pgSelectedPage");
            selectedindex.value = pagenum;
            document.forms[0].submit();
        }
    </script>

<?php include("header.php"); ?>
 <!-- POP UP FOR MESSAGES -->
 <div id="fade" class="black_overlay"></div>
<div id="loading" class="loading"></div>
<div id="light" class="white_content">
    <div id="title" class="light-title"></div>
    <div id="msg" class="light-message"></div>
    <div id="button" class="light-button">
        <input type="button" onclick="javascript: document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none';" value="Okay"/>
    </div>
    <div class="light-footer"></div>
</div>
<!-- POP UP FOR MESSAGES -->
<form name="frmXMLUpload" method="post" enctype="multipart/form-data">
    <?php echo $hidaid;?>
    <?php echo $hiddenflag;?>
    <?php echo $xmltype;?>
    <?php echo $flag;?>
    <div class="titleCont">
    <div class="titleCont_left"></div>
    <div class="titleCont_body">Batch File Validation History</div>
    <div class="titleCont_right"></div>
</div>
<div class="content-page">
    <table width="80%">
        <tr>
            <td><?php echo $ddlUsername; ?></td>
            <td><?php echo $ddlgamename; ?></td>
            <td><?php echo $ddlgametype; ?></td>
        </tr>
    </table>
    
    <table width="58%">
        <tr>
            <td>From:</td>
            <td>
                <?php echo $txtDateFr;?>
                <img id="cal" name="cal" src="images/close.gif" width="16" height="16" border="0" alt="Pick a date" onClick="displayDatePicker('txtDateFr', false, 'ymd', '-');"/>
            </td>
            <td>To:</td>
            <td>
                <?php echo $txtDateTo;?>
                <img id="cal" name="cal" src="images/close.gif" width="16" height="16" border="0" alt="Pick a date" onClick="displayDatePicker('txtDateTo', false, 'ymd', '-');"/>
            </td>
            <td>
                <div class="form-button" style="margin-right: 18%;">
                    <?php echo $btnSubmit;  ?>
                </div>
            </td>
        </tr>
     </table><br/><br/>
    
    <table class="table-list" style="clear: both;">
        <tr>
            <th>Date</th>
            <th>Game Number</th>
            <th>Game Name</th>
            <th>Retailer</th>
            <th>File Name</th>
            <th>Session ID</th>
<!--            <th>No. of Records</th>-->
        </tr>
        <?php if (isset($batchhistory)): ?>
        <div class="form-page"><?php echo $btnExportCSV;?></div>
        <?php endif; ?>
        <div class="form-page"><?php echo $pgTransactionHistory;?></div>
        <?php if(count($batchhistory) > 0){?>
        <?php for($ctr = 0 ; $ctr < count($batchhistory) ; $ctr++):?>
        <?php ($ctr % 2) == 0 ? $class = "evenrow" : $class = "oddrow" ; ?>
        <tr class = "<?php echo $class?>">
                <?php $dateTime = new DateTime($batchhistory[$ctr]['TransactionDate']); ?>
            <td align="center"><?php echo $dateTime->format('Y-m-d h:i:s A');?></td>
            <td align="center"><?php echo $batchhistory[$ctr]['GameNumber']?></td>
            <td align="center"><?php echo $batchhistory[$ctr]['ProductName']?></td>
            <td align="center"><?php echo $batchhistory[$ctr]['UserName']?></td>
            <td align="center"><?php echo $batchhistory[$ctr]['FileName']?></td>
            <td align="center"><?php echo $batchhistory[$ctr]['BatchFileValidationSessionID']?></td>
<!--            <td align="center"><?php echo $batchhistory[$ctr]['Records']?></td>-->
        </tr>
        <?php endfor;?>
        <?php } else {?>
        <tr class="no-record"><td colspan="9">No records to display</td></tr>
        <?php }?>
    </table>
</div>   
</form>
<?php include("footer.php"); ?>

<script language="javascript" type="text/javascript">
function validate()
{
    var SDate = document.getElementById("txtDateFr").value;
    var EDate = document.getElementById("txtDateTo").value;
    var endDate = new Date(EDate);
    var startDate = new Date(SDate);
    var currDate = new Date();

    var days_between = 0;
    days_between = (endDate.getTime() - startDate.getTime()) / 86400000;
    if (days_between > 31)
    {
        document.getElementById('title').innerHTML = "ERROR";
        document.getElementById('msg').innerHTML = "Date range selection should not exceed within 30 days.";
        document.getElementById('light').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    else if(SDate != '' && EDate != '' && startDate > endDate)
    {
    	document.getElementById('title').innerHTML = "ERROR";
        document.getElementById('msg').innerHTML = "Start date is ahead than the End Date.";
        document.getElementById('light').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }   
    else if (SDate.trim() == "")
    {
        document.getElementById('title').innerHTML = "ERROR";
        document.getElementById('msg').innerHTML = "Please select your Start Date.";
        document.getElementById('light').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    else if (EDate.trim() == "")
    {
        document.getElementById('title').innerHTML = "ERROR";
        document.getElementById('msg').innerHTML = "Please select your End Date.";
        document.getElementById('light').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    else if (endDate > currDate)
    {
        document.getElementById('title').innerHTML = "ERROR";
        document.getElementById('msg').innerHTML = "The date is ahead of time.";
        document.getElementById('light').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    else{ return true;}
}
</script>