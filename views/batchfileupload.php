<?php
/**
 * @author      :   Noel Antonio
 * Purpose      :   view for batchfileupload
 */
include("init.inc.php");
$pagesubmenuid = 44;
include_once("../controller/managesession.php");
include("../controller/batchfileuploadprocess.php");
?>
<script language="javascript" src="jscripts/jquery-1.5.2.min.js"></script>
<script language ="javascript" type="text/javascript">
        function validation()
        {
            var cardvalue = document.getElementById('ddlcardvalue').options[document.getElementById('ddlcardvalue').selectedIndex].value;
            var username = document.getElementById('ddlUsername').options[document.getElementById('ddlUsername').selectedIndex].value;
            var prodid = document.getElementById('ddlgamename').options[document.getElementById('ddlgamename').selectedIndex].value;
            var gameid = document.getElementById('ddlgametype').options[document.getElementById('ddlgametype').selectedIndex].value;
            
            if(cardvalue == 0)
            {
                document.getElementById('title').innerHTML = "ERROR!";
                document.getElementById('msg').innerHTML = " Please select card value.";
                document.getElementById('light').style.display = "block";
                document.getElementById('fade').style.display = "block";
                return false;
            }
            else if (username == 0)
            {
                document.getElementById('title').innerHTML = "ERROR!";
                document.getElementById('msg').innerHTML = " Please select distributor.";
                document.getElementById('light').style.display = "block";
                document.getElementById('fade').style.display = "block";
                return false;
            }
            else if (prodid == 0)
            {
                document.getElementById('title').innerHTML = "ERROR!";
                document.getElementById('msg').innerHTML = " Please select game name.";
                document.getElementById('light').style.display = "block";
                document.getElementById('fade').style.display = "block";
                return false;
            }
            else if (gameid== 0)
            {
                document.getElementById('title').innerHTML = "ERROR!";
                document.getElementById('msg').innerHTML = " Please select game number.";
                document.getElementById('light').style.display = "block";
                document.getElementById('fade').style.display = "block";
                return false;
            }
            document.getElementById("hiddenflag").value = 1;
            return true;
        }
        
        function SearchValidation()
        {
            var searchkey = document.getElementById('txtSearchByID').value;
            if (searchkey.length == 0 || searchkey == '')
            {
                document.getElementById('title').innerHTML = "ERROR!";
                document.getElementById('msg').innerHTML = " Please enter search details.";
                document.getElementById('light').style.display = "block";
                document.getElementById('fade').style.display = "block";
                return false;
            }
            return true;
        }

        function get_gamenumber()
        {
            $("#ddlgametype").attr('disabled',false);
            var prodid = document.getElementById('ddlgamename').options[document.getElementById('ddlgamename').selectedIndex].value;
            var cardvalue = document.getElementById('ddlcardvalue').options[document.getElementById('ddlcardvalue').selectedIndex].value;
            var prodid1 = prodid;

            if(prodid == 0)
            {
                $("#ddlgametype").attr('disabled',true);
            }

            if ( (prodid == 0 || cardvalue == 0) && (prodid != 0 && cardvalue == 0))
            { 
                prodid1 = 0;
            }

            $("#ddlgametype").load(
                "../controller/get_gamenum.php",
                {
                    prodid: prodid1
                },

                function(status, xhr)
                {
                    if(status == "error")
                    {
                        document.getElementById('title').innerHTML = "ERROR!";
                        document.getElementById('msg').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
                        document.getElementById('light').style.display = "block";
                        document.getElementById('fade').style.display = "block";
                    }
                }
            );
        }
        
        function get_dist()
        {
                $("#ddlUsername").attr('disabled',false);
                var setTodefault = document.getElementById ('ddlUsername');
                setTodefault.value = 0; //set to default -please select
                var cardvalue = document.getElementById('ddlcardvalue').options[document.getElementById('ddlcardvalue').selectedIndex].value;
                var status = document.getElementById('ddlUsername').options[document.getElementById('ddlUsername').selectedIndex].value;
                
                if(cardvalue == 0)
                {
                    $("#ddlgamename").empty();
                    $("#ddlgametype").empty();
                    $("#ddlgamename").prepend('<option selected value=0>Please Select</option>');
                    $("#ddlgametype").prepend('<option selected value=0>Please Select</option>');
                    
                    $("#ddlUsername").attr('disabled',true);
                    $("#ddlgamename").attr('disabled',true);
                    $("#ddlgametype").attr('disabled',true);
                    document.getElementById("hiddenflag").value = 0;
                }
                
                if(cardvalue != 0  &&  status == 0 )
                {
                    $("#ddlgamename").attr('disabled',true);
                    $("#ddlgametype").attr('disabled',true);
                }
        }
        
        function get_gamename()
        {
            $("#ddlgamename").attr('disabled',false);
            var setTodefault = document.getElementById ('ddlgamename');
            setTodefault.value = 0; //set to default -please select
            var cardvalue = document.getElementById('ddlcardvalue').options[document.getElementById('ddlcardvalue').selectedIndex].value;
            var gamename = document.getElementById('ddlgamename').options[document.getElementById('ddlgamename').selectedIndex].value;
            var ddlusername = document.getElementById('ddlUsername').options[document.getElementById('ddlUsername').selectedIndex].value;
            var gamenum = document.getElementById('ddlgametype').options[document.getElementById('ddlgametype').selectedIndex].value;
            var flag = 0;

            if(ddlusername == 0)
            {
                $("#ddlgamename").attr('disabled',true);
                $("#ddlgametype").attr('disabled',true);
            }
            
            if(ddlusername != 0 && (gamename != 0 || gamename == 0)  && (gamenum != 0 || gamenum == 0))
            {
                $("#ddlgametype").attr('disabled',true);
                  
            }

            $("#ddlgamename").load(
                "../controller/get_gamename.php",
                {
                    cardvalue: cardvalue,
                    gamename: gamename,
                    flag: flag
                },

                function(status, xhr)
                {
                    if(status == "error")
                    {
                        document.getElementById('title').innerHTML = "ERROR!";
                        document.getElementById('msg').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
                        document.getElementById('light').style.display = "block";
                        document.getElementById('fade').style.display = "block";
                    }
                }
            );
        }
        
        function confirm()
        {
            document.getElementById('title3').innerHTML = "<?php echo "CLAIM"; ?>";
            document.getElementById('msg3').innerHTML = "<?php echo "Continue?"; ?>";
            document.getElementById('light3').style.display = 'block';
            document.getElementById('fade').style.display = 'block';
        }
        
        $(document).ready(function(){

                if ($('#hiddenflag').val() == 1)
                {
                    $("#ddlgamename").attr('disabled',false);
                    $("#ddlgametype").attr('disabled',false);
                    $("#ddlUsername").attr('disabled',false);
                }
                else
                {
                    $("#ddlgamename").attr('disabled',true);
                    $("#ddlgametype").attr('disabled',true);
                    $("#ddlUsername").attr('disabled',true);
                }
        });
        
    </script>
<?php include("header.php"); ?>
<div id="fade" class="black_overlay"></div>
<div id="loading" class="white_content">
	<div class="light-title"></div>
    <div class="light-message">
        Uploading the csv file.<br/>
        <div class="loading" id="loading-image"></div>
    </div>
	<div class="light-footer"></div>
</div>
 <!-- POP UP FOR MESSAGES -->
<div id="light" class="white_content">
    <div id="title" class="light-title"></div>
    <div id="msg" class="light-message"></div>
    <div id="button" class="light-button">
        <input type="button" onclick="javascript: document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none';" value="Okay"/>
    </div>
    <div class="light-footer"></div>
</div>
<!-- POP UP FOR MESSAGES -->
<form name="frmXMLUpload" method="post" enctype="multipart/form-data">
<!-- POP UP FOR MESSAGES -->
<div id="light2" class="white_content">
    <div id="title2" class="light-title"></div>
    <div id="msg2" class="light-message"></div>
    <div id="button2" class="light-button">
        <div id="button2A" class="button2A"></div>
        <div id="button2B" class="button2B">
            <input type="button" onclick="javascript: document.getElementById('light2').style.display='none';document.getElementById('fade').style.display='none';" value="No"/>
        </div>
    </div>
    <div class="light-footer"></div>
</div>
<!-- POP UP FOR MESSAGES -->

 <!-- POP UP FOR CONFIRMATION MESSAGES -->
<div id="light3" class="white_content">
    <div id="title3" class="light-title"></div>
    <div id="msg3" class="light-message"></div>
    <div id="button3" class="light-button">
        <?php echo $btnOkay;?>
        <input type="button" onclick="javascript: document.getElementById('light3').style.display='none';document.getElementById('fade').style.display='none';return false;" value="Cancel"/>
    </div>
    <div class="light-footer"></div>
</div>
<!-- POP UP FOR CONFIRMATION MESSAGES -->
    <?php echo $hiddenfilename;?>
    <?php echo $hidaid;?>
    <?php echo $hiddenflag;?>
    <?php echo $xmltype;?>
    <div class="titleCont">
    <div class="titleCont_left"></div>
    <div class="titleCont_body">Batch File Validation</div>
    <div class="titleCont_right"></div>
</div>
<div class="content-page">
    <table width="100%">
        <tr>
            <td width="70%" align="left">
                <?php echo $ddlcardvalue; ?>
                <?php echo $ddlUsername; ?>
                
                <?php echo $ddlgamename; ?>
                <?php echo $ddlgametype; ?>
            </td>
            <td align="right">
                <?php echo $txtSearchByID; ?>
            </td>
        </tr>
        <tr>
            <td>
                File Name: <input type="file" name="file" id="file" size="49"/>
            </td>
            <td align="right">
                <div class="form-button">
                    <?php echo $btnSearchByID; ?>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="form-button" style="margin-right: 18%;">
                    <?php echo $btnUpload;  ?>
                </div>
            </td>
       </tr>
     </table>
    <?php
        if ($bfvid != '')
        {
        ?>
            <div class="form-page">
                    <?php echo $btnExportCSV;?>
            </div>
        <table class="table-list" style="clear: both;">
       
             
        <?php ($ctr % 2) == 0 ? $class = "evenrow" : $class = "oddrow" ; ?>
        <tr>
            <th colspan ="2" style="width:625px;">Validation Summary</th>
        </tr>
        <tr class = "evenrow">            
            <th align="left" style="width:315px;">No. of Valid Tickets</th>
            <td align="center"><?php echo number_format($countValid); ?></td>
        </tr>
         <tr class = "oddrow">            
            <th align="left" style="width:310px;">No. of Invalid Tickets</th>
            <td align="center"><?php echo number_format($countInvalid); echo $hiddenId;?></td>
        </tr>
        <tr class = "evenrow">            
            <th align="left" style="width:310px;">No. of Claimed Tickets</th>
            <td align="center"><?php echo number_format($total_claimed);?></td>
        </tr>
        <tr class = "oddrow">            
            <th align="left" style="width:310px;">No. of Void Tickets</th>
            <td align="center"><?php echo number_format($total_cancelled);?></td>
        </tr>
        <tr class = "evenrow">            
            <th align="left" style="width:310px;">No. of Reimbursed Tickets</th>
            <td align="center"><?php echo number_format($total_reimbursed);?></td>
        </tr>
        <tr class = "oddrow">            
            <th align="left" style="width:310px;">No. of Inactive Tickets</th>
            <td align="center"><?php echo number_format($total_inactive);?></td>
        </tr>
        <tr class = "evenrow">            
            <th align="left" style="width:310px;">No. of Stolen Tickets (Payable)</th>
            <td align="center"><?php echo number_format($total_stolen_pay);?></td>
        </tr>
        <tr class = "oddrow">            
            <th align="left" style="width:310px;">No. of Stolen Tickets (Non-Payable)</th>
            <td align="center"><?php echo number_format($total_stolen_nonpay);?></td>
        </tr>
        <tr class = "evenrow">            
            <th align="left" style="width:310px;">No. of Expired Tickets (Payable)</th>
            <td align="center"><?php echo number_format($total_expired_pay);?></td>
        </tr>
        <tr class = "oddrow">            
            <th align="left" style="width:310px;">No. of Expired Tickets (Non-Payable)</th>
            <td align="center"><?php echo number_format($total_expired_nonpay);?></td>
        </tr>
        <tr class = "evenrow">            
            <th align="left" style="width:310px;">Total Redeemable Prize</th>
            <td align="center"><?php echo $currency . " " . number_format($total_prize_redeem); ?></td>
        </tr>
        <?php if ($validate == 1) { ?>
        <tr>
            <td colspan ="2" align ="right"> <?php echo $btnClaim;  ?></td>
         </tr>
         <?php
         }
         else
         {
             // Claim button must not be visible.
         }
         ?>
    </table>
    <br/>
        <?php
        }
        ?>
        <?php
        if ($bfvid != '')
        {
        ?>
        <table class="table-list" style="clear: both;">
        <tr>
            <th style="width:200px;">Card Value</th>
            <th style="width:200px;">Game Number</th>
            <th style="width:200px;">Game Name</th>
            <th style="width:200px;">Retailer Username</th>
            <th style="width:200px;">Book Ticket Number</th>
            <th style="width:200px;">Validation Number</th>
            <th style="width:200px;">Status</th>
            <th style="width:200px;">Prize Amount</th>
        </tr>
        <?php if(count($arrSessionData) > 0):?>
        <?php for($ctr = 0 ; $ctr < count($arrSessionData) ; $ctr++):?>
        <?php ($ctr % 2) == 0 ? $class = "evenrow" : $class = "oddrow" ; ?>
        <tr class = "<?php echo $class?>">
            <td align="center"><?php echo $arrSessionData[$ctr]['CardPrice']?></td>
            <td align="center"><?php echo $arrSessionData[$ctr]['GameNumber']?></td>
            <td align="center"><?php echo $arrSessionData[$ctr]['GameName']?></td>
            <td align="center"><?php echo $arrSessionData[$ctr]['Distributor']?></td>
            <td align="center"><?php echo $arrSessionData[$ctr]['TicketNo']?></td>
            <td align="center"><?php echo $arrSessionData[$ctr]['ValidationNo']?></td>
            <td align="center"><?php echo $arrSessionData[$ctr]['StatusString']?></td>
            <td align="center"><?php echo $arrSessionData[$ctr]['PrizeName']?></td>
        </tr>
        <?php endfor;?>
        <?php else:?>
        <tr class="no-record"><td colspan="9">No records to display</td></tr>
        <?php endif;?>
    </table>
            <?php
        }
        ?>
</div>   
<?php 
if (isset($errormsg))
{
?>
<script>
    document.getElementById('title').innerHTML = "<?php echo $errormsgtitle; ?>";
    document.getElementById('msg').innerHTML = '<?php echo $errormsg; ?>';
    document.getElementById('light').style.display = 'block';
    document.getElementById('fade').style.display = 'block';
</script> 
<?php
}
?>
<?php if(isset($uploadmsg)):?>
<script>
    document.getElementById('title').innerHTML = "<?php echo $uploadmsgtitle; ?>";
    document.getElementById('msg').innerHTML = "<?php echo $uploadmsg; ?>";
    document.getElementById('light').style.display = 'block';
    document.getElementById('fade').style.display = 'block';
</script>
<?php endif;?>

<script type="text/javascript">
$(document).ready(function(){
    $('#txtSearchByID').keypress(function(event){
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if(keycode == '13'){
            $('#btnSearchByID').click();	
        }
        event.stopPropagation();
    });
});
</script>

</form>
<?php include("footer.php"); ?>