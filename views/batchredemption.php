<?php
/*
 * Created by Arlene R. Salazar on 01-24-2012
 * Purpose: view for batch redemption
 */
require_once ("init.inc.php");
$pagesubmenuid = 41;
include_once("../controller/managesession.php");
include("../controller/batchredemptionprocess.php");
?>
<?php include("header.php"); ?>
<div id="fade" class="black_overlay"></div>
<script language="javascript" type="text/javascript">
    function ChangePage(pagenum)
    {
        selectedindex = document.getElementById("pgSelectedPage");
        selectedindex.value = pagenum;
        document.forms[0].submit();
    }

    function get_gamename()
    {
        var gamenum = document.getElementById('ddlGameNum').options[document.getElementById('ddlGameNum').selectedIndex].value;
        if(gamenum == 0)
        {
            document.getElementById("ddlGameName").onchange = get_gamenumber;
            document.getElementById("hiddenflag").value = 2;
        }
        else
        {
            document.getElementById("ddlGameName").onchange = null;
        }

        $("#ddlGameName").load(
            "../controller/get_gamename.php",
            {
                gamenum: gamenum
            },

            function(status, xhr)
            {
                if(status == "error")
                {
                    document.getElementById('title5').innerHTML = "ERROR!";
                    document.getElementById('msg5').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
                    document.getElementById('light5').style.display = "block";
                    document.getElementById('fade').style.display = "block";
                }
            }
        );

    }

    function get_gamenumber()
    {
        var prodid = document.getElementById('ddlGameName').options[document.getElementById('ddlGameName').selectedIndex].value;
        document.getElementById('hiddenflag').value = "1";

        if (prodid == 0)
        {
            document.getElementById("ddlGameNum").onchange = function(){
               get_gamename();
               get_gamebatches()
           };
        }
        else
        {
            document.getElementById("ddlGameNum").onchange = get_gamebatches;
        }
        $("#ddlGameNum").load(
            "../controller/get_gamenum.php",
            {
                prodid: prodid
            },
            function(status, xhr)
            {
                if(status == "error")
                {
                    document.getElementById('title5').innerHTML = "ERROR!";
                    document.getElementById('msg5').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
                    document.getElementById('light5').style.display = "block";
                    document.getElementById('fade').style.display = "block";
                }
            }
        );
    }

    function get_gamebatches()
    {
        var gameid = document.getElementById('ddlGameNum').options[document.getElementById('ddlGameNum').selectedIndex].value;
        var prodid = document.getElementById('ddlGameName').options[document.getElementById('ddlGameName').selectedIndex].value;

        if(prodid == 0)
        {
            document.getElementById('hiddenflag').value = "2";
        }
        $("#ddlBatchID").load(
            "../controller/get_gamebatch.php",
            {
                gamenum: gameid,
                xmltype: 2,
                prodid: 0,
                flag: 0
            },
            function(status, xhr)
            {
                if(status == "error")
                {
                    document.getElementById('title5').innerHTML = "ERROR!";
                    document.getElementById('msg5').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
                    document.getElementById('light5').style.display = "block";
                    document.getElementById('fade').style.display = "block";
                }
            }
        );
    }

    function viewBatchRedemptionDtls(commid,gameid)
    {
        document.getElementById('hiddenbatchvalidationid').value = commid;
        document.getElementById('hiddengameid').value = gameid;
        document.batchredemptionform.action = "redemptioncommdetailed.php";
		document.batchredemptionform.submit();
    }

    function ClaimBatchRedemptionDtls(commid,gameid)
    {
        document.getElementById('hiddenbatchvalidationid').value = commid;
        document.getElementById('hiddengameid').value = gameid;
        document.batchredemptionform.submit();
    }

    function redirectToBatchRedemption()
    {
        window.location = "batchredemption.php";
    }
</script>

<!-- POP UP FOR MESSAGES -->
<div id="light" class="white_content">
    <div id="title" class="light-title"></div>
    <div id="msg" class="light-message"></div>
    <div id="button" class="light-button">
        <input type="button" onclick="javascript: document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none';" value="Okay"/>
    </div>
    <div class="light-footer"></div>
</div>
<!-- POP UP FOR MESSAGES -->
<div class="titleCont">
    <div class="titleCont_left"></div>
    <div class="titleCont_body">Batch Redemption</div>
    <div class="titleCont_right"></div>
</div>
<div class="content-page">
    <form action="batchredemption.php" method="post" name="batchredemptionform">
		<?php echo $hiddenflag;?>
        <table>
            <tr>
                <td width="250">Username: &nbsp;&nbsp;&nbsp;<?php echo $ddlUsername;?></td>
                <td width="300">Game Name: &nbsp;&nbsp;&nbsp;<?php echo $ddlGameName;?></td>
                <td width="200">Game Number: &nbsp;&nbsp;&nbsp;<?php echo $ddlGameNum;?></td>
                <td width="250">Game Batch: &nbsp;&nbsp;&nbsp;<?php echo $ddlBatchID;?></td>
                <td><?php echo $btnSubmit;?></td>
            </tr>
        </table>
    
    <?php if (isset($batchredemptionlist)):?>
        <div class="form-page">
            <?php echo $pgTransactionHistory;?><br/>
            <?php echo $btnExport;?>
        </div>

        <?php echo $hiddengameid;?>
        <?php echo $hiddenprodid;?>
        <?php echo $hiddenuserid;?>
        <?php echo $hiddenbatchvalidationid;?>
		<?php echo $hiddenprocessedtag;?>
        <?php echo $hiddenbatchid;?>
        <table class="table-list">
            <tr>
				<th>Game Batch</th>
                <th>Username</th>
                <th>Date & Time</th>
                <th>Total Redemption Commission</th>
                <th>Option</th>
            </tr>

        <?php if(count($batchredemptionlist) > 0):?>
            <?php for($i = 0 ; $i < count($batchredemptionlist) ; $i++):?>
            <?php ($i % 2) == 0 ? $class = "evenrow" : $class = "oddrow" ; ?>
            <tr class="<?php echo $class;?>">
				<td><?php echo $batchredemptionlist[$i]["BatchID"];?></td>
                <td><?php echo $batchredemptionlist[$i]["Username"];?></td>
                <td><?php echo $batchredemptionlist[$i]["DateValidated"];?></td>
                <td style="text-align: right;"><?php echo "$" . number_format(($batchredemptionlist[$i]["Total"] * ($incentive / 100)) , 2 , "." , ",");?></td>
                <td>
                    <input type="button" value="View Details" onclick="viewBatchRedemptionDtls(<?php echo $batchredemptionlist[$i]["BatchValidationSessionID"];?>,<?php echo $batchredemptionlist[$i]["GameID"]?>)"/>
                    <input type="submit" id="btnClaimTickets" name="btnClaimTickets" value="Claim Tickets" onclick="ClaimBatchRedemptionDtls(<?php echo $batchredemptionlist[$i]["BatchValidationSessionID"];?>,<?php echo $batchredemptionlist[$i]["GameID"]?>)"/>
                    <?php //echo $btnClaim;?>
                </td>
            </tr>
            <?php endfor;?>
        <?php else:?>
            <tr class="no-record">
                <td colspan="5">No Record Found</td>
            </tr>
        <?php endif;?>
        </table>

        <div class="form-page">
            <?php echo $pgTransactionHistory;?> 
        </div>
    <?php endif;?>
    <!-- POP UP FOR MESSAGES -->
    <div id="light2" class="white_content">
        <div id="title2" class="light-title">CONFIRMATION</div>
        <div id="msg2" class="light-message">
            Are you sure you want to claim this tickets?
        </div>
        <div id="button2" class="light-button">
            <?php echo $btnConfirmClaimTickets;?>
            <input type="button" onclick="javascript: document.getElementById('light2').style.display='none';document.getElementById('fade').style.display='none';" value="Cancel"/>
        </div>
        <div class="light-footer"></div>
    </div>
    <!-- POP UP FOR MESSAGES -->
    <?php if(isset($claimtickets)):?>
        <script>
            document.getElementById('light2').style.display = 'block';
            document.getElementById('fade').style.display = 'block';
        </script>
    <?php endif;?>
    <?php if(isset($errormsg)):?>
        <script>
            document.getElementById('title').innerHTML = "<?php echo $errortitle;?>";
            document.getElementById('msg').innerHTML = "<?php echo $errormsg;?>";
            document.getElementById('light').style.display = 'block';
            document.getElementById('fade').style.display = 'block';
        </script>
    <?php endif;?>
    <?php if(isset($successmsg)):?>
        <script>
            document.getElementById('title').innerHTML = "<?php echo $successtitle;?>";
            document.getElementById('msg').innerHTML = "<?php echo $successmsg;?>";
            document.getElementById('button').innerHTML = "<input type=\"button\" onclick=\"javascript: return redirectToBatchRedemption();\" value=\"Okay\"/>";
            document.getElementById('light').style.display = 'block';
            document.getElementById('fade').style.display = 'block';
        </script>
    <?php endif;?>
    </form>
</div>
<?php include("footer.php"); ?>