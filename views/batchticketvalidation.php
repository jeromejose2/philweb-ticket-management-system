<?php
/*
 * @author
 * Purpose   : view for batchticketvalidation
 */
include("init.inc.php");
$pagesubmenuid = 38;
include_once("../controller/managesession.php");
include("../controller/batchticketvalidationprocess.php");
?>
<script language="javascript" src="jscripts/validations.js"></script>
<script language="javascript" src="jscripts/jquery-1.5.2.min.js"></script>
<script language="javascript" type="text/javascript">
    $('#txtBookNumber').live('keypress',function(e){
        if(e.keyCode == 13 || e.keyCode == 9) {
            $('#txtValidationNumber').focus();
            return false;
        }
    });

    $('#txtValidationNumber').live('keypress',function(e){
        if(e.keyCode == 13 || e.keyCode == 9) {
            $('#btnAddToBatch').focus();
            $('#btnAddToBatch').click();
            return false;
        }
    });

    function checkKeycode(e) 
    {
        var keycode;
        if (window.event) keycode = window.event.keyCode;
        else if (e) keycode = e.which;

        if (keycode == 13 || keycode > 31 && (keycode < 48 || keycode > 57))
            return false;
        return true;
    }

    function setIndex()
    {
        document.getElementById("txtValidationNumber").tabIndex = 1;
    }
    
    $(document).ready(function(){
    
        $('#btnCancelSession').live('click', function()
        {
            $('#flag').val(0);
            $("#ddlcardvalue option[value='0']").attr("selected", "selected");
            $("#ddlUsername option[value='0']").attr("selected", "selected");
            $("#ddlgamename").attr('disabled',true);
            $("#ddlgamenum").attr('disabled',true);
        });
        
        $('#btnValidate').live('click', function()
        {
            $('#flag').val(0);
            $("#ddlcardvalue option[value='0']").attr("selected", "selected");
            $("#ddlUsername option[value='0']").attr("selected", "selected");
            $("#ddlgamename option[value='0']").attr("selected", "selected");
            $("#ddlgamenum option[value='0']").attr("selected", "selected");
            $("#ddlgamename").attr('disabled',true);
            $("#ddlgamenum").attr('disabled',true);
        });
        
        if ($('#flag').val() == 1)
        {
            $("#ddlgamename").attr('disabled',false);
            $("#ddlgamenum").attr('disabled',false);
        }
        else
        {
            $("#ddlgamename").attr('disabled',true);
            $("#ddlgamenum").attr('disabled',true);
        }
        
    });
    
    function get_gamename()
    {
        $("#ddlgamename").attr('disabled',false);
        var cardvalue = document.getElementById('ddlcardvalue').options[document.getElementById('ddlcardvalue').selectedIndex].value;
        var gamename = document.getElementById('ddlgamename').options[document.getElementById('ddlgamename').selectedIndex].value;
        var flag = 0;
        
        if(cardvalue == 0)
        {
            $("#ddlgamename").attr('disabled',true);
            $("#ddlgamenum").attr('disabled',true);
        }
        
        $("#ddlgamename").load(
            "../controller/get_gamename.php",
            {
                cardvalue: cardvalue,
                gamename: gamename,
                flag: flag
            },

            function(status, xhr)
            {
                if(status == "error")
                {
                    document.getElementById('title').innerHTML = "ERROR!";
                    document.getElementById('msg').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
                    document.getElementById('light').style.display = "block";
                    document.getElementById('fade').style.display = "block";
                }
            }
        );
    }

    function get_gamenumber()
    {
        $("#ddlgamenum").attr('disabled',false);
        document.getElementById("flag").value = 1;
        var prodid = document.getElementById('ddlgamename').options[document.getElementById('ddlgamename').selectedIndex].value;
        var cardvalue = document.getElementById('ddlcardvalue').options[document.getElementById('ddlcardvalue').selectedIndex].value;
        var prodid1 = prodid;
        
        if(prodid == 0)
        {
            $("#ddlgamenum").attr('disabled',true);
            $("#ddlgamebatch").attr('disabled',true);
        }
        
        if ( (prodid == 0 || cardvalue == 0) && (prodid != 0 && cardvalue == 0))
        { 
            prodid1 = 0;
        }
        
        $("#ddlgamenum").load(
            "../controller/get_gamenum.php",
            {
                prodid: prodid1
            },

            function(status, xhr)
            {
                if(status == "error")
                {
                    document.getElementById('title').innerHTML = "ERROR!";
                    document.getElementById('msg').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
                    document.getElementById('light').style.display = "block";
                    document.getElementById('fade').style.display = "block";
                }
            }
        );
    }
</script>

<?php include("header.php"); ?>
<script>
    $(document).ready(function(){
        $('#txtBookNumber').bind("cut copy paste",function(e) {
          e.preventDefault();
        });
        $('#txtValidationNumber').bind("cut copy paste",function(e) {
          e.preventDefault();
        });
    }); 
</script>
<form id="frmValidation" name="frmValidation" method="post" onload="javascript: return setIndex();">
        <div id="fade" class="black_overlay"></div>
        <div id="loading" class="loading"></div>
	<!-- POP UP FOR MESSAGES -->
        <div id="light" class="white_content">
        <div id="title" class="light-title"></div>
        <div id="msg" class="light-message"></div>
        <div id="button" class="light-button"><input id="btnOk" name="btnOk" type="button" onclick="javascript: document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none';document.getElementById('txtBookNumber').focus();" value="OKAY"/></div>
	<div class="light-footer"></div>
        </div>    
        <div id="light2" class="white_content">
        <div id="title2" class="light-title"></div>
        <div id="msg2" class="light-message"></div>
        <div id="button" class="light-button">
            <?php echo $btnValidate; ?>&nbsp;
            <input type="button" id="btnCancel" name="btnCancel" value="NO" onclick="javascript: document.getElementById('light2').style.display = 'none';document.getElementById('fade').style.display = 'none';"/>
        </div>
        <div class="light-footer"></div>
	</div>  
        <div id="light3" class="white_content">
        <div id="title3" class="light-title"></div>
        <div id="msg3" class="light-message"></div>
        <div id="button" class="light-button">
            <?php echo $btnOkay; ?>&nbsp;
        </div>
        <div class="light-footer"></div>
	</div> 
        <!-- POP UP FOR MESSAGES -->
                    <div class="titleCont">
                        <div class="titleCont_left"></div>
                        <div class="titleCont_body">Retailer Ticket Validation</div>
                        <div class="titleCont_right"></div>
                    </div>
                    <div class="content-page">
                        <?php echo $flag; ?>
                        <table style="margin-left: 0.5%; width: 100%">
                                <tr>
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                                <?php
                                if ($accttypeid < 4) 
                                {    
                                    ?>
                                <tr>
                                    <td><?php echo $ddlcardvalue; echo $lblcardvalue;?></td>
                                    <td><?php echo $ddlgamename; echo $lblprodid; ?></td>
                                    <td><?php echo $ddlgamenum; echo $lblgameid;?></td>
                                    <td><?php echo $ddlUsername; echo $lbluserid;?></td>
                                    <td><?php echo $btnCreateSession; ?></td>
                                    <td><?php echo $btnCancelSession; ?></td>
                                </tr>
                                <?php
                                }
                                ?>
                        </table>
                        <div id="batchdiv">
                        <table  style="margin-left: 0.5%;">
                                <tr>
                                    <td class="label2">Ticket Number</td>
                                    <td><?php echo $txtBookNumber; ?></td>
                                </tr>
                                <tr>
                                    <td class="label2">Validation Number</td>
                                    <td><?php echo $txtValidationNumber; ?><div id="divIsValid" name="divIsValid"></div><input id ="hidIsValid" name="hidIsValid" type="hidden" value=""></input>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan ="2" style="text-align: right"><?php echo $btnAddToBatch; ?><?php echo $hidProdID; ?></td>
                                </tr>
                        </table>
                        <table class="table-list" style="text-align: center">
                            <thead>
                                <tr align="center">
                                    <th>Account Name</th>
                                    <th>Ticket Number</th>
                                    <th>Validation Number</th>
                                    <th>Prize Name</th>
                                </tr>
                            </thead>
                            <tbody id="tbodytickets">
                            <?php for($ctr=0; $ctr < count($valTickets_list); $ctr++){ ?>
                            <?php ($ctr % 2) == 0 ? $class = "evenrow" : $class = "oddrow" ; ?>
                            <tr class = "<?php echo $class?>">
                                <td><?php echo $valTickets_list[$ctr]['UserName'];?></td>
                                <td><?php echo $valTickets_list[$ctr]['TicketNumber'];?></td>
                                <td><?php echo $valTickets_list[$ctr]['ValidationNumber'];?></td>
                                <td><?php echo $valTickets_list[$ctr]['PrizeName'];?></td>
                            </tr>
                            <?php } ?>
                            </tbody>
                            <?php if (count($valTickets_list) == 0) {?>
                                <tr id="no-record" class="no-record"><td colspan="8">No records to display</td></tr>
                            <?php } else {?>
                            <tr><td id="tdvalidate" colspan="4" style="text-align: center"><?php echo $btnFinalize; ?></td></tr>
                            <?php } ?>
                        </table>
                    </div>
                    </div>
                            <?php if ($confirmation == 'true' && $okay == 'false') : ?>
                            <script>
                                document.getElementById('title2').innerHTML = "<?php echo $errormsgtitle; ?>";
                                document.getElementById('msg2').innerHTML = "<?php echo $errormsg; ?>";
                                document.getElementById('light2').style.display = 'block';
                                document.getElementById('fade').style.display = 'block';
                            </script>  
                            <?php endif; ?>
                            <?php if ($confirmation == 'true' && $okay == 'true') : ?>
                            <script>
                                document.getElementById('title3').innerHTML = "<?php echo $errormsgtitle; ?>";
                                document.getElementById('msg3').innerHTML = "<?php echo $errormsg; ?>";
                                document.getElementById('light3').style.display = 'block';
                                document.getElementById('fade').style.display = 'block';
                            </script>  
                            <?php endif;?>
                            <?php if ((isset($errormsg) && $status > 1) || (isset($successful))) : ?>
                            <script>
                                document.getElementById('title').innerHTML = "<?php echo $errormsgtitle; ?>";
                                document.getElementById('msg').innerHTML = "<?php echo $errormsg;?>";
                                document.getElementById('light').style.display = 'block';
                                document.getElementById('fade').style.display = 'block';
                            </script>  
                            <?php endif; ?>     
                            
                            <?php if (isset($errormsg) && (($status == 0) || ($status == 1)) && ($confirmation == 'false')) : ?>
                            <script>
                                document.getElementById('title').innerHTML = "<?php echo $errormsgtitle; ?>";
                                document.getElementById('msg').innerHTML = "<?php echo $errormsg;?>";
                                document.getElementById('light').style.display = 'block';
                                document.getElementById('fade').style.display = 'block';
                            </script>  
                            <?php endif; ?>
</form>
<?php include("footer.php"); ?>