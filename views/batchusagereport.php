<?php
/*
 * Batch Usage Report
 * 
 * View page of the module.
 * 
 * @author: Noel Antonio
 * 
 * lastupdatedby: Noel Antonio
 * dateupdated: feb. 6, 2013
 * purpose: fix prize counter outputs. it must be the same with the prize history.
 * change the xmltype pointing to winners.
 * 
 */

include("init.inc.php");
$pagesubmenuid = 42;
include_once("../controller/managesession.php");
include("../controller/batchusagereportprocess.php");
?>
<?php include("header.php"); ?>
<script language="javascript" type="text/javascript">
	function get_gamenumber()
            {
                document.getElementById("ddlgametype").onchange = get_batchID;
                document.getElementById("flag").value = 1;
                document.getElementById("xmltype").value = 2;
                document.getElementById("ddlgamebatch").innerHTML = "<option value ='0'>ALL</option>";
                var prodid = document.getElementById('ddlgamename').options[document.getElementById('ddlgamename').selectedIndex].value;
                if (prodid == 0)
                {
                   document.getElementById("ddlgametype").onchange= function(){get_gamename();get_batchID()};     
                }
                $("#ddlgametype").load(
                    "../controller/get_gamenumwithdefall.php",
                    {
                        prodid: prodid
                    },
                    function(status, xhr)
                    {
                        if(status == "error")
                        {
                            document.getElementById('title5').innerHTML = "ERROR!";
                            document.getElementById('msg5').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
                            document.getElementById('light5').style.display = "block";
                            document.getElementById('fade').style.display = "block";
                        }
                    }
                );
            }
            function get_batchID()
            {
                document.getElementById("xmltype").value = 2;
                var gamenum = document.getElementById('ddlgametype').options[document.getElementById('ddlgametype').selectedIndex].value; 
                var prodid = document.getElementById('ddlgamename').options[document.getElementById('ddlgamename').selectedIndex].value;
                var xmltype = document.getElementById('xmltype').value;    
                $("#ddlgamebatch").load(
                    "../controller/get_batchidwithdefall.php",
                    {
                        gamenum: gamenum,
                        prodid: prodid,
                        xmltype:xmltype            
                    },

                    function(status, xhr)
                    {
                        if(status == "error")
                        {
                            document.getElementById('title').innerHTML = "ERROR!";
                            document.getElementById('msg').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
                            document.getElementById('light').style.display = "block";
                            document.getElementById('fade').style.display = "block";
                        }
                    }
                );
            }
            function get_gamename()
            {
               document.getElementById("ddlgamename").onchange = null;
               document.getElementById("xmltype").value = 2;  
               document.getElementById("flag").value = 2;
               var gamenum = document.getElementById('ddlgametype').options[document.getElementById('ddlgametype').selectedIndex].value;
               if (gamenum ==0)
                   {
                      document.getElementById("ddlgamename").onchange = get_gamenumber; 
                   }
                $("#ddlgamename").load(
                    "../controller/get_gamenamewithdefall.php",
                    {
                        gamenum: gamenum
                    },

                    function(status, xhr)
                    {
                        if(status == "error")
                        {
                            document.getElementById('title').innerHTML = "ERROR!";
                            document.getElementById('msg').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
                            document.getElementById('light').style.display = "block";
                            document.getElementById('fade').style.display = "block";
                        }
                    }
                );
            }
</script>
<form id="frmBatchUsage" name="frmBatchUsage" method="post">
    <?php echo $hiddenflag;?>
    <?php echo $xmltype;?>
    <?php echo $flag;?>
    <div id="fade" class="black_overlay"></div>
    <div id="loading" class="loading"></div>
    <!-- POP UP FOR MESSAGES -->
    <div id="light" class="white_content">
    <div id="title" class="light-title"></div>
    <div id="msg" class="light-message"></div>
    <div id="button" class="light-button"><input id="btnOk" name="btnOk" type="button" onclick="javascript: document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none';" value="OKAY"/></div>
    <div class="light-footer"></div>
    </div>    
    <!-- POP UP FOR MESSAGES -->
    <div class="titleCont">
        <div class="titleCont_left"></div>
        <div class="titleCont_body">Scratch Card Batch Usage</div>
        <div class="titleCont_right"></div>
    </div>
    <div class="content-page">
        <table width ="100%" style="margin-left: 0.5%;">
            <tr>
                <td><?php echo $ddlgamename;?></td>
                <td><?php echo $ddlgametype;?></td>
                <td><?php echo $ddlgamebatch;?></td>
                <td><?php echo $btnSubmit;?></td> 
            </tr>
        </table><br/>
		<?php if (isset($di)):?>
			<div class="form-page">
				<?php echo $btnExportCSV;?>
			</div>
		<?php endif;?>

        <table class="table-list">
            <tr><th>Date Imported :</th>
            <?php if (!isset($di)):?><td>No records to display</td><?php endif;?>
            <td><?php echo $di[0]["DateImported"];?></td>
            </tr></table><br/>
        
        <!-- BATCH CONSUMPTION TABLE-->
        <table class="table-list" style="text-align: center">
            <th colspan="6">BATCH CONSUMPTION</th>
            <tr align="center">
                <th>Batch Size</th>
                <th>Total Winning Entries</th>
                <th>Total Entries Used</th>
                <th>Total Cancelled Entries</th>
                <th>Balance Entries</th>
                <th>Batch Consumption Percentage</th>
            </tr>
            <?php if (!isset($batchsize)):?>
                <tr class="no-record"><td colspan="6">No records to display</td></tr>
            <?php endif;?>
            <?php for($ctr=0; $ctr < count($batchsize); $ctr++){ ?>
            <?php ($ctr % 2) == 0 ? $class = "evenrow" : $class = "oddrow" ; ?>
            <tr class = "<?php echo $class?>">
                <td><?php echo number_format($batchsize[$ctr]['DeckSize']) ;?></td>
                <td><?php echo number_format($twe[$ctr]['WinningsCount']);?></td>
                <td><?php echo number_format($teu[$ctr]['EntriesUsed']);?></td>
                <td><?php echo number_format($tce[$ctr]['CancelledTicketCount']);?></td>
                <?php 
                    $be = ($batchsize[$ctr]['DeckSize'] - ($teu[$ctr]['EntriesUsed'] + $tce[$ctr]['CancelledTicketCount']));
                    $bcp = round((($teu[$ctr]['EntriesUsed'] / $batchsize[$ctr]['DeckSize']) * 100), 4);
                ?>
                <td><?php echo $be;?></td>
                <td><?php echo $bcp . "%";?></td>
            </tr>
            <?php } ?>
        </table><br/>
        
        <!-- PRIZE AMOUNT CONSUMPTION TABLE-->
        <table class="table-list" style="text-align: center">
            <th colspan="4">PRIZE AMOUNT CONSUMPTION</th>
            <tr align="center">
                <th>Total Prize</th>
                <th>Prize Used</th>
                <th>Prize Left</th>
                <th>Prize Consumption Percentage</th>
            </tr>
            <?php if (!isset($tp)):?>
                <tr class="no-record"><td colspan="4">No records to display</td></tr>
            <?php endif;?>
            <?php for($ctr=0; $ctr < count($tp); $ctr++){ ?>
            <?php ($ctr % 2) == 0 ? $class = "evenrow" : $class = "oddrow" ; ?>
            <tr class = "<?php echo $class?>">
                <td><?php echo $currency.' '.number_format($tp[$ctr]['TotalPrize']);?></td>
                <td><?php 
                        if ($pu[$ctr]['TotalPrizeUsed']!='' || $pu[$ctr]['TotalPrizeUsed']!=NULL){
                                echo $currency.' '.number_format($pu[$ctr]['TotalPrizeUsed']);
                            } else {
                                $pu[$ctr]['TotalPrizeUsed'] = 0;
                                echo $currency.' '.number_format($pu[$ctr]['TotalPrizeUsed']);
                            }
                ?></td>
                <?php 
                    $pl = ($tp[$ctr]['TotalPrize'] - $pu[$ctr]['TotalPrizeUsed']);
                    $pcp = round((($pu[$ctr]['TotalPrizeUsed'] / $tp[$ctr]['TotalPrize']) * 100), 4);
                ?>
                <td><?php echo $currency.' '.number_format($pl);?></td>
                <td><?php echo $pcp . "%";?></td>
            </tr>
            <?php } ?>
        </table><br/>
        
        <!-- PRIZE COUNTER TABLE-->
        <table class="table-list" style="text-align: center">
            <th colspan="5">PRIZE COUNTER</th>
            <tr align="center">
                <th>Prize Type</th>
                <th>Total Quantity of Prize</th>
                <th>Total Quantity Used</th>
                <th>Prize Balance Entries</th>
                <th>Consumption Percentage</th>
            </tr>
            <?php if (!isset($getprize_list)):?>
                <tr class="no-record"><td colspan="5">No records to display</td></tr>
            <?php endif;?>
                
            <?php
                // Added By Noel Antonio 02-06-2013
                $array = array();
                for($ctr=0; $ctr < count($getprize_list); $ctr++)
                {
                    for ($j = 0; $j < count($getprizeclaimed_list); $j++)
                    {
                        if (!isset($array[$getprizeclaimed_list[$j]["PrizeName"]]))
                        {
                            $array[$getprizeclaimed_list[$j]["PrizeName"]] = $getprizeclaimed_list[$j]['TQU'];
                        }
                    }
                    $array[$getprize_list[$ctr]["PrizeName"]] += 0;
                }
            ?>
                
            <?php for($ctr=0; $ctr < count($getprize_list); $ctr++){ ?>
            <?php ($ctr % 2) == 0 ? $class = "evenrow" : $class = "oddrow" ; ?>
            <tr class = "<?php echo $class?>">
                <td><?php echo $getprize_list[$ctr]['PrizeName'];?></td>
                <?php 
                    if ($getprize_list[$ctr]['TQP'] == ""){ $getprize_list[$ctr]['TQP'] = 0;}
                ?>
                <td><?php echo number_format($getprize_list[$ctr]['TQP']);?></td>
                <td><?php echo number_format($array[$getprize_list[$ctr]["PrizeName"]]);?></td>
                <?php
                    $pbe = $getprize_list[$ctr]['TQP'] - $array[$getprize_list[$ctr]["PrizeName"]];
                    $cp = round((($array[$getprize_list[$ctr]["PrizeName"]] / $getprize_list[$ctr]['TQP']) * 100), 4);
                ?>
                <td><?php echo number_format($pbe); ?></td>
                <td><?php echo $cp . "%";?></td>
            </tr>
            <?php } ?>
            
            
            <!-- Commented as of February 6, 2013 
                    <?php /* for($ctr=0; $ctr < count($getprize_list); $ctr++){ ?>
                    <?php ($ctr % 2) == 0 ? $class = "evenrow" : $class = "oddrow" ; ?>
                        <tr class = "<?php echo $class?>">
                            <td><?php echo $getprize_list[$ctr]['PrizeName'];?></td>
                    <?php 
                    if ($getprize_list[$ctr]['TQP'] == ""){ $getprize_list[$ctr]['TQP'] = 0;}
                    if ($getprizeclaimed_list[$ctr]['TQU'] == ""){ $getprizeclaimed_list[$ctr]['TQU'] = 0;}
                    ?>
                            <td><?php echo number_format($getprize_list[$ctr]['TQP']);?></td>
                            <td><?php echo number_format($getprizeclaimed_list[$ctr]['TQU']);?></td>
                    <?php   
                    $pbe = $getprize_list[$ctr]['TQP'] - $getprizeclaimed_list[$ctr]['TQU'];
                    $cp = round((($getprizeclaimed_list[$ctr]['TQU'] / $getprize_list[$ctr]['TQP']) * 100), 4);
                    ?>
                            <td><?php echo number_format($pbe);?></td>
                            <td><?php echo $cp . "%";?></td>
                        </tr>
                    <?php } */ ?>
            -->
        </table>
    </div>

</form>
<?php include("footer.php"); ?>