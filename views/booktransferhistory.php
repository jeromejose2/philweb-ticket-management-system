<?php
/*
 * Created By: Arlene R. Salazar on 10-06-2011
 * Purpose: For book transfer history
 */
require_once ("init.inc.php");
$pagesubmenuid = 36;
include_once("../controller/managesession.php");
include("../controller/booktransferhistoryprocess.php");
?>
<script language="javascript" type="text/javascript">
    function ChangePage(pagenum)
    {
        selectedindex = document.getElementById("pgSelectedPage");
        selectedindex.value = pagenum;
        document.forms[0].submit();
    }
    function get_gamenumber()
    {
        document.getElementById("ddlgametype").onchange = get_batchID;
        document.getElementById("flag").value = 1;
        document.getElementById("xmltype").value = 1;
        document.getElementById("ddlgamebatch").innerHTML = "<option value ='0'>ALL</option>";
        var prodid = document.getElementById('ddlgamename').options[document.getElementById('ddlgamename').selectedIndex].value;
         if (prodid == 0)
        {
           document.getElementById("ddlgametype").onchange= function(){get_gamename();get_batchID()};     
        }
        $("#ddlgametype").load(
            "../controller/get_gamenumwithdefall.php",
            {
                prodid: prodid
            },
            function(status, xhr)
            {
                if(status == "error")
                {
                    document.getElementById('title5').innerHTML = "ERROR!";
                    document.getElementById('msg5').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
                    document.getElementById('light5').style.display = "block";
                    document.getElementById('fade').style.display = "block";
                }
            }
        );
    }
    function get_batchID()
    {
        document.getElementById("xmltype").value = 1;
        var gamenum = document.getElementById('ddlgametype').options[document.getElementById('ddlgametype').selectedIndex].value; 
        var prodid = document.getElementById('ddlgamename').options[document.getElementById('ddlgamename').selectedIndex].value;
        var xmltype = document.getElementById('xmltype').value;    
        $("#ddlgamebatch").load(
            "../controller/get_batchidwithdefall.php",
            {
                gamenum: gamenum,
                prodid: prodid,
                xmltype:xmltype            
            },

            function(status, xhr)
            {
                if(status == "error")
                {
                    document.getElementById('title').innerHTML = "ERROR!";
                    document.getElementById('msg').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
                    document.getElementById('light').style.display = "block";
                    document.getElementById('fade').style.display = "block";
                }
            }
        );
    }
    function get_gamename()
    {
       document.getElementById("ddlgamename").onchange = null;
       document.getElementById("xmltype").value = 1;  
       document.getElementById("flag").value = 2;
       var gamenum = document.getElementById('ddlgametype').options[document.getElementById('ddlgametype').selectedIndex].value;
       if (gamenum ==0)
           {
              document.getElementById("ddlgamename").onchange = get_gamenumber; 
           }
        $("#ddlgamename").load(
            "../controller/get_gamenamewithdefall.php",
            {
                gamenum: gamenum
            },

            function(status, xhr)
            {
                if(status == "error")
                {
                    document.getElementById('title').innerHTML = "ERROR!";
                    document.getElementById('msg').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
                    document.getElementById('light').style.display = "block";
                    document.getElementById('fade').style.display = "block";
                }
            }
        );
    }
</script>
<?php include("header.php");?>
<div id="fade" class="black_overlay"></div>
<!-- POP UP FOR MESSAGES -->
<div id="light5" class="white_content">
    <div id="title5" class="light-title"></div>
    <div id="msg5" class="light-message"></div>
    <div id="button" class="light-button">
        <input type="button" onclick="javascript: document.getElementById('light5').style.display='none';document.getElementById('fade').style.display='none';" value="OKAY"/>
    </div>
	<div class="light-footer"></div>
</div>
<!-- POP UP FOR MESSAGES -->
<form name="frmbooktranshist" method="post">
    <?php echo $hiddenflag;?>
    <?php echo $xmltype;?>
    <?php echo $flag;?>
    <div class="titleCont">
        <div class="titleCont_left"></div>
        <div class="titleCont_body">Book Reassignment History</div>
        <div class="titleCont_right"></div>
    </div>
    <div class="content-page">
        <table width="100%">
            <tr>
                <td>From: </td>
                <td>
                    <?php echo $txtDateFr;?>
                    <img name="cal" src="images/close.gif" width="16" height="16" border="0" alt="Pick a date" onClick="displayDatePicker('txtDateFr', false, 'ymd', '-');"/>
                </td>
                <td>To: </td>
                <td>
                    <?php echo $txtDateTo;?>
                    <img name="cal" src="images/close.gif" width="16" height="16" border="0" alt="Pick a date" onClick="displayDatePicker('txtDateTo', false, 'ymd', '-');"/>
                </td>
                <td>Game Name: </td>
                <td><?php echo $ddlgamename;?></td>
                <td>Game Number: </td>
                <td><?php echo $ddlgametype;?></td>
                <!-- <td>Game Name: </td>
                <td><?php echo $txtprodtype;?></td> -->
				<td>Game Batch: </td>
                <td><?php echo $ddlgamebatch;?></td>
				<td><?php echo $btnSubmit;?></td>
            </tr>
        </table>
        <?php if(isset($booktranshistlist)):?>
        <div class="form-page"><br/><?php echo isset($pgBookTransHist) ? $pgBookTransHist : "";?></div>
        <div class="form-page">
			<?php //echo $btnExport;?>
		</div>
        <div class="form-page">
			<?php echo $btnExportCSV;?>
		</div>
        <br/><table class="table-list"> <!--Modified by MKGE | 04-25-13-->
            <tr>
                <th>Game Name</th>
                <th>Book Size</th>
                <th>Game Number</th>
		<th>Game Batch</th>    
                <th>Book Number</th>
                <th>Originally Assigned</th>
                <th>Old Invoice Number</th>
                <th>Transferred To</th>
                <th>New Invoice Number</th>
                <th>Transferred By</th>
                <th>Approved By</th>
                <th>Transferred Date and Time</th>
            </tr>
            <?php if(count($booktranshistlist)):?>
                <?php for($ctr = 0 ; $ctr < count($booktranshistlist) ; $ctr++):?>
                <?php ($ctr % 2) == 0 ? $class = "evenrow" : $class = "oddrow" ; ?>
                <tr class="<?php echo $class;?>">
                    <td><?php echo $booktranshistlist[$ctr]["ProductName"];?></td>
                    <td><?php echo $booktranshistlist[$ctr]["BookTicketCount"];?></td>
                    <td><?php echo $booktranshistlist[$ctr]["GameNumber"];?></td>
                    <td><?php echo $booktranshistlist[$ctr]["BatchID"];?></td>   
                    <td><?php echo $booktranshistlist[$ctr]["BookNumber"];?></td>
                    <td><?php echo $booktranshistlist[$ctr]["AssignedFrom"];?></td>
                    <td><?php echo $booktranshistlist[$ctr]["OldInvoiceNo"];?></td>
                    <td><?php echo $booktranshistlist[$ctr]["AssignedTo"];?></td>
                    <td><?php echo $booktranshistlist[$ctr]["CurrentInvoiceNo"];?></td>
                    <td><?php echo $booktranshistlist[$ctr]["TransferredBy"];?></td>
                    <td><?php echo $booktranshistlist[$ctr]["ApprovedBy"];?></td>
                    <?php
                    $dateTime = new DateTime($booktranshistlist[$ctr]["DateReassigned"]);
                    ?>
                    <td><?php echo $dateTime->format('Y-m-d\ h:i:s\ A');?></td>
                </tr>
                <?php endfor;?>
            <?php else: ?>
                <tr class="no-record">
                    <td colspan="10">No results to display</td>
                </tr>
            <?php endif;?>
        </table>
        
        <?php endif;?>
    </div>
</form>
<div class="content-page"><div class="form-page"><br/><?php echo isset($pgBookTransHist) ? $pgBookTransHist : "";?></div></div>
<?php include("footer.php");?>