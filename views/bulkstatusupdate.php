<?php
/*
 * Created by Arlene R. Salazar on 01-16-2011
 * Purpose: view for scratch card bulk update
 */
require_once("init.inc.php");
$pagesubmenuid = 43;
include_once("../controller/managesession.php");
include("../controller/bulkstatusupdateprocess.php");
?><script type="text/javascript" src="jscripts/jquery-1.5.2.min.js"></script>
<?php include("header.php"); ?>
<script language="javascript" type="text/javascript">
     $(document).ready(function(){
        $('input[type=text],[type=password]').bind('cut copy paste', function (e) {
            e.preventDefault();
        });
    });
    function ChangePage(pagenum)
    {
        selectedindex = document.getElementById("pgSelectedPage");
        selectedindex.value = pagenum;
        document.forms[0].submit();
    }

  

    function check_active()
    {
        if(document.getElementById('ddlStatus').value == 1)
        {
            document.getElementById('ddlAssignedTo').disabled = false;
        }
        else if(document.getElementById('ddlStatus').value == 4)
        {
            document.getElementById('ddlAssignedTo').disabled = false;
        }
        else
        {
            document.getElementById('ddlAssignedTo').disabled = true;
        }
    }

    function get_address()
    {
        var aid = document.getElementById('ddlNewAssignedTo').options[document.getElementById('ddlNewAssignedTo').selectedIndex].value;
//        if(aid == '0'){
//            document.getElementById('txtInvoiceNo').disabled = true;
//        }else{
//            document.getElementById('txtInvoiceNo').disabled = false;
//        }
        $.ajax
        ({
            url: '../controller/get_address.php?aid='+aid,
            type : 'post',
            success : function(data)
            {
                $('#txtRetailerAddress').val(data);             
                //alert(data);
            },
            error: function(e)
            {
                alert("Error");
            }
        });

    }

    function check_new_status()
    {
        var newstatus = document.getElementById('ddlNewStatus').options[document.getElementById('ddlNewStatus').selectedIndex].value;
        if(newstatus == 4)
        {
            document.getElementById('ddlNewAssignedTo').disabled = false;
            document.getElementById('date').style.visibility = "hidden";
        }
        else if(newstatus == 1)
        {
            document.getElementById('date').style.visibility = "visible";
            document.getElementById('txtInvoiceNo').disabled = false;
        }
        else
        {
            document.getElementById('date').style.visibility = "hidden";
            document.getElementById('txtInvoiceNo').disabled = true;
        }
    }

    function checkNewStatus()
    {
        var newstat = document.getElementById('ddlNewStatus').options[document.getElementById('ddlNewStatus').selectedIndex].value;
        var newassignedto = document.getElementById('ddlNewAssignedTo').options[document.getElementById('ddlNewAssignedTo').selectedIndex].value;
        var releasedate = document.getElementById('txtReleaseDate').value;
        
        if(newstat == 0)
        {
            document.getElementById('title').innerHTML = "ERROR!";
            document.getElementById('msg').innerHTML = "Please select the new status.";
            document.getElementById('light').style.display = "block";
            document.getElementById('fade').style.display = "block";
            return false;
        }
        if(newstat == 4)
        {
            if(newassignedto == 0)
            {
                document.getElementById('title').innerHTML = "ERROR!";
                document.getElementById('msg').innerHTML = "Please select an account from the Assigned To field.";
                document.getElementById('light').style.display = "block";
                document.getElementById('fade').style.display = "block";
                return false;
            }
        }
        if(newstat == 1)
        {
            var currentdate = document.getElementById('hidden_date').value;
            var releasedate = document.getElementById('txtReleaseDate').value;
            var arr1 = releasedate.split('-');
            var arr2 = currentdate.split('-');

            var dt1 = new Date();
            dt1.setFullYear(arr1[2], arr1[1], arr1[0]);
            var dt2 = new Date();
            dt2.setFullYear(arr2[2], arr2[1], arr2[0]);
            var sBankTransNewDate = new Date(arr1[2],arr1[0],arr1[1]);
            var sCurrentNewDate = new Date(arr2[2],arr2[0],arr2[1]);
            var sDateDiff = ((sCurrentNewDate - sBankTransNewDate) / 86400000); /* 86400000 is the answer of (60 * 60 * 24 * 1000)*/
            
            if(releasedate.replace(/^\s\s*/, '').replace(/\s\s*$/, '').length == 0)
            {
                document.getElementById('title').innerHTML = "ERROR!";
                document.getElementById('msg').innerHTML = "Invalid release date.";
                document.getElementById('light').style.display = "block";
                document.getElementById('fade').style.display = "block";
                return false;
            }
//            if(sDateDiff > 0)
//            {
//                document.getElementById('title').innerHTML = "ERROR!";
//                document.getElementById('msg').innerHTML = "Please select a release date not later than the current date.";
//                document.getElementById('light').style.display = "block";
//                document.getElementById('fade').style.display = "block";
//                return false;
//            }
        }
        return true;
    }

    function CloseLightBox2()
    {
        document.getElementById('light2').style.display = "none";
        document.getElementById('fade').style.display = "none";
        var newstatus = document.getElementById('ddlNewStatus').options[document.getElementById('ddlNewStatus').selectedIndex].value;
        if(newstatus == 4)
        {
            document.getElementById('ddlNewAssignedTo').disabled = false;
            document.getElementById('date').style.visibility = "hidden";
        }
        else if(newstatus == 1)
        {
            document.getElementById('date').style.visibility = "visible";
        }
        else
        {
            document.getElementById('date').style.visibility = "hidden";
        }
    }
    
    function redirectToBulkUpdate()
    {
        window.location = "bulkstatusupdate.php";
    }
    
$(document).ready(function()
{
        $("#ddlgamename").attr('disabled',true);
        $("#ddlStatus").attr('disabled',true);
        $("#ddlgametype").attr('disabled',true);
        $("#ddlgamebatch").attr('disabled',true);
        $("#ddlStartBookNum").attr('disabled',true);
        $("#ddlEndBookNum").attr('disabled',true);
        document.getElementById('ddlAssignedTo').disabled = true;
   var cardvalue = document.getElementById('ddlcardvalue').options[document.getElementById('ddlcardvalue').selectedIndex].value;
            if(cardvalue !=  ' ')
                 {
                     $("#ddlgamename").attr('disabled',false);
                     $("#ddlgametype").attr('disabled',false);
                     $("#ddlgamebatch").attr('disabled',false);
                     $("#ddlStartBookNum").attr('disabled',false);
                     $("#ddlEndBookNum").attr('disabled',false);
                       $("#ddlStatus").attr('disabled',false);
                       if(document.getElementById('ddlStatus').value == 1)
                        {document.getElementById('ddlAssignedTo').disabled = false;}
                        else if(document.getElementById('ddlStatus').value == 4){document.getElementById('ddlAssignedTo').disabled = false;
                        }else{document.getElementById('ddlAssignedTo').disabled = true;}
                     document.getElementById("hiddenflag").value = 4;
                 }

});
function get_status()
{
  
     $("#ddlStatus").attr('disabled',false);
     var setTodefault = document.getElementById ('ddlStatus');
        setTodefault.value = 0; //set to default -please select
                
      var cardvalue = document.getElementById('ddlcardvalue').options[document.getElementById('ddlcardvalue').selectedIndex].value;
       var status = document.getElementById('ddlStatus').options[document.getElementById('ddlStatus').selectedIndex].value;
    if(cardvalue == ' ')
         {
            $("#ddlStatus").attr('disabled',true);
            $("#ddlgamename").attr('disabled',true);
            $("#ddlgametype").attr('disabled',true);
            $("#ddlgamebatch").attr('disabled',true);
             document.getElementById('ddlAssignedTo').disabled = true;
             $("#ddlStartBookNum").attr('disabled',true);
            $("#ddlEndBookNum").attr('disabled',true);
         }
          if(cardvalue != ' ' &&  status == 0 )
         {
            $("#ddlgamename").attr('disabled',true);
            $("#ddlgametype").attr('disabled',true);
            $("#ddlgamebatch").attr('disabled',true);
             document.getElementById('ddlAssignedTo').disabled = true;
             $("#ddlStartBookNum").attr('disabled',true);
            $("#ddlEndBookNum").attr('disabled',true);
         }
     
}
function get_gamename()
    {
    document.getElementById("hiddenflag").value = 2;
    var setTodefault = document.getElementById ('ddlgamename');
        setTodefault.value = 0; //set to default -please select
    $("#ddlgamename").attr('disabled',false);
    var cardvalue = document.getElementById('ddlcardvalue').options[document.getElementById('ddlcardvalue').selectedIndex].value;
    var gamename = document.getElementById('ddlgamename').options[document.getElementById('ddlgamename').selectedIndex].value;
    var gamenum = document.getElementById('ddlgametype').options[document.getElementById('ddlgametype').selectedIndex].value;
    var gamebatch = document.getElementById('ddlgamebatch').options[document.getElementById('ddlgamebatch').selectedIndex].value;
      var status = document.getElementById('ddlStatus').options[document.getElementById('ddlStatus').selectedIndex].value;
   var flag = 0;
         if(status == 0)
         {
            $("#ddlgamename").attr('disabled',true);
            $("#ddlgametype").attr('disabled',true);
            $("#ddlgamebatch").attr('disabled',true);
            $("#ddlStartBookNum").attr('disabled',true);
            $("#ddlEndBookNum").attr('disabled',true);
             document.getElementById('ddlAssignedTo').disabled = true;
         }
          if(status != 0 && (gamename != 0 || gamename == 0)  && (gamenum != 0 || gamenum == 0)&& (gamebatch != 0 || gamebatch == 0))
            {
                 $("#ddlgametype").attr('disabled',true);
                 $("#ddlgamebatch").attr('disabled',true);
                 $("#ddlStartBookNum").attr('disabled',true);
                 $("#ddlEndBookNum").attr('disabled',true);
                 $("#ddlAssignedTo").attr('disabled',true);
                  
            }
    $("#ddlgamename").load(
        "../controller/get_gamename.php",
        {
            cardvalue: cardvalue,
            gamename: gamename,
            flag: flag
        },

        function(status, xhr)
        {
            if(status == "error")
            {
                document.getElementById('title').innerHTML = "ERROR!";
                document.getElementById('msg').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
                document.getElementById('light').style.display = "block";
                document.getElementById('fade').style.display = "block";
            }
        }
    );
}

    function get_gamenumber()
    {
        document.getElementById('hiddenflag').value = 1;
        $("#ddlgametype").attr('disabled',false);
        document.getElementById("ddlgamebatch").innerHTML = "<option value ='0'>Please select</option>";
        var prodid = document.getElementById('ddlgamename').options[document.getElementById('ddlgamename').selectedIndex].value;
        var cardvalue = document.getElementById('ddlcardvalue').options[document.getElementById('ddlcardvalue').selectedIndex].value;
         var gamenum = document.getElementById('ddlgametype').options[document.getElementById('ddlgametype').selectedIndex].value;
         var gamebatch = document.getElementById('ddlgamebatch').options[document.getElementById('ddlgamebatch').selectedIndex].value;
        var prodid1 = prodid;
     if(prodid == 0)
         {
            $("#ddlgametype").attr('disabled',true);
            $("#ddlgamebatch").attr('disabled',true);
             $("#ddlStartBookNum").attr('disabled',true);
             $("#ddlEndBookNum").attr('disabled',true);
         }
            if(gamebatch == 0 && gamenum != 0)
                {
                    $("#ddlgamebatch").attr('disabled',true);
                }
//    if ( prodid ==0 || cardvalue==0 && prodid !=0 && cardvalue==0 )
//    { 
//       var prodid1 = 0;
//    }
        if(cardvalue == 0)
        {
                            $("#ddlgametype").load(
                        "../controller/get_gamenum.php",
                        {
                            prodid: prodid
                            
                        },
                        function(status, xhr)
                        {
                            if(status == "error")
                            {
                                document.getElementById('title').innerHTML = "ERROR!";
                                document.getElementById('msg').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
                                document.getElementById('light').style.display = "block";
                                document.getElementById('fade').style.display = "block";
                            }
                        }
                    );
        }else
        {
            
                $("#ddlgametype").load(
                    "../controller/get_gamenum_1.php",
                    {
                        prodid: prodid
                        ,cardvalue : cardvalue
                    },
                    function(status, xhr)
                    {
                        if(status == "error")
                        {
                            document.getElementById('title').innerHTML = "ERROR!";
                            document.getElementById('msg').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
                            document.getElementById('light').style.display = "block";
                            document.getElementById('fade').style.display = "block";
                        }
                    }
                );
        }
}

    function get_batchid()
    {
       $("#ddlgamebatch").attr('disabled',false);
    var gamenum = document.getElementById('ddlgametype').options[document.getElementById('ddlgametype').selectedIndex].value;
      var gamebatch = document.getElementById('ddlgamebatch').options[document.getElementById('ddlgamebatch').selectedIndex].value;
     if(gamenum == 0)
         {
            $("#ddlgamebatch").attr('disabled',true);
            $("#ddlStartBookNum").attr('disabled',true);
            $("#ddlEndBookNum").attr('disabled',true);
         }
        
         
    $("#ddlgamebatch").load(
        "../controller/get_gamebatches.php",
        {
            gamenum: gamenum,
            xmltype: 1            
        },
            
        function(status, xhr)
        {
            if(status == "error")
            {
                document.getElementById('title').innerHTML = "ERROR!";
                document.getElementById('msg').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
                document.getElementById('light').style.display = "block";
                document.getElementById('fade').style.display = "block";
            }
        }
    );
}
      function get_booknumbers()
    {
        $("#ddlStartBookNum").attr('disabled',false);
        $("#ddlEndBookNum").attr('disabled',false);
        var batchid = document.getElementById('ddlgamebatch').options[document.getElementById('ddlgamebatch').selectedIndex].value;
        var status = document.getElementById('ddlStatus').options[document.getElementById('ddlStatus').selectedIndex].value;
         if(batchid == 0)
             {
            $("#ddlStartBookNum").attr('disabled',true);
            $("#ddlEndBookNum").attr('disabled',true);
             }
        $("#ddlStartBookNum,#ddlEndBookNum").load(
            "../controller/get_booknumbers.php",
            {
                batchid: batchid,
                status: status
            },
            function(status, xhr)
            {
                if(status == "error")
                {
                    document.getElementById('title').innerHTML = "ERROR!";
                    document.getElementById('msg').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
                    document.getElementById('light').style.display = "block";
                    document.getElementById('fade').style.display = "block";
                }
            }
        );
    }
    function get_ddlassign()
    {
         $("#ddlAssignedTo").attr('disabled',false);
        var setTodefault = document.getElementById ('ddlAssignedTo');
        setTodefault.value = 0; //set to default -please select
        var endbook = document.getElementById('ddlEndBookNum').options[document.getElementById('ddlEndBookNum').selectedIndex].value;
        if(endbook == 0)
            {
                  if(document.getElementById('ddlStatus').value == 1)
                    {
                        document.getElementById('ddlAssignedTo').disabled = false;
                    }
                    else if(document.getElementById('ddlStatus').value == 4)
                    {
                        document.getElementById('ddlAssignedTo').disabled = false;
                    }
                    else
                    {
                        document.getElementById('ddlAssignedTo').disabled = true;
                    }
            }else
                {
                   if(document.getElementById('ddlStatus').value == 1)
                    {
                        document.getElementById('ddlAssignedTo').disabled = false;
                    }
                    else if(document.getElementById('ddlStatus').value == 4)
                    {
                        document.getElementById('ddlAssignedTo').disabled = false;
                    }
                    else
                    {
                        document.getElementById('ddlAssignedTo').disabled = true;
                    }  
                }
            
    }
    function checkBulkStatusUpdate()
    {
        var status = document.getElementById("ddlStatus").options[document.getElementById('ddlStatus').selectedIndex].value;
        var gamename = document.getElementById("ddlgamename").options[document.getElementById('ddlgamename').selectedIndex].value;
        var gamenum = document.getElementById("ddlgametype").options[document.getElementById('ddlgametype').selectedIndex].value;
        var batchid = document.getElementById("ddlgamebatch").options[document.getElementById('ddlgamebatch').selectedIndex].value;
        var bookfrom = document.getElementById("ddlStartBookNum").options[document.getElementById('ddlStartBookNum').selectedIndex].value;
        var bookto = document.getElementById("ddlEndBookNum").options[document.getElementById('ddlEndBookNum').selectedIndex].value;
        var account = document.getElementById("ddlAssignedTo").options[document.getElementById('ddlAssignedTo').selectedIndex].value;
        var cardvalue = document.getElementById("ddlcardvalue").options[document.getElementById('ddlcardvalue').selectedIndex].value;
        if (cardvalue == ' ')
        {
            document.getElementById('title').innerHTML = "ERROR!";
            document.getElementById('msg').innerHTML = "Please select card value.";
            document.getElementById('light').style.display = "block";
            document.getElementById('fade').style.display = "block";
            return false;
        }
        if (status == 0)
        {
            document.getElementById('title').innerHTML = "ERROR!";
            document.getElementById('msg').innerHTML = "Please select a status.";
            document.getElementById('light').style.display = "block";
            document.getElementById('fade').style.display = "block";
            return false;
        }
        
        if (gamename == 0)
        {
            document.getElementById('title').innerHTML = "ERROR!";
            document.getElementById('msg').innerHTML = "Please select a Game Name.";
            document.getElementById('light').style.display = "block";
            document.getElementById('fade').style.display = "block";
            return false;
        }
        if (gamenum == 0)
        {
            document.getElementById('title').innerHTML = "ERROR!";
            document.getElementById('msg').innerHTML = "Please select a Game Number.";
            document.getElementById('light').style.display = "block";
            document.getElementById('fade').style.display = "block";
            return false;
        }
        if (batchid == 0)
        {
            document.getElementById('title').innerHTML = "ERROR!";
            document.getElementById('msg').innerHTML = "Please select a Batch ID.";
            document.getElementById('light').style.display = "block";
            document.getElementById('fade').style.display = "block";
            return false;
        }
        if (bookfrom == '')
        {
            document.getElementById('title').innerHTML = "ERROR!";
            document.getElementById('msg').innerHTML = "Please select From Book Number.";
            document.getElementById('light').style.display = "block";
            document.getElementById('fade').style.display = "block";
            return false;
        }
        if (bookto == '')
        {
            document.getElementById('title').innerHTML = "ERROR!";
            document.getElementById('msg').innerHTML = "Please select To Book Number.";
            document.getElementById('light').style.display = "block";
            document.getElementById('fade').style.display = "block";
            return false;
        }
        if (bookfrom > bookto)
        {
            document.getElementById('title').innerHTML = "ERROR!";
            document.getElementById('msg').innerHTML = "Kindly check the range of Book Numbers.";
            document.getElementById('light').style.display = "block";
            document.getElementById('fade').style.display = "block";
            return false;
        }
        if (status != 3)
        {
            if (account == 0)
            {
                document.getElementById('title').innerHTML = "ERROR!";
                document.getElementById('msg').innerHTML = "Please select an account.";
                document.getElementById('light').style.display = "block";
                document.getElementById('fade').style.display = "block";
                return false;
            }
        }
        return true;
    }
</script>
<!-- POP UP FOR MESSAGES -->
<div id="light" class="white_content">
    <div id="title" class="light-title"></div>
    <div id="msg" class="light-message"></div>
    <div id="button" class="light-button">
        <input type="button" onclick="javascript: document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none';" value="Okay"/>
    </div>
    <div class="light-footer"></div>
</div>
<!-- POP UP FOR MESSAGES -->
<div class="titleCont">
    <div class="titleCont_left"></div>
    <div class="titleCont_body">Scratch Card Bulk Update</div>
    <div class="titleCont_right"></div>
</div>
<div class="content-page">
    <form action="bulkstatusupdate.php" method="post">
        <table>
            <tr>
                
                  <td width="300">Card Value: &nbsp;&nbsp;&nbsp;<?php echo $ddlcardvalue;?></td>
                <td width="300">Status: &nbsp;&nbsp;&nbsp;<?php echo $ddlStatus;?></td>
                <td width="300">Game Name: &nbsp;&nbsp;&nbsp;<?php echo $ddlgamename;?></td>
                <td width="300">Game Number: &nbsp;&nbsp;&nbsp;<?php echo $ddlgametype;?></td>
               
            </tr>
            <tr>
                 <td width="300">Game Batch: &nbsp;&nbsp;&nbsp;<?php echo $ddlgamebatch;?></td>
                <td width="300">From Book Number: &nbsp;&nbsp;&nbsp;<?php echo $ddlStartBookNum;?></td>
                <td width="300">To Book Number: &nbsp;&nbsp;&nbsp;<?php echo $ddlEndBookNum;?></td>
                <td width="300">Assigned To: &nbsp;&nbsp;&nbsp;<?php echo $ddlAssignedTo;?></td>
                <td><?php echo $btnSubmit;?></td>
            </tr>
        </table>
        <?php echo $hiddendeckids;?>
        <?php echo $hidden_date;?>
        <?php echo $hiddenflag;?>
    	<br/><br/>
        <?php if(isset($selecteddeckdtls_list)):?>
                        <div style="float: right;"><?php echo $btnExportCSV; ?></div>
			<?php if(count($selecteddeckdtls_list) > 0):?>
		        <table>
		            <tr>
		                <td><label style="font-weight: bold; font-size: 16px;"><u>Batch Information</u></label></td>
		            </tr>
		            <tr>
		                <td>Status: &nbsp;&nbsp;&nbsp; <?php echo $ddlNewStatus;?> </td>
		            </tr>
		            <tr>
		                <td><label style="font-weight: bold; font-size: 16px;"><u>Assignment Information</u></label></td>
		            </tr>
		            <tr>
		                <td>Assigned To: &nbsp;&nbsp;&nbsp; <?php echo $ddlNewAssignedTo; ?> Release date: &nbsp;&nbsp;&nbsp; <?php echo $txtReleaseDate;?><img name="date" id="date" style="visibility: hidden;" src="images/close.gif" width="16" height="16" border="0" alt="Pick a date" onClick="displayDatePicker('txtReleaseDate', false, 'mmddyyyy', '-');"/></td>
		            </tr>
		            <tr>
		                <td>Address: &nbsp;&nbsp;&nbsp; <?php echo $txtRetailerAddress; ?>&nbsp;&nbsp;&nbsp;Invoice Number: &nbsp;&nbsp;&nbsp; <?php echo $txtInvoiceNo; ?></td>
		            </tr>
		        </table>
		        <table class="table-list">
		            <tr>
                                 <th>Card Value</th>
                                <th>Game Name</th>
                                <th>Book Size</th>
		                <th>Game Number</th>		                
                                <th>Game Batch</th>
		                <th>Carton Number</th>
		                <th>Book Number</th>
		                <th>Status</th>
		                <th>Assigned To</th>
		            </tr>
		            
		            <?php $recCnt = 0;
                                  $recCnt = count($selecteddeckdtls_list);
                                  for($i = 0 ;  $i < count($selecteddeckdtls_list) ; $i++):?>
		            <?php ($i % 2) == 0 ? $class = "evenrow" : $class = "oddrow" ; ?>
		            <tr class="<?php echo $class;?>">
                                 <td><?php echo $selecteddeckdtls_list[$i]["CardPrice"];?></td>
                                <td><?php echo $selecteddeckdtls_list[$i]["ProductName"];?></td>
                                <td><?php echo $selecteddeckdtls_list[$i]["BookTicketCount"];?></td>
		                <td><?php echo $selecteddeckdtls_list[$i]["GameNumber"];?></td>		                
                                <td><?php echo $selecteddeckdtls_list[$i]["BatchID"];?></td>
		                <td><?php echo $selecteddeckdtls_list[$i]["BoxNumber"];?></td>
		                <td><?php echo $selecteddeckdtls_list[$i]["BookNumber"];?></td>
		                    <?php
		                        switch($selecteddeckdtls_list[$i]["Status"])
		                        {
		                            case 1: $status = "Active";
		                            case 3: $status = "On Stock"; break;
		                            case 4: $status = "Assigned"; break;
		                            default : $status = "Cancelled"; break;
		                        }
		                    ?>
		                <td><?php echo $status;?></td>
		                <td><?php echo $selecteddeckdtls_list[$i]["Username"];?></td>
		            </tr>
		            <?php endfor;?>
                            <tr>
                                <th>&nbsp;</th>
                                <th>&nbsp;</th>
                                <th>&nbsp;</th>
                                <th>&nbsp;</th>
                                <th>&nbsp;</th>
				    <th>&nbsp;</th>
                                    <th>&nbsp;</th>
                                <th>Total Books:</th>
                                <th><?php echo $recCnt ?></th>
                            </tr>
		        </table>
                        <div style="float: right;"><?php echo $btnProcess; ?></div>
        
			<?php else:?>
		        <table class="table-list">
		            <tr>
		                <th>Game Number</th>
		                <th>Game Name</th>
		                <th>Carton Number</th>
		                <th>Book Number</th>
		                <th>Status</th>
		                <th>Assigned To</th>
		            </tr>
		            <tr class="no-record">
		                <td colspan="6">No Record Found</td>
		            </tr>
		        </table>
            <?php endif;?>
        <?php endif;?>
        <!-- POP UP FOR MESSAGES -->
        <div id="light2" class="white_content">
            <div id="title2" class="light-title"></div>
            <div id="msg2" class="light-message"></div>
            <div id="button2" class="light-button">
                <?php echo $btnConfirmProcess;?> &nbsp;&nbsp;&nbsp; <?php echo $btnClose;?>
            </div>
            <div class="light-footer"></div>
        </div>
        <!-- POP UP FOR MESSAGES -->
        
    </form>
</div>
<div id="fade" class="black_overlay"></div>

<?php if(isset($errormsg)):?>
    <script>
        document.getElementById('title').innerHTML = "<?php echo $errortitle?>";
        document.getElementById('msg').innerHTML = "<?php echo $errormsg?>";        
        document.getElementById('button').innerHTML = "<input type=\"button\" onclick=\"javascript: document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none';\" value=\"Okay\"/>";
        document.getElementById('light').style.display = "block";
        document.getElementById('fade').style.display = "block";
//     $("#ddlNewAssignedTo").attr('disabled',false);
    </script>
<?php endif;?> 

<?php if(isset($confirm_msg)):?>
    <script>
        document.getElementById('title2').innerHTML = "<?php echo $confirm_title?>";
        document.getElementById('msg2').innerHTML = "<?php echo $confirm_msg?>";
        document.getElementById('light2').style.display = "block";
        document.getElementById('fade').style.display = "block";
//        $("#txtInvoiceNo").attr('disabled',false);
    </script>
<?php endif;?>
    
<?php if(isset($msg)):?>
    <script>
        document.getElementById('title').innerHTML = "<?php echo $msgtitle?>";
        document.getElementById('msg').innerHTML = "<?php echo $msg?>";
        document.getElementById('button').innerHTML = "<?php echo $btnOkay;?>";
        document.getElementById('light').style.display = "block";
        document.getElementById('fade').style.display = "block";
    </script>
<?php endif;?>      

<?php include("footer.php"); ?>
