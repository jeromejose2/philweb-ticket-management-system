<?php
/*
 * Author: Noel Antonio
 * Date Created: 2011-09-01
 * Company: Philweb Corporation
 * Modified by: Frances Ralph DL. Sison
 * Modified on: 2013-04-05
 */

include("init.inc.php");
$pagesubmenuid = 10;
include_once("../controller/managesession.php");
include("../controller/cancellationcontroller.php");
?>

<?php include("header.php"); ?>
        <link rel="stylesheet" type="text/css" href="css/datepicker.css" />
        <link rel="stylesheet" type="text/css" href="css/default.css" />
        <script type="text/javascript" src="jscripts/datetimepicker.js"></script>
        <script language="javascript" type="text/javascript">
            function ChangePage(pagenum)
            {
                selectedindex = document.getElementById("pgSelectedPage");
                selectedindex.value = pagenum;
                document.forms[0].submit();
            }
            function get_gamenumber()
            {
                document.getElementById("ddlgametype").onchange = get_batchID;
                document.getElementById("flag").value = 1;
                document.getElementById("xmltype").value = 1;
                document.getElementById("ddlgamebatch").innerHTML = "<option value ='0'>ALL</option>";
                var prodid = document.getElementById('ddlgamename').options[document.getElementById('ddlgamename').selectedIndex].value;
                 if (prodid == 0)
                {
                   document.getElementById("ddlgametype").onchange= function(){get_gamename();get_batchID()};     
                }
                $("#ddlgametype").load(
                    "../controller/get_gamenumwithdefall.php",
                    {
                        prodid: prodid
                    },
                    function(status, xhr)
                    {
                        if(status == "error")
                        {
                            document.getElementById('title5').innerHTML = "ERROR!";
                            document.getElementById('msg5').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
                            document.getElementById('light5').style.display = "block";
                            document.getElementById('fade').style.display = "block";
                        }
                    }
                );
            }

            function get_batchID()
            {
                document.getElementById("xmltype").value = 1;
                var gamenum = document.getElementById('ddlgametype').options[document.getElementById('ddlgametype').selectedIndex].value; 
                var prodid = document.getElementById('ddlgamename').options[document.getElementById('ddlgamename').selectedIndex].value;
                var xmltype = document.getElementById('xmltype').value;    
                $("#ddlgamebatch").load(
                    "../controller/get_batchidwithdefall.php",
                    {
                        gamenum: gamenum,
                        prodid: prodid,
                        xmltype:xmltype            
                    },

                    function(status, xhr)
                    {
                        if(status == "error")
                        {
                            document.getElementById('title').innerHTML = "ERROR!";
                            document.getElementById('msg').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
                            document.getElementById('light').style.display = "block";
                            document.getElementById('fade').style.display = "block";
                        }
                    }
                );
            }
            function get_gamename()
            {
               document.getElementById("ddlgamename").onchange = null;
               document.getElementById("xmltype").value = 1;  
               document.getElementById("flag").value = 2;
               var gamenum = document.getElementById('ddlgametype').options[document.getElementById('ddlgametype').selectedIndex].value;
               if (gamenum ==0)
                   {
                      document.getElementById("ddlgamename").onchange = get_gamenumber; 
                   }
                $("#ddlgamename").load(
                    "../controller/get_gamenamewithdefall.php",
                    {
                        gamenum: gamenum
                    },

                    function(status, xhr)
                    {
                        if(status == "error")
                        {
                            document.getElementById('title').innerHTML = "ERROR!";
                            document.getElementById('msg').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
                            document.getElementById('light').style.display = "block";
                            document.getElementById('fade').style.display = "block";
                        }
                    }
                );
    
}
        </script>
        <form action="cancellationhistory.php" method="POST">
            <div class="titleCont">
            <div class="titleCont_left"></div>
            <div class="titleCont_body">Cancellation History</div>
            <div class="titleCont_right"></div>
            </div>
            <div class="content-page">
                <table>
                    <tr>
                        <td>From: </td>
                        <td width="150">
                            <?php echo $flag;?>
                            <?php echo $xmltype;?>
                            <?php echo $txtDateFr;?>
                            <img name="cal" src="images/close.gif" width="16" height="16" border="0" alt="Pick a date" onClick="displayDatePicker('txtDateFr', false, 'ymd', '-');"/>
                        </td>
                        <td>To: </td>
                        <td width="150">
                            <?php echo $txtDateTo;?>
                            <img name="cal" src="images/close.gif" width="16" height="16" border="0" alt="Pick a date" onClick="displayDatePicker('txtDateTo', false, 'ymd', '-');"/>
                        </td>
                        <td>Game Name: </td>
                        <td><?php echo $ddlgamename; //echo $txtprodtype;?></td>
                        <td> Game Number: </td>
                        <td><?php echo $ddlgametype;?></td>
                        <td> Game Batch: </td>
                        <td><?php echo $ddlgamebatch;?></td>
                    </tr>
                    <tr>
                        <td colspan=4"></td>
                        <td>View By: </td>
                        <td><?php echo $ddlcanceltype;?></td>
                        <td colspan="4" align="right"><?php echo $btnSubmit;?></td>
                    </tr>
                </table><br/>
                    <?php if (isset($cancel_list)):?>
                    <div class="form-page"><?php echo $pgTransactionHistory; ?></div>
                    <div class="form-page"><?php echo $btnExportCSV;?></div>
                    <table class="table-list" style="text-align: center;">
                        <tr align="center">
                            <th>Date</th>
                            <th>Account</th>
                            <th>Mode of Cancellation</th>                            
                            <th>Game Name</th>
                            <th>Game Number</th>
                            <th>Game Batch</th>
                            <th>Tracking Number</th>
                            <th>Book Size</th>
                            <th>Remarks</th>
                        </tr>
                        <?php if(count($cancel_list) > 0): ?>
                        <?php for($ctr=0; $ctr < count($cancel_list); $ctr++): ?>
                        <?php ($ctr % 2) == 0 ? $class = "evenrow" : $class = "oddrow" ; ?>
                        <tr class = "<?php echo $class?>">
                            <?php $dateTime = new DateTime($cancel_list[$ctr]['DateCancelled']);?>
                            <td><?php echo $dateTime->format('Y-m-d h:i:s A');?></td>
                            <td><?php echo $cancel_list[$ctr]['Username'];?></td>
                            <td><?php echo $cancel_list[$ctr]['CancelMode'];?></td>
                            <td><?php echo $cancel_list[$ctr]['ProductName'];?></td>
                            <td><?php echo $cancel_list[$ctr]['GameNumber'];?></td>                            
                            <td><?php echo $cancel_list[$ctr]['BatchID'];?></td>
<!--                            <td><?php //echo $cancel_list[$ctr]['GameNumber'].$cancel_list[$ctr]['BookNumber'].$cancel_list[$ctr]['TicketNumber'];?></td>-->
                            <td><?php echo $cancel_list[$ctr]['TicketTrackingNumber'];?></td>
                            <td><?php echo $cancel_list[$ctr]['BookTicketCount'];?></td>
                            <td><?php echo $cancel_list[$ctr]['Remarks'];?></td>
                        </tr>
                        <?php endfor; ?>
                        <?php else: ?>
                        <tr class="no-record">
                            <td colspan="9">No records found</td>
                        </tr>
                        <?php endif;?>
                    </table>
                         <?php endif;?>
               </div>
            <div id="fade" class="black_overlay"></div>
            <div id="loading" class="loading"></div>
            <!-- POP UP FOR MESSAGES -->
            <div id="light5" class="white_content">
                <div id="title5" class="light-title"></div>
                <div id="msg5" class="light-message"></div>
                <div id="button" class="light-button"><input type="button" onclick="javascript: document.getElementById('light5').style.display='none';document.getElementById('fade').style.display='none';" value="OKAY"/></div>
                <div class="light-footer"></div>
            </div>
            <!-- POP UP FOR MESSAGES -->
        </form>
        <div class="content-page"><div class="form-page"><?php echo $pgTransactionHistory; ?></div></div>
       <?php include("footer.php"); ?>

<script language="javascript" type="text/javascript">
function validate()
{
    var SDate = document.getElementById("txtDateFr").value;
    var EDate = document.getElementById("txtDateTo").value;

    var endDate = new Date(EDate);
    var startDate = new Date(SDate);
    var currDate = new Date();

    if(SDate != '' && EDate != '' && startDate > endDate)
    {
    	document.getElementById('title5').innerHTML = "ERROR";
        document.getElementById('msg5').innerHTML = "Please enter a valid date range.";
        document.getElementById('light5').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    else if (SDate.replace(/^\s\s*/, '').replace(/\s\s*$/, '').length == 0)
    {
        document.getElementById('title5').innerHTML = "ERROR";
        document.getElementById('msg5').innerHTML = "Please select your Start Date.";
        document.getElementById('light5').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    else if (EDate.replace(/^\s\s*/, '').replace(/\s\s*$/, '').length == 0)
    {
        document.getElementById('title5').innerHTML = "ERROR";
        document.getElementById('msg5').innerHTML = "Please select your End Date.";
        document.getElementById('light5').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    else if (endDate > currDate)
    {
        document.getElementById('title5').innerHTML = "ERROR";
        document.getElementById('msg5').innerHTML = "Please enter a valid date range.";
        document.getElementById('light5').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    else{ return true;}
}
</script>