<?php
/*
 * Created By: Arlene R. Salazar on 08/26/2011
 * Purpose: For updating account status
 */
require_once ("init.inc.php");
//include_once("../controller/managesession.php");
include("../controller/changepasswordprocess.php");
?>
<html>
    <head>
        <title>Change Password</title>
        <link rel="stylesheet" type="text/css" media="screen" href="../css/default.css" />
        <script type="text/javascript" src="../jscripts/validations.js"></script>
    </head>
    <body>
	<?php if(isset($current_password)):?>
        <div id="fade" class="black_overlay"></div>
        <form name="frmChangeStatus" method="POST">
        <table border="0" cellspacing="0" cellpadding="0" class="container">
        <tr>
            <td colspan="2">
                <div class="header">
                    <div class="company_logo">
                        <img src="../images/philweb_logo.png" alt="" height ="70px" width="190px"/>
                  </div>
                </div>
            </td>
       </tr>
       <tr>
           <td colspan="2">
                <div class="project_logo" align="center">
                    <img src="../images/scratch_n_win_logo.png" alt="" height ="70px" width="140px"/>
                </div>
           </td>
       </tr>
          <tr height="370">
                    <td valign="top" class="content_container">
                        <div class="login-form" style="margin-bottom: 5.2%;">
                            <table class="form-change-password">
                                <tr>
                                    <td><h1 align="center">Welcome <?php echo $username;?></h1></td>
                                </tr>
                                <tr><td></td></tr>
                                <tr>
                                    <th>Change Password:</th>
                                </tr>
                                <tr>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td><?php echo $txtnewpword;?></td>
                                </tr>
                                <tr>
                                    <td><?php echo $txtcfrmpword;?></td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="form-button">
                                            <div><?php echo $btnSubmit;?></div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="footer"></td>
                </tr>
            </table>
            
            <!-- POP UP FOR CONFIRMATION MESSAGES -->
            <div id="light6" class="white_content">
                <div id="title6" class="light-title">CONFIRMATION</div>
                <div id="msg6" class="light-message">Are you sure you want to change your password?</div>
                <div id="button6" class="light-button">
                    <?php echo $btnConfirmChangePword;?>
                    <input type="button" id="btnOk6" name="btnOk" value="Cancel" onclick="javascript: document.getElementById('light6').style.display = 'none';document.getElementById('fade').style.display = 'none';"/>
                </div>
                <div class="light-footer"></div>
            </div>
            <!-- POP UP FOR CONFIRMATION MESSAGES -->
            <!-- POP UP FOR MESSAGES -->
            <div id="light7" class="white_content">
                <div id="title7" class="light-title"></div>
                <div id="msg7" class="light-message"></div>
                <div id="button7" class="light-button">
                    <input type="button" id="btnOk7" name="btnOk" value="Okay" onclick="javascript: document.getElementById('light7').style.display = 'none';document.getElementById('fade').style.display = 'none';"/>
                </div>
                <div class="light-footer"></div>
            </div>
            <!-- POP UP FOR MESSAGES -->
			<!-- POP UP FOR MESSAGES -->
            <div id="light8" class="white_content">
                <div id="title8" class="light-title"></div>
                <div id="msg8" class="light-message"></div>
                <div id="button8" class="light-button">
                    <input type="button" id="btnOk8" name="btnOk" value="Okay" onclick="javascript: window.location = 'login.php';"/>
                </div>
				<div class="light-footer"></div>
            </div>
            <!-- POP UP FOR MESSAGES -->
            <?php if(isset($chgepwordmsg)):?>
                <script>
                    document.getElementById('title7').innerHTML = "<?php echo $chgepwordtitle;?>";
                    document.getElementById('msg7').innerHTML = "<?php echo $chgepwordmsg;?>";
                    document.getElementById('light7').style.display = "block";
                    document.getElementById('fade').style.display = "block";
                </script>
            <?php endif;?>
            <?php $chgepwordmsg = null; $chgepwordtitle = null;?>
            <?php if(isset($okchgepwordmsg)):?>
                <script>
                    document.getElementById('light6').style.display = "block";
                    document.getElementById('fade').style.display = "block";
                </script>
            <?php endif;?>
            <?php $okchgepwordmsg = null;?>
            <?php if(isset($confchangepword_title)):?>
                <script>
                    document.getElementById('title8').innerHTML = "<?php echo $confchangepword_title;?>";
                    document.getElementById('msg8').innerHTML = "<?php echo $confchangepword_msg;?>";
                    document.getElementById('light8').style.display = "block";
                    document.getElementById('fade').style.display = "block";
		    //document.getElementById('button7').innerHTML = "<input type='button' onclick='javascript:self.close();' value='OKAY'></input>";
                </script>
            <?php endif;?>
            <?php $confchangepword_title = null; $confchangepword_msg = null;?>
        </form>
	<?php else: ?>
		<h1 style="text-align: center;">Sorry. You cannot access this page.<h1>
	<?php endif; ?>
    </body>
</html>