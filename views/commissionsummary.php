<?php
/*
 * Created by Arlene R. Salazar on 01-11-2012
 * Modified By Noel Antonio on 08-01-2012
 * Purpose: controller for commission summary
 */
require_once ("init.inc.php");
$pagesubmenuid = 41;
include_once("../controller/managesession.php");
include("../controller/commissionsummaryprocess.php");
?>
<script language="javascript" type="text/javascript">
    function ChangePage(pagenum)
    {
        selectedindex = document.getElementById("pgSelectedPage");
        selectedindex.value = pagenum;
        document.forms[0].submit();
    }

    function get_gamenumber()
    {
        document.getElementById("ddlgametype").onchange = get_batchID;
        document.getElementById("flag").value = 1;
        document.getElementById("xmltype").value = 1;
        document.getElementById("ddlgamebatch").innerHTML = "<option value ='0'>ALL</option>";
        var prodid = document.getElementById('ddlgamename').options[document.getElementById('ddlgamename').selectedIndex].value;
        if (prodid == 0)
        {
           document.getElementById("ddlgametype").onchange= function(){get_gamename();get_batchID()};     
        }
        $("#ddlgametype").load(
            "../controller/get_gamenumwithdefall.php",
            {
                prodid: prodid
            },
            function(status, xhr)
            {
                if(status == "error")
                {
                    document.getElementById('title5').innerHTML = "ERROR!";
                    document.getElementById('msg5').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
                    document.getElementById('light5').style.display = "block";
                    document.getElementById('fade').style.display = "block";
                }
            }
        );
        
//        var prodid = document.getElementById('ddlgamename').options[document.getElementById('ddlgamename').selectedIndex].value;
//        var cardvalue = document.getElementById('ddlcardvalue').options[document.getElementById('ddlcardvalue').selectedIndex].value;
//        var prodid1 = prodid;
//
//        if ( (prodid ==0 || cardvalue==0) && (prodid !=0 && cardvalue==0) )
//        { 
//            var prodid1 = 0;
//            document.getElementById("ddlgametype").onchange= function(){ get_batchID() };     
//        }
//        $("#ddlgametype").load(
//            "../controller/get_gamenum.php",
//            {
//                prodid: prodid1
//            },
//
//            function(response, status, xhr)
//            {
//                if (status == "success")
//                {
//                    $('#ddlgametype option[value="0"]').remove();
//                    $("#ddlgametype").prepend("<option selected value=\"0\" >ALL</option>");
//                }
//                if(status == "error")
//                {
//                    document.getElementById('title').innerHTML = "ERROR!";
//                    document.getElementById('msg').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
//                    document.getElementById('light').style.display = "block";
//                    document.getElementById('fade').style.display = "block";
//                }
//            }
//        );
    }
    function get_batchID()
    {
        document.getElementById("xmltype").value = 2;
        var gamenum = document.getElementById('ddlgametype').options[document.getElementById('ddlgametype').selectedIndex].value; 
        var prodid = document.getElementById('ddlgamename').options[document.getElementById('ddlgamename').selectedIndex].value;
        var xmltype = document.getElementById('xmltype').value;    
        $("#ddlgamebatch").load(
            "../controller/get_batchidwithdefall.php",
            {
                gamenum: gamenum,
                prodid: prodid,
                xmltype:xmltype            
            },

            function(status, xhr)
            {
                if(status == "error")
                {
                    document.getElementById('title').innerHTML = "ERROR!";
                    document.getElementById('msg').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
                    document.getElementById('light').style.display = "block";
                    document.getElementById('fade').style.display = "block";
                }
            }
        );
    }
    function get_gamename()
    {
       document.getElementById("ddlgamename").onchange = null;
       document.getElementById("xmltype").value = 1;  
       document.getElementById("flag").value = 2;
       var gamenum = document.getElementById('ddlgametype').options[document.getElementById('ddlgametype').selectedIndex].value;
       if (gamenum ==0)
           {
              document.getElementById("ddlgamename").onchange = get_gamenumber; 
           }
        $("#ddlgamename").load(
            "../controller/get_gamenamewithdefall.php",
            {
                gamenum: gamenum
            },

            function(status, xhr)
            {
                if(status == "error")
                {
                    document.getElementById('title').innerHTML = "ERROR!";
                    document.getElementById('msg').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
                    document.getElementById('light').style.display = "block";
                    document.getElementById('fade').style.display = "block";
                }
            }
        );
        
//        document.getElementById("flag").value = 1;
//        var cardvalue = document.getElementById('ddlcardvalue').options[document.getElementById('ddlcardvalue').selectedIndex].value;
//        var gamename = document.getElementById('ddlgamename').options[document.getElementById('ddlgamename').selectedIndex].value;
//        var flag = 0;
//
//        $("#ddlgamename").load(
//            "../controller/get_gamename.php",
//            {
//                cardvalue: cardvalue,
//                gamename: gamename,
//                flag: flag
//            },
//
//            function(response, status, xhr)
//            {
//                if (status == "success")
//                {
//                    $('#ddlgamename option[value="0"]').remove();
//                    $("#ddlgamename").prepend("<option selected value=\"0\" >ALL</option>");
//
//                    $("#ddlgametype").empty();
//                    $("#ddlgametype").prepend("<option selected value=\"0\" >ALL</option>");
//                }
//                if(status == "error")
//                {
//                    document.getElementById('title').innerHTML = "ERROR!";
//                    document.getElementById('msg').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
//                    document.getElementById('light').style.display = "block";
//                    document.getElementById('fade').style.display = "block";
//                }
//            }
//        );
    }

    function viewCommissionDtls(commid,gameid)
    {
        document.getElementById('hiddenbatchvalidationid').value = commid;
        document.getElementById('hiddenselectedgameid').value = gameid;
        document.forms["commsummlist"].submit();
    }
</script>
<?php include("header.php"); ?>
<!-- POP UP FOR MESSAGES -->
<div id="light" class="white_content">
    <div id="title" class="light-title"></div>
    <div id="msg" class="light-message"></div>
    <div id="button" class="light-button">
        <input type="button" onclick="javascript: document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none';" value="Okay"/>
    </div>
    <div class="light-footer"></div>
</div>
<!-- POP UP FOR MESSAGES -->
<div class="titleCont">
    <div class="titleCont_left"></div>
    <div class="titleCont_body">Redemption Commission</div>
    <div class="titleCont_right"></div>
</div>
<div class="content-page">
    <form action="commissionsummary.php" method="post">
        <?php echo $hiddenflag;?>
        <?php echo $xmltype; ?>
        <?php echo $flag; ?>
        <table>
            <tr>
                <td width="250">Username: &nbsp;&nbsp;&nbsp;<?php echo $ddlUsername;?></td>
<!--                <td width="250">Card Value: &nbsp;&nbsp;&nbsp;<?php //echo $ddlcardvalue;?></td>-->
                <td width="300">Game Name: &nbsp;&nbsp;&nbsp;<?php echo $ddlgamename;?></td>
                <td width="200">Game Number: &nbsp;&nbsp;&nbsp;<?php echo $ddlgametype;?></td>
                <td width="250">Game Batch: &nbsp;&nbsp;&nbsp;<?php echo $ddlgamebatch;?></td>
                <td><?php echo $btnSubmit;?></td>
            </tr>
        </table>
    
    <?php if (isset($commissionsummlist)):?>
        <div class="form-page">
            <?php echo $pgTransactionHistory;?><br/><br/>
            <?php echo $btnExportCSV;?>
        </div>
	</form>
        <form action="redemptioncommdetailed.php" method="post" name="commsummlist">
            <?php //echo $hiddencardvalue;?>
            <?php echo $hiddengameid;?>
            <?php echo $hiddenprodid;?>
            <?php echo $hiddenuserid;?>
            <?php echo $hiddenbatchvalidationid;?>
            <?php echo $hiddenprocessedtag;?>
            <?php echo $hiddenbatchid;?>
            <?php echo $hiddenselectedgameid; ?>
            <table class="table-list">
                <tr>
                    <th>Username</th>
                    <th>Date & Time</th>
                    <th>Total Redemption Commission</th>
                    <th>Option</th>
                </tr>

            <?php if(count($commissionsummlist) > 0):?>
                <?php for($i = 0 ; $i < count($commissionsummlist) ; $i++):?>
                <?php ($i % 2) == 0 ? $class = "evenrow" : $class = "oddrow" ; ?>
                <tr class="<?php echo $class;?>">
                    <td><?php echo $commissionsummlist[$i]["Username"];?></td>
                    <td><?php echo $commissionsummlist[$i]["DateValidated"];?></td>
                    <td style="text-align: right;">
                        <?php if (($commissionsummlist[$i]["AccountTypeID"] == 6 || $commissionsummlist[$i]["AccountTypeID"] == 1  || $commissionsummlist[$i]["AccountTypeID"] == 3) && ($commissionsummlist[$i]["PrizeType"] == 2))
                        { 
                            echo $currency . " " . number_format(($commissionsummlist[$i]["Total"] * ($incentive / 100)) , 2 , "." , ",");
                        } 
                        else 
                        { 
                            echo $currency . " 0.00";
                        }
                        ?>
                    </td>
                    <td><input type="button" value="View Details" onclick="viewCommissionDtls(<?php echo $commissionsummlist[$i]["BatchValidationSessionID"];?>,<?php echo $commissionsummlist[$i]["GameID"]?>)"/></td>
                </tr>
                <?php endfor;?>
            <?php else:?>
                <tr class="no-record">
                    <td colspan="5">No Record Found</td>
                </tr>
            <?php endif;?>
            </table>
        </form>
		<div class="form-page">
            <?php echo $pgTransactionHistory;?>
        </div>
    <?php endif;?>
</div>
<div id="fade" class="black_overlay"></div>
<?php include("footer.php"); ?>