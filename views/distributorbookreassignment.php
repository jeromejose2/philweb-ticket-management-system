<?php
/* 
 * Created By: Arlene R. Salazar on 10-06-2011
 * Modified By: Noel Antonio on 01-14-2013
 * Purpose: For book transfer
 */
require_once ("init.inc.php");
$pagesubmenuid = 38;
include_once("../controller/managesession.php");
include("../controller/distributorbookreassignmentprocess.php");
?><script type="text/javascript" src="jscripts/jquery-1.5.2.min.js"></script>
<script language="javascript">
    function SelectedIndexChange(index)
    {
        selectedindex = document.getElementById("hdnSelectedProviderID");
        selectedindex.value = index;
        document.forms[0].submit();
    }
    function ChangePage(pagenum)
    {
        selectedindex = document.getElementById("pgSelectedPage");
        selectedindex.value = pagenum;
        document.forms[0].submit();
    }
    function get_product()
    {
        var pid = document.getElementById('ddlgametype').value;

        $.ajax
        ({
            url: '../controller/get_product.php?pid='+pid,
            type : 'post',
            success : function(data)
            {
                $('#txtproductname').val(data)
            },
            error: function(e)
            {
                alert("Error");
            }
        });
    }
    function Check(chk)
    {
        if(chk == undefined)
        {
                //alert("ok");
        }
        else
        {
            if(document.forms["frmbooktransfer"].maincheckbox.checked==true)
            {
                if(("<?php echo $_SESSION['accttype']?>" == 2) || ("<?php echo $_SESSION['accttype']?>" == 4))
                {

                }
                else
                    document.getElementById('btntransfer').style.display = "block";
                if(chk.length > 0)
                {
                    for (i = 0; i < chk.length; i++)
                        chk[i].checked = true ;
                }
                else
                {
                    chk.checked = true ;
                }
            }else
            {
                if(("<?php echo $_SESSION['accttype']?>" == 2) || ("<?php echo $_SESSION['accttype']?>" == 4))
                {

                }
                else
                    document.getElementById('btntransfer').style.display = "none";
                if(chk.length > 0)
                {
                    for (i = 0; i < chk.length; i++)
                        chk[i].checked = false ;
                }
                else
                {
                    chk.checked = false ;
                }
            }
        }
    }
    function enablebutton(chk)
    {
        document.forms["frmbooktransfer"].maincheckbox.checked = false;
        var chkBoxCount = chk.length;
        var totalChecked = 0;
        if(chkBoxCount > 0)
        {
            for (var i = 0; i < chkBoxCount; i++)
            {
                //check the state of each CheckBox
                //replace "YourFormName" with the name of your form
                if (eval("chk[" + i + "].checked") == true)
                {
                    //it's checked so increment the counter
                    totalChecked += 1;
                }
            }
        }
        else
        {
            if (eval("chk.checked") == true)
            {
                //it's checked so increment the counter
                totalChecked += 1;
                document.forms["frmbooktransfer"].maincheckbox.checked = true;
            }
        }
        if(("<?php echo $_SESSION['accttype']?>" == 2) || ("<?php echo $_SESSION['accttype']?>" == 4))
        {

        }
        else
        {
            if(totalChecked == chkBoxCount)
            {
                document.forms["frmbooktransfer"].maincheckbox.checked = true;
            }
            if(totalChecked > 0)
                document.getElementById('btntransfer').style.display = "block";
            else
                document.getElementById('btntransfer').style.display = "none";
        }
    }
    function formUpdateBookTransSubmit()
    {
        document.forms["frmbooktransfer"].action="distributorbookreassignment_update.php";
        document.forms["frmbooktransfer"].submit();
    }
    
    $(document).ready(function()
    {
        $("#ddlgamename").attr('disabled',true);
        $("#ddlgametype").attr('disabled',true);
        $("#ddlgamebatch").attr('disabled',true);
        $("#ddlassingedto").attr('disabled',true);
        var cardvalue = document.getElementById('ddlcardvalue').options[document.getElementById('ddlcardvalue').selectedIndex].value;
        
        if(cardvalue != ' ')
        {
            $("#ddlgamename").attr('disabled',false);
            $("#ddlgametype").attr('disabled',false);
            $("#ddlgamebatch").attr('disabled',false);
            $("#ddlassingedto").attr('disabled',false);
            document.getElementById('hiddenflag').value = 4;
        }
});


function get_gamename()
{
    document.getElementById('hiddenflag').value = 2;
    $("#ddlgamename").attr('disabled',false);
    var setTodefault = document.getElementById ('ddlgamename');
    setTodefault.value = 0; //set to default -please select
    var cardvalue = document.getElementById('ddlcardvalue').options[document.getElementById('ddlcardvalue').selectedIndex].value;
    var gamename = document.getElementById('ddlgamename').options[document.getElementById('ddlgamename').selectedIndex].value;
    var gamenum = document.getElementById('ddlgametype').options[document.getElementById('ddlgametype').selectedIndex].value;
    var gamebatch = document.getElementById('ddlgamebatch').options[document.getElementById('ddlgamebatch').selectedIndex].value;
    var flag = 0;
    
    if(cardvalue == ' ')
    {
        $("#ddlgamename").attr('disabled',true);
        $("#ddlgametype").attr('disabled',true);
        $("#ddlgamebatch").attr('disabled',true);
        $("#ddlassingedto").attr('disabled',true);
    }
    if(cardvalue != ' ' && (gamename != 0 || gamename == 0) && (gamenum != 0 || gamenum == 0)&& (gamebatch != 0 || gamebatch == 0))
    {
        $("#ddlgametype").attr('disabled',true);
        $("#ddlgamebatch").attr('disabled',true);
        $("#ddlassingedto").attr('disabled',true);
    }
    $("#ddlgamename").load(
        "../controller/get_gamename.php",
        {
            cardvalue: cardvalue,
            gamename: gamename,
            flag: flag
        },

        function(status, xhr)
        {
            if(status == "error")
            {
                document.getElementById('title').innerHTML = "ERROR!";
                document.getElementById('msg').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
                document.getElementById('light').style.display = "block";
                document.getElementById('fade').style.display = "block";
            }
        }
    );
}


function get_gamenumber()
{
    document.getElementById('hiddenflag').value = "1";
    $("#ddlgametype").attr('disabled',false);
    document.getElementById("ddlgamebatch").innerHTML = "<option value ='0'>Please select</option>";
    var prodid = document.getElementById('ddlgamename').options[document.getElementById('ddlgamename').selectedIndex].value;
    var cardvalue = document.getElementById('ddlcardvalue').options[document.getElementById('ddlcardvalue').selectedIndex].value;
    var gamenum = document.getElementById('ddlgametype').options[document.getElementById('ddlgametype').selectedIndex].value;
    var gamebatch = document.getElementById('ddlgamebatch').options[document.getElementById('ddlgamebatch').selectedIndex].value;
    var prodid1 = prodid;
    if(prodid == 0)
    {
        $("#ddlgametype").attr('disabled',true);
        $("#ddlgamebatch").attr('disabled',true);
        $("#ddlassingedto").attr('disabled',true);
    }
    
    if(gamebatch == 0 && gamenum != 0)
    {
        $("#ddlgamebatch").attr('disabled',true);
        $("#ddlassingedto").attr('disabled',true);
    }

    if(cardvalue == 0)
    {
        $("#ddlgametype").load(
            "../controller/get_gamenum.php",
            {
                prodid: prodid

            },
        
            function(status, xhr)
            {
                if(status == "error")
                {
                    document.getElementById('title').innerHTML = "ERROR!";
                    document.getElementById('msg').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
                    document.getElementById('light').style.display = "block";
                    document.getElementById('fade').style.display = "block";
                }
            }
        );
    }
    else
    {
        $("#ddlgametype").load(
            "../controller/get_gamenum_1.php",
            {
                prodid: prodid
                ,cardvalue : cardvalue
            },
            function(status, xhr)
            {
                if(status == "error")
                {
                    document.getElementById('title').innerHTML = "ERROR!";
                    document.getElementById('msg').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
                    document.getElementById('light').style.display = "block";
                    document.getElementById('fade').style.display = "block";
                }
            }
        );
    }
}

function get_batchID()
{
    $("#ddlgamebatch").attr('disabled',false);
    var gamenum = document.getElementById('ddlgametype').options[document.getElementById('ddlgametype').selectedIndex].value;   
    var gamebatch = document.getElementById('ddlgamebatch').options[document.getElementById('ddlgamebatch').selectedIndex].value;
    var assingedto = document.getElementById('ddlassingedto').options[document.getElementById('ddlassingedto').selectedIndex].value;
    if(gamenum == 0)
    {
        $("#ddlgamebatch").attr('disabled',true);
        $("#ddlassingedto").attr('disabled',true);
    }
    
    if(gamenum != 0 && (gamebatch !=0 || gamebatch==0) && (assingedto != 0 || assingedto == 0))
    {
        $("#ddlassingedto").attr('disabled',true);
    }
    
    $("#ddlgamebatch").load(
        "../controller/get_gamebatches.php",
        {
            gamenum: gamenum,
            xmltype: 1            
        },
            
        function(status, xhr)
        {
            if(status == "error")
            {
                document.getElementById('title').innerHTML = "ERROR!";
                document.getElementById('msg').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
                document.getElementById('light').style.display = "block";
                document.getElementById('fade').style.display = "block";
            }
        }
    );
}


function get_assignedto()
{
    $("#ddlassingedto").attr('disabled',false);
    var setTodefault = document.getElementById('ddlassingedto');
    setTodefault.value = '-'; //set to default -please select
    var gamebatch = document.getElementById('ddlgamebatch').options[document.getElementById('ddlgamebatch').selectedIndex].value;
    if(gamebatch == 0)
    {
        $("#ddlassingedto").attr('disabled',true);
    }
}
</script>

<?php include("header.php");?>
<div id="fade" class="black_overlay"></div>
<!-- POP UP FOR MESSAGES -->
<div id="light" class="white_content">
    <div id="title" class="light-title"></div>
    <div id="msg" class="light-message"></div>
    <div id="button" class="light-button">
        <input type="button" onclick="javascript: document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none';" value="Okay"/>
    </div>
	<div class="light-footer"></div>
</div>
<!-- POP UP FOR MESSAGES -->
<form method="post" name="frmbooktransfer">
    <div class="titleCont">
            <div class="titleCont_left"></div>
            <div class="titleCont_body">Book Reassignment (New)</div>
            <div class="titleCont_right"></div>
    </div>
	<div class="content-page">
		<?php echo $hiddencttrid;?>
		<?php echo $hiddenflag;?>
                <?php echo $hiddenfield;?>
		<div class="search-container">
		    <div class="form-search">
		        <?php echo $txtsearch;?><?php echo $btnsearch;?>
		    </div>
		    <br/>
		    <br/>
		    <div class="form-view">
                        &nbsp;Card Value: <?php echo $ddlcardvalue;?>
                        Game Name: <?php echo $ddlgamename;?> &nbsp;&nbsp;&nbsp;
                        Game Number: <?php echo $ddlgametype;?> &nbsp;&nbsp;&nbsp; 
                        Game Batch: <?php echo $ddlgamebatch;?> &nbsp;&nbsp;&nbsp;
                        <!-- Game Name: <?php echo $txtproductname;?> &nbsp;&nbsp;&nbsp; -->
                        Assigned To: <?php echo $ddlassingedto;?> &nbsp;&nbsp;&nbsp;
		    </div>
                    <div class="form-button" style="float: left;"><?php echo $btnsubmit; ?></div>
		</div>
	
    <?php if(isset($booktranslist)):?>
    <div class="form-page"><?php echo isset($pgbooktransfer) ? $pgbooktransfer : "";?><br/><?php echo $btnExportCSV; ?></div>
    <table class="table-list">
        <tr>
            <th><input type="checkbox" id="maincheckbox" name="maincheckbox" value="yes" onClick="Check(document.frmbooktransfer.chkbooktrans)"/></th>
             <th>Card Value</th>
            <th>Game Name</th>
            <th>Book Size</th>
            <th>Game Number</th>
            <th>Game Batch</th>
            <th>Book Number</th>
            <th>Status</th>
            <th>Assigned To</th>
            <th>Invoice Number</th>
        </tr>
        <?php if(count($booktranslist)): ?>
        <?php for($i = 0 ; $i < count($booktranslist) ; $i++):?>
        <?php ($i % 2) == 0 ? $class = "evenrow" : $class = "oddrow" ; ?>
        <tr class = "<?php echo $class?>">
            <td><input type="checkbox" id="chkbooktrans" name="chkbooktrans[]" value="<?php echo $booktranslist[$i]['ID']?>" onclick="enablebutton(document.frmbooktransfer.chkbooktrans);"/></td>
            <td><?php echo $booktranslist[$i]["CardPrice"];?></td>
            <td><?php echo $booktranslist[$i]["ProductName"];?></td>
            <td><?php echo $booktranslist[$i]["BookTicketCount"];?></td>
            <td><?php echo $booktranslist[$i]["GameNumber"];?></td>
            <td><?php echo $booktranslist[$i]["BatchID"];?></td>
            <td><?php echo $booktranslist[$i]["BookNumber"];?></td>
            <td><?php echo $booktranslist[$i]["Status"];?></td>
            <td><?php echo $booktranslist[$i]["Username"];?></td>
            <td><?php echo $booktranslist[$i]["InvoiceNumber"];?></td>
        </tr>
        <?php endfor;?>
        <?php else: ?>
        <tr class="no-record">
            <td colspan="7">No results to display</td>
        </tr>
        <?php endif; ?>
    </table>
	<div style="float: right;">
	<?php echo $btntransfer; ?>
	<br/>
    </div>
    <?php endif;?>
</div>
</form>
<div class="content-page"><div class="form-page"><?php echo isset($pgbooktransfer) ? $pgbooktransfer : "";?></div></div>
<?php include("footer.php");?>