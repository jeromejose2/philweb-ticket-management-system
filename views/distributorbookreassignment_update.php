<?php
/*
 * Created By: Arlene R. Salazar on 10-06-2011
 * Modified By: Noel Antonio on 01-14-2013
 * Purpose: For book transfer update
 */
require_once ("init.inc.php");
$pagesubmenuid = 38;
include_once("../controller/managesession.php");
include("../controller/distributorbookreassignment_updateprocess.php");
?>
<?php include("header.php");?>
<script language="javascript" type="text/javascript">
 $(document).ready(function(){
        $('input[type=text],[type=password]').bind('cut copy paste', function (e) {
            e.preventDefault();
        });
    });
</script>
<div id="fade" class="black_overlay"></div>
<!-- POP UP FOR MESSAGES -->
<div id="light2" class="white_content">
    <div id="title2" class="light-title"></div>
    <div id="msg2" class="light-message"></div>
    <div id="button2" class="light-button">
        <input type="button" onclick="javascript: document.getElementById('light2').style.display='none';document.getElementById('fade').style.display='none';" value="Okay"/>
    </div>
	<div class="light-footer"></div>
</div>
<!-- POP UP FOR MESSAGES -->
<!-- POP UP FOR MESSAGES -->
<div id="light4" class="white_content">
    <div id="title4" class="light-title"></div>
    <div id="msg4" class="light-message"></div>
    <div id="button4" class="light-button">
        <?php echo $btnError;?>
    </div>
	<div class="light-footer"></div>
</div>
<!-- POP UP FOR MESSAGES -->
<form name="frmbooktransupdate" method="post">
    <div class="titleCont">
        <div class="titleCont_left"></div>
        <div class="titleCont_body">Book Reassignment (New)</div>
        <div class="titleCont_right"></div>
    </div>

<div class="content-page">
        <label style="font-weight: bold; font-size: 16px;"><u>Assignment Information</u></label>
        <br/>
        <div>
            Currently Assigned From: <?php echo $assignedtouname;?>
            <br/>
            Transfer Items To: <?php echo $ddlassingedto;?> &nbsp&nbsp New Invoice Number: <?php echo $txtInvoiceNo;?>
        </div>
    <table class="table-list">
        <tr>
            <th>Card Value</th>
            <th>Game Name</th>
            <th>Book Size</th>
            <th>Game Number</th>
            <th>Book Number</th>
            <th>Status</th>
            <th>Invoice Number</th>
        </tr>
        <?php if(count($bookslist) > 0):?>
        <?php for($i = 0 ; $i < count($bookslist) ; $i++):?>
        <?php ($i % 2) == 0 ? $class = "evenrow" : $class = "oddrow" ; ?>
        <tr class = "<?php echo $class?>">
            <td><?php echo $bookslist[$i]["CardPrice"]?></td>
            <td><?php echo $bookslist[$i]["Product"]?></td>
            <td><?php echo $bookslist[$i]["BookTicketCount"]?></td>
            <td><?php echo $bookslist[$i]["GameNumber"]?></td>
            <td><?php echo $bookslist[$i]["BookNumber"]?></td>
            <td><?php echo $bookslist[$i]["StatusName"]?></td>
            <td><?php echo $bookslist[$i]["InvoiceNumber"]?></td>
        </tr>
        <?php endfor;?>
        <?php else: ?>
        <tr class="no-record">
            <td colspan="4">No results to display</td>
        </tr>
        <?php endif; ?>
    </table>
    <div class="form-button">
        <?php echo $btnProcess;?> <?php echo $btnCancel;?>
    </div>
</div>
    <!-- POP UP FOR MESSAGES -->
    <div id="light3" class="white_content">
        <div id="title3" class="light-title">Administrator Confirmation</div>
        <div id="msg3" class="light-message">
            Please enter the administrator account to proceed.<br/><br/>
            Username :  <?php echo $txtusername;?> <br/>
            Password  &nbsp;:  <?php echo $txtpassword;?>
        </div>
        <div id="button3" class="light-button">
            <?php echo $btnConfirm;?>
            <input type="button" onclick="javascript: document.getElementById('light3').style.display='none';document.getElementById('fade').style.display='none';" value="Cancel"/>
        </div>
		<div class="light-footer"></div>
    </div>
    
    <div id="light31" class="white_content">
        <div id="title31" class="light-title">Book Reassignment Confirmation</div>
        <div id="msg31" class="light-message">Would you like to reassign the book(s)?</div>
        <div id="button3" class="light-button">
            <?php echo $btnyes;?>
            <input type="button" onclick="javascript: document.getElementById('light31').style.display='none';document.getElementById('fade').style.display='none';" value="NO"/>
        </div>
		<div class="light-footer"></div>
    </div>
    <!-- POP UP FOR MESSAGES -->    
    <?php if(isset($confirm)):?>
        <script>
            document.getElementById('light31').style.display = "block";
            document.getElementById('fade').style.display = "block";
        </script>
    <?php endif;?>
    <?php if(isset($errormsg)):?>
        <script>
            document.getElementById('title2').innerHTML = "<?php echo $errortitle;?>";
            document.getElementById('msg2').innerHTML = "<?php echo $errormsg;?>";
            document.getElementById('light2').style.display = "block";
            document.getElementById('fade').style.display = "block";
        </script>
    <?php endif;?>
    <?php if(isset($successtitle)):?>
        <script>
            document.getElementById('title2').innerHTML = "<?php echo $successtitle;?>";
            document.getElementById('msg2').innerHTML = "<?php echo $successmsg;?>";
            document.getElementById('button2').innerHTML = "<?php echo $btnOkay;?>";
            document.getElementById('light2').style.display = "block";
            document.getElementById('fade').style.display = "block";
        </script>
    <?php endif;?>
    <?php if(isset($logintitle)):?>
        <script>
            document.getElementById('title4').innerHTML = "<?php echo $logintitle;?>";
            document.getElementById('msg4').innerHTML = "<?php echo $loginmsg;?>";
            document.getElementById('light4').style.display = "block";
            document.getElementById('fade').style.display = "block";
        </script>
    <?php endif;?>
    <?php echo $hiddenassignedto;?>
    <?php echo $hiddenassignedtouname;?>
    <?php echo $hiddenbookids;?>
    <?php echo $hiddeninvoiceno;?>     
</form>
<?php include("footer.php");?>