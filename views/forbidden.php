<?php
/*
 * Author: Noel Antonio
 * Date Created: 2011-09-26
 * Company: Philweb Corporation
 */
include("../init.inc.php");
include_once("../controller/managesession.php");
?>

<html>
    <body>
        <link rel="stylesheet" type="text/css" href="../css/default.css" />
        <form action="forbidden.php" method="POST">
            <table border="0" cellspacing="0" cellpadding="0" class="container">
        <tr>
            <td colspan="2">
                <div class="header">
                    <div class="company_logo">
                        <img src="../images/philweb_logo.png" alt="" height ="95px" width="220px"/>
                  </div>
                </div>
            </td>
       </tr>
       <tr>
           <td colspan="2">
                <div class="project_logo" align="center">
                    <img src="../images/scratch_n_win_logo.png" alt="" height ="70px" width="140px"/>
                </div>
           </td>
       </tr>
          <tr>
            <td valign="top" class="content_container">
                <div class="content-page" style="text-align: center;color: #c50d0d;">
                    <br/><br/><br/>
                <h1>You are not allowed to view this page.</h1>
            </div> 
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="footer">Copyright 2011 PhilWeb Corporation</td>
       </tr>
            </table>
        </form>
    </body>
</html>