<?php
/*
 * Created By: Arlene R. Salazar on 09-14-2011
 * Purpose: For Game List
 */
//require_once("include/core/init.inc.php");
require_once("init.inc.php");
$pagesubmenuid = 18;
include_once("../controller/managesession.php");
include("../controller/gamelistprocess.php");
?>

<?php include("header.php"); ?>
<script language="javascript" type="text/javascript">
    function SelectedIndexChange(index)
    {
        selectedindex = document.getElementById("hdnSelectedProviderID");
        selectedindex.value = index;
        document.forms[0].submit();
    }
    function ChangePage(pagenum)
    {
        selectedindex = document.getElementById("pgSelectedPage");
        selectedindex.value = pagenum;
        document.forms[0].submit();
    }
</script>
<div class="titleCont">
            <div class="titleCont_left"></div>
            <div class="titleCont_body">Games</div>
            <div class="titleCont_right"></div>
</div>
    <div id="fade" class="black_overlay"></div>
    <!-- POP UP FOR MESSAGES -->
    <div id="light" class="white_content">
	<div id="title" class="light-title"></div>
	<div id="msg" class="light-message"></div>
	<div id="button" class="light-button"><input type="button" onclick="javascript: document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none';" value="OKAY"/></div>
        <div class="light-footer"></div>
    </div>
<!-- POP UP FOR MESSAGES -->
    <form name="frmGamelist" method="post">
        <?php echo $hiddenctr;?>
        <div class="content-page">
            <div class="search-container" style="width: 100%;">
                <div class="form-view">
                    <table>
                    <tr>
                        <td><?php echo $ddlProviders;?></td>
                        <td><?php echo $btnSubmit;?></td>
                    </tr>
                </table>
                </div>
                <div class="form-button"><?php echo $btnAddGame; ?></div>
            </div>
            <div class="form-page">
                <?php echo $pgTransactionHistory;?>
            </div>
        </div>
    </form>
    <form name="frmGameLists" method="post" action="updategame.php">
    <?php if(isset($gameslist)): ?>
    <?php echo $hiddengameid; ?>
        <div class="content-page">
    <table class="table-list">
        <tr>
	    <th>Edit</th>
            <th>Provider</th>
            <th>Game Number</th>
            <th>Game Name</th>
            <th>Card Value</th>
            <th>Book Size</th>
        </tr>
        <?php if(count($gameslist) > 0): ?>
        <?php for($i = 0 ; $i < count($gameslist) ; $i++): ?>
        <?php ($i % 2) == 0 ? $class = "evenrow" : $class = "oddrow"; ?>
        <tr class="<?php echo $class;?>">
	    <td><a href="#" onclick="javascript: return editGame(<?php echo $gameslist[$i]["GameID"]; ?>)">Edit</a></td>
            <td><?php echo $gameslist[$i]["Name"]; ?></td>
            <td><?php echo $gameslist[$i]["GameNumber"]; ?></td>
            <td><?php echo $gameslist[$i]["ProductName"]; ?></td>
            <td><?php echo $_CONFIG["currency"]." ".$gameslist[$i]["CardPrice"]; ?></td>
            <td><?php echo $gameslist[$i]["BookTicketCount"]; ?></td>
        </tr>
        <?php endfor; ?>
        <?php else: ?>
        <tr class="no-record">
            <td colspan="4">No result to display</td>
        </tr>
        <?php endif; ?>
    </table>
    <?php endif; ?>
    <div class="form-page">
        <?php echo $pgTransactionHistory;?>
    </div>
    </div>
    </form>
<?php include("footer.php"); ?>