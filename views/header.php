<?php
/*
 * @author
 * Purpose   : view for header
 */
require_once("init.inc.php");
?>

<?php
$headerinfo = "";
if((($pagesubmenuid > 1) && ($pagesubmenuid < 23)) || (($pagesubmenuid > 23) && ($pagesubmenuid < 28)) || ($pagesubmenuid > 28))
{
    unset($_SESSION['srchaccttype']);
    unset($_SESSION['srchuname']);
    unset($_SESSION['srchstatus']);
    unset($_SESSION['prodaccttype']);
}

for ($i = 0; $i < count($javascripts); $i++)
{
    $js = $javascripts[$i];
    $headerinfo .= "<script language='javascript' type='text/javascript' src='$js'></script>";
}
for ($i = 0; $i < count($stylesheets); $i++)
{
    $css = $stylesheets[$i];
    $headerinfo .= "<link rel='stylesheet' type='text/css' media='screen' href='$css' />";
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>Ticket Management System</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <script src="jscripts/jquery-1.5.2.min.js" type="text/javascript"></script>
        <link rel="stylesheet" href="css_navigation/navigation.css" />
        <?php echo $headerinfo; ?>
        <script type="text/javascript">
        function preventBackandForward()
        {
          window.history.forward();
        }
        preventBackandForward();
        window.inhibited_load=preventBackandForward;
        window.onpageshow=function(evt){if(evt.persisted)preventBackandForward();};
        window.inhibited_unload=function(){void(0);};
        </script> 
		<script language="javascript">
            $(document).ready(function(){
                document.getElementById('fade').style.height = $(document).height();
            });
        </script>
    </head>
    <body ondragstart="return false;" ondrop="return false;">
       <table border="0" cellspacing="0" cellpadding="0" class="container">
        <tr>
            <td colspan="2">
                <div class="header">
                    <div class="company_logo">
                        <img src="images/philweb_logo.png" alt="" height ="95px" width="220px"/>
                  </div>
                  <div class="acct">
                        <div class="acct_left"></div>
                        <table class="acct_body"><tr><td><b><?php echo $_SESSION['uname']; ?></b> | <a href="logout.php" class="loginheader">Logout</a></td></tr></table>
                        <div class="acct_right"></div>
                  </div>
                </div>
            </td>
       </tr>
       <tr>
           <td colspan="2">
                <div class="project_logo" align="center">
                    <img src="images/scratch_n_win_logo.png" alt="" height ="70px" width="140px"/>
                </div>
           </td>
       </tr>
       <tr class="content_container">
          <td valign="top" width="230px">
              <div class="leftnavigationcontainer">
              <div class="leftNavTop"></div>
              <div class="leftNavBody">
                  <?php include("navigation.php"); ?>
              </div>
              <div class="leftNavBottom"></div>
              </div>
          </td>
          <td valign ="top" class="maincontainer">
              <div id="mainContentBox">
                  <div class="rightContainerTop">
                      <img src="images/mainContainer_top.png" height="11px" width="100%"/>
                  </div>
                  <div class="rightContainerBody">