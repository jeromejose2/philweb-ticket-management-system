<?php
/*****************************
 * Author: Roger Sanchez
 * Date Created: 01 17, 11
 * Company: Philweb
 *****************************/

require_once("include/core/init.inc.php");
global $_CONFIG;
$_CONFIG["EnableMagicQuotes"] = true;
$_CONFIG["appdir"] = dirname(__FILE__) . "/";
$_CONFIG["apptemplatedir"] = dirname(__FILE__) . "/templates/";
$_CONFIG["appcontrollerdir"] = dirname(__FILE__) . "../controller/";
$_CONFIG["appviewdir"] = dirname(__FILE__);
$_CONFIG["logdir"] = dirname(__FILE__) . "/logs/";

/* Added by ARS 01/26/12 */
$_CONFIG["FPDF_FONTPATH"] = $librarydir . "fpdf/font";

/* Added by NDA 04/20/2012 */
$_CONFIG["currency"] = "$";         // Currency can be "$", "KHR" etc.
$_CONFIG["xmlnaming"] = "FC";         // XML Naming can be "FT", "FC" etc.
$_CONFIG["csvnaming"] = "CM";         // CSV Naming can be TL, CM etc.
$_CONFIG["country"] = "Cambodia";     // Country name

/* EMAIL RECIPIENTS */
$email[] = "ndantonio@philweb.com.ph";
$email[] = "mccalderon@philweb.com.ph";
$_CONFIG["email"] = $email;

/* EMAIL NAME RECIPIENTS */
$name[] = "Noel Antonio";
$name[] = "Tere Calderon";
$_CONFIG["name"] = $name;


/*TIME ZONE*/
$_CONFIG["useapplicationdatetime"] = true;
$_CONFIG["applicationtimezone"] = "Asia/Dili";

/*
if (URL::CurrentPage() != "login.php")
{
    include_once("../controller/managesession.php");
}
*/
?>
