//login
function checklogin()
{
    var uname = document.getElementById("txtUsername").value;
    var pword = document.getElementById("txtPassword").value;

    uname = uname.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
    pword = pword.replace(/^\s\s*/, '').replace(/\s\s*$/, '');

    if (uname.length == 0)
    {
        document.getElementById('txtUsername').focus();
        document.getElementById('title').innerHTML= 'ERROR';
        document.getElementById('msg').innerHTML = 'Please input username.';
        document.getElementById('light').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    if (uname.length < 8)
    {
        document.getElementById('txtUsername').focus();
        document.getElementById('title').innerHTML= 'ERROR';
        document.getElementById('msg').innerHTML = 'Username must not be less than 8 characters.';
        document.getElementById('light').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    if (pword.length == 0)
    {
        document.getElementById('txtPassword').focus();
        document.getElementById('title').innerHTML= 'ERROR';
        document.getElementById('msg').innerHTML = 'Please input password.';
        document.getElementById('light').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    if (pword.length < 8)
    {
        document.getElementById('txtPassword').focus();
        document.getElementById('title').innerHTML= 'ERROR';
        document.getElementById('msg').innerHTML = 'Password must not be less than 8 characters.';
        document.getElementById('light').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;

    }    
    
    return true;
}

function openforgotpw()
{
    document.getElementById('title2').innerHTML= 'Reset Password';
    document.getElementById('msg2').innerHTML = 'You are about to reset your current password. Please enter your registered e-mail address.';
    document.getElementById('light2').style.display = 'block';
    document.getElementById('fade').style.display = 'block';
    return false;
}

function closelightbox()
{
    var email = document.getElementById('txtEmail').value;

    if(email.replace(/^\s\s*/, '').replace(/\s\s*$/, '').length == 0)
    {
        document.getElementById('title2').innerHTML = 'Provide Email Address!';
        document.getElementById('msg2').innerHTML = ' Please enter your registered e-mail address.';
        document.getElementById('light2').style.display='block';
        return false;
    }
    return true;
    /*document.getElementById('light2').style.display='none';
    document.getElementById('fade').style.display='none';*/
}

function checkaudittrail()
{
    var sDate = document.getElementById("txtDateFrom").value;
    if (sDate.length == 0)
    {
        document.getElementById('title').innerHTML= 'ERROR!';
        document.getElementById('msg').innerHTML = 'Please enter the date';
        document.getElementById('light').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    return true;
}

/*
function loadgames()
{
    document.getElementById('hidProdID').value = document.getElementById('ddlProduct').options[document.getElementById('ddlProduct').selectedIndex].value;
    alert(document.getElementById('hidProdID').value);
}
*/
function checkaddproduct()
{
    var provider = document.getElementById('ddlProviders').options[document.getElementById('ddlProviders').selectedIndex].value;
    var name = document.getElementById("txtName").value;
    //var desc = document.getElementById("txtDescription").value;

    name = name.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
    //desc = desc.replace(/^\s\s*/, '').replace(/\s\s*$/, '');

    if(provider == 0)
    {
        document.getElementById('ddlProviders').focus();
        document.getElementById('title').innerHTML= 'ERROR';
        document.getElementById('msg').innerHTML = 'Please select provider.';
        document.getElementById('light').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    if (name.length == 0)
    {
        document.getElementById('txtName').focus();
        document.getElementById('title').innerHTML= 'ERROR';
        document.getElementById('msg').innerHTML = 'Product name must not be blank.';
        document.getElementById('light').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    /*if (desc.length == 0)
    {
        document.getElementById('txtDescription').focus();
        document.getElementById('title').innerHTML= 'ERROR';
        document.getElementById('msg').innerHTML = 'Product description must not be blank.';
        document.getElementById('light').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }*/
    return true;
   
}

function checkupdateproduct()
{
    var provider = document.getElementById('ddlProviders').options[document.getElementById('ddlProviders').selectedIndex].value;
    var name = document.getElementById("txtName").value;
    //var desc = document.getElementById("txtDescription").value;

    name = name.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
    //desc = desc.replace(/^\s\s*/, '').replace(/\s\s*$/, '');

    if(provider == 0)
    {
        document.getElementById('ddlProviders').focus();
        document.getElementById('title').innerHTML= 'ERROR';
        document.getElementById('msg').innerHTML = 'Please select provider.';
        document.getElementById('light').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    if (name.length == 0)
    {
        document.getElementById('txtName').focus();
        document.getElementById('title').innerHTML= 'ERROR';
        document.getElementById('msg').innerHTML = 'Product name must not be blank.';
        document.getElementById('light').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    /*if (desc.length == 0)
    {
        document.getElementById('txtDescription').focus();
        document.getElementById('title').innerHTML= 'ERROR';
        document.getElementById('msg').innerHTML = 'Product description must not be blank.';
        document.getElementById('light').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }*/
    return true;
   
}

function checkaddprovider()
{
    var name = document.getElementById("txtName").value;
    var address = document.getElementById('txtAddress').value;
    var contactnum= document.getElementById('txtContactNumber').value;
    //var desc = document.getElementById("txtDescription").value;

    name = name.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
    address = address.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
    contactnum = contactnum.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
    //desc = desc.replace(/^\s\s*/, '').replace(/\s\s*$/, '');

    if (name.length == 0)
    {
        document.getElementById('txtName').focus();
        document.getElementById('title').innerHTML= 'ERROR';
        document.getElementById('msg').innerHTML = 'Please fill in all the required fields.';
        document.getElementById('light').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    if (contactnum.length == 0)
    {
        document.getElementById('txtContactNumber').focus();
        document.getElementById('title').innerHTML= 'ERROR';
        document.getElementById('msg').innerHTML = 'Please fill in all the required fields.';
        document.getElementById('light').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    if (address.length == 0)
    {
        document.getElementById('txtAddress').focus();
        document.getElementById('title').innerHTML= 'ERROR';
        document.getElementById('msg').innerHTML = 'Please fill in all the required fields.';
        document.getElementById('light').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    /*if (desc.length == 0)
    {
        document.getElementById('txtDescription').focus();
        document.getElementById('title').innerHTML= 'ERROR';
        document.getElementById('msg').innerHTML = 'Provider description must not be blank.';
        document.getElementById('light').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }*/
    return true;
   
}

function checkupdateprovider()
{
    //var name = document.getElementById("txtName").value;
    var address = document.getElementById('txtAddress').value;
    var contactnum= document.getElementById('txtContactNumber').value;
    //var desc = document.getElementById("txtDescription").value;

    //name = name.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
    //desc = desc.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
	address = address.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
    contactnum = contactnum.replace(/^\s\s*/, '').replace(/\s\s*$/, '');

    /*if (name.length == 0)
    {
        document.getElementById('txtName').focus();
        document.getElementById('title').innerHTML= 'ERROR';
        document.getElementById('msg').innerHTML = 'Provider name must not be blank.';
        document.getElementById('light').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }*/
    if (contactnum.length == 0)
    {
        document.getElementById('txtContactNumber').focus();
        document.getElementById('title').innerHTML= 'ERROR';
        document.getElementById('msg').innerHTML = 'Please fill in all the required fields.';
        document.getElementById('light').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    if (address.length == 0)
    {
        document.getElementById('txtAddress').focus();
        document.getElementById('title').innerHTML= 'ERROR';
        document.getElementById('msg').innerHTML = 'Please fill in all the required fields.';
        document.getElementById('light').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    /*if (desc.length == 0)
    {
        document.getElementById('txtDescription').focus();
        document.getElementById('title').innerHTML= 'ERROR';
        document.getElementById('msg').innerHTML = 'Provider description must not be blank.';
        document.getElementById('light').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
      return false;
    }*/
    return true;
   
}

function checkvalidation()
{
    var valno = document.getElementById("txtValidationNumber").value;    
    var bookno = document.getElementById("txtBookNumber").value;
    //var gametype = document.getElementById('ddlGameType').options[document.getElementById('ddlGameType').selectedIndex].value;
    //var prodid = document.getElementById('ddlProduct').options[document.getElementById('ddlProduct').selectedIndex].value;
    
    valno = valno.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
    bookno = bookno.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
    
    /*
    if (gametype == 0)
    {
        document.getElementById('ddlGameType').focus();
        document.getElementById('title').innerHTML= 'ERROR';
        document.getElementById('msg').innerHTML = 'Please select game type.';
        document.getElementById('light').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    if (prodid == 0)
    {
        document.getElementById('ddlProduct').focus();
        document.getElementById('title').innerHTML= 'ERROR';
        document.getElementById('msg').innerHTML = 'Please select product name.';
        document.getElementById('light').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    */
    if (bookno.length == 0)
    {
        document.getElementById('txtBookNumber').focus();
        document.getElementById('title').innerHTML= 'ERROR';
        document.getElementById('msg').innerHTML = 'Please input book ticket number.';
        document.getElementById('light').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    if (bookno.length < 12)
    {
        document.getElementById('txtBookNumber').focus();
        document.getElementById('title').innerHTML= 'ERROR';
        document.getElementById('msg').innerHTML = 'Ticket number must not be less than 12 characters.';
        document.getElementById('light').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    if (valno.length == 0)
    {
        document.getElementById('txtValidationNumber').focus();
        document.getElementById('title').innerHTML= 'ERROR';
        document.getElementById('msg').innerHTML = 'Please input validation number.';
        document.getElementById('light').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    if (valno.length < 12)
    {
        document.getElementById('txtValidationNumber').focus();
        document.getElementById('title').innerHTML= 'ERROR';
        document.getElementById('msg').innerHTML = 'Validation number must not be less than 12 characters.';
        document.getElementById('light').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    if (document.getElementById('hidIsValid').value == 'invalid')
    {
        document.getElementById('txtValidationNumber').focus();
        document.getElementById('title').innerHTML= 'ERROR';
        document.getElementById('msg').innerHTML = 'Validation number is invalid.';
        document.getElementById('light').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    
    return true;   
}

function checkcreatesession()
{
    var cardvalue = document.getElementById('ddlcardvalue').options[document.getElementById('ddlcardvalue').selectedIndex].value;
    var gamename = document.getElementById('ddlgamename').options[document.getElementById('ddlgamename').selectedIndex].value;
    var gamenum = document.getElementById('ddlgamenum').options[document.getElementById('ddlgamenum').selectedIndex].value;
    var account = document.getElementById('ddlUsername').options[document.getElementById('ddlUsername').selectedIndex].value;

    if (cardvalue == 0)
    {
        document.getElementById('ddlgamename').focus();
        document.getElementById('title').innerHTML= 'ERROR';
        document.getElementById('msg').innerHTML = 'Please select card value.';
        document.getElementById('light').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    if (gamename == 0)
    {
        document.getElementById('ddlgamename').focus();
        document.getElementById('title').innerHTML= 'ERROR';
        document.getElementById('msg').innerHTML = 'Please select game name.';
        document.getElementById('light').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    if (gamenum == 0)
    {
        document.getElementById('ddlgamenum').focus();
        document.getElementById('title').innerHTML= 'ERROR';
        document.getElementById('msg').innerHTML = 'Please select game number.';
        document.getElementById('light').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    if (account == 0)
    {
        document.getElementById('ddlUsername').focus();
        document.getElementById('title').innerHTML= 'ERROR';
        document.getElementById('msg').innerHTML = 'Please select an account.';
        document.getElementById('light').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    
    return true;  
}

function checkticketcancellation()
{
    var bookno = document.getElementById("txtBookNumber").value;
    var cancelmode = document.getElementById("ddlModeSelect").value;
    var remarks = document.getElementById("txtRemarks").value;
    
    bookno = bookno.replace(/^\s\s*/, '').replace(/\s\s*$/, '');

    remarks = remarks.replace(/^\s\s*/, '').replace(/\s\s*$/, '');

    if (bookno.length == 0)
    {
        document.getElementById('txtBookNumber').focus();
        document.getElementById('title2').innerHTML= 'ERROR!';
        document.getElementById('msg2').innerHTML = 'Please input tracking number.';
        document.getElementById('light2').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }  
    if (bookno.length < 12)
    {
        document.getElementById('txtBookNumber').focus();
        document.getElementById('title2').innerHTML= 'ERROR!';
        document.getElementById('msg2').innerHTML = 'Ticket/Book Number must not be less than 12 characters.';
        document.getElementById('light2').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }

    /*var temp_end_index = bookno.length;
    var temp_beginning_index = temp_end_index-3;
    var test_string = bookno.substring(temp_beginning_index, temp_end_index);
    if (test_string > 200)
    {
        document.getElementById('title2').innerHTML= 'ERROR!';
        document.getElementById('msg2').innerHTML = 'Ticket/Book Number is invalid.';
        document.getElementById('light2').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;

    }*/

    if (cancelmode == 0)
    {
        document.getElementById('title2').innerHTML= 'ERROR!';
        document.getElementById('msg2').innerHTML = 'Please input cancel mode.';
        document.getElementById('light2').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;

    }
    if (remarks.length == 0)
    {
        document.getElementById('txtBookNumber').focus();
        document.getElementById('title2').innerHTML= 'ERROR!';
        document.getElementById('msg2').innerHTML = 'Please input the remarks.';
        document.getElementById('light2').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    if (remarks.length >150)
    {
        document.getElementById('txtBookNumber').focus();
        document.getElementById('title2').innerHTML= 'ERROR!';
        document.getElementById('msg2').innerHTML = 'Remarks must only have a maximum of 150 characters.';
        document.getElementById('light2').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }

    return true;       
}

function getvalue()
{
	var okbutton = document.getElementById('btnOkay').value;
	return true;
	
}

function donothing()
{
	var cancelbutton = document.getElementById('btnCancel').value;
	return true;
}
function specialkeys(e)
{
    var k;
    document.all ? k = e.keyCode : k = e.which;
    return ((k >= 64 && k < 91) || (k > 96 && k < 123) || k == 8 || e.keyCode == 9 || k == 32 || k == 46 || (e.keyCode > 36 && e.keyCode <= 40) || (k >= 48 && k <= 57));
}

function alphanumeric(e)
{
    var k;
    document.all ? k = e.keyCode : k = e.which;
    return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || e.keyCode == 9 || k == 32 || (e.keyCode > 36 && e.keyCode <= 40) || (k >= 48 && k <= 57));
}
/*Added By JFJ 04/04-2013*/
function numeric(e)
{
    var k;
    document.all ? k = e.keyCode : k = e.which;
    return ((k > 47 && k < 58) ||  (k == 8 || k == 0));
}
/*Added By ARS 08/31/2011*/
function checkuserdata()
{
    var username = document.getElementById('txtusername').value;
    var password = document.getElementById('txtpassword').value;
    var cpassword = document.getElementById('txtcpassword').value;
    var fname = document.getElementById('txtfname').value;
    var email = document.getElementById('txtemail').value;
    var acctype = document.getElementById('ddlaccttype').options[document.getElementById('ddlaccttype').selectedIndex].value;
    var contactnum = document.getElementById('txtcontactnum').value;
    var address = document.getElementById('txtaddress').value;
    var filter = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    
    if(username.replace(/^\s\s*/, '').replace(/\s\s*$/, '').length == "")
    {
        document.getElementById('txtusername').focus();
        document.getElementById('title').innerHTML= 'ERROR';
        document.getElementById('msg').innerHTML = 'Please provide an information for username.';
        document.getElementById('light').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    if(username.replace(/^\s\s*/, '').replace(/\s\s*$/, '').length < 8)
    {
        document.getElementById('txtusername').focus();
        document.getElementById('title').innerHTML= 'ERROR';
        document.getElementById('msg').innerHTML = 'Please provide username with more than 7 characters.';
        document.getElementById('light').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    if(acctype == 0)
    {
        document.getElementById('ddlaccttype').focus();
        document.getElementById('title').innerHTML= 'ERROR';
        document.getElementById('msg').innerHTML = 'Please provide an information for account type.';
        document.getElementById('light').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    if(password.replace(/^\s\s*/, '').replace(/\s\s*$/, '').length == "")
    {
        document.getElementById('txtpassword').focus();
        document.getElementById('title').innerHTML= 'ERROR';
        document.getElementById('msg').innerHTML = 'Please provide an information for password.';
        document.getElementById('light').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    if(password.length < 8)
    {
        document.getElementById('txtpassword').focus();
        document.getElementById('title').innerHTML= 'ERROR';
        document.getElementById('msg').innerHTML = 'Please provide password with more than 7 characters.';
        document.getElementById('light').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    if(cpassword.replace(/^\s\s*/, '').replace(/\s\s*$/, '').length == "")
    {
        document.getElementById('txtcpassword').focus();
        document.getElementById('title').innerHTML= 'ERROR';
        document.getElementById('msg').innerHTML = 'Please provide an information for confirm password.';
        document.getElementById('light').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    if(cpassword.replace(/^\s\s*/, '').replace(/\s\s*$/, '').length < 8)
    {
        document.getElementById('txtcpassword').focus();
        document.getElementById('title').innerHTML= 'ERROR';
        document.getElementById('msg').innerHTML = 'Please provide password with more than 7 characters.';
        document.getElementById('light').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    if(password != cpassword)
    {
        document.getElementById('txtcpassword').focus();
        document.getElementById('title').innerHTML= 'ERROR';
        document.getElementById('msg').innerHTML = 'Please provide the same password.';
        document.getElementById('light').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    if(fname.replace(/^\s\s*/, '').replace(/\s\s*$/, '').length == "")
    {
        document.getElementById('txtfname').focus();
        document.getElementById('title').innerHTML= 'ERROR';
        document.getElementById('msg').innerHTML = 'Please provide an information for full name.';
        document.getElementById('light').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    if(address.replace(/^\s\s*/, '').replace(/\s\s*$/, '').length == 0)
    {
        document.getElementById('txtaddress').focus();
        document.getElementById('title').innerHTML= 'ERROR';
        document.getElementById('msg').innerHTML = 'Please provide an information for address.';
        document.getElementById('light').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    if(contactnum.replace(/^\s\s*/, '').replace(/\s\s*$/, '').length == 0)
    {
        document.getElementById('txtcontactnum').focus();
        document.getElementById('title').innerHTML= 'ERROR';
        document.getElementById('msg').innerHTML = 'Please provide an information for contact number.';
        document.getElementById('light').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email))
    {
        return (true);
    }
    else
    {
        document.getElementById('txtemail').focus();
        document.getElementById('title').innerHTML= 'ERROR';
        document.getElementById('msg').innerHTML = 'Please enter a valid input for email address.';
        document.getElementById('light').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
//    if (!filter.test(email))
//    {
//        document.getElementById('txtemail').focus();
//        document.getElementById('title').innerHTML= 'ERROR 1';
//        document.getElementById('msg').innerHTML = 'Please enter a valid input for email address.';
//        document.getElementById('light').style.display = 'block';
//        document.getElementById('fade').style.display = 'block';
//        return false;
//    }
//    if(email.replace(/^\s\s*/, '').replace(/\s\s*$/, '').length == "")
//    {
//        document.getElementById('txtemail').focus();
//        document.getElementById('title').innerHTML= 'ERROR';
//        document.getElementById('msg').innerHTML = 'Please provide an information for email.';
//        document.getElementById('light').style.display = 'block';
//        document.getElementById('fade').style.display = 'block';
//        return false;
//    }
//    if(!checkemail(email))
//    {
//        document.getElementById('txtemail').focus();
//        document.getElementById('title').innerHTML= 'ERROR 2';
//        document.getElementById('msg').innerHTML = 'Please enter a valid input for email address.';
//        document.getElementById('light').style.display = 'block';
//        document.getElementById('fade').style.display = 'block';
//        return false;
//    }
    
    return true;
}

function checkproviderdata()
{
    var provname = document.getElementById('txtprovname').value;
    var provdesc = document.getElementById('txtprovdesc').value;

    if(provname.replace(/^\s\s*/, '').replace(/\s\s*$/, '').length == 0)
    {
        alert("Please provide project name");
        document.getElementById('txtprovname').focus();
        return false;
    }
    if(provdesc.replace(/^\s\s*/, '').replace(/\s\s*$/, '').length == 0)
    {
        alert("Please provide project description");
        document.getElementById('txtprovdesc').focus();
        return false;
    }
    var addprovider = confirm("Are you sure you want to add provider?");
    if(addprovider)
        return true;
    else
        return false;
}

function checkupdatedata()
{
    //var username = document.getElementById('txtusername').value;
    var fname = document.getElementById('txtfname').value;
    var email = document.getElementById('txtemail').value;
    var acctype = document.getElementById('ddlaccttype').options[document.getElementById('ddlaccttype').selectedIndex].value;
    var contactnum = document.getElementById('txtcontactnum').value;
    var address = document.getElementById('txtaddress').value;

    //if(username.replace(/^\s\s*/, '').replace(/\s\s*$/, '').length == "")
    /*{
        document.getElementById('txtusername').focus();
        document.getElementById('title3').innerHTML= 'ERROR';
        document.getElementById('msg3').innerHTML = 'Please provide username.';
        document.getElementById('light3').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }*/
    //if(username.replace(/^\s\s*/, '').replace(/\s\s*$/, '').length < 8)
    /*{
        document.getElementById('txtusername').focus();
        document.getElementById('title3').innerHTML= 'ERROR';
        document.getElementById('msg3').innerHTML = 'Please provide username with more than 8 characters.';
        document.getElementById('light3').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }*/
    if(acctype == 0)
    {
        document.getElementById('ddlaccttype').focus();
        document.getElementById('title3').innerHTML= 'ERROR';
        document.getElementById('msg3').innerHTML = 'Please provide an information for account type.';
        document.getElementById('light3').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    if(fname.replace(/^\s\s*/, '').replace(/\s\s*$/, '').length == "")
    {
        document.getElementById('txtfname').focus();
        document.getElementById('title3').innerHTML= 'ERROR';
        document.getElementById('msg3').innerHTML = 'Please provide an information for full name.';
        document.getElementById('light3').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    if(address.replace(/^\s\s*/, '').replace(/\s\s*$/, '').length == 0)
    {
	document.getElementById('txtaddress').focus();
        document.getElementById('title3').innerHTML= 'ERROR';
        document.getElementById('msg3').innerHTML = 'Please provide an information for address.';
        document.getElementById('light3').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    if(contactnum.replace(/^\s\s*/, '').replace(/\s\s*$/, '').length == 0)
    {
        document.getElementById('txtcontactnum').focus();
        document.getElementById('title3').innerHTML= 'ERROR';
        document.getElementById('msg3').innerHTML = 'Please provide an information for contact number.';
        document.getElementById('light3').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email))
    {
        return (true);
    }
    else
    {
        document.getElementById('txtemail').focus();
        document.getElementById('title3').innerHTML= 'ERROR';
        document.getElementById('msg3').innerHTML = 'Please enter a valid input for email address.';
        document.getElementById('light3').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
//    if(email.replace(/^\s\s*/, '').replace(/\s\s*$/, '').length == "")
//    {
//        document.getElementById('txtemail').focus();
//        document.getElementById('title3').innerHTML= 'ERROR';
//        document.getElementById('msg3').innerHTML = 'Please provide email.';
//        document.getElementById('light3').style.display = 'block';
//        document.getElementById('fade').style.display = 'block';
//        return false;
//    }
//	if(email.replace(/^\s\s*/, '').replace(/\s\s*$/, '').length != "")
//    {
//		if(!checkemail(email))
//		{
//		    document.getElementById('txtemail').focus();
//		    document.getElementById('title3').innerHTML= 'ERROR';
//		    document.getElementById('msg3').innerHTML = 'Please provide valid email address.';
//		    document.getElementById('light3').style.display = 'block';
//		    document.getElementById('fade').style.display = 'block';
//		    return false;
//		}
//	}

    return true;
}

function checkemail(email)
{
    var str=email
    var filter=/^.+@.+\..{2,3}$/

    if (filter.test(str))
        return true;
    else
        return false;
}

function isNumberKey(evt)
{
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
    return false;

    return true;
}

function isAlphaKey(evt)
{
    var charCode = (evt.which) ? evt.which : event.keyCode
    if ((charCode > 32 && charCode <65) || (charCode < 97 && charCode > 90) || (charCode < 128 && charCode > 122))
        return false;

    return true;
}

function loadDetails(id)
{
    document.getElementById('loading').style.display = 'block';
    document.getElementById('fade').style.display = 'block';
    $.ajax({
          url     : "../controllers/ajaxqueryprocess.php",
          dataType: 'json',
          data    : {"id" : id, "purpose" : "viewacctdtls"},
          type    : "POST",
          success : function(data){
            var uname = data.Username;
            var name = data.Name;
            var fname = data.Firstname;
            var mname = data.Middlename;
            var lname = data.Lastname;
            var email = data.Email;
            var stat = data.Status;
            var ID = data.ID;
            var button = '<input type="button" value="Edit" onclick="javascript: return editDtls(' + ID + ');"/>';

            document.getElementById("acctuname").innerHTML = uname;
            document.getElementById("acctfname").innerHTML = fname;
            document.getElementById("acctmname").innerHTML = mname;
            document.getElementById("acctlname").innerHTML = lname;
            document.getElementById("acctemail").innerHTML = email;
            document.getElementById("accttype").innerHTML = name;
            document.getElementById("acctstat").innerHTML = stat;
            document.getElementById("button").innerHTML = button;

            document.getElementById('loading').style.display = 'none';
            document.getElementById('light3').style.display = "block";
            document.getElementById('fade').style.display = "block";
          },
          error   : function(){
             alert('error');
          }
    });
}

function editDtls(id)
{
    document.getElementById('loading').style.display = 'block';
    document.getElementById('fade').style.display = 'block';
    $.ajax({
          url     : "../controllers/ajaxqueryprocess.php",
          dataType: 'json',
          data    : {"id" : id, "purpose" : "viewacctdtls"},
          type    : "POST",
          success : function(data){
            var uname = data.Username;
            var name = data.AccountType;
            var fname = data.Firstname;
            var mname = data.Middlename;
            var lname = data.Lastname;
            var email = data.Email;
            var stat = data.Status;
            var ID = data.ID;

            document.getElementById('hidden_editid').value = ID;
            document.getElementById("editacctuname").value = uname;
            document.getElementById("editacctfname").value = fname;
            document.getElementById("editacctmname").value = mname;
            document.getElementById("editacctlname").value = lname;
            document.getElementById("editacctemail").value = email;
            document.getElementById("ddleditaccttype").value = name;
            document.getElementById("ddleditstat").value = stat;

            document.getElementById('loading').style.display = 'none';
            document.getElementById('light4').style.display = "block";
            document.getElementById('fade').style.display = "block";
          },
          error   : function(){
            alert('error');
          }
    });
}

function checkusersearch()
{
    var uname = document.getElementById("txtsrchuname").value;
    var accttype = document.getElementById("ddlaccttype").options[document.getElementById('ddlaccttype').selectedIndex].value;

    /*if(accttype == "0")
    {
        document.getElementById('title5').innerHTML = "ERROR";
        document.getElementById('msg5').innerHTML = "You cannot proceed because no Account type is selected.";
        document.getElementById('light5').style.display = "block";
        document.getElementById('fade').style.display = "block";
        return false;
    }*/
    if(uname.replace(/^\s\s*/, '').replace(/\s\s*$/, '').length == 0)
    {
        document.getElementById('title5').innerHTML = "ERROR";
        document.getElementById('msg5').innerHTML = "Please provide information to search.";
        document.getElementById('light5').style.display = "block";
        document.getElementById('fade').style.display = "block";
        return false;
    }
    return true;
}

function checkretailersearch()
{
    var uname = document.getElementById("txtsrchuname").value;

    if(uname.replace(/^\s\s*/, '').replace(/\s\s*$/, '').length == 0)
    {
        document.getElementById('title5').innerHTML = "ERROR";
        document.getElementById('msg5').innerHTML = "Please provide information to search.";
        document.getElementById('light5').style.display = "block";
        document.getElementById('fade').style.display = "block";
        return false;
    }
    return true;
}

function redirectToAcctList()
{
    //window.location = "../views/accountslist.php";
    //window.location = "../views/template.php?page=acctlist";
    window.location = "acctlist.php";
}

function redirectToResetPass()
{
    //window.location = "../views/accountslist.php";
    //window.location = "../views/template.php?page=acctlist";
    window.location = "resetpasswordnoemail.php";
}

function redirectToAcctListRetailer()
{
    //window.location = "../views/accountslist.php";
    //window.location = "../views/template.php?page=acctlist";
    window.location = "acctlistretailer.php";
}

function redirectToProviderList()
{
    window.location = "providerlist.php";
}

function redirectToProductList()
{
    window.location = "productlist.php";
}

function redirectToGameList()
{
    window.location = "gamelist.php";
}

function redirectToInventoryXMLUpload()
{
    window.location = "inventoryxmlupload.php";
}

function checkChangeStatus()
{
    var status = document.getElementById("ddlstatus").options[document.getElementById('ddlstatus').selectedIndex].value;
    if(status == "")
    {
        document.getElementById('title3').innerHTML = "ERROR!";
        document.getElementById('msg3').innerHTML = "Please select new Status.";
        document.getElementById('light3').style.display = "block";
        document.getElementById('fade').style.display = "block";
        return false;
    }
    if(status == 1)
        document.getElementById('msg7').innerHTML = "Activate the account?";
    else
        document.getElementById('msg7').innerHTML = "Lock the account?";
    document.getElementById('light7').style.display = "block";
    document.getElementById('fade').style.display = "block";
    return true;
}

function resetPassword()
{
    document.getElementById('light8').style.display = "block";
    document.getElementById('fade').style.display = "block";
}

function changePassword()
{
    document.getElementById('chgestat').style.display = "block";
    document.getElementById('acctdtls').style.display = "none";
}

function cancelChangeStatus()
{
    document.getElementById("chgestat").style.display = "none";
    document.getElementById("acctdtls").style.display = "block";
}

function checkChangePassword()
{
    //var old_password = document.getElementById("txtoldpword").value;
    var new_password = document.getElementById("txtnewpword").value;
    var conf_new_password = document.getElementById("txtcfrmpword").value;
//    new_password = new_password.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
//    conf_new_password = conf_new_password.replace(/^\s\s*/, '').replace(/\s\s*$/, '');

    /*if(old_password.length == 0)
    {
        document.getElementById('title7').innerHTML = "ERROR!";
        document.getElementById('msg7').innerHTML = "Please supply old password.";
        document.getElementById('light7').style.display = "block";
        document.getElementById('fade').style.display = "block";
        return false;
    }*/
    if(new_password.length == 0)
    {
        document.getElementById('title7').innerHTML = "ERROR!";
        document.getElementById('msg7').innerHTML = "Please supply new password.";
        document.getElementById('light7').style.display = "block";
        document.getElementById('fade').style.display = "block";
        return false;
    }
    if(new_password.length < 8)
    {
        document.getElementById('title7').innerHTML = "ERROR!";
        document.getElementById('msg7').innerHTML = "New password must not be less than 8 characters.";
        document.getElementById('light7').style.display = "block";
        document.getElementById('fade').style.display = "block";
        return false;
    }
    if(conf_new_password.length == 0)
    {
        document.getElementById('title7').innerHTML = "ERROR!";
        document.getElementById('msg7').innerHTML = "Please supply confirm password.";
        document.getElementById('light7').style.display = "block";
        document.getElementById('fade').style.display = "block";
        return false;
    }
    if(conf_new_password.length < 8)
    {
        document.getElementById('title7').innerHTML = "ERROR!";
        document.getElementById('msg7').innerHTML = "Confirm password must not be less than 8 characters.";
        document.getElementById('light7').style.display = "block";
        document.getElementById('fade').style.display = "block";
        return false;
    }
    if(conf_new_password != new_password)
    {
        document.getElementById('title7').innerHTML = "ERROR!";
        document.getElementById('msg7').innerHTML = "Please supply the same password.";
        document.getElementById('light7').style.display = "block";
        document.getElementById('fade').style.display = "block";
        return false;
    }
    return true;
}

function editAccount(id)
{
    document.getElementById("hiddenaccountid").value = id;
    //document.frmAccountList.action = "updateuser.php";
    document.forms["frmAccountList"].submit();
    //document.forms["frmAcctList"].submit();
}

function editGame(id)
{
    document.getElementById("hidGameID").value = id;
    document.forms["frmGameList"].submit();
}

function checkAcctType()
{
	/*document.getElementById('fade').style.height =  document.body.offsetHeight;// + "px";
	alert(document.body.offsetHeight);*/
    var accttype = document.getElementById("ddlaccttype").options[document.getElementById('ddlaccttype').selectedIndex].value;
    var status = document.getElementById("ddlStatus").options[document.getElementById('ddlStatus').selectedIndex].value;
    if(accttype == "")
    {
        document.getElementById('title5').innerHTML = "ERROR!";
        document.getElementById('msg5').innerHTML = "Please select account type.";
        document.getElementById('light5').style.display = "block";
        document.getElementById('fade').style.display = "block";
        return false;
    }
    if(status == "")
    {
        document.getElementById('title5').innerHTML = "ERROR!";
        document.getElementById('msg5').innerHTML = "Please select status.";
        document.getElementById('light5').style.display = "block";
        document.getElementById('fade').style.display = "block";
        return false;
    }
    return true;
}

function checkProviderChngeStat()
{
    var stat = document.getElementById("ddlStatus").options[document.getElementById('ddlStatus').selectedIndex].value;
    if(stat == "")
    {
	document.getElementById('title').innerHTML = "ERROR!";
	document.getElementById('msg').innerHTML = "Please select new Status.";
	document.getElementById('light').style.display = "block";
	document.getElementById('fade').style.display = "block";
	return false;
    }
    return true;
}

function checkProviderSubmit()
{
    var provider = document.getElementById("ddlProviders").options[document.getElementById('ddlProviders').selectedIndex].value;
    if(provider == "")
    {
        document.getElementById('title').innerHTML = "ERROR!";
	document.getElementById('msg').innerHTML = "Please select provider.";
	document.getElementById('light').style.display = "block";
	document.getElementById('fade').style.display = "block";
        return false;
    }
    return true;
}

function checkAddGame()
{
    document.getElementById('hiddenCardValue').value = document.getElementById('txtCardValue').value;
    var provider = document.getElementById("ddlProvider").options[document.getElementById('ddlProvider').selectedIndex].value;
    if(provider == "")
    {
        document.getElementById('title').innerHTML = "ERROR!";
        document.getElementById('msg').innerHTML = "Please fill in all the required fields.";
        document.getElementById('light').style.display = "block";
        document.getElementById('fade').style.display = "block";
        return false;
    }
    
    var gamename = document.getElementById("ddlProduct").options[document.getElementById('ddlProduct').selectedIndex].value;
    var newgamename = document.getElementById('txtNewGameName').value;
    var gamenum = document.getElementById('txtGameNumber').value;
    var cardvalue = document.getElementById('txtCardValue').value;
    var bookcount = document.getElementById('txtBookSize').value;

    newgamename = newgamename.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
    if(provider == "")
    {
        document.getElementById('title').innerHTML = "ERROR!";
		document.getElementById('msg').innerHTML = "Please fill in all the required fields.";
		document.getElementById('light').style.display = "block";
		document.getElementById('fade').style.display = "block";
        return false;
    }
    if(gamenum == "")
    {
        document.getElementById('title').innerHTML = "ERROR!";
		document.getElementById('msg').innerHTML = "Please fill in all the required fields.";
		document.getElementById('light').style.display = "block";
		document.getElementById('fade').style.display = "block";
        return false;
    }
    if(cardvalue == 0 || cardvalue == "")
    {
        document.getElementById('title').innerHTML = "ERROR!";
		document.getElementById('msg').innerHTML = "Please fill in all the required fields.";
		document.getElementById('light').style.display = "block";
		document.getElementById('fade').style.display = "block";
        return false;
    }
    if((gamenum.replace(/^\s\s*/, '').replace(/\s\s*$/, '').length < 3) || (gamenum.replace(/^\s\s*/, '').replace(/\s\s*$/, '').length > 10))
    {
        document.getElementById('title').innerHTML = "ERROR!";
		document.getElementById('msg').innerHTML = "Please enter a valid input for game number.";
		document.getElementById('light').style.display = "block";
		document.getElementById('fade').style.display = "block";
        return false;
    }
    if(gamename == "")
    {
        if(newgamename.length == 0)
        {
            document.getElementById('title').innerHTML = "ERROR!";
            document.getElementById('msg').innerHTML = "Please fill in all the required fields.";
            document.getElementById('light').style.display = "block";
            document.getElementById('fade').style.display = "block";
            return false;
        }
    }
    if(bookcount == "")
    {
            document.getElementById('title').innerHTML = "ERROR!";
            document.getElementById('msg').innerHTML = "Please fill in all the required fields.";
            document.getElementById('light').style.display = "block";
            document.getElementById('fade').style.display = "block";
            return false;
    }
    if(parseInt(bookcount)<10 || parseInt(bookcount)>500)
    {
            document.getElementById('title').innerHTML = "ERROR!";
            document.getElementById('msg').innerHTML = "Book size should only be 10-500.";
            document.getElementById('light').style.display = "block";
            document.getElementById('fade').style.display = "block";
            return false;
    }    
    
    return true;
    //document.getElementById('light2').style.display = "block";
    //document.getElementById('fade').style.display = "block";
}

function checkUpdateGame()
{
    var gamenum = document.getElementById('txtGameNumber').value;
    //var gamename = document.getElementById("ddlProductName").options[document.getElementById('ddlProductName').selectedIndex].value;
    var gamename = document.getElementById('txtNewGameName').value;
    var cardvalue = document.getElementById('txtCardValue').value;

    gamenum = gamenum.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
    gamename = gamename.replace(/^\s\s*/, '').replace(/\s\s*$/, '');

    if(gamenum.length == 0)
    {
        document.getElementById('title').innerHTML = "ERROR!";
        document.getElementById('msg').innerHTML = "Please fill in all the required fields.";
        document.getElementById('light').style.display = "block";
        document.getElementById('fade').style.display = "block";
        return false;
    }
    if((gamenum.replace(/^\s\s*/, '').replace(/\s\s*$/, '').length < 3) || (gamenum.replace(/^\s\s*/, '').replace(/\s\s*$/, '').length > 10))
    {
        document.getElementById('title').innerHTML = "ERROR!";
		document.getElementById('msg').innerHTML = "Please enter a valid input for game number.";
		document.getElementById('light').style.display = "block";
		document.getElementById('fade').style.display = "block";
        return false;
    }
	if(gamename.length == 0)
    {
        document.getElementById('title').innerHTML = "ERROR!";
        document.getElementById('msg').innerHTML = "Please fill in all the required fields.";
        document.getElementById('light').style.display = "block";
        document.getElementById('fade').style.display = "block";
        return false;
    }
    if(cardvalue.length < 1)
    {
        document.getElementById('title').innerHTML = "ERROR!";
        document.getElementById('msg').innerHTML = "Please fill in all the required fields.";
        document.getElementById('light').style.display = "block";
        document.getElementById('fade').style.display = "block";
        return false;
    }    
    if(cardvalue == 0)
    {
        document.getElementById('title').innerHTML = "ERROR!";
        document.getElementById('msg').innerHTML = "Card value is invalid.";
        document.getElementById('light').style.display = "block";
        document.getElementById('fade').style.display = "block";
        return false;
    }

    return true;
    //document.getElementById('light2').style.display = "block";
    //document.getElementById('fade').style.display = "block";
}

function editGame(id)
{
    document.getElementById('hiddengameid').value = id;
    document.forms["frmGameLists"].submit();
}

/*Added by ARS 09-26-2011*/
function showloadingpage()
{
    var provider = document.getElementById("ddlProviders").options[document.getElementById('ddlProviders').selectedIndex].value;
//	var ponum = document.getElementById("txtPONumber").value;
//    var delnum = document.getElementById("txtDeliveryNumber").value;

    if(provider == "")
    {
        document.getElementById('title').innerHTML = "ERROR!";
        document.getElementById('msg').innerHTML = "Please select a provider.";
        document.getElementById('light').style.display = "block";
        document.getElementById('fade').style.display = "block";
        return false;
    }
//	else if(ponum.replace(/^\s\s*/, '').replace(/\s\s*$/, '').length == 0)
//    {
//        document.getElementById('title').innerHTML = "ERROR!";
//        document.getElementById('msg').innerHTML = "Please provide PO Number.";
//        document.getElementById('light').style.display = "block";
//        document.getElementById('fade').style.display = "block";
//        return false;
//    }
//    else if(delnum.replace(/^\s\s*/, '').replace(/\s\s*$/, '').length == 0)
//    {
//        document.getElementById('title').innerHTML = "ERROR!";
//        document.getElementById('msg').innerHTML = "Please provide Delivery Number.";
//        document.getElementById('light').style.display = "block";
//        document.getElementById('fade').style.display = "block";
//        return false;
//    }
    else
    {
        document.getElementById('loading').style.display = "block";
        document.getElementById('fade').style.display = "block";
        document.getElementById('loading-image').style.display = "block";
        return true;
    }
}

function showwinnersloadingpage()
{
    var provider = document.getElementById("ddlProviders").options[document.getElementById('ddlProviders').selectedIndex].value;

    if(provider == "")
    {
        document.getElementById('title').innerHTML = "ERROR!";
        document.getElementById('msg').innerHTML = "Please select a provider.";
        document.getElementById('light').style.display = "block";
        document.getElementById('fade').style.display = "block";
        return false;
    }
    else
    {
        document.getElementById('loading').style.display = "block";
        document.getElementById('fade').style.display = "block";
        document.getElementById('loading-image').style.display = "block";
        return true;
    }
}
/*Added by ARS*/
/*Added by ARS 01-06-2012*/
function showloadingpage2()
{
    var provider = document.getElementById("ddlProviders").options[document.getElementById('ddlProviders').selectedIndex].value;
    if(provider == "")
    {
        document.getElementById('title').innerHTML = "ERROR!";
        document.getElementById('msg').innerHTML = "Please select a provider.";
        document.getElementById('light').style.display = "block";
        document.getElementById('fade').style.display = "block";
        return false;
    }
    else
    {
        document.getElementById('light2').style.display = "none";
        document.getElementById('loading').style.display = "block";
        document.getElementById('fade').style.display = "block";
        document.getElementById('loading-image').style.display = "block";
        return true;
    }
}

function showwinnersloadingpage2()
{
    var provider = document.getElementById("ddlProviders").options[document.getElementById('ddlProviders').selectedIndex].value;
    if(provider == "")
    {
        document.getElementById('title').innerHTML = "ERROR!";
        document.getElementById('msg').innerHTML = "Please select a provider.";
        document.getElementById('light').style.display = "block";
        document.getElementById('fade').style.display = "block";
        return false;
    }
    else
    {
        document.getElementById('light2').style.display = "none";
        document.getElementById('loading').style.display = "block";
        document.getElementById('fade').style.display = "block";
        document.getElementById('loading-image').style.display = "block";
        return true;
    }
}
/*Added by ARS*/

/*start: added by mtcc on 01/20/2012*/
function showloadingpage3()
{
    document.getElementById('loading').style.display = "block";
    document.getElementById('fade').style.display = "block";
    document.getElementById('loading-image').style.display = "block";
    return true;
}
/*end: added by mtcc on 01/20/2012*/

/*Added by ARS 10-06-2011*/
function booktransvalidation()
{
   var gamenum = document.getElementById("ddlgametype").options[document.getElementById('ddlgametype').selectedIndex].value;
    var assignedto = document.getElementById("ddlassingedto").options[document.getElementById('ddlassingedto').selectedIndex].value;
    var gamename = document.getElementById("ddlgamename").options[document.getElementById('ddlgamename').selectedIndex].value;
    var batchid = document.getElementById("ddlgamebatch").options[document.getElementById('ddlgamebatch').selectedIndex].value;
     var cardvalue = document.getElementById("ddlcardvalue").options[document.getElementById('ddlcardvalue').selectedIndex].value;

    if(cardvalue == ' ')
    {
        document.getElementById('title').innerHTML = "ERROR!";
        document.getElementById('msg').innerHTML = "Please select card value.";
        document.getElementById('light').style.display = "block";
        document.getElementById('fade').style.display = "block";
        return false;
    }
    if(gamename == 0)
    {
        document.getElementById('title').innerHTML = "ERROR!";
        document.getElementById('msg').innerHTML = "Please select game name.";
        document.getElementById('light').style.display = "block";
        document.getElementById('fade').style.display = "block";
        return false;
    }
    if(gamenum == 0)
    {
        document.getElementById('title').innerHTML = "ERROR!";
        document.getElementById('msg').innerHTML = "Please select game number.";
        document.getElementById('light').style.display = "block";
        document.getElementById('fade').style.display = "block";
        return false;
    }
	if(batchid == 0)
    {
        document.getElementById('title').innerHTML = "ERROR!";
        document.getElementById('msg').innerHTML = "Please select a game batch.";
        document.getElementById('light').style.display = "block";
        document.getElementById('fade').style.display = "block";
        return false;
    }
    if(assignedto == "-")
    {
        document.getElementById('title').innerHTML = "ERROR!";
        document.getElementById('msg').innerHTML = "Please select a Redemption Center.";
        document.getElementById('light').style.display = "block";
        document.getElementById('fade').style.display = "block";
        return false;
    }
    return true;
}
function distributorbooktransvalidation()
{
    var gamenum = document.getElementById("ddlgametype").options[document.getElementById('ddlgametype').selectedIndex].value;
    var assignedto = document.getElementById("ddlassingedto").options[document.getElementById('ddlassingedto').selectedIndex].value;
    var gamename = document.getElementById("ddlgamename").options[document.getElementById('ddlgamename').selectedIndex].value;
    var batchid = document.getElementById("ddlgamebatch").options[document.getElementById('ddlgamebatch').selectedIndex].value;
    var cardvalue = document.getElementById("ddlcardvalue").options[document.getElementById('ddlcardvalue').selectedIndex].value;

    if(cardvalue == 0)
    {
        document.getElementById('title').innerHTML = "ERROR!";
        document.getElementById('msg').innerHTML = "Please select card value.";
        document.getElementById('light').style.display = "block";
        document.getElementById('fade').style.display = "block";
        return false;
    }
    if(gamename == 0)
    {
        document.getElementById('title').innerHTML = "ERROR!";
        document.getElementById('msg').innerHTML = "Please select game name.";
        document.getElementById('light').style.display = "block";
        document.getElementById('fade').style.display = "block";
        return false;
    }
    if(gamenum == 0)
    {
        document.getElementById('title').innerHTML = "ERROR!";
        document.getElementById('msg').innerHTML = "Please select game number.";
        document.getElementById('light').style.display = "block";
        document.getElementById('fade').style.display = "block";
        return false;
    }
	if(batchid == 0)
    {
        document.getElementById('title').innerHTML = "ERROR!";
        document.getElementById('msg').innerHTML = "Please select a game batch.";
        document.getElementById('light').style.display = "block";
        document.getElementById('fade').style.display = "block";
        return false;
    }
    if(assignedto == '-')
    {
        document.getElementById('title').innerHTML = "ERROR!";
        document.getElementById('msg').innerHTML = "Please select assigned to.";
        document.getElementById('light').style.display = "block";
        document.getElementById('fade').style.display = "block";
        return false;
    }
    return true;
}
function booktranssearch()
{
    var srchstring = document.getElementById('txtsearch').value;
    if(srchstring.replace(/^\s\s*/, '').replace(/\s\s*$/, '').length == 0)
    {
        document.getElementById('title').innerHTML = "ERROR!";
        document.getElementById('msg').innerHTML = "Please provide details to search.";
        document.getElementById('light').style.display = "block";
        document.getElementById('fade').style.display = "block";
        return false;
    }
    return true;
}
function checkBookTransUpdate()
{
    var reassignedto = document.getElementById("ddlassingedto").options[document.getElementById('ddlassingedto').selectedIndex].value;
    if(reassignedto == "")
    {
        document.getElementById('title2').innerHTML = "ERROR!";
        //document.getElementById('msg2').innerHTML = "Please select a company retailer.";
		document.getElementById('msg2').innerHTML = "Please select an account.";
        document.getElementById('light2').style.display = "block";
        document.getElementById('fade').style.display = "block";
        return false;
    }
    return true;
}
function redirectToBookTransfer()
{
    window.location = "booktransfer.php";
}
function redirectToDistributorBookTransfer()
{
    window.location = "distributorbookreassignment.php";
}
function ShowAdminConfirmation()
{
    document.getElementById('light4').style.display = "none";
    document.getElementById('light3').style.display = "block";
    document.getElementById('fade').style.display = "block";
}
function ValidateAdminConfirmation()
{
    var uname = document.getElementById('txtusername').value;
    var pword = document.getElementById('txtpassword').value;
    if(uname.replace(/^\s\s*/, '').replace(/\s\s*$/, '').length == 0)
    {
        document.getElementById('light3').style.display = "none";
        document.getElementById('title4').innerHTML = "ERROR!";
        document.getElementById('msg4').innerHTML = "Please provide admin username.";
        document.getElementById('light4').style.display = "block";
        document.getElementById('fade').style.display = "block";
        return false;
    }
    if(pword.replace(/^\s\s*/, '').replace(/\s\s*$/, '').length == 0)
    {
        document.getElementById('light3').style.display = "none";
        document.getElementById('title4').innerHTML = "ERROR!";
        document.getElementById('msg4').innerHTML = "Please provide admin password.";
        document.getElementById('light4').style.display = "block";
        document.getElementById('fade').style.display = "block";
        return false;
    }
    return true;
}
/*Added by ARS*/
/*Added by ARS 10-07-2011*/
function booktranshistvalidation()
{
    var SDate = document.getElementById("txtDateFr").value;
    var EDate = document.getElementById("txtDateTo").value;

    var endDate = new Date(EDate);
    var startDate = new Date(SDate);
    var currDate = new Date();

    if(SDate != '' && EDate != '' && startDate > endDate)
    {
    	document.getElementById('title5').innerHTML = "ERROR";
        document.getElementById('msg5').innerHTML = "Please enter a valid date range.";
        document.getElementById('light5').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    else if (SDate.replace(/^\s\s*/, '').replace(/\s\s*$/, '').length == 0)
    {
        document.getElementById('title5').innerHTML = "ERROR";
        document.getElementById('msg5').innerHTML = "Please select your Start Date.";
        document.getElementById('light5').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    else if (EDate.replace(/^\s\s*/, '').replace(/\s\s*$/, '').length == 0)
    {
        document.getElementById('title5').innerHTML = "ERROR";
        document.getElementById('msg5').innerHTML = "Please select your End Date.";
        document.getElementById('light5').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    else if (endDate > currDate)
    {
        document.getElementById('title5').innerHTML = "ERROR";
        document.getElementById('msg5').innerHTML = "Please enter a valid date range.";
        document.getElementById('light5').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    else{ return true;}
    } 
/*Added by ARS*/
/*Added by ARS 10-13-2011*/
function disableSpace(evt)
{
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode == 32)
        return false;

    return true;
}
/*Added by ARS*/
/*Added by ARS 12-06-2011*/
function DisableSpecialCharacters(evt)
{
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if((charCode > 32 && charCode < 48) || (charCode > 57 && charCode < 65) || (charCode > 90 && charCode < 97) || (charCode > 122 && charCode < 128))
        return false;

    return true;
}
/*Added by ARS*/

/*Added by ARS 12-27-2011*/
function isAlphaNumericKey(evt)
{
	var charCode = (evt.which) ? evt.which : evt.keyCode;
	if((charCode > 31 && charCode < 48) || (charCode > 57 && charCode < 65) || (charCode > 90 && charCode < 97) || (charCode > 122 && charCode < 128) || (charCode > 127))
		return false;

    return true;
}
/*Added by ARS*/
/*Added by ARS 01-11-2012*/
function checkCommissionSummary()
{
    var username = document.getElementById("ddlUsername").options[document.getElementById('ddlUsername').selectedIndex].value;
    var gamename = document.getElementById("ddlGameName").options[document.getElementById('ddlGameName').selectedIndex].value;
    var gamenum = document.getElementById("ddlGameNum").options[document.getElementById('ddlGameNum').selectedIndex].value;
	var batchid = document.getElementById("ddlBatchID").options[document.getElementById('ddlBatchID').selectedIndex].value;

    if(username == 0)
    {
        document.getElementById('title').innerHTML = "ERROR!";
        document.getElementById('msg').innerHTML = "Please select username.";
        document.getElementById('light').style.display = "block";
        document.getElementById('fade').style.display = "block";
        return false;
    }
    if(gamename == 0)
    {
        document.getElementById('title').innerHTML = "ERROR!";
        document.getElementById('msg').innerHTML = "Please select game name.";
        document.getElementById('light').style.display = "block";
        document.getElementById('fade').style.display = "block";
        return false;
    }
	if(gamenum == 0)
    {
        document.getElementById('title').innerHTML = "ERROR!";
        document.getElementById('msg').innerHTML = "Please select game number.";
        document.getElementById('light').style.display = "block";
        document.getElementById('fade').style.display = "block";
        return false;
    }
    if(batchid == "")
    {
        document.getElementById('title').innerHTML = "ERROR!";
        document.getElementById('msg').innerHTML = "Please select game batch.";
        document.getElementById('light').style.display = "block";
        document.getElementById('fade').style.display = "block";
        return false;
    }
    return true;
}
/*Added by ARS*/

/* Added by JFJ 01-07-2013*/
function ticketreassignmentvalidation()
{
    var gamename = document.getElementById("ddlgamename").options[document.getElementById('ddlgamename').selectedIndex].value;
    var gamenum = document.getElementById("ddlgametype").options[document.getElementById('ddlgametype').selectedIndex].value;
    var booknumber = document.getElementById("ddlbooknumber").options[document.getElementById('ddlbooknumber').selectedIndex].value;
    var ticketcount = document.getElementById("txtTicketCount").value;
    var transferto = document.getElementById("ddlassingedto").options[document.getElementById('ddlassingedto').selectedIndex].value;
    if(gamename == 0)
    {
        document.getElementById('title1').innerHTML = "ERROR!";
        document.getElementById('msg1').innerHTML = "Please select game name.";
        document.getElementById('light1').style.display = "block";
        document.getElementById('fade').style.display = "block";
        return false;
    }
    if(gamenum == 0)
    {
        document.getElementById('title1').innerHTML = "ERROR!";
        document.getElementById('msg1').innerHTML = "Please select game number.";
        document.getElementById('light1').style.display = "block";
        document.getElementById('fade').style.display = "block";
        return false;
    }
     if(booknumber == 0)
    {
        document.getElementById('title1').innerHTML = "ERROR!";
        document.getElementById('msg1').innerHTML = "Please select book number.";
        document.getElementById('light1').style.display = "block";
        document.getElementById('fade').style.display = "block";
        return false;
    }
    if(ticketcount == 0)
    {
        document.getElementById('title1').innerHTML = "ERROR!";
        document.getElementById('msg1').innerHTML = "Please enter ticket count.";
        document.getElementById('light1').style.display = "block";
        document.getElementById('fade').style.display = "block";
        return false;
    }
    if(ticketcount > 200)
    {
        document.getElementById('title1').innerHTML = "ERROR!";
        document.getElementById('msg1').innerHTML = "Ticket count must be less than 200.";
        document.getElementById('light1').style.display = "block";
        document.getElementById('fade').style.display = "block";
        return false;
    }
    if(transferto == 0)
    {
        document.getElementById('title1').innerHTML = "ERROR!";
        document.getElementById('msg1').innerHTML = "Please select transfer to.";
        document.getElementById('light1').style.display = "block";
        document.getElementById('fade').style.display = "block";
        return false;
    }
    
    return true;
}
function disableapostrophe(evt)
{
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode == 39)
        return false;

    return true;
}
function OnlyNumbers(event)
{
    var e = event || evt; // for trans-browser compatibility
    var charCode = e.which || e.keyCode;

    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

    return true;

}
function checkDistibutorUpdate()
{
    var reassignedto = document.getElementById("ddlassingedto").options[document.getElementById('ddlassingedto').selectedIndex].value;
    var InvoiceNo = document.getElementById("txtInvoiceNo").value;
    var oldInvoiceNo = document.getElementById("hiddeninvoiceno").value;
    if(reassignedto == "")
    {
        document.getElementById('title2').innerHTML = "ERROR!";
        //document.getElementById('msg2').innerHTML = "Please select a company retailer.";
	document.getElementById('msg2').innerHTML = "Please select an account.";
        document.getElementById('light2').style.display = "block";
        document.getElementById('fade').style.display = "block";
        return false;
    }    
    else if(InvoiceNo == "")
    {
        document.getElementById('title2').innerHTML = "ERROR!";
	document.getElementById('msg2').innerHTML = "Please input an Invoice number.";
        document.getElementById('light2').style.display = "block";
        document.getElementById('fade').style.display = "block";
        return false;
    }
    if(InvoiceNo.length<5)
    {
        document.getElementById('title2').innerHTML = "ERROR!";
	document.getElementById('msg2').innerHTML = "Invoice number must not be less than 5 digits.";
        document.getElementById('light2').style.display = "block";
        document.getElementById('fade').style.display = "block";
        return false;
    }
//    if(InvoiceNo == oldInvoiceNo)
//    {
//        document.getElementById('title2').innerHTML = "ERROR!";
//	document.getElementById('msg2').innerHTML = "Please provide new Invoice number.";
//        document.getElementById('light2').style.display = "block";
//        document.getElementById('fade').style.display = "block";
//        return false;
//    }    
    return true;
}


/*
 * This function is used to validate email address inputs
 * Added By Noel Antonio 05-31-2013
 */
function verifyEmail(event) {
    var charCode = (event.which) ? event.which : event.keyCode;
    if ((charCode == 64) || (charCode == 95) || (charCode == 46))/* @=64, _=95, .=46 characters are allowed*/
    {
        return true;
        return true;
    } else {
        if (/*DisableWhiteSpaces*/(charCode == 32) || /*DisableSpecialCharacters*/ (charCode > 32 && charCode < 48) || (charCode > 57 && charCode < 65) || (charCode > 90 && charCode < 97) || (charCode > 122 && charCode < 128))
            return false;
        return true;
    }
}
