<?php
/*
 * @author mtcc 08/24/2011
 * Purpose   : view for login
 */
require_once("init.inc.php");
include("../controller/loginprocess.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <link rel="stylesheet" type="text/css" media="screen" href="css/default.css" />
  <script src="jscripts/jquery-1.5.2.min.js" type="text/javascript"></script>
  <script language="javascript" src="jscripts/validations.js"></script>
  <script type="text/javascript">
        $('#txtEmail').live('keypress',function(e){
            if(e.keyCode == 13) {
                $('#btnReset').focus();
                return false;
            }
        });
  </script>
  <title>Ticket Management System</title>
</head>
    <body>
        <form id="frmLogin" name="frmLogin" method="post">
        <table border="0" cellspacing="0" cellpadding="0" class="container">
        <tr>
            <td colspan="2">
                <div class="header">
                    <div class="company_logo">
                        <img src="images/philweb_logo.png" alt="" height ="95px" width="220px"/>
                  </div>
                </div>
            </td>
       </tr>
       <tr>
           <td colspan="2">
                <div class="project_logo" align="center">
                    <img src="images/scratch_n_win_logo.png" alt="" height ="70px" width="140px"/>
                </div>
           </td>
       </tr>
          <tr>
                    <td valign="top" class="content_container">
                        <div class="login-form">
                <div id="mainContentBox">
                  <div class="rightContainerTop">
                      <img src="images/leftContainer_top.png" height="11px" width="100%"/>
                  </div>
                  <div class="rightContainerBody">
                            <div class="titleCont">
                                <div class="titleCont_left"></div>
                                    <div class="titleCont_body" style="width: 87%;">Ticket Management System</div>
                                    <div class="titleCont_right"></div>
                            </div>
                        <table style="width: 300px;padding-left: 3%;padding-right: 4%;">
                            <tr>
                                <td>Username : </td>
                                <td><?php echo $txtUsername; ?></td>
                            </tr>
                            <tr>
                                <td>Password : </td>
                                <td><?php echo $txtPassword; ?></td>
                            </tr>
                            <tr>
                                <td colspan="2" style="text-align: right;">
                                    <br/><?php echo $btnSubmit; ?>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2"  style="text-align: right;">
                                    <br/><a href="#" onclick="javascript: return openforgotpw();" style="font-size:1.0em; font-weight:bold; cursor:pointer;"><u>Reset Password</u></a>
                                </td>
                            </tr>
                        </table>
                  </div>
                    </div>
                      <div class="rightContainerBottom">
                          <img src="images/leftContainer_bottom.png" height="11px" width="100%"/>
                      </div>
                </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div class="footerContainer">
                            <div class="footerContainer_left"></div>
                            <div class="footerContainer_body">
                                <div style="padding-top: 0.8%;padding-left: 0.5%;">Copyright 2011 PhilWeb Corporation</div>
                            </div>
                            <div class="footerContainer_right"></div>
                        </div>
                    </td>
       </tr>
            </table>
            <div id="fade" class="black_overlay"></div>
            <div id="loading" class="loading"></div>
            <!-- POP UP FOR MESSAGES -->
            <div id="light" class="white_content">
                <div id="title" class="light-title"></div>
                <div id="msg" class="light-message"></div>
                <div id="button" class="light-button"><input id="btnForgot" name="btnForgot" type="button" onclick="javascript: document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none';" value="OKAY"/></div>
                <div class="light-footer"></div>
            </div>
            <!-- POP UP FOR MESSAGES -->
            <!-- FORGOT PASSWORD -->
            <div id="light2" class="white_content">
                <div id="title2" class="light-title"></div>
                <div id="msg2" class="light-message"></div>
                <div align="center" class="light-message"><?php echo $txtEmail; ?></div>
                <div id="button" class="light-button"><?php echo $btnReset; ?>
                &nbsp;<input id="btnCancel" name="btnCancel" type="button" onclick="javascript: document.getElementById('light2').style.display='none';document.getElementById('fade').style.display='none';" value="CANCEL"/></div>
                <div class="light-footer"></div>
            </div>
            <!-- FORGOT PASSWORD -->
            <?php if (isset($errormsg)): ?>
            <script>
                document.getElementById('title').innerHTML = "<?php echo $errormsgtitle;?>";
                document.getElementById('msg').innerHTML = "<?php echo $errormsg;?>";
                document.getElementById('light').style.display = 'block';
                document.getElementById('fade').style.display = 'block';
            </script>
            <?php endif; ?>
            <?php if (isset($errormsgfp)): ?>
            <script>
                document.getElementById('title2').innerHTML = "<?php echo $errormsgtitle;?>";
                document.getElementById('msg2').innerHTML = "<?php echo $errormsgfp;?>";
                document.getElementById('light2').style.display = 'block';
                document.getElementById('fade').style.display = 'block';
            </script>
            <?php endif; ?>
      </form>
  </body>
</html>