<?php
/*
 * @author 
 * Purpose   : view for menu
 */
$pagename = $_GET["page"];
?>
<script type="text/javascript" src="jscripts/jquery-1.5.2.min.js"></script>
<script type="text/javascript">
function preventBackandForward()
{
window.history.forward();
}
preventBackandForward();
window.inhibited_load=preventBackandForward;
window.onpageshow=function(evt){if(evt.persisted)preventBackandForward();};
window.inhibited_unload=function(){void(0);};
</script> 
<script type="text/javascript" src="../jscripts/leftmenu.js"></script>
<link type="text/css" href="../css/default.css" media="projection, screen" rel="stylesheet" />
<div id="masterdiv">
    <div class="menutitle" onclick="SwitchMenu('sub1')">
    Account Management
    </div>
    <span class="submenu" id="sub1">
       <a href="template.php?page=" class="linkmenu">Account List<br/></a>
       <a href="template.php?page=" class="linkmenu">Account Profile<br/></a>
    </span>

    <div class="menutitle" onclick="SwitchMenu('sub2')">
    Inventory Management
    </div>
    <span class="submenu" id="sub2">
       <a href="template.php?page=" class="linkmenu">Warehouse Inventory<br/></a>
       <a href="template.php?page=" class="linkmenu">Item Information<br/></a>  
       <a href="template.php?page=" class="linkmenu">Site Inventory<br/></a>  
    </span> 

    <div class="menutitle" onclick="SwitchMenu('sub3')">
    Operations Management
    </div>
    <span class="submenu" id="sub3">
       <a href="template.php?page=" class="linkmenu">Record Sales<br/></a>
       <a href="template.php?page=" class="linkmenu">Ticket Validation<br/></a>
       <a href="template.php?page=" class="linkmenu">Ticket Cancellation<br/></a>
    </span> 

    <div class="menutitle" onclick="SwitchMenu('sub4')">
    Reports
    </div>
    <span class="submenu" id="sub4">
       <a href="template.php?page=" class="linkmenu">Sales Transaction History<br/></a>
       <a href="template.php?page=" class="linkmenu">Cancellation History<br/></a>
       <a href="template.php?page=" class="linkmenu">Settlement Report<br/></a>
       <a href="template.php?page=" class="linkmenu">Prize History<br/></a>
    </span> 
   <div class="menutitle" onclick="SwitchMenu('sub5')">
     Game Management
   </div>
    <span class="submenu" id="sub5">
       <a href="template.php?page=assigngame" class="linkmenu">Game List<br/></a>
       <a href="template.php?page=gamemgmt" class="linkmenu">Game Profile<br/></a>
    </span> 

</div>