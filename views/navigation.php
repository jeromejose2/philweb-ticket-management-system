<?php
/*
 * @author 
 * Purpose   : view for navigation
 */
$menus = $_SESSION["usermenus"];

App::LoadModuleClass("TicketManagementCM", "TMSubMenus");
$selectedmenuid = 0;
$accounttypeid = $_SESSION['accttype'];
$navigationsubmenu = new TMSubMenus();
$arrnavsubmenus = $navigationsubmenu->GetGroupSubMenusBySubMenuID($pagesubmenuid, $accounttypeid);
?>

<div id="mainmenucontainer">
    <ul>
        <?php
        for ($i = 0; $i < count($menus); $i++)
        {
            $menu = $menus[$i];
            ?>
            <div class="navCont">
                <div class="navCont_left"></div>
                <div class="navCont_body"><?php echo $menu["title"];?></div>
                <div class="navCont_right"></div>
            </div>	
<!--        <a href="<?php //echo $menu["url"];?>"><?php //echo $menu["title"];?></a>-->
                <?php
                for ($x = 0; $x < count($arrnavsubmenus); $x++)
                {
                    if ($menu["MenuID"] == $arrnavsubmenus[$x]["MenuID"])
                    {
                        $navsubmenu = $arrnavsubmenus[$x];
                ?>
                <li id="submenu">
                    <a href="<?php echo $navsubmenu["Link"]; ?>" style="white-space: nowrap;" <?php echo $pagesubmenuid == $navsubmenu["SubMenuID"] ? "id='selectedsubmenu'" : ''; ?>><?php echo $navsubmenu["Name"]; ?></a></b>
                </li>
                <?php        
                    }
                }
                ?>
            
        <?php } ?>
    </ul>
</div>