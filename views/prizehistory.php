<?php
/*
 * Author: Noel Antonio
 * Date Created: 2011-08-31
 * Company: Philweb Corporation
 */
include("init.inc.php");
$pagesubmenuid = 12;
include_once("../controller/managesession.php");
include("../controller/prizehistorycontroller.php");
?>

<?php include("header.php"); ?>
        <link rel="stylesheet" type="text/css" href="css/datepicker.css" />
        <link rel="stylesheet" type="text/css" href="css/default.css" />
        <script type="text/javascript" src="jscripts/datetimepicker.js"></script>
        <script type="text/javascript" src="jscripts/jquery-1.5.2.min.js" lang="Javascript"></script>
        <script language="javascript" type="text/javascript">
            function ChangePage(pagenum)
            {
                selectedindex = document.getElementById("pgSelectedPage");
                selectedindex.value = pagenum;
                document.forms[0].submit();
            }
            function settoall()
            {       
                   $("#ddlgametype").val("0");
                   $("#cboPrizeType").val("0");
                   get_gamenumber();
                   get_prizetype();
            }
            
            function get_gamenumber()
            {
                var prodid = document.getElementById('ddlgamename').options[document.getElementById('ddlgamename').selectedIndex].value;

                if (prodid == 0)
                {
                    $('#ddlgametype').empty();
                    $("#ddlgametype").prepend("<option selected value=\"0\" >ALL</option>");
                }
                else 
                {
                    $("#ddlgametype").load(
                        "../controller/get_gamenum.php",
                        {
                            prodid: prodid
                        },

                        function(response, status, xhr)
                        {
                            if (status == "success")
                            {
                                $('#ddlgametype option[value="0"]').remove();
                                $("#ddlgametype").prepend("<option selected value=\"0\" >ALL</option>");
                            }
                            if(status == "error")
                            {
                                document.getElementById('title').innerHTML = "ERROR!";
                                document.getElementById('msg').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
                                document.getElementById('light').style.display = "block";
                                document.getElementById('fade').style.display = "block";
                            }
                        }
                    );
                }
            }
            
            function get_gamename()
            {
                    document.getElementById("flag").value = 1;
                    var cardvalue = document.getElementById('ddlcardvalue').options[document.getElementById('ddlcardvalue').selectedIndex].value;
                    var gamename = document.getElementById('ddlgamename').options[document.getElementById('ddlgamename').selectedIndex].value;
                    var flag = document.getElementById("flag").value;

                    $("#ddlgamename").load(
                        "../controller/get_gamename.php",
                        {
                            cardvalue: cardvalue,
                            gamename: gamename,
                            flag: flag
                        },

                        function(response, status, xhr)
                        {
                            if (status == "success")
                            {
                                $('#ddlgamename option[value="0"]').remove();
                                $("#ddlgamename").prepend("<option selected value=\"0\" >ALL</option>");

                                $("#ddlgametype").empty();
                                $("#ddlgametype").prepend("<option selected value=\"0\" >ALL</option>");
                                
                                $("#cboPrizeType").empty();
                                $("#cboPrizeType").prepend("<option selected value=\"0\" >ALL</option>");
                            }
                            if(status == "error")
                            {
                                document.getElementById('title').innerHTML = "ERROR!";
                                document.getElementById('msg').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
                                document.getElementById('light').style.display = "block";
                                document.getElementById('fade').style.display = "block";
                            }
                        }
                    );
            }
            
            function get_prizetype()
            {   
                
                var gameid = document.getElementById('ddlgametype').options[document.getElementById('ddlgametype').selectedIndex].value;
                if (gameid == 0)
                {                    
                   $("#cboPrizeType").val("0");
                }
                
                $('#cboPrizeType').load(
                    '../controller/get_prizetype.php',
                    {
                        gameid: gameid
                    },
                    function(status,xhr)
                    {
                        if (status == "error") {
                            document.getElementById('title').innerHTML= 'ERROR!';
                            document.getElementById('msg').innerHTML = "Sorry but an error has occured : " + xhr.status + " " + xhr.statusText;
                            document.getElementById('light').style.display = 'block';
                            document.getElementById('fade').style.display = 'block';
                        }
                    }
                );
            }            
        </script>
        <form action="prizehistory.php" method="POST">
            <?php $hidPrizeTypeID;?>
            <?php echo $xmltype; ?>
            <?php echo $flag; ?>
            <div id="fade" class="black_overlay"></div>
<!-- POP UP FOR MESSAGES -->
<div id="light" class="white_content">
    <div id="title" class="light-title"></div>
    <div id="msg" class="light-message"></div>
    <div id="button" class="light-button"><input type="button" onclick="javascript: document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none';" value="Okay"/></div>
    <div class="light-footer"></div>
</div>
            
<div class="titleCont">
<div class="titleCont_left"></div>
<div class="titleCont_body">Prize History</div>
<div class="titleCont_right"></div>
</div>
<div class="content-page">
    <table width ="70%" style="margin-left: 0.5%;">
        <tr>
            <td>Card Value: <?php echo $ddlcardvalue;?></td>
            <td><?php echo $ddlgamename;?></td>                        
            <td><?php echo $ddlgametype;?></td>
            <!-- <td><?php echo $txtGameName;?></td> -->
            <td><?php echo $cboPrizeType;?></td>
            <td align="right"><?php echo $btnSubmit;?></td>
        </tr>
    </table>
    <br/>

    <!-- START OF DISPLAYING RECORDS IN TABLE -->
    <?php if (isset($allprize_list) || isset($prize_list)): ?>
	<div class="form-page">
		<?php //echo $btnExport;?>
	</div>
	<div class="form-page">
		<?php echo $btnExportCSV;?>
	</div>
    <table class="table-list">
        <tr><th colspan ="6">SUMMARY</th></tr>
        <tr>
            <th>Batch Size</th>
            <th>Total Winning Entries</th>
            <th>Total Prizes</th>
            <th>Prize Left</th>
            <th>Prize Redeemed</th>
            <th>%</th>
        </tr>
        <tr class="oddrow">
            <td style="text-align: right;"><?php echo number_format($total_tickets[0]["Total_Tickets"] , 0 , "" , ",");?></td>
            <td style="text-align: right;"><?php echo number_format($total_winnings[0]["Total_Winning"] , 0 , "" , ",");?></td>
            <td style="text-align: right;"><?php echo number_format($totalprizes , 0 , "" , ",");?></td>
            <td style="text-align: right;"><?php echo number_format($total_prize_left_converted , 0 , "" , ",");?></td>
            
            <td style="text-align: right;"><?php echo number_format($total_prize_redeem_converted , 0 , "" , ",");?></td>
            <td style="text-align: right;"><?php echo number_format($percentage,4);?></td>
        </tr>
    </table>

    <table class="table-list" style="text-align: right">
        <tr align="center">
            <th>Card Value</th>
            <th>Game Name</th>
            <th>Game Number</th>            
            <th>Prize Type</th>
            <th>Total Quantity of Prize</th>
            <th>Total Quantity of Prize Used</th>
            <th>Prize Balance</th>
            <th>Percentage Consumption</th>
        </tr>

    <!-- Start of Retrieving All Prize List Records -->
    <?php
        $arrWC = null;
        for($ctr=0; $ctr < count($allprize_list); $ctr++){
            $arrWC[$ctr]["CP"] = $allprize_list[$ctr]['CardPrice'];
            $arrWC[$ctr]["GI"] = $allprize_list[$ctr]['GameNumber'];
            $arrWC[$ctr]["PN"] = $allprize_list[$ctr]['PrizeName'];
            $arrWC[$ctr]["GN"] = $allprize_list[$ctr]['ProductName'];
            $arrWC[$ctr]["TNA"] = $allprize_list[$ctr]['TotalAlloted'];
            $arrWC[$ctr]["PrizeID"] = $allprize_list[$ctr]['PrizeID'];
            $tempGameID = $allprize_list[$ctr]['GameID'];
            
            // Check if there are claimed prizes..
            for($ctr2 = 0; $ctr2 < count($allprize_list2); $ctr2++){
                if ($tempGameID == $allprize_list2[$ctr2]['GameID'] && $arrWC[$ctr]["PrizeID"] == $allprize_list2[$ctr2]['PrizeID'])
                {
                      $arrWC[$ctr]["TNC"] = $allprize_list2[$ctr2]['TotalClaimed'];
                      $arrWC[$ctr]["RNP"] = $allprize_list[$ctr]['TotalAlloted'] - $allprize_list2[$ctr2]['TotalClaimed'];
                      $arrWC[$ctr]["RNP"] =  ( $arrWC[$ctr]["RNP"] == NULL ) ? '000' : $arrWC[$ctr]["RNP"];
                }
            }
        }
    ?>
    <!-- End of Retrieving All Prize List Records-->

    <!--Start Displaying All Prize List Records-->
    <div class="form-page"><?php 
   if($reccount != $itemsperpage)
    echo $pgTransactionHistory;
    ?></div>
    <?php
    for($ctr=0; $ctr < count($allprize_list); $ctr++){?>
    <?php ($ctr % 2) == 0 ? $class = "evenrow" : $class = "oddrow" ; ?>
        <tr class = "<?php echo $class?>">
            <td style="text-align: center;"><?php echo $arrWC[$ctr]["CP"];?></td>
            <td style="text-align: center;"><?php echo $arrWC[$ctr]["GN"];?></td>
            <td style="text-align: center;"><?php echo $arrWC[$ctr]["GI"];?></td>            
            <td style="text-align: center;"><?php echo $arrWC[$ctr]["PN"];?></td>
            <td><?php echo number_format($arrWC[$ctr]["TNA"]);?></td>
            <td><?php 
            if ($arrWC[$ctr]["TNC"] == ""){
                echo "0";
                $arrWC[$ctr]["TNC"] = 0;
            } else {
                echo number_format($arrWC[$ctr]["TNC"]);
            }
            ?></td>
            <td><?php
            if ($arrWC[$ctr]["RNP"] == ""){
                echo number_format($arrWC[$ctr]["TNA"]);
            } else {
                echo number_format($arrWC[$ctr]["RNP"]);
            }
            ?></td>
            <td>
                <?php
                    if ($arrWC[$ctr]["TNC"] == ""){
                        $used =  0;
                    } else {
                        $used = $arrWC[$ctr]["TNC"];
                    }
                    $totalquan = $arrWC[$ctr]["TNA"];
                    echo number_format(round((($used / $totalquan) * 100) , 4),4)  . "%";
                ?>
            </td>
        </tr>
    <?php }?>
    <!-- End Displaying All Prize List Records-->

    <!-- For Specific Searching of Prize -->
    <?php
    if (isset($prize_list) && isset($claim_list)):
        for($ctr=0; $ctr < count($prize_list); $ctr++){ ?>
        <?php ($ctr % 2) == 0 ? $class = "evenrow" : $class = "oddrow" ; ?>
        <tr class = "<?php echo $class?>">
            <td style="text-align: center;"><?php echo $prize_list[$ctr]['CardPrice'];?></td>
            <td style="text-align: center;"><?php echo $prize_list[$ctr]['ProductName'];?></td>
            <td style="text-align: center;"><?php echo $prize_list[$ctr]['GameNumber'];?></td>            
            <td style="text-align: center;"><?php echo $prize_list[$ctr]['Description'];?></td>
            <td><?php echo number_format($prize_list[$ctr]['TotalAlloted']);?></td>
            <td><?php echo number_format($claim_list[0]['Status']);?></td>
            <?php
                $total_remaining = $prize_list[$ctr]['TotalAlloted'] - $claim_list[0]['Status'];
            ?>
            <td><?php echo number_format($total_remaining);?></td>
            <td><?php echo round((($claim_list[0]['Status'] / $prize_list[$ctr]['TotalAlloted']) * 100) , 4) . "%";?></td>
        </tr>
        <?php } ?>
    <?php endif; ?>
    <!--End of Specific Searching of Prize-->

    </table>
    <?php endif; ?>
    <?php //$allprize_list=null;$prize_list=null;?>
    <!-- END OF DISPLAYING RECORDS IN TABLE -->
    </div>
</form>
<div class="content-page"><div class="form-page"><?php
 if($reccount != $itemsperpage)
     echo $pgTransactionHistory;?></div></div>
<?php include("footer.php"); ?>
<script language="javascript" type="text/javascript">
function validate()
{  
    var gameid = document.getElementById('cboGameID').options[document.getElementById('cboGameID').selectedIndex].value;
    var gamename = document.getElementById('ddlgamename').options[document.getElementById('ddlgamename').selectedIndex].value;
    var prizetype = document.getElementById('cboPrizeType').options[document.getElementById('cboPrizeType').selectedIndex].value;

    if (gamename == 0)
    {
        document.getElementById('title').innerHTML= 'ERROR!';
        document.getElementById('msg').innerHTML = "Please select game name.";
        document.getElementById('light').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    else if (gameid == 0)
    {
        document.getElementById('title').innerHTML= 'ERROR!';
        document.getElementById('msg').innerHTML = "Please select game number.";
        document.getElementById('light').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    else if (prizetype == "")
    {
        document.getElementById('title').innerHTML= 'ERROR!';
        document.getElementById('msg').innerHTML = "Please select prize type.";
        document.getElementById('light').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    else{ return true;}
}
</script>