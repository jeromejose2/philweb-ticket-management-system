<?php
/*
 * @author 
 * Purpose   : view for productlist
 */
include("../init.inc.php");
$pagesubmenuid = 23;
include_once("../controller/managesession.php");
include("../controller/productlistprocess.php");

?>
<script language="javascript" src="../jscripts/validations.js"></script>
<script language="javascript">
    function SelectedIndexChange(index)
    {
        selectedindex = document.getElementById("hdnSelectedProviderID");
        selectedindex.value = index;
        document.forms[0].submit();
    }
    function ChangePage(pagenum)
    {
        selectedindex = document.getElementById("pgSelectedPage");
        selectedindex.value = pagenum;
        document.forms[0].submit();
    }
    function updateproduct(id)
    {
        document.getElementById("hiddenprodid").value = id;
        document.forms["frmproductslist"].submit();
    }
</script>
<?php include("header.php"); ?>
<div id="fade" class="black_overlay"></div>
<div class="content-page">
<form name="frmProductList" method="post">
    <?php echo $hiddenctr;?>
    <div class="search-container">
        <div class="form-view">
            <?php echo $ddlProviders;?> &nbsp; <?php echo $btnSubmit;?>
        </div>
    </div>
    <div class="form-actions">
        <div class="form-button"><?php echo $btnAddProduct;?></div>
    </div>
    <div class="form-page">
        <?php echo $pgTransactionHistory;?>
    </div>
</form>
<form name="frmproductslist" method="post" action="updateproduct.php">
    <?php echo $hiddenprodid;?><br/>
        <table class="table-list">
            <tr>
                <th>Product</th>
                <!-- <th>Product Description</th> -->
                <th>Provider</th>
                <th>Status</th>
                <th>Edit</th>
            </tr>
            <?php if(count($product_list) > 0):?>
            <?php for($ctr = 0 ; $ctr < count($product_list) ; $ctr++):?>
            <?php ($ctr % 2) == 0 ? $class = "evenrow" : $class = "oddrow"; ?>
            <tr class="<?php echo $class; ?>">
                <td><?php echo $product_list[$ctr]["ProductName"];?></td>
                <!-- <td><?php echo $product_list[$ctr]["ProductDescription"];?></td> -->
                <td><?php echo $product_list[$ctr]["Name"];?></td>
                <td><?php echo $product_list[$ctr]["Status"] == 1 ? "Active" : "Inactive";?></td>
                <td><a href="#" onclick="javascript: return updateproduct(<?php echo $product_list[$ctr]["ProductID"]?>)">Edit</a></td>
            </tr>
            <?php endfor;?>
            <?php else: ?>
            <tr class="no-record"><td colspan="5">No result to display</td></tr>
            <?php endif;?>
        </table>
        <br />
	<?php if (isset($errormsg)): ?>
	<div  class="labelboldred"><?php echo $errormsg; ?></div><br />
	<?php endif; ?>  
</form>
</div>
<!-- POP UP FOR MESSAGES -->
<div id="light" class="white_content">
    <div id="title" class="light-title"></div>
    <div id="msg" class="light-message"></div>
    <div id="button" class="light-button">
        <input type="button" onclick="javascript: document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none';" value="Okay"/>
    </div>
</div>
<!-- POP UP FOR MESSAGES -->
<?php include("footer.php"); ?>