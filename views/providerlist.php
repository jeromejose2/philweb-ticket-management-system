<?php
/*
 * @author 
 * Purpose   : view for providerlist
 */
include("init.inc.php");
$pagesubmenuid = 22;
include_once("../controller/managesession.php");
include("../controller/providerlistprocess.php");
?>
<script language="javascript" src="../jscripts/validations.js"></script>
<script language="javascript">
    function SelectedIndexChange(index)
    {
        selectedindex = document.getElementById("hdnSelectedProviderID");
        selectedindex.value = index;
        document.forms[0].submit();
    }
    function ChangePage(pagenum)
    {
        selectedindex = document.getElementById("pgSelectedPage");
        selectedindex.value = pagenum;
        document.forms[0].submit();
    }
    function updateprovider(id)
    {
        document.getElementById("hiddenprovid").value = id;
        document.forms["frmproviderslist"].submit();
    }
</script>
<?php include("header.php"); ?>
<form name="frmProviderList" method="post">
    <div class="titleCont">
            <div class="titleCont_left"></div>
            <div class="titleCont_body">Provider</div>
            <div class="titleCont_right"></div>
    </div>
    <div class="content-page">
<!--	<form name="frmproviderlist" method="post">-->
	    <?php //echo $btnAddProvider;?><br/>
	    <div class="form-button"><?php echo $pgTransactionHistory;?></div>
    </div>
    </form>
	<form action="updateprovider.php" method="post" name="frmproviderslist">
            <div class="content-page">
	    <?php echo $hiddenprovid; ?>
	    <table class="table-list">
		<tr>
            <th>Edit</th>
		    <th>Provider</th>
		    <th>Contact Number</th>
            <th>Head Office Address</th>
		</tr>
		<?php if(count($provider_list) > 0): ?>
		<?php for($ctr = 0 ; $ctr < count($provider_list) ; $ctr++): ?>
		<?php ($ctr % 2) == 0 ? $class = "evenrow" : $class = "oddrow"; ?>
		<tr class="<?php echo $class;?>">
            <td><a href="#" onclick="javascript: return updateprovider(<?php echo $provider_list[$ctr]["ProviderID"]?>);">Edit</a></td>
		    <td><?php echo $provider_list[$ctr]["Name"]; ?></td>
            <td><?php echo $provider_list[$ctr]["ContactNumber"]; ?></td>
            <td><?php echo $provider_list[$ctr]["Address"]; ?></td>
		    <!-- <td><?php echo $provider_list[$ctr]["Status"] == 1 ? "Active" : "Inactive"; ?></td> -->
		</tr>
		<?php endfor; ?>
		<?php else: ?>
		<tr>
		    <td colspan="4">No result to display</td>
		</tr>
		<?php endif; ?>
	    </table>
            <div class="form-button"><?php echo $pgTransactionHistory;?></div>
        </div>
	</form>
    <br />
    <?php if (isset($errormsg)) : ?>
    <div  class="labelboldred"><?php echo $errormsg; ?></div><br />
    <?php endif; ?>  
</form>
<?php include("footer.php"); ?>