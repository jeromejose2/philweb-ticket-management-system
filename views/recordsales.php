<?php
/*
 * @author 
 * Purpose   : view for recordsales
 */
include("../init.inc.php");
$pagesubmenuid = 6;
include_once("../controller/managesession.php");
include("../controller/recordsalesprocess.php");
?>
<script language="javascript" src="../jscripts/validations.js"></script>
<?php include("header.php"); ?>
<form name="frmCancellation" method="post">
    <div class="content-page">
        <br />
    <?php
    if ($errormsg != "") 
    { ?>
    <div  class="labelboldred"><?php echo $errormsg; ?></div><br />
    <?php } ?>  
    </div>

</form>
<?php include("footer.php"); ?>