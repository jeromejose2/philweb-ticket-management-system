<?php
/*
 * Added By : Arlene R. Salazar
 * Added On : Jan 12, 2012
 * Modified By: Noel Antonio
 * Date Modified: August 1, 2012
 * Purpose : View for redemption commission detailed view
 */
require_once ("init.inc.php");
$pagesubmenuid = 41;
include_once("../controller/managesession.php");
include("../controller/redemptioncommdetailedprocess.php");
?>
<?php include("header.php"); ?>
<script language="javascript" type="text/javascript">
    function ChangePage(pagenum)
    {
        selectedindex = document.getElementById("pgSelectedPage");
        selectedindex.value = pagenum;
        document.forms[0].submit();
    }
</script>
<div class="titleCont">
    <div class="titleCont_left"></div>
    <div class="titleCont_body">Redemption Commission</div>
    <div class="titleCont_right"></div>
</div>
<div class="content-page">
    <form action="redemptioncommdetailed.php" method="post">
        <?php echo $hiddengameid; ?>
        <?php echo $hiddenprodid; ?>
        <?php echo $hiddenuserid; ?>
        <?php echo $hiddenbatchvalidationid;?>
        <?php echo $hiddenprocessedtag;?>
        <?php echo $hiddenbatchid;?>
        <?php echo $hiddenselectedgameid;?>

        <table class="table-list" style="width:500px;">
            <tr>
                <th colspan="2">Payout Details</th>
            </tr>
            <tr class="oddrow">
                <th style ="width:250px">Retailer Username:</th>
                <td style ="width:250px;text-align: center;"><?php echo $redemptionsumm_list[0]["Username"]; ?></td>
            </tr>
            <tr class="oddrow">
                <th>Date & Time:</th>
                <td style ="width:250px;text-align: center;"><?php echo $redemptionsumm_list[0]["DateClaimed"]; ?></td>
            </tr>
        </table>
		<div class="form-page">
            <?php echo $pgTransactionHistory;?>
        </div>
        <table class="table-list">
            <tr>
				<th>Game Batch</th>
                <th>Game Number</th>
                <th>Game Name</th>
                <th>Book Ticket Number</th>
                <th>Validation Number</th>
                <th>Prize Amount</th>
            </tr>
        
        <?php if(count($redemptionsumm_list) > 0):?>
            <?php for($i = 0 ; $i < count($redemptionsumm_list) ; $i++):?>
            <?php ($i % 2) == 0 ? $class = "evenrow" : $class = "oddrow" ; ?>
            <tr class="<?php echo $class;?>">
                <td style ="text-align: center;"><?php echo $redemptionsumm_list[$i]["BatchID"]?></td>
                <td style ="text-align: center;"><?php echo $redemptionsumm_list[$i]["GameNumber"]?></td>
                <td style ="text-align: center;"><?php echo $redemptionsumm_list[$i]["ProductName"]?></td>
                <td style ="text-align: center;"><?php echo $redemptionsumm_list[$i]["TicketNumber"]?></td>
                <td style ="text-align: center;"><?php echo $redemptionsumm_list[$i]["ValidationNumber"]?></td>
                <td style ="text-align: right;"><?php echo $redemptionsumm_list[$i]["PrizeName"]?></td>
            </tr>
            <?php endfor;?>
            
            <?php if($current_page == $total_page):?>
            <tr class="<?php echo $class;?>">
                <th colspan="5">Total Prizes Redeemed:</th>
                <th style="text-align: right;"><?php echo $currency . " " . number_format($total_prizes_redeemed , 2 , '.' , ','); ?></th>
            </tr>
            <tr class="<?php echo $class;?>">
                <th colspan="5">Total Redemption Commission:</th>
                <th style="text-align: right;">
                    <?php 
                        if ($prizesredeemed[0]["AccountTypeID"] == 6  && $prizesredeemed[0]["PrizeType"] == 2)
                      { 
                            echo $currency . " " . number_format($total_redemption_comm , 2 , '.' , ',');
                        } 
                        else 
                        { 
                            echo $currency . " 0.00";
                        }
                    ?>
                </th>
            </tr>
            <?php endif;?>
        <?php else:?>
			<tr class="no-record">
                <td colspan="6">No records found</td>
            </tr>
        <?php endif;?>
        </table>
		<div class="form-page">
            <?php echo $pgTransactionHistory;?><br/>
            <?php echo $btnBack;?>
        </div>
    </form>
</div>
<div id="fade" class="black_overlay"></div>
<?php include("footer.php"); ?>