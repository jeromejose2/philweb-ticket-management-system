<?php
/*
 * @author 
 * Purpose   : view for resetpasswordnoemail
 */
include("../init.inc.php");
$pagesubmenuid = 39;
include_once("../controller/managesession.php");
include("../controller/resetpasswordnoemailprocess.php");
?>

<?php include("header.php"); ?>
<div id="fade" class="black_overlay"></div>
<div id="loading" class="loading"></div>
<!-- POP UP FOR MESSAGES -->
<div id="light" class="white_content">
    <div id="title" class="light-title"></div>
    <div id="msg" class="light-message"></div>
    <div id="button" class="light-button"><input type="button" onclick="javascript: document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none';" value="OKAY"/></div>
    <div class="light-footer"></div>
</div>
<!-- POP UP FOR MESSAGES -->

  
<div class="titleCont">
    <div class="titleCont_left"></div>
    <div class="titleCont_body">Reset Password</div>
    <div class="titleCont_right"></div>
</div>
    <?php echo $hiddenctrid;?>
    <div class="search-container">
        <br/>
        <div class="form-view">
             <form id="frmReset" name="frmReset" method="post" action="resetpassword.php">
                &nbsp;&nbsp;&nbsp;
                    Username:  <?php echo $username;?>        
                <div class="form-button">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <?php echo $btnSubmit; ?>
                </div>
            </form>
        </div>
    </div>
<script type="text/javascript" lang="Javascript">
function validate()
{
    var user = document.getElementById('username').options[document.getElementById('username').selectedIndex].value;
    if (user == 0)
    {
        document.getElementById('title').innerHTML = "ERROR";
        document.getElementById('msg').innerHTML = "Please select a user name.";
        document.getElementById('light').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    return true;
}
</script>
<?php include("footer.php"); ?>