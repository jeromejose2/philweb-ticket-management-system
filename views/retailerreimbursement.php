<?php
/*
 * Created by Noel Antonio on 02-10-2012
 * Purpose: controller for retailer reimbursement
 */
require_once ("init.inc.php");
$pagesubmenuid = 45;
include_once("../controller/managesession.php");
include("../controller/retailerreimbursementprocess.php");
?>
<script language="javascript" type="text/javascript">
    function ChangePage(pagenum)
    {
        selectedindex = document.getElementById("pgSelectedPage");
        selectedindex.value = pagenum;
        document.forms[0].submit();
    }

    function get_gamename()
    {
        var gamenum = document.getElementById('ddlGameNum').options[document.getElementById('ddlGameNum').selectedIndex].value;
        if(gamenum == 0)
        {
            document.getElementById("ddlGameName").onchange = get_gamenumber;
            document.getElementById("hiddenflag").value = 2;
        }
        else
        {
            document.getElementById("ddlGameName").onchange = null;
        }

        $("#ddlGameName").load(
            "../controller/get_gamename.php",
            {
                gamenum: gamenum
            },

            function(status, xhr)
            {
                if(status == "error")
                {
                    document.getElementById('title5').innerHTML = "ERROR!";
                    document.getElementById('msg5').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
                    document.getElementById('light5').style.display = "block";
                    document.getElementById('fade').style.display = "block";
                }
            }
        );

    }

    function get_gamenumber()
    {
        var prodid = document.getElementById('ddlGameName').options[document.getElementById('ddlGameName').selectedIndex].value;
        document.getElementById('hiddenflag').value = "1";

        if (prodid == 0)
        {
            document.getElementById("ddlGameNum").onchange = function(){
               get_gamename();
               get_batchid()
           };
        }
        else
        {
            document.getElementById("ddlGameNum").onchange = get_batchid;
        }

        $("#ddlGameNum").load(
            "../controller/get_gamenum.php",
            {
                prodid: prodid
            },
            function(status, xhr)
            {
                if(status == "error")
                {
                    document.getElementById('title5').innerHTML = "ERROR!";
                    document.getElementById('msg5').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
                    document.getElementById('light5').style.display = "block";
                    document.getElementById('fade').style.display = "block";
                }
            }
        );
    }

    function get_batchid()
    {
        var gameid = document.getElementById('ddlGameNum').options[document.getElementById('ddlGameNum').selectedIndex].value;
        var prodid = document.getElementById('ddlGameName').options[document.getElementById('ddlGameName').selectedIndex].value;
        
        if(prodid == 0)
        {
            document.getElementById('hiddenflag').value = "2";
        }
        
        $("#ddlBatchID").load(
            "../controller/get_gamebatch.php",
            {
                gamenum: gameid,
                xmltype: 2
            },
            function(status, xhr)
            {
                if(status == "error")
                {
                    document.getElementById('title5').innerHTML = "ERROR!";
                    document.getElementById('msg5').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
                    document.getElementById('light5').style.display = "block";
                    document.getElementById('fade').style.display = "block";
                }
            }
        );
    }

    function viewCommissionDtls(commid,gameid)
    {
        document.getElementById('hiddenbatchvalidationid').value = commid;
        document.getElementById('hiddengameid').value = gameid;
        document.forms["commsummlist"].submit();
    }
    
    function reimburse(batchsessionid)
    {
        document.getElementById('hiddenbatchsessionid').value = batchsessionid;
        document.getElementById('title2').innerHTML = "RETAILER REIMBURSEMENT";
        document.getElementById('msg2').innerHTML = "Continue?";
        document.getElementById('light2').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
    }
</script>
<?php include("header.php"); ?>
<!-- POP UP FOR MESSAGES -->
<div id="light" class="white_content">
    <div id="title" class="light-title"></div>
    <div id="msg" class="light-message"></div>
    <div id="button" class="light-button">
        <input type="button" onclick="javascript: document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none';" value="Okay"/>
    </div>
    <div class="light-footer"></div>
</div>
<!-- POP UP FOR MESSAGES -->

<div class="titleCont">
    <div class="titleCont_left"></div>
    <div class="titleCont_body">Retailer Reimbursement</div>
    <div class="titleCont_right"></div>
</div>
<div class="content-page">
    <form name="frmreimburse" action="retailerreimbursement.php" method="post">
        <!-- POP UP FOR CONFIRMATION MESSAGES -->
        <div id="light2" class="white_content">
            <div id="title2" class="light-title"></div>
            <div id="msg2" class="light-message"></div>
            <div id="button" class="light-button">
                <?php echo $btnOkay; ?>&nbsp;
                <input type="button" onclick="javascript: document.getElementById('light2').style.display='none';document.getElementById('fade').style.display='none';" value="Cancel"/>
            </div>
            <div class="light-footer"></div>
        </div>
        <!-- POP UP FOR CONFIRMATION MESSAGES -->
        <?php echo $hiddenflag;?>
        <?php echo $hiddenbatchsessionid; ?>
        <table>
            <tr>
                <td width="250">Username: &nbsp;&nbsp;&nbsp;<?php echo $ddlUsername;?></td>
                <td width="300">Game Name: &nbsp;&nbsp;&nbsp;<?php echo $ddlGameName;?></td>
                <td width="200">Game Number: &nbsp;&nbsp;&nbsp;<?php echo $ddlGameNum;?></td>
                <td width="250">Game Batch: &nbsp;&nbsp;&nbsp;<?php echo $ddlBatchID;?></td>
                <td><?php echo $btnSubmit;?></td>
            </tr>
        </table>
    
    <?php if (isset($commissionsummlist)):?>
        <div class="form-page">
            <?php echo $pgTransactionHistory;?><br/><br/>
            <?php //echo $btnExport;?><br/>
			<?php echo $btnExportCSV;?>
        </div>
	</form>
        <form action="retailreimbursedetails.php" method="post" name="commsummlist">
            <?php echo $hiddengameid;?>
            <?php echo $hiddenprodid;?>
            <?php echo $hiddenuserid;?>
            <?php echo $hiddenbatchvalidationid;?>
            <?php echo $hiddenprocessedtag;?>
            <?php echo $hiddenbatchid;?>
            <table class="table-list">
                <tr>
                    <th>Game Batch</th>
                    <th>Username</th>
                    <th>Date & Time</th>
                    <th>Total Prize Redeemed</th>
                    <th>Total Redemption Commission</th>
                    <th>Status</th>
                    <th colspan="100">Option</th>
                </tr>

            <?php if(count($commissionsummlist) > 0):?>
                <?php for($i = 0 ; $i < count($commissionsummlist) ; $i++):?>
                <?php ($i % 2) == 0 ? $class = "evenrow" : $class = "oddrow" ; ?>
                <tr class="<?php echo $class;?>">
                    <td><?php echo $commissionsummlist[$i]["BatchID"];?></td>
                    <td><?php echo $commissionsummlist[$i]["Username"];?></td>
                    <td><?php echo $commissionsummlist[$i]["DateValidated"];?></td>
                    <td style="text-align: right;"><?php echo "$" . number_format($commissionsummlist[$i]["Total"]);?></td>
                    <td style="text-align: right;"><?php echo "$" . number_format(($commissionsummlist[$i]["Total"] * ($incentive / 100)) , 2 , "." , ",");?></td>
                    <?php if ($commissionsummlist[$i]["Status"] == 2){ ?>
                    <td>Redeemed</td>
                    <td style="text-align: center;"><input id="btnReimburse" type="button" value="Reimburse" onclick="reimburse(<?php echo $commissionsummlist[$i]["BatchValidationSessionID"];?>);" /></td>
                    <?php } else {?>
                    <td>Reimbursed</td>
                    <?php }?>
                    <td colspan="100" style="text-align: center;"><input type="button" value="View Details" onclick="viewCommissionDtls(<?php echo $commissionsummlist[$i]["BatchValidationSessionID"];?>,<?php echo $commissionsummlist[$i]["GameID"]?>)"/></td>
                </tr>
                <?php endfor;?>
            <?php else:?>
                <tr class="no-record">
                    <td colspan="7">No Record Found</td>
                </tr>
            <?php endif;?>
            </table>
        </form>
		<br/><div class="form-page">
            <?php echo $pgTransactionHistory;?>
        </div>
    <?php endif;?>
</div>
<div id="fade" class="black_overlay"></div>
<?php include("footer.php"); ?>
<script type="text/javascript">
<?php if (isset ($issuccessful)): ?>
        document.getElementById('title').innerHTML = "SUCCESSFUL";
        document.getElementById('msg').innerHTML = "Tickets successfully reimbursed.";
        document.getElementById('light').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
<?php endif;?>
    
<?php if (isset($errormsg)): ?>
        document.getElementById('title').innerHTML = <?php echo $errortitle;?>;
        document.getElementById('msg').innerHTML = <?php echo $errormsg;?>;
        document.getElementById('light').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
<?php endif;?>
</script>