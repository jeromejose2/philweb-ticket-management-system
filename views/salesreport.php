<?php
/*
 * Author: Noel Antonio
 * Date Created: 2012-01-06
 * Company: Philweb Corporation
 */
include("init.inc.php");
$pagesubmenuid = 40;
include_once("../controller/managesession.php");
include("../controller/salescontroller.php");
?>

<?php include("header.php"); ?>
        <link rel="stylesheet" type="text/css" href="css/datepicker.css" />
        <link rel="stylesheet" type="text/css" href="css/default.css" />
        <script type="text/javascript" src="jscripts/datetimepicker.js"></script>
        <script type="text/javascript" src="jscripts/jquery-1.5.2.min.js" lang="Javascript"></script>
        <script language="javascript" type="text/javascript">  
            $(document).ready(function(){
                var rpttype = document.getElementById('ddlrpttype').options[document.getElementById('ddlrpttype').selectedIndex].value;
                if (rpttype == 1 || rpttype == 2 || rpttype == 3)
                {
                    $('#ddlUsername').attr('disabled', true);
                }
                else {
                    $('#ddlUsername').attr('disabled', false);
                }
            });

            function checkreporttype()
            {
                var rpttype = document.getElementById('ddlrpttype').options[document.getElementById('ddlrpttype').selectedIndex].value;
                if (rpttype == 1 || rpttype == 2 || rpttype == 3)
                {
                    $('#ddlUsername').attr('disabled', true);
                }
                else {
                    $('#ddlUsername').attr('disabled', false);
                }
            }

            function get_gamename()
            {
                    document.getElementById("flag").value = 1;
                    var cardvalue = document.getElementById('ddlcardvalue').options[document.getElementById('ddlcardvalue').selectedIndex].value;
                    var gamename = document.getElementById('ddlgamename').options[document.getElementById('ddlgamename').selectedIndex].value;
                    var flag = document.getElementById("flag").value;

                    $("#ddlgamename").load(
                        "../controller/get_gamename.php",
                        {
                            cardvalue: cardvalue,
                            gamename: gamename,
                            flag: flag
                        },

                        function(response, status, xhr)
                        {
                            if (status == "success")
                            {
                                $('#ddlgamename option[value="0"]').remove();
                                $("#ddlgamename").prepend("<option selected value=\"0\" >ALL</option>");

                                $("#ddlgametype").empty();
                                $("#ddlgametype").prepend("<option selected value=\"0\" >ALL</option>");
                                
                                $("#ddlgamebatch").empty();
                                $("#ddlgamebatch").prepend("<option selected value=\"0\" >ALL</option>");
                            }
                            if(status == "error")
                            {
                                document.getElementById('title').innerHTML = "ERROR!";
                                document.getElementById('msg').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
                                document.getElementById('light').style.display = "block";
                                document.getElementById('fade').style.display = "block";
                            }
                        }
                    );
                
                    /*
                     * Previous Ajax Call for Game Name
                     * 
                    document.getElementById("ddlgamename").onchange = null;
                    document.getElementById("xmltype").value = 1;  
                    document.getElementById("flag").value = 2;
                    var gamenum = document.getElementById('ddlgametype').options[document.getElementById('ddlgametype').selectedIndex].value;
                    if (gamenum ==0)
                    {
                        document.getElementById("ddlgamename").onchange = get_gamenumber; 
                    }
                    $("#ddlgamename").load(
                        "../controller/get_gamenamewithdefall.php",
                        {
                            gamenum: gamenum
                        },

                        function(status, xhr)
                        {
                            if(status == "error")
                            {
                                document.getElementById('title').innerHTML = "ERROR!";
                                document.getElementById('msg').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
                                document.getElementById('light').style.display = "block";
                                document.getElementById('fade').style.display = "block";
                            }
                        }
                    );*/
            }

            function get_gamenumber()
            {
                    var prodid = document.getElementById('ddlgamename').options[document.getElementById('ddlgamename').selectedIndex].value;
                    var prodid1 = prodid;

                    if (prodid == 0)
                    {
                        $('#ddlgametype').empty();
                        $("#ddlgametype").prepend("<option selected value=\"0\" >ALL</option>");
                    }
                    else 
                    {
                        $("#ddlgametype").load(
                            "../controller/get_gamenum.php",
                            {
                                prodid: prodid1
                            },

                            function(response, status, xhr)
                            {
                                if (status == "success")
                                {
                                    $('#ddlgametype option[value="0"]').remove();
                                    $("#ddlgametype").prepend("<option selected value=\"0\" >ALL</option>");
                                }
                                if(status == "error")
                                {
                                    document.getElementById('title').innerHTML = "ERROR!";
                                    document.getElementById('msg').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
                                    document.getElementById('light').style.display = "block";
                                    document.getElementById('fade').style.display = "block";
                                }
                            }
                        );
                    }
                
                    /*
                    * Previous Ajax Call for Game Number
                    * 
                    document.getElementById("ddlgametype").onchange = get_batchID;
                    document.getElementById("flag").value = 1;
                    document.getElementById("xmltype").value = 1;
                    document.getElementById("ddlgamebatch").innerHTML = "<option value ='0'>ALL</option>";
                    var prodid = document.getElementById('ddlgamename').options[document.getElementById('ddlgamename').selectedIndex].value;
                    if (prodid == 0)
                    {
                    document.getElementById("ddlgametype").onchange= function(){get_gamename();get_batchID()};     
                    }
                    $("#ddlgametype").load(
                        "../controller/get_gamenumwithdefall.php",
                        {
                            prodid: prodid
                        },
                        function(status, xhr)
                        {
                            if(status == "error")
                            {
                                document.getElementById('title5').innerHTML = "ERROR!";
                                document.getElementById('msg5').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
                                document.getElementById('light5').style.display = "block";
                                document.getElementById('fade').style.display = "block";
                            }
                        }
                    );*/
            }

            function get_batchID()
            {
                    document.getElementById("xmltype").value = 1;
                    var gamenum = document.getElementById('ddlgametype').options[document.getElementById('ddlgametype').selectedIndex].value; 
                    var prodid = document.getElementById('ddlgamename').options[document.getElementById('ddlgamename').selectedIndex].value;
                    var xmltype = document.getElementById('xmltype').value;    
                    $("#ddlgamebatch").load(
                        "../controller/get_batchidwithdefall.php",
                        {
                            gamenum: gamenum,
                            prodid: prodid,
                            xmltype:xmltype            
                        },

                        function(response, status, xhr)
                        {
                            if(status == "error")
                            {
                                document.getElementById('title').innerHTML = "ERROR!";
                                document.getElementById('msg').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
                                document.getElementById('light').style.display = "block";
                                document.getElementById('fade').style.display = "block";
                            }
                        }
                    );
            }
        </script>
        <form id="frmSales" name="frmSales" action="salesreport.php" method="POST">
            <?php echo $hiddenflag;?>
            <?php echo $xmltype;?>
            <?php echo $flag;?>
            <?php $colspan = count($arrgames) + 2;?>
            <div class="titleCont">
            <div class="titleCont_left"></div>
            <div class="titleCont_body">Sales Report</div>
            <div class="titleCont_right"></div>
            </div>
            <div class="content-page">
                <table width="100%" style="margin-left: 0.5%;">
                    <tr>
                        <td id="tdrpttype"><?php echo $ddlrpttype;?></td>
                        <td id="tdcardvalue"><?php echo $ddlcardvalue;?></td>
                        <td id="tdgamename"><?php echo $ddlgamename;?></td>
                        <td id="tdgamenum"><?php echo $ddlgametype;?></td>
                        <td id="tdbatchno"><?php echo $ddlgamebatch;?></td>
                    </tr>
                </table><br/>
                <table width="70%" style="margin-left: 0.5%;">
                    <tr> 
                        <td>
                            <?php echo $txtDateFr;?>
                            <img id="cal" name="cal" src="images/close.gif" width="16" height="16" border="0" alt="Pick a date" onClick="displayDatePicker('txtDateFr', false, 'ymd', '-');"/>
                        </td>
                        <td>
                            <?php echo $txtDateTo;?>
                            <img id="cal" name="cal" src="images/close.gif" width="16" height="16" border="0" alt="Pick a date" onClick="displayDatePicker('txtDateTo', false, 'ymd', '-');"/>
                        </td>
                        <td id="tduser"><?php echo $ddlUsername;?></td>
                        <td><?php echo $btnSubmit;?></td>
                    </tr>
                </table><br/>

                <?php if (($display == 0 || $display == 1) && isset($sales_list) || isset($sales_list2)): $colspan = count($arrgames) + 5;?>
                <div class="form-page"><?php echo $btnExportCSV;?><br/><br/></div>
                    <!--
                    <table class="table-list" style="width:45%">
                        <tr><th colspan="  <?php echo $colspan;?>">REPORT SUMMARY</th></tr>
                        <tr>
                            <th></th>
                            <?php
                                for ($i = 0;$i < count($arrgames); $i++)
                                { 
                                    $explode = explode(".", $arrgames[$i]["CardPrice"]);
                                    if ($explode[1] != "00" || $explode[1] > 00)
                                        $strcp = $arrgames[$i]["CardPrice"];
                                    else
                                        $strcp = floor($arrgames[$i]["CardPrice"]);
                                ?>
                                <th><?php echo $arrgames[$i]["ProductName"] . "<br>" . "[$" . $strcp . "]"; ?></th>
                            <?php } ?>
                            <th>Total Tickets Sold</th>
                            <th>Total Tickets Transferred</th>
                            <th>Total Tickets Recalled</th>
                            <th>Total Sales</th>
                        </tr>
                        
                         <tr class="oddrow"><td align="center">Week To Date</td>
                        <?php //for ($i = 0;$i < count($arrgames); $i++) { ?>
                            <td align="center"><?php //echo number_format($week[$i]["Sales"]);?></td>
                        <?php //} ?>
                        <td align="center"><?php //echo number_format($total_in_week);?></td>
                        <td align="center"><?php //echo number_format($total_transfer_week);?></td>
                        <td align="center"><?php //echo number_format($total_recalled_week);?></td>
                        <td align="center"><?php //echo number_format($total_sales_week);?></td>
                        </tr> 
                        
                        <tr class="evenrow"><td align="center">Month To Date</td>
                        <?php for ($i=0;$i<count($arrgames);$i++) { ?>
                            <td align="center"><?php echo number_format($mos[$i]["Sales"]);?></td>
                        <?php } ?>
                        <td align="center"><?php echo number_format($total_in_month);?></td>
                        <td align="center"><?php echo number_format($total_transfer_mos);?></td>
                        <td align="center"><?php echo number_format($total_recalled_mos);?></td>
                        <td align="center"><?php echo number_format($total_sales_month);?></td>
                        </tr>
                        
                        <tr class="oddrow"><td align="center">Year To Date</td>
                        <?php for ($i=0;$i<count($arrgames);$i++) { ?>
                            <td align="center"><?php echo number_format($yr[$i]["Sales"]);?></td>
                        <?php } ?>
                        <td align="center"><?php echo number_format($total_in_year);?></td>
                        <td align="center"><?php echo number_format($total_transfer_yr);?></td>
                        <td align="center"><?php echo number_format($total_recalled_yr);?></td>
                        <td align="center"><?php echo number_format($total_sales_year);?></td>
                        </tr>
                    </table>--><br/>
                    <?php endif; ?>
                    
                    <?php if (isset($sales_list) && $display == 0): ?>
                    <table class="table-list" style="width:45%">
                        <tr><th colspan="<?php echo $colspan;?>">TICKET SALES REPORT</th></tr>
                        <tr><th>Date</th>
                        <?php for ($i = 0;$i < count($arrgames); $i++)
                              {
                                    $explode = explode(".", $arrgames[$i]["CardPrice"]);
                                    if ($explode[1] != "00" || $explode[1] > 00)
                                        $strcp = $arrgames[$i]["CardPrice"];
                                    else
                                        $strcp = floor($arrgames[$i]["CardPrice"]);
                        ?>
                            <th><?php echo $arrgames[$i]["ProductName"] . "<br>" . "[$" . $strcp . "]"; ?></th>
                        <?php } ?>
                        <th>Total Tickets Sold</th>
                        <th>Total Tickets Transferred</th>
                        <th>Total Tickets Recalled</th>
                        <th>Total Sales</th>
                        </tr>
                        
                        <?php
                            if (is_array($sales_list))
                            {
                                $total_tickets_sold = 0;
                                $total_tickets_transfer = 0;
                                $total_tickets_recalled = 0;
                                $total_sales = 0;
                                $array_temp = array();
                                
                                foreach ($sales_list as $date => $val) 
                                {  
                                    ($i % 2) == 0 ? $bgcolor = "oddrow" : $bgcolor = "evenrow" ;
                                    if ($rpttypeval == 1) { echo '<tr class="'.$bgcolor.'"><td align="center">'.date("Y-M-d", strtotime($date)).'</td>';}
                                    elseif ($rpttypeval == 2 || $rpttypeval == 3) { echo '<tr class="'.$bgcolor.'"><td align="center">'.$date.'</td>';}
                                    //foreach ($val as $game => $sales) 
                                    for ($a = 0; $a < count($arrgames); $a++)
                                    {
                                        $game = $arrgames[$a]["ProductName"];
                                        echo '<td align="center">'.number_format($val[$game]["Sales"]).'</td>';
                                        
                                        $total_tickets_sold += $val[$game]["Sales"];
                                        $total_tickets_transfer += $val[$game]["Transfer"];
                                        $total_tickets_recalled += $val[$game]["Recalled"];
                                        $total_sales += $val[$game]["Total"];
                                    }
                                    
                                    echo '<td align="center">'. number_format($total_tickets_sold) .'</td>';
                                    echo '<td align="center">'. number_format($total_tickets_transfer) .'</td>';
                                    echo '<td align="center">'. number_format($total_tickets_recalled) .'</td>';
                                    echo '<td align="center">'. number_format($total_sales) .'</td>';
                                    
                                    $i++;
                                    $total_tickets_sold = 0;
                                    $total_tickets_transfer = 0;
                                    $total_tickets_recalled = 0;
                                    $total_sales = 0;
                                }
                                echo '</tr>';
                            }
                        ?>
                    <tr/></table><br/>
                    <?php endif;?>
                    
                    <?php if (isset($sales_list2) && $display == 1): $colspan = count($temp) + 3;?>
                    <table class="table-list" style="width:45%">
                        <tr><th colspan="<?php echo $colspan;?>">TICKET SALES REPORT</th></tr>
                        <tr><th>Distributor</th>
                        <?php 
                            foreach ($sales_list2 as $distr => $val) {
                                foreach ($val as $date => $sales) {
                                    if ($rpttypeval == 4) { echo '<th align="center">'.date("M d", strtotime($date)).'</th>';}
                                    elseif ($rpttypeval == 5 || $rpttypeval == 6) { echo '<th align="center">'.$date.'</th>';} 
                                } break; 
                            } 
                        ?>
                        <th>Total Tickets Sold</th>
                        <th>Total Sales</th>
                        </tr>
                        
                    <?php
                        $total_tickets_sold = 0;
                        $total_sales = 0;
                        $array_temp = array();
                        
                        foreach ($sales_list2 as $distr => $val)
                        {
                            ($i % 2) == 0 ? $bgcolor = "oddrow" : $bgcolor = "evenrow" ;
                            echo '<tr class="'.$bgcolor.'"><td align="center">'.$distr.'</td>';
                            foreach ($val as $date => $sales)
                            {
                                echo '<td>
                                        <strong>Sold: </strong>'.number_format($sales["Sales"]).'<br/>
                                        <strong>Transferred: </strong>'.number_format($sales["Transfer"]).'<br/>
                                        <strong>Recalled: </strong>'.number_format($sales["Recalled"]).'
                                      </td>';
                                
                                $total_tickets_sold += $sales["Sales"];
                                $total_sales += $sales["Total"];
                                
                                if(!isset($array_temp[$date]))
                                    $array_temp[$date] = $sales["Sales"];
                                else
                                    $array_temp[$date] += $sales["Sales"];
                            }
                            
                            echo '<td align="center">'.number_format($total_tickets_sold).'</td>';
                            echo '<td align="center">'.number_format($total_sales).'</td>';
                            
                            $i++;
                            $total_tickets_sold = 0;
                            $total_sales = 0;
                        }
                        echo '</tr>';
                    ?>
                    <tr/></table><br/>
                    <?php endif;?>
                    
                    <?php if (($display == 0 || $display == 1) && isset($sales_list) || isset($sales_list2)):?>
                    <table class="table-list">
                        <th>Sales Graph</th>
                        <tr>
                            <td align="center">
                                <iframe src="images_sales/salesgraph1.png" width="950" height="500"></iframe>
                            </td>
                        </tr>
                    </table><br/>
                    <?php endif;?>
            </div>
            
            <div id="fade" class="black_overlay"></div>
            <div id="loading" class="loading"></div>
            <!-- POP UP FOR MESSAGES -->
            <div id="light5" class="white_content">
                <div id="title5" class="light-title"></div>
                <div id="msg5" class="light-message"></div>
                <div id="button" class="light-button"><input type="button" onclick="javascript: document.getElementById('light5').style.display='none';document.getElementById('fade').style.display='none';" value="OKAY"/></div>
                <div class="light-footer"></div>
            </div>
            <!-- POP UP FOR MESSAGES -->
        </form>
<?php include("footer.php"); ?>

<script language="javascript" type="text/javascript">
function validate()
{
    var SDate = document.getElementById("txtDateFr").value;
    var EDate = document.getElementById("txtDateTo").value;
    var rpttype =document.getElementById("ddlrpttype").options[document.getElementById('ddlrpttype').selectedIndex].value;
    var endDate = new Date(EDate);
    var startDate = new Date(SDate);
    var currDate = new Date();

    var days_between = 0;
    days_between = (endDate.getTime() - startDate.getTime()) / 86400000;
    if(SDate != '' && EDate != '' && startDate > endDate)
    {
    	document.getElementById('title5').innerHTML = "ERROR";
        document.getElementById('msg5').innerHTML = "Start date is ahead than the End Date.";
        document.getElementById('light5').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    else if (rpttype == 0)
    {
        document.getElementById('title5').innerHTML = "ERROR";
        document.getElementById('msg5').innerHTML = "Please select report type.";
        document.getElementById('light5').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    else if (SDate.trim() == "")
    {
        document.getElementById('title5').innerHTML = "ERROR";
        document.getElementById('msg5').innerHTML = "Please select your Start Date.";
        document.getElementById('light5').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    else if (EDate.trim() == "")
    {
        document.getElementById('title5').innerHTML = "ERROR";
        document.getElementById('msg5').innerHTML = "Please select your End Date.";
        document.getElementById('light5').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    else if (endDate > currDate || startDate > currDate)
    {
        document.getElementById('title5').innerHTML = "ERROR";
        document.getElementById('msg5').innerHTML = "The date is ahead of time.";
        document.getElementById('light5').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    else if (rpttype == 1 || rpttype == 4)
    {
        if (days_between >= 7)
        {
            document.getElementById('title5').innerHTML = "ERROR";
            document.getElementById('msg5').innerHTML = "Date range selection should not exceed within 7 days.";
            document.getElementById('light5').style.display = 'block';
            document.getElementById('fade').style.display = 'block';
            return false;
        }
    }
    else if (rpttype == 2 || rpttype == 3 || rpttype == 5 || rpttype == 6)
    {
        if (days_between >= 31)
        {
            document.getElementById('title5').innerHTML = "ERROR";
            document.getElementById('msg5').innerHTML = "Date range selection should not exceed within 30 days.";
            document.getElementById('light5').style.display = 'block';
            document.getElementById('fade').style.display = 'block';
            return false;
        }
    }
    
    return true;
}
</script>