<?php
/*
 * Author: Noel Antonio
 * Date Created: 2011-08-31
 * Company: Philweb Corporation
 */
include("init.inc.php");
$pagesubmenuid = 11;
include_once("../controller/managesession.php");
include("../controller/settlementcontroller.php");

?>

<?php include("header.php"); ?>
        <link rel="stylesheet" type="text/css" href="css/datepicker.css" />
        <link rel="stylesheet" type="text/css" href="css/default.css" />
        <script type="text/javascript" src="jscripts/datetimepicker.js"></script>
        <script type="text/javascript" src="jscripts/jquery-1.5.2.min.js" lang="Javascript"></script>
        <script language="javascript" type="text/javascript">
            function ChangePage(pagenum)
            {
                selectedindex = document.getElementById("pgSelectedPage");
                selectedindex.value = pagenum;
                document.forms[0].submit();
            }
            function get_gamenumber()
            {
                document.getElementById("ddlgametype").onchange = get_batchID;
                document.getElementById("flag").value = 1;
                document.getElementById("xmltype").value = 2;                   // * previously 1
                document.getElementById("ddlgamebatch").innerHTML = "<option value ='0'>ALL</option>";
                var prodid = document.getElementById('ddlgamename').options[document.getElementById('ddlgamename').selectedIndex].value;
                 if (prodid == 0)
                {
                   document.getElementById("ddlgametype").onchange= function(){get_gamename();get_batchID()};     
                }
                $("#ddlgametype").load(
                    "../controller/get_gamenumwithdefall.php",
                    {
                        prodid: prodid
                    },
                    function(status, xhr)
                    {
                        if(status == "error")
                        {
                            document.getElementById('title5').innerHTML = "ERROR!";
                            document.getElementById('msg5').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
                            document.getElementById('light5').style.display = "block";
                            document.getElementById('fade').style.display = "block";
                        }
                    }
                );
            }
            function get_batchID()
            {
                document.getElementById("xmltype").value = 2;                   // * previously 1
                var gamenum = document.getElementById('ddlgametype').options[document.getElementById('ddlgametype').selectedIndex].value; 
                var prodid = document.getElementById('ddlgamename').options[document.getElementById('ddlgamename').selectedIndex].value;
                var xmltype = document.getElementById('xmltype').value;    
                $("#ddlgamebatch").load(
                    "../controller/get_batchidwithdefall.php",
                    {
                        gamenum: gamenum,
                        prodid: prodid,
                        xmltype:xmltype            
                    },

                    function(status, xhr)
                    {
                        if(status == "error")
                        {
                            document.getElementById('title').innerHTML = "ERROR!";
                            document.getElementById('msg').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
                            document.getElementById('light').style.display = "block";
                            document.getElementById('fade').style.display = "block";
                        }
                    }
                );
            }
            function get_gamename()
            {
               document.getElementById("ddlgamename").onchange = null;
               document.getElementById("xmltype").value = 2;                    // * previously 1  
               document.getElementById("flag").value = 2;
               var gamenum = document.getElementById('ddlgametype').options[document.getElementById('ddlgametype').selectedIndex].value;
               if (gamenum ==0)
                   {
                      document.getElementById("ddlgamename").onchange = get_gamenumber; 
                   }
                $("#ddlgamename").load(
                    "../controller/get_gamenamewithdefall.php",
                    {
                        gamenum: gamenum
                    },

                    function(status, xhr)
                    {
                        if(status == "error")
                        {
                            document.getElementById('title').innerHTML = "ERROR!";
                            document.getElementById('msg').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
                            document.getElementById('light').style.display = "block";
                            document.getElementById('fade').style.display = "block";
                        }
                    }
                );
            }
        </script>
        <form action="settlementreport.php" method="POST">
            <div class="titleCont">
            <div class="titleCont_left"></div>
            <div class="titleCont_body">Payout History</div>
            <div class="titleCont_right"></div>
            </div>
            <div class="content-page">
                <table width ="100%" style="margin-left: 0.5%;">
                        <tr>
                            <td><?php echo $cboAccount;?></td>
                        </tr>
                        </table><table width="100%" style="margin-left: 0.5%;">
                        <tr>
                            
                            <td><?php echo $txtDateFr;?><?php echo $flag;?><?php echo $xmltype;?>
                            <img name="cal" src="images/close.gif" width="16" height="16" border="0" alt="Pick a date" onClick="displayDatePicker('txtDateFr', false, 'ymd', '-');"/></td>
                            <td><?php echo $txtDateTo;?>
                            <img name="cal" src="images/close.gif" width="16" height="16" border="0" alt="Pick a date" onClick="displayDatePicker('txtDateTo', false, 'ymd', '-');"/></td>
                            <td><?php echo $ddlgamename;?></td>
                            <td><?php echo $ddlgametype;?></td>
                            <td><?php echo $ddlgamebatch;?></td>
                            <!-- <td><?php echo $txtGameName;?></td> -->
                            <td><?php echo $btnSubmit;?></td>
                        </tr>
                    </table><br/>

                  
                    <table class="table-list" style="width:500">
                        <tr>
                            <th colspan="2">Payout Summary</th>
                        </tr>
                        <tr class="oddrow">
                            <th style ="width:250px">Total Payout Processed:</th>
                            <td style ="width:250px;text-align: right;"><?php echo $currency . number_format($total_payout_processed , 2 , "." , ","); ?></td>
                        </tr>
                        <tr class="evenrow">
                            <th>Redemption Incentive Rate:</th>
                            <td style ="width:250px;text-align: right;"><?php echo $incentive . "%"; ?></td>
                        </tr>
                        <tr class="oddrow">
                            <th>Redemption Incentive:</th>
                            <td style ="width:250px;text-align: right;"><?php echo $currency . $totalincentive; ?></td>
                        </tr>
                    </table>
                      <?php if (isset($win_list)):?>
                    <table class="table-list" style="text-align: center">
                        <tr align="center">
                            <th>Date</th>
                            <th>Account</th>
                            <th>Card Value</th>
                            <th>Game Number</th>
                            <th>Game Name</th>
                            <th>Game Batch</th>
                            <th>Book Ticket Number</th>
                            <th>Validation Number</th>
                            <th>Prize Won</th>
                        </tr>
                        <?php if (count($win_list) == 0):?>
                            <tr class="no-record"><td colspan="8">No records to display</td></tr>
                        <?php endif;?>
                            
                        <div class="form-page"><?php //echo $btnExport;?><?php echo $btnExportCSV;?><br/></div>
                        <div class="form-page"><?php echo $pgTransactionHistory;?></div>
                        <?php for($ctr=0; $ctr < count($win_list); $ctr++){ ?>
                        <?php ($ctr % 2) == 0 ? $class = "evenrow" : $class = "oddrow" ; ?>
                        <tr class = "<?php echo $class?>">
                            <?php $dateTime = new DateTime($win_list[$ctr]['DateClaimed']);?>
                            <td><?php echo $dateTime->format('Y-m-d h:i:s A');?></td>
                            <td><?php echo $win_list[$ctr]['UserName'];?></td>
                            <td><?php echo $win_list[$ctr]['CardPrice'];?></td>
                            <td><?php echo $win_list[$ctr]['GameNumber'];?></td>
                            <td><?php echo $win_list[$ctr]['ProductName'];?></td>
                            <td><?php echo $win_list[$ctr]['BatchID'];?></td>
                            <td><?php echo $win_list[$ctr]['TicketNumber'];?></td>
                            <td><?php echo $win_list[$ctr]['ValidationNumber'];?></td>
                            <td><?php echo $win_list[$ctr]['PrizeName'];?></td>
                        </tr>
                        <?php }?>
                    </table>
                    <?php endif;?>
            </div>
            
            <div id="fade" class="black_overlay"></div>
            <div id="loading" class="loading"></div>
            <!-- POP UP FOR MESSAGES -->
            <div id="light5" class="white_content">
                <div id="title5" class="light-title"></div>
                <div id="msg5" class="light-message"></div>
                <div id="button" class="light-button"><input type="button" onclick="javascript: document.getElementById('light5').style.display='none';document.getElementById('fade').style.display='none';" value="OKAY"/></div>
                <div class="light-footer"></div>
            </div>
            <!-- POP UP FOR MESSAGES -->
        </form>
        <div class="content-page"><div class="form-page"><?php echo $pgTransactionHistory;?></div></div>
<?php include("footer.php"); ?>

<script language="javascript" type="text/javascript">
function validate()
{
    var SDate = document.getElementById("txtDateFr").value;
    var EDate = document.getElementById("txtDateTo").value;

    var endDate = new Date(EDate);
    var startDate = new Date(SDate);
    var currDate = new Date();

    if(SDate != '' && EDate != '' && startDate > endDate)
    {
    	document.getElementById('title5').innerHTML = "ERROR";
        document.getElementById('msg5').innerHTML = "Start date is ahead than the End Date.";
        document.getElementById('light5').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    else if (SDate.trim() == "")
    {
        document.getElementById('title5').innerHTML = "ERROR";
        document.getElementById('msg5').innerHTML = "Please select your Start Date.";
        document.getElementById('light5').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    else if (EDate.trim() == "")
    {
        document.getElementById('title5').innerHTML = "ERROR";
        document.getElementById('msg5').innerHTML = "Please select your End Date.";
        document.getElementById('light5').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    else if (endDate > currDate)
    {
        document.getElementById('title5').innerHTML = "ERROR";
        document.getElementById('msg5').innerHTML = "The date is ahead of time.";
        document.getElementById('light5').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    else{ return true;}
}
</script>