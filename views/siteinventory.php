<?php
/*
 * Author: Noel Antonio
 * Date Created: 2011-08-31
 * Company: Philweb Corporation
 */
include("init.inc.php");
$pagesubmenuid = 5;
include_once("../controller/managesession.php");
include("../controller/sitecontroller.php");

?>
 <script type="text/javascript" src="jscripts/jquery-1.5.2.min.js"></script>
<?php include("header.php"); ?>
        <link rel="stylesheet" type="text/css" href="css/datepicker.css" />
        <link rel="stylesheet" type="text/css" href="css/default.css" />
        <script type="text/javascript" src="jscripts/datetimepicker.js"></script>
        <script type="text/javascript" src="jscripts/jquery-1.5.2.min.js" lang="Javascript"></script>
        <script language="javascript" type="text/javascript">
            function ChangePage(pagenum)
            {
                selectedindex = document.getElementById("pgSelectedPage");
                selectedindex.value = pagenum;
                document.forms[0].submit();
            }
            function SelectGameName()
            {
                var pid = document.getElementById('ddlgametype').options[document.getElementById('ddlgametype').selectedIndex].value;

                $.ajax
                ({
                    url: '../controller/get_product.php?pid='+pid,
                    type : 'post',
                    success : function(data)
                    {
                        $('#txtGameName').val(data)
                    },
                    error: function(e)
                    {
                        alert("Error");
                    }
                });
                /*jQuery('form').attr('action','siteinventory.php?changecbo=1');
                document.forms[0].submit();*/
            }
$(document).ready(function(){
    
        $("#ddlgamename").attr('disabled',true);
        $("#ddlgametype").attr('disabled',true); //cbo GameID
        $("#ddlgametype").attr('disabled',true);
        $("#ddlgamebatch").attr('disabled',true);
   var cardvalue = document.getElementById('ddlcardvalue').options[document.getElementById('ddlcardvalue').selectedIndex].value;
            if(cardvalue != ' ')
                 {
                     $("#ddlgamename").attr('disabled',false);
                     $("#ddlgametype").attr('disabled',false);
                     $("#ddlgamebatch").attr('disabled',false);
                     document.getElementById("flag").value = 4;
                 }
        
});
function get_gamename()
{
    document.getElementById("flag").value = 2;
    $("#ddlgamename").attr('disabled',false);
    var setTodefault = document.getElementById ('ddlgamename');
        setTodefault.value = 0; //set to default -please select
    var cardvalue = document.getElementById('ddlcardvalue').options[document.getElementById('ddlcardvalue').selectedIndex].value;
    var gamename = document.getElementById('ddlgamename').options[document.getElementById('ddlgamename').selectedIndex].value;
    var gamenum = document.getElementById('ddlgametype').options[document.getElementById('ddlgametype').selectedIndex].value;
    var gamebatch = document.getElementById('ddlgamebatch').options[document.getElementById('ddlgamebatch').selectedIndex].value;
    var flag = 0;
         if(cardvalue == ' ')
         {  
            $("#ddlgametype").val($("#ddlgametype option:first").val());
            $("#ddlgamebatch").val($("#ddlgamebatch option:first").val());
            
            $("#ddlgamename").attr('disabled',true);
            $("#ddlgametype").attr('disabled',true);
            $("#ddlgamebatch").attr('disabled',true);
            
         }
          if(cardvalue != ' ' && (gamename != 0 || gamename == 0) && (gamenum != 0 || gamenum == 0) && (gamebatch != 0 || gamebatch == 0))
            {    
                 $("#ddlgametype").val($("#ddlgametype option:first").val());
                 $("#ddlgamebatch").val($("#ddlgamebatch option:first").val());
                 $("#ddlgametype").attr('disabled',true);
                 $("#ddlgamebatch").attr('disabled',true);
            }
    $("#ddlgamename").load(
        "../controller/get_gamename.php",
        {
            cardvalue: cardvalue,
            gamename: gamename,
            flag: flag
        },

        function(status, xhr)
        {
            if(status == "error")
            {
                document.getElementById('title').innerHTML = "ERROR!";
                document.getElementById('msg').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
                document.getElementById('light').style.display = "block";
                document.getElementById('fade').style.display = "block";
            }
        }
    );
}

function get_gamenumber()
{
    document.getElementById("flag").value = 1;
    $("#ddlgametype").attr('disabled',false);
    document.getElementById("xmltype").value = 1;
    document.getElementById("ddlgamebatch").innerHTML = "<option value ='0'>Please select</option>";
    var prodid = document.getElementById('ddlgamename').options[document.getElementById('ddlgamename').selectedIndex].value;
    var cardvalue = document.getElementById('ddlcardvalue').options[document.getElementById('ddlcardvalue').selectedIndex].value;
     var gamenum = document.getElementById('ddlgametype').options[document.getElementById('ddlgametype').selectedIndex].value;
     var gamebatch = document.getElementById('ddlgamebatch').options[document.getElementById('ddlgamebatch').selectedIndex].value;
    var prodid1 = prodid;
            $("#ddlgametype").val($("#ddlgametype option:first").val());
            $("#ddlgamebatch").val($("#ddlgamebatch option:first").val());
     if(prodid == 0)
         {
            $("#ddlgametype").val($("#ddlgametype option:first").val());
            $("#ddlgamebatch").val($("#ddlgamebatch option:first").val());
            $("#ddlgametype").attr('disabled',true);
            $("#ddlgamebatch").attr('disabled',true);
         }
            if(gamebatch == 0 && gamenum != 0)
                {
                    $("#ddlgamebatch").attr('disabled',true);
                }
    if ( prodid ==0 || cardvalue==0 && prodid !=0 && cardvalue==0 )
    { 
       var prodid1 = 0;
    }
       if(cardvalue == 0)
        {
                            $("#ddlgametype").load(
                        "../controller/get_gamenum.php",
                        {
                            prodid: prodid
                            
                        },
                        function(status, xhr)
                        {
                            if(status == "error")
                            {
                                document.getElementById('title').innerHTML = "ERROR!";
                                document.getElementById('msg').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
                                document.getElementById('light').style.display = "block";
                                document.getElementById('fade').style.display = "block";
                            }
                        }
                    );
        }else
        {
            
                $("#ddlgametype").load(
                    "../controller/get_gamenum_1.php",
                    {
                        prodid: prodid
                        ,cardvalue : cardvalue
                    },
                    function(status, xhr)
                    {
                        if(status == "error")
                        {
                            document.getElementById('title').innerHTML = "ERROR!";
                            document.getElementById('msg').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
                            document.getElementById('light').style.display = "block";
                            document.getElementById('fade').style.display = "block";
                        }
                    }
                );
        }
}
            
            /*Added by: Sheryl S.Basbas
             *Date: Jan 25, 2012 
             */
            

function get_batchID()
{
    $("#ddlgamebatch").attr('disabled',false);
    document.getElementById("xmltype").value = 1;
    var gamenum = document.getElementById('ddlgametype').options[document.getElementById('ddlgametype').selectedIndex].value;   
    var xmltype = document.getElementById('xmltype').value;    
     if(gamenum == 0)
         {
            $("#ddlgamebatch").attr('disabled',true);
         }
    $("#ddlgamebatch").load(
        "../controller/get_gamebatches.php",
        {
            gamenum: gamenum,
            xmltype:xmltype            
        },
            
        function(status, xhr)
        {
            if(status == "error")
            {
                document.getElementById('title').innerHTML = "ERROR!";
                document.getElementById('msg').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
                document.getElementById('light').style.display = "block";
                document.getElementById('fade').style.display = "block";
            }
        }
    );
}
           
            /*Added by: Sheryl S.Basbas*/
        </script>
        <form action="siteinventory.php" method="POST">
            <?php echo $flag;?>
            <?php echo $xmltype;?>
             <?php echo $hidden;?>
             <div class="titleCont">
            <div class="titleCont_left"></div>
            <div class="titleCont_body">Distributor Inventory</div>
            <div class="titleCont_right"></div>
            </div>
            <div class="content-page">
    <div class="search-container">
        <div class="form-search">
           <?php echo $txtBookNo; ?><?php echo $btnSearch; ?>
        </div>
        <br/>
        <br/>
        <div class="form-view">
         <table width="120%">
             <tr>
            <td>Card Value: <?php echo $ddlcardvalue;?></td>
            <td><?php echo $cboAccount;?></td>
            <td> <?php echo $txtDateFr;?><img name="cal" src="images/close.gif" width="16" height="16" border="0" alt="Pick a date" onClick="displayDatePicker('txtDateFr', false, 'ymd', '-');"/></td>
            <td><?php echo $txtDateTo;?><img name="cal" src="images/close.gif" width="16" height="16" border="0" alt="Pick a date" onClick="displayDatePicker('txtDateTo', false, 'ymd', '-');"/></td>
            </tr>
            <tr>
            <td><?php echo $ddlgamename; ?></td>    
            <td><?php echo $ddlgametype; ?></td>
            <td><?php echo $ddlgamebatch; ?></td>
            <td>
            <?php echo $btnSubmit; ?>
            </td>
            </tr>          
        </table> 
        <!--<div class="form-button"><?php echo $btnSubmit; ?></div>-->
        </div>
    </div>
                <br/>
                    <?php if (isset($site_list)):?>
                    <table class="table-list" style="text-align: center">
                        <tr align="center" style="font-weight: bold;">
                            <th>Date</th>
                            <th>Card Value</th>
                            <th>Game Name</th>
                            <th>Book Size</th>
                            <th>Game Number</th>                            
                            <th>Game Batch</th>
                            <th>Book Number</th>
                            <th>Status</th>
                            <th>Invoice Number</th>
                        </tr>
                        <?php if (count($site_list) == 0):?>
                            <tr class="no-record"><td colspan="8">No records to display</td></tr>
                        <?php endif;?>
                            
                        
                           <div class="form-page"><?php echo $pgTransactionHistory;?><br/><?php echo $btnExportCSV;?></div>    
                        <?php 
                        for($ctr=0; $ctr < count($site_list); $ctr++){ ?>
                        <?php ($ctr % 2) == 0 ? $class = "evenrow" : $class = "oddrow" ; ?>
                        <tr class = "<?php echo $class?>">
                            <?php $dateTime = new DateTime($site_list[$ctr]['DateReleased']);?>
                            <td><?php echo $dateTime->format('Y-m-d');?></td>
                            <td><?php echo $site_list[$ctr]['CardPrice'];?></td>
                            <td><?php echo $site_list[$ctr]['ProductName'];?></td>
                            <td><?php echo $site_list[$ctr]['BookTicketCount'];?></td>
                            <td><?php echo $site_list[$ctr]['GameNumber'];?></td>                            
                            <td><?php echo $site_list[$ctr]['BatchID'];?></td>
                            <td><?php echo $site_list[$ctr]['BookNumber'];?></td>
                            <td>
                                <?php if ($site_list[$ctr]['Status'] == 1){echo "Active";}
                                      elseif ($site_list[$ctr]['Status'] == 2){ echo "On Freight";} 
                                      elseif ($site_list[$ctr]['Status'] == 3){ echo "On Stock";} 
                                      elseif ($site_list[$ctr]['Status'] == 4){ echo "Assigned";} 
                                      else { echo "Cancelled";}
                                ?>
                            </td>
                            <td><?php if ($site_list[$ctr]['Status'] == 1){echo $site_list[$ctr]['InvoiceNumber'];}?></td>
                        </tr>
                        <?php } ?>
                    </table>
                        <?php endif; ?>
            </div>
            <div id="fade" class="black_overlay"></div>
            <div id="loading" class="loading"></div>
            <!-- POP UP FOR MESSAGES -->
            <div id="light5" class="white_content">
                <div id="title5" class="light-title"></div>
                <div id="msg5" class="light-message"></div>
                <div id="button" class="light-button"><input type="button" onclick="javascript: document.getElementById('light5').style.display='none';document.getElementById('fade').style.display='none';" value="OKAY"/></div>
                <div class="light-footer"></div>
            </div>
            <!-- POP UP FOR MESSAGES -->
        </form>
        <div class="content-page"><div class="form-page"><?php echo $pgTransactionHistory;?></div></div>
<?php include("footer.php"); ?>

<script language="javascript" type="text/javascript">
function validateBook()
{
    if (document.getElementById("txtBookNo").value.trim() == "")
    {
        document.getElementById('title5').innerHTML = "ERROR";
        document.getElementById('msg5').innerHTML = "Please enter valid book number.";
        document.getElementById('light5').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }else{ return true;}
}

function validateAll()
{
  
    var cboAccount = document.getElementById('cboAccount').options[document.getElementById('cboAccount').selectedIndex].value;
    var SDate = document.getElementById('txtDateFr').value;
    var EDate = document.getElementById('txtDateTo').value;
    var gamename = document.getElementById('ddlgamename').options[document.getElementById('ddlgamename').selectedIndex].value;
    var gamenum = document.getElementById('ddlgametype').options[document.getElementById('ddlgametype').selectedIndex].value;
    var gamebatch = document.getElementById('ddlgamebatch').options[document.getElementById('ddlgamebatch').selectedIndex].value;
    var cardvalue = document.getElementById('ddlcardvalue').options[document.getElementById('ddlcardvalue').selectedIndex].value;
    var accttype = '<?php echo $_SESSION['accttype']; ?>'
    var txtsearch = document.getElementById('txtBookNo').value;

    var endDate = new Date(EDate);
    var startDate = new Date(SDate);
    var currDate = new Date();
         
//   if (accttype == 1 || accttype == 3 || accttype == 4 || accttype == 6 || accttype == 7)
     if (cardvalue ==  ' ')
    {
        document.getElementById('title5').innerHTML = "ERROR";
        document.getElementById('msg5').innerHTML = "Please select card value.";
        document.getElementById('light5').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
     if (accttype == 2 || accttype == 4 || accttype == 5 || accttype == 6 || accttype == 7)
     {
     } else {
        if (cboAccount == 0)
        {
            document.getElementById('title5').innerHTML = "ERROR";
            document.getElementById('msg5').innerHTML = "Please select an account.";
            document.getElementById('light5').style.display = 'block';
            document.getElementById('fade').style.display = 'block';
            return false;
        }
    }
    if(SDate != '' && EDate != '' && startDate > endDate)
    {
    	document.getElementById('title5').innerHTML = "ERROR";
        document.getElementById('msg5').innerHTML = "Please enter a valid date range.";
        document.getElementById('light5').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    else if (SDate.trim() == "")
    {
        document.getElementById('title5').innerHTML = "ERROR";
        document.getElementById('msg5').innerHTML = "Please select your Start Date.";
        document.getElementById('light5').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    else if (EDate.trim() == "")
    {
        document.getElementById('title5').innerHTML = "ERROR";
        document.getElementById('msg5').innerHTML = "Please select your End Date.";
        document.getElementById('light5').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    else if (endDate > currDate)
    {
        document.getElementById('title5').innerHTML = "ERROR";
        document.getElementById('msg5').innerHTML = "The date is ahead of time.";
        document.getElementById('light5').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    if (gamename == 0)
    {
        document.getElementById('title5').innerHTML = "ERROR";
        document.getElementById('msg5').innerHTML = "Please select game name.";
        document.getElementById('light5').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    if (gamenum == 0)
    {
        document.getElementById('title5').innerHTML = "ERROR";
        document.getElementById('msg5').innerHTML = "Please select game number.";
        document.getElementById('light5').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    if (gamebatch == 0)
    {
        document.getElementById('title5').innerHTML = "ERROR";
        document.getElementById('msg5').innerHTML = "Please select game batch.";
        document.getElementById('light5').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    
    else{ return true;}
}

function isNumber(evt) {
      var charCode = (evt.which) ? evt.which : event.keyCode;
      if (charCode > 31 && (charCode < 48 || charCode > 57)) {
         return false;
      }
      return true;
   }
</script>