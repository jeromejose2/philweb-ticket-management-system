<?php
/* 
 * Created By: Arlene R. Salazar 03/14/2012
 * Purpose: View for splitting xml files
 */
include("init.inc.php");
$pagesubmenuid = 47;
include_once("../controller/managesession.php");
include("../controller/splitxmlprocess.php");
?>
<?php include("header.php"); ?>
<script type="text/javascript">
    function showloading()
    {
        document.getElementById('loading').style.display = "block";
        document.getElementById('fade').style.display = "block";
        document.getElementById('loading-image').style.display = "block";
        return true;
    }
    function redirectToSplitXMLPage()
    {
        document.getElementById('light2').style.display = 'none';
        document.getElementById('fade').style.display = 'none';
    }
</script>
<div id="fade" class="black_overlay"></div>
<form action="splitxml.php" method="post" id="frmSplitXml" enctype="multipart/form-data">
    <div class="titleCont">
        <div class="titleCont_left"></div>
        <div class="titleCont_body">Split XML</div>
        <div class="titleCont_right"></div>
    </div>
    <div class="content-page">
        <table>
            <td>
                    <div class="form-combobox">
                        <?php echo $ddlfiletype; ?>
                    </div>
             <tr>
                <td>&nbsp;</td>
            
            
            <tr>
                <td>
                    <label for="file">File Name:</label>
                    <input type="file" name="file" id="file" size="50"/>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <div class="form-button">
                        <?php echo $btnSplit; ?>
                    </div>
                </td>
            </tr>
        </table>
    </div>
<div id="light2" class="white_content">
    <div id="title2" class="light-title"></div>
    <div id="msg2" class="light-message"></div>
    <div id="button" class="light-button">
        <?php echo $btnSuccessSplit; ?>
    </div>
    <div class="light-footer"></div>
</div>
</form>
 <!-- POP UP FOR MESSAGES -->
<div id="light" class="white_content">
    <div id="title" class="light-title"></div>
    <div id="msg" class="light-message"></div>
    <div id="button" class="light-button">
        <input type="button" onclick="javascript: document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none';" value="Okay"/>
    </div>
    <div class="light-footer"></div>
</div>

<!-- POP UP FOR MESSAGES -->
<div id="loading" class="white_content">
	<div class="light-title"></div>
    <div class="light-message">
        <?php if(isset($loading)){
            echo $loading;
        }
        ?> 
        <br/>
        <div class="loading" id="loading-image"></div>
    </div>
	<div class="light-footer"></div>
</div>
<?php if(isset($errormsg)):?>
    <script>
        document.getElementById('title').innerHTML = "<?php echo $errormsgtitle; ?>";
        document.getElementById('msg').innerHTML = "<?php echo $errormsg; ?>";
        document.getElementById('light').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
    </script>
<?php endif;?>
<?php if(isset($success_msg)):?>
    <script>
        document.getElementById('light').style.display = 'none';
        document.getElementById('title2').innerHTML = "<?php echo $success_title; ?>";
        document.getElementById('msg2').innerHTML = "<?php echo $success_msg; ?>";
        document.getElementById('light2').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
    </script>
<?php endif;?>
<?php include("footer.php"); ?>