<?php
/*
 * @author 
 * Purpose   : view for subnavigation
 */
if (count($arrnavsubmenus) > 0)
{
    ?>
    <ul>
        <?php
        for ($j = 0; $j  < count($arrnavsubmenus); $j++)
        {
            $navsubmenu = $arrnavsubmenus[$j];
            ?><li>
                <?php if( $pagesubmenuid == $navsubmenu["SubMenuID"]) { ?>
                <b><a href="<?php echo $navsubmenu["Link"]; ?>" style="white-space: nowrap;" class="nowrap <?php echo $pagesubmenuid == $navsubmenu["SubMenuID"] ? ' selected' : ''; ?>"><?php echo $navsubmenu["Name"]; ?></a></b>
                <?php } else { ?>
                <a href="<?php echo $navsubmenu["Link"]; ?>" style="white-space: nowrap;"  class="nowrap <?php echo $pagesubmenuid == $navsubmenu["SubMenuID"] ? ' selected' : ''; ?>"><?php echo $navsubmenu["Name"]; ?></a>
                <?php } ?>
            </li>
        <?php } ?>
    </ul>
    <?php
}
?>