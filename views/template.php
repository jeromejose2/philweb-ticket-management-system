<?php
/*
 * @author 
 * Purpose   : view for templeate
 */
session_start();
require_once ("../init.inc.php");
$pagesubmenuid = 13;
include("../controller/managesession.php");
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>Untitled Document</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link rel="stylesheet" href="css/default.css" />
        <link rel="stylesheet" href="css/navigation.css" />
    </head>
    <body>
        <table width="1050" border="0" cellspacing="0" cellpadding="0">
            <tr >
                <td colspan="2" id="loginheader">Hi <?php echo $_SESSION['uname']; ?> | <a href="logout.php" class="loginheader">Logout</a> | <a href="#" class="loginheader">Settings</a></td>
            </tr>
            <tr>
                <td height="100" colspan="2"><img src="images/masthead.jpg" width="1050" height="136"></td>
            </tr>
            <tr>
                <td width="250" valign="top" class="leftnavigationcontainer">
                    <?php include("navigation.php"); ?>

                </td>
                <td width="1000" valign="top" class="maincontainer">
                    <?php
                    if (isset($_GET['page']))
                    {
                        // check that the file exists
                        if (file_exists( $_GET['page'] . '.php'))
                        {
                            // we include the file
                            include_once($_GET['page'] . '.php');
                        }
                        else
                        {
                            // if the file does not exist
                            echo 'Requested page does not exist.';
                        }
                    }
                    // if incorrect url
                    else
                    {
                        include_once("adminmenus.php");
                    }
                    ?>

                </td>
            </tr>
        </table>
    </body>
</html>
