<?php
/*
 * @author 
 * Purpose   : view for ticketcancellation
 */
include("init.inc.php");
$pagesubmenuid = 8;
include_once("../controller/managesession.php");
include("../controller/ticketcancellationprocess.php");
?>
<script language="javascript" src="jscripts/validations.js"></script>
<script language="javascript" type="text/javascript">
function checkKeycode(e) 
{
    var keycode;
    if (window.event) keycode = window.event.keyCode;
    else if (e) keycode = e.which;
    
    if (keycode == 13 || keycode > 31 && (keycode < 48 || keycode > 57))
        return false;
    return true;
}


</script>
<?php include("header.php"); ?>
<script>
    $(document).ready(function(){
        $('#txtBookNumber').bind("cut copy paste",function(e) {
          e.preventDefault();
        });
        $('#txtValidationNumber').bind("cut copy paste",function(e) {
          e.preventDefault();
        });
    });
</script>

<div id="fade" class="black_overlay"></div>
<!-- POP UP FOR MESSAGES -->
    
<!-- POP UP FOR MESSAGES -->
    <div id="light2" class="white_content">
        <div id="title2" class="light-title"></div>
        <div id="msg2" class="light-message"></div>
        <div id="button" class="light-button">
        <input type="button" onclick="javascript: document.getElementById('light2').style.display='none';document.getElementById('fade').style.display='none';" value="OKAY"/>
<!--           <input type="button" id="btnCancel" name="btnCancel" value="NO" onclick="javascript: document.getElementById('light2').style.display = 'none';document.getElementById('fade').style.display = 'none';"/>-->
        </div>
        <div class="light-footer"></div>
    </div>
    
<!-- POP UP FOR MESSAGES -->
<div id="light4" class="white_content">
    <div id="title4" class="light-title"></div>
    <div id="msg4" class="light-message"></div>
    <div id="button4" class="light-button">
        <?php echo $btnError;?>
    </div>
	<div class="light-footer"></div>
</div>

<!--    <div id="light2" class="white_content">
        <div id="title2" class="light-title"></div>
        <div id="msg2" class="light-message"></div>
        <div id="button" class="light-button">
            <?php //echo $btnOk; ?>&nbsp;
            <input type="button" id="btnCancel" name="btnCancel" value="NO" onclick="javascript: document.getElementById('light2').style.display = 'none';document.getElementById('fade').style.display = 'none';"/>
        </div>
        <div class="light-footer"></div>        
    </div>-->
<!-- POP UP FOR MESSAGES -->

<!--    <div id="fade" class="black_overlay"></div>
    <div id="light" class="white_content">
    <div id="title" class="light-title"></div>
    <div id="msg" class="light-message"></div>
    <div id="button2" class="light-button">
        <input type="button" onclick="javascript: document.getElementById('light2').style.display='none';document.getElementById('fade').style.display='none'; window.location.href='ticketcancellation.php'" value="Okay" />
    </div>
	<div class="light-footer"></div>
    </div>-->


<!--<div id="light4" class="white_content">
    <div id="title4" class="light-title"></div>
    <div id="msg4" class="light-message"></div>
    <div id="button4" class="light-button">
    </div>
	<div class="light-footer"></div>
</div>-->
<!--<div id="light" class="white_content">
        <div id="title" class="light-title"></div>
        <div id="msg" class="light-message"></div>
        <div id="button" class="light-button"><input id="btnOk" name="btnOk" value="OKAY" type="button" onclick="javascript: document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none';" /></div>
        <div class="light-footer"></div>
    </div> 
<div id="light" class="white_content">
        <div id="title" class="light-title"></div>
        <div id="msg" class="light-message"></div>
        <div id="button" class="light-button"><input id="btnOk" name="btnOk" type="button" onclick="javascript: document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none';" value="OKAY"/></div>
        <div class="light-footer"></div>
        </div>  -->
<!-- POP UP FOR MESSAGES -->
<form name="frmCancellation" method="post">
    <div class="titleCont">
    <div class="titleCont_left"></div>
    <div class="titleCont_body">Cancellation</div>
    <div class="titleCont_right"></div>
</div>
   
<!--    <div id="light" class="white_content">
        <div id="title" class="light-title"></div>
        <div id="msg" class="light-message"></div>
        <div id="button5" class="light-button"><input id="btnOk" name="btnOk" value="OKAY" type="button" onclick="javascript: document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none';" />  
            <?php //echo $btnOkay; ?>
            <input type="button" id="btnCancel" name="btnCancel" value="Cancel" onclick="javascript: document.getElementById('light').style.display = 'none';document.getElementById('fade').style.display = 'none';"/>
        </div>
        <div class="light-footer"></div>
    </div>-->
    <!-- POP UP FOR MESSAGES -->

    <!--     POP UP FOR MESSAGES -->
    <div id="light3" class="white_content">
        <div id="title3" class="light-title">ADMINISTRATOR CONFIRMATION</div>
        <div id="msg3" class="light-message">
            Please enter the administrator account to proceed.<br/><br/>
            Username :  <?php echo $txtusername;?> <br/>
            Password  &nbsp;:  <?php echo $txtpassword;?>
        </div>
        <div id="button3" class="light-button">
            <?php echo $btnConfirm;?>
        <input type="button" onclick="javascript: document.getElementById('light3').style.display='none';document.getElementById('fade').style.display='none';" value="Cancel"/>
       
        </div>
        <div class="light-footer"></div>
    </div>
    <!-- POP UP FOR MESSAGES -->
<!--    <div id="loading" class="loading"></div>-->
<!--   <div id="light" class="white_content">
        <div id="title" class="light-title"></div>
        <div id="msg" class="light-message"></div>
        <div id="button" class="light-button"><input id="btnOk" name="btnOk" value="OKAY" type="button" onclick="javascript: document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none';" /></div>
        <div class="light-footer"></div>
    </div>  -->
<!--    <div id="light2" class="white_content">
        <div id="title2" class="light-title"></div>
        <div id="msg2" class="light-message"></div>
        <div id="button" class="light-button">
            <?php //echo $btnOk; ?>&nbsp;
            <input type="button" id="btnCancel" name="btnCancel" value="NO" onclick="javascript: document.getElementById('light2').style.display = 'none';document.getElementById('fade').style.display = 'none';"/>
        </div>
        <div class="light-footer"></div>        
    </div>-->
    
        <!-- POP UP FOR MESSAGES -->
<!--        <div id="light" class="white_content">
        <div id="title" class="light-title"></div>
        <div id="msg" class="light-message"></div>
        <div id="button" class="light-button"><input id="btnOk" name="btnOk" type="button" onclick="javascript: document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none';" value="OKAY"/></div>
        <div class="light-footer"></div>
        </div>  
        <div id="light2" class="white_content">
        <div id="title2" class="light-title"></div>
        <div id="msg2" class="light-message"></div>
        <div id="button" class="light-button">
            <?php //echo $btnOk; ?>&nbsp;
            
            <input type="button" id="btnCancel" name="btnCancel" value="NO" onclick="javascript: document.getElementById('light2').style.display = 'none';document.getElementById('fade').style.display = 'none';"/>
        </div>
        <div class="light-footer"></div>
	</div>-->

<div class="content-page">
    <table style="margin-left: 0.5%;">
        <tr>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td class="label2">Ticket/Book Number:&nbsp;&nbsp;</td>
            <td><?php echo $txtBookNumber; ?></td>
        </tr>
        <tr>
            <td class="label2">Cancel Mode&nbsp;</td>
            <td><?php echo $ddlModeSelect; ?></td>
        </tr>
        <tr>
            <td class="label2">*Remarks:</td>
            <td><?php echo $txtRemarks; ?></td>
        </tr>
        <tr>
            <td colspan ="2" style="text-align: right"><?php echo $btnProcess; ?></td>
        </tr>
    </table>
 </div>
             <!-- POP UP FOR MESSAGES -->
    <div id="light" class="white_content">
        <div id="title" class="light-title">CONFIRMATION</div>
        <div id="msg" class="light-message">Proceed cancellation of the book?<br/><br/>
            <div id="button2" class="light-button">
            <center>
            <?php echo $btnOkay; ?>&nbsp;
<!--                <input type="button" value="Okay" onclick="javascript: document.getElementById('light').style.display = 'none';document.getElementById('fade').style.display = 'none';"/>-->
            
            <!--<input type="submit" value="Cancel" onclick="javascript: document.getElementById('light').style.display = 'none';document.getElementById('fade').style.display = 'none';"/>-->
            <?php echo $btnCancel; ?>
            </center>
            </div>
            
        </div>
<!--       <div id="button5" class="light-button"><input id="btnOk" name="btnOk" value="OKAY" type="button" onclick="javascript: document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none';" />  
        </div>-->
        <div class="light-footer"></div>
    </div>
                          <!-- POP UP FOR MESSAGES -->
   <!-- POP UP FOR MESSAGES -->
    <div id="light5" class="white_content">
        <div id="title5" class="light-title">CONFIRMATION</div>
        <div id="msg5" class="light-message">Proceed cancellation of the ticket?<br/><br/>
            <div id="button5" class="light-button">
            <center>
            <?php echo $btnOk; ?>&nbsp;
<!--                <input type="button" value="Okay" onclick="javascript: document.getElementById('light').style.display = 'none';document.getElementById('fade').style.display = 'none';"/>-->
            
            <!--<input type="submit" value="Cancel" onclick="javascript: document.getElementById('light').style.display = 'none';document.getElementById('fade').style.display = 'none';"/>-->
            <?php echo $btnCancel; ?>
            </center>
            </div>
            
        </div>
<!--       <div id="button5" class="light-button"><input id="btnOk" name="btnOk" value="OKAY" type="button" onclick="javascript: document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none';" />  
        </div>-->
        <div class="light-footer"></div>
    </div>
                          <!-- POP UP FOR MESSAGES -->

 <?php if(isset($confirmation)):?>
          <script>
//                                document.getElementById('title').innerHTML = "CONFIRMATION";
//                                document.getElementById('msg').innerHTML =  "<?php //echo $msg2; ?>";
                                document.getElementById('light').style.display = 'block';
                                document.getElementById('fade').style.display = 'block';                               
                            </script>
    <?php endif;?>
 <?php if(isset($confirmation2)):?>
          <script>
//                                document.getElementById('title').innerHTML = "CONFIRMATION";
//                                document.getElementById('msg').innerHTML =  "<?php //echo $msg2; ?>";
                                document.getElementById('light5').style.display = 'block';
                                document.getElementById('fade').style.display = 'block';                               
                            </script>
    <?php endif;?> 
    <?php if(isset($confirm)):?>
        <script>
//            document.getElementById('title3').innerHTML = "ADMIN CONFIRMATION";
//            document.getElementById('msg3').innerHTML =  "<?php // echo $msg3; ?>";
            document.getElementById('light3').style.display = "block";
            document.getElementById('fade').style.display = "block";
        </script>
    <?php endif;?>
    <?php if(isset($errormsg)):?> 
        <script>
            document.getElementById('title2').innerHTML = "<?php echo $errormsgtitle;?>";
            document.getElementById('msg2').innerHTML =  "<?php echo $errormsg;?>";
            document.getElementById('light2').style.display = 'block';
            document.getElementById('fade').style.display = 'block';
        </script>
    <?php endif;?>
                                 
<!--                       <div  class="labelboldred"><?php //echo $errormsg; ?></div><br />-->
                            <?php //} ?>             
    <?php if(isset($successtitle)):?>
        <script>
            document.getElementById('title2').innerHTML = "<?php echo $successtitle;?>";
            document.getElementById('msg2').innerHTML = "<?php echo $successmsg;?>";
//            document.getElementById('button2').innerHTML = "<?php //echo $btnOk;?>";
            document.getElementById('light2').style.display = "block";
            document.getElementById('fade').style.display = "block";
        </script>
    <?php endif;?>
    <?php if(isset($logintitle)):?>
        <script>
            document.getElementById('title4').innerHTML = "<?php echo $logintitle;?>";
            document.getElementById('msg4').innerHTML = "<?php echo $loginmsg;?>";
            document.getElementById('light4').style.display = "block";
            document.getElementById('fade').style.display = "block";
        </script>
    <?php endif;?>
  
</form>
<?php include("footer.php"); ?>