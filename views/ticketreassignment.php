<?php

/* Date : January 7 ,2013
 * Created by Jerome F. Jose
 * Modified By: Noel Antonio
 * Date Modified: January 14, 2013
 * Purpose : To transfer Ticket
 */

require_once ("init.inc.php");
$pagesubmenuid = 49;
include_once("../controller/managesession.php");
include("../controller/ticketreassignmentcontroller.php");
?>

<script type="text/javascript" src="jscripts/jquery-1.5.2.min.js"></script>
<script src="jscripts/validations.js" type="text/javascript" language="javascript"></script>
<script language="javascript">
$(document).ready(function(){
    
        $("#ddlbooknumber").attr('disabled',true);
        $("#ddlgametype").attr('disabled',true);
        var gamename = document.getElementById('ddlgamename').options[document.getElementById('ddlgamename').selectedIndex].value;
        if(gamename != 0)
        {
            $("#ddlgametype").attr('disabled',false);
            $("#ddlbooknumber").attr('disabled',false);
        }
}); 

function get_gamename()
{
    var gamenum = document.getElementById('ddlgametype').options[document.getElementById('ddlgametype').selectedIndex].value;

    $("#ddlgamename").load(
        "../controller/get_gamename.php",
        {
            gamenum: gamenum,
            module: 'TicketReassignment'
        },

        function(status, xhr)
        {
            if(status == "error")
            {
                document.getElementById('title').innerHTML = "ERROR!";
                document.getElementById('msg').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
                document.getElementById('light').style.display = "block";
                document.getElementById('fade').style.display = "block";
            }
        }
    );
}

function get_gamenumber()
{
        var prodid1 = document.getElementById('ddlgamename').options[document.getElementById('ddlgamename').selectedIndex].value;
        var gamenum = document.getElementById('ddlgametype').options[document.getElementById('ddlgametype').selectedIndex].value;
        if(gamenum != 0 && prodid1 != 0)
        {
            $("#ddlbooknumber").attr('disabled',true);
        }
        if(prodid1 != 0)
        {
            $("#ddlgametype").attr('disabled',false);
            //document.getElementById("ddlGameName").onchange = get_null();
        }
        if(prodid1 == 0)
        {
            $("#ddlgametype").attr('disabled',true);
            $("#ddlbooknumber").empty();
            $("#ddlbooknumber").prepend("<option selected value=\"0\" >Please select</option>");
            $("#ddlbooknumber").attr('disabled',true);
        }
       
       
        $("#ddlgametype").load(
            "../controller/get_gamenum.php",
            {
                prodid: prodid1
            },

            function(status, xhr)
            {
                if(status == "error")
                {
                    document.getElementById('title').innerHTML = "ERROR!";
                    document.getElementById('msg').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
                    document.getElementById('light').style.display = "block";
                    document.getElementById('fade').style.display = "block";
                }
            }
        );
}

function get_booknumbers()
{
        document.getElementById('hiddenflag').value = "3";
        var prodid = document.getElementById('ddlgamename').options[document.getElementById('ddlgamename').selectedIndex].value;
        var gamenum = document.getElementById('ddlgametype').options[document.getElementById('ddlgametype').selectedIndex].value;

        if(gamenum != 0)
        {
                $("#ddlbooknumber").attr('disabled',false);
        }
        
        if(gamenum == 0)
        {
                $("#ddlbooknumber").attr('disabled',true);
        }
       
        $("#ddlbooknumber").load(
            "../controller/get_booknumbers_1.php",
            {
                gamename: prodid,
                gamenumber: gamenum,
                status: 1
            },
            function(status, xhr)
            {
                if(status == "error")
                {
                    document.getElementById('title').innerHTML = "ERROR!";
                    document.getElementById('msg').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
                    document.getElementById('light').style.display = "block";
                    document.getElementById('fade').style.display = "block";
                }
            }
        );
}
</script>

<?php include("header.php");?>
<form method="post" name = "frmticketreassigent" action="ticketreassignment.php">
    
    <div id="fade" class="black_overlay"></div>
    <!-- POP UP FOR MESSAGES -->
    <div id="light" class="white_content">
        <div id="title" class="light-title"></div>
        <div id="msg" class="light-message"></div>
        <div id="button" class="light-button">

        <?php if(isset($errormsg)):?>
        <input type="button" onclick="javascript: document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none';" value="Okay"/>
        <?php   endif; ?>

        <?php if(isset($succesmsg)):?>
        <input type="button" onclick="javascript: window.location = 'ticketreassignment.php';" value="Okay"/>
        <?php   endif; ?>

        <?php if(isset($confirm_msg)):?>
        <?php echo $btnYes;?>
        <input type="button" onclick="javascript: document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none';" value="No"/>
        <?php   endif; ?>
        </div>
            <div class="light-footer"></div>
    </div>

    <div id="light1" class="white_content">
        <div id="title1" class="light-title"></div>
        <div id="msg1" class="light-message"></div>
        <div id="button" class="light-button">
        <input type="button" onclick="javascript: document.getElementById('light1').style.display='none';document.getElementById('fade').style.display='none';" value="Okay"/>
        </div>
            <div class="light-footer"></div>
    </div>
    <!-- POP UP FOR MESSAGES -->

     <?php echo $hiddenflag;?>
     <div class="titleCont">
            <div class="titleCont_left"></div>
            <div class="titleCont_body">Ticket Reassignment</div>
            <div class="titleCont_right"></div>
    </div>
    
    <div class="content-page">
        <div class="search-container">
            <div class="form-view">
                <table>
                    <tr><td width="100px">Game Name:</td><td><?php echo $ddlgamename;?></td></tr>
                    <tr><td>Game Number:</td><td><?php echo $ddlgametype;?></td></tr>
                    <tr><td>Book Number:</td><td><?php echo $ddlbooknumber;?></td></tr>
                    <tr><td>Ticket Count:</td><td><?php echo $txtTicketCount; ?></td></tr>
                    <tr><td>Transfer To:</td><td><?php echo $ddlassingedto;?></td></tr>
                <tr>
                    <td>
                        <br/><div class="form-button" style="float: left;"><p>
                            &nbsp;&nbsp;<?php echo $btnsubmit; ?></p></div> 
                    </td>
                </tr> 
            </table>
                </div>
        </div>
    </div>
</form>

<?php if(isset($errormsg)):?> 
        <script>
            document.getElementById('title').innerHTML = "<?php echo $errortitle;?>";
            document.getElementById('msg').innerHTML =  "<?php echo $errormsg;?>";
            document.getElementById('light').style.display = 'block';
            document.getElementById('fade').style.display = 'block';
        </script>   
<?php endif;?>
<?php if(isset($succesmsg)):?> 
        <script>
            document.getElementById('title').innerHTML = "SUCCESSFUL";
            document.getElementById('msg').innerHTML =  "<?php echo $succesmsg;?>";
            document.getElementById('light').style.display = 'block';
            document.getElementById('fade').style.display = 'block';
        </script>   
<?php endif;?>
<?php if(isset($confirm_msg)):?> 
        <script>
            document.getElementById('title').innerHTML = "Confirmation";
            document.getElementById('msg').innerHTML =  "<?php echo $confirm_msg;?>";
            document.getElementById('light').style.display = 'block';
            document.getElementById('fade').style.display = 'block';
        </script>   
<?php endif;?>

<?php include("footer.php");?>
