<?php
/*
 * @author 
 * Purpose   : view for ticketvalidation
 */
include("init.inc.php");
$pagesubmenuid = 7;
include_once("../controller/managesession.php");
include("../controller/ticketvalidationprocess.php");
?>
<link rel="stylesheet" type="text/css" href="css/datepicker.css" />
<script type="text/javascript" src="jscripts/datetimepicker.js"></script>
<script language="javascript" src="jscripts/validations.js"></script>
<script language="javascript" src="jscripts/jquery-1.5.2.min.js"></script>
<script language="javascript" type="text/javascript">
    $('#txtBookNumber').live('keypress',function(e){
        if(e.keyCode == 13 || e.keyCode == 9) {
            $('#txtValidationNumber').focus();
            return false;
        }
    });

    $('#txtValidationNumber').live('keypress',function(e){
        if(e.keyCode == 13 || e.keyCode == 9) {
            $('#btnValidate').focus();
            $('#btnValidate').click();
            return false;
        }
    });

    function checkKeycode(e) 
    {
        var keycode;
        if (window.event) keycode = window.event.keyCode;
        else if (e) keycode = e.which;

        if (keycode == 13 || keycode > 31 && (keycode < 48 || keycode > 57))
            return false;
        return true;
    }

    function setIndex()
    {
        document.getElementById("txtValidationNumber").tabIndex = 1;
    }
</script>

<?php include("header.php"); ?>
<script>
    function ChangePage(pagenum)
    {
        selectedindex = document.getElementById("pgSelectedPage");
        selectedindex.value = pagenum;
        document.getElementById("hidchangedate").value = 1;
        document.forms["frmValidation"].submit();
    }
    
    $(document).ready(function(){
        $('#txtBookNumber').bind("cut copy paste",function(e) {
          e.preventDefault();
        });
        $('#txtValidationNumber').bind("cut copy paste",function(e) {
          e.preventDefault();
        });
    });
</script>

<div class="titleCont">
    <div class="titleCont_left"></div>
    <div class="titleCont_body">Ticket Validation</div>
    <div class="titleCont_right"></div>
</div>
<div class="content-page">
<form name="frmValidation" method="post" onload="javascript: return setIndex();">

<div id="fade" class="black_overlay"></div>
<div id="loading" class="loading"></div>
<!-- POP UP FOR MESSAGES -->
<div id="light" class="white_content">
<div id="title" class="light-title"></div>
<div id="msg" class="light-message"></div>
<div id="button" class="light-button"><input id="btnOk" name="btnOk" type="button" onclick="javascript: document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none';" value="OKAY"/></div>
<div class="light-footer"></div>
</div>    
<div id="light2" class="white_content">
<div id="title2" class="light-title"></div>
<div id="msg2" class="light-message"></div>
<div id="button" class="light-button">
    <?php if (!$chk_claim_reimburse == 1){ echo $btnOk; } else { echo $btnClaimReimburse; } ?>&nbsp;
    <input type="button" id="btnCancel" name="btnCancel" value="CANCEL" onclick="javascript: document.getElementById('light').style.display = 'none';document.getElementById('light2').style.display = 'none';document.getElementById('fade').style.display = 'none';"/>
</div>
<div class="light-footer"></div>
</div>    
<!-- POP UP FOR MESSAGES -->
<table style="margin-left: 0.5%;">
    <?php
    if ($accttypeid < 4) 
    {    
        ?>
    <tr>
        <td class="label2">Account Name</td>
        <td><?php echo $ddlUsername; ?></td>
    </tr>
    <?php
    }
    ?>
    <tr>
        <td class="label2">Ticket Number</td>
        <td><?php echo $txtBookNumber; ?></td>
    </tr>
    <tr>
        <td class="label2">Validation Number</td>
        <td><?php echo $txtValidationNumber; ?><div id="divIsValid" name="divIsValid"></div><input id ="hidIsValid" name="hidIsValid" type="hidden" value=""></input>
        </td>
    </tr>
    <tr>
        <td colspan ="2" style="text-align: right"><?php echo $btnValidate; ?><?php echo $hidProdID; ?><?php echo $hidchangedate; ?></td>
    </tr>
</table>
    
<br/><br/>

<table width="100%">
    <tr>
        <td width="30%">
            <div class="titleCont" style="margin-left: -0.2%;">
                <div class="titleCont_left"></div>
                <div class="titleCont_body" style="width: 70%;">Hot Tickets</div>
                <div class="titleCont_right"></div>
            </div>
        </td>
        <td>
            <?php echo $txtDateFr;?>
            <img id="cal" name="cal" src="images/close.gif" width="16" height="16" border="0" alt="Pick a date" onClick="displayDatePicker('txtDateFr', false, 'ymd', '-');"/>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <?php echo $txtDateTo;?>
            <img id="cal" name="cal" src="images/close.gif" width="16" height="16" border="0" alt="Pick a date" onClick="displayDatePicker('txtDateTo', false, 'ymd', '-');"/>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <?php echo $btnSubmit; ?>
        </td>
    </tr>
</table>
<div class="form-page"><?php echo $pgTransactionHistory;?></div>
<table class="table-list">
    <tr>
        <th>Mode of Cancellation</th>
        <th>Ticket Number</th>
        <th>VIRN</th>
        <th>Transaction Date</th>
        <th>Account</th>
    </tr>
    <?php if (count($list_ht) > 0): ?>
    <?php
    for($ctr=0; $ctr < count($list_ht); $ctr++){ ?>
    <?php ($ctr % 2) == 0 ? $class = "evenrow" : $class = "oddrow" ; ?>
    <tr class = "<?php echo $class?>">
        <td>
            <?php if ($list_ht[$ctr]['CancellationMode'] == 5){ echo "Void"; } 
                  else if ($list_ht[$ctr]['CancellationMode'] == 7){ echo "Stolen (Payable)"; }
                  else if ($list_ht[$ctr]['CancellationMode'] == 8){ echo "Stolen (Non-Payable)"; }
                  else if ($list_ht[$ctr]['CancellationMode'] == 9){ echo "Expired (Payable)"; }
                  else if ($list_ht[$ctr]['CancellationMode'] == 10){ echo "Expired (Non-Payable)"; }
            ?>
        </td>
        <td><?php echo $list_ht[$ctr]['TicketNumber'];?></td>
        <td><?php echo $list_ht[$ctr]['VIRN'];?></td>                            
        <td><?php echo $list_ht[$ctr]['DateCreated'];?></td>
        <td><?php echo $list_ht[$ctr]['UserName'];?></td>
    </tr>
    <?php } ?>
    <?php else: ?>
        <tr class="no-record"><td colspan="5">No records to display</td></tr>
    <?php endif; ?>
</table>
</form>
<div class="form-page"><?php echo $pgTransactionHistory;?></div>
</div>


<?php if (($confirmation == 'true') || ($chk_claim_reimburse == 1)) : ?>
<script>
    document.getElementById('title2').innerHTML = "<?php echo $errormsgtitle; ?>";
    document.getElementById('msg2').innerHTML = "<?php echo $errormsg; ?>";
    document.getElementById('light2').style.display = 'block';
    document.getElementById('fade').style.display = 'block';
</script>  
<?php endif; ?>
<?php if (($confirmation == 'false' && isset($errormsg)) || ($successful == 'true')) : ?>
<script>
    document.getElementById('title').innerHTML = "<?php echo $errormsgtitle; ?>";
    document.getElementById('msg').innerHTML = "<?php echo $errormsg;?>";
    document.getElementById('light').style.display = 'block';
    document.getElementById('fade').style.display = 'block';
</script>  
<?php endif; unset($errormsg); ?> 
</form>
<?php include("footer.php"); ?>