<?php
/* 
 * @author: MCS 02/13/2013
 * @purpose: For TMS Indonesia Alfamart API 
*/

require_once('nusoap/nusoap.php');
include("../init.inc.php");

App::LoadModuleClass("TicketManagementCMINDO", "TMTicketValidation");

$URL = 'http';
if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
$URL .= "://";
$folder = $_SERVER["REQUEST_URI"];
$folder = substr($folder,0,strrpos($folder,'/') + 1);
if ($_SERVER["SERVER_PORT"] != "80") 
{
  $URL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"];
} 
else 
{
  $URL .= $_SERVER["SERVER_NAME"];
}

$URL       .= "/views/tms_api";
$namespace = $URL . '?wsdl';

//using soap_server to create server object
$server    = new soap_server;
$server->configureWSDL('tms_api', $namespace);

//register a function that works on server
$server->register('validateticket');
$server->register('claimticket');
$server->register('authenticateuser');
$server->register('hello');

/* 
 * @author: MCS 02/13/2013
 * @purpose: to validate scratch cards
 * @param: $ticketnumber,$validationnumber
 * @return array
*/
function validateticket($ticketnumber,$validationnumber)
{
     
}

/* 
 * @author: MCS 02/13/2013
 * @purpose: to process winings of scratch cards
 * @param: $ticketnumber,$validationnumber
 * @return array
*/
function claimticket($ticketnumber,$validationnumber)
{
    
}

/* 
 * @author: MCS 02/13/2013
 * @purpose: to authenticate user
 * @param: $ticketnumber,$validationnumber
 * @return array
*/
function authenticate($username,$password,$ip)
{
        
}

// 
function hello($name)
{
    if (!$name) {
        return new soap_fault('Client', '', 'Put your name!');
    }
    $result = "Hello, " . $name;
    return $result;
}


// create HTTP listener
$server->service($HTTP_RAW_POST_DATA);
//exit();
?>