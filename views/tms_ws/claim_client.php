<?php
include('init.inc.php');
include('claimprocess.php');

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <title></title>
    <head>
         <link rel="stylesheet" href="style.css" />
       
    </head>
    <body>
    <div id="light" class="white_content">    
    <div id="msg" class="light-message"></div>
    <div id="button" class="light-button">
        <input type="button" onclick="javascript: document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none';" value="Okay"/>
    </div>   
    </div>
    <div id ="content">
        <form name ="validationFrom" method ="post">
        <div id="fade" class="black_overlay"></div>
        <p class ="heading1">Ticket Management System</p>
        <p class="listAll">Click <a href="http://192.168.20.8:8088/tms_ws/server.php">here</a> for a complete list of operations.</p>
        <h2>Validate</h2>
        <h3>Test</h3>
        To test the operation using the HTTP POST protocol, click the 'Invoke' button.
        <table class ="table" cellpadding ="4">
            <tr>
                <td class="parameter">Parameter</td>
                <td class="value">Value</td>
            </tr>
            <tr>
                <td>Account Name </td>
                <td><?php echo $uname ?></td>
            </tr>
            <tr>
                <td>Ticket Number</td>
                <td><?php echo $booknum ?></td>
            </tr>
            <tr>
                <td>Validation Number</td>
                <td><?php echo $virn ?></td>
            </tr>
            <tr>
                <td></td>
                <td align="right" ><?php echo $btnValidate ?></td>        
            </tr>
        </table>  
    </form>
          <p>The following is a sample SOAP 1.1 request and response.  The <font class="placeholder">placeholders</font> shown need to be replaced with actual values.</p>
    <?php
    echo '<h2>Request</h2>';
    echo '<pre>'. 
htmlspecialchars(
'POST /tms_ws/server.php HTTP/1.0
Host: 192.168.20.8:8088
User-Agent: NuSOAP/0.7.3 (1.114)
Content-Type: text/xml; charset=ISO-8859-1
SOAPAction: ""
Content-Length: 611

<?xml version="1.0" encoding="ISO-8859-1"?>
<SOAP-ENV:Envelope SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" 
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/">
    <SOAP-ENV:Body>
        <ns4255:Validate xmlns:ns4255="http://tempuri.org">
            <uname xsi:type="xsd:string">string</uname>
            <booknum xsi:type="xsd:string">int</booknum>
            <virn xsi:type="xsd:string">int</virn>
        </ns4255:Validate>
    </SOAP-ENV:Body>
</SOAP-ENV:Envelope>',ENT_QUOTES).'
            </pre>';
    
    
    echo '<h2>Response</h2>';
    echo '<pre>' . htmlspecialchars('
<?xml version="1.0" encoding="ISO-8859-1"?>
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/" xmlns:tns="http://192.168.20.8:8088/">
    <SOAP-ENV:Body>
        <ns1:ValidateResponse xmlns:ns1="http://tempuri.org">
            <return xsi:type="tns:result">
                <try xsi:type="xsd:string">return</try>
            </return>
        </ns1:ValidateResponse>
    </SOAP-ENV:Body>
</SOAP-ENV:Envelope>', ENT_QUOTES) . '</pre>';
    
    echo '<h2>Response</h2>';
    echo '<pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';
    ?>
    </div>
  
    </body>
    <?php if (isset($message)) : ?>
        <script>
            
            document.getElementById('msg').innerHTML = "<?php echo $message;?>";
            document.getElementById('light').style.display = 'block';
            document.getElementById('fade').style.display = 'block';
        </script>  
        <?php endif; ?>
</html>
    

