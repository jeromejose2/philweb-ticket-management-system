<?php
App::LoadControl("TextBox");
App::LoadControl("Button");
$stylesheets[] = "style.css";
$javascripts[] = "../jscripts/jquery-1.5.2.min.js";

require_once('nusoap/nusoap.php');
$form = new FormsProcessor();

$uname = new TextBox("uname","uname", "Account Name");
$uname->Args ="class = 'input'";
$uname->Size = 50;
$uname->ShowCaption = false;

$booknum = new TextBox("booknum","booknum", "Ticket Number");
$booknum->Args ="class = 'input'";
$booknum->Size = 50;
$booknum->ShowCaption = false;

$virn = new TextBox("virn","virn", "Validation Number");
$virn->Args ="class = 'input'";
$virn->Size = 50;
$virn->ShowCaption = false;

$btnValidate = new Button("btnValidate","btnValidate", "Claim");
$btnValidate->Args = "class ='button'";
$btnValidate->IsSubmit = true;



$form->AddControl($uname);
$form->AddControl($booknum);
$form->AddControl($virn);
$form->AddControl($btnValidate);
$form->ProcessForms();

if($form->IsPostBack)
{
    if($btnValidate->SubmittedValue == "Claim")
    {
        $uname1 = $uname->SubmittedValue;
        $booknum1 =  $booknum->SubmittedValue;
        $virn1 = $virn->SubmittedValue;
    
        $param = array('uname' =>$uname1,'booknum' => $booknum1,'virn'=>$virn1);
 
        $client = new nusoap_client('http://192.168.20.8:8088/tms_ws/server.php');
        
        $result = $client->call('claimPrize', $param);
        
        if(!$client->getError())
        {
            $message = $result['message'];
        }
        else
        {
            $message = $client->getError(); 
        }
        //$l_aParam   = array((string)$_POST['uname'],(string)$_POST['booknum'],(string)$_POST['virn']);        
        //$l_oClient  = new nusoap_client('http://192.168.20.8:8088/tms_ws/server.php');
        //   $l_stResult = $l_oClient->call('Validate', $l_aParam);
    }
}
?>