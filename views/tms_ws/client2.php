<style>
.white_content {
    background-color: white;
    border: medium solid #000000;
    display: none;
    height: auto;
    left: 500px;
    margin-left: -5%;
    overflow: auto;
    padding-bottom: 0;
    position: absolute;
    top: 10%;
    width: 500px;
    z-index: 1002;
}
</style>
<div id="light" class="white_content">
    <div id="title" class="light-title"></div>
    <div id="msg" class="light-message"></div>
    <div id="button" class="light-button">
        <input type="button" onclick="javascript: document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none';" value="Okay"/>
    </div>   
</div>

<?php
    // Pull in the NuSOAP code        
if ((string)$_GET['action'] == 'get_data') {
  
    // includes nusoap classes
    require_once('nusoap/nusoap.php');

    // set parameters and create client
    $l_aParam   = array((string)$_POST['uname'],(string)$_POST['booknum'],(string)$_POST['virn']);
   
    $l_oClient  = new nusoap_client('http://192.168.20.8:8088/tms_ws/validate_server.php');

    // call a webmethod (Validate)
    $l_stResult = $l_oClient->call('Validate', $l_aParam);
   
    
    // check for errors
    if (!$l_oClient->getError()) 
    {
        // print results     
        print '<h1>'    . $l_stResult['try']. '</h1>';
        $message = $l_stResult['try'];
        echo "<script type='text/javascript'>        
        document.getElementById('msg').innerHTML = '.$message.';
        document.getElementById('light').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        </script>";
        //this is the same with above only that in server.php it is not mandatory to put message in an array.
        //        $try = $l_oClient->getHTTPBody('ValidateResponse');
        //        print '<h1>'    . $l_stResult['try']. '</h1>';
    }
    // print error description
    else 
    {

        echo '<h1>Error: ' . $l_oClient->getError() . '</h1>';
        
    }
}

 
print '
  <form name="input" action="'.$_SERVER['PHP_SELF'].'?action=get_data"  method="POST">
    <table>
    <tr>
        <td>Account Name </td>
        <td><input type="text" name="uname"></td>
    </tr>
    <tr>
        <td>Ticket Number</td>
        <td><input type="text" name="booknum"></td>
    </tr>
        <td>Validation Number</td>
        <td><input type="text" name="virn"></td>
    </tr>
        <td><input type="submit" value="Validate"></td>        
    </tr>
  </form>
';
// Display the request and response
echo '<h2>Request</h2>';
echo '<pre>' . htmlspecialchars($l_oClient->request, ENT_QUOTES) . '</pre>';
echo '<h2>Response</h2>';
echo '<pre>' . htmlspecialchars($l_oClient->response, ENT_QUOTES) . '</pre>';
?>