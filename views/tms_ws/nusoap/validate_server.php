<?php

require_once('nusoap/nusoap.php');
// Create the server instance
$server = new soap_server;

$server->debug_flag=false;
$server->configureWSDL('TickeT Management System', 'http://192.168.20.8:8088/');
$server->wsdl->schemaTargetNamespace = 'http://192.168.20.8:8088/';


// add complex type
$server->wsdl->addComplexType(
    'results',
    'complexType',
    'struct',
    'all',
    '',
    array(
    'UserName' => array('name'=>'UserName', 'type'=>'xsd:string'),
    'AccountTypeID' => array('name'=>'AccountTypeID', 'type'=>'xsd:string'),
    'num' => array('name'=>'num', 'type'=>'xsd:int'))
    );

$name_checkdigit = 'checkDigit';        
$server->register($name_checkdigit, array('num' => 'xsd:int'), array('return'=>'tns:result'));

function CheckDigit($number) 
{ 
       
        
	// Strip any non-digits 
	$number=preg_replace('/\D/', '', $number);
       // return new soap_fault('Client', '', ''.$number.'');
	
	// Set the string length 
	$number_length=strlen($number);
	  

	for ($i=1; $i<=$number_length; $i++) 
	{
		$digit = $number[$i-1];
		if($i % 2)
		{
			//step 1
			//odd numbers
			$total_step1 += $digit;
		}
		else
		{
			//step 3
			//even numbers
			$total_step3 += $digit;
		}
	}
        
	//step2
	$result = $total_step1 * 3;
        	
	//step4
	$result = $result + $total_step3;
        
	//step5
	$result = 10 - $result % 10;
        //return new soap_fault('Client', '', ''.$result.'');
        
        //return new soap_fault('Client', '', ''.$result.'');
        $record1['num'] = $result;
       
        
        return $record1;      
}
?>
