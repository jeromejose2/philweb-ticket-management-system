<?php

    // Pull in the NuSOAP code
    require_once('nusoap/nusoap.php');
    // Create the server instance
    $server = new soap_server;

    // Register the method to expose
    $server->register('hello');
    $server->register('test');    
   
    // Define the method as a PHP function
    function hello($name) 
    {
            return 'Hello, ' . $name;	
    }
    
    function test($num1,$num2)
    {
        $num = $num1 + $num2;
        return $num;
    }

    // Use the request to (try to) invoke the service
    $HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? 
    $HTTP_RAW_POST_DATA : '';

    $server->service($HTTP_RAW_POST_DATA);

?>
