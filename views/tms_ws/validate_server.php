<?php
//App::LoadModuleClass("TicketManagementCM", "TMAccounts");
//$account = new TMAccounts();
include('init.inc.php');

App::LoadModuleClass("TicketManagementCM", "TMAccounts");
App::LoadModuleClass("TicketManagementCM", "TMTicketValidation");
App::LoadModuleClass("TicketManagementCM", "TMDecks");
App::LoadModuleClass("TicketManagementCM", "TMDeckInfo");
App::LoadModuleClass("TicketManagementCM", "TMAccounts");
App::LoadModuleClass("TicketManagementCM", "TMAuditLog");
App::LoadModuleClass("TicketManagementCM", "TMPrizes");
App::LoadModuleClass("TicketManagementCM", "TMProducts");
App::LoadModuleClass("TicketManagementCM", "TMGameManagement");
App::LoadModuleClass("TicketManagementCM", "TMTickets");

//initialize database objects
$account = new TMAccounts();
$tmticketvalidation = new TMTicketValidation();
$tmdecks = new TMDecks();
$tmdeckinfo = new TMDeckInfo();
$tmaccount = new TMAccounts();
$tmauditlog = new TMAuditLog();
$tmprizes = new TMPrizes();
$tmproducts = new TMProducts();
$tmgamemgt = new TMGameManagement();
$tmtickets = new TMTickets();

// Pull in the NuSOAP code
require_once('nusoap/nusoap.php');
// Create the server instance
$server = new soap_server;

$server->debug_flag=false;
$server->configureWSDL('TickeT Management System', 'http://192.168.20.8:8088/');
$server->wsdl->schemaTargetNamespace = 'http://192.168.20.8:8088/';


// add complex type
$server->wsdl->addComplexType(
    'result',
    'complexType',
    'struct',
    'all',
    '',
    array(
    'num' => array('name'=>'num', 'type'=>'xsd:int'),
    'try' => array('name'=>'try', 'type'=>'xsd:string'),
    'UserName' => array('name'=>'UserName', 'type'=>'xsd:string'),
    'AccountTypeID' => array('name'=>'AccountTypeID', 'type'=>'xsd:string')
        )
    );

// Register the method to expose


$name_checkdigit = 'checkDigit';        
$server->register($name_checkdigit, array('num' => 'xsd:int'), array('return'=>'tns:result'));

function CheckDigit($number) 
{ 
       
        
	// Strip any non-digits 
	$number=preg_replace('/\D/', '', $number);
       // return new soap_fault('Client', '', ''.$number.'');
	
	// Set the string length 
	$number_length=strlen($number);
	  

	for ($i=1; $i<=$number_length; $i++) 
	{
		$digit = $number[$i-1];
		if($i % 2)
		{
			//step 1
			//odd numbers
			$total_step1 += $digit;
		}
		else
		{
			//step 3
			//even numbers
			$total_step3 += $digit;
		}
	}
        
	//step2
	$result = $total_step1 * 3;
        	
	//step4
	$result = $result + $total_step3;
        
	//step5
	$result = 10 - $result % 10;
        //return new soap_fault('Client', '', ''.$result.'');
        
        //return new soap_fault('Client', '', ''.$result.'');
        $record1['num'] = $result;   
    
        
        return $record1;      
}

$name_validate = 'Validate';        
$server->register($name_validate, array('uname' => 'xsd:string','booknum' => 'xsd:int','virn' => 'xsd:int'), array('return'=>'tns:result'));

function Validate($UserName,$TicketNum,$Virn)
{
   
    //return new soap_fault('Client', '', ''.$UserName.','.$TicketNum.''.$Virn.'');
    $tmgamemgt = new TMGameManagement();
    $account = new TMAccounts();
    $tmticketvalidation = new TMTicketValidation();
    $tmdecks = new TMDecks();
    $tmdeckinfo = new TMDeckInfo();
    $tmaccount = new TMAccounts();
    $tmauditlog = new TMAuditLog();
    $tmprizes = new TMPrizes();
    $tmproducts = new TMProducts();
    $tmgamemgt = new TMGameManagement();
    $tmtickets = new TMTickets();
    
    $valno = trim($Virn);
    $fullvirl = trim($Virn);   
    $checksum = substr($Virn, strlen($Virn) - 1, strlen($Virn));    
    $book = trim($TicketNum);
    $gameno = substr($Virn,0,strlen($Virn) - 9);
    $valno = substr($Virn, 0,strlen($Virn) - 1);
   
    $valno2 = substr($valno, strlen($valno) - 8, strlen($valno));
    $ticketno = substr($TicketNum,strlen($TicketNum) - 3,3);
    $book = substr($TicketNum, strlen($TicketNum) - 9 ,6);
    
    //checking of inpput
    if(strlen($UserName)>20)
    {
        return new soap_fault('Client', '', 'Exceeds maximum character for Username.');
    }
    if(strlen($TicketNum)>12)
    {
        return new soap_fault('Client', '', 'Ticket Number exceeds maximum character.');
    }
    elseif(!is_numeric($TicketNum))
    {
        return new soap_fault('Client', '', 'Ticket Number must be a number.');
    }
    
    if(strlen($Virn)>12)
    {
        return new soap_fault('Client', '', 'Validation Number exceeds maximum character.');
    }
    
    elseif(!is_numeric($Virn))
    {
        return new soap_fault('Client', '', 'Validation Number must be a number.');
    }
    
    //check if user exist      
    $UserID = $account->SelectUserIDbyUsername($UserName);
    if(count($UserID) ==1)
    {        
        
        for($i = 0; $i<count($UserID); $i++ )
        {
            $userid = $UserID[$i]['AID'];       
        }         
    }
    else
    {
        return new soap_fault('Client', '', 'Enter correct Username!.');
    }
    
    //check if game number is valid
    $arrGame = $tmgamemgt->SelectByGameNumber($gameno);
    if (count($arrGame) == 1)
    {
        $arrdtls = $arrGame[0];
        $gameid = $arrdtls["GameID"];
    }
    else
    {
        return new soap_fault('Client', '', 'Game number does not Exist!.');
    }
    $client = new nusoap_client('http://192.168.20.8:8088/tms_ws/server.php');
    $num  = array((int)$valno);
    
    
    $checkdigit = $client->call('checkDigit', $num);     
  
    //$checkdigit = checkDigit($num);
    
    if($checkdigit['num'] % 10 != $checksum)
    {
        return new soap_fault('Client', '', 'Please enter a valid VIRN.');        
    } 
    
    else
    {
        //check if valid VIRN
        $arrWinning = $tmticketvalidation->IsValidVIRN($valno2,$gameid);
        $count = count($arrWinning);
        if ($count == 1)
        {
            //check if valid book number
            $arrBookDtls = $tmdecks->IsBookNoValid($book,$gameid);
            if (count($arrBookDtls) == 1)
            {                 
                $arrdtlsbooks = $arrBookDtls[0];
                $statusbooks = $arrdtlsbooks["Status"];
                $bookid = $arrdtlsbooks["BookID"];
                if ($statusbooks == 1)
                {     
                    //check if ticketno is valid
                    $arrTicketDtls = $tmtickets->IsTicketNoValid($ticketno, $bookid);
                    if (count($arrTicketDtls) == 1)
                    {
                        $arrdtls2 = $arrTicketDtls[0];
                        $ticketstatus = $arrdtls2["Status"];
                        if ($ticketstatus == 1 || $ticketstatus == 6)
                        {
                            $arrdtls = $arrWinning[0];
                            $winningcardid = $arrdtls["WinningCardID"];
                            $cardno = $arrdtls["CardNumber"];
                            $prizetypeid = $arrdtls["PrizeTypeID"];
                            $prizeid = $arrdtls["PrizeID"];
                            $prizename = $arrdtls["PrizeName"];
                            $claimdate = $arrdtls["ClaimDate"];
                            $claimtime = $arrdtls["ClaimTime"];
                            $status = $arrdtls["Status"];
                        }
                        elseif ($ticketstatus == 5)
                        {
                            return new soap_fault('Client', '', 'Scratch card has been deemed cancelled. Performing the ticket validation will not be allowed.');
                        }
                    }
                    else
                    {
                        return new soap_fault('Client', '', 'Scratch card does not exist.');                      
                    }
                    
                }
                else
                {
                    if ($statusbooks == 4)
                    {
                        return new soap_fault('Client', '', 'Please enter a book ticket number that has already been activated.');                 
                    }
                    elseif ($statusbooks == 2)
                    {
                        return new soap_fault('Client', '', 'Please enter a book ticket number that has already been activated.');                
                    }
                    elseif ($statusbooks == 3)
                    {
                        return new soap_fault('Client', '', 'Please enter a book ticket number that has already been activated.');                   
                    }
                    elseif ($statusbooks == 5)
                    {
                        return new soap_fault('Client', '', 'Scratch card has been deemed cancelled. Performing the ticket validation will not be allowed.');  
                    }
                }

            }
            else
            {   
                return new soap_fault('Client', '', 'Book number does not exist.');                                
            }
            
        }
        else
        {
            return new soap_fault('Client', '', 'Sorry, your ticket is not a winning card.');  
        }
                //get payout level and check if user is allowed to payout the prize
        $arrprize = $tmprizes->SelectByPrizeStatus($prizename);                
        if (count($arrprize) == 1)
        {
            $arrdtls = $arrprize[0];
            $payoutlevel = $arrdtls["PayoutLevel"];
        }
        
        if ($status == 0 || $status == 1)// new and verified
            {                 
                if ($prizetypeid == 1)//major prize
                {         
                    if ($accttypeid > 3)
                    {
                        return new soap_fault('Client', '', 'Congratulations, your scratch card has won the major prize. Please go to the Head Office to claim your prize.');  
                    }
                    else
                    {
                      
                        if ($userid > 0)
                        {
                            return new soap_fault('Client', '', 'Your ticket has won a major prize of '.$prizename.'. Claim prize now using the '.$UserName.' account?');  
                        }
                        else
                        {
                            return new soap_fault('Client', '', 'Congratulations, your scratch card has won a major prize of '.$UserName.'. Claim winning prize?');  
                        }  
                    }
                }
                elseif ($prizetypeid == 2)//minor prize
                {   
                    $confirmation = 'true';
                    if ($userid > 0)
                    {
                        $message['try'] = 'Your ticket has won a consolation prize of '.$prizename.'. Claim prize now using the '.$UserName.' account?';
                        return $message;
                        //return new soap_fault('Client', '', 'Your ticket has won a consolation prize of '.$prizename.'. Claim prize now using the '.$UserName.' account?'); 
                    }
                    else
                    {
                        return new soap_fault('Client', '', 'Your ticket has won a consolation prize of '.$prizename.'. Claim prize now using the '.$_SESSION['uname'].' account?'); 
                    }   
                     
                    if (($payoutlevel > 2)  && (($accttypeid == 5) || ($accttypeid == 6)))
                    {
                        return new soap_fault('Client', '', 'Congratulations, your scratch card has won a consolation prize of '.$prizename.'. Your account is not authorized to payout this amount.');                     
                    } 
                   
                }
            }
            elseif ($status == 2) // claimed
            {                
                return new soap_fault('Client', '', 'Scratch card prize has already been claimed last '.$claimdate.' at '.$claimtime.'.');                     
                    
               
            }
            if ($status != '2')
            {
                $tmparam["Status"] = '1';
            }

            $tmparam["WinningCardID"] = $winningcardid;
            $tmupdatewinningcard = $tmticketvalidation->UpdateByArray($tmparam);
            if ($tmticketvalidation->HasError)
            {
                $errormsg = $tmticketvalidation->getError();
                return new soap_fault('Client', '', $errormsg); 
                
            }
    }
    
}
$name = 'getWeather';        
$server->register($name, array('UserName' => 'xsd:string'), array('return'=>'tns:return'));


function getWeather ($a_stInput) 
{          

    if (is_string($a_stInput))
    {   
    //            $l_oDBlink   = @mysql_connect('172.16.102.35', 'TicketManagementCM', 'TicketManagementCM');            
    //            $l_oDBresult = @mysql_db_query('TicketManagementCMdb', 'SELECT UserName, AccountTypeID FROM accounts WHERE UserName = LCASE("' . mysql_escape_string((string)$a_stInput) . '") LIMIT 1');
    $account = new TMAccounts();
    $l_oDBresult = $account->SelectUsername($a_stInput);

    for($i = 0; $i<count($l_oDBresult); $i++ )
    {
        $record1['UserName'] = $l_oDBresult[$i]['UserName'];
        $record1['AccountTypeID'] = $l_oDBresult[$i]['AccountTypeID'];

    }  
    // if no result
    if (count($record1) == 0) 
    {
        return new soap_fault('Server', '', 'No Account can be found that correspond to your username.');
    }

    // return data
    $record = new ArrayList();
    $record->AddArray($record1);
    return $record1;
    } 
    // we accept only a string
    else 
    {
    return new soap_fault('Client', '', 'Service requires a string parameter.');
    }
}

// Use the request to (try to) invoke the service
$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? 
$HTTP_RAW_POST_DATA : '';
$server->service($HTTP_RAW_POST_DATA);

?>
