<?php
// **********************************************
// Webservice Demo by Elm� (www.netcult.ch/elmue)
// **********************************************
?>
<html>
<head>
<title>Amazon WebService Client Demo</title>
<style>
h1  { color:#000066; }
h3  { color:#666600; }
pre { background-color:#FFFFE0; padding:5px; border:1px solid #666600; }
</style>
</head>
<body>
<h1>Amazon SimpleDB Webservice</h1>

<?php
// ----------- AMAZON SimpleDB Webservice (1GB/month free) -------------

$SERVER_URL = "https://sdb.amazonaws.com";

require_once("classUtils.php");
require_once("classWebservice.php");
$Service = new Webservice($SERVER_URL, "POST", "utf-8", "Amazon SimpleDB PHP5 Library");

// You have to subscribe for the SimpleDB Webservice on http://aws.amazon.com/simpledb
// You can obtain your own Access Identifiers (public + private key) on http://aws.amazon.com/account
// Although the first 1GB datatransfer each month is gratis you have to subscribe with valid billing information (e.g. MasterCard)
// otherwise you will get the error:"AWS was not able to validate the provided access credentials."
// If you did not subscribe at all you get the error: "The AWS Access Key Id you provided does not exist in our records."
$PublicKey  = "AIWMFIAEQGPKI3LFRADW";
$PrivateKey = "HC9Yt3ZGK+A8TU2GrH8Llr+M378FZqu04ur8S+eY";

date_default_timezone_set('UTC');

$Params["Action"]         = $_REQUEST["Action"];
$Params["DomainName"]     = $_REQUEST["Domain"];
$Params["Version"]        = "2009-04-15";
$Params["AWSAccessKeyId"] = $PublicKey;
$Params["Timestamp"]      = date("Y-m-d\TH:i:s").".000Z";

// Amazon requires a signature which is calculated from all parameters which are sent and your private key.
// The parameters must include the public key and a valid UTC timestamp
AddAmazonSignature($Params, $PrivateKey);

if ($_REQUEST["Debug"] == "on") $Service->PRINT_DEBUG = true;

flush();
$Response = $Service->SendRequest($Params);

/* returns $Response["Body"]=

<?xml version="1.0"?>
<Response>
	<Errors>
		<Error>
			<Code>AuthFailure</Code>
			<Message>AWS was not able to validate the provided access credentials</Message>
		</Error>
	</Errors>
	<RequestID>7d63d0fa-740b-0113-2bea-c92c5d7f4198</RequestID>
</Response>

*/

$XPath = $Response["XPath"];

echo "<h3>Result:</h3><pre>";	

// To understand XPath queries read this: http://www.w3schools.com/XPath/xpath_syntax.asp
// Note that XML is case sensitive !!
echo "<b>Error</b>: ".Utils::GetValue($XPath, "//Response/Errors/Error/Message");

echo "</pre>";


function AddAmazonSignature(&$Params, $PrivateKey)
{
	$Params["SignatureVersion"] = 1;
	$Params["SignatureMethod"]  = "HmacSHA256";
	
    uksort($Params, 'strcasecmp');

    $Data = '';
    foreach ($Params as $Name => $Value) 
    {
        $Data .= "$Name$Value";
    }
    $Params["Signature"] = base64_encode(hash_hmac('sha256', $Data, $PrivateKey, true));
}

?>
<b><a href="index.php">Back to Startpage</a></b>
</body>
</html>

