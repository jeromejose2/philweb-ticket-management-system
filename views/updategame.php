<?php
/* 
 * Created by Arlene R. Salazar on 09-15-2011
 * Purpose: view for game update
 */
require_once ("init.inc.php");
$pagesubmenuid = 18;
include_once("../controller/managesession.php");
include("../controller/updategameprocess.php");
?>
<?php include("header.php"); ?>
<script>
        function get_products_cardvalue()
        {   
            
            var prodid = document.getElementById('ddlProductName').options[document.getElementById('ddlProductName').selectedIndex].value;

             
            $("#txtCardValue").load(
                    "../controller/get_gamecardvalue.php",
                    {
                       
                        gamename: prodid
                    },

                    function(r,status, xhr)
                    {

                        if(status == "error")
                        {
                            document.getElementById('title').innerHTML = "ERROR!";
                            document.getElementById('msg').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
                            document.getElementById('light').style.display = "block";
                            document.getElementById('fade').style.display = "block";
                        }
                        $('#txtCardValue').val(r);
                        var value = document.getElementById('txtCardValue').value;
//                        if(value > 0)
//                        {
//                        $("#txtCardValue").attr('disabled',true);
//                        $("#txtNewGameName").attr('disabled',true);
//                        }
                    }
                ); 
                if(prodid == "")
                {
                $("#txtCardValue").attr('enabled',true);
                $("#txtNewGameName").attr('enabled',true);
                document.getElementById("txtNewGameName").disabled = false;
                document.getElementById("txtCardValue").disabled = false;
                document.getElementById("txtNewGameName").value = "";
                }else{
                $("#txtNewGameName").attr('disabled',true); 
                
                }
            
        }
        function get_products_cardvalue2()
        {   
            var provider = "<?php echo $providername; ?>";
            var prodid = document.getElementById('txtNewGameName').value;

             
            $("#txtCardValue").load(
                    "../controller/get_gamecardvaluebyname.php",
                    {
                       
                        gamename: prodid,
                        providername: provider
                    },

                    function(r,status, xhr)
                    {

                        if(status == "error")
                        {
                            document.getElementById('title').innerHTML = "ERROR!";
                            document.getElementById('msg').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
                            document.getElementById('light').style.display = "block";
                            document.getElementById('fade').style.display = "block";
                        }

                            $('#txtCardValue').val(r);
                            var value = document.getElementById('txtCardValue').value;
//                            if(value > 0)
//                            {
//                            $("#txtCardValue").attr('disabled',true);
//                            $("#txtNewGameName").attr('disabled',true);
//                            //document.getElementById("txtNewGameName").disabled = false;
//                            }else{
//                            //document.getElementById("txtCardValue").disabled = true;
//                            
//                            }
                        
                        
                    }
                );  
                    
            
        }        
    function ClearTextbox()
	{
		var product = document.getElementById("ddlProductName").options[document.getElementById('ddlProductName').selectedIndex].value;
		if(product == "")
		{
			document.getElementById("txtNewGameName").disabled = false;
		}
		else
		{
			document.getElementById("txtNewGameName").disabled = true;
                        document.getElementById("txtNewGameName").value = "";
                        document.getElementById("txtCardValue").value = "";
                            $("#txtCardValue").attr('disabled',true);
                            $("#txtNewGameName").attr('disabled',true);
		}
	}
    $(document).ready(function(){
        $('#txtGameNumber').bind("cut copy paste",function(e) {
          e.preventDefault();
        });
    });
function verifycardvalue(event)
{
    var charCode = (event.which) ? event.which : event.keyCode;
    if ((charCode == 46))/*  .=46 characters are allowed*/
  {  
    return true;
    return true;
  }else{
  if((charCode == 32) ||(charCode > 32 && charCode < 48) || (charCode > 57 && charCode < 65) || (charCode > 90 && charCode < 97)
          || (charCode > 122 && charCode < 128) || (charCode > 31) && (charCode < 48 || charCode > 57)) 
    return false;
    return true;
  }
}
</script>
<div id="fade" class="black_overlay"></div>
<!-- POP UP FOR MESSAGES -->
<div id="light" class="white_content">
    <div id="title" class="light-title"></div>
    <div id="msg" class="light-message"></div>
    <div id="button" class="light-button"><input type="button" onclick="javascript: document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none';" value="Okay"/></div>
    <div class="light-footer"></div>
</div>
<!-- POP UP FOR MESSAGES -->
<div class="titleCont">
            <div class="titleCont_left"></div>
            <div class="titleCont_body">Games</div>
            <div class="titleCont_right"></div>
    </div>
<form name="frmUpdateGame" method="post">
    <?php echo $hiddenproviderid;?>
    <?php echo $hiddenproductid;?>
    <?php echo $hiddenselectedid;?>
    
    <div class="content-page">
    <table class="form-add-provider">
        <tr>
            <th colspan="3">Game Profile: </th>
        </tr>
        <tr>
            <td>Provider: </td>
            <td><?php echo $providername; ?></td>
        </tr>
        <tr>
            <td>Game Number: </td>
            <td><?php echo $txtGameNumber; ?></td>
        </tr>
        <tr>
            <td>Game Name: </td>
            <td><?php echo $ddlProductName; ?></td>
            <td><?php echo $txtNewGameName; ?></td>
<!--            <td><?php //echo $txtProductName;?></td>-->
        </tr>
         <tr>
            <td>Card Value: </td>
            <td><?php echo $txtCardValue; ?></td>
        </tr>
        <tr>
            <td>Book Size: </td>
            <td><?php echo $txtBookSize; ?></td>
        </tr>            
        <tr>
            <td colspan="3">
                <div class="form-button">
                    <div><?php echo $btnSave?> &nbsp; <?php echo $btnCancel;?></div>
                </div>
            </td>
        </tr>
    </table>
    </div>
<!-- POP UP FOR CONFIRMATION MESSAGES -->
<div id="light2" class="white_content">
    <div id="title2" class="light-title">Confirmation</div>
    <div id="msg2" class="light-message">
        Commit changes?
    </div>
    <div id="button2" class="light-button">
        <?php echo $btnConfirm;?>
        <input type="button" onclick="javascript: document.getElementById('light2').style.display='none';document.getElementById('fade').style.display='none';" value="Cancel"/>
    </div>
    <div class="light-footer"></div>
</div>
<!-- POP UP FOR CONFIRMATION MESSAGES -->
</form>
<?php if(isset($confirm)):?>
    <script>
        document.getElementById('light2').style.display = "block";
        document.getElementById('fade').style.display = "block";
    </script>
<?php endif;?>
<?php if(isset($errormsg)):?>
    <script>
        document.getElementById('title').innerHTML = "<?php echo $errortitle;?>";
        document.getElementById('msg').innerHTML = "<?php echo $errormsg;?>";
        document.getElementById('light').style.display = "block";
        document.getElementById('fade').style.display = "block";
    </script>
<?php endif; ?>
<?php if(isset($successmsg)):?>
    <script>
        document.getElementById('title').innerHTML = "<?php echo $successtitle;?>";
        document.getElementById('msg').innerHTML = "<?php echo $successmsg;?>";
        document.getElementById('button').innerHTML = "<?php echo $btnOkay?>";
        document.getElementById('light').style.display = "block";
        document.getElementById('fade').style.display = "block";
    </script>
<?php endif; ?>
<?php include("footer.php"); ?>


