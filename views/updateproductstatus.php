<?php
/*
 * @author 
 * Purpose   : view for updateproductstatus
 */
require_once ("init.inc.php");
$pagesubmenuid = 23;
include_once("../controller/managesession.php");
include("../controller/updateproductprocess.php");
?>
<html>
    <body>
        <form name="frmProduct" method="post">
            <div>
                <div>Update Product Information</div>
                <div><?php echo $ddlProduct; ?>&nbsp;<?php echo $btnSearch; ?><br/><br/></div>
                <div><?php echo $txtName; ?></div>
                <div><?php echo $txtDescription; ?></div>
                <div><?php echo $btnUpdate; ?></div>
            </div>          
        </form>
    </body>
</html>
