<?php
/*
* Added By : Arlene R. Salazar
* Added On : Aug 18, 2011
* Purpose: For updating operator
*/
require_once ("init.inc.php");
$pagesubmenuid = 1;
include_once("../controller/managesession.php");
include("../controller/updateuserprocess.php");
?>
<script type="text/javascript" src="jscripts/validations.js"></script>
<?php include("header.php"); ?>
<script>
    $(document).ready(function(){
        $('#txtfname').bind("cut copy paste",function(e) {
          e.preventDefault();
        });
        $('#txtcontactnum').bind("cut copy paste",function(e) {
          e.preventDefault();
        });
        $('#txtemail').bind("cut copy paste",function(e) {
          e.preventDefault();
        });
    });
</script>
<div id="fade" class="black_overlay"></div>
<div class="titleCont">
            <div class="titleCont_left"></div>
            <div class="titleCont_body">Account List</div>
            <div class="titleCont_right"></div>
</div>
<form name="frmUpdateUser" method="POST">
    <div id="acctdtls">
        <table class="form-add-account">
            <tr>
                <!-- <th colspan="2">Account Information</th> -->
		<th>Account Information</th>
		<td>
                    <a href="#" onclick="javascript: return resetPassword();" style="text-decoration: none">Reset Password</a> &nbsp;&nbsp;&nbsp;
                    <a href="#" onclick="javascript: return changePassword();" style="text-decoration: none">Change Status</a>
                </td>
            </tr>
            <tr>
            <tr>
                <td><?php echo $hiddenpostedid;?><?php echo $txtusername; ?> <?php echo $hiddenacctid;?></td>
                <td><?php echo $ddlaccttype; ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Status : <?php echo $status;?></td>
            </tr>
            <!-- <tr>
                <td><?php echo $txtpassword; ?></td>
                <td>Status : <?php echo $status;?></td>
            </tr>
            <tr>
                <td><?php echo $txtcpassword; ?></td>
                <td>
                    <?php //echo $btnResetPword; ?> &nbsp; <?php //echo $btnChangeStat;?>
                </td>
            </tr> -->
            <tr><th colspan="2">Personal Information</th></tr>
            <tr>
                <td colspan="2"><?php echo $txtfname;?></td>
            </tr>
            <tr>
                <td colspan="2"><?php echo $txtaddress;?></td>
            </tr>
            <tr><th colspan="2">Contact Information</th></tr>
            <tr>
                <td><?php echo $txtcontactnum;?></td>
                <td><?php echo $txtemail;?></td>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="form-button">
                        <div><?php echo $btnUpdate;?> &nbsp; <?php echo $btnCancel;?></div>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <div id="chgestat" style="display: none;">
        <table class="form-change-status">
	    <tr>
		<th>Status Information: </th>
	    </tr>
            <tr>
                <td>Current Status: <?php echo $status;?></td>
            </tr>
            <tr>
                <td><?php echo $ddlstatus;?></td>
            </tr>
            <tr>
		<td colspan="2">
                    <div class="form-button">
                        <div><?php echo $btnChange;?> &nbsp; <?php echo $btnCancelChangeStat;?></div>
                    </div>
                </td>
            </tr>
        </table>
    </div>
<!-- POP UP FOR MESSAGES -->
<div id="light3" class="white_content">
    <div id="title3" class="light-title"></div>
    <div id="msg3" class="light-message"></div>
    <div id="button3" class="light-button">
	<input type="button" id="btnOk" name="btnOk" value="Okay" onclick="javascript: document.getElementById('light3').style.display = 'none';document.getElementById('fade').style.display = 'none';"/>
    </div>
    <div class="light-footer"></div>
</div>
<!-- POP UP FOR MESSAGES -->
<!-- POP UP FOR CONFIRMATION MESSAGES -->
<div id="light4" class="white_content">
    <div id="title4" class="light-title">CONFIRMATION</div>
    <div id="msg4" class="light-message">Save changes?</div>
    <div id="button4" class="light-button">
        <?php echo $btnConfirm;?>
        <input type="button" id="btnOk" name="btnOk" value="Cancel" onclick="javascript: document.getElementById('light4').style.display = 'none';document.getElementById('fade').style.display = 'none';"/>
    </div>
    <div class="light-footer"></div>
</div>
<!-- POP UP FOR CONFIRMATION MESSAGES -->
<!-- POP UP FOR RESET PASSWORD MESSAGES -->
<div id="light8" class="white_content">
    <div id="title8" class="light-title">CONFIRMATION</div>
    <div id="msg8" class="light-message">Reset Password?</div><!--Would you like to reset the password for <?php echo $username;?>?-->
    <div id="button8" class="light-button">
        <?php echo $btnConfirmResetPword;?>
        <input type="button" id="btnOk" name="btnOk" value="Cancel" onclick="javascript: document.getElementById('light8').style.display = 'none';document.getElementById('fade').style.display = 'none';"/>
    </div>
    <div class="light-footer"></div>
</div>
<!-- POP UP FOR RESET PASSWORD MESSAGES -->
<!-- POP UP FOR CONFIRMATION MESSAGES -->
    <div id="light7" class="white_content">
        <div id="title7" class="light-title">CONFIRMATION</div>
        <div id="msg7" class="light-message"></div>
        <div id="button7" class="light-button">
            <?php echo $btnConfirmChangeStat;?>
            <input type="button" id="btnOk" name="btnOk" value="Cancel" onclick="javascript: document.getElementById('light7').style.display = 'none';document.getElementById('fade').style.display = 'none';"/>
        </div>
        <div class="light-footer"></div>
    </div>
    <!-- POP UP FOR CONFIRMATION MESSAGES -->
<?php if(isset($updateuser_msg)): ?>
    <script>
        document.getElementById('title3').innerHTML = "<?php echo $updateuser_title;?>";
        document.getElementById('msg3').innerHTML = "<?php echo $updateuser_msg;?>";
        document.getElementById('light3').style.display = "block";
        document.getElementById('fade').style.display = "block";
    </script>
<?php endif; ?>
<?php if(isset($okupdateuser_title)): ?>
    <script>
        document.getElementById('light4').style.display = "block";
        document.getElementById('fade').style.display = "block";
    </script>
<?php endif; ?>
<?php $updateuser_msg = null;$updateuser_title = null;$okupdateuser_title = null;?>

<?php if(isset($confupdateuser_msg)): ?>
    <script>
        document.getElementById('title3').innerHTML = "<?php echo $confupdateuser_title;?>";
        document.getElementById('msg3').innerHTML = "<?php echo $confupdateuser_msg;?>";
        document.getElementById('light3').style.display = "block";
        document.getElementById('fade').style.display = "block";
        //document.getElementById('button3').innerHTML = "<input type='button' value='OKAY' onclick='javascript: return redirectToAcctList();'/>";
        document.getElementById('button3').innerHTML = "<?php echo $btnOkay;?>";
    </script>
<?php endif; ?>
<?php $confupdateuser_msg = null;$confupdateuser_title = null;?>
<?php if(isset($updatestat_msg)): ?>
    <script>
        document.getElementById('title3').innerHTML = "<?php echo $updatestat_title;?>";
        document.getElementById('msg3').innerHTML = "<?php echo $updatestat_msg;?>";
        document.getElementById('light3').style.display = "block";
        document.getElementById('fade').style.display = "block";
    </script>
<?php endif;?>
<?php $updatestat_msg = null; $updatestat_title= null;?>
<?php if(isset($emailmsg)):?>
    <script>
        document.getElementById('title3').innerHTML = "<?php echo $emailtitle;?>";
        document.getElementById('msg3').innerHTML = "<?php echo $emailmsg;?>";
        document.getElementById('light3').style.display = "block";
        document.getElementById('fade').style.display = "block";
    </script>
<?php endif;?>
<?php $emailtitle = null; $emailmsg = null;?>
</form>
<?php include("footer.php"); ?>