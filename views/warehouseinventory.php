<?php
/*
 * @author 
 * Purpose   : view for warehouseinventory
 */
include("init.inc.php");
$pagesubmenuid = 3;
include_once("../controller/managesession.php");
include("../controller/warehouseinvprocess.php");
?>
 <script type="text/javascript" src="jscripts/jquery-1.5.2.min.js"></script>
<script language="javascript" type="text/javascript">
function ChangePage(pagenum)
{
    selectedindex = document.getElementById("pgSelectedPage");
    selectedindex.value = pagenum;
    document.forms[0].submit();
}


function editDeckChkBox()
{
    /*
    var chkbx = document.getElementById('checkbox').checked;

    if (document.getElementById('checkbox').checked)
    {
        alert("Please select at least one record.");
        return false ;
    }
    else
    {
        document.forms["frmDecks"].action="warehouseinventory_update.php";
        document.forms["frmDecks"].submit();
        return true;
    }
    */
    document.forms["frmDecks"].action = "warehouseinventory_update.php";
    document.forms["frmDecks"].submit();
}

function Check(chk)
{
	
    /*for (i = 0; i < chk.length; i++)
        chk[i].checked = true ;*/
	if(chk == undefined)
	{
		//alert("ok");
	}
	else
	{
		if(document.forms["frmDecks"].maincheckbox.checked == true)
		{
			if(("<?php echo $_SESSION['accttype']?>" == 2) || ("<?php echo $_SESSION['accttype']?>" == 4) || ("<?php echo $_SESSION['accttype']?>" == 7))
			{
		
			}
			else
                            if ("<?php echo $boolean ?>" == true)
                                {
                                    document.getElementById('btnviewdtls').style.display = "block";
                                } else {
                                    document.getElementById('btnchngestatus').style.display = "block";
                                }
				
		    if(chk.length > 0)
		    {
		        for (i = 0; i < chk.length; i++)
		            chk[i].checked = true ;
		    }
		    else
		    {
		        chk.checked = true ;
		    }
		    /*document.getElementById('btnchngestatus').style.display = "block";
		    for (i = 0; i < chk.length; i++)
		    chk[i].checked = true ;*/
		}else
		{
			if(("<?php echo $_SESSION['accttype']?>" == 2) || ("<?php echo $_SESSION['accttype']?>" == 4) || ("<?php echo $_SESSION['accttype']?>" == 7))
			{
		
			}
			else
		    	document.getElementById('btnchngestatus').style.display = "none";
			if(chk.length > 0)
		    {
		        for (i = 0; i < chk.length; i++)
		            chk[i].checked = false ;
		    }
		    else
		    {
		        chk.checked = false ;
		    }
		    /*for (i = 0; i < chk.length; i++)
		    chk[i].checked = false ;*/
		}
	}
}

function check_active()
{
    if(document.getElementById('ddlstatus').value == 1)
    {
        document.getElementById('ddlassignedto').disabled = false;
    }
    else if(document.getElementById('ddlstatus').value == 4)
    {
        document.getElementById('ddlassignedto').disabled = false;
    }
    else
    {
        document.getElementById('ddlassignedto').disabled = true;
    }
}

function validation()
{
    var statusval = document.getElementById('ddlstatus').options[document.getElementById('ddlstatus').selectedIndex].value;
    var assignedto = document.getElementById('ddlassignedto').options[document.getElementById('ddlassignedto').selectedIndex].value;
    var gamenumber = document.getElementById('ddlgametype').options[document.getElementById('ddlgametype').selectedIndex].value;
    var gamename = document.getElementById('ddlgamename').options[document.getElementById('ddlgamename').selectedIndex].value;
    var gamebatch = document.getElementById('ddlgamebatch').options[document.getElementById('ddlgamebatch').selectedIndex].value;
    var cardvalue = document.getElementById('ddlcardvalue').options[document.getElementById('ddlcardvalue').selectedIndex].value;
    if(cardvalue == ' ')
    {
        document.getElementById('title').innerHTML = "ERROR!";
        document.getElementById('msg').innerHTML = " Please select a card value.";
        document.getElementById('light').style.display = "block";
        document.getElementById('fade').style.display = "block";
        return false;
    }
    if(statusval == 0)
    {
        document.getElementById('title').innerHTML = "ERROR!";
        document.getElementById('msg').innerHTML = " Please select an item status.";
        document.getElementById('light').style.display = "block";
        document.getElementById('fade').style.display = "block";
        return false;
    }
    if(gamename == 0)
    {
        document.getElementById('title').innerHTML = "ERROR!";
        document.getElementById('msg').innerHTML = " Please select a game name.";
        document.getElementById('light').style.display = "block";
        document.getElementById('fade').style.display = "block";
        return false;
    }
    if(gamenumber == 0)
    {
        document.getElementById('title').innerHTML = "ERROR!";
        document.getElementById('msg').innerHTML = "Please select a game number.";
        document.getElementById('light').style.display = "block";
        document.getElementById('fade').style.display = "block";
        return false;
    }
     if(gamebatch == 0)
    {
        document.getElementById('title').innerHTML = "ERROR!";
        document.getElementById('msg').innerHTML = "Please select a game batch.";
        document.getElementById('light').style.display = "block";
        document.getElementById('fade').style.display = "block";
        return false;
    }
    
    if((statusval == 1) || (statusval == 4))
    {
        if(assignedto == 0)
        {
            document.getElementById('title').innerHTML = "ERROR!";
            document.getElementById('msg').innerHTML = "Please select assigned to.";
            document.getElementById('light').style.display = "block";
            document.getElementById('fade').style.display = "block";
            return false;
        }
    }
    return true;
}

function get_product()
{
    var pid = document.getElementById('ddlgametype').value;
    
    $.ajax
    ({
        url: '../controller/get_product.php?pid='+pid,
        type : 'post',
        success : function(data)
        {
            $('#ddlprodtype').val(data)
        },
        error: function(e)
        {
            alert("Error");
        }
    });
}

function enablebutton(chk)
{
    document.forms["frmDecks"].maincheckbox.checked = false;
    var chkBoxCount = chk.length;
    var totalChecked = 0;
    if(chkBoxCount > 0)
    {
        for (var i = 0; i < chkBoxCount; i++)
        {
            //check the state of each CheckBox
            //replace "YourFormName" with the name of your form
            if (eval("chk[" + i + "].checked") == true)
            {
                //it's checked so increment the counter
                totalChecked += 1;
            }
        }
    }
    else
    {
        if (eval("chk.checked") == true)
        {
            //it's checked so increment the counter
            totalChecked += 1;
			document.forms["frmDecks"].maincheckbox.checked = true;
        }
    }

	if(("<?php echo $_SESSION['accttype']?>" == 2) || ("<?php echo $_SESSION['accttype']?>" == 4) || ("<?php echo $_SESSION['accttype']?>" == 7))
	{
		
	}
	else
	{
		if(totalChecked == chkBoxCount)
		{
		    document.forms["frmDecks"].maincheckbox.checked = true;
		}
		if(totalChecked > 0)
		    document.getElementById('btnchngestatus').style.display = "block";
		else
		    document.getElementById('btnchngestatus').style.display = "none";
	}
}



function checksearchdata()
{
    var searchstr = document.getElementById('txtsearch').value;
    if(searchstr.replace(/^\s\s*/, '').replace(/\s\s*$/, '').length == 0)
    {
        document.getElementById('title').innerHTML = "ERROR!";
        document.getElementById('msg').innerHTML = "Please enter search details.";
        document.getElementById('light').style.display = "block";
        document.getElementById('fade').style.display = "block";
        return false;
    }
    return true;
}

$(document).ready(function()
{
        $("#ddlgamename").attr('disabled',true);
        $("#ddlgametype").attr('disabled',true);
        $("#ddlgamebatch").attr('disabled',true);
        $("#ddlstatus").attr('disabled',true);
   var cardvalue = document.getElementById('ddlcardvalue').options[document.getElementById('ddlcardvalue').selectedIndex].value;
            if(cardvalue != ' ')
                 {
                     $("#ddlstatus").attr('disabled',false);
                     $("#ddlgamename").attr('disabled',false);
                     $("#ddlgametype").attr('disabled',false);
                     $("#ddlgamebatch").attr('disabled',false);
                     document.getElementById("flag").value = 4;
                     
                    if(document.getElementById('ddlstatus').value == 1)
                        {
                            document.getElementById('ddlassignedto').disabled = false;
                        }
                        else if(document.getElementById('ddlstatus').value == 4)
                        {
                            document.getElementById('ddlassignedto').disabled = false;
                        }
                        else
                        {
                            document.getElementById('ddlassignedto').disabled = true;
                        }
                 }
        
});
function get_status()
{
  
     $("#ddlstatus").attr('disabled',false);
     var setTodefault = document.getElementById ('ddlstatus');
        setTodefault.value = 0; //set to default -please select
                
      var cardvalue = document.getElementById('ddlcardvalue').options[document.getElementById('ddlcardvalue').selectedIndex].value;
       var status = document.getElementById('ddlstatus').options[document.getElementById('ddlstatus').selectedIndex].value;
    if(cardvalue == ' ')
         {
            $("#ddlstatus").attr('disabled',true);
            $("#ddlgamename").attr('disabled',true);
            $("#ddlgametype").attr('disabled',true);
            $("#ddlgamebatch").attr('disabled',true);
             document.getElementById('ddlassignedto').disabled = true;
         }
          if(cardvalue != ' ' &&  status == 0 )
         {
            $("#ddlgamename").attr('disabled',true);
            $("#ddlgametype").attr('disabled',true);
            $("#ddlgamebatch").attr('disabled',true);
             document.getElementById('ddlassignedto').disabled = true;
         }
     
}
function get_gamename()
{
    document.getElementById("flag").value = 2;
     var setTodefault = document.getElementById ('ddlgamename');
        setTodefault.value = 0; //set to default -please select
        
    $("#ddlgamename").attr('disabled',false);
    var cardvalue = document.getElementById('ddlcardvalue').options[document.getElementById('ddlcardvalue').selectedIndex].value;
    var gamename = document.getElementById('ddlgamename').options[document.getElementById('ddlgamename').selectedIndex].value;
    var gamenum = document.getElementById('ddlgametype').options[document.getElementById('ddlgametype').selectedIndex].value;
    var gamebatch = document.getElementById('ddlgamebatch').options[document.getElementById('ddlgamebatch').selectedIndex].value;
      var status = document.getElementById('ddlstatus').options[document.getElementById('ddlstatus').selectedIndex].value;
    var flag = 0;
         if(status == 0)
         {
            $("#ddlgamename").attr('disabled',true);
            $("#ddlgametype").attr('disabled',true);
            $("#ddlgamebatch").attr('disabled',true);
             document.getElementById('ddlassignedto').disabled = true;
         }
          if(status != 0 && (gamename != 0 || gamename == 0) && (gamenum != 0 || gamenum == 0)&& (gamebatch != 0 || gamebatch == 0))
            {
                 $("#ddlgametype").attr('disabled',true);
                 $("#ddlgamebatch").attr('disabled',true);
                  document.getElementById('ddlassignedto').disabled = true;
            }
    $("#ddlgamename").load(
        "../controller/get_gamename.php",
        {
            cardvalue: cardvalue,
            gamename: gamename,
            flag: flag
        },

        function(status, xhr)
        {
            if(status == "error")
            {
                document.getElementById('title').innerHTML = "ERROR!";
                document.getElementById('msg').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
                document.getElementById('light').style.display = "block";
                document.getElementById('fade').style.display = "block";
            }
        }
    );
}


function get_gamenumber()
{
    document.getElementById("flag").value = 1;
    $("#ddlgametype").attr('disabled',false);
    //document.getElementById("ddlgametype").onchange = get_batchID;
    document.getElementById("xmltype").value = 1;
    document.getElementById("ddlgamebatch").innerHTML = "<option value ='0'>Please select</option>";
    var prodid = document.getElementById('ddlgamename').options[document.getElementById('ddlgamename').selectedIndex].value;
    var cardvalue = document.getElementById('ddlcardvalue').options[document.getElementById('ddlcardvalue').selectedIndex].value;
     var gamenum = document.getElementById('ddlgametype').options[document.getElementById('ddlgametype').selectedIndex].value;
     var gamebatch = document.getElementById('ddlgamebatch').options[document.getElementById('ddlgamebatch').selectedIndex].value;
    var prodid1 = prodid;
     if(prodid == 0)
         {
            $("#ddlgametype").attr('disabled',true);
            $("#ddlgamebatch").attr('disabled',true);
            
         }
            if(gamebatch == 0 && gamenum != 0)
                {
                    $("#ddlgamebatch").attr('disabled',true);
                }
//    if ( (prodid ==0 || cardvalue==0) && (prodid !=0 && cardvalue==0) )
//    { 
//       var prodid1 = 0;
//        //document.getElementById("ddlgametype").onchange= function(){get_batchID()};    // get_gamename();
//    }
    if(cardvalue == 0)
        {
                            $("#ddlgametype").load(
                        "../controller/get_gamenum.php",
                        {
                            prodid: prodid
                            
                        },
                        function(status, xhr)
                        {
                            if(status == "error")
                            {
                                document.getElementById('title').innerHTML = "ERROR!";
                                document.getElementById('msg').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
                                document.getElementById('light').style.display = "block";
                                document.getElementById('fade').style.display = "block";
                            }
                        }
                    );
        }else
        {
            
                $("#ddlgametype").load(
                    "../controller/get_gamenum_1.php",
                    {
                        prodid: prodid
                        ,cardvalue : cardvalue
                    },
                    function(status, xhr)
                    {
                        if(status == "error")
                        {
                            document.getElementById('title').innerHTML = "ERROR!";
                            document.getElementById('msg').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
                            document.getElementById('light').style.display = "block";
                            document.getElementById('fade').style.display = "block";
                        }
                    }
                );
        }
}

function get_batchID()
{
    $("#ddlgamebatch").attr('disabled',false);
    document.getElementById("xmltype").value = 1;
    var gamenum = document.getElementById('ddlgametype').options[document.getElementById('ddlgametype').selectedIndex].value;   
    var gamebatch = document.getElementById('ddlgamebatch').options[document.getElementById('ddlgamebatch').selectedIndex].value;
    var assingedto = document.getElementById('ddlassignedto').options[document.getElementById('ddlassignedto').selectedIndex].value;
    var xmltype = document.getElementById('xmltype').value;    
     if(gamenum == 0)
         {
            $("#ddlgamebatch").attr('disabled',true);
            document.getElementById('ddlassignedto').disabled = true;
         }
         if(gamenum != 0 && (gamebatch !=0 || gamebatch==0) && (assingedto != 0 || assingedto == 0))
         {
            document.getElementById('ddlassignedto').disabled = true;
         }
    $("#ddlgamebatch").load(
        "../controller/get_gamebatches.php",
        {
            gamenum: gamenum,
            xmltype:xmltype            
        },
            
        function(status, xhr)
        {
            if(status == "error")
            {
                document.getElementById('title').innerHTML = "ERROR!";
                document.getElementById('msg').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
                document.getElementById('light').style.display = "block";
                document.getElementById('fade').style.display = "block";
            }
        }
    );
}
function get_assigndto()
{
     var setTodefault = document.getElementById('ddlassignedto');
        setTodefault.value = 0; //set to default -please select
        
         var gamebatch = document.getElementById('ddlgamebatch').options[document.getElementById('ddlgamebatch').selectedIndex].value;
         if(gamebatch == 0)
         {
              document.getElementById('ddlassignedto').disabled = true;
         }else
             {
                if(document.getElementById('ddlstatus').value == 1)
                {
                    document.getElementById('ddlassignedto').disabled = false;
                }
                else if(document.getElementById('ddlstatus').value == 4)
                {
                    document.getElementById('ddlassignedto').disabled = false;
                }
                else
                {
                    document.getElementById('ddlassignedto').disabled = true;
                }
                
             }
         
             
      
}
</script>
</script>



<?php include("header.php"); ?>
<div id="fade" class="black_overlay"></div>
<div id="loading" class="loading"></div>
<!-- POP UP FOR MESSAGES -->
<div id="light" class="white_content">
    <div id="title" class="light-title"></div>
    <div id="msg" class="light-message"></div>
    <div id="button" class="light-button"><input type="button" onclick="javascript: document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none';" value="OKAY"/></div>
    <div class="light-footer"></div>
</div>
<!-- POP UP FOR MESSAGES -->
<form id="frmDecks" name="frmDecks" method="post">
    <?php echo $flag?>
    <?php echo $xmltype?>
<div class="titleCont">
            <div class="titleCont_left"></div>
            <div class="titleCont_body">Head Office Inventory</div>
            <div class="titleCont_right"></div>
    </div>
    <?php echo $hiddenctrid;?>
    <div class="content-page">
    <div class="search-container">
        <div class="form-search">
            <?php echo $txtsearch; ?><?php echo $btnsearch; ?>
        
        </div>
        <br/>
        <br/>
        <div class="form-view">
            Card Value: <?php echo $ddlcardvalue;?>
             Status: <?php echo $ddldeckstatus; ?> 
            Game Name: <?php echo  $ddlgamename;?>
            Game Number: <?php echo $ddlgametype; ?>
            Game Batch: <?php echo $ddlgamebatch?>
            Assigned To: <?php echo $ddlassignedto; ?>
            <div class="form-button">
               &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <?php echo $btnSubmit; ?>
            </div>
        </div>
    </div>
<!--    <div class="form-actions">
        
    </div>-->
    <?php if(isset($tbldata_list)):?>
    <div class="form-page"><?php echo $pgwarehouseinv; ?><br/><?php echo $btnExportCSV;?></div>
    <table class="table-list" style="clear: both;">
        <tr>
            <!-- <th><input type="checkbox" id="checkbox2" name="checkbox2" value="yes" onClick="Check(document.forms['frmDecks'].checkbox)" /></th> -->
            <?php if (($_SESSION['accttype'] == 2) || ($_SESSION['accttype'] == 4)) { } else { ?>
            <th><input type="checkbox" id="maincheckbox" name="maincheckbox" value="yes" onClick="Check(document.frmDecks.checkbox)" /></th>
            <?php } ?>
            <th>Card Value</th>
            <th>Game Name</th>
            <th>Book Size</th>
            <th>Game Number</th>
            <th>Game Batch</th>
            <th>Carton Number</th>
            <th>Book Number</th>            
            <th>Status</th>
            <?php if ($selectedstatus == 1 || $selectedstatus == 4) {?>
            <th>Assigned To</th>
            <?php }
            if ($selectedstatus == 1){
            ?>
			<th>Date Activated</th>
                        <th>Invoice Number</th>
                        <?php }?>
                      
        </tr>
        <?php if(count($tbldata_list) > 0):?>
        <?php for($ctr = 0 ; $ctr < count($tbldata_list) ; $ctr++):?>
        <?php ($ctr % 2) == 0 ? $class = "evenrow" : $class = "oddrow" ; ?>
        <tr class = "<?php echo $class?>">
            <!-- <td><input type="checkbox" id="box" name="checkbox[]" value="<?php echo $tbldata_list[$ctr]['ID']?>" /></td> -->
            <?php if (($_SESSION['accttype'] == 2) || ($_SESSION['accttype'] == 4)) { } else { ?>
            <td><input type="checkbox" id="checkbox" name="checkbox[]" value="<?php echo $tbldata_list[$ctr]['ID']?>" onclick="javascript: return enablebutton(document.frmDecks.checkbox);"/></td>
            <?php } ?>
            <td><?php echo $tbldata_list[$ctr]['CardPrice']?></td>
            <td><?php echo $tbldata_list[$ctr]['ProductName']?></td>
            <td><?php echo $tbldata_list[$ctr]['BookTicketCount']?></td>
            <td><?php echo $tbldata_list[$ctr]['GameNumber']?></td>
            <td><?php echo $tbldata_list[$ctr]['BatchID']?></td>
            <td><?php echo $tbldata_list[$ctr]['BoxNumber']?></td>
            <td><?php echo $tbldata_list[$ctr]['BookNumber']?></td>            
            <td><?php echo $tbldata_list[$ctr]['Status']?></td>
            <?php if ($selectedstatus == 1 || $selectedstatus == 4) {?>
            <td><?php echo $tbldata_list[$ctr]['Username']?></td>
            <?php }
            if ($selectedstatus == 1){
            ?>
                        <td><?php echo $tbldata_list[$ctr]['DateReleased'];?></td>
                        <td><?php echo $tbldata_list[$ctr]['InvoiceNumber'];?></td>
                        <?php }?>
                        
        </tr>
        <?php endfor;?>
        <?php else:?>
        <tr class="no-record"><td colspan="9">No records to display</td></tr>
        <?php endif;?>
    </table>
    <?php endif;?>
    <br/>
    <div style="float: right;">
	<?php echo $btnchngestatus; ?>
        <?php echo $btnviewdtls; ?>
	<br/>
    </div>
    <!-- <div style="float: right; margin-right: -8%; margin-top: 30px;"> -->
</div>
</form>
<div class="content-page"><div class="form-page"><?php echo $pgwarehouseinv; ?></div></div>
<?php include("footer.php"); ?>