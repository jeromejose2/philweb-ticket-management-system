<?php
include("init.inc.php");
$pagesubmenuid = 3;
include_once("../controller/managesession.php");
include("../controller/warehouseinventory_updateprocess.php");
?>
<link rel="stylesheet" type="text/css" media="screen" href="../css/default.css" />
<script language="javascript" src="js/popcalendar2.js"></script>
<script type="text/javascript" src="jscripts/jquery-1.5.2.min.js" language="Javascript"></script> 
<link rel="stylesheet" type="text/css" href="css/datepicker.css" />
<script type="text/javascript" src="jscripts/datetimepicker.js"></script>
<script language="javascript" src="jscripts/validations.js"></script>
<script language="javascript" type="text/javascript">
    $(document).ready(function(){
        $('input[type=text],[type=password]').bind('cut copy paste', function (e) {
            e.preventDefault();
        });
    });
    
    function get_address()
    {
        var aid = document.getElementById('ddlassignedto').value;

        $.ajax
        ({
            url: '../controller/get_address.php?aid='+aid,
            type : 'post',
            success : function(data)
            {
                $('#txtaddress').val(data);
                //alert(data);
            },
            error: function(e)
            {
                alert("Error");
            }
        });
    }

    function check_active()
    {
        if(document.getElementById('hidden_assignedid').value == "")
            hidden_assignedid = "";
        else
            hidden_assignedid = document.getElementById('hidden_assignedid').value;
        if(document.getElementById('hidden_assignedaddress').value == "")
            hidden_assignedaddress = "";
        else
            hidden_assignedaddress = document.getElementById('hidden_assignedaddress').value;
        if(document.getElementById('ddlstatus').value == 4)
        {
            $("#ddlassignedto").val(hidden_assignedid);
            document.getElementById('ddlassignedto').disabled = false;
            document.getElementById('date').style.visibility = "hidden";
            document.getElementById('btnprocess').disabled = false;
            document.getElementById('txtinvoice').disabled = true;
            document.getElementById('txtaddress').value = hidden_assignedaddress;
            document.getElementById('txtreleasedate').value = "";
        }
        else if(document.getElementById('ddlstatus').value == 1)
        {
            $("#ddlassignedto").val(hidden_assignedid);
            document.getElementById('date').style.visibility = "visible";
            document.getElementById('txtinvoice').disabled = false;
            document.getElementById('txtinvoice').readOnly = false;
            document.getElementById('ddlassignedto').disabled = true;
            document.getElementById('txtaddress').value = hidden_assignedaddress;
        }
        else
        {
            document.getElementById('ddlassignedto').disabled = true;
            document.getElementById('date').style.visibility = "hidden";
            document.getElementById('btnprocess').disabled = false;
            document.getElementById('txtinvoice').disabled = true;
            document.getElementById('ddlassignedto').selectedIndex = 0;
            document.getElementById('txtaddress').value = "";
            document.getElementById('txtreleasedate').value = "";
        }
    }

    function check_date()
    {

    }

    function redirectToWarehouseInventory()
    {
        window.location = "warehouseinventory.php";
    }

    function checkupdatestatus()
    {
        var status = document.getElementById('ddlstatus').options[document.getElementById('ddlstatus').selectedIndex].value;
        var assignedto = document.getElementById('ddlassignedto').options[document.getElementById('ddlassignedto').selectedIndex].value;
        if(status == "")
        {
            document.getElementById('title').innerHTML = "ERROR!";
            document.getElementById('msg').innerHTML = "Please select status.";
            document.getElementById('light').style.display = "block";
            document.getElementById('fade').style.display = "block";
            return false;
        }
        if(status == 4)
        {
            if(assignedto == 0)
            {
                document.getElementById('title').innerHTML = "ERROR!";
                document.getElementById('msg').innerHTML = "Please select an account from the Assigned To field.";
                document.getElementById('light').style.display = "block";
                document.getElementById('fade').style.display = "block";
                return false;
            }
        }
        if(status == 1)
        {
            var currentdate = document.getElementById('hidden_date').value;
            var releasedate = document.getElementById('txtreleasedate').value;
            var txtinvoice = document.getElementById('txtinvoice').value;
            var arr1 = releasedate.split('-');
            var arr2 = currentdate.split('-');
        
            var dt1 = new Date();
            dt1.setFullYear(arr1[2], arr1[1], arr1[0]);
            var dt2 = new Date();
            dt2.setFullYear(arr2[2], arr2[1], arr2[0]);
            var sBankTransNewDate = new Date(arr1[2],arr1[0],arr1[1]);
            var sCurrentNewDate = new Date(arr2[2],arr2[0],arr2[1]);
            var sDateDiff = ((sCurrentNewDate - sBankTransNewDate) / 86400000); /* 86400000 is the answer of (60 * 60 * 24 * 1000)*/

            if(releasedate.replace(/^\s\s*/, '').replace(/\s\s*$/, '').length == 0)
            {
                document.getElementById('title').innerHTML = "ERROR!";
                document.getElementById('msg').innerHTML = "Invalid release date.";
                document.getElementById('light').style.display = "block";
                document.getElementById('fade').style.display = "block";
                return false;
            }
            if(txtinvoice.length == 0 )
            {
                document.getElementById('title').innerHTML = "ERROR!";
                document.getElementById('msg').innerHTML = "Please supply an invoice number.";
                document.getElementById('light').style.display = "block";
                document.getElementById('fade').style.display = "block";
                return false;
            }
            if(txtinvoice.length < 5 )
            {
                document.getElementById('title').innerHTML = "ERROR!";
                document.getElementById('msg').innerHTML = "Invoice number must not be less than 5 digits.";
                document.getElementById('light').style.display = "block";
                document.getElementById('fade').style.display = "block";
                return false;
            }
        }
        //document.getElementById('light2').style.display = "block";
        //document.getElementById('fade').style.display = "block";
        return true;
    }

    function CloseLightBox()
    {
        check_active();
        document.getElementById('light2').style.display = "none";
        document.getElementById('fade').style.display = "none";
        return false;
    }
</script>
<?php include("header.php"); ?>
<div id="fade" class="black_overlay"></div>
<!-- POP UP FOR MESSAGES -->
<div id="light" class="white_content">
    <div id="title" class="light-title"></div>
    <div id="msg" class="light-message"></div>
    <div id="button" class="light-button"><input type="button" onclick="javascript: document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none';" value="OKAY"/></div>
    <div class="light-footer"></div>
</div>
<!-- POP UP FOR MESSAGES -->
<form id="frmDeckUpdt" name="frmDeckUpdt" method="post">
    <?php echo $hidden_date;
    echo $hidden_assignedaddress;
    echo $hidden_assignedid; ?>
    <div class="titleCont">
        <div class="titleCont_left"></div>
        <div class="titleCont_body">Item Information</div>
        <div class="titleCont_right"></div>
    </div>
    <div style="margin-bottom: 10px; margin-left: 10px; margin-top: 10px;">
        <label style="font-weight: bold; font-size: 16px;"><u>Item Information</u></label>
        <br/>
<?php echo $ddldeckstatus; ?>
        <br/><br/>
        <label style="font-weight: bold; font-size: 16px;"><u>Assignment Information</u></label>
        <br/><br/>
        <?php echo $ddlassignedto; ?>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<?php echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Release Date:  " . $txtreleasedate; ?>
        <!-- <a id="date" style="visibility: hidden;" href="javascript: popUpCalendar(document.forms[0].imgCalStart, document.forms[0].elements['txtreleasedate'], 'yyyy-mm-dd');"><img id="imgCalStart" src="images/calendar.gif" align="absMiddle" border="0"></a> -->
        <!-- <a id="date" style="visibility: hidden;" href="#"><img name="cal" src="../images/close.gif" width="16" height="16" border="0" alt="Pick a date" onClick="displayDatePicker('txtreleasedate', false, 'mm-dd-yyyy', '-');"/></a> -->
        <img name="date" id="date" style="visibility: hidden;" src="images/close.gif" width="16" height="16" border="0" alt="Pick a date" onClick="displayDatePicker('txtreleasedate', false, 'mmddyyyy', '-');"/>
        <br/><br/>
        <?php echo $txtaddress; ?> &nbsp;&nbsp;
<?php echo $txtinvoice; ?>
    </div>
    <table class="table-list">
        <tr>
            <th>Card Value</th>
            <th>Game Name</th>
            <th>Book Size</th>
            <th>Game Number</th>            
            <th>Carton Number</th>
            <th>Book Number</th>
            <th>Status</th>
            <th>Assigned To</th>
            <th>Invoice Number</th>
        </tr>
        <?php for ($ctr = 0; $ctr < count($tbldata_list); $ctr++): ?>
                <?php ($ctr % 2) == 0 ? $class = "evenrow" : $class = "oddrow"; ?>
            <tr class = "<?php echo $class ?>">
                <?php
                Switch ($tbldata_list[$ctr]['Status'])
                {
                    case 1:
                        $status = "Active";
                        break;
                    case 2:
                        $status = "On Freight";
                        break;
                    case 3:
                        $status = "On Stock";
                        break;
                    case 4:
                        $status = "Assigned";
                        break;
                    case 5:
                        $status = "Cancelled";
                        break;
                    default:
                        break;
                }
                ?>
                <td><?php echo $tbldata_list[$ctr]['CardPrice'] ?></td>
                <td><?php echo $tbldata_list[$ctr]['Product'] ?></td>
                <td><?php echo $tbldata_list[$ctr]['BookTicketCount'] ?></td>
                <td><?php echo $tbldata_list[$ctr]['GameNumber'] ?></td>            
                <td><?php echo $tbldata_list[$ctr]['BoxNumber'] ?></td>
                <td><?php echo $tbldata_list[$ctr]['BookNumber'] ?></td>
                <td><?php echo $tbldata_list[$ctr]['StatusName']; ?></td>
                <td><?php echo $tbldata_list[$ctr]['Username'] ?></td>
                <td><?php echo $tbldata_list[$ctr]['InvoiceNumber'] ?></td>
            </tr>
<?php endfor; ?>
    </table>
    <br/>
    <div style="float: right;">
<?php echo $btnprocess; ?> &nbsp; <?php echo $btncancel; ?>
    </div>
    <br/><br/>
    <!-- POP UP FOR CONFIRMATION MESSAGES -->
    <div id="light2" class="white_content">
        <div id="title2" class="light-title"></div>
        <div id="msg2" class="light-message"></div>
        <div id="button2" class="light-button">
            <?php echo $btnConfirm; ?>
<?php echo $btnClose; ?>
        </div>
        <div class="light-footer"></div>
    </div>
    <!-- POP UP FOR CONFIRMATION MESSAGES -->
</form>
<?php include("footer.php"); ?>
<?php if ( isset($successtitle) ): ?>
    <script>
        document.getElementById('title').innerHTML = "<?php echo $successtitle ?>";
        document.getElementById('msg').innerHTML = "<?php echo $successmsg ?>";
        document.getElementById('button').innerHTML = "<?php echo $btnOkay ?>";
        document.getElementById('light').style.display = "block";
        document.getElementById('fade').style.display = "block";
    </script>
<?php endif; ?>
<?php if ( isset($confirmtitle) ): ?>
    <script>
        document.getElementById('title2').innerHTML = "<?php echo $confirmtitle ?>";
        document.getElementById('msg2').innerHTML = "<?php echo $confirmmsg ?>";
        document.getElementById('light2').style.display = "block";
        document.getElementById('fade').style.display = "block";
    </script>
<?php endif; ?>