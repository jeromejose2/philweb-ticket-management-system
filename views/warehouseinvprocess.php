<?php
/* * *****************
 * Author: J.O. Pormento
 * Date Created: 2011-09-01
 * Description: View booklets and Change status
 * ***************** */
$pagesubmenuid = 3;

App::LoadModuleClass("TicketManagementCM", "TMAccounts");
App::LoadModuleClass("TicketManagementCM", "TMDecks");

App::LoadControl("TextBox");
App::LoadControl("ComboBox");
App::LoadControl("Button");
App::LoadControl("PagingControl2");
App::LoadControl("Hidden");

$frmwarehouseinv = new FormsProcessor();
$account = new TMAccounts();
$deck = new TMDecks();

$accounts = $account->SelectByAccountType();
$accounts_list = new ArrayList();
$accounts_list->AddArray($accounts);


$ddldeckstatus = new ComboBox("ddlstatus", "ddlstatus", "Status: ");
$litem = null;
$litem[] = new ListItem("Please select", "0");
$litem[] = new ListItem("Active", "1");
$litem[] = new ListItem("On Freight", "2");
$litem[] = new ListItem("On Stock", "3");
$litem[] = new ListItem("Assigned", "4");
$litem[] = new ListItem("Cancelled", "5");
$ddldeckstatus->Items = $litem;
$ddldeckstatus->ShowCaption = true;
$ddldeckstatus->Args = "style='margin-top: 10px;'";


$btnSubmit = new Button("btnSubmit","btnSubmit","Submit");
$btnSubmit->IsSubmit = true;
//$btnSubmit->Args = "onclick='javascript: return checkAcctType();'";


$txtsearch = new TextBox("txtsearch","txtsearch","");
$txtsearch->ShowCaption = true;
$txtsearch->Args = "style='margin-left: 35%;'";


$btnsearch = new Button("btnsearch","btnsearch","Search");
$btnsearch->IsSubmit = true;
$btnsearch->Args = "onchange='check_active();'";


$ddlassignedto = new ComboBox("ddlassignedto", "ddlassignedto", "Assigned To: ");
$litemassto = null;
$litemassto[] = new ListItem("Please select", "0", true);
$ddlassignedto->Items = $litemassto;
$ddlassignedto->ShowCaption = true;
$ddlassignedto->DataSource = $accounts_list;
$ddlassignedto->DataSourceText = "Name";
$ddlassignedto->DataSourceValue = "AID";
$ddlassignedto->DataBind();


$btnchngestatus = new Button("btnchngestatus","btnchngestatus","Change Status");
$btnchngestatus->IsSubmit = false;
$btnchngestatus->Args = "onclick='javascript: return editDeckChkBox();'";

//pagination
$itemsperpage = 50;
$pgcon = new PagingControl2($itemsperpage, 1);
$pgcon->URL = "javascript:ChangePage(%currentpage);";
$pgcon->ShowMoveToFirstPage = true;
$pgcon->ShowMoveToLastPage = true;
$pgcon->PageGroup = 5;


$frmwarehouseinv->AddControl($ddldeckstatus);
$frmwarehouseinv->AddControl($btnSubmit);
$frmwarehouseinv->AddControl($txtsearch);
$frmwarehouseinv->AddControl($btnsearch);
$frmwarehouseinv->AddControl($ddlassignedto);
$frmwarehouseinv->AddControl($btnchngestatus);


$frmwarehouseinv->ProcessForms();


if($frmwarehouseinv->IsPostBack)
{
    if ($ddldeckstatus->SubmittedValue != 0 && $ddlassignedto->SubmittedValue != 0)
    {
        $count = $deck->SelectDeckByStatusAndAssignedToCount($ddldeckstatus->SubmittedValue,$ddlassignedto->SubmittedValue);
        $rowcount = $count[0]['Count'];

        //start pagination//
        if ($btnSubmit->SubmittedValue == "Submit")
        {
            $pgcon->SelectedPage = 1;
        }
        $pgcon->Initialize($itemsperpage, $rowcount);
        $pgwarehouseinv = $pgcon->PreRender();
        //end pagination//

        $tbldata = $deck->SelectDeckByStatusAndAssignedToWithLimit($ddldeckstatus->SubmittedValue,$ddlassignedto->SubmittedValue,$pgcon->SelectedItemFrom-1,$itemsperpage);
        $tbldata_list = new ArrayList();
        $tbldata_list->AddArray($tbldata);
    }
    else if ($ddldeckstatus->SubmittedValue != 0)
    {
        $count = $deck->SelectDeckByStatusCount($ddldeckstatus->SubmittedValue);
        $rowcount = $count[0]['Count'];

        //start pagination//
        if ($btnSubmit->SubmittedValue == "Submit")
        {
            $pgcon->SelectedPage = 1;
        }
        $pgcon->Initialize($itemsperpage, $rowcount);
        $pgwarehouseinv = $pgcon->PreRender();
        //end pagination//
        
        $tbldata = $deck->SelectDeckByStatusWithLimit($ddldeckstatus->SubmittedValue,$pgcon->SelectedItemFrom-1,$itemsperpage);
        $tbldata_list = new ArrayList();
        $tbldata_list->AddArray($tbldata);
    }
    else if ($ddlassignedto->SubmittedValue != 0)
    {
        $count = $deck->SelectDeckByAssignedToCount($ddlassignedto->SubmittedValue);
        $rowcount = $count[0]['Count'];

        //start pagination//
        if ($btnSubmit->SubmittedValue == "Submit")
        {
            $pgcon->SelectedPage = 1;
        }
        $pgcon->Initialize($itemsperpage, $rowcount);
        $pgwarehouseinv = $pgcon->PreRender();
        //end pagination//

        $tbldata = $deck->SelectDeckByAssignedToWithLimit($ddlassignedto->SubmittedValue,$pgcon->SelectedItemFrom-1,$itemsperpage);
        $tbldata_list = new ArrayList();
        $tbldata_list->AddArray($tbldata);
    }
    else if ($txtsearch->SubmittedValue != '')
    {
        $count = $deck->SelectDeckByBookNumCount($txtsearch->SubmittedValue);
        $rowcount = $count[0]['Count'];

        //start pagination//
        if ($btnSubmit->SubmittedValue == "Submit")
        {
            $pgcon->SelectedPage = 1;
        }
        $pgcon->Initialize($itemsperpage, $rowcount);
        $pgwarehouseinv = $pgcon->PreRender();
        //end pagination//

        $tbldata = $deck->SelectDeckByBookNumWithLimit($txtsearch->SubmittedValue,$pgcon->SelectedItemFrom-1,$itemsperpage);
        $tbldata_list = new ArrayList();
        $tbldata_list->AddArray($tbldata);
    }
}

?>