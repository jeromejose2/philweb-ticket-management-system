<?php
/*
 * Created By: Arlene R. Salazar 03/21/2012
 * Purpose: View for multiple inventory xml uploads
 */
include("init.inc.php");
$pagesubmenuid = 33;
include_once("../controller/managesession.php");
include("../controller/winnersmultipleuploadprocess.php");
?>
<?php include("header.php"); ?>
<style type="text/css">
    .fileupload
    {
        padding: 5px;
    }
</style>
<script type="text/javascript">
    function uploadFiles()
    {      
        document.forms['multipleupload'].submit();        
        document.getElementById('loading').style.display = "block";
        document.getElementById('fade').style.display = "block";
        document.getElementById('loading-image').style.display = "block";
        return true;
    }

    function addFileUpload()
    {
        var oldhiddenid = document.getElementById('hiddenid').value;
        var newhiddenid = parseInt(oldhiddenid) + 1;
        document.getElementById('hiddenid').value = newhiddenid;
        var newfileupload = '<div id = "file' + newhiddenid + '"  class="fileupload">Filename: &nbsp;<input type="file" name="file' + newhiddenid + '" id="file' + newhiddenid + '" size="50" onchange="javascript: return uploadFiles();"/></div>';
        document.getElementById('fileuploads').innerHTML += newfileupload;
    }

    function redirecttoMultipleUpload()
    {
        window.location = "winnersmultipleupload.php";
    }
</script>
<div class="titleCont">
    <div class="titleCont_left"></div>
    <div class="titleCont_body">Winners File</div>
    <div class="titleCont_right"></div>
</div>
<div class="content-page">
    <form action="winnersmultipleupload.php" method="post" enctype="multipart/form-data" id="multipleupload" name="multipleupload">
        <div id="fade" class="black_overlay"></div>
        <!-- POP UP FOR MESSAGES -->
        <div id="light" class="white_content">
            <div id="title" class="light-title"></div>
            <div id="msg" class="light-message"></div>
            <div id="button" class="light-button">
                <input type="button" onclick="javascript: document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none';" value="Okay"/>
            </div>
            <div class="light-footer"></div>
        </div>
        <div id="light1" class="white_content">
            <div id="title1" class="light-title"></div>
            <div id="msg1" class="light-message"></div>
            <div id="button" class="light-button">
                <?php echo $btnOkay;?>
            </div>
            <div class="light-footer"></div>
        </div>
        <div id="light2" class="white_content">
            <div id="title2" class="light-title"></div>
            <div id="msg2" class="light-message"></div>
            <div id="button" class="light-button">
                <?php echo $btnConfirmUpload;?>
                <input type="button" onclick="javascript: document.getElementById('light2').style.display='none';document.getElementById('fade').style.display='none';" value="No"/>
            </div>
            <div class="light-footer"></div>
        </div>
        <div id="loading" class="white_content">
            <div class="light-title"></div>
            <div class="light-message">
                <?php if(isset($loading)){echo $loading;} ?><br/><br/>
                <div class="loading" id="loading-image"></div>
            </div>
                <div class="light-footer"></div>
        </div>
        <!-- POP UP FOR MESSAGES -->

        <?php echo $hiddenid;?>
        <?php echo $hiddenUploadedFiles;?>
        <?php echo $hiddenTag;?>
        <?php echo $hiddenSuccessfulUpload;?>
        <?php echo $hiddenVIRNCount;?>
        <?php echo $hiddenPrizeCount;?>
        <?php echo $hiddenProviderName;?>
        <?php echo $hiddenGameName;?>
        <?php echo $hiddenGameNum;?>
        <table>
            <tr>
                <td><div class="fileupload"><?php echo $ddlProviders;?></div></td>
            </tr>
            <tr>
                <td>
                    <div id="fileuploads">
                        <div id="file1" class="fileupload">
                            Filename: &nbsp;<input type="file" name="file1" id="file1" size="50" onchange="javascript: return uploadFiles();"/>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <a href="#" onclick="javascript:return addFileUpload();">Add more files</a>
                    <div class="form-button">
                        <?php echo $btnDone; ?>
                    </div>
                </td>
            </tr>
        </table>   
    </form>
</div>
<?php if($hiddenTag->Text == 1):?>
    <script>
        <?php
            $filesuploaded_array = explode("<br/>",$hiddenUploadedFiles->Text);
        ?>
        <?php if($successful_upload == 1):?>
            document.getElementById('file1').innerHTML = "<?php echo $filesuploaded_array[0]?>";
        <?php else:?>
            document.getElementById('file1').innerHTML = "<?php echo $filesuploaded_array[0]?>";
            <?php for($i = 2 , $a = 1 ; $i <= $successful_upload ; $i++,$a++): ?>
                var newfileupload = '<div id = "file' + <?php echo $i;?> + '" class="fileupload">Filename: &nbsp;<input type="file" name="file' + <?php echo $i;?> + '" id="file' + <?php echo $i;?> + '" size="50" onchange="javascript: return uploadFiles()"/></div>';
                document.getElementById('fileuploads').innerHTML += newfileupload;
                document.getElementById('file' + <?php echo $i;?>).innerHTML = "<?php echo $filesuploaded_array[$a]?>";
            <?php endfor;?>
        <?php endif;?>
    </script>
<?php endif;?>
<?php if(isset($errormsg)):?>
    <script>
        document.getElementById('title').innerHTML = "<?php echo $errormsgtitle; ?>";
        document.getElementById('msg').innerHTML = "<?php echo $errormsg; ?>";
        document.getElementById('light').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
    </script>
<?php endif;?>
<?php if(isset($countmsg)):?>
    <script>
        document.getElementById('title1').innerHTML = "<?php echo $counttitle; ?>";
        document.getElementById('msg1').innerHTML = "<?php echo $countmsg; ?>";
        document.getElementById('light1').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
    </script>
<?php endif;?>
<?php if(isset($confirmuploadmsg)):?>
<script>
    document.getElementById('title2').innerHTML = "<?php echo $confirmuploadtitle; ?>";
    document.getElementById('msg2').innerHTML = "<?php echo $confirmuploadmsg; ?>";
    document.getElementById('light2').style.display = 'block';
    document.getElementById('fade').style.display = 'block';
</script>
<?php endif;?>
<?php include("footer.php"); ?>