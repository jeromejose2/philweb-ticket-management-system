<?php
/*
 * @author 
 * Purpose   : view for winnersxmlupload
 */
include("init.inc.php");
$pagesubmenuid = 33;
include_once("../controller/managesession.php");
include("../controller/winnersxmluploadprocess.php");
?>
<?php include("header.php"); ?>
<div id="fade" class="black_overlay"></div>
<div id="loading" class="white_content">
	<div class="light-title"></div>
    <div class="light-message">
        Uploading the winners file.<br/>
        <div class="loading" id="loading-image"></div>
    </div>
	<div class="light-footer"></div>
</div>
<form name="frmXMLUpload" method="post" enctype="multipart/form-data">
<!-- POP UP FOR MESSAGES -->
<div id="light2" class="white_content">
    <div id="title2" class="light-title"></div>
    <div id="msg2" class="light-message"></div>
    <div id="button2" class="light-button">
        <div id="button2A" class="button2A"></div>
        <div id="button2B" class="button2B">
            <input type="button" onclick="javascript: document.getElementById('light2').style.display='none';document.getElementById('fade').style.display='none';" value="No"/>
        </div>
    </div>
    <div class="light-footer"></div>
</div>
<!-- POP UP FOR MESSAGES -->
    <?php echo $hiddenfilename;?>
    <?php echo $hiddengameid;?>
    <?php echo $hiddenproductid;?>
    <?php echo $hiddenproviderid;?>
    <?php echo $hiddenprovidername;?>
    <?php echo $hiddenprodname;?>
    <?php echo $hiddengamenum;?>
    <?php echo $hiddengamecode;?>
    <?php echo $hiddenbatchid;?>
<div class="titleCont">
    <div class="titleCont_left"></div>
    <div class="titleCont_body">Winners File</div>
    <div class="titleCont_right"></div>
</div>
<div class="content-page">
    <table>
<!--        <tr>
            <td class="labelboldunderline">Winners File Upload</td>
        </tr>-->
        <tr>
            <td >&nbsp;</td>
        </tr>
        <tr>
            <td><?php echo $ddlProviders;?></td>
        </tr>
        <!-- <tr>
            <td><?php echo $ddlProducts; ?></td>
        </tr> -->
        <tr>
            <td>
                <label for="file">File Name:</label>
                <input type="file" name="file" id="file" size="50"/>
            </td>
        </tr>
        <tr>
            <td>
                <div class="form-button">
                    <?php echo $btnUpload; ?>
                </div>
            </td>
        </tr>
     </table>
</div>    
<!-- POP UP FOR MESSAGES -->
<div id="light" class="white_content">
    <div id="title" class="light-title"></div>
    <div id="msg" class="light-message"></div>
    <div id="button" class="light-button"><input type="button" onclick="javascript: document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none';" value="OKAY"/></div>
    <div class="light-footer"></div>
</div>
<!-- POP UP FOR MESSAGES -->
<?php 
//echo isset($_SESSION['errormsg']);
if (isset($errormsg))
{
?>
<script>
    document.getElementById('title').innerHTML = "<?php echo $errormsgtitle; ?>";
    document.getElementById('msg').innerHTML = "<?php echo $errormsg; ?>";
    document.getElementById('light').style.display = 'block';
    document.getElementById('fade').style.display = 'block';
</script> 
<?php
}
?>
<?php if(isset($countmsg)):?>
<script>
    document.getElementById('title').innerHTML = "<?php echo $countmsgtitle; ?>";
    document.getElementById('msg').innerHTML = "<?php echo $countmsg; ?>";
    document.getElementById('light').style.display = 'block';
    document.getElementById('fade').style.display = 'block';
</script> 
<?php endif; ?>
<?php if(isset($confirmuploadmsg)):?>
<script>
    document.getElementById('title2').innerHTML = "<?php echo $confirmuploadtitle; ?>";
    document.getElementById('msg2').innerHTML = "<?php echo $confirmuploadmsg; ?>";
    document.getElementById('button2A').innerHTML = "<?php echo $btnConfirmUpload; ?>";
    document.getElementById('light2').style.display = 'block';
    document.getElementById('fade').style.display = 'block';
</script>
<?php endif;?>
</form>
<?php include("footer.php"); ?>